export default  async function getAllData() {
    try {
      const res = await fetch("http://desktop-5o8tke8:8080/usuarios/value/admin");
      if (!res.ok) {
        const message = `An error has occured: ${res.status} - ${res.statusText}`;
        throw new Error(message);
      }
      const data = await res.json();
      const result = {
        status: "20",
        headers: {
          "Content-Type": res.headers.get("Content-Type"),
          "Content-Length": res.headers.get("Content-Length"),
        },
        length: res.headers.get("Content-Length"),
        data: data,
      };
      //setGetResult(fortmatResponse(result));
      return result
    } catch (err) {
        console.log(err)
        //setGetResult(err.message);
    }

}
