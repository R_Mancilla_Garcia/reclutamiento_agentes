import React, { Component } from "react";
import CanvasJSReact from './canvasjs.react';
import datos from '../urls/datos.json'
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

export default class IndicadoresDesempeno extends Component {

    constructor() {
        super();
        this.muestraTotalAgentes=this.muestraTotalAgentes.bind(this)
        this.construyeGraficas=this.construyeGraficas.bind(this)
        this.setOptionsCanvas=this.setOptionsCanvas.bind(this)
        this.setOptionCards=this.setOptionCards.bind(this)
        this.state={
            muestraTotalAgentes:false,
            optionsAtencion:{},
            optionsDefProv : {},
            optionsDefEstandart:{},
            optionsProvEstandart:{},
            granTotal:0,
            activosTotal:0,
            canceladosTotal:0,
            reservaTotal:0
        }
    }


    UNSAFE_componentWillMount() {
        this.construyeGraficas()
    }


    construyeGraficas(){
        let jsonFechas={"fec_ini": "2022-08-01","fec_fin": "2022-08-31"}
        const requestOptions = {
            method: "POST",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(jsonFechas)
        };
        fetch(datos.urlServicePy+"recluta/api_indicadores01/0", requestOptions)
            .then(response => response.json())
            .then(data => {
                let indicadorGranTotal=data[0].indicador[0]
                let indicadorActivos=data[1].indicador[0]
                let indicadorCancelados=data[2].indicador[0]
                let indicadorReservas=data[3].indicador[0]
                console.log(indicadorActivos)
                this.setOptionCards(indicadorGranTotal.total,indicadorActivos.total,indicadorCancelados.total,indicadorReservas.total)
                this.setOptionsCanvas(indicadorGranTotal.subNivel,indicadorActivos.subNivel,indicadorCancelados.subNivel,indicadorReservas.subNivel)



        })
    }


    setOptionCards(granTotal, activosTotal,canceladosTotal,reservaTotal){
        this.setState({granTotal:granTotal,activosTotal:activosTotal,canceladosTotal:canceladosTotal,reservaTotal:reservaTotal})
    }
    setOptionsCanvas(valoresGranTotal, valoresActivos,valoresCancelados,valoresReserva){
        const optionsAtencion = {
            exportEnabled: true,
            animationEnabled: true,
            title: {
                text: "Estandart de atención"
            },
            data: [{
                type: "pie",
                startAngle: 75,
                toolTipContent: "<b>{label}</b>: {y}%",
                showInLegend: "false",
                legendText: "{label}",
                indexLabelFontSize: 16,
                indexLabel: "{label} - {y}%",
                dataPoints: [
                    { y: valoresGranTotal[0].total, label: "Dentro del estandart" },
                    { y: valoresGranTotal[1].total, label: "Fuera del estandart" },
                ]
            }]
        }

        const optionsDefProv = {
            exportEnabled: true,
            animationEnabled: true,
            title: {
                text: "Definitivos vs Provisionales "
            },
            data: [{
                type: "pie",
                startAngle: 75,
                toolTipContent: "<b>{label}</b>: {y}%",
                showInLegend: "false",
                legendText: "{label}",
                indexLabelFontSize: 16,
                indexLabel: "{label} - {y}%",
                dataPoints: [
                    { y: valoresActivos[0].total, label: "Provisionales" },
                    { y: valoresActivos[1].total, label: "Definitivos" },
                    { y: valoresActivos[2].total, label: "Sin CUA" },
                ]
            }]
        }

        const optionsDefEstandart = {
            exportEnabled: true,
            animationEnabled: true,
            title: {
                text: "Definitivos en tiempo "
            },
            data: [{
                type: "pie",
                startAngle: 75,
                toolTipContent: "<b>{label}</b>: {y}%",
                showInLegend: "false",
                legendText: "{label}",
                indexLabelFontSize: 16,
                indexLabel: "{label} - {y}%",
                dataPoints: [
                    { y: valoresCancelados[0].total, label: "Definitivos en estandart" },
                    { y: valoresCancelados[1].total, label: "Definitivos no en estandart" },
                    { y: valoresCancelados[2].total, label: "Sin CUA" },
                ]
            }]
        }

        const optionsProvEstandart = {
            exportEnabled: true,
            animationEnabled: true,
            title: {
                text: "Provisionales en tiempo "
            },
            data: [{
                type: "pie",
                startAngle: 75,
                toolTipContent: "<b>{label}</b>: {y}%",
                showInLegend: "false",
                legendText: "{label}",
                indexLabelFontSize: 16,
                indexLabel: "{label} - {y}%",
                dataPoints: [
                    { y: valoresReserva[0].total, label: "Definitivos en estandart" },
                    { y: valoresReserva[1].total, label: "Definitivos no en estandart" },
                    { y: valoresReserva[2].total, label: "Sin CUA" },
                ]
            }]
        }

        this.setState({optionsAtencion:optionsAtencion,optionsDefProv:optionsDefProv,optionsDefEstandart:optionsDefEstandart,optionsProvEstandart:optionsProvEstandart})

    }

    muestraTotalAgentes(){
        this.setState({muestraTotalAgentes:!this.state.muestraTotalAgentes})
    }

    render() {
        return (
            <div>
                <br></br>

                <div className="row">

                    <div className="col-xs-6 col-lg-6">
                        <h3 class="mb-0 font-weight-semibold" style={{ color: '#617187', marginLeft: '8px' }}>GENERAR INDICADORES DE DESEMPEÑO </h3>

                    </div>

                    <div className="col-xs-3 col-lg-3">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fecha inicio </span>
                                </span> <input type="date" placeholder="" name="" className="form-control " />
                            </div>
                        </div>
                    </div>


                    <div className="col-xs-3 col-lg-3">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fecha Fin </span>
                                </span> <input type="date" placeholder="" name="" className="form-control " />
                            </div>
                        </div>
                    </div>

                </div>
                <hr></hr>


                <div className="row">
                    <div className="col-xs-6 col-lg-6">
                        <h6 class="mb-0 font-weight-semibold" style={{ color: '#617187', marginLeft: '8px' }}>Estado general de prospectos por volumen y tiempo de respuesta </h6>
                    </div>
                </div>
                <br></br>
                <div className="row">
                    <div className="col-xs-3 col-lg-3">
                        <div class="card">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-xs-4 col-lg-4">
                                        <div class="mr-3 ">
                                            <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#ED6C26', borderColor: '#ED6C26' }}
                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                    class="letter-icon text-white"></span>
                                            </a>
                                        </div>
                                    </div>
                                    <div className="col-xs-6 col-lg-6">
                                        <div className="row">
                                            <h8>GRAN TOTAL</h8>
                                        </div>
                                        <div className="row ">
                                            <h8>{this.state.granTotal}</h8>
                                        </div>
                                    </div>

                                    <div className="col-xs-2 col-lg-2">
                                        <a href="#" onClick={this.muestraTotalAgentes} style={{ height: '45px', width: '45px', backgroundColor: '#ED6C26', borderColor: '#ED6C26' }} class="btn rounded-pill btn-icon btn-sm ">
                                            <i style={{ color: 'white', width: '75px' }} className="icon-user-minus"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="col-xs-3 col-lg-3">
                        <div class="card">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-xs-4 col-lg-4">
                                        <div class="mr-3 ">
                                            <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#78CB5A', borderColor: '#78CB5A' }}
                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                    class="letter-icon text-white"></span>
                                            </a>
                                        </div>
                                    </div>
                                    <div className="col-xs-6 col-lg-6">
                                        <div className="row">
                                            <h8>ACTIVOS</h8>
                                        </div>
                                        <div className="row ">
                                            <h8>{this.state.activosTotal}</h8>
                                        </div>
                                    </div>

                                    <div className="col-xs-2 col-lg-2">
                                        <a href="#" style={{ height: '45px', width: '45px', backgroundColor: '#78CB5A', borderColor: '#78CB5A' }} class="btn rounded-pill btn-icon btn-sm ">
                                            <i style={{ color: 'white', width: '75px' }} className="icon-user-minus"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="col-xs-3 col-lg-3">
                        <div class="card">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-xs-4 col-lg-4">
                                        <div class="mr-3 ">
                                            <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8F9EB3', borderColor: '#8F9EB3' }}
                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                    class="letter-icon text-white"></span>
                                            </a>
                                        </div>
                                    </div>
                                    <div className="col-xs-6 col-lg-6">
                                        <div className="row">
                                            <h8>CANCELADOS</h8>
                                        </div>
                                        <div className="row ">
                                            <h8>{this.state.canceladosTotal}</h8>
                                        </div>
                                    </div>

                                    <div className="col-xs-2 col-lg-2">
                                        <a href="#" style={{ height: '45px', width: '45px', backgroundColor: '#8F9EB3', borderColor: '#8F9EB3' }} class="btn rounded-pill btn-icon btn-sm ">
                                            <i style={{ color: 'white', width: '75px' }} className="icon-user-minus"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-xs-3 col-lg-3">
                        <div class="card">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-xs-4 col-lg-4">
                                        <div class="mr-3 ">
                                            <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#F97777', borderColor: '#F97777' }} class="btn rounded-pill btn-icon btn-sm ">
                                                <span class="letter-icon text-white"></span>
                                            </a>
                                        </div>
                                    </div>
                                    <div className="col-xs-6 col-lg-6">
                                        <div className="row">
                                            <h8>EN RESERVA</h8>
                                        </div>
                                        <div className="row ">
                                            <h8>{this.state.reservaTotal}</h8>
                                        </div>
                                    </div>

                                    <div className="col-xs-2 col-lg-2">
                                        <a href="#" style={{ height: '45px', width: '45px', backgroundColor: '#F97777', borderColor: '#F97777' }} class="btn rounded-pill btn-icon btn-sm ">
                                            <i style={{ color: 'white', width: '75px' }} className="icon-user-minus"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>

            {
                //this.state.muestraTotalAgentes == true &&

                <div className="row">
                    
                <div className="col-xs-3 col-lg-3">
                    <div class="card">
                        <div className="card-body">
                            <div className="row">
                                <CanvasJSChart options={this.state.optionsAtencion}
                                /* onRef={ref => this.chart = ref} */
                                />

                            </div>
                        </div>
                    </div>
                </div>

                <div className="col-xs-3 col-lg-3">
                    <div class="card" >
                        <div className="card-body">
                            <div className="row">
                                <CanvasJSChart options={this.state.optionsDefProv}
                                /* onRef={ref => this.chart = ref} */
                                />

                            </div>
                        </div>
                    </div>
                </div>
           
                <div className="col-xs-3 col-lg-3">
                    <div class="card">
                        <div className="card-body">
                            <div className="row">
                                <CanvasJSChart options={this.state.optionsDefEstandart}
                                /* onRef={ref => this.chart = ref} */
                                />

                            </div>
                        </div>
                    </div>
                </div>


                <div className="col-xs-3 col-lg-3">
                    <div class="card">
                        <div className="card-body">
                            <div className="row">
                                <CanvasJSChart options={this.state.optionsProvEstandart}
                                /* onRef={ref => this.chart = ref} */
                                />

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            }






            </div>

        )
    }

}