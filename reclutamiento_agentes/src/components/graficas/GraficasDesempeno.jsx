import React, { Component } from "react";
import CanvasJSReact from './canvasjs.react';
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

export default class GraficasDesempeno extends Component {

    constructor() {
        super();
        this.state = {
            jsonData: {},
            jsonPieData: {},
            jsonMultiSerie: {},
            jsonMultiColumn: {},
            jsonTimeChart: {},
            jsonLabelFormat: {},
            jsonMonth: {},
            jsonPiramid: {},
            jsonBar100: {},
            selectFruts: undefined,
            idFrut: 0
        };
    }

    onChangeFruta(event) {
        event.stopPropagation();
        event.preventDefault();
        console.log("entro aquí");
        let fruta = this.state.jsonData.data[0].dataPoints;
        let contador = 1;
        /*let frutaSalida = this.state.jsonDataSalida;
        let lenFruta = parseInt(event.target.value) -1;
        //fruta[lenFruta]
        frutaSalida.data[0].dataPoints = fruta[lenFruta];
        this.setState({jsonDataSalida:frutaSalida})*/
        if (event.target.value == 5) {
            for (var i = 0; i < fruta.length; i++) {
                this.chart.options.data[0].dataPoints[i].y = 20 + contador;
                contador = contador + 5
            }
        } else {
            let lenFruta = parseInt(event.target.value) - 1;
            this.chart.options.data[0].dataPoints[lenFruta].y = 5;
        }
        this.chart.render();
    }

    UNSAFE_componentWillMount() {
        console.log("entro en indicadoress::")
        let columnasSalida = {};
        let options = [];
        options.push(<option selected value={1}>{"Entrevista Profunda"}</option>)
        options.push(<option value={2}>{"Venta de Carrera"}</option>)
        options.push(<option value={3}>{"En Conexión"}</option>)
        options.push(<option value={4}>{"En capacitación"}</option>)
        options.push(<option value={5}>{"Todos"}</option>)

        let salida = []
        salida.push(<select onChange={this.onChangeFruta.bind(this)} name={"fruts"} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
        this.setState({ selectFruts: salida })

        let jsonData = this.state.jsonData;
        let jsonPieData = this.state.jsonPieData;
        let jsonMultiSerie = this.state.jsonMultiSerie;
        let jsonMultiColumn = this.state.jsonMultiColumn;
        let jsonTimeChart = this.state.jsonTimeChart;
        let jsonLabelFormat = this.state.jsonLabelFormat;
        let jsonMonth = this.state.jsonMonth;
        let jsonPiramid = this.state.jsonPiramid;
        let jsonBar100 = this.state.jsonBar100;
        jsonData = {
            title: {
                text: "Cancelados"
            },
            data: [{
                type: "column",
                dataPoints: [
                    { label: "Entrevista Profunda", y: 5 },
                    { label: "Venta de Carrera", y: 5 },
                    { label: "En Conexión", y: 3 },
                    { label: "En capacitación", y: 2 }
                ]
            }]
        }
        jsonPieData = {
            exportEnabled: true,
            animationEnabled: true,
            title: {
                text: "Conectados"
            },
            data: [{
                type: "pie",
                startAngle: 75,
                toolTipContent: "<b>{label}</b>: {y}%",
                showInLegend: "true",
                legendText: "{label}",
                indexLabelFontSize: 16,
                indexLabel: "{label} - {y}%",
                dataPoints: [
                    { y: 40, label: "Definitivos Dentro de estandar" },
                    { y: 10, label: "Definitivos Fuera de estandar" },
                    { y: 30, label: "Temporales Dentro de estandar" },
                    { y: 20, label: "Temporales Fuera de estandar" }
                ]
            }]
        }
        jsonMultiSerie = {
            title: {
                text: "Multi-Series Line Chart"
            },
            data: [
                {
                    type: "line",
                    dataPoints: [
                        { x: 10, y: 21 },
                        { x: 20, y: 25 },
                        { x: 30, y: 20 },
                        { x: 40, y: 25 },
                        { x: 50, y: 27 },
                        { x: 60, y: 28 },
                        { x: 70, y: 28 },
                        { x: 80, y: 24 },
                        { x: 90, y: 26 }

                    ]
                },
                {
                    type: "line",
                    dataPoints: [
                        { x: 10, y: 31 },
                        { x: 20, y: 35 },
                        { x: 30, y: 30 },
                        { x: 40, y: 35 },
                        { x: 50, y: 35 },
                        { x: 60, y: 38 },
                        { x: 70, y: 38 },
                        { x: 80, y: 34 },
                        { x: 90, y: 44 }

                    ]
                },
                {
                    type: "line",
                    dataPoints: [
                        { x: 10, y: 45 },
                        { x: 20, y: 50 },
                        { x: 30, y: 40 },
                        { x: 40, y: 45 },
                        { x: 50, y: 45 },
                        { x: 60, y: 48 },
                        { x: 70, y: 43 },
                        { x: 80, y: 41 },
                        { x: 90, y: 28 }

                    ]
                },
                {
                    type: "line",
                    dataPoints: [
                        { x: 10, y: 71 },
                        { x: 20, y: 55 },
                        { x: 30, y: 50 },
                        { x: 40, y: 65 },
                        { x: 50, y: 95 },
                        { x: 60, y: 68 },
                        { x: 70, y: 28 },
                        { x: 80, y: 34 },
                        { x: 90, y: 14 }

                    ]
                }
            ]
        }

        jsonMultiColumn = {
            title: {
                text: "A Multi-series Column Chart"

            },
            data: [{
                type: "column",
                dataPoints: [
                    { x: 10, y: 171 },
                    { x: 20, y: 155 },
                    { x: 30, y: 150 },
                    { x: 40, y: 165 },
                    { x: 50, y: 195 },
                    { x: 60, y: 168 },
                    { x: 70, y: 128 },
                    { x: 80, y: 134 },
                    { x: 90, y: 114 }
                ]
            },
            {
                type: "column",
                dataPoints: [
                    { x: 10, y: 71 },
                    { x: 20, y: 55 },
                    { x: 30, y: 50 },
                    { x: 40, y: 65 },
                    { x: 50, y: 95 },
                    { x: 60, y: 68 },
                    { x: 70, y: 28 },
                    { x: 80, y: 34 },
                    { x: 90, y: 14 }
                ]
            }
            ]
        }

        jsonTimeChart = {
            zoomEnabled: true,

            title: {
                text: "Chart With Date-Time Stamps Inputs"
            },

            data: [
                {
                    type: "area",
                    xValueType: "dateTime",
                    dataPoints: [
                        { x: 1088620200000, y: 71 },
                        { x: 1104517800000, y: 55 },
                        { x: 1112293800000, y: 50 },
                        { x: 1136053800000, y: 65 },
                        { x: 1157049000000, y: 95 },
                        { x: 1162319400000, y: 68 },
                        { x: 1180636200000, y: 28 },
                        { x: 1193855400000, y: 34 },
                        { x: 1209580200000, y: 14 },
                        { x: 1230748200000, y: 34 },
                        { x: 1241116200000, y: 44 },
                        { x: 1262284200000, y: 84 },
                        { x: 1272652200000, y: 4 },
                        { x: 1291141800000, y: 44 },
                        { x: 1304188200000, y: 11 }
                    ]
                }
            ]
        }

        jsonLabelFormat = {
            title: {
                text: "Custom labelFormatter function for axisX"
            },
            axisX: {
                labelFormatter: function (e) {
                    return "x: " + e.value;
                }
            },
            data: [
                {
                    type: "spline",
                    dataPoints: [
                        { y: 5 },
                        { y: 9 },
                        { y: 17 },
                        { y: 32 },
                        { y: 22 },
                        { y: 14 },
                        { y: 25 },
                        { y: 18 },
                        { y: 20 }
                    ]
                }
            ]
        }

        jsonMonth = {
            title: {
                text: "Activos",
                fontFamily: "Impact",
                fontWeight: "normal"
            },

            legend: {
                verticalAlign: "bottom",
                horizontalAlign: "center"
            },
            data: [
                {
                    //startAngle: 45,
                    indexLabelFontSize: 20,
                    indexLabelFontFamily: "Garamond",
                    indexLabelFontColor: "darkgrey",
                    indexLabelLineColor: "darkgrey",
                    indexLabelPlacement: "outside",
                    type: "doughnut",
                    showInLegend: true,
                    dataPoints: [
                        { y: 40, legendText: "Entrevista Profunda", indexLabel: "Entrevista Profunda" },
                        { y: 25, legendText: "Venta de Carrera", indexLabel: "Venta de Carrera" },
                        { y: 15, legendText: "En Conexión", indexLabel: "En Conexión" },
                        { y: 5, legendText: "En Capacitación", indexLabel: "En Capacitación" },
                        { y: 15, legendText: "Convenio de Arranque", indexLabel: "Convenio de Arranque" }
                    ]
                }
            ]
        }

        jsonPiramid = {
            title: {
                text: "Assets of ACME Company"
            },
            animationEnabled: true,
            data: [
                {
                    type: "pyramid",
                    //valueRepresents: "area",
                    indexLabelFontSize: 20,
                    indexLabelFontFamily: "Lucida",
                    dataPoints: [
                        { y: 30000, indexLabel: "Stocks", color: "#4071B0" },
                        { y: 22000, indexLabel: "Bond", color: "#BEDF7A", exploded: true },
                        { y: 20000, indexLabel: "Real Estate", color: "#BCA7E9" },
                        { y: 15000, indexLabel: "Annuities", color: "#F9C89F" },
                        { y: 8000, indexLabel: "Insurance", color: "#D19393" },
                        { y: 10000, indexLabel: "Cash", color: "#CBC689" }
                    ]
                }
            ]
        }

        jsonBar100 = {
            title: {
                text: "Division of products Sold in Quarter."
            },
            toolTip: {
                shared: true
            },
            axisY: {
                title: "percent"
            },
            data: [
                {
                    type: "stackedBar100",
                    showInLegend: true,
                    name: "April",
                    dataPoints: [
                        { y: 600, label: "Water Filter" },
                        { y: 400, label: "Modern Chair" },
                        { y: 120, label: "VOIP Phone" },
                        { y: 250, label: "Microwave" },
                        { y: 120, label: "Water Filter" },
                        { y: 374, label: "Expresso Machine" },
                        { y: 350, label: "Lobby Chair" }

                    ]
                },
                {
                    type: "stackedBar100",
                    showInLegend: true,
                    name: "May",
                    dataPoints: [
                        { y: 400, label: "Water Filter" },
                        { y: 500, label: "Modern Chair" },
                        { y: 220, label: "VOIP Phone" },
                        { y: 350, label: "Microwave" },
                        { y: 220, label: "Water Filter" },
                        { y: 474, label: "Expresso Machine" },
                        { y: 450, label: "Lobby Chair" }

                    ]
                },
                {
                    type: "stackedBar100",
                    showInLegend: true,
                    name: "June",
                    dataPoints: [
                        { y: 300, label: "Water Filter" },
                        { y: 610, label: "Modern Chair" },
                        { y: 215, label: "VOIP Phone" },
                        { y: 221, label: "Microwave" },
                        { y: 75, label: "Water Filter" },
                        { y: 310, label: "Expresso Machine" },
                        { y: 340, label: "Lobby Chair" }

                    ]
                }

            ]

        }

        this.setState({ jsonData: jsonData, jsonPieData: jsonPieData, jsonMultiSerie: jsonMultiSerie, jsonMultiColumn: jsonMultiColumn, jsonTimeChart: jsonTimeChart, jsonLabelFormat: jsonLabelFormat, jsonMonth: jsonMonth, jsonPiramid: jsonPiramid, jsonBar100: jsonBar100 })
    }

    render() {


        return (

            <div class="card">
                <div style={{ 'max-height': '1150px', 'max-width': '2300px', 'overflow': 'scroll' }}>
                    <div className="col-xs-6 col-lg-6">
                        <h3 class="mb-0 font-weight-semibold" style={{ color: '#617187', marginLeft: '8px' }}>GENERAR INDICADORES DE DESEMPEÑO </h3>
                    </div>

                    <div className="col-xs-3 col-lg-3">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Frutas </span>
                                </span> {this.state.selectFruts}
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header text-center bg-white text-dark">
                            <div class="text-center mb-3">
                                <h3 class="mb-0 font-weight-semibold" ><strong>AGENTES</strong></h3>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-4 col-lg-4">
                            <div class="card">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-xs-6 col-lg-6">
                                            <div className="row">
                                                <h8><strong>DEFINITIVOS DENTRO DE ESTANDAR</strong></h8>
                                            </div>
                                            <div className="row">
                                                <h8><strong>DEFINITIVOS FUERA DE ESTANDAR</strong></h8>
                                            </div>
                                            <div className="row">
                                                <h8><strong>TEMPORALES FUERA DE ESTANDAR</strong></h8>
                                            </div>
                                            <div className="row">
                                                <h8><strong>TEMPORALES FUERA DE ESTANDAR</strong></h8>
                                            </div>
                                            <div className="row">
                                                <h8><strong>TOTAL</strong></h8>
                                            </div>
                                        </div>
                                        <div className="col-xs-2 col-lg-2">
                                            <div className="row">
                                                <h8>4</h8>
                                            </div>
                                            <div className="row">
                                                <h8>1</h8>
                                            </div>
                                            <div className="row">
                                                <h8>2</h8>
                                            </div>
                                            <div className="row">
                                                <h8>3</h8>
                                            </div>
                                            <div className="row">
                                                <h8>10</h8>
                                            </div>
                                        </div>
                                        <div className="col-xs-4 col-lg-4">
                                            <div className="row">
                                                <h8></h8>
                                            </div>
                                            <div className="row">
                                                <h8></h8>
                                            </div>
                                            <div className="row">
                                                <div class="col-6 col-lg-1 d-flex align-items-center pb-sm-4">
                                                    <div class="mr-10">
                                                        <a href="#"
                                                            class="btn  rounded-pill btn-icon btn-sm" style={{ borderColor: '#41B3D1' }}> <span style={{ color: '#41B3D1' }}
                                                                class="letter-icon">C</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <h8></h8>
                                            </div>
                                            <div className="row">
                                                <h8></h8>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-4 col-lg-4">
                            <div class="card">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-xs-6 col-lg-6">
                                            <div className="row">
                                                <h8><strong>ENTREVISTA PROFUNDA</strong></h8>
                                            </div>
                                            <div className="row">
                                                <h8><strong>VENTA DE CARRERA</strong></h8>
                                            </div>
                                            <div className="row">
                                                <h8><strong>EN CONEXIÓN</strong></h8>
                                            </div>
                                            <div className="row">
                                                <h8><strong>EN CAPACITACIÓN</strong></h8>
                                            </div>
                                            <div className="row">
                                                <h8><strong>CONVENIO DE ARRANQUE</strong></h8>
                                            </div>
                                            <div className="row">
                                                <h8><strong>TOTAL</strong></h8>
                                            </div>
                                        </div>
                                        <div className="col-xs-2 col-lg-2">
                                            <div className="row">
                                                <h8>4O</h8>
                                            </div>
                                            <div className="row">
                                                <h8>25</h8>
                                            </div>
                                            <div className="row">
                                                <h8>15</h8>
                                            </div>
                                            <div className="row">
                                                <h8>5</h8>
                                            </div>
                                            <div className="row">
                                                <h8>15</h8>
                                            </div>
                                            <div className="row">
                                                <h8>100</h8>
                                            </div>
                                        </div>
                                        <div className="col-xs-4 col-lg-4">
                                            <div className="row">
                                                <h8></h8>
                                            </div>
                                            <div className="row">
                                                <h8></h8>
                                            </div>
                                            <div className="row">
                                                <div class="col-6 col-lg-1 d-flex align-items-center pb-sm-4">
                                                    <div class="mr-2">
                                                        <a href="#"
                                                            class="btn  rounded-pill btn-icon btn-sm" style={{ borderColor: 'rgb(120, 203, 90)' }}> <span style={{ color: 'rgb(120, 203, 90)' }}
                                                                class="letter-icon">A</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <h8></h8>
                                            </div>
                                            <div className="row">
                                                <h8></h8>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-4 col-lg-4">
                            <div class="card">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-xs-6 col-lg-6">
                                            <div className="row">
                                                <h8><strong>ENTREVISTA PROFUNDA</strong></h8>
                                            </div>
                                            <div className="row">
                                                <h8><strong>VENTA DE CARRERA</strong></h8>
                                            </div>
                                            <div className="row">
                                                <h8><strong>EN CONEXIÓN</strong></h8>
                                            </div>
                                            <div className="row">
                                                <h8><strong>EN CAPACITACIÓN</strong></h8>
                                            </div>
                                            <div className="row">
                                                <h8><strong>TOTAL</strong></h8>
                                            </div>
                                        </div>
                                        <div className="col-xs-2 col-lg-2">
                                            <div className="row">
                                                <h8>5</h8>
                                            </div>
                                            <div className="row">
                                                <h8>5</h8>
                                            </div>
                                            <div className="row">
                                                <h8>3</h8>
                                            </div>
                                            <div className="row">
                                                <h8>3</h8>
                                            </div>
                                            <div className="row">
                                                <h8>16</h8>
                                            </div>
                                        </div>
                                        <div className="col-xs-4 col-lg-4">
                                            <div className="row">
                                                <h8></h8>
                                            </div>
                                            <div className="row">
                                                <h8></h8>
                                            </div>
                                            <div className="row">
                                                <div class="col-6 col-lg-1 d-flex align-items-center pb-sm-4">
                                                    <div class="mr-2">
                                                        <a href="#"
                                                            class="btn  rounded-pill btn-icon btn-sm" style={{ borderColor: '#C70039' }}> <span style={{ color: '#C70039' }}
                                                                class="letter-icon">C</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <h8></h8>
                                            </div>
                                            <div className="row">
                                                <h8></h8>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">
                                <div className="input-group">
                                    <CanvasJSChart options={this.state.jsonPieData}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">
                                <div className="input-group">
                                    <CanvasJSChart options={this.state.jsonMonth}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">
                                <div className="input-group">
                                    <CanvasJSChart options={this.state.jsonData}
                                        onRef={ref => this.chart = ref}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">
                                <div className="input-group">
                                    {<CanvasJSChart options={this.state.jsonMultiColumn}
                                    />}
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">
                                <div className="input-group">
                                    {<CanvasJSChart options={this.state.jsonTimeChart}
                                    />}
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">
                                <div className="input-group">
                                    {<CanvasJSChart options={this.state.jsonLabelFormat}
                                    />}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">
                                <div className="input-group">
                                    {<CanvasJSChart options={this.state.jsonMultiSerie}
                                    />}
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">
                                <div className="input-group">
                                    {<CanvasJSChart options={this.state.jsonPiramid}
                                    />}
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">
                                <div className="input-group">
                                    {<CanvasJSChart options={this.state.jsonBar100}
                                    />}
                                </div>
                            </div>
                        </div>
                    </div></div>
            </div>
        )
    }

}