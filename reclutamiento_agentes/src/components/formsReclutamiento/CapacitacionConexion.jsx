import React, { Component } from "react";
import ReporteCapacitacion from "./ReporteCapacitacion";
import ConvenioArranque from "./ConvenioArranque";
import HistoricoGeneral from "./HistoricoGeneral";
import axios from 'axios';
import datos from '../urls/datos.json'

const avatar = require("../imagenes/avatar.png")
export default class CapacitacionConexion extends Component {

    constructor() {
        super();
        this.peticionAReserva = this.peticionAReserva.bind(this)
        this.peticionABaja = this.peticionABaja.bind(this)
        this.regresaryGuardar = this.regresaryGuardar.bind(this)
        this.construyeSelectMotivos = this.construyeSelectMotivos.bind(this)
        this.onClickBotonArchivoIdentificacion = this.onClickBotonArchivoIdentificacion.bind(this)
        this.onClickBotonArchivoActaNacimiento = this.onClickBotonArchivoActaNacimiento.bind(this)
        this.onClickBotonArchivoCertificadoEstudios = this.onClickBotonArchivoCertificadoEstudios.bind(this)
        this.onClickBotonArchivoCurp = this.onClickBotonArchivoCurp.bind(this)
        this.onClickBotonArchivoRFC = this.onClickBotonArchivoRFC.bind(this)
        this.onClickBotonArchivoCedula = this.onClickBotonArchivoCedula.bind(this)
        this.onClickBotonArchivoRenuncia = this.onClickBotonArchivoRenuncia.bind(this)
        this.onClickBotonArchivoComprobantePagoCEI = this.onClickBotonArchivoComprobantePagoCEI.bind(this)
        this.onClickBotonArchivoAprobacionExamenCEI = this.onClickBotonArchivoAprobacionExamenCEI.bind(this)
        this.onClickBotonArchivoConvenioFirmado = this.onClickBotonArchivoConvenioFirmado.bind(this)
        this.onClickBotonArchivoConvenioFirmado = this.onClickBotonArchivoConvenioFirmado.bind(this)
        this.onClickBotonArchivoCapacitacion = this.onClickBotonArchivoCapacitacion.bind(this)
        this.onClickImagen = this.onClickImagen.bind(this)
        this.mostrarReporteCapacitacion = this.mostrarReporteCapacitacion.bind(this)
        this.mostrarConexionCapacitacion = this.mostrarConexionCapacitacion.bind(this)
        this.mostrarConvenioArranque = this.mostrarConvenioArranque.bind(this)
        this.onChange = this.onChange.bind(this)
        this.guardarFormulario = this.guardarFormulario.bind(this)
        this.guardarCapacitacion = this.guardarCapacitacion.bind(this)
        this.construyeSelect = this.construyeSelect.bind(this)
        this.cambiaEstatusDelProspecto = this.cambiaEstatusDelProspecto.bind(this)
        this.state = {
            archivoIdentificacion: undefined, nombreArchivoIdentificacion: "", urlIdentificacion: "",
            muestraDocIdentificacion: false,// se activa al edittar 
            colorSubirArchivoIdentificacion: "#78CB5A", colorTextoSubirArchivoIdentificacion: "#FFFFFF", colorBotonSubirArchivoIdentificacion: "#41B3D1",

            archivoActaNacimiento: undefined, nombreArchivoActaNacimiento: "", urlActaNacimiento: "",
            muestraDocActaNacimiento: false,// se activa al edittar 
            colorSubirArchivoActaNacimiento: "#78CB5A", colorTextoSubirArchivoActaNacimiento: "#FFFFFF", colorBotonSubirArchivoActaNacimiento: "#41B3D1",

            archivoCertificadoEstudios: undefined, nombreArchivoCertificadoEstudios: "", urlCertificadoEstudios: "",
            muestraDocCertificadoEstudios: false,// se activa al edittar 
            colorSubirArchivoCertificadoEstudios: "#78CB5A", colorTextoSubirArchivoCertificadoEstudios: "#FFFFFF", colorBotonSubirArchivoCertificadoEstudios: "#41B3D1",

            archivoCurp: undefined, nombreArchivoCurp: "", urlCurp: "",
            muestraDocCurp: false,// se activa al edittar 
            colorSubirArchivoCurp: "#78CB5A", colorTextoSubirArchivoCurp: "#FFFFFF", colorBotonSubirArchivoCurp: "#41B3D1",

            archivoRFC: undefined, nombreArchivoRFC: "", urlRFC: "",
            muestraDocRFC: false,// se activa al edittar 
            colorSubirArchivoRFC: "#78CB5A", colorTextoSubirArchivoRFC: "#FFFFFF", colorBotonSubirArchivoRFC: "#41B3D1",

            archivoCedula: undefined, nombreArchivoCedula: "", urlCedula: "",
            muestraDocCedula: false,// se activa al edittar 
            colorSubirArchivoCedula: "#78CB5A", colorTextoSubirArchivoCedula: "#FFFFFF", colorBotonSubirArchivoCedula: "#41B3D1",

            archivoRenuncia: undefined, nombreArchivoRenuncia: "", urlRenuncia: "",
            muestraDocRenuncia: false,// se activa al edittar 
            colorSubirArchivoRenuncia: "#78CB5A", colorTextoSubirArchivoRenuncia: "#FFFFFF", colorBotonSubirArchivoRenuncia: "#41B3D1",

            archivoComprobantePagoCEI: undefined, nombreArchivoComprobantePagoCEI: "", urlComprobantePagoCEI: "",
            muestraDocComprobantePagoCEI: false,// se activa al edittar 
            colorSubirArchivoComprobantePagoCEI: "#78CB5A", colorTextoSubirArchivoComprobantePagoCEI: "#FFFFFF", colorBotonSubirArchivoComprobantePagoCEI: "#41B3D1",

            archivoAprobacionExamenCEI: undefined, nombreArchivoAprobacionExamenCEI: "", urlAprobacionExamenCEI: "",
            muestraDocAprobacionExamenCEI: false,// se activa al edittar 
            colorSubirArchivoAprobacionExamenCEI: "#78CB5A", colorTextoSubirArchivoAprobacionExamenCEI: "#FFFFFF", colorBotonSubirArchivoAprobacionExamenCEI: "#41B3D1",

            archivoConvenioFirmado: undefined, nombreArchivoConvenioFirmado: "", urlConvenioFirmado: "",
            muestraDocConvenioFirmado: false,// se activa al edittar 
            colorSubirArchivoConvenioFirmado: "#78CB5A", colorTextoSubirArchivoConvenioFirmado: "#FFFFFF", colorBotonSubirArchivoConvenioFirmado: "#41B3D1",

            archivoCapacitacion: undefined, nombreArchivoCapacitacion: "", urlCapacitacion: "",
            muestraDocCapacitacion: false,// se activa al edittar 
            colorSubirArchivoCapacitacion: "#78CB5A", colorTextoSubirArchivoCapacitacion: "#FFFFFF", colorBotonSubirArchivoCapacitacion: "#41B3D1",

            tipoConexion: '',
            existefoto: false,
            definitivo: false,
            provisional: false,
            provitodefini:false,
            mostrarReporteCapacitacion: false,
            mostrarConvenioArranque: false,
            imagenEnCambio: undefined,
            fotografiaFile: undefined,
            selectMotivosReserva: [],
            selectMotivosBaja: [],
            idMotivoBaja: 0,
            idMotivoReserva: 0,
            selectAgentesConsolidados: [],
            prospecto: {
                "id": null,
                "prospecto_id": null,
                "estatus_id": 17,
                "doc_foto": null,
                "conyuge": null,
                "fecha_conyuge": null,
                "doc_identificacion": null,
                "doc_acta_nacimiento": null,
                "doc_cerfificado": null,
                "doc_curp": null,
                "doc_rfc": null,
                "doc_cedula": null,
                "doc_carta_renuncia": null,
                "envio_correo": null,
                "fecha_examen_cei": null,
                "doc_examen_cei": null,
                "aprobacion_examen_cei": null,
                "doc_aprobacion_cei": null,
                "fecha_curso_cedula": null,
                "comprobante_cedula": null,
                "fecha_entrega_cedula": null,
                "fecha_alta_cnsf": null,
                "fecha_ini_cedula": null,
                "fecha_fin_cedula": null,
                "clave_cua": null,
                "clave_cua_provisional": null,
                "fecha_conexion_provicional": null,
                "fecha_conexion_definitiva": null,
                "antiguedad_agente": null,
                "alta_sat": null,
                "envio_gnp_multa": null,
                "curso_ideas": null,
                "generacion": null,
                "agente_consolidado_id": null,
                "correo_aps": null,
                "fecha_fin_tarjetas": null,
                "fecha_alta_boletin": null,
                "fecha_plenarias_aps": null,
                "fecha_alta_chats": null,
                "alta_chats_noveles": null,
                "alta_agenda_aps": null,
                "alta_prospecto_ideas": null,
                "curso_inmersion_aps": null,
                "baja_curso_inmersion_aps": null,
                "doc_capacitacion": null,
                "doc_convenio": null,
                "baja_conexion": null
            },
            conexion: {},
            entrevistador: '',
            ban_baja_conexion: false,
            ban_ventaCarrera: false
        }
    }


    construyeSelect(url, selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                /*if (selector == "selectAgentesConsolidados") {
                    filas.push({ "id": 1, "descripcion": "Agente reclutado directamente por el GDD y desarrollado en su equipo" })
                    filas.push({ "id": 2, "descripcion": "Agente entregado al GDD desde su inicio y desarrollado en su equipo" })
                    filas.push({ "id": 3, "descripcion": "Agente heredado por el GDD sin haber pasado sus años de novel en el equipo" })
                }*/

                console.log("respuesta de las ciudades ", json)
                for (var i = 0; i < filas.length; i++) {

                    let fila = filas[i]
                    if (selector == "selectMotivosBaja") {

                        if (this.state.prospecto.baja_curso_inmersion_aps == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }

                    } else if (selector == "selectAgentesConsolidados") {
                        if (this.state.prospecto.agente_consolidado_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }
                    }
                }
                let salida = []
                let banBloquear = false;
                if (selector == "selectAgentesConsolidados" && this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER") {
                    banBloquear = true; 
                }
                if (selector == "selectMotivosBaja" && this.props.tipoUsuario != "ASRSIS"  && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "ASESJRCAP" && this.props.tipoUsuario != "GTECAP" && this.props.tipoUsuario != "SUPER") {
                    banBloquear = true;
                }
                salida.push(<select disabled={banBloquear} onChange={this.onChange} name={selector} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ [selector]: salida })
            });
    }

    onChange = e => {
        let pros = this.state.prospecto
        if (e.target.name == "envio_correo" || e.target.name == "aprobacion_examen_cei" || e.target.name == "comprobante_cedula" || e.target.name == "alta_sat"
            || e.target.name == "envio_gnp_multa" || e.target.name == "curso_ideas" || e.target.name == "alta_chats_noveles" || e.target.name == "alta_agenda_aps" || e.target.name == "alta_prospecto_ideas"
            || e.target.name == "curso_inmersion_aps"
        ) {
            if (e.target.checked == true) {
                pros["" + e.target.name + ""] = 1
                if(e.target.name == "curso_inmersion_aps"){
                    let jsonNotificacion = {
                        "mail_id": 10,
                        "prospecto_id": this.props.idProspecto
                    }
                    const requestOptions = {
                        method: "POST",
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify(jsonNotificacion)
                    };
    
                    fetch(datos.urlServicePy+"recluta/api_notificador/0", requestOptions)
                        .then(response => response.json())
                        .then(data => { })
                }
                if(e.target.name == "envio_correo"){
                    let jsonNotificacion = {
                        "mail_id": 19,
                        "prospecto_id": this.props.idProspecto
                    }
                    const requestOptions = {
                        method: "POST",
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify(jsonNotificacion)
                    };

                    fetch(datos.urlServicePy+"recluta/api_notificador/0", requestOptions)
                        .then(response => response.json())
                        .then(data => {})
                }

            } else {
                pros["" + e.target.name + ""] = 0

            }
        } else if (e.target.name == "selectMotivosBaja") {
            pros.baja_curso_inmersion_aps = e.target.value
        } else if (e.target.name == "selectAgentesConsolidados") {
            pros.agente_consolidado_id = parseInt(e.target.value)
        } else if (e.target.name == "baja_conexion") {
            if (e.target.checked == true) {
                this.setState({ ban_baja_conexion: true })
                pros["" + e.target.name + ""] = 1
                
                    let jsonNotificacion = {
                        "mail_id": 11,
                        "prospecto_id": this.props.idProspecto
                    }
                    const requestOptions = {
                        method: "POST",
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify(jsonNotificacion)
                    };
                   // this.props.botonCancelar() 
                    fetch(datos.urlServicePy+"recluta/api_notificador/0", requestOptions)
                        .then(response => response.json())
                        .then(data => { })
                
            } else {
                pros["" + e.target.name + ""] = 0
                pros.baja_curso_inmersion_aps = 0
                this.setState({ ["selectMotivosBaja"]: null })
                this.construyeSelect(datos.urlServicePy+"parametros/api_cat_causas_baja/0", "selectMotivosBaja")
                this.setState({ ban_baja_conexion: false })
            }
        } else if (e.target.name == 'selectMotivosReserva') {
            this.setState({ idMotivoReserva: parseInt(e.target.value) })
        } else if (e.target.name == 'selectMotivosBaja') {
            this.setState({ idMotivoBaja: parseInt(e.target.value) })

        }else if(e.target.name == "antiguedad_agente"){
            if(e.target.value == ""){
                pros["" + e.target.name + ""] = null
            }else{
                pros["" + e.target.name + ""] = parseInt(e.target.value) 
            }
        }else {
            pros["" + e.target.name + ""] = e.target.value
        }

        console.log(pros, e.target.name, e.target.value)
        this.setState({ prospecto: pros })

    }


    onChangeFileIdentificacion(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file de puesto", file);
        this.setState({ archivoIdentificacion: file, colorSubirArchivoIdentificacion: "#78CB5A", colorTextoSubirArchivoIdentificacion: "#FFFFFF", colorBotonSubirArchivoIdentificacion: "#E5F6E0", nombreArchivoIdentificacion: file.name })

    }
    onClickBotonArchivoIdentificacion() {
        console.log("entrnaod a la imagen")
        this.upload.click()
    }


    onChangeFileActaNacimiento(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file de puesto", file);
        this.setState({ archivoActaNacimiento: file, colorSubirArchivoActaNacimiento: "#78CB5A", colorTextoSubirArchivoActaNacimiento: "#FFFFFF", colorBotonSubirArchivoActaNacimiento: "#E5F6E0", nombreArchivoActaNacimiento: file.name })

    }
    onClickBotonArchivoActaNacimiento() {
        console.log("entrnaod a la imagen")
        this.acta.click()
    }

    onChangeFileCertificadoEstudios(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file de puesto", file);
        this.setState({ archivoCertificadoEstudios: file, colorSubirArchivoCertificadoEstudios: "#78CB5A", colorTextoSubirArchivoCertificadoEstudios: "#FFFFFF", colorBotonSubirArchivoCertificadoEstudios: "#E5F6E0", nombreArchivoCertificadoEstudios: file.name })

    }
    onClickBotonArchivoCertificadoEstudios() {
        console.log("entrnaod a la imagen")
        this.certificado.click()
    }


    onChangeFileCurp(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file de puesto", file);
        this.setState({ archivoCurp: file, colorSubirArchivoCurp: "#78CB5A", colorTextoSubirArchivoCurp: "#FFFFFF", colorBotonSubirArchivoCurp: "#E5F6E0", nombreArchivoCurp: file.name })

    }
    onClickBotonArchivoCurp() {
        console.log("entrnaod a la imagen")
        this.curp.click()
    }


    onChangeFileRFC(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file de puesto", file);
        this.setState({ archivoRFC: file, colorSubirArchivoRFC: "#78CB5A", colorTextoSubirArchivoRFC: "#FFFFFF", colorBotonSubirArchivoRFC: "#E5F6E0", nombreArchivoRFC: file.name })

    }
    onClickBotonArchivoRFC() {
        console.log("entrnaod a la imagen")
        this.rfc.click()
    }


    onChangeFileCedula(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file de puesto", file);
        this.setState({ archivoCedula: file, colorSubirArchivoCedula: "#78CB5A", colorTextoSubirArchivoCedula: "#FFFFFF", colorBotonSubirArchivoCedula: "#E5F6E0", nombreArchivoCedula: file.name })

    }
    onClickBotonArchivoCedula() {
        console.log("entrnaod a la imagen")
        this.cedula.click()
    }

    onChangeFileRenuncia(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file de puesto", file);
        this.setState({ archivoRenuncia: file, colorSubirArchivoRenuncia: "#78CB5A", colorTextoSubirArchivoRenuncia: "#FFFFFF", colorBotonSubirArchivoRenuncia: "#E5F6E0", nombreArchivoRenuncia: file.name })

    }
    onClickBotonArchivoRenuncia() {
        console.log("entrnaod a la imagen")
        this.renuncia.click()
    }


    onChangeFileComprobantePagoCEI(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file de puesto", file);
        this.setState({ archivoComprobantePagoCEI: file, colorSubirArchivoComprobantePagoCEI: "#78CB5A", colorTextoSubirArchivoComprobantePagoCEI: "#FFFFFF", colorBotonSubirArchivoComprobantePagoCEI: "#E5F6E0", nombreArchivoComprobantePagoCEI: file.name })

    }
    onClickBotonArchivoComprobantePagoCEI() {
        console.log("entrnaod a la imagen")
        this.pagocei.click()
    }


    onChangeFileAprobacionExamenCEI(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file de puesto", file);
        this.setState({ archivoAprobacionExamenCEI: file, colorSubirArchivoAprobacionExamenCEI: "#78CB5A", colorTextoSubirArchivoAprobacionExamenCEI: "#FFFFFF", colorBotonSubirArchivoAprobacionExamenCEI: "#E5F6E0", nombreArchivoAprobacionExamenCEI: file.name })

    }
    onClickBotonArchivoAprobacionExamenCEI() {
        console.log("entrnaod a la imagen")
        this.aprobacioncei.click()
    }

    onChangeFileConvenioFirmado(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file de puesto", file);
        this.setState({ archivoConvenioFirmado: file, colorSubirArchivoConvenioFirmado: "#78CB5A", colorTextoSubirArchivoConvenioFirmado: "#FFFFFF", colorBotonSubirArchivoConvenioFirmado: "#E5F6E0", nombreArchivoConvenioFirmado: file.name })

    }
    onClickBotonArchivoConvenioFirmado() {
        console.log("entrnaod a la imagen")
        this.convenioFirmado.click()
    }


    onChangeFileCapacitacion(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file de puesto", file);
        this.setState({ archivoCapacitacion: file, colorSubirArchivoCapacitacion: "#78CB5A", colorTextoSubirArchivoCapacitacion: "#FFFFFF", colorBotonSubirArchivoCapacitacion: "#E5F6E0", nombreArchivoCapacitacion: file.name })

    }
    onClickBotonArchivoCapacitacion() {
        console.log("entrnaod a la imagen")
        this.capacitacion.click()
    }

    mostrarReporteCapacitacion() {
        this.guardarFormulario(false)

    }

    regresaryGuardar() {
       
        /* if(this.state.prospecto.envio_correo && (this.state.urlCapacitacion == "" || this.state.urlCapacitacion == null)){
            let jsonNotificacion = {
                "mail_id": 20,
                "prospecto_id": this.props.idProspecto
            }
            const requestOptions = {
                method: "POST",
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(jsonNotificacion)
            };

            fetch(datos.urlServicePy+"recluta/api_notificador/0", requestOptions)
                .then(response => response.json())
                .then(data => { })
        }*/
      /*  if(this.state.prospecto.baja_conexion){
            let jsonNotificacion = {
                "mail_id": 11,
                "prospecto_id": this.props.idProspecto
            }
            const requestOptions = {
                method: "POST",
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(jsonNotificacion)
            };
            this.props.botonCancelar() 
            fetch(datos.urlServicePy+"recluta/api_notificador/0", requestOptions)
                .then(response => response.json())
                .then(data => { })
        }*/
        this.guardarFormulario(false)
        this.props.botonCancelar()
    }

    guardarCapacitacion() {
        this.guardarFormulario(false)
    }

    mostrarConexionCapacitacion() {
        fetch(datos.urlServicePy+"recluta/api_recluta_conexion17/" + this.props.idProspecto)
            .then(response => response.json())
            .then(existeProspecto => {
                this.setState({ mostrarReporteCapacitacion: false, mostrarConvenioArranque: true })

            })


        fetch(datos.urlServicePy+"recluta/api_recluta_conexion17/" + this.props.idProspecto)
            .then(response => response.json())
            .then(existeProspecto => {
                if (existeProspecto.length > 0) {
                    this.setState({ prospecto: existeProspecto[0] })

                    let prospecto = existeProspecto[0]

                    if (prospecto.doc_identificacion != null && prospecto.doc_identificacion.length > 0) {
                        this.setState({
                            urlIdentificacion: prospecto.doc_identificacion, muestraDocIdentificacion: true, colorSubirArchivoIdentificacion: "#78CB5A",
                            colorTextoSubirArchivoIdentificacion: "#FFFFFF", colorBotonSubirArchivoIdentificacion: "#E5F6E0", nombreArchivoIdentificacion: 'modificar'
                        })
                    }
                    if (prospecto.doc_acta_nacimiento != null && prospecto.doc_acta_nacimiento.length > 0) {
                        this.setState({
                            urlActaNacimiento: prospecto.doc_acta_nacimiento, muestraDocActaNacimiento: true, colorSubirArchivoActaNacimiento: "#78CB5A",
                            colorTextoSubirArchivoActaNacimiento: "#FFFFFF", colorBotonSubirArchivoActaNacimiento: "#E5F6E0", nombreArchivoActaNacimiento: 'modificar'
                        })
                    }
                    if (prospecto.doc_cerfificado != null && prospecto.doc_cerfificado.length > 0) {
                        this.setState({
                            urlCertificadoEstudios: prospecto.doc_cerfificado, muestraDocCertificadoEstudios: true, colorSubirArchivoCertificadoEstudios: "#78CB5A",
                            colorTextoSubirArchivoCertificadoEstudios: "#FFFFFF", colorBotonSubirArchivoCertificadoEstudios: "#E5F6E0", nombreArchivoCertificadoEstudios: 'modificar'
                        })
                    }
                    if (prospecto.doc_curp != null && prospecto.doc_curp.length > 0) {
                        this.setState({
                            urlCurp: prospecto.doc_curp, muestraDocCurp: true, colorSubirArchivoCurp: "#78CB5A",
                            colorTextoSubirArchivoCurp: "#FFFFFF", colorBotonSubirArchivoCurp: "#E5F6E0", nombreArchivoCurp: 'modificar'
                        })
                    }
                    if (prospecto.doc_rfc != null && prospecto.doc_rfc.length > 0) {
                        this.setState({
                            urlRFC: prospecto.doc_rfc, muestraDocRFC: true, colorSubirArchivoRFC: "#78CB5A",
                            colorTextoSubirArchivoRFC: "#FFFFFF", colorBotonSubirArchivoRFC: "#E5F6E0", nombreArchivoRFC: 'modificar'
                        })
                    }
                    if (prospecto.doc_cedula != null && prospecto.doc_cedula.length > 0) {
                        this.setState({
                            urlCedula: prospecto.doc_cedula, muestraDocCedula: true, colorSubirArchivoCedula: "#78CB5A",
                            colorTextoSubirArchivoCedula: "#FFFFFF", colorBotonSubirArchivoCedula: "#E5F6E0", nombreArchivoCedula: 'modificar'
                        })
                    }
                    if (prospecto.doc_carta_renuncia != null && prospecto.doc_carta_renuncia.length > 0) {
                        this.setState({
                            urlRenuncia: prospecto.doc_carta_renuncia, muestraDocRenuncia: true, colorSubirArchivoRenuncia: "#78CB5A",
                            colorTextoSubirArchivoRenuncia: "#FFFFFF", colorBotonSubirArchivoRenuncia: "#E5F6E0", nombreArchivoRenuncia: 'modificar'
                        })
                    }
                    if (prospecto.doc_examen_cei != null && prospecto.doc_examen_cei.length > 0) {
                        this.setState({
                            urlComprobantePagoCEI: prospecto.doc_examen_cei, muestraDocComprobantePagoCEI: true, colorSubirArchivoComprobantePagoCEI: "#78CB5A",
                            colorTextoSubirArchivoComprobantePagoCEI: "#FFFFFF", colorBotonSubirArchivoComprobantePagoCEI: "#E5F6E0", nombreArchivoComprobantePagoCEI: 'modificar'
                        })
                    }

                    if (prospecto.doc_aprobacion_cei != null && prospecto.doc_aprobacion_cei.length > 0) {
                        this.setState({
                            urlAprobacionExamenCEI: prospecto.doc_aprobacion_cei, muestraDocAprobacionExamenCEI: true, colorSubirArchivoAprobacionExamenCEI: "#78CB5A",
                            colorTextoSubirArchivoAprobacionExamenCEI: "#FFFFFF", colorBotonSubirArchivoAprobacionExamenCEI: "#E5F6E0", nombreArchivoAprobacionExamenCEI: 'modificar'
                        })
                    }

                    if (prospecto.doc_convenio != null && prospecto.doc_convenio.length > 0) {
                        this.setState({
                            urlConvenioFirmado: prospecto.doc_convenio, muestraDocConvenioFirmado: true, colorSubirArchivoConvenioFirmado: "#78CB5A",
                            colorTextoSubirArchivoConvenioFirmado: "#FFFFFF", colorBotonSubirArchivoConvenioFirmado: "#E5F6E0", nombreArchivoConvenioFirmado: 'modificar'
                        })
                    }
                    if (prospecto.doc_capacitacion != null && prospecto.doc_capacitacion.length > 0) {
                        this.setState({
                            urlCapacitacion: prospecto.doc_capacitacion, muestraDocCapacitacion: true, colorSubirArchivoCapacitacion: "#78CB5A",
                            colorTextoSubirArchivoCapacitacion: "#FFFFFF", colorBotonSubirArchivoCapacitacion: "#E5F6E0", nombreArchivoCapacitacion: 'modificar'
                        })
                    }

                    console.log("el props ", prospecto)
                    if (prospecto.doc_foto != null && prospecto.doc_foto.length > 0) {
                        console.log("la puta foto")
                        this.setState({
                            imagenEnCambio: prospecto.doc_foto,
                            urlIdentificacion: prospecto.doc_foto
                        })
                    }



                }
                this.construyeSelect(datos.urlServicePy+"parametros/api_cat_causas_baja/0", "selectMotivosBaja")
                this.construyeSelect(datos.urlServicePy+"parametros/api_cat_manejo_agentes_consolidado/0", "selectAgentesConsolidados")
                this.setState({ mostrarReporteCapacitacion: false, mostrarConvenioArranque: false })
            })

        //this.setState({ mostrarReporteCapacitacion: false, mostrarConvenioArranque: false })



    }

    mostrarConvenioArranque() {
        this.setState({ mostrarReporteCapacitacion: false, mostrarConvenioArranque: true })
    }

    cambiaEstatusDelProspecto() {
        let jsonActualiza = {
            "estatus_id": 17
        }
        const requestOptions = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(jsonActualiza)
        };
        fetch(datos.urlServicePy+"parametros_upd/api_recluta_prospectos/" + this.props.idProspecto, requestOptions)
            .then(response => response.json())
            .then(data => {
                if(this.state.prospecto.clave_cua != null || this.state.prospecto.clave_cua != "" || this.state.prospecto.clave_cua_provisional != null || this.state.prospecto.clave_cua_provisional != ""){
                    let jsonNotificacion = {
                        "mail_id": 14,
                        "prospecto_id": this.props.idProspecto
                    }
                    const requestOptions = {
                        method: "POST",
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify(jsonNotificacion)
                    };

                    fetch(datos.urlServicePy+"recluta/api_notificador/0", requestOptions)
                        .then(response => response.json())
                        .then(data => { 
                            let jsonNotificacion = {
                                "mail_id": 15,
                                "prospecto_id": this.props.idProspecto
                            }
                            const requestOptions = {
                                method: "POST",
                                headers: { 'Content-Type': 'application/json' },
                                body: JSON.stringify(jsonNotificacion)
                            };
        
                            fetch(datos.urlServicePy+"recluta/api_notificador/0", requestOptions)
                                .then(response => response.json())
                                .then(data => { 
                                    let jsonNotificacion = {
                                        "mail_id": 16,
                                        "prospecto_id": this.props.idProspecto
                                    }
                                    const requestOptions = {
                                        method: "POST",
                                        headers: { 'Content-Type': 'application/json' },
                                        body: JSON.stringify(jsonNotificacion)
                                    };
                
                                    fetch(datos.urlServicePy+"recluta/api_notificador/0", requestOptions)
                                        .then(response => response.json())
                                        .then(data => { })
                                })
                        })
                }


                if((this.state.muestraDocComprobantePagoCEI == true || this.state.nombreArchivoComprobantePagoCEI != "") && (this.state.muestraDocAprobacionExamenCEI == true || this.state.nombreArchivoAprobacionExamenCEI != "")){
                    let jsonNotificacion = {
                        "mail_id": 21,
                        "prospecto_id": this.props.idProspecto
                    }
                    const requestOptions = {
                        method: "POST",
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify(jsonNotificacion)
                    };

                    fetch(datos.urlServicePy+"recluta/api_notificador/0", requestOptions)
                        .then(response => response.json())
                        .then(data => {})
                }

                this.props.botonCancelar()
            })


    }

    guardarArchivos(existenArchivos, fileset,insertToFile){
        console.log("filse :::"+fileset)
        let lenFiles = fileset.length;
        const config = {
            headers: { 'content-type': 'multipart/form-data' }
        }
        if(insertToFile >= 1){
            if(!existenArchivos){
                if(lenFiles >= 1){
                    let formData = new FormData();
                    formData.append('id', this.props.idProspecto);
                    formData.append('prospecto_id', this.props.idProspecto);
                    formData.append(fileset[0].file_name, fileset[0].file);
                    axios.post(datos.urlServicePy+"api/doc_conexion/", formData, config)
                    .then(res1 => {
                    if(lenFiles >= 2){
                        let formData = new FormData();
                        formData.append('id', this.props.idProspecto);
                        formData.append('prospecto_id', this.props.idProspecto);
                        formData.append(fileset[1].file_name, fileset[1].file);
                        axios.put(datos.urlServicePy+"api/doc_conexion/" + this.props.idProspecto + "/", formData, config)
                        .then(res2 => {
                            if(lenFiles >= 3){
                                let formData = new FormData();
                                formData.append('id', this.props.idProspecto);
                                formData.append('prospecto_id', this.props.idProspecto);
                                formData.append(fileset[2].file_name, fileset[2].file);
                                axios.put(datos.urlServicePy+"api/doc_conexion/" + this.props.idProspecto + "/", formData, config)
                                .then(res3 => {
                                    if(lenFiles >= 4){
                                        let formData = new FormData();
                                        formData.append('id', this.props.idProspecto);
                                        formData.append('prospecto_id', this.props.idProspecto);
                                        formData.append(fileset[3].file_name, fileset[3].file);
                                        axios.put(datos.urlServicePy+"api/doc_conexion/" + this.props.idProspecto + "/", formData, config)
                                        .then(res4 => {
                                            if(lenFiles >= 5){
                                                let formData = new FormData();
                                                formData.append('id', this.props.idProspecto);
                                                formData.append('prospecto_id', this.props.idProspecto);
                                                formData.append(fileset[4].file_name, fileset[4].file);
                                                axios.put(datos.urlServicePy+"api/doc_conexion/" + this.props.idProspecto + "/", formData, config)
                                                .then(res5 => {
                                                    if(lenFiles >= 6){
                                                        let formData = new FormData();
                                                        formData.append('id', this.props.idProspecto);
                                                        formData.append('prospecto_id', this.props.idProspecto);
                                                        formData.append(fileset[5].file_name, fileset[5].file);
                                                        axios.put(datos.urlServicePy+"api/doc_conexion/" + this.props.idProspecto + "/", formData, config)
                                                        .then(res6 => {
                                                            if(lenFiles >= 7){
                                                                let formData = new FormData();
                                                                formData.append('id', this.props.idProspecto);
                                                                formData.append('prospecto_id', this.props.idProspecto);
                                                                formData.append(fileset[6].file_name, fileset[6].file);
                                                                axios.put(datos.urlServicePy+"api/doc_conexion/" + this.props.idProspecto + "/", formData, config)
                                                                .then(res7 => {
                                                                    if(lenFiles >= 8){
                                                                        let formData = new FormData();
                                                                        formData.append('id', this.props.idProspecto);
                                                                        formData.append('prospecto_id', this.props.idProspecto);
                                                                        formData.append(fileset[7].file_name, fileset[7].file);
                                                                        axios.put(datos.urlServicePy+"api/doc_conexion/" + this.props.idProspecto + "/", formData, config)
                                                                        .then(res8 => {
                                                                            if(lenFiles >= 9){
                                                                                let formData = new FormData();
                                                                                formData.append('id', this.props.idProspecto);
                                                                                formData.append('prospecto_id', this.props.idProspecto);
                                                                                formData.append(fileset[8].file_name, fileset[8].file);
                                                                                axios.put(datos.urlServicePy+"api/doc_conexion/" + this.props.idProspecto + "/", formData, config)
                                                                                .then(res9 => {
                                                                                    if(lenFiles >= 10){
                                                                                        let formData = new FormData();
                                                                                        formData.append('id', this.props.idProspecto);
                                                                                        formData.append('prospecto_id', this.props.idProspecto);
                                                                                        formData.append(fileset[9].file_name, fileset[9].file);
                                                                                        axios.put(datos.urlServicePy+"api/doc_conexion/" + this.props.idProspecto + "/", formData, config)
                                                                                        .then(res10 => {
                                                                                        })
                                                                                    }
                                                                                })
                                                                            }
                                                                        })
                                                                    }
                                                                })
                                                            }
                                                        })
                                                    }
                                                })
                                            }
                                        })
                                    }
                                }) 
                            }
                        })
                    }
                    })
                }

            } else{
                if(lenFiles >= 1){
                    let formData = new FormData();
                    formData.append('id', this.props.idProspecto);
                    formData.append('prospecto_id', this.props.idProspecto);
                    formData.append(fileset[0].file_name, fileset[0].file);
                    axios.put(datos.urlServicePy+"api/doc_conexion/" + this.props.idProspecto + "/", formData, config)
                    .then(res1 => {
                    if(lenFiles >= 2){
                        let formData = new FormData();
                        formData.append('id', this.props.idProspecto);
                        formData.append('prospecto_id', this.props.idProspecto);
                        formData.append(fileset[1].file_name, fileset[1].file);
                        axios.put(datos.urlServicePy+"api/doc_conexion/" + this.props.idProspecto + "/", formData, config)
                        .then(res2 => {
                            if(lenFiles >= 3){
                                let formData = new FormData();
                                formData.append('id', this.props.idProspecto);
                                formData.append('prospecto_id', this.props.idProspecto);
                                formData.append(fileset[2].file_name, fileset[2].file);
                                axios.put(datos.urlServicePy+"api/doc_conexion/" + this.props.idProspecto + "/", formData, config)
                                .then(res3 => {
                                    if(lenFiles >= 4){
                                        let formData = new FormData();
                                        formData.append('id', this.props.idProspecto);
                                        formData.append('prospecto_id', this.props.idProspecto);
                                        formData.append(fileset[3].file_name, fileset[3].file);
                                        axios.put(datos.urlServicePy+"api/doc_conexion/" + this.props.idProspecto + "/", formData, config)
                                        .then(res4 => {
                                            if(lenFiles >= 5){
                                                let formData = new FormData();
                                                formData.append('id', this.props.idProspecto);
                                                formData.append('prospecto_id', this.props.idProspecto);
                                                formData.append(fileset[4].file_name, fileset[4].file);
                                                axios.put(datos.urlServicePy+"api/doc_conexion/" + this.props.idProspecto + "/", formData, config)
                                                .then(res5 => {
                                                    if(lenFiles >= 6){
                                                        let formData = new FormData();
                                                        formData.append('id', this.props.idProspecto);
                                                        formData.append('prospecto_id', this.props.idProspecto);
                                                        formData.append(fileset[5].file_name, fileset[5].file);
                                                        axios.put(datos.urlServicePy+"api/doc_conexion/" + this.props.idProspecto + "/", formData, config)
                                                        .then(res6 => {
                                                            if(lenFiles >= 7){
                                                                let formData = new FormData();
                                                                formData.append('id', this.props.idProspecto);
                                                                formData.append('prospecto_id', this.props.idProspecto);
                                                                formData.append(fileset[6].file_name, fileset[6].file);
                                                                axios.put(datos.urlServicePy+"api/doc_conexion/" + this.props.idProspecto + "/", formData, config)
                                                                .then(res7 => {
                                                                    if(lenFiles >= 8){
                                                                        let formData = new FormData();
                                                                        formData.append('id', this.props.idProspecto);
                                                                        formData.append('prospecto_id', this.props.idProspecto);
                                                                        formData.append(fileset[7].file_name, fileset[7].file);
                                                                        axios.put(datos.urlServicePy+"api/doc_conexion/" + this.props.idProspecto + "/", formData, config)
                                                                        .then(res8 => {
                                                                            if(lenFiles >= 9){
                                                                                let formData = new FormData();
                                                                                formData.append('id', this.props.idProspecto);
                                                                                formData.append('prospecto_id', this.props.idProspecto);
                                                                                formData.append(fileset[8].file_name, fileset[8].file);
                                                                                axios.put(datos.urlServicePy+"api/doc_conexion/" + this.props.idProspecto + "/", formData, config)
                                                                                .then(res9 => {
                                                                                    if(lenFiles >= 10){
                                                                                        let formData = new FormData();
                                                                                        formData.append('id', this.props.idProspecto);
                                                                                        formData.append('prospecto_id', this.props.idProspecto);
                                                                                        formData.append(fileset[9].file_name, fileset[9].file);
                                                                                        axios.put(datos.urlServicePy+"api/doc_conexion/" + this.props.idProspecto + "/", formData, config)
                                                                                        .then(res10 => {
                                                                                        })
                                                                                    }
                                                                                })
                                                                            }
                                                                        })
                                                                    }
                                                                })
                                                            }
                                                        })
                                                    }
                                                })
                                            }
                                        })
                                    }
                                }) 
                            }
                        })
                    }
                    })
                }
            }
        }
    }

    guardarFormulario(banFormulario) {
        console.log("json ", this.state.prospecto)
        let json = this.state.prospecto
        json.id = this.props.idProspecto
        json.prospecto_id = this.props.idProspecto
        json.doc_foto = null
        json.doc_identificacion = null
        json.doc_acta_nacimiento = null
        json.doc_curp = null
        json.doc_rfc = null
        json.doc_cedula = null
        json.doc_carta_renuncia = null
        json.doc_examen_cei = null
        json.doc_aprobacion_cei = null
        //json.doc_capacitacion = null
        //json.doc_convenio = null
        json.doc_cerfificado = null


        /*fetch(datos.urlServicePy+"api/doc_conexion/" + this.props.idProspecto + "/?format=json")
        .then(response => response.json())
        .then(existeFile => {
            if(existeFile.detail != ""){
                banExisteFile = true
            }
        })*/

        fetch(datos.urlServicePy+"recluta/api_recluta_conexion17/" + this.props.idProspecto)
            .then(response => response.json())
            .then(existeProspecto => {
                let url = datos.urlServicePy+"recluta/api_recluta_conexion17/"
                if (existeProspecto.length > 0) {
                    url = url + this.props.idProspecto
                } else {
                    url = url + "0"
                }
                const requestOptions = {
                    method: "POST",
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(json)
                };                

                fetch(url, requestOptions)
                    .then(response => response.json())
                    .then(data => {
                        let banExisteFile = false
                        let listFiles = []
                        let insertToFile = 0

                        if (this.state.existefoto  || this.state.urlIdentificacion.length > 0 || this.state.urlActaNacimiento.length > 0 || 
                            this.state.urlCertificadoEstudios.length > 0 || this.state.urlCurp.length > 0 ||
                            this.state.urlRFC.length > 0 || this.state.urlCedula.length > 0 || this.state.urlRenuncia.length > 0 || 
                            this.state.urlComprobantePagoCEI.length > 0 || this.state.urlAprobacionExamenCEI.length > 0) {
                                banExisteFile = true
                        }

                        if(this.state.fotografiaFile != undefined){
                            listFiles.push({'file_name':'doc_foto','file':this.state.fotografiaFile})
                            insertToFile = insertToFile + 1
                        }
                        if(this.state.archivoIdentificacion != undefined){
                            listFiles.push({'file_name':'doc_identificacion','file':this.state.archivoIdentificacion})
                            insertToFile = insertToFile + 1
                        }
                        if(this.state.archivoActaNacimiento != undefined){
                            listFiles.push({'file_name':'doc_acta_nacimiento','file':this.state.archivoActaNacimiento})
                            insertToFile = insertToFile + 1
                        }
                        if(this.state.archivoCertificadoEstudios != undefined){
                            listFiles.push({'file_name':'doc_cerfificado','file':this.state.archivoCertificadoEstudios})
                            insertToFile = insertToFile + 1
                        }
                        if(this.state.archivoCurp != undefined){
                            listFiles.push({'file_name':'doc_curp','file':this.state.archivoCurp})
                            insertToFile = insertToFile + 1
                        }
                        if(this.state.archivoRFC != undefined){
                            listFiles.push({'file_name':'doc_rfc','file':this.state.archivoRFC})
                            insertToFile = insertToFile + 1
                        }
                        if(this.state.archivoCedula != undefined){
                            listFiles.push({'file_name':'doc_cedula','file':this.state.archivoCedula})
                            insertToFile = insertToFile + 1
                        }
                        if(this.state.archivoRenuncia != undefined){
                            listFiles.push({'file_name':'doc_carta_renuncia','file':this.state.archivoRenuncia})
                            insertToFile = insertToFile + 1
                        }
                        if(this.state.archivoComprobantePagoCEI != undefined){
                            listFiles.push({'file_name':'doc_examen_cei','file':this.state.archivoComprobantePagoCEI})
                            insertToFile = insertToFile + 1
                        }
                        if(this.state.archivoAprobacionExamenCEI != undefined){
                            listFiles.push({'file_name':'doc_aprobacion_cei','file':this.state.archivoAprobacionExamenCEI})
                            insertToFile = insertToFile + 1
                        }

                        if(!banExisteFile){
                            this.guardarArchivos(false,listFiles,insertToFile)
                        }else{
                            this.guardarArchivos(true,listFiles,insertToFile)
                        }
                        
                        this.setState({ mostrarReporteCapacitacion: true, mostrarConvenioArranque: false })

                        if (banFormulario) {
                            if (this.props.tipoUsuario == "SASSR" || this.props.tipoUsuario == "SASJR") {
                                let jsonNotificacion = {
                                    "mail_id": 13,
                                    "prospecto_id": this.props.idProspecto
                                }
                                const requestOptions = {
                                    method: "POST",
                                    headers: { 'Content-Type': 'application/json' },
                                    body: JSON.stringify(jsonNotificacion)
                                };
        
                                fetch(datos.urlServicePy+"recluta/api_notificador/0", requestOptions)
                                    .then(response => response.json())
                                    .then(data => {})
                                this.cambiaEstatusDelProspecto()
                            } else {
                                this.props.botonCancelar()
                               /* if(this.state.prospecto.curso_inmersion_aps && (this.state.prospecto.clave_cua != null || this.state.prospecto.clave_cua != "" || this.state.prospecto.clave_cua_provisional != null || this.state.prospecto.clave_cua_provisional != "")){
                                    let jsonNotificacion = {
                                        "mail_id": 13,
                                        "prospecto_id": this.props.idProspecto
                                    }
                                    const requestOptions = {
                                        method: "POST",
                                        headers: { 'Content-Type': 'application/json' },
                                        body: JSON.stringify(jsonNotificacion)
                                    };
            
                                    fetch(datos.urlServicePy+"recluta/api_notificador/0", requestOptions)
                                        .then(response => response.json())
                                        .then(data => {})
                                }*/
                              //  if(this.state.prospecto.curso_inmersion_aps && this.state.prospecto.comprobante_cedula){
                                   /* let jsonNotificacion = {
                                        "mail_id": 19,
                                        "prospecto_id": this.props.idProspecto
                                    }
                                    const requestOptions = {
                                        method: "POST",
                                        headers: { 'Content-Type': 'application/json' },
                                        body: JSON.stringify(jsonNotificacion)
                                    };
            
                                    fetch(datos.urlServicePy+"recluta/api_notificador/0", requestOptions)
                                        .then(response => response.json())
                                        .then(data => {}) */
                               // }
                                //
                            }
                        }
                       /* if(this.state.prospecto.baja_conexion){
                            let jsonNotificacion = {
                                "mail_id": 11,
                                "prospecto_id": this.props.idProspecto
                            }
                            const requestOptions = {
                                method: "POST",
                                headers: { 'Content-Type': 'application/json' },
                                body: JSON.stringify(jsonNotificacion)
                            };
                            this.props.botonCancelar() 
                            fetch(datos.urlServicePy+"recluta/api_notificador/0", requestOptions)
                                .then(response => response.json())
                                .then(data => { })
                        }*/
                    })
            })
    }

    UNSAFE_componentWillMount() {
        fetch(datos.urlServicePy+"recluta/api_recluta_conexion17/" + this.props.idProspecto)
            .then(response => response.json())
            .then(existeProspecto => {
                if (existeProspecto.length > 0) {
                    this.setState({ prospecto: existeProspecto[0] })

                    let prospecto = existeProspecto[0]

                    if(prospecto.baja_conexion){
                        this.setState({ ban_baja_conexion: true })
                    }

                    if (prospecto.doc_identificacion != null && prospecto.doc_identificacion.length > 0) {
                        this.setState({
                            urlIdentificacion: prospecto.doc_identificacion, muestraDocIdentificacion: true, colorSubirArchivoIdentificacion: "#78CB5A",
                            colorTextoSubirArchivoIdentificacion: "#FFFFFF", colorBotonSubirArchivoIdentificacion: "#E5F6E0", nombreArchivoIdentificacion: 'modificar'
                        })
                    }
                    if (prospecto.doc_acta_nacimiento != null && prospecto.doc_acta_nacimiento.length > 0) {
                        this.setState({
                            urlActaNacimiento: prospecto.doc_acta_nacimiento, muestraDocActaNacimiento: true, colorSubirArchivoActaNacimiento: "#78CB5A",
                            colorTextoSubirArchivoActaNacimiento: "#FFFFFF", colorBotonSubirArchivoActaNacimiento: "#E5F6E0", nombreArchivoActaNacimiento: 'modificar'
                        })
                    }
                    if (prospecto.doc_cerfificado != null && prospecto.doc_cerfificado.length > 0) {
                        this.setState({
                            urlCertificadoEstudios: prospecto.doc_cerfificado, muestraDocCertificadoEstudios: true, colorSubirArchivoCertificadoEstudios: "#78CB5A",
                            colorTextoSubirArchivoCertificadoEstudios: "#FFFFFF", colorBotonSubirArchivoCertificadoEstudios: "#E5F6E0", nombreArchivoCertificadoEstudios: 'modificar'
                        })
                    }
                    if (prospecto.doc_curp != null && prospecto.doc_curp.length > 0) {
                        this.setState({
                            urlCurp: prospecto.doc_curp, muestraDocCurp: true, colorSubirArchivoCurp: "#78CB5A",
                            colorTextoSubirArchivoCurp: "#FFFFFF", colorBotonSubirArchivoCurp: "#E5F6E0", nombreArchivoCurp: 'modificar'
                        })
                    }
                    if (prospecto.doc_rfc != null && prospecto.doc_rfc.length > 0) {
                        this.setState({
                            urlRFC: prospecto.doc_rfc, muestraDocRFC: true, colorSubirArchivoRFC: "#78CB5A",
                            colorTextoSubirArchivoRFC: "#FFFFFF", colorBotonSubirArchivoRFC: "#E5F6E0", nombreArchivoRFC: 'modificar'
                        })
                    }
                    if (prospecto.doc_cedula != null && prospecto.doc_cedula.length > 0) {
                        this.setState({
                            urlCedula: prospecto.doc_cedula, muestraDocCedula: true, colorSubirArchivoCedula: "#78CB5A",
                            colorTextoSubirArchivoCedula: "#FFFFFF", colorBotonSubirArchivoCedula: "#E5F6E0", nombreArchivoCedula: 'modificar'
                        })
                    }
                    if (prospecto.doc_carta_renuncia != null && prospecto.doc_carta_renuncia.length > 0) {
                        this.setState({
                            urlRenuncia: prospecto.doc_carta_renuncia, muestraDocRenuncia: true, colorSubirArchivoRenuncia: "#78CB5A",
                            colorTextoSubirArchivoRenuncia: "#FFFFFF", colorBotonSubirArchivoRenuncia: "#E5F6E0", nombreArchivoRenuncia: 'modificar'
                        })
                    }
                    if (prospecto.doc_examen_cei != null && prospecto.doc_examen_cei.length > 0) {
                        this.setState({
                            urlComprobantePagoCEI: prospecto.doc_examen_cei, muestraDocComprobantePagoCEI: true, colorSubirArchivoComprobantePagoCEI: "#78CB5A",
                            colorTextoSubirArchivoComprobantePagoCEI: "#FFFFFF", colorBotonSubirArchivoComprobantePagoCEI: "#E5F6E0", nombreArchivoComprobantePagoCEI: 'modificar'
                        })
                    }

                    if (prospecto.doc_aprobacion_cei != null && prospecto.doc_aprobacion_cei.length > 0) {
                        this.setState({
                            urlAprobacionExamenCEI: prospecto.doc_aprobacion_cei, muestraDocAprobacionExamenCEI: true, colorSubirArchivoAprobacionExamenCEI: "#78CB5A",
                            colorTextoSubirArchivoAprobacionExamenCEI: "#FFFFFF", colorBotonSubirArchivoAprobacionExamenCEI: "#E5F6E0", nombreArchivoAprobacionExamenCEI: 'modificar'
                        })
                    }

                    if (prospecto.doc_convenio != null && prospecto.doc_convenio.length > 0) {
                        this.setState({
                            urlConvenioFirmado: prospecto.doc_convenio, muestraDocConvenioFirmado: true, colorSubirArchivoConvenioFirmado: "#78CB5A",
                            colorTextoSubirArchivoConvenioFirmado: "#FFFFFF", colorBotonSubirArchivoConvenioFirmado: "#E5F6E0", nombreArchivoConvenioFirmado: 'modificar'
                        })
                    }
                    if (prospecto.doc_capacitacion != null && prospecto.doc_capacitacion.length > 0) {
                        this.setState({
                            urlCapacitacion: prospecto.doc_capacitacion, muestraDocCapacitacion: true, colorSubirArchivoCapacitacion: "#78CB5A",
                            colorTextoSubirArchivoCapacitacion: "#FFFFFF", colorBotonSubirArchivoCapacitacion: "#E5F6E0", nombreArchivoCapacitacion: 'modificar'
                        })
                    }

                    console.log("el props ", prospecto)
                    if (prospecto.doc_foto != null && prospecto.doc_foto.length > 0) {
                        this.setState({
                            imagenEnCambio: prospecto.doc_foto,existefoto:true
                        })
                    }
                }

                // this.setState({ tipoConexion: "Agente provisional", provisional: true }) 
                fetch(datos.urlServicePy+"recluta/api_recluta_ventacarrera16/" + this.props.idProspecto)
                    .then(response => response.json())
                    .then(jsonVenta => {
                        if (jsonVenta.length > 0) {
                            this.setState({ prospecto_ventaCarrera: jsonVenta[0] })
                            if (jsonVenta[0].curso_cedula != null) {
                                if (jsonVenta[0].curso_cedula == 1) {
                                    this.setState({ ban_ventaCarrera: true })
                                }
                            }
                            if (jsonVenta[0].conexion_id == 1) {
                                //this.state.prospecto.clave_cua_provisional = null
                                if(this.state.prospecto.clave_cua_provisional != null){
                                    this.setState({provitodefini:true})
                                }
                                this.setState({ tipoConexion: "Agente definitivo", definitivo: true })
                            } else {
                                //this.state.prospecto.clave_cua = null
                                this.setState({ tipoConexion: "Agente provisional", provisional: true })
                            }
                        }
                    })

                fetch(datos.urlServicePy+"parametros/api_recluta_prospectos/" + this.props.idProspecto)
                    .then(response => response.json())
                    .then(json => {
                        let fila = json.filas[0].fila
                        let columnas = json.columnas
                        let columnasSalida = {}
                        for (var i = 0; i < fila.length; i++) {
                            console.log(fila[i])
                            columnasSalida["" + columnas[i].key + ""] = fila[i].value
                        }

                        this.setState({ conexion: columnasSalida })
                    })

                fetch(datos.urlServicePy+"parametros/api_recluta_prospectos/" + this.props.idProspecto)
                    .then(response => response.json())
                    .then(recluta_prospecto => {
                        console.log("recluta_propspecto", recluta_prospecto)
                        fetch(datos.urlServicePy+"parametros/api_cat_empleados/" + recluta_prospecto.filas[0].fila[12].value)
                            .then(response => response.json())
                            .then(entrevistador => {
                                console.log("entrevistador ", entrevistador)
                                this.setState({ entrevistador: entrevistador.filas[0].fila[2].value + " " + entrevistador.filas[0].fila[3].value + " " + (entrevistador.filas[0].fila[4].value != null ? entrevistador.filas[0].fila[4].value : '') })
                            })

                    })

                this.construyeSelect(datos.urlServicePy+"parametros/api_cat_manejo_agentes_consolidado/0", "selectAgentesConsolidados")
                this.construyeSelectMotivos(datos.urlServicePy+"parametros/api_cat_motivo_envio_reserva/0","selectMotivosReserva")
                //this.construyeSelectMotivos(datos.urlServicePy+"parametros/api_cat_causas_baja/0","selectMotivosBaja")
                this.construyeSelect(datos.urlServicePy+"parametros/api_cat_causas_baja/0", "selectMotivosBaja")
                console.log("Entro aquí");
            })
    }

    construyeSelectMotivos(url,selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                console.log("respuesta  ", json)
                for (var i = 0; i < filas.length; i++) {
                    let fila = filas[i]

                    options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                }
                let salida = []

                if (selector == "selectMotivosReserva") {
                    salida.push(<select onChange={this.onChange} name="selectMotivosReserva" style={{ borderColor: '#F1F3FA' }} class="form-control" aria-label="Default select example">{options}</select>)
                    this.setState({ selectMotivosReserva: salida })
                }else if(selector == "selectMotivosBaja"){
                    salida.push(<select onChange={this.onChange} name="selectMotivosBaja" style={{ borderColor: '#F1F3FA' }} class="form-control" aria-label="Default select example">{options}</select>)
                    this.setState({ selectMotivosBaja: salida })
                }
            })
    }

    onClickImagen() {
        console.log("entrnaod a la imagen")
        this.imagen.click()
    }

    onChangeFile(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file", file);
        this.setState({ imagenEnCambio: URL.createObjectURL(file) })
        this.setState({ fotografiaFile: file }); /// if you want to upload latter
    }

    peticionAReserva() {
        let json = {
            "estatus_id": 18,
            "cauSASSR_baja_id": this.state.idMotivoBaja

        }
        const requestOptions = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };
        console.log("enviando a reserva")

        fetch(datos.urlServicePy+"parametros_upd/api_recluta_prospectos/" + this.props.idProspecto, requestOptions)
            .then(response => response.json())
            .then(data => {
                this.props.botonCancelar()
            })

    }

    peticionABaja() {
        let json = {
            "estatus_id": 19,
            "cauSASSR_baja_id": this.state.idMotivoBaja
        }
        const requestOptions = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };
        console.log("enviando a reserva")

        fetch(datos.urlServicePy+"parametros_upd/api_recluta_prospectos/" + this.props.idProspecto, requestOptions)
            .then(response => response.json())
            .then(data => {
                //this.construyeTarjetas()
                // this.props.guardaFormulario10(this.state.prospecto)
                this.props.botonCancelar()
            })
    }

    render() {
        return (
            <div>
                <div id="modal-reserva" className="modal fade" tabindex="-1">
                    <div className="modal-dialog ">
                        <div className="modal-content text-white" style={{ backgroundColor: "#FFFFF" }}>
                            <div className="modal-header text-white text-center" style={{ backgroundColor: "#617187" }}>
                                <h6 className="modal-title col-12 text-center">Enviar a reserva</h6>

                            </div>
                            <div className="modal-body">
                                <div className="card-body">
                                    <h8 style={{ color: '#8F9EB3' }}>Si está seguro de enviar a reserva el registro, por favor seleccione el motivo</h8>
                                    {this.state.selectMotivosReserva}
                                    <br></br>
                                    <h8 style={{ color: '#8F9EB3' }}>Fecha acordada para retomar el proceso</h8>
                                    <input type="date" max="2100-01-01" placeholder="Escribir" name="fecha_alta" className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />

                                </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-center">
                                <div className="col-12">
                                    <div className="row">
                                        <button type="button" style={{ width: '100%', backgroundColor: "#617187" }} class="btn text-white" onClick={this.peticionAReserva} data-dismiss="modal"  >Enviar a reserva</button>
                                    </div>
                                    <br></br>
                                    <div className="row">
                                        <button type="button" style={{ width: '100%', backgroundColor: '#8F9EB3' }} class="btn text-white" id="btnAgregar" data-dismiss="modal"  >Cancelar</button>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                <div id="modal-baja" className="modal fade" tabindex="-1">
                    <div className="modal-dialog ">
                        <div className="modal-content text-white" style={{ backgroundColor: "#FFFFF" }}>
                            <div className="modal-header text-white text-center" style={{ backgroundColor: "#617187" }}>
                                <h6 className="modal-title col-12 text-center">Dar baja el registro</h6>

                            </div>
                            <div className="modal-body">
                                <div className="card-body">
                                    <h8 style={{ color: '#8F9EB3' }}>Si está seguro de descartar el registro, por favor seleccione el motivo</h8>
                                    {this.state.selectMotivosBaja}


                                </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-center">
                                <div className="col-12">
                                    <div className="row">
                                        <button type="button" style={{ width: '100%', backgroundColor: "#617187" }} class="btn text-white" onClick={this.peticionABaja} data-dismiss="modal" >Dar de baja registro</button>
                                    </div>
                                    <br></br>
                                    <div className="row">
                                        <button type="button" style={{ width: '100%', backgroundColor: '#8F9EB3' }} class="btn text-white" id="btnAgregar" data-dismiss="modal"  >Cancelar</button>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                {
                    this.state.mostrarReporteCapacitacion == true &&
                    <ReporteCapacitacion mostrarConexionCapacitacion={this.mostrarConexionCapacitacion} idProspecto={this.props.idProspecto} tipoUsuario={this.props.tipoUsuario} nombreUsuario={this.props.nombreUsuario} tipoPantalla={"conexion"} guardarCapacitacion={this.guardarCapacitacion} />
                }
                {
                    this.state.mostrarConvenioArranque == true &&
                    <ConvenioArranque mostrarConexionCapacitacion={this.mostrarConexionCapacitacion} idProspecto={this.props.idProspecto} tipoUsuario={this.props.tipoUsuario} nombreUsuario={this.props.nombreUsuario} tipoPantalla={"conectado"} />
                }


                {
                    this.state.mostrarReporteCapacitacion == false && this.state.mostrarConvenioArranque == false &&
                    <div>
                        <div class="card">
                            <div class="row mb-2 mt-2 ml-1">
                                <div class="col-12 col-lg-9">
                                    <div class="row">

                                        <div class="col-6 col-lg-1 d-flex align-items-center pb-sm-1">
                                            <div class="mr-2">
                                                <a href="#"
                                                    class="btn rounded-pill btn-icon btn-sm" style={{ backgroundColor: '#78CB5A' }}> <span
                                                        class="letter-icon text-white"><i className="icon-checkmark2"></i></span>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-6 col-lg-1 d-flex align-items-center pb-sm-1">
                                            <div class="mr-2">
                                                <a href="#"
                                                    class="btn rounded-pill btn-icon btn-sm" style={{ backgroundColor: '#78CB5A' }}> <span
                                                        class="letter-icon text-white"><i className="icon-checkmark2"></i></span>
                                                </a>
                                            </div>
                                        </div>


                                        <div class="col-6 col-lg-1 d-flex align-items-center pb-sm-1">
                                            <div class="mr-2">
                                                <a href="#"
                                                    class="btn rounded-pill btn-icon btn-sm" style={{ backgroundColor: '#78CB5A' }}> <span
                                                        class="letter-icon text-white"><i className="icon-checkmark2"></i></span>
                                                </a>
                                            </div>
                                        </div>




                                        <div class="col-6 col-lg-1 d-flex align-items-center pb-sm-1">
                                            <div class="mr-2">
                                                <a href="#"
                                                    class="btn  rounded-pill btn-icon btn-sm" style={{ borderColor: '#41B3D1' }}> <span style={{ color: '#41B3D1' }}
                                                        class="letter-icon">CC</span>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-6 col-lg-2 d-flex align-items-center pb-sm-1">

                                            <div class="mr-5 ">
                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#41B3D1', borderColor: '#41B3D1' }}
                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                        class="letter-icon text-white">1</span>
                                                </a>
                                            </div>
                                        </div>





                                        <div class="col-6 col-lg-1 d-flex align-items-center pb-sm-1">
                                            <div class="mr-2">
                                                <a href="#"
                                                    class="btn  rounded-pill btn-icon btn-sm" style={{ borderColor: '#78CB5A' }}> <span style={{ color: '#78CB5A' }}
                                                        class="letter-icon">C</span>
                                                </a>
                                            </div>

                                        </div>




                                    </div>
                                </div>


                                <div class="col-12 col-lg-3">
                                    <div class="float-right mr-3">
                                        <button type="button" title="Guardar y Regresar" onClick={this.regresaryGuardar} class="btn rounded-pill mr-1" style={{ backgroundColor: '#8F9EB3', borderColor: '#8F9EB3' }}><span><i style={{ color: 'white' }} className="icon-backward2"></i></span></button>
                                        <button type="button" title="Registrar Conexión/Capacitación" onClick={(e) => this.guardarFormulario(true)} disabled={
                                            this.state.nombreArchivoIdentificacion == "" ||
                                            this.state.nombreArchivoActaNacimiento == "" ||
                                            this.state.nombreArchivoCertificadoEstudios == "" ||
                                            this.state.nombreArchivoCurp == "" ||
                                            this.state.nombreArchivoRFC == "" ||
                                            this.state.nombreArchivoCedula == "" ||
                                            this.state.provisional == true && (this.state.prospecto.clave_cua_provisional == null || this.state.prospecto.clave_cua_provisional == "") ||
                                            this.state.provisional == true && (this.state.prospecto.antiguedad_agente == null) ||
                                            this.state.provisional == true && (this.state.prospecto.generacion == null || this.state.prospecto.generacion == 0) ||
                                            this.state.provisional == true && (this.state.prospecto.agente_consolidado_id == 0 || this.state.prospecto.agente_consolidado_id == null) ||
                                            this.state.provisional == true && (this.state.prospecto.alta_prospecto_ideas == 0 || this.state.prospecto.alta_prospecto_ideas == null) ||
                                            this.state.definitivo == true && (this.state.prospecto.fecha_examen_cei == null || this.state.prospecto.fecha_examen_cei == "") ||
                                            this.state.definitivo == true && (this.state.prospecto.clave_cua == null || this.state.prospecto.clave_cua == "") ||
                                            /*this.state.definitivo == true && (this.state.prospecto.clave_cua_provisional == null || this.state.prospecto.clave_cua_provisional == "") ||*/
                                            this.state.definitivo == true && (this.state.prospecto.fecha_alta_cnsf == undefined || this.state.prospecto.fecha_alta_cnsf == "") ||
                                            this.state.definitivo == true && (this.state.prospecto.fecha_ini_cedula == undefined || this.state.prospecto.fecha_ini_cedula == "") ||
                                            this.state.definitivo == true && (this.state.prospecto.fecha_fin_cedula == undefined || this.state.prospecto.fecha_fin_cedula == "") ||
                                            this.state.definitivo == true && (this.state.prospecto.fecha_conexion_definitiva == undefined || this.state.prospecto.fecha_conexion_definitiva == "") ||
                                            this.state.definitivo == true && (this.state.prospecto.antiguedad_agente == undefined) ||
                                            this.state.definitivo == true && (this.state.prospecto.generacion == null || this.state.prospecto.generacion == 0) ||
                                            this.state.definitivo == true && (this.state.prospecto.agente_consolidado_id == 0 || this.state.prospecto.agente_consolidado_id == null) ||
                                            this.state.definitivo == true && (this.state.ban_ventaCarrera && (this.state.prospecto.fecha_curso_cedula == undefined || this.state.prospecto.fecha_curso_cedula == ""))
                                        } class="btn rounded-pill mr-1"
                                            style={{ backgroundColor: '#8F9EB3', borderColor: '#8F9EB3' }}><span><i style={{ color: 'white' }} className="icon-forward3"></i></span></button>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="card-header bg-transparent header-elements-sm-inline">
                            <h3 class="mb-0 font-weight-semibold" style={{ color: '#617187' }}>EN CONEXIÓN / CAPACITACIÓN</h3>
                            <div class="header-elements">
                                <ul class="list-inline mb-0">
                                    <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Tipo de conexión:<strong >{this.state.tipoConexion}</strong></li>
                                    <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Prospecto: <strong >{this.state.conexion.nombre + ' ' + this.state.conexion.ape_paterno + ' ' + (this.state.conexion.ape_materno != null ? this.state.conexion.ape_materno : '')}</strong></li>
                                    <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Fecha de registro de Venta de Carrera: <strong >{this.state.conexion.ts_alta_audit}</strong></li>
                                    <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Entrevistador: <strong >{this.state.entrevistador}</strong></li>
                                    <li class="list-inline-item">
                                        <h6 style={{ backgroundColor: "#41B3D1", color: '#FFFF', borderRadius: '8px' }}> <>&nbsp;&nbsp;</>EN CONEXIÓN / CAPACITACIÓN<>&nbsp;&nbsp;</> </h6>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <br />

                        <div className="row">
                            <div className="col-xs-9 col-lg-9">
                                <>&nbsp;&nbsp;</>
                            </div>
                            <div className="col-xs-3 col-lg-3">
                                <div className="row">
                                    <div className="col-xs-6 col-lg-6">
                                        <button data-toggle="modal" data-target="#modal-reserva" class="btn  mr-1" style={{ backgroundColor: "#617187", color: '#FFFF' }}> Enviar a reserva</button>
                                    </div>
                                    <div className="col-xs-6 col-lg-6">
                                        <button data-toggle="modal" data-target="#modal-baja" class="btn  mr-1" style={{ backgroundColor: "#617187", color: '#FFFF' }}> Enviar a baja</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-2 col-lg-4">
                                <h7 class="mb-0 font-weight-semibold">Juego de Conexión</h7>
                            </div>
                        </div>
                        <div class="card">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-xs-4 col-lg-4">
                                        <div className="row">
                                            <div className="col-xs-2 col-lg-4">
                                                <h8 class="mb-0 font-weight-semibold">Fotografía conexión</h8>
                                            </div>

                                            <div className="col-xs-2 col-lg-4">


                                                <div>
                                                    <input type="image" disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onClick={this.onClickImagen} src={this.state.imagenEnCambio == undefined ? avatar : this.state.imagenEnCambio}
                                                        class="img-fluid rounded-circle" width="40" height="40" alt="" />
                                                    <form encType="multipart/form">
                                                        <input type="file" disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} style={{ display: 'none' }} ref={(ref) => this.imagen = ref} onChange={this.onChangeFile.bind(this)}></input>
                                                    </form>
                                                </div>
                                                {/*<img src={avatar} class="img-fluid rounded-circle" width="40" height="40" alt="" />*/}

                                            </div>

                                        </div>
                                    </div>

                                    <div className="col-xs-4 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <form encType="multipart/form" style={{ display: 'none' }} >
                                                    <input type="file" style={{ display: 'none' }} ref={(ref) => this.upload = ref} onChange={this.onChangeFileIdentificacion.bind(this)}></input>
                                                </form>
                                                <input type="text" className="form-control " disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} placeholder="Identificación Oficial" value={this.state.nombreArchivoIdentificacion} />
                                                <span className="input-group-prepend">

                                                    {
                                                        this.state.muestraDocIdentificacion == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoIdentificacion }}>
                                                            <a href={this.state.urlIdentificacion} target="_blank" rel="noopener noreferrer" >
                                                                <i className="fas fa-eye"
                                                                    style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                            </a>
                                                        </button> : ''


                                                    }
                                                    <button type="button" class="btn text-white" disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onClick={this.onClickBotonArchivoIdentificacion} style={{ backgroundColor: this.state.colorBotonSubirArchivoIdentificacion }}>
                                                        <h10 style={{ color: "black" }}>+</h10>
                                                    </button>
                                                    <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >Identificación Oficial</span>
                                                </span>
                                            </div>
                                            {
                                                this.state.nombreArchivoIdentificacion == "" ?
                                                    <span style={{ color: "red" }}>Identificación Oficial, es requerida</span> : ''
                                            }
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">

                                                <form encType="multipart/form" style={{ display: 'none' }} >
                                                    <input type="file" style={{ display: 'none' }} ref={(ref) => this.acta = ref} onChange={this.onChangeFileActaNacimiento.bind(this)}></input>
                                                </form>
                                                <input type="text" className="form-control " disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} placeholder="Acta de nacimiento" value={this.state.nombreArchivoActaNacimiento} />
                                                <span className="input-group-prepend">

                                                    {
                                                        this.state.muestraDocActaNacimiento == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoActaNacimiento }}>
                                                            <a href={this.state.urlActaNacimiento} target="_blank" rel="noopener noreferrer" >
                                                                <i className="fas fa-eye"
                                                                    style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                            </a>
                                                        </button> : ''


                                                    }
                                                    <button type="button" class="btn text-white" disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onClick={this.onClickBotonArchivoActaNacimiento} style={{ backgroundColor: this.state.colorBotonSubirArchivoActaNacimiento }}>
                                                        <h10 style={{ color: "white" }}>+</h10>
                                                    </button>

                                                    <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >Acta de nacimiento</span>
                                                </span>
                                            </div>
                                            {
                                                this.state.nombreArchivoActaNacimiento == "" ?
                                                    <span style={{ color: "red" }}>Acta de nacimiento, es requerida</span> : ''
                                            }
                                        </div>
                                    </div>


                                    {/* aqui esta el conyge */}
                                    {/* <div className="col-xs-4 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Cónyugue</span>
                                                </span> <input type="text" placeholder="Escribir" name="conyuge" onChange={this.onChange} value={this.state.prospecto.conyuge} className="form-control " />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-xs-4 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Nacimiento cónyugue</span>
                                                </span> <input type="date" placeholder="" name="fecha_conyuge" onChange={this.onChange} value={this.state.prospecto.fecha_conyuge} className="form-control " />
                                            </div>
                                        </div>
                                    </div> */}

                                </div>

                                <div className="row">

                                    {/*<div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <form encType="multipart/form" style={{ display: 'none' }} >
                                                    <input type="file" style={{ display: 'none' }} ref={(ref) => this.upload = ref} onChange={this.onChangeFileIdentificacion.bind(this)}></input>
                                                </form>
                                                <input type="text" className="form-control " placeholder="Archivo PSP" value={this.state.nombreArchivoIdentificacion} />
                                                <span className="input-group-prepend">

                                                    {
                                                        this.state.muestraDocIdentificacion == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoIdentificacion }}>
                                                            <a href={this.state.urlIdentificacion} target="_blank" rel="noopener noreferrer" >
                                                                <i className="fas fa-eye"
                                                                    style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                            </a>
                                                        </button> : ''


                                                    }
                                                    <button type="button" class="btn text-white" onClick={this.onClickBotonArchivoIdentificacion} style={{ backgroundColor: this.state.colorBotonSubirArchivoIdentificacion }}>
                                                        <h10 style={{ color: "white" }}>+</h10>
                                                    </button>

                                                    <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >Identificación Oficial *</span>





                                                </span>
                                            </div>
                                        </div>
                                                </div>

                                    <div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">

                                                <form encType="multipart/form" style={{ display: 'none' }} >
                                                    <input type="file" style={{ display: 'none' }} ref={(ref) => this.acta = ref} onChange={this.onChangeFileActaNacimiento.bind(this)}></input>
                                                </form>
                                                <input type="text" className="form-control " placeholder="Archivo PSP" value={this.state.nombreArchivoActaNacimiento} />
                                                <span className="input-group-prepend">

                                                    {
                                                        this.state.muestraDocActaNacimiento == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoActaNacimiento }}>
                                                            <a href={this.state.urlActaNacimiento} target="_blank" rel="noopener noreferrer" >
                                                                <i className="fas fa-eye"
                                                                    style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                            </a>
                                                        </button> : ''


                                                    }
                                                    <button type="button" class="btn text-white" onClick={this.onClickBotonArchivoActaNacimiento} style={{ backgroundColor: this.state.colorBotonSubirArchivoActaNacimiento }}>
                                                        <h10 style={{ color: "white" }}>+</h10>
                                                    </button>

                                                    <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >Acta de nacimiento *</span>





                                                </span>


                                            </div>
                                        </div>
                                     </div>*/}


                                    <div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">

                                                <form encType="multipart/form" style={{ display: 'none' }} >
                                                    <input type="file" style={{ display: 'none' }} ref={(ref) => this.certificado = ref} onChange={this.onChangeFileCertificadoEstudios.bind(this)}></input>
                                                </form>
                                                <input type="text" className="form-control " disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} placeholder="Certificado de estudios" value={this.state.nombreArchivoCertificadoEstudios} />
                                                <span className="input-group-prepend">

                                                    {
                                                        this.state.muestraDocCertificadoEstudios == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoCertificadoEstudios }}>
                                                            <a href={this.state.urlCertificadoEstudios} target="_blank" rel="noopener noreferrer" >
                                                                <i className="fas fa-eye"
                                                                    style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                            </a>
                                                        </button> : ''


                                                    }
                                                    <button type="button" class="btn text-white" disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onClick={this.onClickBotonArchivoCertificadoEstudios} style={{ backgroundColor: this.state.colorBotonSubirArchivoCertificadoEstudios }}>
                                                        <h10 style={{ color: "white" }}>+</h10>
                                                    </button>
                                                    <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >Certificado de estudios</span>
                                                </span>
                                            </div>
                                            {
                                                this.state.nombreArchivoCertificadoEstudios == "" ?
                                                    <span style={{ color: "red" }}>Certificado de estudios, es requerida</span> : ''
                                            }
                                        </div>
                                    </div>
                                    <div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <form encType="multipart/form" style={{ display: 'none' }} >
                                                    <input type="file" style={{ display: 'none' }} ref={(ref) => this.curp = ref} onChange={this.onChangeFileCurp.bind(this)}></input>
                                                </form>
                                                <input type="text" className="form-control " disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} placeholder="CURP" value={this.state.nombreArchivoCurp} />
                                                <span className="input-group-prepend">

                                                    {
                                                        this.state.muestraDocCurp == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoCurp }}>
                                                            <a href={this.state.urlCurp} target="_blank" rel="noopener noreferrer" >
                                                                <i className="fas fa-eye"
                                                                    style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                            </a>
                                                        </button> : ''


                                                    }
                                                    <button type="button" class="btn text-white" disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onClick={this.onClickBotonArchivoCurp} style={{ backgroundColor: this.state.colorBotonSubirArchivoCurp }}>
                                                        <h10 style={{ color: "white" }}>+</h10>
                                                    </button>

                                                    <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >CURP</span>
                                                </span>
                                            </div>
                                            {
                                                this.state.nombreArchivoCurp == "" ?
                                                    <span style={{ color: "red" }}>CURP, es requerida</span> : ''
                                            }
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <form encType="multipart/form" style={{ display: 'none' }} >
                                                    <input type="file" style={{ display: 'none' }} ref={(ref) => this.rfc = ref} onChange={this.onChangeFileRFC.bind(this)}></input>
                                                </form>
                                                <input type="text" className="form-control " disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} placeholder="RFC" value={this.state.nombreArchivoRFC} />
                                                <span className="input-group-prepend">

                                                    {
                                                        this.state.muestraDocRFC == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoRFC }}>
                                                            <a href={this.state.urlRFC} target="_blank" rel="noopener noreferrer" >
                                                                <i className="fas fa-eye"
                                                                    style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                            </a>
                                                        </button> : ''


                                                    }
                                                    <button type="button" class="btn text-white" disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onClick={this.onClickBotonArchivoRFC} style={{ backgroundColor: this.state.colorBotonSubirArchivoRFC }}>
                                                        <h10 style={{ color: "white" }}>+</h10>
                                                    </button>

                                                    <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >RFC</span>
                                                </span>
                                            </div>
                                            {
                                                this.state.nombreArchivoRFC == "" ?
                                                    <span style={{ color: "red" }}>RFC, es requerida</span> : ''
                                            }
                                        </div>
                                    </div>

                                </div>


                                <div className="row">

                                    {/*<div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <form encType="multipart/form" style={{ display: 'none' }} >
                                                    <input type="file" style={{ display: 'none' }} ref={(ref) => this.curp = ref} onChange={this.onChangeFileCurp.bind(this)}></input>
                                                </form>
                                                <input type="text" className="form-control " placeholder="Archivo PSP" value={this.state.nombreArchivoCurp} />
                                                <span className="input-group-prepend">

                                                    {
                                                        this.state.muestraDocCurp == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoCurp }}>
                                                            <a href={this.state.urlCurp} target="_blank" rel="noopener noreferrer" >
                                                                <i className="fas fa-eye"
                                                                    style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                            </a>
                                                        </button> : ''


                                                    }
                                                    <button type="button" class="btn text-white" onClick={this.onClickBotonArchivoCurp} style={{ backgroundColor: this.state.colorBotonSubirArchivoCurp }}>
                                                        <h10 style={{ color: "white" }}>+</h10>
                                                    </button>

                                                    <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >CURP *</span>





                                                </span>


                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <form encType="multipart/form" style={{ display: 'none' }} >
                                                    <input type="file" style={{ display: 'none' }} ref={(ref) => this.rfc = ref} onChange={this.onChangeFileRFC.bind(this)}></input>
                                                </form>
                                                <input type="text" className="form-control " placeholder="Archivo PSP" value={this.state.nombreArchivoRFC} />
                                                <span className="input-group-prepend">

                                                    {
                                                        this.state.muestraDocRFC == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoRFC }}>
                                                            <a href={this.state.urlRFC} target="_blank" rel="noopener noreferrer" >
                                                                <i className="fas fa-eye"
                                                                    style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                            </a>
                                                        </button> : ''


                                                    }
                                                    <button type="button" class="btn text-white" onClick={this.onClickBotonArchivoRFC} style={{ backgroundColor: this.state.colorBotonSubirArchivoRFC }}>
                                                        <h10 style={{ color: "white" }}>+</h10>
                                                    </button>

                                                    <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >RFC *</span>





                                                </span>
                                            </div>
                                        </div>
                                        </div>*/}


                                    <div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <form encType="multipart/form" style={{ display: 'none' }} >
                                                    <input type="file" style={{ display: 'none' }} ref={(ref) => this.cedula = ref} onChange={this.onChangeFileCedula.bind(this)}></input>
                                                </form>
                                                <input type="text" className="form-control " disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} placeholder="Cédula de agente" value={this.state.nombreArchivoCedula} />
                                                <span className="input-group-prepend">

                                                    {
                                                        this.state.muestraDocCedula == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoCedula }}>
                                                            <a href={this.state.urlCedula} target="_blank" rel="noopener noreferrer" >
                                                                <i className="fas fa-eye"
                                                                    style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                            </a>
                                                        </button> : ''


                                                    }
                                                    <button type="button" class="btn text-white" disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onClick={this.onClickBotonArchivoCedula} style={{ backgroundColor: this.state.colorBotonSubirArchivoCedula }}>
                                                        <h10 style={{ color: "white" }}>+</h10>
                                                    </button>

                                                    <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >Cédula de agente</span>
                                                </span>
                                            </div>
                                            {
                                                this.state.nombreArchivoCedula == "" ?
                                                    <span style={{ color: "red" }}>Cédula de agente, es requerida</span> : ''
                                            }
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <form encType="multipart/form" style={{ display: 'none' }} >
                                                    <input type="file" style={{ display: 'none' }} ref={(ref) => this.renuncia = ref} onChange={this.onChangeFileRenuncia.bind(this)}></input>
                                                </form>
                                                <input type="text" className="form-control " disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} placeholder="Carta de renuncia" value={this.state.nombreArchivoRenuncia} />
                                                <span className="input-group-prepend">

                                                    {
                                                        this.state.muestraDocRenuncia == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoRenuncia }}>
                                                            <a href={this.state.urlRenuncia} target="_blank" rel="noopener noreferrer" >
                                                                <i className="fas fa-eye"
                                                                    style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                            </a>
                                                        </button> : ''


                                                    }
                                                    <button type="button" class="btn text-white" disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onClick={this.onClickBotonArchivoRenuncia} style={{ backgroundColor: this.state.colorBotonSubirArchivoRenuncia }}>
                                                        <h10 style={{ color: "white" }}>+</h10>
                                                    </button>

                                                    <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >Carta de renuncia</span>
                                                </span>
                                            </div>
                                            {/*
                                                    this.state.nombreArchivoRenuncia == "" ?
                                                    <span style={{ color: "red" }}>Carta de renuncia, es requerida</span> : ''
                                                */}
                                        </div>
                                    </div>


                                </div>





                            </div>
                        </div>

                        {this.state.provisional == true &&
                            <div>
                                <div class="card">
                                    <div class="card-header  header-elements-sm-inline">
                                        <h5 class="mb-0 font-weight-semibold" >Datos Conexión</h5>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-xs-2 col-lg-4">
                                        <h7 class="mb-0 font-weight-semibold">Servicio al socio</h7>
                                    </div>
                                </div>
                                <div class="card">
                                    <div className="card-body">
                                        <div className="row">
                                            <div className="col-xs-4 col-lg-4">
                                                <div className="row">
                                                    <div className="col-xs-8 col-lg-8">
                                                        <h8 class="mb-0 font-weight-semibold">Finalizó Curso IDEAS</h8>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-4">
                                                        <div className="custom-control custom-switch  mb-2" >
                                                            <input type="checkbox" class="custom-control-input" id="curso_ideas" name="curso_ideas" disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospecto.curso_ideas}></input>
                                                            <label class="custom-control-label" for="curso_ideas"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            { this.state.definitivo ?
                                            <div className="col-xs-4 col-lg-4">
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        <span className="input-group-prepend"> <span
                                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Clave única de agente CUA definitivo</span>
                                                        </span> <input type="number" placeholder="" name="clave_cua" disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.clave_cua} className="form-control " />
                                                    </div>
                                                    {
                                                        this.state.provisional == true && (this.state.prospecto.clave_cua == null || this.state.prospecto.clave_cua == "") ?
                                                            <span style={{ color: "red" }}>Clave única de agente CUA definitivo, es un campo requerido</span> : ''
                                                    }
                                                </div>
                                            </div> : ''}

                                            { this.state.provisional ? 
                                            <div className="col-xs-4 col-lg-4">
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        <span className="input-group-prepend"> <span
                                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Clave única de agente CUA provisional</span>
                                                        </span> <input type="number" placeholder="" name="clave_cua_provisional" disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.clave_cua_provisional} className="form-control " />
                                                    </div>
                                                    {
                                                        this.state.provisional == true && (this.state.prospecto.clave_cua_provisional == null || this.state.prospecto.clave_cua_provisional == "") ?
                                                            <span style={{ color: "red" }}>Clave única de agente CUA provisional, es un campo requerido</span> : ''
                                                    }
                                                </div>
                                            </div> : ''}
                                        </div>

                                        <div className="row">
                                            <div className="col-xs-4 col-lg-4">
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        <span className="input-group-prepend"> <span
                                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Conexión GNP agente provisional</span>
                                                        </span> <input type="date" max="2100-01-01" placeholder="" name="fecha_conexion_provicional" disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.fecha_conexion_provicional} className="form-control " />
                                                    </div>
                                                    {
                                                        this.state.provisional == true && (this.state.prospecto.fecha_conexion_provicional == undefined || this.state.prospecto.fecha_conexion_provicional == "") ?
                                                            <span style={{ color: "red" }}>Conexión GNP agente provisional, es un campo requerido</span> : ''
                                                    }
                                                </div>
                                            </div>
                                            <div className="col-xs-4 col-lg-4">
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        <span className="input-group-prepend"> <span
                                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Antigüedad del agente</span>
                                                        </span> <input type="number" placeholder="" name="antiguedad_agente" disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.antiguedad_agente} className="form-control " />
                                                    </div>
                                                    {
                                                        this.state.provisional == true && (this.state.prospecto.antiguedad_agente == null) ?
                                                            <span style={{ color: "red" }}>Antigüedad del agente, es un campo requerido</span> : ''
                                                    }
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="col-xs-4 col-lg-4">
                                                <div className="row">
                                                    {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>} <label >Generación del agente</label>
                                                </div>
                                                <div className="row">
                                                    {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" value={1} disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} id="G1" name="generacion" checked={this.state.prospecto.generacion == 1} />
                                                        <label class="form-check-label" for="G1">
                                                            G1
                                                        </label>
                                                    </div>
                                                    {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" value={2} disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} id="G2" name="generacion" checked={this.state.prospecto.generacion == 2} />
                                                        <label class="form-check-label" for="G2">
                                                            G2
                                                        </label>
                                                    </div>

                                                    {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" value={3} disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} id="G3" name="generacion" checked={this.state.prospecto.generacion == 3} />
                                                        <label class="form-check-label" for="G3">
                                                            G3
                                                        </label>
                                                    </div>
                                                    {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" value={4} disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} id="G4" name="generacion" checked={this.state.prospecto.generacion == 4} />
                                                        <label class="form-check-label" for="G4">
                                                            G4
                                                        </label>
                                                    </div>
                                                    {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" value={5} disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} id="C" name="generacion" checked={this.state.prospecto.generacion == 5} />
                                                        <label class="form-check-label" for="C">
                                                            C
                                                        </label>
                                                    </div>
                                                </div>
                                                {
                                                    this.state.provisional == true && (this.state.prospecto.generacion == null || this.state.prospecto.generacion == 0) ?
                                                        <span style={{ color: "red" }}>Generación de la figura comercial, debe de ser seleccionada</span> : ''
                                                }
                                            </div>

                                            <div className="col-xs-8 col-lg-8">
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        <span className="input-group-prepend"> <span
                                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }} onChange={this.onChange}>Manejo y consideraciones de Agentes Consolidados</span>
                                                        </span> {this.state.selectAgentesConsolidados}
                                                    </div>
                                                    {
                                                        this.state.provisional == true && (this.state.prospecto.agente_consolidado_id == 0 || this.state.prospecto.agente_consolidado_id == null) ?
                                                            <span style={{ color: "red" }}>Manejo y consideraciones de Agentes Consolidados, debe de ser seleccionada</span> : ''
                                                    }
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        }


                        {this.state.provisional == true &&
                            <div>
                                <div className="row">
                                    <div className="col-xs-2 col-lg-4">
                                        <h7 class="mb-0 font-weight-semibold">Capacitación</h7>
                                    </div>
                                </div>
                                <div class="card">
                                    <div className="card-body">
                                        <div className="row">
                                            <div className="col-xs-2 col-lg-2">
                                                <h8 class="mb-0 font-weight-semibold">Alta del prospecto del Curso IDEAS</h8>
                                            </div>
                                            <div className="col-xs-2 col-lg-4">
                                                <div className="custom-control custom-switch  mb-2" >
                                                    <input type="checkbox" class="custom-control-input" id="alta_prospecto_ideas" name="alta_prospecto_ideas" disabled={this.props.tipoUsuario != "ASESJRCAP" && this.props.tipoUsuario != "GTECAP" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospecto.alta_prospecto_ideas}></input>
                                                    <label class="custom-control-label" for="alta_prospecto_ideas"></label>
                                                </div>
                                                {
                                                    this.state.provisional == true && (this.state.prospecto.alta_prospecto_ideas == 0 || this.state.prospecto.alta_prospecto_ideas == null) ?
                                                        <span style={{ color: "red" }}>Alta del prospecto del Curso IDEAS, debe de ser seleccionada</span> : ''
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>}

                        {this.state.definitivo == true &&
                            <div>
                                <div class="card">
                                    <div class="card-header  header-elements-sm-inline">
                                        <h5 class="mb-0 font-weight-semibold" >Datos Conexión</h5>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-xs-2 col-lg-4">
                                        <h7 class="mb-0 font-weight-semibold">Servicio al socio</h7>
                                    </div>
                                </div>
                                <div class="card">
                                    <div className="card-body">
                                        <div className="row">
                                            <div className="col-xs-8 col-lg-8">
                                                <div className="row">
                                                    <div className="col-xs-8 col-lg-8">
                                                        <h8 class="mb-0 font-weight-semibold">Se envió correo electrónico solicitando pago de cedula y gestión de certificación al prospecto</h8>
                                                    </div>

                                                    <div className="col-xs-2 col-lg-4">
                                                        <div className="custom-control custom-switch  mb-2" >
                                                            <input type="checkbox" class="custom-control-input" id="envio_correo" name="envio_correo" disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospecto.envio_correo}></input>
                                                            <label class="custom-control-label" for="envio_correo"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br></br>
                                        <div className="row">

                                            <div className="col-xs-8 col-lg-8">
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        <form encType="multipart/form" style={{ display: 'none' }} >
                                                            <input type="file" style={{ display: 'none' }} ref={(ref) => this.pagocei = ref} disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onChange={this.onChangeFileComprobantePagoCEI.bind(this)}></input>
                                                        </form>
                                                        <input type="text" className="form-control " placeholder="Comprobante de pago del examen en el CEI" value={this.state.nombreArchivoComprobantePagoCEI} />
                                                        <span className="input-group-prepend">

                                                            {
                                                                this.state.muestraDocComprobantePagoCEI == true ? <button type="button" class="btn text-white" disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} style={{ backgroundColor: this.state.colorBotonSubirArchivoComprobantePagoCEI }}>
                                                                    <a href={this.state.urlComprobantePagoCEI} target="_blank" rel="noopener noreferrer" >
                                                                        <i className="fas fa-eye"
                                                                            style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                    </a>
                                                                </button> : ''


                                                            }
                                                            <button type="button" class="btn text-white" onClick={this.onClickBotonArchivoComprobantePagoCEI} disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} style={{ backgroundColor: this.state.colorBotonSubirArchivoComprobantePagoCEI }}>
                                                                <h10 style={{ color: "white" }}>+</h10>
                                                            </button>

                                                            <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >Comprobante de pago del examen en el CEI</span>





                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-xs-4 col-lg-4">
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        <span className="input-group-prepend"> <span
                                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Examen en el CEI</span>
                                                        </span> <input type="date" placeholder="" name="fecha_examen_cei" max="2100-01-01" disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.fecha_examen_cei} className="form-control " />
                                                    </div>
                                                    {
                                                        this.state.definitivo == true && (this.state.prospecto.fecha_examen_cei == null || this.state.prospecto.fecha_examen_cei == "") ?
                                                            <span style={{ color: "red" }}>La fecha Examen en el CEI, es un campo requerido</span> : ''
                                                    }
                                                </div>
                                            </div>



                                        </div>
                                        <br></br>
                                        <div className="row">


                                            <div className="col-xs-4 col-lg-4">
                                                <div className="row">
                                                    <div className="col-xs-8 col-lg-8">
                                                        <h8 class="mb-0 font-weight-semibold">Aprobación del examen en el CEI</h8>
                                                    </div>

                                                    <div className="col-xs-2 col-lg-4">
                                                        <div className="custom-control custom-switch  mb-2" >
                                                            <input type="checkbox" class="custom-control-input" id="aprobacion_examen_cei" name="aprobacion_examen_cei" disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospecto.aprobacion_examen_cei}></input>
                                                            <label class="custom-control-label" for="aprobacion_examen_cei"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-xs-8 col-lg-8">
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        <form encType="multipart/form" style={{ display: 'none' }} >
                                                            <input type="file" style={{ display: 'none' }} ref={(ref) => this.aprobacioncei = ref} onChange={this.onChangeFileAprobacionExamenCEI.bind(this)}></input>
                                                        </form>
                                                        <input type="text" className="form-control " disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} placeholder="Evidencia de aprobación del examen en el CEI" value={this.state.nombreArchivoAprobacionExamenCEI} />
                                                        <span className="input-group-prepend">

                                                            {
                                                                this.state.muestraDocAprobacionExamenCEI == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoAprobacionExamenCEI }}>
                                                                    <a href={this.state.urlAprobacionExamenCEI} target="_blank" rel="noopener noreferrer" >
                                                                        <i className="fas fa-eye"
                                                                            style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                    </a>
                                                                </button> : ''


                                                            }
                                                            <button type="button" class="btn text-white" disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onClick={this.onClickBotonArchivoAprobacionExamenCEI} style={{ backgroundColor: this.state.colorBotonSubirArchivoAprobacionExamenCEI }}>
                                                                <h10 style={{ color: "white" }}>+</h10>
                                                            </button>

                                                            <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >Evidencia de aprobación del examen en el CEI</span>





                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>


                                        <div className="row">
                                            <div className="col-xs-4 col-lg-4">
                                                <div className="row">
                                                    <div className="col-xs-8 col-lg-8">
                                                        <h8 class="mb-0 font-weight-semibold">Alta en SAT</h8>
                                                    </div>

                                                    <div className="col-xs-2 col-lg-4">
                                                        <div className="custom-control custom-switch  mb-2" >
                                                            <input type="checkbox" class="custom-control-input" id="alta_sat" name="alta_sat" disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospecto.alta_sat}></input>
                                                            <label class="custom-control-label" for="alta_sat"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-xs-4 col-lg-4">
                                            { this.state.definitivo ?
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        <span className="input-group-prepend"> <span
                                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Clave única de agente CUA definitivo</span>
                                                        </span> <input type="number" placeholder="" name="clave_cua" disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.clave_cua} className="form-control " />
                                                    </div>
                                                    {
                                                        this.state.definitivo == true && (this.state.prospecto.clave_cua == null || this.state.prospecto.clave_cua == "") ?
                                                            <span style={{ color: "red" }}>Clave única de agente CUA definitivo, es un campo requerido</span> : ''
                                                    }
                                                </div> : ''}
                                            </div>
                                            { this.state.definitivo && this.state.provitodefini ? 
                                            <div className="col-xs-4 col-lg-4">
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        <span className="input-group-prepend"> <span
                                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Clave única de agente CUA provisional</span>
                                                        </span> <input type="number" placeholder="" name="clave_cua_provisional" disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.clave_cua_provisional} className="form-control " />
                                                    </div>
                                                    {
                                                        /*this.state.definitivo == true && (this.state.prospecto.clave_cua_provisional == null || this.state.prospecto.clave_cua_provisional == "") ?
                                                            <span style={{ color: "red" }}>Clave única de agente CUA provisional, es un campo requerido</span> : ''
                                                    */}
                                                </div>
                                            </div> : ''}
                                        </div>


                                        <div className="row">
                                            <div className="col-xs-4 col-lg-4">
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        <span className="input-group-prepend"> <span
                                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Entrega de cédula en CNCF</span>
                                                        </span> <input type="date" placeholder="" name="fecha_entrega_cedula" max="2100-01-01" disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.fecha_entrega_cedula} className="form-control " />
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-xs-4 col-lg-4">
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        <span className="input-group-prepend"> <span
                                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Alta en CNSF</span>
                                                        </span> <input type="date" placeholder="" name="fecha_alta_cnsf" max="2100-01-01" disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.fecha_alta_cnsf} className="form-control " />
                                                    </div>
                                                    {
                                                        this.state.definitivo == true && (this.state.prospecto.fecha_alta_cnsf == undefined || this.state.prospecto.fecha_alta_cnsf == "") ?
                                                            <span style={{ color: "red" }}>El campo Alta en CNSF, es un campo requerido</span> : ''
                                                    }
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="col-xs-4 col-lg-4">
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        <span className="input-group-prepend"> <span
                                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fecha inicio vigencia de cédula</span>
                                                        </span> <input type="date" placeholder="" name="fecha_ini_cedula" max="2100-01-01" disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.fecha_ini_cedula} className="form-control " />
                                                    </div>
                                                    {
                                                        this.state.definitivo == true && (this.state.prospecto.fecha_ini_cedula == undefined || this.state.prospecto.fecha_ini_cedula == "") ?
                                                            <span style={{ color: "red" }}>El campo Fecha inicio vigencia de cédula, es un campo requerido</span> : ''
                                                    }
                                                </div>
                                            </div>
                                            <div className="col-xs-4 col-lg-4">
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        <span className="input-group-prepend"> <span
                                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fecha fin vigencia de cédula</span>
                                                        </span> <input type="date" placeholder="" name="fecha_fin_cedula" max="2100-01-01" disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.fecha_fin_cedula} className="form-control " />
                                                    </div>
                                                    {
                                                        this.state.definitivo == true && (this.state.prospecto.fecha_fin_cedula == undefined || this.state.prospecto.fecha_fin_cedula == "") ?
                                                            <span style={{ color: "red" }}>El campo Fecha fin vigencia de cédula, es un campo requerido</span> : ''
                                                    }
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="col-xs-4 col-lg-4">
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        <span className="input-group-prepend"> <span
                                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Conexión GNP agente definitivo</span>
                                                        </span> <input type="date" placeholder="" name="fecha_conexion_definitiva" max="2100-01-01" disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.fecha_conexion_definitiva} className="form-control " />
                                                    </div>
                                                    {
                                                        this.state.definitivo == true && (this.state.prospecto.fecha_conexion_definitiva == undefined || this.state.prospecto.fecha_conexion_definitiva == "") ?
                                                            <span style={{ color: "red" }}>El campo Conexión GNP agente definitivo, es un campo requerido</span> : ''
                                                    }
                                                </div>
                                            </div>

                                            <div className="col-xs-4 col-lg-4">
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        <span className="input-group-prepend"> <span
                                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Antigüedad del agente</span>
                                                        </span> <input type="number" placeholder="" name="antiguedad_agente" disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.antiguedad_agente} className="form-control " />
                                                    </div>
                                                    {
                                                        this.state.definitivo == true && (this.state.prospecto.antiguedad_agente == undefined) ?
                                                            <span style={{ color: "red" }}>El campo Antigüedad del agente, es un campo requerido</span> : ''
                                                    }
                                                </div>
                                            </div>

                                        </div>

                                        <div className="row">
                                            <div className="col-xs-4 col-lg-4">
                                                <div className="row">
                                                    {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>} <label >Generación de la figura comercial</label>
                                                </div>
                                                <div className="row">
                                                    {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" value={1} disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} id="G1" name="generacion" checked={this.state.prospecto.generacion == 1} />
                                                        <label class="form-check-label" for="G1">
                                                            G1
                                                        </label>
                                                    </div>
                                                    {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" value={2} disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} id="G2" name="generacion" checked={this.state.prospecto.generacion == 2} />
                                                        <label class="form-check-label" for="G2">
                                                            G2
                                                        </label>
                                                    </div>

                                                    {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" value={3} disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} id="G3" name="generacion" checked={this.state.prospecto.generacion == 3} />
                                                        <label class="form-check-label" for="G3">
                                                            G3
                                                        </label>
                                                    </div>
                                                    {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" value={4} disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} id="G4" name="generacion" checked={this.state.prospecto.generacion == 4} />
                                                        <label class="form-check-label" for="G4">
                                                            G4
                                                        </label>
                                                    </div>
                                                    {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" value={5} disabled={this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} id="C" name="generacion" checked={this.state.prospecto.generacion == 5} />
                                                        <label class="form-check-label" for="C">
                                                            C
                                                        </label>
                                                    </div>
                                                </div>
                                                {
                                                    this.state.definitivo == true && (this.state.prospecto.generacion == null || this.state.prospecto.generacion == 0) ?
                                                        <span style={{ color: "red" }}>Generación de la figura comercial, debe de ser seleccionada</span> : ''
                                                }
                                            </div>

                                            <div className="col-xs-8 col-lg-8">
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        <span className="input-group-prepend"> <span
                                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Manejo y consideraciones de Agentes Consolidados</span>
                                                        </span> {this.state.selectAgentesConsolidados}
                                                    </div>
                                                    {
                                                        this.state.definitivo == true && (this.state.prospecto.agente_consolidado_id == 0 || this.state.prospecto.agente_consolidado_id == null) ?
                                                            <span style={{ color: "red" }}>Manejo y consideraciones de Agentes Consolidados, debe de ser seleccionada</span> : ''
                                                    }
                                                </div>
                                            </div>
                                        </div>



                                    </div>
                                </div>
                            </div>
                        }


                        {this.state.definitivo == true &&
                            <div>
                                <div className="row">
                                    <div className="col-xs-2 col-lg-4">
                                        <h7 class="mb-0 font-weight-semibold">Capacitación</h7>
                                    </div>
                                </div>
                                <div class="card">
                                    <div className="card-body">

                                        <div className="row">
                                            <div className="col-xs-4 col-lg-4">
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        <span className="input-group-prepend"> <span
                                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Curso cédula A</span>
                                                        </span> <input type="date" placeholder="" max="2100-01-01" disabled={this.props.tipoUsuario != "ASESJRCAP" && this.props.tipoUsuario != "GTECAP" && this.props.tipoUsuario != "SUPER"} name="fecha_curso_cedula" onChange={this.onChange} value={this.state.prospecto.fecha_curso_cedula} className="form-control " />
                                                    </div>
                                                    {
                                                        this.state.definitivo == true && (this.state.ban_ventaCarrera && (this.state.prospecto.fecha_curso_cedula == undefined || this.state.prospecto.fecha_curso_cedula == "")) ?
                                                            <span style={{ color: "red" }}>El campo Curso cédula A, es un campo requerido</span> : ''
                                                    }
                                                </div>
                                            </div>
                                            <div className="col-xs-4 col-lg-4">
                                                <div className="row">
                                                    <div className="col-xs-8 col-lg-8">
                                                        <h8 class="mb-0 font-weight-semibold">Calificaciones y comprobante de pago de cédula</h8>
                                                    </div>

                                                    <div className="col-xs-2 col-lg-4">
                                                        <div className="custom-control custom-switch  mb-2" >
                                                            <input type="checkbox" class="custom-control-input" id="comprobante_cedula" name="comprobante_cedula" disabled={this.props.tipoUsuario != "ASESJRCAP" && this.props.tipoUsuario != "GTECAP" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospecto.comprobante_cedula}></input>
                                                            <label class="custom-control-label" for="comprobante_cedula"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="col-xs-4 col-lg-4">
                                                <div className="row">
                                                    <div className="col-xs-8 col-lg-8">
                                                        <h8 class="mb-0 font-weight-semibold">Envío GNP multa</h8>
                                                    </div>

                                                    <div className="col-xs-2 col-lg-4">
                                                        <div className="custom-control custom-switch  mb-2" >
                                                            <input type="checkbox" class="custom-control-input" id="envio_gnp_multa" name="envio_gnp_multa" disabled={this.props.tipoUsuario != "ASESJRCAP" && this.props.tipoUsuario != "GTECAP" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospecto.envio_gnp_multa}></input>
                                                            <label class="custom-control-label" for="envio_gnp_multa"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        }


                        <div className="row">
                            <div className="col-xs-2 col-lg-4">
                                <h7 class="mb-0 font-weight-semibold">Datos Capacitación</h7>
                            </div>
                        </div>
                        <div class="card">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-xs-8 col-lg-8">
                                        <div className="row">
                                            <div className="col-xs-8 col-lg-8">
                                                <h8 class="mb-0 font-weight-semibold">Se envió correo electrónico con detalles para el Curso de Inmersión APS al prospecto</h8>
                                            </div>

                                            <div className="col-xs-2 col-lg-4">
                                                <div className="custom-control custom-switch  mb-2" >
                                                    <input type="checkbox" class="custom-control-input" id="curso_inmersion_aps" name="curso_inmersion_aps" disabled={this.props.tipoUsuario != "ASESJRCAP" && this.props.tipoUsuario != "GTECAP" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospecto.curso_inmersion_aps}></input>
                                                    <label class="custom-control-label" for="curso_inmersion_aps"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-xs-2 col-lg-2">
                                        <h6 class="mb-0 font-weight-semibold">Baja</h6>
                                    </div>
                                    <div className="col-xs-4 col-lg-4">
                                        <div className="custom-control custom-switch  mb-2" >
                                            <input type="checkbox" class="custom-control-input" id="baja_conexion" name="baja_conexion" disabled={this.props.tipoUsuario != "ASESJRCAP" && this.props.tipoUsuario != "GTECAP" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospecto.baja_conexion}></input>
                                            <label class="custom-control-label" for="baja_conexion"></label>
                                        </div>
                                    </div>
                                    {
                                        this.state.ban_baja_conexion ?
                                            <div className="col-xs-6 col-lg-6">
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        <span className="input-group-prepend"> <span
                                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Si es el caso, especificar el motivo de baja del Curso de Inmersión APS</span>
                                                        </span>{this.state.selectMotivosBaja}
                                                    </div>
                                                </div>
                                            </div> : ''
                                    }
                                </div>
                                <div className="row">
                                    <div className="col-xs-4 col-lg-4">
                                        <h6 class="mb-0 font-weight-semibold">Reporte de Capacitación</h6>
                                    </div>

                                    <div className="col-xs-4 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <form encType="multipart/form" style={{ display: 'none' }} >
                                                    <input type="file" style={{ display: 'none' }} ref={(ref) => this.capacitacion = ref} onChange={this.onChangeFileCapacitacion.bind(this)}></input>
                                                </form>
                                                <input type="text" className="form-control " disabled={this.props.tipoUsuario != "GDD" && this.props.tipoUsuario != "GTECAP" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} placeholder="Capacitación" value={this.state.nombreArchivoCapacitacion} />
                                                <span className="input-group-prepend">

                                                    {
                                                        this.state.muestraDocCapacitacion == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoCapacitacion }}>
                                                            <a href={this.state.urlCapacitacion} target="_blank" rel="noopener noreferrer" >
                                                                <i className="fas fa-eye"
                                                                    style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                            </a>
                                                        </button> : ''


                                                    }
                                                    <button type="button" class="btn text-black" disabled={this.props.tipoUsuario != "GDD" && this.props.tipoUsuario != "GTECAP" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onClick={this.onClickBotonArchivoCapacitacion} style={{ backgroundColor: this.state.colorBotonSubirArchivoCapacitacion }}>
                                                        <h10 style={{ color: "black" }}>+</h10>
                                                    </button>

                                                    <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >Capacitación</span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-xs-4 col-lg-4">
                                        <div className="row">
                                            <div className="col-xs-2 col-lg-2">
                                                <>&nbsp;&nbsp;&nbsp;&nbsp;</>
                                            </div><div className="col-xs-2 col-lg-2">
                                                <>&nbsp;&nbsp;&nbsp;&nbsp;</>
                                            </div>


                                            <div className="col-xs-2 col-lg-2">
                                                {/*<button class="btn " style={{ borderColor: 'black' }} type="button" disabled = {this.props.tipoUsuario != "ASESJRCAP" && this.props.tipoUsuario != "GTECAP" && this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "GTECAP"   && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "GDD"} ><i className="icon-download"></i></button>*/}
                                                {
                                                    this.state.muestraDocCapacitacion == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorSubirArchivoCapacitacion }}>
                                                        <a href={this.state.urlCapacitacion} target="_blank" rel="noopener noreferrer" >
                                                            <i className="fas fa-eye"
                                                                style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Consultar Reporte Capacitación" ></i>
                                                        </a>
                                                    </button> : <a href="#">
                                                        <i className="fas fa-eye"
                                                            style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Documento no existe" ></i>
                                                    </a>
                                                }
                                            </div>



                                            <div className="col-xs-6 col-lg-6">
                                                <button class="btn " onClick={this.mostrarReporteCapacitacion} type="button" style={{ backgroundColor: '#D5D9E8', color: '#617187' }}
                                                    disabled={this.props.tipoUsuario != "GTEDES" && this.props.tipoUsuario != "GTECAP" && this.props.tipoUsuario != "ASESJRCAP" && this.props.tipoUsuario != "GTECAP" && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"}
                                                >Generar documento</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <HistoricoGeneral idProspecto={this.props.idProspecto} tipoUsuario={this.props.tipoUsuario} nombreUsuario= {this.props.nombreUsuario} presentarSeccion={"EP12"} />
                    </div>
                }

            </div >
        )
    }

}