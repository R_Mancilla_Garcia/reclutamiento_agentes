import React, { Component } from "react";
import HistoricoGeneral from "./HistoricoGeneral";
import axios from 'axios';
import datos from '../urls/datos.json'

export default class EntrevistaProfundaExplorando8 extends Component {

    constructor() {
        super();
        this.onChange = this.onChange.bind(this)
        this.onChangeEdat2 = this.onChangeEdat2.bind(this)
        this.onChangeGDD = this.onChangeGDD.bind(this)
        this.onChangeCapacita = this.onChangeCapacita.bind(this)
        this.onChangeDireccion = this.onChangeDireccion.bind(this)
        this.onClickBotonArchivoPuesto = this.onClickBotonArchivoPuesto.bind(this)
        this.onClickBotonArchivoPSP = this.onClickBotonArchivoPSP.bind(this)
        this.functionValidaNumber = this.functionValidaNumber.bind(this)
        this.validaFactoresVitales = this.validaFactoresVitales.bind(this)
        this.cargaInfoPSP = this.cargaInfoPSP.bind(this)
        this.cargaPdf14 = this.cargaPdf14.bind(this)

        this.state = {
            entrevistador:'',
            prospecto:
            {
                "id": null,
                "prospecto_id": null,
                "estatus_id": 12,
                "orientacion_logro_edat": null,
                "orientacion_logro_edat2": null,
                "orientacion_logro_gdd": null,
                "orientacion_logro_capacita": null,
                "orientacion_logro_direccion": null,
                "perceverancia_edat": null,
                "perceverancia_edat2": null,
                "perceverancia_gdd": null,
                "perceverancia_capacita": null,
                "perceverancia_direccion": null,
                "integridad_edat": null,
                "integridad_edat2": null,
                "integridad_gdd": null,
                "integridad_capacita": null,
                "integridad_direccion": null,
                "sentido_comun_edat": null,
                "sentido_comun_edat2": null,
                "sentido_comun_gdd": null,
                "sentido_comun_capacita": null,
                "sentido_comun_direccion": null,
                "energia_edat": null,
                "energia_edat2": null,
                "energia_gdd": null,
                "energia_capacita": null,
                "energia_direccion": null,
                "motivacion_edat": null,
                "motivacion_edat2": null,
                "motivacion_gdd": null,
                "motivacion_capacita": null,
                "motivacion_direccion": null,
                "total_edat": null,
                "total_edat2": null,
                "total_gdd": null,
                "total_capacita": null,
                "total_direccion": null
            },
            muestraInfoPSP: false,
            archivoPSP: undefined,
            muestraDocPSP: false,
            colorSubirArchivoPSP: "#E1E5F0",
            colorTextoSubirArchivoPSP: "#617187",
            colorBotonSubirArchivoPSP: "#313A46",
            nombreArchivoPSP: "",
            archivoPSP: undefined,
            textoPuesto: 'Subir archivo',

        };
    }

    UNSAFE_componentWillMount() {
        console.log("entrando es render de explorando ", this.props)
        fetch(datos.urlServicePy+"recluta/api_recluta_vitales08/" + this.props.idProspecto)
            .then(response => response.json())
            .then(jsonData => {
                if (jsonData.length > 0) {
                    this.setState({ prospecto: jsonData[0] })
                }
                fetch(datos.urlServicePy+"parametros/api_recluta_prospectos/" + this.props.idProspecto)
                .then(response => response.json())
                .then(recluta_prospecto => {
                    console.log("recluta_propspecto", recluta_prospecto)
                    fetch(datos.urlServicePy+"parametros/api_cat_empleados/" + recluta_prospecto.filas[0].fila[12].value)
                    .then(response => response.json())
                    .then(entrevistador => {
                            console.log("entrevistador ", entrevistador)
                            this.setState({entrevistador:entrevistador.filas[0].fila[2].value + " "+ entrevistador.filas[0].fila[3].value + " " + (entrevistador.filas[0].fila[4].value != null ? entrevistador.filas[0].fila[4].value:'') })
                    })
                })
            })
        fetch(datos.urlServicePy+"recluta/api_recluta_vcargapsppdf14/" + this.props.idProspecto)
            .then(response => response.json())
            .then(jsonRespuestaCarga => {
                console.log("respuesta de la carga ", jsonRespuestaCarga)

                if (jsonRespuestaCarga.length > 0) {
                    let interpretacionConcatenada = jsonRespuestaCarga[6] === undefined ? "" : jsonRespuestaCarga[6].respuesta
                    this.setState({
                        muestraInfoPSP: true,
                        rutaPSP: jsonRespuestaCarga[0].ironpsp,
                        fechaRespPSP: jsonRespuestaCarga[0].respuesta,
                        eficaciaVentas: jsonRespuestaCarga[1].porcentaje,
                        eficaciaEmpresarial: jsonRespuestaCarga[2].porcentaje,
                        rendimientoVentas: jsonRespuestaCarga[3].porcentaje,
                        ventaDominante: jsonRespuestaCarga[4].respuesta,
                        interpretacion: jsonRespuestaCarga[5].respuesta + " " + interpretacionConcatenada,
                        muestraDocPSP: true,
                        colorSubirArchivoPSP: "#78CB5A", colorTextoSubirArchivoPSP: "#FFFFFF", colorBotonSubirArchivoPSP: "#78CB5A"
                    })
                }


            })

    }

    functionValidaNumber(entero){
        if(Number.isInteger(entero)) {
            return true
          }
        return false
    }

    validaFactoresVitales(banFV){
        let prospecto = this.state.prospecto
        //EDAT
        console.log("El valor es :::"+this.functionValidaNumber(prospecto.orientacion_logro_edat));
        prospecto.orientacion_logro_edat = prospecto.orientacion_logro_edat == "" || !this.functionValidaNumber(prospecto.orientacion_logro_edat) ? null : prospecto.orientacion_logro_edat
        prospecto.perceverancia_edat = prospecto.perceverancia_edat == "" || !this.functionValidaNumber(prospecto.perceverancia_edat) ? null : prospecto.perceverancia_edat
        prospecto.integridad_edat = prospecto.integridad_edat == "" || !this.functionValidaNumber(prospecto.integridad_edat) ? null : prospecto.integridad_edat
        prospecto.sentido_comun_edat = prospecto.sentido_comun_edat == "" || !this.functionValidaNumber(prospecto.sentido_comun_edat) ? null : prospecto.sentido_comun_edat
        prospecto.energia_edat = prospecto.energia_edat == "" || !this.functionValidaNumber(prospecto.energia_edat) ? null : prospecto.energia_edat
        prospecto.motivacion_edat = prospecto.motivacion_edat == "" || !this.functionValidaNumber(prospecto.motivacion_edat) ? null : prospecto.motivacion_edat

        //GERENTE EDAT
        prospecto.orientacion_logro_edat2 = prospecto.orientacion_logro_edat2 == "" || !this.functionValidaNumber(prospecto.orientacion_logro_edat2) ? null : prospecto.orientacion_logro_edat2
        prospecto.perceverancia_edat2 = prospecto.perceverancia_edat2 == "" || !this.functionValidaNumber(prospecto.perceverancia_edat2) ? null : prospecto.perceverancia_edat2
        prospecto.integridad_edat2 = prospecto.integridad_edat2 == "" || !this.functionValidaNumber(prospecto.integridad_edat2) ? null : prospecto.integridad_edat2
        prospecto.sentido_comun_edat2 = prospecto.sentido_comun_edat2 == "" || !this.functionValidaNumber(prospecto.sentido_comun_edat2) ? null : prospecto.sentido_comun_edat2
        prospecto.energia_edat2 = prospecto.energia_edat2 == "" || !this.functionValidaNumber(prospecto.energia_edat2) ? null : prospecto.energia_edat2
        prospecto.motivacion_edat2 = prospecto.motivacion_edat2 == "" || !this.functionValidaNumber(prospecto.motivacion_edat2) ? null : prospecto.motivacion_edat2

        //GDD
        prospecto.orientacion_logro_gdd = prospecto.orientacion_logro_gdd == "" || !this.functionValidaNumber(prospecto.orientacion_logro_gdd) ? null : prospecto.orientacion_logro_gdd
        prospecto.perceverancia_gdd = prospecto.perceverancia_gdd == "" || !this.functionValidaNumber(prospecto.perceverancia_gdd) ? null : prospecto.perceverancia_gdd
        prospecto.integridad_gdd = prospecto.integridad_gdd == "" || !this.functionValidaNumber(prospecto.integridad_gdd) ? null : prospecto.integridad_gdd
        prospecto.sentido_comun_gdd = prospecto.sentido_comun_gdd == "" || !this.functionValidaNumber(prospecto.sentido_comun_gdd) ? null : prospecto.sentido_comun_gdd
        prospecto.energia_gdd = prospecto.energia_gdd == "" || !this.functionValidaNumber(prospecto.energia_gdd) ? null : prospecto.energia_gdd
        prospecto.motivacion_gdd = prospecto.motivacion_gdd == "" || !this.functionValidaNumber(prospecto.motivacion_gdd) ? null : prospecto.motivacion_gdd

        //CAPACITA
        prospecto.orientacion_logro_capacita = prospecto.orientacion_logro_capacita == "" || !this.functionValidaNumber(prospecto.orientacion_logro_capacita) ? null : prospecto.orientacion_logro_capacita
        prospecto.perceverancia_capacita = prospecto.perceverancia_capacita == "" || !this.functionValidaNumber(prospecto.perceverancia_capacita) ? null : prospecto.perceverancia_capacita
        prospecto.integridad_capacita = prospecto.integridad_capacita == "" || !this.functionValidaNumber(prospecto.integridad_capacita) ? null : prospecto.integridad_capacita
        prospecto.sentido_comun_capacita = prospecto.sentido_comun_capacita == "" || !this.functionValidaNumber(prospecto.sentido_comun_capacita) ? null : prospecto.sentido_comun_capacita
        prospecto.energia_capacita = prospecto.energia_capacita == "" || !this.functionValidaNumber(prospecto.energia_capacita) ? null : prospecto.energia_capacita
        prospecto.motivacion_capacita = prospecto.motivacion_capacita == "" || !this.functionValidaNumber(prospecto.motivacion_capacita) ? null : prospecto.motivacion_capacita

        //SUB DIR DESARROLL
        prospecto.orientacion_logro_direccion = prospecto.orientacion_logro_direccion == "" || !this.functionValidaNumber(prospecto.orientacion_logro_direccion) ? null : prospecto.orientacion_logro_direccion
        prospecto.perceverancia_direccion = prospecto.perceverancia_direccion == "" || !this.functionValidaNumber(prospecto.perceverancia_direccion) ? null : prospecto.perceverancia_direccion
        prospecto.integridad_direccion = prospecto.integridad_direccion == "" || !this.functionValidaNumber(prospecto.integridad_direccion) ? null : prospecto.integridad_direccion
        prospecto.sentido_comun_direccion = prospecto.sentido_comun_direccion == "" || !this.functionValidaNumber(prospecto.sentido_comun_direccion) ? null : prospecto.sentido_comun_direccion
        prospecto.energia_direccion = prospecto.energia_direccion == "" ? null  || !this.functionValidaNumber(prospecto.energia_direccion): prospecto.energia_direccion
        prospecto.motivacion_direccion = prospecto.motivacion_direccion == "" || !this.functionValidaNumber(prospecto.motivacion_direccion) ? null : prospecto.motivacion_direccion
                
        if(banFV){
            this.props.muestraForm9Explorando(prospecto)
        }else{
            this.props.regresaForm7explorando()
        }
    }

    onChange = e => {
        console.log(e.target.value)
        let pros = this.state.prospecto
        if (e.target.value >= 1 && e.target.value <= 10) {
            pros["" + e.target.name + ""] = parseInt(e.target.value)
            pros.total_edat = parseInt(pros.orientacion_logro_edat) + parseInt(pros.perceverancia_edat) + parseInt(pros.integridad_edat) + parseInt(pros.sentido_comun_edat) + parseInt(pros.energia_edat) + parseInt(pros.motivacion_edat)
            this.setState({ prospecto: pros })
        } else {
            if(e.target.value.length <= 1){
                pros["" + e.target.name + ""] = ""
            }
        }
        this.setState({ prospecto: pros })
    }

    onChangeEdat2 = e => {
        console.log(e.target.value)
        let pros = this.state.prospecto
        if (e.target.value >= 1 && e.target.value <= 10) {
            pros["" + e.target.name + ""] = parseInt(e.target.value)
            pros.total_edat2 = parseInt(pros.orientacion_logro_edat2) + parseInt(pros.perceverancia_edat2) + parseInt(pros.integridad_edat2) + parseInt(pros.sentido_comun_edat2) + parseInt(pros.energia_edat2) + parseInt(pros.motivacion_edat2)
        } else {
            if(e.target.value.length <= 1){
                pros["" + e.target.name + ""] = ""
            }
        }
        this.setState({ prospecto: pros })
    }

    onChangeGDD = e => {
        console.log(e.target.value)
        let pros = this.state.prospecto
        if (e.target.value >= 1 && e.target.value <= 10) {
            pros["" + e.target.name + ""] = parseInt(e.target.value)
            pros.total_gdd = parseInt(pros.orientacion_logro_gdd) + parseInt(pros.perceverancia_gdd) + parseInt(pros.integridad_gdd) + parseInt(pros.sentido_comun_gdd) + parseInt(pros.energia_gdd) + parseInt(pros.motivacion_gdd)
        } else {
            if(e.target.value.length <= 1){
                pros["" + e.target.name + ""] = ""
            }
        }
        this.setState({ prospecto: pros })
    }

    onChangeCapacita = e => {
        console.log(e.target.value)
        let pros = this.state.prospecto
        if (e.target.value >= 1 && e.target.value <= 10) {
            pros["" + e.target.name + ""] = parseInt(e.target.value)
            pros.total_capacita = parseInt(pros.orientacion_logro_capacita) + parseInt(pros.perceverancia_capacita) + parseInt(pros.integridad_capacita) + parseInt(pros.sentido_comun_capacita) + parseInt(pros.energia_capacita) + parseInt(pros.motivacion_capacita)
        } else {
            if(e.target.value.length <= 1){
                pros["" + e.target.name + ""] = ""
            }
        }
        this.setState({ prospecto: pros })
    }

    onChangeDireccion = e => {
        console.log(e.target.value)
        let pros = this.state.prospecto
        if (e.target.value >= 1 && e.target.value <= 10) {
            pros["" + e.target.name + ""] = parseInt(e.target.value)
            pros.total_direccion = parseInt(pros.orientacion_logro_direccion) + parseInt(pros.perceverancia_direccion) + parseInt(pros.integridad_direccion) + parseInt(pros.sentido_comun_direccion) + parseInt(pros.energia_direccion) + parseInt(pros.motivacion_direccion)
        } else {
            if(e.target.value.length <= 1){
                pros["" + e.target.name + ""] = ""
            }
        }
        this.setState({ prospecto: pros })
    }


    onChangeFilePuesto(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file de puesto", file);
        this.setState({ archivoPuesto: file, colorSubirArchivo: "#78CB5A", colorTextoSubirArchivo: "#FFFFFF", colorBotonSubirArchivo: "#E5F6E0", nombreArchivoPuesto: file.name })
        // this.props.guardaArchivo(file, "archivoPuesto")

    }


    onClickBotonArchivoPuesto() {
        console.log("entrnaod a la imagen")
        this.upload.click()
    }



    onClickBotonArchivoPSP() {
        console.log("entrnaod a la imagen")
        this.upload.click()
    }


    cargaInfoPSP(json) {
        const requestOptions = {
            method: "POST",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };

        fetch(datos.urlServicePy+"recluta/api_recluta_cargapsppdf/0", requestOptions)
        .then(response => response.json())
        .then(data => {
            console.log("la respuesta de la carga", data)
            this.cargaPdf14()
        });
    }

    cargaPdf14(){
        fetch(datos.urlServicePy+"recluta/api_recluta_vcargapsppdf14/" + this.props.idProspecto)
        .then(response => response.json())
        .then(jsonRespuestaCarga => {
            console.log("respuesta de la carga ", jsonRespuestaCarga)
            let interpretacionConcatenada=jsonRespuestaCarga[6] === undefined ?  "":jsonRespuestaCarga[6].respuesta

            this.setState({
                muestraInfoPSP: true,
                fechaRespPSP: jsonRespuestaCarga[0].respuesta,
                eficaciaVentas: jsonRespuestaCarga[1].porcentaje,
                eficaciaEmpresarial: jsonRespuestaCarga[2].porcentaje,
                rendimientoVentas: jsonRespuestaCarga[3].porcentaje,
                ventaDominante:jsonRespuestaCarga[4].respuesta,
                interpretacion:jsonRespuestaCarga[5].respuesta + " "+interpretacionConcatenada,
            })
        })
    }


    onChangeFilePSP(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file de puesto", file);

        //lanzamos el envio del psp 
        let formData = new FormData();
        const config = {
            headers: { 'content-type': 'multipart/form-data' }
        }
        
        if(this.state.muestraDocPSP == true){
            formData.append('ironpsp', file);
            axios.put(datos.urlServicePy+"api/doc_ironpsp/"+this.props.idProspecto+"/", formData, config)
            .then(res => {
                console.log("respuesta  del archivo cargado", res.data);
                this.cargaInfoPSP(res.data)
                let ruta = res.data.ironpsp
                let rutaCorrecta = ruta.replace("8000/media", "8001/documentos")
                rutaCorrecta = rutaCorrecta.replace("http", "https")
                console.log(rutaCorrecta)
                this.setState({ rutaPSP: rutaCorrecta, archivoPSP: file, colorSubirArchivoPSP: "#78CB5A", colorTextoSubirArchivoPSP: "#FFFFFF", colorBotonSubirArchivoPSP: "#78CB5A", nombreArchivoPSP: file.name, muestraDocPSP: true })
            })
        } else {
            formData.append('id', this.props.idProspecto);
            formData.append('ironpsp', file);
            formData.append('prospecto_id', this.props.idProspecto);
            axios.post(datos.urlServicePy+"api/doc_ironpsp/", formData, config)
            .then(res => {
                console.log("respuesta  del archivo cargado", res.data);
                this.cargaInfoPSP(res.data)
                let ruta = res.data.ironpsp
                let rutaCorrecta = ruta.replace("8000/media", "8001/documentos")
                rutaCorrecta = rutaCorrecta.replace("http", "https")
                console.log(rutaCorrecta)
                this.setState({ rutaPSP: rutaCorrecta, archivoPSP: file, colorSubirArchivoPSP: "#78CB5A", colorTextoSubirArchivoPSP: "#FFFFFF", colorBotonSubirArchivoPSP: "#78CB5A", nombreArchivoPSP: file.name, muestraDocPSP: true })
            })
        }
        
    }


    render() {
        return (
            <div>
                <button type="button" style={{ display: 'none' }} ref={(ref) => this.modalInicio = ref} data-toggle="modal" data-target="#modal-Continuar"   ></button>

                <div id="modal-psp" className="modal fade" tabindex="-1">
                    <div className="modal-dialog modal-full">
                        <div className="modal-content text-white" style={{ backgroundColor: "#313A46" }}>
                            <div className="modal-header text-white" style={{ backgroundColor: "#8189D4" }}>
                                <h6 className="modal-title">Documento PSP </h6>
                                <button type="button" className="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div className="modal-body">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-xs-12 col-lg-4"><>&nbsp;&nbsp;</></div>
                                        <div className="col-xs-12 col-lg-4">
                                            <div className="input-group">
                                                <form encType="multipart/form" style={{ display: 'none' }} >
                                                    <input type="file" style={{ display: 'none' }} ref={(ref) => this.upload = ref} onChange={this.onChangeFilePSP.bind(this)}></input>
                                                </form>
                                                <input type="text" style={{ borderColor: 'black', backgroundColor: "#313A46", color: 'white' }} className="form-control " placeholder="Archivo PSP" value={this.state.nombreArchivoPSP} />
                                                <span className="input-group-prepend" style={{ backgroundColor: this.state.colorBotonSubirArchivoPSP }}>
                                                    {/*<button type="button" class="btn text-white" onClick={this.onClickBotonArchivoPSP} style={{ backgroundColor: this.state.colorBotonSubirArchivoPSP }}>
                                                        <h10 style={{ color: "white" }}>+</h10>
                                                         </button>*/}

                                                    {
                                                                this.state.muestraDocPSP == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoPSP }}>
                                                                <a href={this.state.rutaPSP} target="_blank" rel="noopener noreferrer" >
                                                                    <i className="fas fa-eye"
                                                                        style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h10 onClick={this.onClickBotonArchivoPSP} style={{ color: "white" }}>+</h10>
                                                            </button> : <button type="button" class="btn text-white" onClick={this.onClickBotonArchivoPSP} style={{ backgroundColor: this.state.colorBotonSubirArchivoPSP }}>
                                                                <h10 style={{ color: "white" }}>+</h10>
                                                            </button>

                                                    }



                                                    <span className="input-group-text" style={{ background: this.state.colorBotonSubirArchivoPSP, borderColor: this.state.colorBotonSubirArchivoPSP, color: "white" }} >{this.state.textoPuesto}</span>
                                                </span>
                                            </div>
                                        </div>
                                        <div className="col-xs-12 col-lg-4"><>&nbsp;&nbsp;</></div>
                                    </div>

                                    <br></br>
                                    <br></br>
                                    {this.state.muestraInfoPSP == true &&
                                        <div>
                                            <div className="row">
                                                <div className="col-xs-6 col-lg-6">
                                                    <h8 class="mb-0 font-weight-semibold text-white"> Resultados PSP</h8>
                                                </div>
                                                <div className="col-xs-4 col-lg-4">
                                                    <table className="table datatable-sorting  table-striped table-hover" style={{ backgroundColor: "#313A46", borderColor: '#313A46' }}>
                                                        <thead style={{ backgroundColor: "#313A46" }}>
                                                            <tr>
                                                                <th className="text-left font-weight-bold text-white" style={{ backgroundColor: '#313A46' }} >
                                                                    <a>ALTO</a>
                                                                </th>
                                                                <th className="text-center font-weight-bold text-white" style={{ backgroundColor: '#313A46' }} >
                                                                    <a>MODERADO</a>
                                                                </th>
                                                                <th className="text-right font-weight-bold text-white" style={{ backgroundColor: '#313A46' }} >
                                                                    <a>BAJO</a>
                                                                </th>
                                                            </tr>
                                                        </thead>

                                                    </table>
                                                </div>
                                            </div>

                                            <div className="row">
                                                <div className="col-xs-4 col-lg-4">
                                                    <div className="form-group">
                                                        <div className="input-group">
                                                            <span className="input-group-prepend"> <span
                                                                className="input-group-text text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }}>Fecha de respuesta del PSP</span>
                                                            </span> <input type="text" placeholder="" value={this.state.fechaRespPSP} className="form-control text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }} />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-xs-2 col-lg-2">
                                                    <h8 class=" text-white"> Eficacia en ventas</h8>
                                                </div>

                                                <div className="col-xs-4 col-lg-4">

                                                    <input type="range" class="custom-range" min="0" name="salud" max="100" step={50} value={this.state.eficaciaVentas} id="salud" />
                                                </div>


                                                <div className="col-xs-2 col-lg-2">




                                                    {
                                                        this.state.eficaciaVentas == 0 &&
                                                        <div class="mr-3 ">
                                                            <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                                    class="letter-icon text-white"></span>
                                                            </a>
                                                        </div>

                                                    }

                                                    {
                                                        this.state.eficaciaVentas == 50 &&
                                                        <div className="row">

                                                            {/*<div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                             </div>*/}

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                        </div>
                                                    }


                                                    {
                                                        this.state.eficaciaVentas == 100 &&
                                                        <div className="row">

                                                            {/*<div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>*/}

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FD0303', borderColor: '#FD0303' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                        </div>
                                                    }





                                                </div>


                                            </div>

                                            <div className="row">

                                                <div className="col-xs-12 col-lg-4">
                                                    <div className="form-group">
                                                        <div className="input-group">
                                                            <span className="input-group-prepend"> <span
                                                                className="input-group-text text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }}>Estilo de venta dominante&ensp;</span>
                                                            </span> <input type="text" placeholder="" value={this.state.ventaDominante} className="form-control text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }} />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-xs-12 col-lg-2">
                                                    <h8 class=" text-white"> Eficacia empresarial</h8>
                                                </div>

                                                <div className="col-xs-12 col-lg-4">
                                                    <input type="range" class="custom-range" min="0" name="salud" max="100" step={50} value={this.state.eficaciaEmpresarial} id="salud" />
                                                </div>


                                                <div className="col-xs-2 col-lg-2">

                                                    {
                                                        this.state.eficaciaEmpresarial == 0 &&
                                                        <div class="mr-3 ">
                                                            <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                                    class="letter-icon text-white"></span>
                                                            </a>
                                                        </div>

                                                    }

                                                    {
                                                        this.state.eficaciaEmpresarial == 50 &&
                                                        <div className="row">

                                                            {/*<div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>*/}

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                        </div>
                                                    }


                                                    {
                                                        this.state.eficaciaEmpresarial == 100 &&
                                                        <div className="row">

                                                            {/* <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>*/}

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FD0303', borderColor: '#FD0303' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                        </div>
                                                    }



                                                </div>



                                            </div>

                                            <div className="row">
                                                <div className="col-xs-12 col-lg-4">
                                                    <div className="form-group">
                                                        <div className="input-group">
                                                            <span className="input-group-prepend"> <span
                                                                className="input-group-text text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }}>Interpretación de precisión</span>
                                                            </span> <textarea disabled={true} type="text" placeholder="" value={this.state.interpretacion} className="form-control text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }} />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-xs-12 col-lg-2">
                                                    <h8 class=" text-white">Rendimiento de ventas general esperado</h8>
                                                </div>

                                                <div className="col-xs-12 col-lg-4">
                                                    <input type="range" class="custom-range" min="0" name="salud" max="100" step={50} value={this.state.rendimientoVentas} id="salud" />
                                                </div>

                                                <div className="col-xs-2 col-lg-2">

                                                    {
                                                        this.state.rendimientoVentas == 0 &&
                                                        <div class="mr-3 ">
                                                            <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                                    class="letter-icon text-white"></span>
                                                            </a>
                                                        </div>

                                                    }

                                                    {
                                                        this.state.rendimientoVentas == 50 &&
                                                        <div className="row">

                                                            {/*<div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>*/}

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                        </div>
                                                    }


                                                    {
                                                        this.state.rendimientoVentas == 100 &&
                                                        <div className="row">

                                                            {/* <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>*/}

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FD0303', borderColor: '#FD0303' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                        </div>
                                                    }



                                                </div>


                                            </div>
                                        </div>

                                    }







                                </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-center">
                                <button type="button" class="btn text-white" data-dismiss="modal" style={{ backgroundColor: '#617187', borderColor: '#617187' }}>Cancelar</button>

                                {
                                    this.state.muestraDocPSP == true ? <button type="button" class="btn text-white " disabled={false} style={{ backgroundColor: '#617187', borderColor: '#617187' }}>
                                        <a href={this.state.rutaPSP} style={{ color: 'white' }} target="_blank" rel="noopener noreferrer" >
                                            Ver PSP
                                        </a>
                                    </button> : <button type="button" class="btn text-white " disabled={true} style={{ backgroundColor: '#617187', borderColor: '#617187' }}>
                                        <a href="#" style={{ color: 'white' }} target="_blank" rel="noopener noreferrer" >
                                            Ver PSP
                                        </a>

                                    </button>
                                }

                                <button type="button" class="btn text-white" data-dismiss="modal" style={{ backgroundColor: '#617187', borderColor: '#617187' }}>Aceptar</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="modal-Continuar" className="modal fade" tabindex="-1">
                    <div className="modal-dialog ">
                        <div className="modal-content text-white" style={{ backgroundColor: "#FFFFF" }}>
                            <div className="modal-header text-white text-center" style={{ backgroundColor: "#FFFFF" }}>
                            </div>
                            <div className="modal-body">
                                <div className="card-body">

                                    <h6 className="col-12 text-center text-dark font-weight-semibold">Desea continuar con la prueba?</h6>

                                </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-center">
                                <div className="col-12">
                                    <div className="row">
                                        <button type="button" style={{ width: '100%', backgroundColor: '#8189D4' }}
                                            class="btn text-white" onClick={(e) => console.log("")} data-dismiss="modal"  >Sí, continuar </button>
                                    </div>
                                </div>

                                <br></br>
                                <br></br>
                                <br></br>
                                <div className="col-12">
                                    <div className="row">
                                        <div className="col-6">
                                            <button type="button" style={{ width: '100%', backgroundColor: "#FFFF", borderColor: 'red', textEmphasisColor: 'red' }} class="btn text-red" onClick={(e) => console.log("")} data-dismiss="modal"  ><span style={{ color: 'red' }}>Enviar a reserva</span></button>
                                        </div>

                                        <div className="col-6">
                                            <button type="button" style={{ width: '100%', backgroundColor: "#FFFF", borderColor: '#617187' }} class="btn text-white" onClick={(e) => console.log("")} data-dismiss="modal"><span style={{ color: '#617187' }}>Dar de baja</span></button>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>
                </div>

                <div class="card">

                    <div class="row mb-2 mt-2 ml-1">
                        <div class="col-12 col-lg-9">
                            <div class="row">

                                <div class="col-6 col-lg-1 d-flex align-items-center pb-sm-1">
                                    <div class="mr-2">
                                        <a href="#"
                                            class="btn rounded-pill btn-icon btn-sm" style={{ backgroundColor: '#78CB5A' }}> <span
                                                class="letter-icon text-white"><i className="icon-checkmark2"></i></span>
                                        </a>
                                    </div>
                                </div>

                                <div class="col-6 col-lg-1 d-flex align-items-center pb-sm-1">
                                    <div class="mr-2">
                                        <a href="#"
                                            class="btn  rounded-pill btn-icon btn-sm " style={{ backgroundColor: '#8189D4', borderColor: '#8189D4' }}> <span style={{ color: 'white' }}
                                                class="letter-icon">EP</span>
                                        </a>
                                    </div>

                                </div>



                                <div class="col-6 col-lg-7 d-flex align-items-center pb-sm-1">

                                    <div class="mr-5 ">
                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8189D4', borderColor: '#8189D4' }}
                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                class="letter-icon text-white">1</span>
                                        </a>
                                    </div>

                                    <div class="mr-5 ">
                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8189D4', borderColor: '#8189D4' }}
                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                class="letter-icon text-white">2</span>
                                        </a>
                                    </div>

                                    <div class="mr-5 ">
                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8189D4', borderColor: '#8189D4' }}
                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                class="letter-icon text-white">3</span>
                                        </a>
                                    </div>

                                    <div class="mr-5 ">
                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8189D4', borderColor: '#8189D4' }}
                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                class="letter-icon text-white">4</span>
                                        </a>
                                    </div>

                                    <div class="mr-5 ">
                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8189D4', borderColor: '#8189D4' }}
                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                class="letter-icon text-white">5</span>
                                        </a>
                                    </div>

                                    <div class="mr-5 ">
                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8189D4', borderColor: '#8189D4' }}
                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                class="letter-icon text-white">6</span>
                                        </a>
                                    </div>

                                    <div class="mr-5 ">
                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8189D4', borderColor: '#8189D4' }}
                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                class="letter-icon text-white">7</span>
                                        </a>
                                    </div>

                                    <div class="mr-5 ">
                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8189D4', borderColor: '#8189D4' }}
                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                class="letter-icon text-white">8</span>
                                        </a>
                                    </div>
                                    <div class="mr-5">
                                        <a href="#" style={{ height: '12px', width: '12px', borderColor: '#C4D3E8' }}
                                            class="btn rounded-pill btn-icon btn-sm "> <span
                                                class="letter-icon"></span>
                                        </a>
                                    </div>

                                    <div class="mr-5">
                                        <a href="#" style={{ height: '12px', width: '12px', borderColor: '#C4D3E8' }}
                                            class="btn rounded-pill btn-icon btn-sm "> <span
                                                class="letter-icon"></span>
                                        </a>
                                    </div>





                                </div>

                                &ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;
                                <div class="d-flex justify-content-between">
                                    <div class="mr-2">
                                        <a href="#"
                                            class="btn  rounded-pill btn-icon btn-sm" style={{ borderColor: '#5B96E2' }}> <span style={{ color: '#5B96E2' }}
                                                class="letter-icon">VC</span>
                                        </a>
                                    </div>

                                </div>
                                <div class="d-flex justify-content-between">
                                    <div class="mr-2">
                                        <a href="#"
                                            class="btn  rounded-pill btn-icon btn-sm" style={{ borderColor: '#41B3D1' }}> <span style={{ color: '#41B3D1' }}
                                                class="letter-icon">CC</span>
                                        </a>
                                    </div>

                                </div>
                                <div class="d-flex justify-content-between">
                                    <div class="mr-2">
                                        <a href="#"
                                            class="btn  rounded-pill btn-icon btn-sm" style={{ borderColor: '#78CB5A' }}> <span style={{ color: '#78CB5A' }}
                                                class="letter-icon">C</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-12 col-lg-3">
                            <div class="float-right mr-3">
                                <button type="button" title="Guardar y Regresar" onClick={(e) => this.validaFactoresVitales(false)} class="btn rounded-pill mr-1" style={{ backgroundColor: '#8F9EB3', borderColor: '#8F9EB3' }}><span><i style={{ color: 'white' }} className="icon-backward2"></i></span></button>
                                <button type="button" title="Registrar entrevista" onClick={(e) => this.validaFactoresVitales(true)} disabled={
                                    this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                    && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER" ||
                                    this.props.tipoUsuario == 'GTEDR' && (this.state.prospecto.orientacion_logro_edat2 == 0 || this.state.prospecto.orientacion_logro_edat2 == 1 || this.state.prospecto.orientacion_logro_edat2 == "") ||
                                    this.props.tipoUsuario == 'GTEDR' && (this.state.prospecto.perceverancia_edat2 == 0 || this.state.prospecto.perceverancia_edat2 == 1 || this.state.prospecto.perceverancia_edat2 == "") ||
                                    this.props.tipoUsuario == 'GTEDR' && (this.state.prospecto.integridad_edat2 == 0 || this.state.prospecto.integridad_edat2 == 1 || this.state.prospecto.integridad_edat2 == "") ||
                                    this.props.tipoUsuario == 'GTEDR' && (this.state.prospecto.sentido_comun_edat2 == 0 || this.state.prospecto.sentido_comun_edat2 == 1 || this.state.prospecto.sentido_comun_edat2 == "") ||
                                    this.props.tipoUsuario == 'GTEDR' && (this.state.prospecto.energia_edat2 == 0 || this.state.prospecto.energia_edat2 == 1 || this.state.prospecto.energia_edat2 == "") ||
                                    this.props.tipoUsuario == 'GTEDR' && (this.state.prospecto.motivacion_edat2 == 0 || this.state.prospecto.motivacion_edat2 == 1)
                                } class="btn rounded-pill mr-1" style={{ backgroundColor: '#8F9EB3', borderColor: '#8F9EB3' }}><span><i style={{ color: 'white' }} className="icon-forward3"></i></span></button>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="card-header bg-transparent header-elements-sm-inline">
                    <h3 class="mb-0 font-weight-semibold" style={{ color: '#617187' }}>Entrevista Profunda - PSP & IRON TALENT </h3>
                    <div class="header-elements">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Prospecto: <strong >{this.props.referido.nombre + ' ' + this.props.referido.ape_paterno + ' ' + (this.props.referido.ape_materno != null ? this.props.referido.ape_materno : '')}</strong></li>
                            <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Fecha de alta: <strong >{this.props.referido.ts_alta_audit}</strong></li>

                            <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Usuario creador: <strong >{this.state.entrevistador}</strong></li>
                            <li class="list-inline-item">
                                <h6 style={{ backgroundColor: "#8189D4", color: '#FFFF', borderRadius: '8px' }}> <>&nbsp;&nbsp;</> Entevista Profunda  <>&nbsp;&nbsp;</> </h6>
                            </li>
                        </ul>
                    </div>
                </div>
                <br></br>



                <div class="card-header bg-transparent header-elements-sm-inline" style={{ border: 'none' }}>
                    <h4 class="mb-0 font-weight-semibold" >IronTalent 2022</h4>
                    <div class="header-elements">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item">
                                {this.state.muestraInfoPSP == false &&
                                    <button data-toggle="modal" data-target="#modal-psp"
                                        disabled={this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} class="btn  mr-1" style={{ backgroundColor: "#8189D4", color: '#FFFF' }}> <>&nbsp;&nbsp;</> Subir archivo PSP*  <>&nbsp;&nbsp;</> </button>
                                }
                                {this.state.muestraInfoPSP == true &&
                                    <button data-toggle="modal" data-target="#modal-psp"
                                        disabled={this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} class="btn  mr-1" style={{ backgroundColor: "#78CB5A", color: '#FFFF' }}> <>&nbsp;&nbsp;</> Ver archivo PSP*  <>&nbsp;&nbsp;</> </button>
                                }
                            </li>
                        </ul>
                    </div>
                </div>

                <br></br>

                <div class="card">
                    <div class="card-header bg-transparent header-elements-sm-inline">
                        <h6 class="mb-0 font-weight-semibold">08. Factores vitales</h6>
                        <div class="header-elements">
                            <ul class="list-inline mb-0">
                                <li class="list-inline-item">
                                    <h6 class="mb-0 font-weight-semibold"> <>&nbsp;&nbsp;</> 08/10  <>&nbsp;&nbsp;</> </h6>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>



                <div class="card bg-transparent" style={{ border: 'none' }}>
                    <div className="card-body bg-transparent" style={{ border: 'none' }}>
                        <div className="row">
                            <div className="col-xs-2 col-lg-2">
                                <h8 class="mb-0 font-weight-semibold"><>&nbsp;&nbsp;</> <>&nbsp;&nbsp;</></h8>
                            </div>

                            <div className="col-xs-2 col-lg-2">
                                <span class="font-weight-semibold">EDAT</span>
                            </div>

                            <div className="col-xs-2 col-lg-2">
                                <span class="font-weight-semibold">GERENTE EDAT</span>
                            </div>
                            <div className="col-xs-2 col-lg-2">
                                <span class="font-weight-semibold">GDD</span>
                            </div>
                            <div className="col-xs-2 col-lg-2">
                                <span class="font-weight-semibold">CAPACITACIÓN</span>
                            </div>
                            <div className="col-xs-2 col-lg-2">
                                <span class="font-weight-semibold">SUB. DIR DESARROLLO</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div className="card-body">
                        <div className="row">
                            <div className="col-xs-2 col-lg-2">
                                <h8 class="mb-0 font-weight-semibold">Orientación al logro </h8>
                            </div>

                            <div className="col-xs-2 col-lg-2"> 
                                <input type="number" name="orientacion_logro_edat" min="1" pattern="^[0-9]+" disabled={this.props.tipoUsuario != 'ASESRSR' && this.props.tipoUsuario != 'ASESRJR' && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.orientacion_logro_edat} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>

                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="orientacion_logro_edat2" disabled={this.props.tipoUsuario != 'GTEDR' && this.props.tipoUsuario != "SUPER"} onChange={this.onChangeEdat2} value={this.state.prospecto.orientacion_logro_edat2} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>
                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="orientacion_logro_gdd" onChange={this.onChangeGDD} disabled={this.props.tipoUsuario != 'GDD' && this.props.tipoUsuario != 'SDDC' && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.orientacion_logro_gdd} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>
                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="orientacion_logro_capacita" onChange={this.onChangeCapacita} disabled={this.props.tipoUsuario != "CAP" && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.orientacion_logro_capacita} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>
                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="orientacion_logro_direccion" onChange={this.onChangeDireccion} disabled={this.props.tipoUsuario != 'SDDC' && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.orientacion_logro_direccion} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-2 col-lg-2"></div>
                            <div className="col-xs-2 col-lg-2"></div>
                            <div className="col-xs-2 col-lg-2">
                            {
                                this.props.tipoUsuario == 'GTEDR' && (this.state.prospecto.orientacion_logro_edat2 == 0 || this.state.prospecto.orientacion_logro_edat2 == 1 || this.state.prospecto.orientacion_logro_edat2 == "") ?
                                    <span style={{ color: "red" }}>El campo Orientación al logros (GERENTE EDAT) debe ser ingresado</span> : ''
                            }</div>
                            <div className="col-xs-2 col-lg-2"></div>
                            <div className="col-xs-2 col-lg-2"></div>
                            <div className="col-xs-2 col-lg-2"></div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div className="card-body">
                        <div className="row">
                            <div className="col-xs-2 col-lg-2">
                                <h8 class="mb-0 font-weight-semibold">Perseverancia</h8>
                            </div>

                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="perceverancia_edat" disabled={this.props.tipoUsuario != 'ASESRSR' && this.props.tipoUsuario != 'ASESRJR' && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.perceverancia_edat} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>

                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="perceverancia_edat2" disabled={this.props.tipoUsuario != 'GTEDR' && this.props.tipoUsuario != "SUPER"} onChange={this.onChangeEdat2} value={this.state.prospecto.perceverancia_edat2} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>
                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="perceverancia_gdd" onChange={this.onChangeGDD} disabled={this.props.tipoUsuario != 'GDD' && this.props.tipoUsuario != 'SDDC' && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.perceverancia_gdd} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>
                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="perceverancia_capacita" onChange={this.onChangeCapacita} disabled={this.props.tipoUsuario != "CAP" && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.perceverancia_capacita} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>
                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="perceverancia_direccion" onChange={this.onChangeDireccion} disabled={this.props.tipoUsuario != 'SDDC' && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.perceverancia_direccion} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-2 col-lg-2"></div>
                            <div className="col-xs-2 col-lg-2"></div>
                            <div className="col-xs-2 col-lg-2">
                            {
                                this.props.tipoUsuario == 'GTEDR' && (this.state.prospecto.perceverancia_edat2 == 0 || this.state.prospecto.perceverancia_edat2 == 1 || this.state.prospecto.perceverancia_edat2 == "") ?
                                    <span style={{ color: "red" }}>El campo Perseverancia (GERENTE EDAT) debe ser ingresado</span> : ''
                            }</div>
                            <div className="col-xs-2 col-lg-2"></div>
                            <div className="col-xs-2 col-lg-2"></div>
                            <div className="col-xs-2 col-lg-2"></div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div className="card-body">
                        <div className="row">
                            <div className="col-xs-2 col-lg-2">
                                <h8 class="mb-0 font-weight-semibold">Carácter e integridad</h8>
                            </div>

                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="integridad_edat" disabled={this.props.tipoUsuario != 'ASESRSR' && this.props.tipoUsuario != 'ASESRJR' && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.integridad_edat} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>

                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="integridad_edat2" disabled={this.props.tipoUsuario != 'GTEDR' && this.props.tipoUsuario != "SUPER"} onChange={this.onChangeEdat2} value={this.state.prospecto.integridad_edat2} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>
                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="integridad_gdd" onChange={this.onChangeGDD} disabled={this.props.tipoUsuario != 'GDD' && this.props.tipoUsuario != 'SDDC' && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.integridad_gdd} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>
                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="integridad_capacita" onChange={this.onChangeCapacita} disabled={this.props.tipoUsuario != "CAP" && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.integridad_capacita} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>
                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="integridad_direccion" onChange={this.onChangeDireccion} disabled={this.props.tipoUsuario != 'SDDC' && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.integridad_direccion} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-2 col-lg-2"></div>
                            <div className="col-xs-2 col-lg-2"></div>
                            <div className="col-xs-2 col-lg-2">
                            {
                                this.props.tipoUsuario == 'GTEDR' && (this.state.prospecto.integridad_edat2 == 0 || this.state.prospecto.integridad_edat2 == 1 || this.state.prospecto.integridad_edat2 == "") ?
                                    <span style={{ color: "red" }}>El campo Carácter e integridad (GERENTE EDAT) debe ser ingresado</span> : ''
                            }</div>
                            <div className="col-xs-2 col-lg-2"></div>
                            <div className="col-xs-2 col-lg-2"></div>
                            <div className="col-xs-2 col-lg-2"></div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div className="card-body">
                        <div className="row">
                            <div className="col-xs-2 col-lg-2">
                                <h8 class="mb-0 font-weight-semibold">Sentido común</h8>
                            </div>

                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="sentido_comun_edat" disabled={this.props.tipoUsuario != 'ASESRSR' && this.props.tipoUsuario != 'ASESRJR' && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.sentido_comun_edat} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>

                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="sentido_comun_edat2" disabled={this.props.tipoUsuario != 'GTEDR' && this.props.tipoUsuario != "SUPER"} onChange={this.onChangeEdat2} value={this.state.prospecto.sentido_comun_edat2} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>
                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="sentido_comun_gdd" onChange={this.onChangeGDD} disabled={this.props.tipoUsuario != 'GDD' && this.props.tipoUsuario != 'SDDC' && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.sentido_comun_gdd} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>
                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="sentido_comun_capacita" onChange={this.onChangeCapacita} disabled={this.props.tipoUsuario != "CAP" && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.sentido_comun_capacita} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>
                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="sentido_comun_direccion" onChange={this.onChangeDireccion} disabled={this.props.tipoUsuario != 'SDDC' && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.sentido_comun_direccion} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-2 col-lg-2"></div>
                            <div className="col-xs-2 col-lg-2"></div>
                            <div className="col-xs-2 col-lg-2">
                            {
                                this.props.tipoUsuario == 'GTEDR' && (this.state.prospecto.sentido_comun_edat2 == 0 || this.state.prospecto.sentido_comun_edat2 == 1 || this.state.prospecto.sentido_comun_edat2 == "") ?
                                    <span style={{ color: "red" }}>El campo Sentido común (GERENTE EDAT) debe ser ingresado</span> : ''
                            }</div>
                            <div className="col-xs-2 col-lg-2"></div>
                            <div className="col-xs-2 col-lg-2"></div>
                            <div className="col-xs-2 col-lg-2"></div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div className="card-body">
                        <div className="row">
                            <div className="col-xs-2 col-lg-2">
                                <h8 class="mb-0 font-weight-semibold">Energía</h8>
                            </div>

                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="energia_edat" disabled={this.props.tipoUsuario != 'ASESRSR'&& this.props.tipoUsuario != 'ASESRJR' && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.energia_edat} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>

                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="energia_edat2" disabled={this.props.tipoUsuario != 'GTEDR' && this.props.tipoUsuario != "SUPER"} onChange={this.onChangeEdat2} value={this.state.prospecto.energia_edat2} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>
                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="energia_gdd" onChange={this.onChangeGDD} disabled={this.props.tipoUsuario != 'GDD' && this.props.tipoUsuario != 'SDDC' && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.energia_gdd} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>
                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="energia_capacita" onChange={this.onChangeCapacita} disabled={this.props.tipoUsuario != "CAP" && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.energia_capacita} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>
                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="energia_direccion" onChange={this.onChangeDireccion} disabled={this.props.tipoUsuario != 'SDDC' && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.energia_direccion} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-2 col-lg-2"></div>
                            <div className="col-xs-2 col-lg-2"></div>
                            <div className="col-xs-2 col-lg-2">
                            {
                                this.props.tipoUsuario == 'GTEDR' && (this.state.prospecto.energia_edat2 == 0 || this.state.prospecto.energia_edat2 == 1 || this.state.prospecto.energia_edat2 == "") ?
                                    <span style={{ color: "red" }}>El campo Energía (GERENTE EDAT) debe ser ingresado</span> : ''
                            }</div>
                            <div className="col-xs-2 col-lg-2"></div>
                            <div className="col-xs-2 col-lg-2"></div>
                            <div className="col-xs-2 col-lg-2"></div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div className="card-body">
                        <div className="row">
                            <div className="col-xs-2 col-lg-2">
                                <h8 class="mb-0 font-weight-semibold">Motivación por el dinero</h8>
                            </div>

                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="motivacion_edat" disabled={this.props.tipoUsuario != 'ASESRSR' && this.props.tipoUsuario != 'ASESRJR' && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.motivacion_edat} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>

                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="motivacion_edat2" disabled={this.props.tipoUsuario != 'GTEDR' && this.props.tipoUsuario != "SUPER"} onChange={this.onChangeEdat2} value={this.state.prospecto.motivacion_edat2} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>
                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="motivacion_gdd" onChange={this.onChangeGDD} disabled={this.props.tipoUsuario != 'GDD' && this.props.tipoUsuario != 'SDDC' && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.motivacion_gdd} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>
                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="motivacion_capacita" onChange={this.onChangeCapacita} disabled={this.props.tipoUsuario != "CAP" && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.motivacion_capacita} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>
                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="motivacion_direccion" onChange={this.onChangeDireccion} disabled={this.props.tipoUsuario != 'SDDC' && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.motivacion_direccion} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-2 col-lg-2"></div>
                            <div className="col-xs-2 col-lg-2"></div>
                            <div className="col-xs-2 col-lg-2">
                            {
                                this.props.tipoUsuario == 'GTEDR' && (this.state.prospecto.motivacion_edat2 == 0 || this.state.prospecto.motivacion_edat2 == 1 || this.state.prospecto.motivacion_edat2 == "") ?
                                    <span style={{ color: "red" }}>El campo Motivación por el dinero (GERENTE EDAT) debe ser ingresado</span> : ''
                            }</div>
                            <div className="col-xs-2 col-lg-2"></div>
                            <div className="col-xs-2 col-lg-2"></div>
                            <div className="col-xs-2 col-lg-2"></div>
                        </div>
                    </div>
                </div>

                <div class="card" style={{ background: '#E1E3F6 ', borderRadius: '3px 3px 0px 0px' }}>
                    <div className="card-body">
                        <div className="row">
                            <div className="col-xs-2 col-lg-2">
                                <h8 class="mb-0 font-weight-semibold">TOTAL</h8>
                            </div>

                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="total_edat" disabled={true} value={this.state.prospecto.total_edat} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>

                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="total_edat2" disabled={true} value={this.state.prospecto.total_edat2} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>
                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="total_gdd" disabled={true} value={this.state.prospecto.total_gdd} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>
                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="total_capacita" disabled={true} value={this.state.prospecto.total_capacita} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>
                            <div className="col-xs-2 col-lg-2">
                                <input type="number" name="total_direccion" disabled={true} value={this.state.prospecto.total_direccion} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                            </div>
                        </div>
                    </div>
                </div>
                <HistoricoGeneral idProspecto={this.props.idProspecto} tipoUsuario={this.state.tipoUsuario} presentarSeccion={"EP7"} />
            </div>

        )
    }
}