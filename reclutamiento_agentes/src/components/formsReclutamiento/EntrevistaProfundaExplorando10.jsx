import React, { Component } from "react";
import HistoricoGeneral from "./HistoricoGeneral";
import axios from 'axios';
import datos from '../urls/datos.json'

export default class EntrevistaProfundaExplorando10 extends Component {

    constructor() {
        super();
        this.onChange = this.onChange.bind(this)
        this.onClickBotonArchivoPSP = this.onClickBotonArchivoPSP.bind(this)
        this.cargaInfoPSP = this.cargaInfoPSP.bind(this)
        this.enviarVentaCarrea = this.enviarVentaCarrea.bind(this)
        this.lanzaModalReserva = this.lanzaModalReserva.bind(this)
        this.construyeSelectMotivos = this.construyeSelectMotivos.bind(this)
        this.onChangeMotivos = this.onChangeMotivos.bind(this)
        this.peticionAReserva = this.peticionAReserva.bind(this)
        this.lanzaModalBaja = this.lanzaModalBaja.bind(this)
        this.peticionABaja = this.peticionABaja.bind(this)
        this.validaGuardarFormulario = this.validaGuardarFormulario.bind(this)
        this.construyeSelect=this.construyeSelect.bind(this)
        this.peticionABajaEnDuro=this.peticionABajaEnDuro.bind(this)
        this.construyeSelectBaja= this.construyeSelectBaja.bind(this)
        this.cargaPdf14 = this.cargaPdf14.bind(this)
        this.state = {
            entrevistador:'',
            exito: false,
            prospecto:
            {
                "id": null,
                "prospecto_id": null,
                "estatus_id": 16,
                "falta_patron": null,
                "incapacidad_aceptar": null,
                "bajo_nivel": null,
                "recientemente_divorciado": null,
                "malos_habitos": null,
                "demasiados_cambios": null,
                "mercado_natural": null,
                "benefactor_profecional": null,
                "falta_movilidad": null,
                "problemas_salud": null,
                "problemas_financieros": null,
                "ventas_retroceso": null,
                "culpa_demas": null,
                "cualquier_trabajo": null,
                "seguros_vida": null,
                "status_quo": null,
                "resultado_entrevista": null,
                "gdd_id":null,
                "sddc_id":null,
                "causas_rechazo":null
            },
            selectMotivos: [],
            selectMotivosBaja:[],
            selectGDD:[],
            selectSDDC:[],
            idMotivoBaja: 0,




            colorSubirArchivoCV: "#E1E5F0",
            colorTextoSubirArchivoCV: "#617187",
            colorBotonSubirArchivoCV: "#0F69B8",
            nombreArchivoCV: "",
            archivoCV: undefined,


            archivoPSP: undefined,
            muestraDocPSP: false,
            colorSubirArchivoPSP: "#E1E5F0",
            colorTextoSubirArchivoPSP: "#617187",
            colorBotonSubirArchivoPSP: "#313A46",
            nombreArchivoPSP: "",
            archivoPSP: undefined,
            textoPuesto: 'Subir archivo',

            muestraInfoPSP: false,
            eficaciaVentas: 0,
            eficaciaEmpresarial: 0,
            rendimientoVentas: 0,
            fechaRespPSP: '',
            ventaDominante: '',
            interpretacion: '',
            rutaPSP: ''


        };
    }

    UNSAFE_componentWillMount() {
        console.log("entrando es render de explorando ", this)
        this.construyeSelectMotivos(datos.urlServicePy+"parametros/api_cat_motivo_envio_reserva/0")
        this.construyeSelectBaja(datos.urlServicePy+"parametros/api_cat_causas_baja_prospecto/0", "selectBajas")
       
        fetch(datos.urlServicePy+"recluta/api_recluta_knockout10/" + this.props.idProspecto)
            .then(response => response.json())
            .then(jsonData => {
                if (jsonData.length > 0) {
                    this.setState({ prospecto: jsonData[0] })
                    this.construyeSelect(datos.urlServicePy+"recluta/api_vcat_gdd/0","selectGDD")
                    this.construyeSelect(datos.urlServicePy+"recluta/api_vcat_sddc/0","selectSDDC")
                }else{
                    this.construyeSelect(datos.urlServicePy+"recluta/api_vcat_gdd/0","selectGDD")
                    this.construyeSelect(datos.urlServicePy+"recluta/api_vcat_sddc/0","selectSDDC")
                }

                    fetch(datos.urlServicePy+"parametros/api_recluta_prospectos/" + this.props.idProspecto)
                    .then(response => response.json())
                    .then(recluta_prospecto => {
                        console.log("recluta_propspecto", recluta_prospecto)
                        fetch(datos.urlServicePy+"parametros/api_cat_empleados/" + recluta_prospecto.filas[0].fila[12].value)
                        .then(response => response.json())
                        .then(entrevistador => {
                                console.log("entrevistador ", entrevistador)
                                this.setState({entrevistador:entrevistador.filas[0].fila[2].value + " "+ entrevistador.filas[0].fila[3].value + " " + (entrevistador.filas[0].fila[4].value != null ? entrevistador.filas[0].fila[4].value:'') })
                        })
                    })
                    fetch(datos.urlServicePy+"recluta/api_recluta_vcargapsppdf14/" + this.props.idProspecto)
                        .then(response => response.json())
                        .then(jsonRespuestaCarga => {
                            console.log("respuesta de la carga del pdf ", jsonRespuestaCarga)
                            if (jsonRespuestaCarga.length > 0) {

                             //   console.log("salida de la validacion ",jsonRespuestaCarga[6] === undefined ?  "":jsonRespuestaCarga[6].respuesta)
                                let interpretacionConcatenada=jsonRespuestaCarga[6] === undefined ?  "":jsonRespuestaCarga[6].respuesta
                                this.setState({
                                    muestraInfoPSP: true,
                                    rutaPSP: jsonRespuestaCarga[0].ironpsp,
                                    fechaRespPSP: jsonRespuestaCarga[0].respuesta,
                                    eficaciaVentas: jsonRespuestaCarga[1].porcentaje,
                                    eficaciaEmpresarial: jsonRespuestaCarga[2].porcentaje,
                                    rendimientoVentas: jsonRespuestaCarga[3].porcentaje,
                                    ventaDominante:jsonRespuestaCarga[4].respuesta,
                                    interpretacion:jsonRespuestaCarga[5].respuesta + " "+interpretacionConcatenada ,
                                    muestraDocPSP: true,
                                    colorSubirArchivoPSP: "#78CB5A", colorTextoSubirArchivoPSP: "#FFFFFF", colorBotonSubirArchivoPSP: "#78CB5A"
                                })
                            }


                        })
                

            })

    }
    onChange = e => {
        console.log(e.target.value)
        let pros = this.state.prospecto
        if (e.target.name == "resultado_entrevista" || e.target.name == "causas_rechazo") {
            pros["" + e.target.name + ""] = e.target.value
        } else if(e.target.name == "selectGDD") {
            pros.gdd_id=parseInt(e.target.value)            

        }else if(e.target.name == "selectSDDC"){
            pros.sddc_id=parseInt(e.target.value)
        }else{
            if (e.target.checked == true) {
                pros["" + e.target.name + ""] = 1
            } else {
                pros["" + e.target.name + ""] = 0
            }
        }
        this.setState({ prospecto: pros })

    }


    onClickBotonArchivoPSP() {
        console.log("entrnaod a la imagen")
        this.upload.click()
    }


    cargaInfoPSP(json) {
        const requestOptions = {
            method: "POST",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };

        fetch(datos.urlServicePy+"recluta/api_recluta_cargapsppdf/0", requestOptions)
        .then(response => response.json())
        .then(data => {
            console.log("la respuesta de la carga", data)
            this.cargaPdf14()
        });
    }

    cargaPdf14(){
        fetch(datos.urlServicePy+"recluta/api_recluta_vcargapsppdf14/" + this.props.idProspecto)
        .then(response => response.json())
        .then(jsonRespuestaCarga => {
            console.log("respuesta de la carga ", jsonRespuestaCarga)
            let interpretacionConcatenada=jsonRespuestaCarga[6] === undefined ?  "":jsonRespuestaCarga[6].respuesta

            this.setState({
                muestraInfoPSP: true,
                fechaRespPSP: jsonRespuestaCarga[0].respuesta,
                eficaciaVentas: jsonRespuestaCarga[1].porcentaje,
                eficaciaEmpresarial: jsonRespuestaCarga[2].porcentaje,
                rendimientoVentas: jsonRespuestaCarga[3].porcentaje,
                ventaDominante:jsonRespuestaCarga[4].respuesta,
                interpretacion:jsonRespuestaCarga[5].respuesta + " "+interpretacionConcatenada,
            })
        })
    }

    onChangeFilePSP(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file de puesto", file);

        //lanzamos el envio del psp 
        let formData = new FormData();
        const config = {
            headers: { 'content-type': 'multipart/form-data' }
        }
        
        if(this.state.muestraDocPSP == true){
            formData.append('ironpsp', file);
            axios.put(datos.urlServicePy+"api/doc_ironpsp/"+this.props.idProspecto+"/", formData, config)
            .then(res => {
                console.log("respuesta  del archivo cargado", res.data);
                this.cargaInfoPSP(res.data)
                let ruta = res.data.ironpsp
                let rutaCorrecta = ruta.replace("8000/media", "8001/documentos")
                rutaCorrecta = rutaCorrecta.replace("http", "https")
                console.log(rutaCorrecta)
                this.setState({ rutaPSP: rutaCorrecta, archivoPSP: file, colorSubirArchivoPSP: "#78CB5A", colorTextoSubirArchivoPSP: "#FFFFFF", colorBotonSubirArchivoPSP: "#78CB5A", nombreArchivoPSP: file.name, muestraDocPSP: true })
            })
        } else {
            formData.append('id', this.props.idProspecto);
            formData.append('ironpsp', file);
            formData.append('prospecto_id', this.props.idProspecto);
            axios.post(datos.urlServicePy+"api/doc_ironpsp/", formData, config)
            .then(res => {
                console.log("respuesta  del archivo cargado", res.data);
                this.cargaInfoPSP(res.data)
                let ruta = res.data.ironpsp
                let rutaCorrecta = ruta.replace("8000/media", "8001/documentos")
                rutaCorrecta = rutaCorrecta.replace("http", "https")
                console.log(rutaCorrecta)
                this.setState({ rutaPSP: rutaCorrecta, archivoPSP: file, colorSubirArchivoPSP: "#78CB5A", colorTextoSubirArchivoPSP: "#FFFFFF", colorBotonSubirArchivoPSP: "#78CB5A", nombreArchivoPSP: file.name, muestraDocPSP: true })
            })
        }
        
    }

    onChangeMotivosBaja = e => {
        if (e.target.name == 'selectMotivosBaja') {
            this.setState({ idBaja: parseInt(e.target.value) })
        } else {
            this.setState({ [e.target.name]: e.target.value })

        }
    }

    construyeSelectBaja(url) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                console.log("respuesta  ", json)
                for (var i = 0; i < filas.length; i++) {
                    let fila = filas[i]

                    options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                }
                let salida = []
                salida.push(<select onChange={this.onChangeMotivosBaja} name="selectMotivosBaja" style={{ borderColor: '#F1F3FA' }} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ selectMotivosBaja: salida })


            })
    }


    enviarVentaCarrea() {
        let json = {
            "estatus_id": 16
        }
        const requestOptions = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };
        console.log("enviando a reserva")

        fetch(datos.urlServicePy+"parametros_upd/api_recluta_prospectos/" + this.props.prospecto_id, requestOptions)
            .then(response => response.json())
            .then(data => {
                //this.construyeTarjetas()
                this.props.botonCancelar()
            })

    }

    lanzaModalReserva() {
        this.reserva.click()
    }




    lanzaModalBaja() {
        this.baja.click()
    }

    construyeSelectMotivos(url) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                console.log("respuesta  ", json)
                for (var i = 0; i < filas.length; i++) {
                    let fila = filas[i]

                    options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                }
                let salida = []
                salida.push(<select onChange={this.onChangeMotivos} name="selectMotivos" style={{ borderColor: '#F1F3FA' }} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ selectMotivos: salida })


            })
    }

    construyeSelect(url,selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                console.log("respuesta  ", filas)
                for (var i = 0; i < filas.length; i++) {
                    let fila = filas[i]
                    console.log("fila ", fila)
                    //options.push(<option value={fila.id}>{fila.empleado}</option>)

                    if (selector == "selectGDD") {
                        if (this.state.prospecto.gdd_id == fila.id) {
                            options.push(<option selected value={fila.id}>{fila.empleado}</option>)
                        } else {
                            options.push(<option value={fila.id}>{fila.empleado}</option>)
                        }
                    }
                    if (selector == "selectSDDC") {
                        if (this.state.prospecto.sddc_id == fila.id) {
                            options.push(<option selected value={fila.id}>{fila.empleado}</option>)
                        } else {
                            options.push(<option value={fila.id}>{fila.empleado}</option>)
                        }
                    }


                }
                let salida = []  
                salida.push(<select onChange={this.onChange} name={selector} style={{ borderColor: '#F1F3FA' }} class="form-control" aria-label="Default select example">{options}</select>)
                if (selector == "selectGDD") {
                    this.setState({ selectGDD: salida })
                }
                if (selector == "selectSDDC") {
                    this.setState({ selectSDDC: salida })
                }


            })
    }

    onChangeMotivos = e => {
        if (e.target.name == 'selectMotivos') {
            this.setState({ idMotivoBaja: parseInt(e.target.value) })
        } else {
            this.setState({ [e.target.name]: e.target.value })

        }
    }



    peticionAReserva() {
        let json = {
            "estatus_id": 18,
            "causas_baja_id": this.state.idMotivoBaja

        }
        const requestOptions = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };
        console.log("enviando a reserva")

        fetch(datos.urlServicePy+"parametros_upd/api_recluta_prospectos/" + this.props.idProspecto, requestOptions)
            .then(response => response.json())
            .then(data => {
                //  this.construyeTarjetas()
                this.props.guardaFormulario10(this.state.prospecto)
                this.props.botonCancelar()
            })

    }

    peticionABaja() {
        let json = {
            "estatus_id": 19,
            "causas_baja_id": this.state.idMotivoBaja
        }
        const requestOptions = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };
        console.log("enviando a reserva")

        fetch(datos.urlServicePy+"parametros_upd/api_recluta_prospectos/" + this.props.idProspecto, requestOptions)
            .then(response => response.json())
            .then(data => {
                //this.construyeTarjetas()
                this.props.guardaFormulario10(this.state.prospecto)
                this.props.botonCancelar()
            })
    }

    peticionABajaEnDuro() {// aqui viene  el de rechazo
        let json = {
            "estatus_id": 19,
            "causas_baja_id": 2
        }
        const requestOptions = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };
        console.log("enviando a reserva")

        fetch(datos.urlServicePy+"parametros_upd/api_recluta_prospectos/" + this.props.idProspecto, requestOptions)
            .then(response => response.json())
            .then(data => {
                //this.construyeTarjetas()
                this.props.guardaFormulario10(this.state.prospecto)
                this.props.botonCancelar()
            })
    }


    validaGuardarFormulario() {

        if (this.state.prospecto.resultado_entrevista == 3) {
            console.log(this.state.prospecto)
            if(this.state.muestraDocPSP == true && this.state.muestraInfoPSP == true){
                if(this.state.prospecto.gdd_id == null || this.state.prospecto.gdd_id == 0){
                    this.avisoGDD.click()
                }else{
                    if(this.state.prospecto.sddc_id == null || this.state.prospecto.sddc_id == 0){
                        this.avisoSDDC.click()
                    }else{
                        this.props.guardaFormulario10(this.state.prospecto)
                    }
 
                }
            }else{
                this.avisoPSP.click()
            }
        } else if (this.state.prospecto.resultado_entrevista == 4) {
            this.lanzaModalReserva()
        } else if (this.state.prospecto.resultado_entrevista == 0) {
            this.lanzaModalBaja()
        }else if(this.state.prospecto.resultado_entrevista == 2){
            this.peticionABajaEnDuro()
        }else if(this.state.prospecto.resultado_entrevista == 1){
            let json=this.state.prospecto
            json.estatus_id=15
            this.props.guardaFormulario10(json)
        }

    }


    render() {
        return (
            <div>
                <button type="button" style={{ display: 'none' }} ref={(ref) => this.reserva = ref} data-toggle="modal" data-target="#modal-reserva"   ></button>
                <button type="button" style={{ display: 'none' }} ref={(ref) => this.baja = ref} data-toggle="modal" data-target="#modal-baja"   ></button>
                <button type="button" style={{ display: 'none' }} ref={(ref) => this.avisoPSP = ref} data-toggle="modal" data-target="#modal-avisoPSP"   ></button>
                <button type="button" style={{ display: 'none' }} ref={(ref) => this.avisoGDD = ref} data-toggle="modal" data-target="#modal-avisoGDD"   ></button>
                <button type="button" style={{ display: 'none' }} ref={(ref) => this.avisoSDDC = ref} data-toggle="modal" data-target="#modal-avisoSDDC"   ></button>


                <div id="modal-psp" className="modal fade" tabindex="-1">
                    <div className="modal-dialog modal-full">
                        <div className="modal-content text-white" style={{ backgroundColor: "#313A46" }}>
                            <div className="modal-header text-white" style={{ backgroundColor: "#8189D4" }}>
                                <h6 className="modal-title">Documento PSP </h6>
                                <button type="button" className="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div className="modal-body">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-xs-12 col-lg-4"><>&nbsp;&nbsp;</></div>
                                        <div className="col-xs-12 col-lg-4">
                                            <div className="input-group">
                                                <form encType="multipart/form" style={{ display: 'none' }} >
                                                    <input type="file" style={{ display: 'none' }} ref={(ref) => this.upload = ref} onChange={this.onChangeFilePSP.bind(this)}></input>
                                                </form>
                                                <input type="text" style={{ borderColor: 'black', backgroundColor: "#313A46", color: 'white' }} className="form-control " placeholder="Archivo PSP" value={this.state.nombreArchivoPSP} />
                                                <span className="input-group-prepend" style={{ backgroundColor: this.state.colorBotonSubirArchivoPSP }}>
                                                    {/*<button type="button" class="btn text-white" onClick={this.onClickBotonArchivoPSP} style={{ backgroundColor: this.state.colorBotonSubirArchivoPSP }}>
                                                        <h10 style={{ color: "white" }}>+</h10>
                                                         </button>*/}

                                                    {
                                                                this.state.muestraDocPSP == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoPSP }}>
                                                                <a href={this.state.rutaPSP} target="_blank" rel="noopener noreferrer" >
                                                                    <i className="fas fa-eye"
                                                                        style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h10 onClick={this.onClickBotonArchivoPSP} style={{ color: "white" }}>+</h10>
                                                            </button> : <button type="button" class="btn text-white" onClick={this.onClickBotonArchivoPSP} style={{ backgroundColor: this.state.colorBotonSubirArchivoPSP }}>
                                                                <h10 style={{ color: "white" }}>+</h10>
                                                            </button>

                                                    }



                                                    <span className="input-group-text" style={{ background: this.state.colorBotonSubirArchivoPSP, borderColor: this.state.colorBotonSubirArchivoPSP, color: "white" }} >{this.state.textoPuesto}</span>
                                                </span>
                                            </div>
                                        </div>
                                        <div className="col-xs-12 col-lg-4"><>&nbsp;&nbsp;</></div>
                                    </div>

                                    <br></br>
                                    <br></br>
                                    {this.state.muestraInfoPSP == true &&
                                        <div>
                                            <div className="row">
                                                <div className="col-xs-6 col-lg-6">
                                                    <h8 class="mb-0 font-weight-semibold text-white"> Resultados PSP</h8>
                                                </div>
                                                <div className="col-xs-4 col-lg-4">
                                                    <table className="table datatable-sorting  table-striped table-hover" style={{ backgroundColor: "#313A46", borderColor: '#313A46' }}>
                                                        <thead style={{ backgroundColor: "#313A46" }}>
                                                            <tr>
                                                                <th className="text-left font-weight-bold text-white" style={{ backgroundColor: '#313A46' }} >
                                                                    <a>ALTO</a>
                                                                </th>
                                                                <th className="text-center font-weight-bold text-white" style={{ backgroundColor: '#313A46' }} >
                                                                    <a>MODERADO</a>
                                                                </th>
                                                                <th className="text-right font-weight-bold text-white" style={{ backgroundColor: '#313A46' }} >
                                                                    <a>BAJO</a>
                                                                </th>
                                                            </tr>
                                                        </thead>

                                                    </table>
                                                </div>
                                            </div>

                                            <div className="row">
                                                <div className="col-xs-4 col-lg-4">
                                                    <div className="form-group">
                                                        <div className="input-group">
                                                            <span className="input-group-prepend"> <span
                                                                className="input-group-text text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }}>Fecha de respuesta del PSP</span>
                                                            </span> <input type="text" placeholder="" value={this.state.fechaRespPSP} className="form-control text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }} />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-xs-2 col-lg-2">
                                                    <h8 class=" text-white"> Eficacia en ventas</h8>
                                                </div>

                                                <div className="col-xs-4 col-lg-4">

                                                    <input type="range" class="custom-range" min="0" name="salud" max="100" step={50} value={this.state.eficaciaVentas} id="salud" />
                                                </div>


                                                <div className="col-xs-2 col-lg-2">




                                                    {
                                                        this.state.eficaciaVentas == 0 &&
                                                        <div class="mr-3 ">
                                                            <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                                    class="letter-icon text-white"></span>
                                                            </a>
                                                        </div>

                                                    }

                                                    {
                                                        this.state.eficaciaVentas == 50 &&
                                                        <div className="row">
                                                           
                                                           { /*<div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>*/}

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                        </div>
                                                    }


                                                    {
                                                        this.state.eficaciaVentas == 100 &&
                                                        <div className="row">

                                                            {/*<div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>*/}

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FD0303', borderColor: '#FD0303' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                        </div>
                                                    }





                                                </div>


                                            </div>

                                            <div className="row">

                                                <div className="col-xs-12 col-lg-4">
                                                    <div className="form-group">
                                                        <div className="input-group">
                                                            <span className="input-group-prepend"> <span
                                                                className="input-group-text text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }}>Estilo de venta dominante&ensp;</span>
                                                            </span> <input type="text" placeholder="" value={this.state.ventaDominante} className="form-control text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }} />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-xs-12 col-lg-2">
                                                    <h8 class=" text-white"> Eficacia empresarial</h8>
                                                </div>

                                                <div className="col-xs-12 col-lg-4">
                                                    <input type="range" class="custom-range" min="0" name="salud" max="100" step={50} value={this.state.eficaciaEmpresarial} id="salud" />
                                                </div>


                                                <div className="col-xs-2 col-lg-2">

                                                    {
                                                        this.state.eficaciaEmpresarial == 0 &&
                                                        <div class="mr-3 ">
                                                            <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                                    class="letter-icon text-white"></span>
                                                            </a>
                                                        </div>

                                                    }

                                                    {
                                                        this.state.eficaciaEmpresarial == 50 &&
                                                        <div className="row">
                                                            
                                                            {/*<div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                              </div>*/}

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                        </div>
                                                    }


                                                    {
                                                        this.state.eficaciaEmpresarial == 100 &&
                                                        <div className="row">

                                                            {/*<div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>*/}

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FD0303', borderColor: '#FD0303' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                        </div>
                                                    }



                                                </div>



                                            </div>

                                            <div className="row">
                                                <div className="col-xs-12 col-lg-4">
                                                    <div className="form-group">
                                                        <div className="input-group">
                                                            <span className="input-group-prepend"> <span
                                                                className="input-group-text text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }}>Interpretación de precisión</span>
                                                            </span> <textarea disabled={true} type="text" placeholder="" value={this.state.interpretacion} className="form-control text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }} />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-xs-12 col-lg-2">
                                                    <h8 class=" text-white">Rendimiento de ventas general esperado</h8>
                                                </div>

                                                <div className="col-xs-12 col-lg-4">
                                                    <input type="range" class="custom-range" min="0" name="salud" max="100" step={50} value={this.state.rendimientoVentas} id="salud" />
                                                </div>

                                                <div className="col-xs-2 col-lg-2">

                                                    {
                                                        this.state.rendimientoVentas == 0 &&
                                                        <div class="mr-3 ">
                                                            <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                                    class="letter-icon text-white"></span>
                                                            </a>
                                                        </div>

                                                    }

                                                    {
                                                        this.state.rendimientoVentas == 50 &&
                                                        <div className="row">
                                                           
                                                           { /*<div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>*/}

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                        </div>
                                                    }


                                                    {
                                                        this.state.rendimientoVentas == 100 &&
                                                        <div className="row">

                                                            {/*<div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>*/}

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FD0303', borderColor: '#FD0303' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                        </div>
                                                    }



                                                </div>


                                            </div>
                                        </div>

                                    }







                                </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-center">
                                <button type="button" class="btn text-white" data-dismiss="modal" style={{ backgroundColor: '#617187', borderColor: '#617187' }}>Cancelar</button>

                                {
                                    this.state.muestraDocPSP == true ? <button type="button" class="btn text-white " disabled={false} style={{ backgroundColor: '#617187', borderColor: '#617187' }}>
                                        <a href={this.state.rutaPSP} style={{ color: 'white' }} target="_blank" rel="noopener noreferrer" >
                                            Ver PSP
                                        </a>
                                    </button> : <button type="button" class="btn text-white " disabled={true} style={{ backgroundColor: '#617187', borderColor: '#617187' }}>
                                        <a href="#" style={{ color: 'white' }} target="_blank" rel="noopener noreferrer" >
                                            Ver PSP
                                        </a>

                                    </button>
                                }

                                <button type="button" class="btn text-white" data-dismiss="modal" style={{ backgroundColor: '#617187', borderColor: '#617187' }}>Aceptar</button>
                            </div>
                        </div>
                    </div>
                </div>


                <div id="modal-reserva" className="modal fade" tabindex="-1">
                    <div className="modal-dialog ">
                        <div className="modal-content text-white" style={{ backgroundColor: "#FFFFF" }}>
                            <div className="modal-header text-white text-center" style={{ backgroundColor: "#617187" }}>
                                <h6 className="modal-title col-12 text-center">Enviar a reserva</h6>

                            </div>
                            <div className="modal-body">
                                <div className="card-body">
                                    <h8 style={{ color: '#8F9EB3' }}>Si está seguro de enviar a reserva el registro, por favor seleccione el motivo</h8>
                                    {this.state.selectMotivos}
                                    <br></br>
                                    <h8 style={{ color: '#8F9EB3' }}>Fecha acordada para retomar el proceso</h8>
                                    <input type="date" placeholder="Escribir" name="fecha_alta" className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />

                                </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-center">
                                <div className="col-12">
                                    <div className="row">
                                        <button type="button" style={{ width: '100%', backgroundColor: "#617187" }} class="btn text-white" onClick={this.peticionAReserva} data-dismiss="modal"  >Enviar a reserva</button>
                                    </div>
                                    <br></br>
                                    <div className="row">
                                        <button type="button" style={{ width: '100%', backgroundColor: '#8F9EB3' }} class="btn text-white" id="btnAgregar" data-dismiss="modal"  >Cancelar</button>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>


                <div id="modal-baja" className="modal fade" tabindex="-1">
                    <div className="modal-dialog ">
                        <div className="modal-content text-white" style={{ backgroundColor: "#FFFFF" }}>
                            <div className="modal-header text-white text-center" style={{ backgroundColor: "#617187" }}>
                                <h6 className="modal-title col-12 text-center">Dar baja el registro</h6>

                            </div>
                            <div className="modal-body">
                                <div className="card-body">
                                    <h8 style={{ color: '#8F9EB3' }}>Si está seguro de descartar el registro, por favor seleccione el motivo</h8>
                                    {this.state.selectMotivosBaja}


                                </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-center">
                                <div className="col-12">
                                    <div className="row">
                                        <button type="button" style={{ width: '100%', backgroundColor: "#617187" }} class="btn text-white" onClick={this.peticionABaja} data-dismiss="modal" >Dar de baja registro</button>
                                    </div>
                                    <br></br>
                                    <div className="row">
                                        <button type="button" style={{ width: '100%', backgroundColor: '#8F9EB3' }} class="btn text-white" id="btnAgregar" data-dismiss="modal"  >Cancelar</button>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>


                <div id="modal-avisoPSP" className="modal fade" tabindex="-1">
                    <div className="modal-dialog ">
                        <div className="modal-content text-white" style={{ backgroundColor: "#FFFFF" }}>
                            <div className="modal-header text-white text-center" style={{ backgroundColor: "#0F69B8" }}>
                                <h6 className="modal-title col-11 text-center">Advertencia</h6>
                            </div>
                            <div className="modal-body">
                                <div className="card-body">
                                    <h8 style={{ color: '#0F69B8' }}>Recuerda que el archivo PSP es requerido, de lo contrario seleccione "En espera de PSP"</h8>
                                </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-center">
                                <div className="col-12">
                                    <div className="row">
                                        <button type="button" style={{ width: '100%', backgroundColor: '#8F9EB3' }} class="btn text-white" id="btnAgregar" data-dismiss="modal"  >Aceptar</button>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                <div id="modal-avisoGDD" className="modal fade" tabindex="-1">
                    <div className="modal-dialog ">
                        <div className="modal-content text-white" style={{ backgroundColor: "#FFFFF" }}>
                            <div className="modal-header text-white text-center" style={{ backgroundColor: "#0F69B8" }}>
                                <h6 className="modal-title col-11 text-center">Advertencia</h6>
                            </div>
                            <div className="modal-body">
                                <div className="card-body">
                                    <h8 style={{ color: '#0F69B8' }}>Recuerda que debes asignar un GDD antes de continuar </h8>
                                </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-center">
                                <div className="col-12">
                                    <div className="row">
                                        <button type="button" style={{ width: '100%', backgroundColor: '#8F9EB3' }} class="btn text-white" id="btnAgregar" data-dismiss="modal"  >Aceptar</button>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                <div id="modal-avisoSDDC" className="modal fade" tabindex="-1">
                    <div className="modal-dialog ">
                        <div className="modal-content text-white" style={{ backgroundColor: "#FFFFF" }}>
                            <div className="modal-header text-white text-center" style={{ backgroundColor: "#0F69B8" }}>
                                <h6 className="modal-title col-11 text-center">Advertencia</h6>
                            </div>
                            <div className="modal-body">
                                <div className="card-body">
                                    <h8 style={{ color: '#0F69B8' }}>Recuerda que debes asignar un SDDC antes de continuar </h8>
                                </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-center">
                                <div className="col-12">
                                    <div className="row">
                                        <button type="button" style={{ width: '100%', backgroundColor: '#8F9EB3' }} class="btn text-white" id="btnAgregar" data-dismiss="modal"  >Aceptar</button>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>



                <div class="card">

                    <div class="row mb-2 mt-2 ml-1">
                        <div class="col-12 col-lg-9">
                            <div class="row">

                                <div class="col-6 col-lg-1 d-flex align-items-center pb-sm-1">
                                    <div class="mr-2">
                                        <a href="#"
                                            class="btn rounded-pill btn-icon btn-sm" style={{ backgroundColor: '#78CB5A' }}> <span
                                                class="letter-icon text-white"><i className="icon-checkmark2"></i></span>
                                        </a>
                                    </div>
                                </div>

                                <div class="col-6 col-lg-1 d-flex align-items-center pb-sm-1">
                                    <div class="mr-2">
                                        <a href="#"
                                            class="btn  rounded-pill btn-icon btn-sm " style={{ backgroundColor: '#8189D4', borderColor: '#8189D4' }}> <span style={{ color: 'white' }}
                                                class="letter-icon">EP</span>
                                        </a>
                                    </div>

                                </div>



                                <div class="col-6 col-lg-7 d-flex align-items-center pb-sm-1">

                                    <div class="mr-5 ">
                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8189D4', borderColor: '#8189D4' }}
                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                class="letter-icon text-white">1</span>
                                        </a>
                                    </div>

                                    <div class="mr-5 ">
                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8189D4', borderColor: '#8189D4' }}
                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                class="letter-icon text-white">2</span>
                                        </a>
                                    </div>

                                    <div class="mr-5 ">
                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8189D4', borderColor: '#8189D4' }}
                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                class="letter-icon text-white">3</span>
                                        </a>
                                    </div>

                                    <div class="mr-5 ">
                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8189D4', borderColor: '#8189D4' }}
                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                class="letter-icon text-white">4</span>
                                        </a>
                                    </div>

                                    <div class="mr-5 ">
                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8189D4', borderColor: '#8189D4' }}
                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                class="letter-icon text-white">5</span>
                                        </a>
                                    </div>

                                    <div class="mr-5 ">
                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8189D4', borderColor: '#8189D4' }}
                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                class="letter-icon text-white">6</span>
                                        </a>
                                    </div>

                                    <div class="mr-5 ">
                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8189D4', borderColor: '#8189D4' }}
                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                class="letter-icon text-white">7</span>
                                        </a>
                                    </div>

                                    <div class="mr-5 ">
                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8189D4', borderColor: '#8189D4' }}
                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                class="letter-icon text-white">8</span>
                                        </a>
                                    </div>


                                    <div class="mr-5 ">
                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8189D4', borderColor: '#8189D4' }}
                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                class="letter-icon text-white">9</span>
                                        </a>
                                    </div>

                                    <div class="mr-5 ">
                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8189D4', borderColor: '#8189D4' }}
                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                class="letter-icon text-white">10</span>
                                        </a>
                                    </div>





                                </div>

                                &ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;    
                                        <div class="d-flex justify-content-between">
                                            <div class="mr-2">
                                                <a href="#"
                                                    class="btn  rounded-pill btn-icon btn-sm" style={{ borderColor: '#5B96E2' }}> <span style={{ color: '#5B96E2' }}
                                                        class="letter-icon">VC</span>
                                                </a>
                                            </div>

                                        </div>
                                        <div class="d-flex justify-content-between">
                                            <div class="mr-2">
                                                <a href="#"
                                                    class="btn  rounded-pill btn-icon btn-sm" style={{ borderColor: '#41B3D1' }}> <span style={{ color: '#41B3D1' }}
                                                        class="letter-icon">CC</span>
                                                </a>
                                            </div>

                                        </div>
                                        <div class="d-flex justify-content-between">
                                            <div class="mr-2">
                                                <a href="#"
                                                    class="btn  rounded-pill btn-icon btn-sm" style={{ borderColor: '#78CB5A' }}> <span style={{ color: '#78CB5A' }}
                                                        class="letter-icon">C</span>
                                                </a>
                                            </div>
                                        </div>
                            </div>
                        </div>


                        <div class="col-12 col-lg-3">
                            <div class="float-right mr-3">
                                <button type="button" title="Guardar y Regresar" onClick={(e) => this.props.regresaForm9explorando()} class="btn rounded-pill mr-1" style={{ backgroundColor: '#8F9EB3', borderColor: '#8F9EB3' }}><span><i style={{ color: 'white' }} className="icon-backward2"></i></span></button>
                                <button type="button" title="Registrar entrevista" onClick={(e) => this.validaGuardarFormulario() /*this.props.guardaFormulario10(this.state.prospecto)*/} disabled={false} class="btn rounded-pill mr-1" style={{ backgroundColor: '#8F9EB3', borderColor: '#8F9EB3' }}><span><i style={{ color: 'white' }} className="icon-forward3"></i></span></button>
                            </div>

                        </div>
                    </div>

                </div>
                <div class="card-header bg-transparent header-elements-sm-inline">
                    <h3 class="mb-0 font-weight-semibold" style={{ color: '#617187' }}>Entrevista Profunda - PSP & IRON TALENT </h3>
                    <div class="header-elements">
                        <ul class="list-inline mb-0">
                        <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Prospecto: <strong >{this.props.referido.nombre+' '+this.props.referido.ape_paterno + ' ' + (this.props.referido.ape_materno != null ? this.props.referido.ape_materno:'')}</strong></li>
                     <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Fecha de alta: <strong >{this.props.referido.ts_alta_audit}</strong></li>

                            <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Usuario creador: <strong >{this.state.entrevistador}</strong></li>
                            <li class="list-inline-item">
                                <h6 style={{ backgroundColor: "#8189D4", color: '#FFFF', borderRadius: '8px' }}> <>&nbsp;&nbsp;</> Entevista Profunda  <>&nbsp;&nbsp;</> </h6>
                            </li>
                        </ul>
                    </div>
                </div>
                <br></br>



                <div class="card-header bg-transparent header-elements-sm-inline" style={{ border: 'none' }}>
                    <h4 class="mb-0 font-weight-semibold" >IronTalent 2022</h4>
                    <div class="header-elements">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item">
                                {this.state.muestraInfoPSP == false &&
                                    <button data-toggle="modal" data-target="#modal-psp" 
                                    disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} class="btn  mr-1" style={{ backgroundColor: "#8189D4", color: '#FFFF' }}> <>&nbsp;&nbsp;</> Subir archivo PSP*  <>&nbsp;&nbsp;</> </button>
                                }
                                {this.state.muestraInfoPSP == true &&
                                    <button data-toggle="modal" data-target="#modal-psp" 
                                    disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} class="btn  mr-1" style={{ backgroundColor: "#78CB5A", color: '#FFFF' }}> <>&nbsp;&nbsp;</> Ver archivo PSP*  <>&nbsp;&nbsp;</> </button>
                                }
                            </li>
                        </ul>
                    </div>
                </div>

                <br></br>

                <div class="card">
                    <div class="card-header bg-transparent header-elements-sm-inline">
                        <h6 class="mb-0 font-weight-semibold">10. Factores knockout</h6>
                        <div class="header-elements">
                            <ul class="list-inline mb-0">
                                <li class="list-inline-item">
                                    <h6 class="mb-0 font-weight-semibold"> <>&nbsp;&nbsp;</> 10/10  <>&nbsp;&nbsp;</> </h6>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>





                <div className="row">
                    <div className="col-xs-6 col-lg-6">
                        <div class="card">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-xs-2 col-lg-11">
                                        <h8 class="mb-0 font-weight-semibold">Falta de patrón de éxito</h8>
                                    </div>
                                    <div className="col-xs-2 col-lg-1">
                                        <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="falta_patron" 
                                        disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                        && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospecto.falta_patron} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div className="col-xs-6 col-lg-6">
                        <div class="card">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-xs-2 col-lg-11">
                                        <h8 class="mb-0 font-weight-semibold">Incapacidad para aceptar capacitación</h8>
                                    </div>
                                    <div className="col-xs-2 col-lg-1">
                                        <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="incapacidad_aceptar" 
                                        disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                        && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospecto.incapacidad_aceptar} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div className="row">
                    <div className="col-xs-6 col-lg-6">
                        <div class="card">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-xs-2 col-lg-11">
                                        <h8 class="mb-0 font-weight-semibold">Bajo nivel de energía</h8>
                                    </div>
                                    <div className="col-xs-2 col-lg-1">
                                        <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="bajo_nivel" 
                                        disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                        && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospecto.bajo_nivel} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div className="col-xs-6 col-lg-6">
                        <div class="card">
                            <div className="card-body">
                                <div className="row">

                                    <div className="col-xs-2 col-lg-11">
                                        <h8 class="mb-0 font-weight-semibold">Recientemente divorciado</h8>
                                    </div>
                                    <div className="col-xs-2 col-lg-1">
                                        <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="recientemente_divorciado" 
                                        disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                        && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospecto.recientemente_divorciado} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div className="row">

                    <div className="col-xs-6 col-lg-6">
                        <div class="card">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-xs-2 col-lg-11">
                                        <h8 class="mb-0 font-weight-semibold">Malos hábitos de vida</h8>
                                    </div>
                                    <div className="col-xs-2 col-lg-1">
                                        <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="malos_habitos" 
                                        disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                        && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospecto.malos_habitos} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div className="col-xs-6 col-lg-6">
                        <div class="card">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-xs-2 col-lg-11">
                                        <h8 class="mb-0 font-weight-semibold">Demasiados cambios de trabajo / inestabilidad</h8>
                                    </div>
                                    <div className="col-xs-2 col-lg-1">
                                        <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="demasiados_cambios" 
                                        disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                        && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospecto.demasiados_cambios} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-xs-6 col-lg-6">
                        <div class="card">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-xs-2 col-lg-11">
                                        <h8 class="mb-0 font-weight-semibold">Mercado natural débil-falta de contactos</h8>
                                    </div>
                                    <div className="col-xs-2 col-lg-1">
                                        <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="mercado_natural" disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                        && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospecto.mercado_natural} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div className="col-xs-6 col-lg-6">
                        <div class="card">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-xs-2 col-lg-11">
                                        <h8 class="mb-0 font-weight-semibold">Benefactor profesional</h8>
                                    </div>
                                    <div className="col-xs-2 col-lg-1">
                                        <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="benefactor_profecional" disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                        && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospecto.benefactor_profecional} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div className="row">

                    <div className="col-xs-6 col-lg-6">
                        <div class="card">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-xs-2 col-lg-11">
                                        <h8 class="mb-0 font-weight-semibold">Falta de movilidad social</h8>
                                    </div>
                                    <div className="col-xs-2 col-lg-1">
                                        <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="falta_movilidad" disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                        && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospecto.falta_movilidad} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div className="col-xs-6 col-lg-6">
                        <div class="card">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-xs-2 col-lg-11">
                                        <h8 class="mb-0 font-weight-semibold">Problemas de salud</h8>
                                    </div>
                                    <div className="col-xs-2 col-lg-1">
                                        <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="problemas_salud" 
                                        disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                        && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospecto.problemas_salud} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


                <div className="row">

                    <div className="col-xs-6 col-lg-6">
                        <div class="card">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-xs-2 col-lg-11">
                                        <h8 class="mb-0 font-weight-semibold">Problemas financieros serios / Problemas crediticios / Bancarrota reciente</h8>
                                    </div>
                                    <div className="col-xs-2 col-lg-1">
                                        <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="problemas_financieros" 
                                        disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                        && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospecto.problemas_financieros} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div className="col-xs-6 col-lg-6">
                        <div class="card">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-xs-2 col-lg-11">
                                        <h8 class="mb-0 font-weight-semibold">Ve las ventas como un retroceso en lugar de una oportunidad</h8>
                                    </div>
                                    <div className="col-xs-2 col-lg-1">
                                        <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="ventas_retroceso" 
                                        disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                        && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospecto.ventas_retroceso} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div className="row">

                    <div className="col-xs-6 col-lg-6">
                        <div class="card">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-xs-2 col-lg-11">
                                        <h8 class="mb-0 font-weight-semibold">Culpa a los demás por su falta de éxito</h8>
                                    </div>
                                    <div className="col-xs-2 col-lg-1">
                                        <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="culpa_demas" 
                                        disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                        && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospecto.culpa_demas} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div className="col-xs-6 col-lg-6">
                        <div class="card">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-xs-2 col-lg-11">
                                        <h8 class="mb-0 font-weight-semibold">Necesita cualquier trabajo inmediata y desesperadamente</h8>
                                    </div>
                                    <div className="col-xs-2 col-lg-1">
                                        <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="cualquier_trabajo" 
                                        disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                        && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospecto.cualquier_trabajo} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div className="row">

                    <div className="col-xs-6 col-lg-6">
                        <div class="card">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-xs-2 col-lg-11">
                                        <h8 class="mb-0 font-weight-semibold">No cree en el seguro de vida</h8>
                                    </div>
                                    <div className="col-xs-2 col-lg-1">
                                        <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="seguros_vida" 
                                        disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                        && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospecto.seguros_vida} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div className="col-xs-6 col-lg-6">
                        <div class="card">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-xs-2 col-lg-11">
                                        <h8 class="mb-0 font-weight-semibold">Satisfecho con el status Quo</h8>
                                    </div>
                                    <div className="col-xs-2 col-lg-1">
                                        <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="status_quo" 
                                        disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                        && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospecto.status_quo} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-xs-12 col-lg-12">
                        <h5 class="mb-0 font-weight-semibold">Resultado de la entrevista</h5>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-12 col-lg-12">
                        <div className="row">
                            <div className="col-xs-2 col-lg-2">
                                <div class="card">
                                    <div class="form-check form-check-inline" style={{ paddingLeft: '8px' }}>
                                        <input class="form-check-input" type="radio" name="resultado_entrevista" id="descarta" value={0} 
                                        onChange={this.onChange} disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                        && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} checked={this.state.prospecto.resultado_entrevista == 0} />
                                        <h6 class="form-check-label mb-0 font-weight-semibold" for="descarta">Se descarta</h6>
                                    </div>
                                </div>
                            </div>


                            <div className="col-xs-2 col-lg-2">
                                <div class="card">
                                    <div class="form-check form-check-inline" style={{ paddingLeft: '8px' }}>
                                        <input class="form-check-input" type="radio" name="resultado_entrevista" id="esperaPSP" disabled={this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                    && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER" || (this.state.muestraDocPSP)} value={1} onChange={this.onChange} checked={this.state.prospecto.resultado_entrevista == 1} />
                                        <h6 class="form-check-label mb-0 font-weight-semibold" for="esperaPSP">En espera de PSP</h6>
                                    </div>
                                </div>
                            </div>


                            <div className="col-xs-2 col-lg-2">
                                <div class="card">
                                    <div class="form-check form-check-inline" style={{ paddingLeft: '8px' }}>
                                        <input class="form-check-input" type="radio" name="resultado_entrevista" id="rechazo" disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} value={2} onChange={this.onChange} checked={this.state.prospecto.resultado_entrevista == 2} />
                                        <h6 class="form-check-label mb-0 font-weight-semibold" for="rechazo">Rechazó</h6>
                                    </div>
                                </div>
                            </div>


                            <div className="col-xs-2 col-lg-2">
                                <div class="card">
                                    <div class="form-check form-check-inline" style={{ paddingLeft: '8px' }}>
                                        <input class="form-check-input" type="radio" name="resultado_entrevista" id="avanza" disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} value={3} onChange={this.onChange} checked={this.state.prospecto.resultado_entrevista == 3} />
                                        <h6 class="form-check-label mb-0 font-weight-semibold" for="avanza">Avanza</h6>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xs-2 col-lg-2">
                                <div class="card">
                                    <div class="form-check form-check-inline" style={{ paddingLeft: '8px' }}>
                                        <input class="form-check-input" type="radio" name="resultado_entrevista" id="reserva" disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} value={4} onChange={this.onChange} checked={this.state.prospecto.resultado_entrevista == 4} />
                                        <h6 class="form-check-label mb-0 font-weight-semibold" for="reserva">Enviar a reserva</h6>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>

                {
                    this.state.prospecto.resultado_entrevista == 2 &&
                    <div className="row">
                    <div className="col-xs-12 col-lg-12">
                        <div className="form-group">
                            <span >
                                <span className="font-weight-semibold">Comentarios de rechazo</span>
                            </span>
                            <div className="input-group">
                                <textarea type="text" placeholder="Escribir" name="causas_rechazo" onChange={this.onChange} disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.causas_rechazo} className="form-control " style={{ borderColor: '#C8CDF6', backgroundColor: '#FFFF' }} />
                            </div>
                        </div>
                    </div>
                </div>

                }

                {
                    this.state.prospecto.resultado_entrevista == 3 &&
                    <div className="row">
                    <div className="col-xs-12 col-lg-12">
                        <div className="form-group">
                            <span >
                                <span className="font-weight-semibold">Selecciona un GDD</span>
                            </span>
                            <div className="input-group">
                                {this.state.selectGDD}
                            </div>
                        </div>
                    </div>
                </div>
                }
                                {
                    this.state.prospecto.resultado_entrevista == 3 &&
                    <div className="row">
                    <div className="col-xs-12 col-lg-12">
                        <div className="form-group">
                            <span >
                                <span className="font-weight-semibold">Selecciona un SDDC</span>
                            </span>
                            <div className="input-group">
                                {this.state.selectSDDC}
                            </div>
                        </div>
                    </div>
                </div>
                }
                <HistoricoGeneral idProspecto={this.props.idProspecto} tipoUsuario={this.state.tipoUsuario} presentarSeccion = {"EP9"} />
            </div>
        )
    }
}