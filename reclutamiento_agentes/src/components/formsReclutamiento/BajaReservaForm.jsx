import React, { Component } from "react";
import HistoricoGeneral from "./HistoricoGeneral";
import datos from '../urls/datos.json'

const queryParams = new URLSearchParams(window.location.search)

export default class BajaReservaForm extends Component {

    constructor() {
        super();
        this.state = {
            referido: {},
        }
    }

    UNSAFE_componentWillMount() {
        console.log("entrando es render de venta de carrera ", this)
        fetch(datos.urlServicePy+"parametros/api_recluta_prospectos/" + this.props.idProspecto)
            .then(response => response.json())
            .then(json => {
                let fila = json.filas[0].fila
                let columnas = json.columnas
                let columnasSalida = {}
                for (var i = 0; i < fila.length; i++) {
                    console.log(fila[i])
                    columnasSalida["" + columnas[i].key + ""] = fila[i].value
                }
                this.setState({ referido: columnasSalida })
            })
    }

    render() {
        return (
            <div>


                <div class="card">
                    <div class="row mb-2 mt-2 ml-1">
                    <div class="col-12 col-lg-12">
                            <div class="float-right mr-3">
                                <button type="button" title="Guardar y Regresar" onClick={(e) => this.props.botonCancelar()} class="btn rounded-pill mr-1" style={{ backgroundColor: '#8F9EB3', borderColor: '#8F9EB3' }}><span><i style={{ color: 'white' }} className="icon-backward2"></i></span></button>
                                
                            </div>

                        </div>
                    </div>
                </div>

                <div class="card-header bg-transparent header-elements-sm-inline">
                    <h3 class="mb-0 font-weight-semibold" style={{ color: '#617187' }}>Formularios del prospecto</h3>
                    <div class="header-elements">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Prospecto: <strong >{this.state.referido.nombre + ' ' + this.state.referido.ape_paterno + ' ' + (this.state.referido.ape_materno != null ? this.state.referido.ape_materno : '')}</strong></li>
                            {  /*<li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Fecha de registro de Venta de Carrera: <strong >{this.state.referido.ts_alta_audit}</strong></li>
                            <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Entrevistador: <strong >{this.state.entrevistador}</strong></li>
                            <li class="list-inline-item">
                                <h6 style={{ backgroundColor: "#5B96E2", color: '#FFFF', borderRadius: '8px' }}> <>&nbsp;&nbsp;</>Venta de Carrera<>&nbsp;&nbsp;</> </h6>
                            </li>*/}
                        </ul>
                    </div>
                </div>

                <HistoricoGeneral idProspecto={this.props.idProspecto} tipoUsuario={this.state.tipoUsuario} presentarSeccion = {"EP14"} />
            </div>
        )
    }

}