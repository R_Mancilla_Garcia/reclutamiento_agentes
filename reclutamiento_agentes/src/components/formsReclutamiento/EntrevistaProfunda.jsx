import React, { Component } from "react";
import EntrevistaProfundaExplorando from "./EntrevistaProfundaExplorando";
import EntrevistaProfundaExplorando3 from "./EntrevistaProfundaExplorando3";
import EntrevistaProfundaExplorando4 from "./EntrevistaProfundaExplorando4";
import EntrevistaProfundaExplorando5 from "./EntrevistaProfundaExplorando5";
import EntrevistaProfundaExplorando6 from "./EntrevistaProfundaExplorando6";
import EntrevistaProfundaExplorando7 from './EntrevistaProfundaExplorando7';
import EntrevistaProfundaExplorando8 from './EntrevistaProfundaExplorando8';
import EntrevistaProfundaExplorando9 from './EntrevistaProfundaExplorando9';
import EntrevistaProfundaExplorando10 from './EntrevistaProfundaExplorando10';
import HistoricoGeneral from "./HistoricoGeneral";
import axios from 'axios';
import datos from '../urls/datos.json'

export default class EntrevistaProfunda extends Component {
    constructor() {
        super();
        this.muestraForm2Explorando = this.muestraForm2Explorando.bind(this)
        this.construyeSelectReferido = this.construyeSelectReferido.bind(this)
        this.construyeSelect = this.construyeSelect.bind(this)
        this.construyeSelectDependienteDeCiudad = this.construyeSelectDependienteDeCiudad.bind(this)
        this.construyeSelectDependienteDeMunicipio = this.construyeSelectDependienteDeMunicipio.bind(this)
        this.muestraForm3Explorando = this.muestraForm3Explorando.bind(this)
        this.muestraForm4Explorando = this.muestraForm4Explorando.bind(this)
        this.muestraForm5Explorando = this.muestraForm5Explorando.bind(this)
        this.muestraForm6Explorando = this.muestraForm6Explorando.bind(this)
        this.muestraForm7Explorando = this.muestraForm7Explorando.bind(this)
        this.muestraForm8Explorando = this.muestraForm8Explorando.bind(this)
        this.muestraForm9Explorando = this.muestraForm9Explorando.bind(this)
        this.muestraForm10Explorando = this.muestraForm10Explorando.bind(this)
        this.guardaFormulario10 = this.guardaFormulario10.bind(this)
        this.construyeSelectEdicion = this.construyeSelectEdicion.bind(this)


        this.regresaForm1explorando = this.regresaForm1explorando.bind(this)
        this.regresaForm2explorando = this.regresaForm2explorando.bind(this)
        this.regresaForm3explorando = this.regresaForm3explorando.bind(this)
        this.regresaForm4explorando = this.regresaForm4explorando.bind(this)
        this.regresaForm5explorando = this.regresaForm5explorando.bind(this)
        this.regresaForm6explorando = this.regresaForm6explorando.bind(this)
        this.regresaForm7explorando = this.regresaForm7explorando.bind(this)
        this.regresaForm8explorando = this.regresaForm8explorando.bind(this)
        this.regresaForm9explorando = this.regresaForm9explorando.bind(this)

        this.validaFuisteAgente = this.validaFuisteAgente.bind(this)
        this.enviarVentaCarrea = this.enviarVentaCarrea.bind(this)

        this.onClickBotonArchivoPuesto = this.onClickBotonArchivoPuesto.bind(this)
        this.onClickBotonArchivoPSP = this.onClickBotonArchivoPSP.bind(this)
        this.cargaInfoPSP = this.cargaInfoPSP.bind(this)
        this.cargaPdf14 = this.cargaPdf14.bind(this)

        this.state = {
            muestraEncuentro: true,
            muestraExplorando: false,
            muestraExplorando3: false,
            muestraExplorando4: false,
            muestraExplorando5: false,
            muestraExplorando6: false,
            muestraExplorando7: false,
            muestraExplorando8: false,
            muestraExplorando9: false,
            muestraExplorando10: false,
            entrevistador:'',
            selectEDAT: [],
            selectFuenteReclutamiento: [],
            selectCiudad: [],
            selectMunicipio: [],
            selectColonia: [],
            ciudad_id: 0,
            municipio_id: 0,
            colonia_id: 0,
            referido: {},
            propspecto: {
                "prospecto_id": 0,
                "id": 0,
                "estatus_id": 5,
                "carrera": null,
                "cp": null,
                "ciudad_id": null,
                "municipio_id": null,
                "colonia_id": null,
                "calle": null,
                "num_ext": null,
                "num_int": null,
                "universidad": null,
                "infonavit": null,
                "monto_credito": null,
                "fuiste_agente": null,
                "grupo_bal": null,
                "gobierno": null,
                "vivienda_resides": null,
                "vivienda_resides_otro": null,
                "conquien_vives": null,
                "conquien_vives_otro": null,
                "medio_transporte": null,
                "medio_transporte_otro": null,
                "ganar_millon": null,
                "facebook": null,
                "instagram": null,
                "twitter": null,
                "observaciones": null
            },
            colorSubirArchivo: "#313A46",
            colorTextoSubirArchivo: "#617187",
            colorBotonSubirArchivo: "#8189D4",
            nombreArchivoPuesto: "",
            archivoPuesto: undefined,
            muestraDocPuesto: false,
            muestraDocCV: false,
            //  muestraDocPSP: false,

            colorSubirArchivoCV: "#E1E5F0",
            colorTextoSubirArchivoCV: "#617187",
            colorBotonSubirArchivoCV: "#0F69B8",
            nombreArchivoCV: "",
            archivoCV: undefined,

            archivoPSP: undefined,
            muestraDocPSP: false,
            colorSubirArchivoPSP: "#E1E5F0",
            colorTextoSubirArchivoPSP: "#617187",
            colorBotonSubirArchivoPSP: "#313A46",
            nombreArchivoPSP: "",
            archivoPSP: undefined,
            textoPuesto: 'Subir archivo',

            muestraInfoPSP: false,
            eficaciaVentas: 0,
            eficaciaEmpresarial: 0,
            rendimientoVentas: 0,
            fechaRespPSP: '',
            ventaDominante: '',
            interpretacion: '',
            rutaPSP: '',


            contadorVacaciones: 0,

            textoPuesto: 'Subir archivo'





        };
    }
    enviarVentaCarrea() { // se deja el nombre del metodo, pero el estatus a donde se dirige realmente es a "Conociendo a tu Candidato"
        let json = {
            "estatus_id": 24
        }
        const requestOptions = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };
        console.log("enviando a reserva")

        fetch(datos.urlServicePy+"parametros_upd/api_recluta_prospectos/" + this.props.idProspecto, requestOptions)
            .then(response => response.json())
            .then(data => {
                //this.construyeTarjetas()
                this.props.botonCancelar()
            })

    }


    guardaFormulario10(prospectoJson) {
        prospectoJson.id = this.props.idProspecto
        prospectoJson.prospecto_id = this.props.idProspecto
        fetch(datos.urlServicePy+"recluta/api_recluta_knockout10/" + this.props.idProspecto)
            .then(response => response.json())
            .then(existeProspecto => {
                let verbo = "";
                let url = datos.urlServicePy+"recluta/api_recluta_knockout10/"
                if (existeProspecto.length > 0) {
                    url = url + this.props.idProspecto
                    verbo = "POST"
                } else {
                    url = url + "0"
                    verbo = "POST"
                }
                const requestOptions = {
                    method: verbo,
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(prospectoJson)
                };

                fetch(url, requestOptions)
                    .then(response => response.json())
                    .then(data => {
                        //this.props.botonCancelar();
                        if(prospectoJson.resultado_entrevista == 3){
                        this.enviarVentaCarrea()
                        }else{
                            this.props.botonCancelar()
                        }
                        
                    })
            })
    }
    muestraForm10Explorando(prospectoJson) {
        prospectoJson.id = this.props.idProspecto
        prospectoJson.prospecto_id = this.props.idProspecto
        console.log("entrando a paso 4")
        fetch(datos.urlServicePy+"recluta/api_recluta_factoresimp09/" + this.props.idProspecto)
            .then(response => response.json())
            .then(existeProspecto => {
                let verbo = "";
                let url = datos.urlServicePy+"recluta/api_recluta_factoresimp09/"
                if (existeProspecto.length > 0) {
                    url = url + this.props.idProspecto
                    verbo = "POST"
                } else {
                    url = url + "0"
                    verbo = "POST"
                }
                const requestOptions = {
                    method: verbo,
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(prospectoJson)
                };

                fetch(url, requestOptions)
                    .then(response => response.json())
                    .then(data => {

                    })
                this.setState({ muestraExplorando10: true, muestraExplorando9: false, muestraExplorando8: false, muestraExplorando7: false, muestraExplorando6: false, muestraExplorando5: false, muestraExplorando4: false, muestraExplorando3: false, muestraEncuentro: false, muestraExplorando: false })
            })
        //  this.setState({ muestraExplorando10: true, muestraExplorando9: false, muestraExplorando8: false, muestraExplorando7: false, muestraExplorando6: false, muestraExplorando5: false, muestraExplorando4: false, muestraExplorando3: false, muestraEncuentro: false, muestraExplorando: false })
    }
    regresaForm9explorando() {
        this.setState({ muestraExplorando9: true, muestraExplorando8: false, muestraExplorando7: false, muestraExplorando6: false, muestraExplorando5: false, muestraExplorando4: false, muestraExplorando3: false, muestraEncuentro: false, muestraExplorando: false })
    }

    muestraForm9Explorando(prospectoJson) {
        prospectoJson.id = this.props.idProspecto
        prospectoJson.prospecto_id = this.props.idProspecto
        console.log("entrando a paso 9", prospectoJson)
        fetch(datos.urlServicePy+"recluta/api_recluta_vitales08/" + this.props.idProspecto)
            .then(response => response.json())
            .then(existeProspecto => {
                let verbo = "";
                let url = datos.urlServicePy+"recluta/api_recluta_vitales08/"
                if (existeProspecto.length > 0) {
                    url = url + this.props.idProspecto
                    verbo = "POST"
                } else {
                    url = url + "0"
                    verbo = "POST"
                }
                const requestOptions = {
                    method: verbo,
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(prospectoJson)
                };

                fetch(url, requestOptions)
                    .then(response => response.json())
                    .then(data => {
                        let jsonNotificacion = {
                            "mail_id": 4,
                            "prospecto_id": this.props.idProspecto
                        }
                        const requestOptions = {
                            method: "POST",
                            headers: { 'Content-Type': 'application/json' },
                            body: JSON.stringify(jsonNotificacion)
                        };

                        fetch(datos.urlServicePy+"recluta/api_notificador/0", requestOptions)
                            .then(response => response.json())
                            .then(data => { })

                    })
                this.setState({ muestraExplorando9: true, muestraExplorando8: false, muestraExplorando7: false, muestraExplorando6: false, muestraExplorando5: false, muestraExplorando4: false, muestraExplorando3: false, muestraEncuentro: false, muestraExplorando: false })
            })

        this.setState({ muestraExplorando9: true, muestraExplorando8: false, muestraExplorando7: false, muestraExplorando6: false, muestraExplorando5: false, muestraExplorando4: false, muestraExplorando3: false, muestraEncuentro: false, muestraExplorando: false })
    }


    regresaForm8explorando() {
        this.setState({ muestraExplorando8: true, muestraExplorando7: false, muestraExplorando6: false, muestraExplorando5: false, muestraExplorando4: false, muestraExplorando3: false, muestraEncuentro: false, muestraExplorando: false })
    }
    muestraForm8Explorando(prospectoJson) {

        prospectoJson.id = this.props.idProspecto
        prospectoJson.prospecto_id = this.props.idProspecto
        console.log("entrando a paso 8", prospectoJson)
        fetch(datos.urlServicePy+"recluta/api_recluta_evaluacion07/" + this.props.idProspecto)
            .then(response => response.json())
            .then(existeProspecto => {
                let verbo = "";
                let url = datos.urlServicePy+"recluta/api_recluta_evaluacion07/"
                if (existeProspecto.length > 0) {
                    url = url + this.props.idProspecto
                    verbo = "POST"
                } else {
                    url = url + "0"
                    verbo = "POST"
                }
                const requestOptions = {
                    method: verbo,
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(prospectoJson)
                };

                fetch(url, requestOptions)
                    .then(response => response.json())
                    .then(data => {

                    })
                this.setState({ muestraExplorando8: true, muestraExplorando7: false, muestraExplorando6: false, muestraExplorando5: false, muestraExplorando4: false, muestraExplorando3: false, muestraEncuentro: false, muestraExplorando: false })
            })
        // this.setState({ muestraExplorando8: true, muestraExplorando7: false, muestraExplorando6: false, muestraExplorando5: false, muestraExplorando4: false, muestraExplorando3: false, muestraEncuentro: false, muestraExplorando: false })


    }

    regresaForm7explorando() {
        this.setState({ muestraExplorando7: true, muestraExplorando6: false, muestraExplorando5: false, muestraExplorando4: false, muestraExplorando3: false, muestraEncuentro: false, muestraExplorando: false })
    }
    muestraForm7Explorando(prospectoJson) {
        prospectoJson.id = this.props.idProspecto
        prospectoJson.prospecto_id = this.props.idProspecto
        prospectoJson.estatus_id = 10
        console.log("entrando a paso 4")
        fetch(datos.urlServicePy+"recluta/api_recluta_areasvida06/" + this.props.idProspecto)
            .then(response => response.json())
            .then(existeProspecto => {
                let verbo = "";
                let url = datos.urlServicePy+"recluta/api_recluta_areasvida06/"
                if (existeProspecto.length > 0) {
                    url = url + this.props.idProspecto
                    verbo = "POST"
                } else {
                    url = url + "0"
                    verbo = "POST"
                }
                const requestOptions = {
                    method: verbo,
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(prospectoJson)
                };

                fetch(url, requestOptions)
                    .then(response => response.json())
                    .then(data => {

                    })
                this.setState({ muestraExplorando7: true, muestraExplorando6: false, muestraExplorando5: false, muestraExplorando4: false, muestraExplorando3: false, muestraEncuentro: false, muestraExplorando: false })
            })
        //this.setState({ muestraExplorando7: true, muestraExplorando6: false, muestraExplorando5: false, muestraExplorando4: false, muestraExplorando3: false, muestraEncuentro: false, muestraExplorando: false })

    }

    regresaForm6explorando() {
        this.setState({ muestraExplorando6: true, muestraExplorando5: false, muestraExplorando4: false, muestraExplorando3: false, muestraEncuentro: false, muestraExplorando: false })

    }


    muestraForm6Explorando(prospectoJson) {
        prospectoJson.id = this.props.idProspecto
        prospectoJson.prospecto_id = this.props.idProspecto
        fetch(datos.urlServicePy+"recluta/api_recluta_expagente05/" + this.props.idProspecto)
            .then(response => response.json())
            .then(existeProspecto => {
                let verbo = "";
                let url = datos.urlServicePy+"recluta/api_recluta_expagente05/"
                if (existeProspecto.length > 0) {
                    url = url + this.props.idProspecto
                    verbo = "POST"
                } else {
                    url = url + "0"
                    verbo = "POST"
                }
                const requestOptions = {
                    method: verbo,
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(prospectoJson)
                };

                fetch(url, requestOptions)
                    .then(response => response.json())
                    .then(data => {

                    })
                this.setState({ muestraExplorando6: true, muestraExplorando5: false, muestraExplorando4: false, muestraExplorando3: false, muestraEncuentro: false, muestraExplorando: false })

            })
    }
    regresaForm5explorando() {
        fetch(datos.urlServicePy+"recluta/api_recluta_encuentro01/" + this.props.idProspecto)
            .then(response => response.json())
            .then(existeProspecto => {
                let bandera = existeProspecto[0].fuiste_agente == 1 || existeProspecto[0].fuiste_agente == 2
                console.log("la bandera fuite agente? ", existeProspecto)
                if (bandera) {
                    this.setState({ muestraExplorando5: true, muestraExplorando4: false, muestraExplorando3: false, muestraEncuentro: false, muestraExplorando: false })
                } else {
                    this.setState({ muestraExplorando4: true, muestraExplorando3: false, muestraEncuentro: false, muestraExplorando: false })
                }

            })




    }

    validaFuisteAgente() {
        fetch(datos.urlServicePy+"recluta/api_recluta_encuentro01/" + this.props.idProspecto)
            .then(response => response.json())
            .then(existeProspecto => {
                console.log("existe prospecto ",existeProspecto)
                let bandera = existeProspecto[0].fuiste_agente == 1 || existeProspecto[0].fuiste_agente == 2
                console.log("la bandera fuite agente? ", existeProspecto)
                if (bandera) {
                    // si fue agente
                    console.log("debe mostrar el ")
                    this.setState({ muestraExplorando5: true, muestraExplorando4: false, muestraExplorando3: false, muestraEncuentro: false, muestraExplorando: false })

                } else {
                    //no fue agente
                    this.setState({ muestraExplorando6: true, muestraExplorando5: false, muestraExplorando4: false, muestraExplorando3: false, muestraEncuentro: false, muestraExplorando: false })
                }

            })
    }
    muestraForm5Explorando(prospectoJson) {
        prospectoJson.id = this.props.idProspecto
        prospectoJson.prospecto_id = this.props.idProspecto
        fetch(datos.urlServicePy+"recluta/api_recluta_encuentro04/" + this.props.idProspecto)
            .then(response => response.json())
            .then(existeProspecto => {
                let verbo = "";
                let url = datos.urlServicePy+"recluta/api_recluta_encuentro04/"
                if (existeProspecto.length > 0) {
                    url = url + this.props.idProspecto
                    verbo = "POST"
                } else {
                    url = url + "0"
                    verbo = "POST"
                }
                const requestOptions = {
                    method: verbo,
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(prospectoJson)
                };

                fetch(url, requestOptions)
                    .then(response => response.json())
                    .then(data => {

                    })
                this.validaFuisteAgente()
            })
    }
    regresaForm4explorando() {
        this.setState({ muestraExplorando4: true, muestraExplorando3: false, muestraEncuentro: false, muestraExplorando: false })
    }
    muestraForm4Explorando(prospectoJson) {

        prospectoJson.id = this.props.idProspecto
        prospectoJson.prospecto_id = this.props.idProspecto
        fetch(datos.urlServicePy+"recluta/api_recluta_encuentro03/" + this.props.idProspecto)
            .then(response => response.json())
            .then(existeProspecto => {
                let verbo = "";
                let url = datos.urlServicePy+"recluta/api_recluta_encuentro03/"
                if (existeProspecto.length > 0) {
                    url = url + this.props.idProspecto
                    verbo = "POST"
                } else {
                    url = url + "0"
                    verbo = "POST"
                }
                const requestOptions = {
                    method: verbo,
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(prospectoJson)
                };

                fetch(url, requestOptions)
                    .then(response => response.json())
                    .then(data => {

                    })

                this.setState({ muestraExplorando4: true, muestraExplorando3: false, muestraEncuentro: false, muestraExplorando: false })
            })
        console.log("entrando a paso 4")

    }

    regresaForm3explorando() {
        this.setState({ muestraExplorando3: true, muestraEncuentro: false, muestraExplorando: false })

    }

    muestraForm3Explorando(prospectoJson) {
        prospectoJson.id = this.props.idProspecto
        prospectoJson.prospecto_id = this.props.idProspecto
        fetch(datos.urlServicePy+"recluta/api_recluta_encuentro02/" + this.props.idProspecto)
            .then(response => response.json())
            .then(existeProspecto => {
                let verbo = "";
                let url = datos.urlServicePy+"recluta/api_recluta_encuentro02/"
                if (existeProspecto.length > 0) {
                    url = url + this.props.idProspecto
                    verbo = "POST"
                } else {
                    url = url + "0"
                    verbo = "POST"
                }
                const requestOptions = {
                    method: verbo,
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(prospectoJson)
                };

                fetch(url, requestOptions)
                    .then(response => response.json())
                    .then(data => {

                    })
                this.setState({ muestraExplorando3: true, muestraEncuentro: false, muestraExplorando: false })

            })
        //console.log("entrando a paso 3")
    }

    regresaForm2explorando() {
        this.setState({ muestraExplorando3: false, muestraEncuentro: false, muestraExplorando: true })
    }

    muestraForm2Explorando() {
        console.log(this.state.propspecto)

        fetch(datos.urlServicePy+"recluta/api_recluta_encuentro01/" + this.props.idProspecto)
            .then(response => response.json())
            .then(existeProspecto => {
                let prospectoJson = this.state.propspecto
                prospectoJson.ciudad_id = this.state.propspecto.ciudad_id
                prospectoJson.municipio_id = this.state.propspecto.municipio_id
                prospectoJson.colonia_id = this.state.propspecto.colonia_id
                prospectoJson.prospecto_id = this.props.idProspecto
                prospectoJson.id = this.props.idProspecto
                let verbo = "";
                let url = datos.urlServicePy+"recluta/api_recluta_encuentro01/"
                if (existeProspecto.length > 0) {
                    url = url + this.props.idProspecto
                    verbo = "POST"
                } else {
                    url = url + "0"
                    verbo = "POST"
                }
                const requestOptions = {
                    method: verbo,
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(prospectoJson)
                };

                fetch(url, requestOptions)
                    .then(response => response.json())
                    .then(data => {

                    })
                this.setState({ muestraEncuentro: false, muestraExplorando: true })
            })

    }

    regresaForm1explorando() {

        fetch(datos.urlServicePy+"recluta/api_recluta_encuentro01/" + this.props.idProspecto)
            .then(response => response.json())
            .then(existeProspecto => {
                this.setState({ muestraEncuentro: true, muestraExplorando: false })

            })

    }



    construyeSelect(url, selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                console.log("respuesta de las ciudades ", json)
                for (var i = 0; i < filas.length; i++) {

                    let fila = filas[i]
                    if (selector == "selectCiudad") {
                        // if (this.props.idEmpleado) {

                        if (this.state.ciudad_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }

                        // } else {
                        //   options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        // }

                    } else if (selector == "selectParentesco") {


                        if (this.props.idEmpleado) {

                            if (this.state.parentesco_id == fila.fila[0].value) {
                                options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            } else {
                                options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            }

                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }

                    }



                }
                let salida = []
                salida.push(<select onChange={this.onChange} 
                    disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} name={selector} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ [selector]: salida })
            });
    }

    construyeSelectEdicion(url, selector, id) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                console.log("respuesta de las ciudades ", json)
                for (var i = 0; i < filas.length; i++) {

                    let fila = filas[i]
                    if (selector == "selectCiudad") {

                        if (this.state.propspecto.ciudad_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }
                    }

                    if (selector == "selectMunicipio") {

                        if (this.state.propspecto.municipio_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[2].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[2].value}</option>)
                        }
                    }
                    if (selector == "selectColonia") {

                        if (this.state.propspecto.colonia_id == fila.fila[1].value) {
                            options.push(<option selected value={fila.fila[1].value}>{fila.fila[2].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[1].value}>{fila.fila[2].value}</option>)
                        }
                    }



                }
                let salida = []
                salida.push(<select onChange={this.onChange} 
                    disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} name={selector} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ [selector]: salida })
            });
    }

    construyeSelectDependienteDeCiudad(url, selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                this.setState({ ["selectMunicipio"]: null })
                this.setState({ ["selectColonia"]: null })
                for (var i = 0; i < filas.length; i++) {
                    let fila = filas[i]
                    if (this.state.ciudad_id == fila.fila[1].value) {
                        if (this.state.municipio_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[2].value}</option>)
                            this.construyeSelectDependienteDeMunicipio(datos.urlServicePy+"parametros/api_cat_colonia/"+fila.fila[0].value, "selectColonia", 10)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[2].value}</option>)
                        }
                    }
                }
                let salida = []
                salida.push(<select onChange={this.onChange} 
                    disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} name={selector} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ [selector]: salida })
            });
    }

    construyeSelectDependienteDeMunicipio(url, selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []

                /*if (this.props.idEmpleado == null)*/ options.push(<option selected value={0}>Seleccionar</option>)

                for (var i = 0; i < filas.length; i++) {

                    let fila = filas[i]
                    //   options.push(<option value={fila.fila[0].value}>{fila.fila[2].value}</option>)
                    //if (this.props.idEmpleado != null) {
                    if (this.state.municipio_id == fila.fila[1].value) {
                        if (this.state.colonia_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[2].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[2].value}</option>)
                        }
                    }
                    //}

                    /* if (this.props.idEmpleado == null) {
                         if (1 == fila.fila[1].value) {
                             if (1 == fila.fila[0].value) {
                                 options.push(<option selected value={fila.fila[0].value}>{fila.fila[2].value}</option>)
 
                             } else {
                                 options.push(<option value={fila.fila[0].value}>{fila.fila[2].value}</option>)
                             }
                         }
                     }*/

                }
                let salida = []
                salida.push(<select style={{ borderColor: '#E1E5F0' }} 
                disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} name={selector} onChange={this.onChange} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ [selector]: salida })
            });
    }


    onChange = e => {
        console.log("entrando", e.target.name, e.target.value)
        if ("selectMunicipio" == e.target.name) {
            console.log("entroa asleect")
            let prospecto = this.state.propspecto
            prospecto["municipio_id"] = parseInt(e.target.value)
            this.setState({ municipio_id: parseInt(e.target.value) })
            this.construyeSelectDependienteDeMunicipio(datos.urlServicePy+"parametros/api_cat_colonia/"+parseInt(e.target.value), "selectColonia")

        } else if ("selectCiudad" == e.target.name) {
            console.log("entro")
            let prospecto = this.state.propspecto
            prospecto["ciudad_id"] = parseInt(e.target.value)
            this.setState({ ciudad_id: parseInt(e.target.value) })
            this.construyeSelectDependienteDeCiudad(datos.urlServicePy+"parametros/api_cat_municipio/0", "selectMunicipio")

        } else if ("selectColonia" == e.target.name) {
            console.log("entro")
            let prospecto = this.state.propspecto
            prospecto["colonia_id"] = parseInt(e.target.value)
            this.setState({ colonia_id: parseInt(e.target.value) })

        } else if ("infonavit" == e.target.name) {
            let prospecto = this.state.propspecto
            prospecto["" + e.target.name + ""] = parseInt(e.target.value)
            if(parseInt(e.target.value) == 0){
                prospecto.monto_credito=0
                //this.state.propspecto.infonavit = ""
            }
            this.setState({ propspecto: prospecto })
        } else if ("fuiste_agente" == e.target.name) {
            let prospecto = this.state.propspecto
            prospecto["" + e.target.name + ""] = parseInt(e.target.value)
            this.setState({ propspecto: prospecto })
        } else if ("grupo_bal" == e.target.name) {
            let prospecto = this.state.propspecto
            prospecto["" + e.target.name + ""] = parseInt(e.target.value)
            this.setState({ propspecto: prospecto })
        } else if ("gobierno" == e.target.name) {
            let prospecto = this.state.propspecto
            prospecto["" + e.target.name + ""] = parseInt(e.target.value)
            this.setState({ propspecto: prospecto })
        } else if ("vivienda_resides" == e.target.name) {
            let prospecto = this.state.propspecto
            if(parseInt(e.target.value) != 4){
                prospecto.vivienda_resides_otro = null
            }
            prospecto["" + e.target.name + ""] = parseInt(e.target.value)
            this.setState({ propspecto: prospecto })
        } else if ("conquien_vives" == e.target.name) {
            let prospecto = this.state.propspecto
            if(parseInt(e.target.value) != 3){
                prospecto.conquien_vives_otro = null
            }
            prospecto["" + e.target.name + ""] = parseInt(e.target.value)
            this.setState({ propspecto: prospecto })
        } else if ("medio_transporte" == e.target.name) {
            let prospecto = this.state.propspecto
            if(parseInt(e.target.value) != 2){
                prospecto.medio_transporte_otro = null
            }
            prospecto["" + e.target.name + ""] = parseInt(e.target.value)
            this.setState({ propspecto: prospecto })
        } else if ("vivienda_resides_otro" == e.target.name) {
            console.log("Entro aquí vivienda_resides_otro");
            if (e.target.value != null) {
                let prospecto = this.state.propspecto
                prospecto.vivienda_resides = 4
                prospecto.vivienda_resides_otro = e.target.value
                this.setState({ propspecto: prospecto })
            }
        } else if ("medio_transporte_otro" == e.target.name) {
            if (e.target.value != null) {
                let prospecto = this.state.propspecto
                prospecto.medio_transporte = 2
                prospecto.medio_transporte_otro = e.target.value
                this.setState({ propspecto: prospecto })
            }

        } else {
            let prospecto = this.state.propspecto
            prospecto["" + e.target.name + ""] = e.target.value
            this.setState({ propspecto: prospecto })
        }
    }

    construyeSelectReferido(url, selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                for (var i = 0; i < filas.length; i++) {

                    let fila = filas[i]
                    if (selector == "selectEDAT") {
                        if (this.state.referido.empleado_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[2].value + ' ' + fila.fila[3].value + ' ' + fila.fila[4].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[2].value + ' ' + fila.fila[3].value + ' ' + fila.fila[4].value}</option>)
                        }
                    }
                    if (selector == "selectFuenteReclutamiento") {
                        if (this.state.referido.fuente_reclutamiento_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }
                    }
                }
                let salida = []
                salida.push(<select disabled onChange={this.onChange} name={selector} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ [selector]: salida })
            });
    }

    UNSAFE_componentWillMount() {
        fetch(datos.urlServicePy+"parametros/api_recluta_prospectos/" + this.props.idProspecto)
            .then(response => response.json())
            .then(json => {
                let fila = json.filas[0].fila
                let columnas = json.columnas
                let columnasSalida = {}
                for (var i = 0; i < fila.length; i++) {
                    console.log(fila[i])
                    columnasSalida["" + columnas[i].key + ""] = fila[i].value
                }
                

                this.setState({ referido: columnasSalida })

                if (columnasSalida.doc_cv != null && columnasSalida.doc_cv.length > 0) {
                    this.setState({ colorSubirArchivoCV: "#78CB5A", colorTextoSubirArchivoCV: "#FFFFFF", colorBotonSubirArchivoCV: "#E5F6E0", nombreArchivoCV: "Reemplazar...", muestraDocCV: true })
                }
                this.construyeSelectReferido(datos.urlServicePy+"parametros/api_cat_empleados/0", "selectEDAT")
                this.construyeSelectReferido(datos.urlServicePy+"parametros/api_cat_fuente_reclutamiento/0", "selectFuenteReclutamiento")
                this.construyeSelect(datos.urlServicePy+"parametros/api_cat_ciudad/0", "selectCiudad")

                fetch(datos.urlServicePy+"recluta/api_recluta_encuentro01/" + this.props.idProspecto)
                    .then(response => response.json())
                    .then(jsonData => {

                        if (jsonData.length > 0) {
                            console.log(jsonData[0], "jsonData")
                            this.setState({ propspecto: jsonData[0] })
                            this.construyeSelectEdicion(datos.urlServicePy+"parametros/api_cat_ciudad/0", "selectCiudad")
                            this.construyeSelectEdicion(datos.urlServicePy+"parametros/api_cat_municipio/0", "selectMunicipio")
                            this.construyeSelectEdicion(datos.urlServicePy+"parametros/api_cat_colonia/"+parseInt(jsonData[0].colonia_id), "selectColonia")
                        }
                    })

                fetch(datos.urlServicePy+"recluta/api_recluta_vcargapsppdf14/" + this.props.idProspecto)
                    .then(response => response.json())
                    .then(jsonRespuestaCarga => {
                        console.log("respuesta de la carga ", jsonRespuestaCarga)
                        if (jsonRespuestaCarga.length > 0) {
                            let interpretacionConcatenada = jsonRespuestaCarga[6] === undefined ? "" : jsonRespuestaCarga[6].respuesta

                            this.setState({
                                muestraInfoPSP: true,
                                rutaPSP: jsonRespuestaCarga[0].ironpsp,
                                fechaRespPSP: jsonRespuestaCarga[0].respuesta,
                                eficaciaVentas: jsonRespuestaCarga[1].porcentaje,
                                eficaciaEmpresarial: jsonRespuestaCarga[2].porcentaje,
                                rendimientoVentas: jsonRespuestaCarga[3].porcentaje,
                                ventaDominante: jsonRespuestaCarga[4].respuesta,
                                interpretacion: jsonRespuestaCarga[5].respuesta + " " + interpretacionConcatenada,
                                muestraDocPSP: true,
                                colorSubirArchivoPSP: "#78CB5A", colorTextoSubirArchivoPSP: "#FFFFFF", colorBotonSubirArchivoPSP: "#78CB5A"
                            })
                        }
                    })
                    fetch(datos.urlServicePy+"parametros/api_recluta_prospectos/" + this.props.idProspecto)
                    .then(response => response.json())
                    .then(recluta_prospecto => {
                        console.log("recluta_propspecto", recluta_prospecto)
                        fetch(datos.urlServicePy+"parametros/api_cat_empleados/" + recluta_prospecto.filas[0].fila[12].value)
                        .then(response => response.json())
                        .then(entrevistador => {
                                console.log("entrevistador ", entrevistador)
                                this.setState({entrevistador:entrevistador.filas[0].fila[2].value + " "+ entrevistador.filas[0].fila[3].value + " " + (entrevistador.filas[0].fila[4].value != null ? entrevistador.filas[0].fila[4].value:'') })
                        })
                    })
            })
    }

    onChangeFilePuesto(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file de puesto", file);
        this.setState({ archivoPuesto: file, colorSubirArchivo: "#78CB5A", colorTextoSubirArchivo: "#FFFFFF", colorBotonSubirArchivo: "#E5F6E0", nombreArchivoPuesto: file.name })
        // this.props.guardaArchivo(file, "archivoPuesto")

    }


    onClickBotonArchivoPuesto() {
        console.log("entrnaod a la imagen")
        this.upload.click()
    }



    onClickBotonArchivoPSP() {
        console.log("entrnaod a la imagen")
        this.upload.click()
    }


    cargaInfoPSP(json) {
        const requestOptions = {
            method: "POST",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };

        fetch(datos.urlServicePy+"recluta/api_recluta_cargapsppdf/0", requestOptions)
        .then(response => response.json())
        .then(data => {
            console.log("la respuesta de la carga", data)
            this.cargaPdf14()
        });
    }

    cargaPdf14(){
        fetch(datos.urlServicePy+"recluta/api_recluta_vcargapsppdf14/" + this.props.idProspecto)
        .then(response => response.json())
        .then(jsonRespuestaCarga => {
            console.log("respuesta de la carga ", jsonRespuestaCarga)
            let interpretacionConcatenada=jsonRespuestaCarga[6] === undefined ?  "":jsonRespuestaCarga[6].respuesta

            this.setState({
                muestraInfoPSP: true,
                fechaRespPSP: jsonRespuestaCarga[0].respuesta,
                eficaciaVentas: jsonRespuestaCarga[1].porcentaje,
                eficaciaEmpresarial: jsonRespuestaCarga[2].porcentaje,
                rendimientoVentas: jsonRespuestaCarga[3].porcentaje,
                ventaDominante:jsonRespuestaCarga[4].respuesta,
                interpretacion:jsonRespuestaCarga[5].respuesta + " "+interpretacionConcatenada,
            })
        })
    }

    onChangeFilePSP(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file de puesto", file);

        //lanzamos el envio del psp 
        let formData = new FormData();
        const config = {
            headers: { 'content-type': 'multipart/form-data' }
        }
        
        if(this.state.muestraDocPSP == true){
            formData.append('ironpsp', file);
            axios.put(datos.urlServicePy+"api/doc_ironpsp/"+this.props.idProspecto+"/", formData, config)
            .then(res => {
                console.log("respuesta  del archivo cargado", res.data);
                this.cargaInfoPSP(res.data)
                let ruta = res.data.ironpsp
                let rutaCorrecta = ruta.replace("8000/media", "8001/documentos")
                rutaCorrecta = rutaCorrecta.replace("http", "https")
                console.log(rutaCorrecta)
                this.setState({ rutaPSP: rutaCorrecta, archivoPSP: file, colorSubirArchivoPSP: "#78CB5A", colorTextoSubirArchivoPSP: "#FFFFFF", colorBotonSubirArchivoPSP: "#78CB5A", nombreArchivoPSP: file.name, muestraDocPSP: true })
            })
        } else {
            formData.append('id', this.props.idProspecto);
            formData.append('ironpsp', file);
            formData.append('prospecto_id', this.props.idProspecto);
            axios.post(datos.urlServicePy+"api/doc_ironpsp/", formData, config)
            .then(res => {
                console.log("respuesta  del archivo cargado", res.data);
                this.cargaInfoPSP(res.data)
                let ruta = res.data.ironpsp
                let rutaCorrecta = ruta.replace("8000/media", "8001/documentos")
                rutaCorrecta = rutaCorrecta.replace("http", "https")
                console.log(rutaCorrecta)
                this.setState({ rutaPSP: rutaCorrecta, archivoPSP: file, colorSubirArchivoPSP: "#78CB5A", colorTextoSubirArchivoPSP: "#FFFFFF", colorBotonSubirArchivoPSP: "#78CB5A", nombreArchivoPSP: file.name, muestraDocPSP: true })
            })
        }
        
    }


    render() {
        return (
            <div>
                {
                    this.state.muestraExplorando10 == true && this.state.muestraExplorando9 == false && this.state.muestraExplorando8 == false && this.state.muestraExplorando7 == false && this.state.muestraExplorando6 == false && this.state.muestraExplorando5 == false && this.state.muestraExplorando4 == false && this.state.muestraExplorando3 == false && this.state.muestraExplorando == false && this.state.muestraEncuentro == false &&
                    <EntrevistaProfundaExplorando10 guardaFormulario10={this.guardaFormulario10}  referido={this.state.referido} regresaForm9explorando={this.regresaForm9explorando} botonCancelar={this.props.botonCancelar} idProspecto={this.props.idProspecto} tipoUsuario = {this.props.tipoUsuario}/>
                }
                {
                    this.state.muestraExplorando9 == true && this.state.muestraExplorando8 == false && this.state.muestraExplorando7 == false && this.state.muestraExplorando6 == false && this.state.muestraExplorando5 == false && this.state.muestraExplorando4 == false && this.state.muestraExplorando3 == false && this.state.muestraExplorando == false && this.state.muestraEncuentro == false &&
                    <EntrevistaProfundaExplorando9 tipoUsuario={this.props.tipoUsuario} regresaForm8explorando={this.regresaForm8explorando} referido={this.state.referido} botonCancelar={this.props.botonCancelar} muestraForm10Explorando={this.muestraForm10Explorando} idProspecto={this.props.idProspecto}/>
                }
                {
                    this.state.muestraExplorando8 == true && this.state.muestraExplorando7 == false && this.state.muestraExplorando6 == false && this.state.muestraExplorando5 == false && this.state.muestraExplorando4 == false && this.state.muestraExplorando3 == false && this.state.muestraExplorando == false && this.state.muestraEncuentro == false &&
                    <EntrevistaProfundaExplorando8 tipoUsuario={this.props.tipoUsuario} regresaForm7explorando={this.regresaForm7explorando} botonCancelar={this.props.botonCancelar} muestraForm9Explorando={this.muestraForm9Explorando} referido={this.state.referido} idProspecto={this.props.idProspecto}/>
                }
                {
                    this.state.muestraExplorando7 == true && this.state.muestraExplorando6 == false && this.state.muestraExplorando5 == false && this.state.muestraExplorando4 == false && this.state.muestraExplorando3 == false && this.state.muestraExplorando == false && this.state.muestraEncuentro == false &&
                    <EntrevistaProfundaExplorando7 regresaForm6explorando={this.regresaForm6explorando} botonCancelar={this.props.botonCancelar} muestraForm8Explorando={this.muestraForm8Explorando} idProspecto={this.props.idProspecto} referido={this.state.referido} tipoUsuario = {this.props.tipoUsuario}/>
                }
                {
                    this.state.muestraExplorando6 == true && this.state.muestraExplorando5 == false && this.state.muestraExplorando4 == false && this.state.muestraExplorando3 == false && this.state.muestraExplorando == false && this.state.muestraEncuentro == false &&
                    <EntrevistaProfundaExplorando6 regresaForm5explorando={this.regresaForm5explorando} botonCancelar={this.props.botonCancelar} muestraForm7Explorando={this.muestraForm7Explorando} idProspecto={this.props.idProspecto} referido={this.state.referido} tipoUsuario = {this.props.tipoUsuario}/>
                }
                {
                    this.state.muestraExplorando5 == true && this.state.muestraExplorando4 == false && this.state.muestraExplorando3 == false && this.state.muestraExplorando == false && this.state.muestraEncuentro == false &&
                    <EntrevistaProfundaExplorando5 regresaForm4explorando={this.regresaForm4explorando} botonCancelar={this.props.botonCancelar} muestraForm6Explorando={this.muestraForm6Explorando} idProspecto={this.props.idProspecto} referido={this.state.referido} tipoUsuario = {this.props.tipoUsuario}/>
                }

                {
                    this.state.muestraExplorando4 == true && this.state.muestraExplorando3 == false && this.state.muestraExplorando == false && this.state.muestraEncuentro == false &&
                    <EntrevistaProfundaExplorando4 enviarReserva={this.props.enviarReserva} referido={this.state.referido}
                        enviarBaja={this.props.enviarBaja} regresaForm3explorando={this.regresaForm3explorando} botonCancelar={this.props.botonCancelar} muestraForm5Explorando={this.muestraForm5Explorando} idProspecto={this.props.idProspecto} tipoUsuario = {this.props.tipoUsuario}/>
                }

                {
                    this.state.muestraExplorando3 == true && this.state.muestraExplorando == false && this.state.muestraEncuentro == false &&
                    <EntrevistaProfundaExplorando3 regresaForm2explorando={this.regresaForm2explorando} muestraForm4Explorando={this.muestraForm4Explorando} botonCancelar={this.props.botonCancelar} idProspecto={this.props.idProspecto} referido={this.state.referido} tipoUsuario = {this.props.tipoUsuario}/>
                }


                {
                    this.state.muestraExplorando == true && this.state.muestraEncuentro == false &&
                    <EntrevistaProfundaExplorando enviarReserva={this.props.enviarReserva} referido={this.state.referido} tipoUsuario = {this.props.tipoUsuario}
                        enviarBaja={this.props.enviarBaja} regresaForm1explorando={this.regresaForm1explorando} muestraForm3Explorando={this.muestraForm3Explorando} muestraForm2Explorando={this.muestraForm2Explorando} botonCancelar={this.props.botonCancelar} idProspecto={this.props.idProspecto} />
                }

                {
                    this.state.muestraEncuentro == true &&
                    <div>

                        <div id="modal-psp" className="modal fade" tabindex="-1">
                            <div className="modal-dialog modal-full">
                                <div className="modal-content text-white" style={{ backgroundColor: "#313A46" }}>
                                    <div className="modal-header text-white" style={{ backgroundColor: "#8189D4" }}>
                                        <h6 className="modal-title">Documento PSP </h6>
                                        <button type="button" className="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div className="modal-body">
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-xs-12 col-lg-4"><>&nbsp;&nbsp;</></div>
                                                <div className="col-xs-12 col-lg-4">
                                                    <div className="input-group">
                                                        <form encType="multipart/form" style={{ display: 'none' }} >
                                                            <input type="file" style={{ display: 'none' }} ref={(ref) => this.upload = ref} onChange={this.onChangeFilePSP.bind(this)}></input>
                                                        </form>
                                                        <input type="text" style={{ borderColor: 'black', backgroundColor: "#313A46", color: 'white' }} className="form-control " placeholder="Archivo PSP" value={this.state.nombreArchivoPSP} />
                                                        <span className="input-group-prepend" style={{ backgroundColor: this.state.colorBotonSubirArchivoPSP }}>
                                                            {/*<button type="button" class="btn text-white" onClick={this.onClickBotonArchivoPSP} style={{ backgroundColor: this.state.colorBotonSubirArchivoPSP }}>
                                                        <h10 style={{ color: "white" }}>+</h10>
                                                         </button>*/}

                                                            {
                                                                this.state.muestraDocPSP == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoPSP }}>
                                                                    <a href={this.state.rutaPSP} target="_blank" rel="noopener noreferrer" >
                                                                        <i className="fas fa-eye"
                                                                            style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                    </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h10 onClick={this.onClickBotonArchivoPSP} style={{ color: "white" }}>+</h10>
                                                                </button> : <button type="button" class="btn text-white" onClick={this.onClickBotonArchivoPSP} style={{ backgroundColor: this.state.colorBotonSubirArchivoPSP }}>
                                                                    <h10 style={{ color: "white" }}>+</h10>
                                                                </button>

                                                            }



                                                            <span className="input-group-text" style={{ background: this.state.colorBotonSubirArchivoPSP, borderColor: this.state.colorBotonSubirArchivoPSP, color: "white" }} >{this.state.textoPuesto}</span>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div className="col-xs-12 col-lg-4"><>&nbsp;&nbsp;</></div>
                                            </div>

                                            <br></br>
                                            <br></br>
                                            {this.state.muestraInfoPSP == true &&
                                                <div>
                                                    <div className="row">
                                                        <div className="col-xs-6 col-lg-6">
                                                            <h8 class="mb-0 font-weight-semibold text-white"> Resultados PSP</h8>
                                                        </div>
                                                        <div className="col-xs-4 col-lg-4">
                                                            <table className="table datatable-sorting  table-striped table-hover" style={{ backgroundColor: "#313A46", borderColor: '#313A46' }}>
                                                                <thead style={{ backgroundColor: "#313A46" }}>
                                                                    <tr>
                                                                        <th className="text-left font-weight-bold text-white" style={{ backgroundColor: '#313A46' }} >
                                                                            <a>ALTO</a>
                                                                        </th>
                                                                        <th className="text-center font-weight-bold text-white" style={{ backgroundColor: '#313A46' }} >
                                                                            <a>MODERADO</a>
                                                                        </th>
                                                                        <th className="text-right font-weight-bold text-white" style={{ backgroundColor: '#313A46' }} >
                                                                            <a>BAJO</a>
                                                                        </th>
                                                                    </tr>
                                                                </thead>

                                                            </table>
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-xs-4 col-lg-4">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                        className="input-group-text text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }}>Fecha de respuesta del PSP</span>
                                                                    </span> <input type="text" placeholder="" value={this.state.fechaRespPSP} className="form-control text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }} />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="col-xs-2 col-lg-2">
                                                            <h8 class=" text-white"> Eficacia en ventas</h8>
                                                        </div>

                                                        <div className="col-xs-4 col-lg-4">

                                                            <input type="range" class="custom-range" min="0" name="salud" max="100" step={50} value={this.state.eficaciaVentas} id="salud" />
                                                        </div>


                                                        <div className="col-xs-2 col-lg-2">




                                                            {
                                                                this.state.eficaciaVentas == 0 &&
                                                                <div class="mr-3 ">
                                                                    <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                        class="btn rounded-pill btn-icon btn-sm "><span
                                                                            class="letter-icon text-white"></span>
                                                                    </a>
                                                                </div>

                                                            }

                                                            {
                                                                this.state.eficaciaVentas == 50 &&
                                                                <div className="row">
                                                                    
                                                                    {/*<div class="mr-3 ">
                                                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                                                class="letter-icon text-white"></span>
                                                                        </a>
                                                            </div>*/}

                                                                    <div class="mr-3 ">
                                                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                                                class="letter-icon text-white"></span>
                                                                        </a>
                                                                    </div>

                                                                </div>
                                                            }


                                                            {
                                                                this.state.eficaciaVentas == 100 &&
                                                                <div className="row">

                                                                    {/*<div class="mr-3 ">
                                                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                                                class="letter-icon text-white"></span>
                                                                        </a>
                                                                    </div>

                                                                    <div class="mr-3 ">
                                                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                                                class="letter-icon text-white"></span>
                                                                        </a>
                                                            </div>*/}

                                                                    <div class="mr-3 ">
                                                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FD0303', borderColor: '#FD0303' }}
                                                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                                                class="letter-icon text-white"></span>
                                                                        </a>
                                                                    </div>

                                                                </div>
                                                            }





                                                        </div>


                                                    </div>

                                                    <div className="row">

                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                        className="input-group-text text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }}>Estilo de venta dominante&ensp;</span>
                                                                    </span> <input type="text" placeholder="" value={this.state.ventaDominante} className="form-control text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }} />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="col-xs-12 col-lg-2">
                                                            <h8 class=" text-white"> Eficacia empresarial</h8>
                                                        </div>

                                                        <div className="col-xs-12 col-lg-4">
                                                            <input type="range" class="custom-range" min="0" name="salud" max="100" step={50} value={this.state.eficaciaEmpresarial} id="salud" />
                                                        </div>


                                                        <div className="col-xs-2 col-lg-2">

                                                            {
                                                                this.state.eficaciaEmpresarial == 0 &&
                                                                <div class="mr-3 ">
                                                                    <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                        class="btn rounded-pill btn-icon btn-sm "><span
                                                                            class="letter-icon text-white"></span>
                                                                    </a>
                                                                </div>

                                                            }

                                                            {
                                                                this.state.eficaciaEmpresarial == 50 &&
                                                                <div className="row">
                                                                  
                                                                  {/*  <div class="mr-3 ">
                                                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                                                class="letter-icon text-white"></span>
                                                                        </a>
                                                            </div>*/}

                                                                    <div class="mr-3 ">
                                                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                                                class="letter-icon text-white"></span>
                                                                        </a>
                                                                    </div>

                                                                </div>
                                                            }


                                                            {
                                                                this.state.eficaciaEmpresarial == 100 &&
                                                                <div className="row">

                                                                  {/*  <div class="mr-3 ">
                                                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                                                class="letter-icon text-white"></span>
                                                                        </a>
                                                                    </div>

                                                                    <div class="mr-3 ">
                                                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                                                class="letter-icon text-white"></span>
                                                                        </a>
                                                            </div>*/}

                                                                    <div class="mr-3 ">
                                                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FD0303', borderColor: '#FD0303' }}
                                                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                                                class="letter-icon text-white"></span>
                                                                        </a>
                                                                    </div>

                                                                </div>
                                                            }



                                                        </div>



                                                    </div>

                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                        className="input-group-text text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }}>Interpretación de precisión</span>
                                                                    </span> <textarea disabled={true} type="text" placeholder="" value={this.state.interpretacion} className="form-control text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46'  }} />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="col-xs-12 col-lg-2">
                                                            <h8 class=" text-white">Rendimiento de ventas general esperado</h8>
                                                        </div>

                                                        <div className="col-xs-12 col-lg-4">
                                                            <input type="range" class="custom-range" min="0" name="salud" max="100" step={50} value={this.state.rendimientoVentas} id="salud" />
                                                        </div>

                                                        <div className="col-xs-2 col-lg-2">

                                                            {
                                                                this.state.rendimientoVentas == 0 &&
                                                                <div class="mr-3 ">
                                                                    <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                        class="btn rounded-pill btn-icon btn-sm "><span
                                                                            class="letter-icon text-white"></span>
                                                                    </a>
                                                                </div>

                                                            }

                                                            {
                                                                this.state.rendimientoVentas == 50 &&
                                                                <div className="row">
                                                                   {/* <div class="mr-3 ">
                                                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                                                class="letter-icon text-white"></span>
                                                                        </a>
                                                            </div>*/}

                                                                    <div class="mr-3 ">
                                                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                                                class="letter-icon text-white"></span>
                                                                        </a>
                                                                    </div>

                                                                </div>
                                                            }


                                                            {
                                                                this.state.rendimientoVentas == 100 &&
                                                                <div className="row">

                                                                  {/*  <div class="mr-3 ">
                                                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                                                class="letter-icon text-white"></span>
                                                                        </a>
                                                                    </div>

                                                                    <div class="mr-3 ">
                                                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                                                class="letter-icon text-white"></span>
                                                                        </a>
                                                            </div>*/}

                                                                    <div class="mr-3 ">
                                                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FD0303', borderColor: '#FD0303' }}
                                                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                                                class="letter-icon text-white"></span>
                                                                        </a>
                                                                    </div>

                                                                </div>
                                                            }



                                                        </div>


                                                    </div>
                                                </div>

                                            }







                                        </div>
                                    </div>
                                    <div class="modal-footer d-flex justify-content-center">
                                        <button type="button" class="btn text-white" data-dismiss="modal" style={{ backgroundColor: '#617187', borderColor: '#617187' }}>Cancelar</button>

                                        {
                                            this.state.muestraDocPSP == true ? <button type="button" class="btn text-white " disabled={false} style={{ backgroundColor: '#617187', borderColor: '#617187' }}>
                                                <a href={this.state.rutaPSP} style={{ color: 'white' }} target="_blank" rel="noopener noreferrer" >
                                                    Ver PSP
                                                </a>
                                            </button> : <button type="button" class="btn text-white " disabled={true} style={{ backgroundColor: '#617187', borderColor: '#617187' }}>
                                                <a href="#" style={{ color: 'white' }} target="_blank" rel="noopener noreferrer" >
                                                    Ver PSP
                                                </a>

                                            </button>
                                        }

                                        <button type="button" class="btn text-white" data-dismiss="modal" style={{ backgroundColor: '#617187', borderColor: '#617187' }}>Aceptar</button>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="card">
                            <div class="row mb-2 mt-2 ml-1">
                                <div class="col-12 col-lg-9">
                                    <div class="row">

                                        <div class="col-6 col-lg-1 d-flex align-items-center pb-sm-1">
                                            <div class="mr-2">
                                                <a href="#"
                                                    class="btn rounded-pill btn-icon btn-sm" style={{ backgroundColor: '#78CB5A' }}> <span
                                                        class="letter-icon text-white"><i className="icon-checkmark2"></i></span>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-6 col-lg-1 d-flex align-items-center pb-sm-1">
                                            <div class="mr-2">
                                                <a href="#"
                                                    class="btn  rounded-pill btn-icon btn-sm " style={{ backgroundColor: '#8189D4', borderColor: '#8189D4' }}> <span style={{ color: 'white' }}
                                                        class="letter-icon">EP</span>
                                                </a>
                                            </div>

                                        </div>



                                        <div class="col-6 col-lg-7 d-flex align-items-center pb-sm-1">

                                            <div class="mr-5 ">
                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8189D4', borderColor: '#8189D4' }}
                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                        class="letter-icon text-white">1</span>
                                                </a>
                                            </div>

                                            <div class="mr-5">
                                                <a href="#" style={{ height: '12px', width: '12px', borderColor: '#C4D3E8' }}
                                                    class="btn rounded-pill btn-icon btn-sm "> <span
                                                        class="letter-icon"></span>
                                                </a>
                                            </div>
                                            <div class="mr-5">
                                                <a href="#" style={{ height: '12px', width: '12px', borderColor: '#C4D3E8' }}
                                                    class="btn rounded-pill btn-icon btn-sm "> <span
                                                        class="letter-icon"></span>
                                                </a>
                                            </div>

                                            <div class="mr-5">
                                                <a href="#" style={{ height: '12px', width: '12px', borderColor: '#C4D3E8' }}
                                                    class="btn rounded-pill btn-icon btn-sm "> <span
                                                        class="letter-icon"></span>
                                                </a>
                                            </div>

                                            <div class="mr-5">
                                                <a href="#" style={{ height: '12px', width: '12px', borderColor: '#C4D3E8' }}
                                                    class="btn rounded-pill btn-icon btn-sm "> <span
                                                        class="letter-icon"></span>
                                                </a>
                                            </div>
                                            <div class="mr-5">
                                                <a href="#" style={{ height: '12px', width: '12px', borderColor: '#C4D3E8' }}
                                                    class="btn rounded-pill btn-icon btn-sm "> <span
                                                        class="letter-icon"></span>
                                                </a>
                                            </div>

                                            <div class="mr-5">
                                                <a href="#" style={{ height: '12px', width: '12px', borderColor: '#C4D3E8' }}
                                                    class="btn rounded-pill btn-icon btn-sm "> <span
                                                        class="letter-icon"></span>
                                                </a>
                                            </div>

                                            <div class="mr-5">
                                                <a href="#" style={{ height: '12px', width: '12px', borderColor: '#C4D3E8' }}
                                                    class="btn rounded-pill btn-icon btn-sm "> <span
                                                        class="letter-icon"></span>
                                                </a>
                                            </div>
                                            <div class="mr-5">
                                                <a href="#" style={{ height: '12px', width: '12px', borderColor: '#C4D3E8' }}
                                                    class="btn rounded-pill btn-icon btn-sm "> <span
                                                        class="letter-icon"></span>
                                                </a>
                                            </div>

                                            <div class="mr-5">
                                                <a href="#" style={{ height: '12px', width: '12px', borderColor: '#C4D3E8' }}
                                                    class="btn rounded-pill btn-icon btn-sm "> <span
                                                        class="letter-icon"></span>
                                                </a>
                                            </div>
                                        </div>

                                        &ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;    
                                        <div class="d-flex justify-content-between">
                                            <div class="mr-2">
                                                <a href="#"
                                                    class="btn  rounded-pill btn-icon btn-sm" style={{ borderColor: '#5B96E2' }}> <span style={{ color: '#5B96E2' }}
                                                        class="letter-icon">VC</span>
                                                </a>
                                            </div>

                                        </div>
                                        <div class="d-flex justify-content-between">
                                            <div class="mr-2">
                                                <a href="#"
                                                    class="btn  rounded-pill btn-icon btn-sm" style={{ borderColor: '#41B3D1' }}> <span style={{ color: '#41B3D1' }}
                                                        class="letter-icon">CC</span>
                                                </a>
                                            </div>

                                        </div>
                                        <div class="d-flex justify-content-between">
                                            <div class="mr-2">
                                                <a href="#"
                                                    class="btn  rounded-pill btn-icon btn-sm" style={{ borderColor: '#78CB5A' }}> <span style={{ color: '#78CB5A' }}
                                                        class="letter-icon">C</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-12 col-lg-3">
                                    <div class="float-right mr-3">
                                        <button type="button" title="Guardar y Regresar" onClick={(e) => this.props.botonCancelar()} class="btn rounded-pill mr-1" style={{ backgroundColor: '#8F9EB3', borderColor: '#8F9EB3' }}><span><i style={{ color: 'white' }} className="icon-backward2"></i></span></button>
                                        <button type="button" title="Registrar entrevista" onClick={this.muestraForm2Explorando} disabled={
                                            this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                            && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER" ||
                                            (this.state.propspecto.carrera == null || this.state.propspecto.carrera == '' ||
                                            this.state.propspecto.cp == 0 || this.state.propspecto.cp == null ||
                                            //  this.state.ciudad_id == null || this.state.ciudad_id == 0 ||
                                            // this.state.municipio_id == null || this.state.municipio_id == 0 ||
                                            // this.state.colonia_id == null || this.state.colonia_id == 0 ||
                                            this.state.propspecto.calle == null || this.state.propspecto.calle == '' ||
                                            this.state.propspecto.num_ext == null || this.state.propspecto.num_ext == '' ||
                                            this.state.propspecto.universidad == null || this.state.propspecto.universidad == '' ||
                                            this.state.propspecto.infonavit == null ||
                                            this.state.propspecto.fuiste_agente == null ||
                                            this.state.propspecto.grupo_bal == null ||
                                            this.state.propspecto.gobierno == null ||
                                            this.state.propspecto.vivienda_resides == null ||
                                            this.state.propspecto.vivienda_resides == 4 && (this.state.propspecto.vivienda_resides_otro == null || this.state.propspecto.vivienda_resides_otro == "") ||
                                            this.state.propspecto.conquien_vives == null ||
                                            this.state.propspecto.conquien_vives == 3 && (this.state.propspecto.conquien_vives_otro == null || this.state.propspecto.conquien_vives_otro == "") ||
                                            this.state.propspecto.medio_transporte == null ||
                                            this.state.propspecto.medio_transporte == 2 && (this.state.propspecto.medio_transporte_otro == null || this.state.propspecto.medio_transporte_otro == "") ||
                                            this.state.propspecto.ciudad_id == 0 || this.state.propspecto.ciudad_id== null ||
                                            this.state.propspecto.municipio_id == 0 || this.state.propspecto.municipio_id == null ||
                                            this.state.propspecto.colonia_id == 0 || this.state.propspecto.colonia_id == null)
                                        } class="btn rounded-pill mr-1"
                                            style={{ backgroundColor: '#8F9EB3', borderColor: '#8F9EB3' }}><span><i style={{ color: 'white' }} className="icon-forward3"></i></span></button>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="card-header bg-transparent header-elements-sm-inline">
                            <h3 class="mb-0 font-weight-semibold" style={{ color: '#617187' }}>Entrevista Profunda - PSP & IRON TALENT </h3>
                            <div class="header-elements">
                                <ul class="list-inline mb-0">
                                    <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Prospecto: <strong >{this.state.referido.nombre+' '+this.state.referido.ape_paterno + ' ' + (this.state.referido.ape_materno != null ? this.state.referido.ape_materno:'')}</strong></li>
                                    <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Fecha de registro de PSP Iron Talent: <strong >{this.state.referido.ts_alta_audit}</strong></li>
                                    <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Usuario creador: <strong >{this.state.entrevistador}</strong></li>
                                    <li class="list-inline-item">
                                        <h6 style={{ backgroundColor: "#8189D4", color: '#FFFF', borderRadius: '8px' }}> <>&nbsp;&nbsp;</> En registro  <>&nbsp;&nbsp;</> </h6>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <br></br>



                        <div class="card-header bg-transparent header-elements-sm-inline" style={{ border: 'none' }}>
                            <h4 class="mb-0 font-weight-semibold" >IronTalent 2022</h4>
                            
                            <div class="header-elements">
                                <ul class="list-inline mb-0">
                                    <li class="list-inline-item">
                                        {this.state.muestraInfoPSP == false &&
                                            <button data-toggle="modal" data-target="#modal-psp" class="btn  mr-1" 
                                            disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} style={{ backgroundColor: "#8189D4", color: '#FFFF' }}> <>&nbsp;&nbsp;</> Subir archivo PSP<>&nbsp;&nbsp;</> </button>
                                        }
                                        {this.state.muestraInfoPSP == true &&
                                            <button data-toggle="modal" data-target="#modal-psp" class="btn  mr-1" 
                                            disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} style={{ backgroundColor: "#78CB5A", color: '#FFFF' }}> <>&nbsp;&nbsp;</> Ver archivo PSP<>&nbsp;&nbsp;</> </button>
                                        }                                    </li>
                                </ul>
                            </div>
                        </div>

                        <br></br>

                        <div class="card">
                            <div class="card-header bg-transparent header-elements-sm-inline">
                                <h6 class="mb-0 font-weight-semibold">01. Encuentro</h6>
                                <div class="header-elements">
                                    <ul class="list-inline mb-0">
                                        <li class="list-inline-item">
                                            <h6 class="mb-0 font-weight-semibold"> <>&nbsp;&nbsp;</> 01/10  <>&nbsp;&nbsp;</> </h6>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div className="card-body">

                                <div className="row">
                                    <div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fuente de reclutamiento</span>
                                                </span> {this.state.selectFuenteReclutamiento}
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Teléfono móvil</span>
                                                </span> <input type="number" placeholder="" disabled name="tel_movil" value={this.state.referido.tel_movil} className="form-control " />
                                            </div>
                                        </div>
                                    </div>


                                    <div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Edad del prospecto</span>
                                                </span> <input type="text" disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} placeholder="Escribir" className="form-control " name="ape_materno" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fecha de registro del Iron talent </span>
                                                </span> <input type="date" placeholder="" disabled name="" className="form-control " value={"" + this.state.referido.fec_entrevista_profunda + ""} />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Entrevistador</span>
                                                </span>{this.state.selectEDAT}
                                            </div>
                                        </div>
                                    </div>


                                    <div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Carrera</span>
                                                </span> <input type="text" placeholder="Escribir" className="form-control " name="carrera" 
                                                disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.propspecto.carrera} />
                                            </div>
                                                {
                                                    this.state.propspecto.carrera == null || this.state.propspecto.carrera == "" ?
                                                        <span className="" style={{ color: "red" }}>Carrera es un campo requerido</span> : ''
                                                }
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Código Postal</span>
                                                </span> <input type="text" placeholder="Escribir" name="cp"  
                                                disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange}  value={this.state.propspecto.cp} className="form-control " />
                                            </div>
                                                {
                                                    this.state.propspecto.cp == 0 || this.state.propspecto.cp == null ?
                                                        <span className="" style={{ color: "red" }}>El Código Postal es un campo requerido</span> : ''
                                                }
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Ciudad</span>
                                                </span> {this.state.selectCiudad}
                                            </div>
                                            {
                                                    this.state.propspecto.ciudad_id == 0 || this.state.propspecto.ciudad_id == null ?
                                                        <span className="" style={{ color: "red" }}>La Ciudad es un campo requerido</span> : ''
                                            }
                                        </div>
                                    </div>


                                    <div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Alcaldía / Municipio</span>
                                                </span> {this.state.selectMunicipio}
                                            </div>
                                            {
                                                    this.state.propspecto.municipio_id == 0 || this.state.propspecto.municipio_id == null?
                                                        <span className="" style={{ color: "red" }}>Alcaldía / Municipio es un campo requerido</span> : ''
                                            }
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Colonia</span>
                                                </span>  {this.state.selectColonia}
                                            </div>
                                            {
                                                    this.state.propspecto.colonia_id == 0 || this.state.propspecto.colonia_id == null ?
                                                        <span className="" style={{ color: "red" }}>Colonia es un campo requerido</span> : ''
                                            }
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Calle</span>
                                                </span> <input type="text" placeholder="Escribir" name="calle" 
                                                disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.propspecto.calle} className="form-control " />
                                            </div>
                                            {
                                                    this.state.propspecto.calle == null || this.state.propspecto.calle == "" ?
                                                        <span className="" style={{ color: "red" }}>Calle es un campo requerido</span> : ''
                                            }
                                        </div>
                                    </div>


                                    <div className="col-xs-12 col-lg-4">
                                        <div className="form-group">

                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }}>No. ext.</span>
                                                </span>
                                                <input type="text" maxlength="10" placeholder="00" name="num_ext" onChange={this.onChange} 
                                                disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} value={this.state.propspecto.num_ext} style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " />
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }}>No. int.</span>
                                                </span> <input type="text" maxlength="10" placeholder="00" name="num_int" 
                                                disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.propspecto.num_int} style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " />
                                            </div>
                                            {
                                                    this.state.propspecto.num_ext == null || this.state.propspecto.num_ext == "" ?
                                                        <span className="" style={{ color: "red" }}>No. ext. es un campo requerido</span> : ''
                                            }

                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Universidad</span>
                                                </span> <input type="text" placeholder="Escribir" name="universidad" 
                                                disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.propspecto.universidad} className="form-control " />
                                            </div>
                                            {
                                                    this.state.propspecto.universidad == null || this.state.propspecto.universidad == "" ?
                                                        <span className="" style={{ color: "red" }}>Universidad es un campo requerido</span> : ''
                                            }
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-lg-4">


                                        <div class="form-check form-check-inline">

                                            <label class="form-check-label" for="inlineRadio1">¿Cuentas con crédito Infonavit?</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="infonavit" id="infonavitSi" value={1} 
                                            disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.propspecto.infonavit == 1} />
                                            <label class="form-check-label" for="infonavitSi">Sí</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="radio" name="infonavit" id="infonavitNo" value={0} 
                                            disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.propspecto.infonavit == 0} />
                                            <label class="form-check-label" for="infonavitNo">No</label>
                                        </div>
                                        {
                                            this.state.propspecto.infonavit  == null ?
                                                <span style={{ color: "red" }}>La pregunta ¿Cuentas con crédito Infonavit? , debe ser seleccionada</span> : ''
                                        }
                                    </div>
                                    <div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Credito Monto Infonavit $</span>
                                                </span> <input type="number" placeholder="00" className="form-control " name="monto_credito" disabled = { 
                                                    this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                    && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER" || (this.state.propspecto.infonavit == 1 ? false : true)} onChange={this.onChange} value={this.state.propspecto.monto_credito} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="card">
                            <div class="card-body">
                                <div className="row">
                                    <div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                >¿Alguna vez fuiste Agente?</span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="fuiste_agente" id="agenteSiEmpleado" value={1} 
                                            disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.propspecto.fuiste_agente == 1} />
                                            <label class="form-check-label" for="agenteSiEmpleado">
                                                Sí, empleado
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="fuiste_agente" id="agenteSiDefinitivo" value={2} 
                                            disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.propspecto.fuiste_agente == 2} />
                                            <label class="form-check-label" for="agenteSiDefinitivo">
                                                Sí,definitivo
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="fuiste_agente" id="agenteNo" value={0} 
                                            disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.propspecto.fuiste_agente == 0} />
                                            <label class="form-check-label" for="agenteNo">
                                                No
                                            </label>
                                        </div>
                                        {
                                                this.state.propspecto.fuiste_agente  == null ?
                                                    <span style={{ color: "red" }}>La pregunta ¿Alguna vez fuiste Agente?, debe ser seleccionada</span> : ''
                                        }
                                    </div>

                                    <div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                >¿Tú o algún familiar ha trabajado en grupo BAL?</span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="grupo_bal" id="familiarEnBallSi" value={1} 
                                            disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.propspecto.grupo_bal == 1} />
                                            <label class="form-check-label" for="familiarEnBallSi">
                                                Sí
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="grupo_bal" id="familiarEnBallNo" value={0} 
                                            disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.propspecto.grupo_bal == 0} />
                                            <label class="form-check-label" for="familiarEnBallNo">
                                                No
                                            </label>
                                        </div>
                                        {
                                                this.state.propspecto.grupo_bal  == null ?
                                                    <span style={{ color: "red" }}>La pregunta ¿Tú o algún familiar ha trabajado en grupo BAL?, debe ser seleccionada</span> : ''
                                        }
                                    </div>

                                    <div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                >¿Actualmente trabajas en el Gobierno?</span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="gobierno" id="gobierno2" value={1} 
                                            disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.propspecto.gobierno == 1} />
                                            <label class="form-check-label" for="gobierno2">
                                                Sí
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="gobierno" id="gobierno1" value={0} 
                                            disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.propspecto.gobierno == 0} />
                                            <label class="form-check-label" for="gobierno1">
                                                No
                                            </label>
                                        </div>
                                        {
                                                this.state.propspecto.gobierno  == null ?
                                                    <span style={{ color: "red" }}>La pregunta ¿Actualmente trabajas en el Gobierno, debe ser seleccionada</span> : ''
                                        }
                                    </div>

                                </div>
                                <hr></hr>
                                <br></br>


                                <div className="row">
                                    <div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                >¿En qué tipo de vivienda resides?</span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="vivienda_resides" checked={this.state.propspecto.vivienda_resides == 0} 
                                            disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} id="vivienda_resides0" value={0} onChange={this.onChange} />
                                            <label class="form-check-label" for="vivienda_resides0">
                                                Casa propia
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="vivienda_resides" checked={this.state.propspecto.vivienda_resides == 1} 
                                            disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} id="vivienda_resides1" value={1} onChange={this.onChange} />
                                            <label class="form-check-label" for="vivienda_resides1">
                                                Rento casa
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="vivienda_resides" checked={this.state.propspecto.vivienda_resides == 2} 
                                            disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} id="vivienda_resides2" value={2} onChange={this.onChange} />
                                            <label class="form-check-label" for="vivienda_resides2">
                                                Departamento propio
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="vivienda_resides" checked={this.state.propspecto.vivienda_resides == 3} 
                                            disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} id="vivienda_resides3" value={3} onChange={this.onChange} />
                                            <label class="form-check-label" for="vivienda_resides3">
                                                Departamento renta
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="vivienda_resides" checked={this.state.propspecto.vivienda_resides == 4} 
                                            disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} id="vivienda_resides4" value={4} onChange={this.onChange} />
                                            <label class="form-check-label" for="vivienda_resides4">
                                                Otro
                                            </label>
                                        </div>

                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend">
                                                </span> <input type="text" placeholder="Otro..." name="vivienda_resides_otro" disabled = {
                                                    this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                    && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER" || (this.state.propspecto.vivienda_resides != 4 ? true : false)} onChange={this.onChange} value={this.state.propspecto.vivienda_resides_otro} className="form-control " />
                                            </div>
                                        </div>
                                        {
                                                this.state.propspecto.vivienda_resides == null ?
                                                    <span style={{ color: "red" }}>La pregunta ¿En qué tipo de vivienda resides?, debe ser seleccionada</span> : ''
                                        }
                                        {
                                                this.state.propspecto.vivienda_resides == 4 && (this.state.propspecto.vivienda_resides_otro == null || this.state.propspecto.vivienda_resides_otro == "") ?
                                                    <span style={{ color: "red" }}>El campo de la pregunta ¿En qué tipo de vivienda resides?, debe ser ingresado</span> : ''
                                        }
                                    </div>

                                    <div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                >Actualmente, ¿Con quién vives?</span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="conquien_vives" checked={this.state.propspecto.conquien_vives == 4} 
                                            disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} id="conquien_vives0" value={4} onChange={this.onChange} />
                                            <label class="form-check-label" for="conquien_vives0">
                                                Esposo (a) e hijos (as)
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="conquien_vives" checked={this.state.propspecto.conquien_vives == 0} 
                                            disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} id="conquien_vives0" value={0} onChange={this.onChange} />
                                            <label class="form-check-label" for="conquien_vives0">
                                                Esposo (a)
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="conquien_vives" id="conquien_vives1" checked={this.state.propspecto.conquien_vives == 1} 
                                            disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} value={1} onChange={this.onChange} />
                                            <label class="form-check-label" for="conquien_vives1">
                                                Hijos
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="conquien_vives" id="conquien_vives2" checked={this.state.propspecto.conquien_vives == 2} 
                                            disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} value={2} onChange={this.onChange} />
                                            <label class="form-check-label" for="conquien_vives2">
                                                Papás
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="conquien_vives" id="conquien_vives3" checked={this.state.propspecto.conquien_vives == 3} 
                                            disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} value={3} onChange={this.onChange} />
                                            <label class="form-check-label" for="conquien_vives3">
                                                Otros
                                            </label>
                                        </div>
                                        <br />
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend">
                                                </span> <input type="text" placeholder="Otro..." className="form-control" name="conquien_vives_otro" disabled = {
                                                    this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" || (this.state.propspecto.conquien_vives != 3 ? true : false)} onChange={this.onChange} value={this.state.propspecto.conquien_vives_otro} />
                                            </div>
                                        </div>
                                        {
                                                this.state.propspecto.conquien_vives == null ?
                                                    <span style={{ color: "red" }}>La pregunta Actualmente, ¿Con quién vives?, debe ser seleccionada</span> : ''
                                        }
                                        {
                                                this.state.propspecto.conquien_vives == 3 && (this.state.propspecto.conquien_vives_otro == null || this.state.propspecto.conquien_vives_otro == "") ?
                                                    <span style={{ color: "red" }}>El campo de la pregunta Actualmente, ¿Con quién vives?, debe ser ingresado</span> : ''
                                        }
                                    </div>

                                    <div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                >Generalmente, ¿En qué medio de transporte te trasladas?</span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="medio_transporte" checked={this.state.propspecto.medio_transporte == 0} 
                                            disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} id="medio_transporte0" value={0} onChange={this.onChange} />
                                            <label class="form-check-label" for="medio_transporte0">
                                                Auto propio
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="medio_transporte" checked={this.state.propspecto.medio_transporte == 1} 
                                            disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} id="medio_transporte1" value={1} onChange={this.onChange} />
                                            <label class="form-check-label" for="medio_transporte1">
                                                Transporte público
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="medio_transporte" checked={this.state.propspecto.medio_transporte == 2} 
                                            disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} id="medio_transporte2" value={2} onChange={this.onChange} />
                                            <label class="form-check-label" for="medio_transporte2">
                                                Otro
                                            </label>
                                        </div>
                                        <br /><br />
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend">
                                                </span> <input type="text" placeholder="Otro..." className="form-control " name="medio_transporte_otro" disabled = {
                                                    this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" || (this.state.propspecto.medio_transporte != 2 ? true : false)} onChange={this.onChange} value={this.state.propspecto.medio_transporte_otro} />
                                            </div>
                                        </div>
                                        {
                                                this.state.propspecto.medio_transporte == null ?
                                                    <span style={{ color: "red" }}>La pregunta Generalmente, ¿En qué medio de transporte te trasladas?, debe ser seleccionada</span> : ''
                                        }
                                        {
                                                this.state.propspecto.medio_transporte == 2 && (this.state.propspecto.medio_transporte_otro == null || this.state.propspecto.medio_transporte_otro == "") ?
                                                    <span style={{ color: "red" }}>El campo de la pregunta Generalmente, ¿En qué medio de transporte te trasladas?, debe ser ingresado</span> : ''
                                        }
                                    </div>

                                </div>

                                <hr></hr>
                                <br></br>

                                <div className="row">
                                    <div className="col-xs-12 col-lg-4">

                                        <div className="form-group">
                                            <span>
                                                <span >Si hoy te ganaras 1 millón de pesos, ¿Qué harías con ellos?</span>
                                            </span>
                                            <div className="input-group">
                                                <textarea style={{ height: '150px' }} type="text" placeholder="Escribir" name="ganar_millon" 
                                                disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.propspecto.ganar_millon} className="form-control " />
                                            </div>
                                        </div>

                                    </div>

                                    <div className="col-xs-12 col-lg-4">

                                        <div className="form-group">
                                            <span>
                                                <span >¿Cómo te podemos encontrar en tus redes sociales?</span>
                                            </span>
                                            <div className="form-group">
                                                <div className="input-group">
                                                    <span className="input-group-prepend"> <span
                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}><i className="icon-facebook"></i></span>
                                                    </span> <input type="text" placeholder="Escribir" className="form-control" name="facebook" onChange={this.onChange} 
                                                    disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"}value={this.state.propspecto.facebook} />
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <div className="input-group">
                                                    <span className="input-group-prepend"> <span
                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}><i className="icon-linkedin2"></i></span>
                                                    </span> <input type="text" placeholder="Escribir" className="form-control " name="instagram" onChange={this.onChange} 
                                                    disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                    && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"}value={this.state.propspecto.instagram} />
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <div className="input-group">
                                                    <span className="input-group-prepend"> <span
                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}><i className="icon-twitter"></i></span>
                                                    </span> <input type="text" placeholder="Escribir" className="form-control " name="twitter" onChange={this.onChange} 
                                                    disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                    && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"}value={this.state.propspecto.twitter} />
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                    <div className="col-xs-12 col-lg-4">
                                        <div className="form-group">
                                            <span>
                                                <span >Observaciones</span>
                                            </span>
                                            <div className="input-group">
                                                <textarea style={{ height: '150px' }} type="text" placeholder="Escribir" name="observaciones" onChange={this.onChange} 
                                                disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"}value={this.state.propspecto.observaciones} className="form-control " />
                                            </div>
                                        </div>
                                    </div>



                                </div>

                            </div>
                            <div>
                                <HistoricoGeneral idProspecto={this.props.idProspecto} tipoUsuario={this.state.tipoUsuario} presentarSeccion = {"REG"} />
                            </div>
                        </div>
                    </div>
                }




            </div>

        )
    }
}