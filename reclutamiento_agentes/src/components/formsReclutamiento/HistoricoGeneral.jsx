import React, { Component } from "react";
import ScrollTo from "react-scroll-into-view";
import ReporteCapacitacion from "./ReporteCapacitacion";
import ConvenioArranque from "./ConvenioArranque";
import axios from 'axios';
import datos from '../urls/datos.json'

const avatar = require("../imagenes/avatar.png")
export default class HistoricoGeneral extends Component {
    constructor() {
        super();
        this.onChange = this.onChange.bind(this)
        this.construyeSelectMotivos = this.construyeSelectMotivos.bind(this)
        this.construyeSelectEdicion = this.construyeSelectEdicion.bind(this)
        this.construyeSelectReferido = this.construyeSelectReferido.bind(this)
        this.construyeSelectConexion = this.construyeSelectConexion.bind(this)
        this.mostrarConvenioArranque = this.mostrarConvenioArranque.bind(this)
        this.mostrarReporteCapacitacion = this.mostrarReporteCapacitacion.bind(this)
        this.mostrarConvenioArranque = this.mostrarConvenioArranque.bind(this)
        this.construyeSelectAgenteReferidor = this.construyeSelectAgenteReferidor.bind(this)
        this.mostrarConectado = this.mostrarConectado.bind(this)
        this.guardarFormulario = this.guardarFormulario.bind(this)
        this.state = {
            estatus_seccion: 1,
            muestra_encuentro: 0,
            muestra_venta_carrera: 0,
            entrevistador: '',
            entrevistadorConexion: '',
            clickScroll: true,
            contadorHijos: 1,
            selectSAS: [],
            selectFuenteReclutamientoBono: [],
            selectAgenteReferidor: [],
            muestraAgente: false,

            archivoPSP: undefined,
            muestraDocPSP: false,
            colorSubirArchivoPSP: "#E1E5F0",
            colorTextoSubirArchivoPSP: "#617187",
            colorBotonSubirArchivoPSP: "#313A46",
            nombreArchivoPSP: "",
            archivoPSP: undefined,
            textoPuesto: 'Subir archivo',
            muestraInfoPSP: false,
            eficaciaVentas: 0,
            eficaciaEmpresarial: 0,
            rendimientoVentas: 0,
            fechaRespPSP: '',
            ventaDominante: '',
            interpretacion: '',
            rutaPSP: '',
            prospectoAgente: {
                "id": null,
                "prospecto_id": null,
                "fecha_aniversario": null,
                "hijo_uno": null,
                "fecha_hijo_uno": null,
                "hijo_dos": null,
                "fecha_hijo_dos": null,
                "hijo_tres": null,
                "fecha_hijo_tres": null,
                "hijo_cuatro": null,
                "fecha_hijo_cuatro": null,
                "hijo_cinco": null,
                "fecha_hijo_cinco": null,
                "asistente": null,
                "telefono_asistente": null,
                "correo_asistente": null,
                "fecha_nacimiento_asistente": null,
                "contacto": null,
                "parentesco_id": null,
                "telefono_contacto": null
            },
            prospectoConexion: {
                "id": null,
                "prospecto_id": null,
                "estatus_id": 17,
                "doc_foto": null,
                "conyuge": null,
                "fecha_conyuge": null,
                "doc_identificacion": null,
                "doc_acta_nacimiento": null,
                "doc_cerfificado": null,
                "doc_curp": null,
                "doc_rfc": null,
                "doc_cedula": null,
                "doc_carta_renuncia": null,
                "envio_correo": null,
                "fecha_examen_cei": null,
                "doc_examen_cei": null,
                "aprobacion_examen_cei": null,
                "doc_aprobacion_cei": null,
                "fecha_curso_cedula": null,
                "comprobante_cedula": null,
                "fecha_entrega_cedula": null,
                "fecha_alta_cnsf": null,
                "fecha_ini_cedula": null,
                "fecha_fin_cedula": null,
                "clave_cua": null,
                "clave_cua_provisional": null,
                "fecha_conexion_provicional": null,
                "fecha_conexion_definitiva": null,
                "antiguedad_agente": null,
                "alta_sat": null,
                "envio_gnp_multa": null,
                "curso_ideas": null,
                "generacion": null,
                "agente_consolidado_id": null,
                "correo_aps": null,
                "fecha_fin_tarjetas": null,
                "fecha_alta_boletin": null,
                "fecha_plenarias_aps": null,
                "fecha_alta_chats": null,
                "alta_chats_noveles": null,
                "alta_agenda_aps": null,
                "alta_prospecto_ideas": null,
                "curso_inmersion_aps": null,
                "baja_curso_inmersion_aps": null,
                "doc_capacitacion": null,
                "doc_convenio": null,
                "baja_conexion": null
            },
            prospecto:
            {
                "id": null,
                "prospecto_id": null,
                "estatus_id": 12,
                "orientacion_logro_edat": 0,
                "orientacion_logro_edat2": 0,
                "orientacion_logro_gdd": 0,
                "orientacion_logro_capacita": 0,
                "orientacion_logro_direccion": 0,
                "perceverancia_edat": 0,
                "perceverancia_edat2": 0,
                "perceverancia_gdd": 0,
                "perceverancia_capacita": 0,
                "perceverancia_direccion": 0,
                "integridad_edat": 0,
                "integridad_edat2": 0,
                "integridad_gdd": 0,
                "integridad_capacita": 0,
                "integridad_direccion": 0,
                "sentido_comun_edat": 0,
                "sentido_comun_edat2": 0,
                "sentido_comun_gdd": 0,
                "sentido_comun_capacita": 0,
                "sentido_comun_direccion": 0,
                "energia_edat": 0,
                "energia_edat2": 0,
                "energia_gdd": 0,
                "energia_capacita": 0,
                "energia_direccion": 0,
                "motivacion_edat": 0,
                "motivacion_edat2": 0,
                "motivacion_gdd": 0,
                "motivacion_capacita": 0,
                "motivacion_direccion": 0,
                "total_edat": 0,
                "total_edat2": 0,
                "total_gdd": 0,
                "total_capacita": 0,
                "total_direccion": 0
            },
            prospecto_ventaCarrera: {
                "id": null,
                "prospecto_id": null,
                "estatus_id": 16,
                "fecha_cita": null,
                "gdd": null,
                "generacion": null,
                "curso_inmersion": true,
                "motivo_curso_inmersion": null,
                "ingreso_curso_inmersion": null,
                "conexion_id": null,
                "participacion_edat": null,
                "descripcion_edat": null,
                "correo_aps": null,
                "curso_cedula": null,
                "grupo_explorando": null
            },

            prospecto_knockout:
            {
                "id": null,
                "prospecto_id": null,
                "estatus_id": 16,
                "falta_patron": null,
                "incapacidad_aceptar": null,
                "bajo_nivel": null,
                "recientemente_divorciado": null,
                "malos_habitos": null,
                "demasiados_cambios": null,
                "mercado_natural": null,
                "benefactor_profecional": null,
                "falta_movilidad": null,
                "problemas_salud": null,
                "problemas_financieros": null,
                "ventas_retroceso": null,
                "culpa_demas": null,
                "cualquier_trabajo": null,
                "seguros_vida": null,
                "status_quo": null,
                "resultado_entrevista": null,
                "gdd_id": null,
                "causas_rechazo": null
            },
            prospectoCandidato: {
                "id": null,
                "prospecto_id": null,
                "invitar_comida": null,
                "invitar_comida_prque": null,
                "aportar_equipo": null,
                "rasgos_importantes": null,
                "tiempo_carrera": null,
                "necesidades_financieras": null,
                "redes_sociales": null,
                "colchon_economico": null,
                "resp_patrimonio": null,
                "resp_patrimonio_porque": null,
                "debilidad": null
            },
            prospecto_importantes: {

                "id": 1,
                "prospecto_id": 1,
                "estatus_id": 13,
                "espiritu_emprendedor_edat": null,
                "espiritu_emprendedor_gdd": null,
                "mercado_natural_edat": null,
                "mercado_natural_gdd": null,
                "apoyo_familiar_edat": null,
                "apoyo_familiar_gdd": null,
                "estabilidad_financiera_edat": null,
                "estabilidad_financiera_gdd": null,
                "educacion_edat": null,
                "educacion_gdd": null,
                "movilidad_social_edat": null,
                "movilidad_social_gdd": null

            },
            prospecto_entrevistador:
            {
                "id": null,
                "prospecto_id": null,
                "estatus_id": 11,
                "puntualidad": null,
                "minutos_tarde": null,
                "habilidad_expresarse": null,
                "habilidad_sintetizar": null,
                "apariencia_personal": null,
                "demostracion_confianza": null,
                "muestra_seguro": null,
                "encuentra_seriamente": null,
                "observaciones": null
            },

            prospecto_areas: {
                salud: 0,
                amigos: 0,
                dinero: 0,
                familia: 0,
                trabajo: 0,
                desarrollo_personal: 0,
                diversion: 0,
                pareja: 0,
            },

            prospecto_agente: {
                "id": null,
                "prospecto_id": null,
                "estatus_id": 9,
                "fecha_alta": null,
                "cedula_vigencia": null,
                "agente_gnp": null,
                "tipo_cedula": null,
                "aseguradoras": null,
                "da_estuviste": null,
                "buscar_cambiarse": null,
                "cartera_vida": null,
                "conservacion_vida": null,
                "cartera_gmm": null,
                "vigente_gmm": null,
                "vigente_autos": null,
                "cartera_autos": null,
                "cartera_danos": null,
                "cartera_empresarial": null
            },

            prospecto_explorando4: {
                "id": null,
                "prospecto_id": null,
                "estatus_id": 8,
                "valores_identifica": null,
                "logro_academico": null,
                "logro_laboral": null,
                "cituacion_dificil": null,
                "deuda_financiera": null,
                "monto_deuda": null,
                "aspectos_mejora": null,
                "hilo_descontento": null
            },
            prospecto_explorando3: {
                "id": null,
                "prospecto_id": null,
                "estatus_id": 7,
                "meta_profecional": null,
                "fortaleza_posees": null,
                "mejores_cualidades": null,
                "piensas_seguros": null,
                "areas_mejora": null,
                "persona_exitosa": null,
                "principal_motivacion": null,
                "hobbi_lectura": null,
                "hobbi_deportes": null,
                "hobbi_cine": null,
                "hobbi_teatro": null,
                "hobbi_viajes": null,
                "grande_sueno": null,
                "colega_cercano": null,
                "mayor_debilidad": null
            },

            prospecto_explorando2: {
                "id": 1,
                "prospecto_id": null,
                "estatus_id": 6,
                "ocupacion_actual": null,
                "desde_cuando": null,
                "bien_trabajo_actual": null,
                "cuanto_percives": null,
                "actividades_disfrutas": null,
                "reconocimiento_recibes": null,
                "cambio_empleo": null,
                "opina_venta": null,
                "opina_emprender": null,
                "esperas_ganar_hoy": null,
                "esperas_ganar_cinco": null,
                "describe_venta": null
            },
            prospecto_explorando1: {
                "prospecto_id": 0,
                "id": 0,
                "estatus_id": 5,
                "carrera": null,
                "cp": null,
                "ciudad_id": null,
                "municipio_id": null,
                "colonia_id": null,
                "calle": null,
                "num_ext": null,
                "num_int": null,
                "universidad": null,
                "infonavit": null,
                "monto_credito": null,
                "fuiste_agente": null,
                "grupo_bal": null,
                "gobierno": null,
                "vivienda_resides": null,
                "vivienda_resides_otro": null,
                "conquien_vives": null,
                "conquien_vives_otro": null,
                "medio_transporte": null,
                "medio_transporte_otro": null,
                "ganar_millon": null,
                "facebook": null,
                "instagram": null,
                "twitter": null,
                "observaciones": null
            },
            archivoIdentificacion: undefined, nombreArchivoIdentificacion: "", urlIdentificacion: "",
            muestraDocIdentificacion: false,// se activa al edittar 
            colorSubirArchivoIdentificacion: "#78CB5A", colorTextoSubirArchivoIdentificacion: "#FFFFFF", colorBotonSubirArchivoIdentificacion: "#41B3D1",

            archivoActaNacimiento: undefined, nombreArchivoActaNacimiento: "", urlActaNacimiento: "",
            muestraDocActaNacimiento: false,// se activa al edittar 
            colorSubirArchivoActaNacimiento: "#78CB5A", colorTextoSubirArchivoActaNacimiento: "#FFFFFF", colorBotonSubirArchivoActaNacimiento: "#41B3D1",

            archivoCertificadoEstudios: undefined, nombreArchivoCertificadoEstudios: "", urlCertificadoEstudios: "",
            muestraDocCertificadoEstudios: false,// se activa al edittar 
            colorSubirArchivoCertificadoEstudios: "#78CB5A", colorTextoSubirArchivoCertificadoEstudios: "#FFFFFF", colorBotonSubirArchivoCertificadoEstudios: "#41B3D1",

            archivoCurp: undefined, nombreArchivoCurp: "", urlCurp: "",
            muestraDocCurp: false,// se activa al edittar 
            colorSubirArchivoCurp: "#78CB5A", colorTextoSubirArchivoCurp: "#FFFFFF", colorBotonSubirArchivoCurp: "#41B3D1",

            archivoRFC: undefined, nombreArchivoRFC: "", urlRFC: "",
            muestraDocRFC: false,// se activa al edittar 
            colorSubirArchivoRFC: "#78CB5A", colorTextoSubirArchivoRFC: "#FFFFFF", colorBotonSubirArchivoRFC: "#41B3D1",

            archivoCedula: undefined, nombreArchivoCedula: "", urlCedula: "",
            muestraDocCedula: false,// se activa al edittar 
            colorSubirArchivoCedula: "#78CB5A", colorTextoSubirArchivoCedula: "#FFFFFF", colorBotonSubirArchivoCedula: "#41B3D1",

            archivoRenuncia: undefined, nombreArchivoRenuncia: "", urlRenuncia: "",
            muestraDocRenuncia: false,// se activa al edittar 
            colorSubirArchivoRenuncia: "#78CB5A", colorTextoSubirArchivoRenuncia: "#FFFFFF", colorBotonSubirArchivoRenuncia: "#41B3D1",

            archivoComprobantePagoCEI: undefined, nombreArchivoComprobantePagoCEI: "", urlComprobantePagoCEI: "",
            muestraDocComprobantePagoCEI: false,// se activa al edittar 
            colorSubirArchivoComprobantePagoCEI: "#78CB5A", colorTextoSubirArchivoComprobantePagoCEI: "#FFFFFF", colorBotonSubirArchivoComprobantePagoCEI: "#41B3D1",

            archivoAprobacionExamenCEI: undefined, nombreArchivoAprobacionExamenCEI: "", urlAprobacionExamenCEI: "",
            muestraDocAprobacionExamenCEI: false,// se activa al edittar 
            colorSubirArchivoAprobacionExamenCEI: "#78CB5A", colorTextoSubirArchivoAprobacionExamenCEI: "#FFFFFF", colorBotonSubirArchivoAprobacionExamenCEI: "#41B3D1",

            archivoConvenioFirmado: undefined, nombreArchivoConvenioFirmado: "", urlConvenioFirmado: "",
            muestraDocConvenioFirmado: false,// se activa al edittar 
            colorSubirArchivoConvenioFirmado: "#78CB5A", colorTextoSubirArchivoConvenioFirmado: "#FFFFFF", colorBotonSubirArchivoConvenioFirmado: "#41B3D1",

            archivoCapacitacion: undefined, nombreArchivoCapacitacion: "", urlCapacitacion: "",
            muestraDocCapacitacion: false,// se activa al edittar 
            colorSubirArchivoCapacitacion: "#78CB5A", colorTextoSubirArchivoCapacitacion: "#FFFFFF", colorBotonSubirArchivoCapacitacion: "#41B3D1",
            mostrarReporteCapacitacion: false,
            mostrarConvenioArranque: false,
            imagenEnCambio: undefined,
            fotografiaFile: undefined,
            selectMotivosBaja: [],
            selectAgentesConsolidados: [],

            referido: {},
            colorSubirArchivoCV: "#E1E5F0",
            colorTextoSubirArchivoCV: "#617187",
            colorBotonSubirArchivoCV: "#0F69B8",
            selectEstadoCivil: [],
            selectMotivos: [],

            selectCiudad: [],
            selectMunicipio: [],
            selectColonia: [],
            selectEDAT: [],
            selectFuenteReclutamiento: [],


            selectMotivos: [],
            selectTipoConexion: undefined,
            selectGDD: undefined,
            selectSDDC: [],
            selectGDDVC: undefined,
            selectParentesco: undefined,
            tipoConexion: '',
            definitivo: false,
            provisional: false,
            ban_baja_conexion: false,
            ban_ventaCarrera: false,
            conexion: {}
        }
    }

    construyeSelectConectado(url, selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)
                for (var i = 0; i < filas.length; i++) {
                    let fila = filas[i]
                    if (selector == "selectParentesco") {
                        if (this.state.prospectoAgente.parentesco_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }
                    }
                }
                let salida = []
                salida.push(<select disabled={true} onChange={this.onChange} name={selector} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ [selector]: salida })
            });
    }

    construyeSelectEdicion(url, selector, id) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                console.log("respuesta de las ciudades ", json)
                for (var i = 0; i < filas.length; i++) {

                    let fila = filas[i]
                    if (selector == "selectCiudad") {

                        if (this.state.prospecto_explorando1.ciudad_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }
                    }
                    if (selector == "selectMunicipio") {

                        if (this.state.prospecto_explorando1.municipio_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[2].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[2].value}</option>)
                        }
                    }
                    if (selector == "selectColonia") {

                        if (this.state.prospecto_explorando1.colonia_id == fila.fila[1].value) {
                            options.push(<option selected value={fila.fila[1].value}>{fila.fila[2].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[1].value}>{fila.fila[2].value}</option>)
                        }
                    }



                }
                let salida = []
                salida.push(<select disabled name={selector} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ [selector]: salida })
            });
    }

    construyeSelectConexion(url, selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                if (selector == "selectAgentesConsolidados") {
                    filas.push({ "id": 1, "descripcion": "Agente reclutado directamente por el GDD y desarrollado en su equipo" })
                    filas.push({ "id": 2, "descripcion": "Agente entregado al GDD desde su inicio y desarrollado en su equipo" })
                    filas.push({ "id": 3, "descripcion": "Agente heredado por el GDD sin haber pasado sus años de novel en el equipo" })
                }

                console.log("respuesta de las ciudades ", json)
                for (var i = 0; i < filas.length; i++) {

                    let fila = filas[i]
                    if (selector == "selectMotivosBaja") {

                        if (this.state.prospectoConexion.baja_curso_inmersion_aps == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }

                    } else if (selector == "selectAgentesConsolidados") {
                        if (this.state.prospectoConexion.agente_consolidado_id == fila.id) {
                            options.push(<option selected value={fila.id}>{fila.descripcion}</option>)
                        } else {
                            options.push(<option value={fila.id}>{fila.descripcion}</option>)
                        }
                    }
                }
                let salida = []
                salida.push(<select disabled={false} onChange={this.onChange} name={selector} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ [selector]: salida })
            });
    }


    construyeSelectReferido(url, selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                for (var i = 0; i < filas.length; i++) {

                    let fila = filas[i]
                    if (selector == "selectEDAT") {
                        if (this.state.referido.empleado_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[2].value + ' ' + fila.fila[3].value + ' ' + fila.fila[4].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[2].value + ' ' + fila.fila[3].value + ' ' + fila.fila[4].value}</option>)
                        }
                    }
                    if (selector == "selectFuenteReclutamiento") {
                        if (this.state.referido.fuente_reclutamiento_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }
                    }
                    if (selector == "selectFuenteReclutamientoBono") {
                        if (this.state.referido.fuente_recluta_bonos_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }
                    }
                    if (selector == "selectEstadoCivil") {
                        if (this.state.referido.estadocivil_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }
                    }
                }
                let salida = []
                salida.push(<select disabled onChange={this.onChange} name={selector} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ [selector]: salida })
            });
    }

    construyeSelectAgenteReferidor(url, selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                for (var i = 0; i < filas.length; i++) {
                    let fila = filas[i]
                    if (selector == "selectAgenteReferidor") {
                        console.log("referido aqui" + this.state.referido.agente_referidor);
                        if (this.state.referido.agente_referidor == fila.id) {
                            options.push(<option selected value={fila.id + "," + fila.agente_referidor + "," + fila.cua_referidor}>{fila.agente_referidor}</option>)
                        } else {
                            options.push(<option value={fila.id + "," + fila.agente_referidor + "," + fila.cua_referidor}>{fila.agente_referidor}</option>)
                        }
                    }

                }
                let salida = []
                salida.push(<select onChange={this.onChange} disabled={true} name={selector} style={{ borderColor: '#E1E5F0' }} className="form-control " aria-label="Default select example">{options}</select>)
                this.setState({ selectAgenteReferidor: salida })
            })
    }

    mostrarReporteCapacitacion() {
        this.guardarFormulario(false)
    }

    guardarFormulario(banFormulario) {
        console.log("json ", this.state.prospectoConexion)
        let json = this.state.prospectoConexion
        json.id = this.props.idProspecto
        json.prospecto_id = this.props.idProspecto
        fetch(datos.urlServicePy + "recluta/api_recluta_conexion17/" + this.props.idProspecto)
            .then(response => response.json())
            .then(existeProspecto => {
                console.log("existeProspecto:::" + existeProspecto)
                if (banFormulario) {
                    this.props.botonCancelar()
                } else {
                    this.setState({ mostrarReporteCapacitacion: true, mostrarConvenioArranque: false })
                }
            })
    }

    onChangeGDD(event) {
        event.stopPropagation();
        event.preventDefault();
        let prospectoGDD = this.state.prospecto;
        console.log("Entro en onChangeGDD");
        /*if (event.target.name == "orientacion_logro_gdd" || event.target.name == "perceverancia_gdd" || event.target.name == "integridad_gdd"  || event.target.name == "sentido_comun_gdd"  || event.target.name == "energia_gdd"  || event.target.name == "motivacion_gdd"){
            prospectoGDD["" + event.target.name + ""] = event.target.value
        }*/
        if (event.target.value <= 10) {
            prospectoGDD["" + event.target.name + ""] = parseInt(event.target.value)
            prospectoGDD.total_gdd = parseInt(prospectoGDD.orientacion_logro_gdd) + parseInt(prospectoGDD.perceverancia_gdd) + parseInt(prospectoGDD.integridad_gdd) + parseInt(prospectoGDD.sentido_comun_gdd) + parseInt(prospectoGDD.energia_gdd) + parseInt(prospectoGDD.motivacion_gdd)
        }
        if (event.target.name == "orientacion_logro_gdd" || event.target.name == "perceverancia_gdd" || event.target.name == "integridad_gdd" || event.target.name == "sentido_comun_gdd" || event.target.name == "energia_gdd" || event.target.name == "motivacion_gdd") {
            let factores_vitales_gdd = event.target.value
            if (factores_vitales_gdd <= 10) {
                prospectoGDD["" + event.target.name + ""] = event.target.value
            }
        }

        this.setState({ prospecto: prospectoGDD })
    }

    onChange = e => {
        console.log(e.target.name, e.target.value, e.target.checked)
        let pros_venta = this.state.prospecto_ventaCarrera;
        let pros_import = this.state.prospecto_importantes;
        let pros_conexion = this.state.prospectoConexion;
        let bandForm = true;

        if (e.target.name == "envio_correo" || e.target.name == "aprobacion_examen_cei" || e.target.name == "comprobante_cedula" || e.target.name == "alta_sat"
            || e.target.name == "envio_gnp_multa" || e.target.name == "curso_ideas" || e.target.name == "alta_chats_noveles" || e.target.name == "alta_agenda_aps" || e.target.name == "alta_prospecto_ideas"
            || e.target.name == "curso_inmersion_aps") {
            if (e.target.checked == true) {
                pros_conexion["" + e.target.name + ""] = 1
            } else {
                pros_conexion["" + e.target.name + ""] = 0
            }
        } else if (e.target.name == "curso_cedula" || e.target.name == "participacion_edat" || e.target.name == "correo_aps") {
            if (e.target.checked == true) {
                pros_venta["" + e.target.name + ""] = 1
            } else {
                pros_venta["" + e.target.name + ""] = 0
            }
        } else if (e.target.name == "curso_inmersion_aps") {
            if (e.target.checked == true) {
                pros_conexion["" + e.target.name + ""] = 1
            } else {
                pros_conexion["" + e.target.name + ""] = 0
            }
        } else if (e.target.name == "grupo_explorando") {
            console.log("entro en grupo_explorando :::");
            if (e.target.checked == true) {
                pros_venta["" + e.target.name + ""] = parseInt(e.target.value)
            } else {
                pros_venta["" + e.target.name + ""] = 0
            }
        } else if (e.target.name == "selectGDD") {
            pros_venta.gdd = parseInt(e.target.value)
        } else if (e.target.name == "selectTipoConexion") {
            pros_venta.conexion_id = parseInt(e.target.value)
        } else if (e.target.name == "espiritu_emprendedor_gdd" || e.target.name == "mercado_natural_gdd" || e.target.name == "apoyo_familiar_gdd" || e.target.name == "estabilidad_financiera_gdd" ||
            e.target.name == "educacion_gdd" || e.target.name == "movilidad_social_gdd") {
            if (e.target.checked == true) {
                pros_venta["" + e.target.name + ""] = 1
            } else {
                pros_venta["" + e.target.name + ""] = 0
            }
            bandForm = false
        } else if (e.target.name == "baja_conexion") {
            if (e.target.checked == true) {
                this.setState({ ban_baja_conexion: true })
                pros_conexion["" + e.target.name + ""] = 1
            } else {
                pros_conexion["" + e.target.name + ""] = 0
                pros_conexion.baja_curso_inmersion_aps = 0
                this.setState({ ["selectMotivosBaja"]: null })
                this.construyeSelectConexion(datos.urlServicePy + "parametros/api_cat_causas_baja/0", "selectMotivosBaja")
                this.setState({ ban_baja_conexion: false })
            }
        } else {
            pros_conexion["" + e.target.name + ""] = e.target.value
        }

        if (bandForm) {
            this.setState({ prospecto_ventaCarrera: pros_venta })
        } else {
            this.setState({ prospecto_importantes: pros_import })
        }
        this.setState({ prospectoConexion: pros_conexion })

    }

    mostrarConvenioArranque() {
        this.setState({ mostrarReporteCapacitacion: false, mostrarConvenioArranque: true })
    }

    mostrarConectado() {
        fetch(datos.urlServicePy + "recluta/api_recluta_conexion17/" + this.props.idProspecto)
            .then(response => response.json())
            .then(existeProspecto => {
                this.setState({ mostrarReporteCapacitacion: false, mostrarConvenioArranque: true })

            })

        fetch(datos.urlServicePy + "recluta/api_recluta_conexion17/" + this.props.idProspecto)
            .then(response => response.json())
            .then(existeProspecto => {
                if (existeProspecto.length > 0) {
                    this.setState({ prospectoConexion: existeProspecto[0] })

                    let prospectoConexion = existeProspecto[0]

                    if (prospectoConexion.doc_identificacion != null && prospectoConexion.doc_identificacion.length > 0) {
                        this.setState({
                            urlIdentificacion: prospectoConexion.doc_identificacion, muestraDocIdentificacion: true, colorSubirArchivoIdentificacion: "#78CB5A",
                            colorTextoSubirArchivoIdentificacion: "#FFFFFF", colorBotonSubirArchivoIdentificacion: "#E5F6E0", nombreArchivoIdentificacion: 'modificar'
                        })
                    }
                    if (prospectoConexion.doc_acta_nacimiento != null && prospectoConexion.doc_acta_nacimiento.length > 0) {
                        this.setState({
                            urlActaNacimiento: prospectoConexion.doc_acta_nacimiento, muestraDocActaNacimiento: true, colorSubirArchivoActaNacimiento: "#78CB5A",
                            colorTextoSubirArchivoActaNacimiento: "#FFFFFF", colorBotonSubirArchivoActaNacimiento: "#E5F6E0", nombreArchivoActaNacimiento: 'modificar'
                        })
                    }
                    if (prospectoConexion.doc_cerfificado != null && prospectoConexion.doc_cerfificado.length > 0) {
                        this.setState({
                            urlCertificadoEstudios: prospectoConexion.doc_cerfificado, muestraDocCertificadoEstudios: true, colorSubirArchivoCertificadoEstudios: "#78CB5A",
                            colorTextoSubirArchivoCertificadoEstudios: "#FFFFFF", colorBotonSubirArchivoCertificadoEstudios: "#E5F6E0", nombreArchivoCertificadoEstudios: 'modificar'
                        })
                    }
                    if (prospectoConexion.doc_curp != null && prospectoConexion.doc_curp.length > 0) {
                        this.setState({
                            urlCurp: prospectoConexion.doc_curp, muestraDocCurp: true, colorSubirArchivoCurp: "#78CB5A",
                            colorTextoSubirArchivoCurp: "#FFFFFF", colorBotonSubirArchivoCurp: "#E5F6E0", nombreArchivoCurp: 'modificar'
                        })
                    }
                    if (prospectoConexion.doc_rfc != null && prospectoConexion.doc_rfc.length > 0) {
                        this.setState({
                            urlRFC: prospectoConexion.doc_rfc, muestraDocRFC: true, colorSubirArchivoRFC: "#78CB5A",
                            colorTextoSubirArchivoRFC: "#FFFFFF", colorBotonSubirArchivoRFC: "#E5F6E0", nombreArchivoRFC: 'modificar'
                        })
                    }
                    if (prospectoConexion.doc_cedula != null && prospectoConexion.doc_cedula.length > 0) {
                        this.setState({
                            urlCedula: prospectoConexion.doc_cedula, muestraDocCedula: true, colorSubirArchivoCedula: "#78CB5A",
                            colorTextoSubirArchivoCedula: "#FFFFFF", colorBotonSubirArchivoCedula: "#E5F6E0", nombreArchivoCedula: 'modificar'
                        })
                    }
                    if (prospectoConexion.doc_carta_renuncia != null && prospectoConexion.doc_carta_renuncia.length > 0) {
                        this.setState({
                            urlRenuncia: prospectoConexion.doc_carta_renuncia, muestraDocRenuncia: true, colorSubirArchivoRenuncia: "#78CB5A",
                            colorTextoSubirArchivoRenuncia: "#FFFFFF", colorBotonSubirArchivoRenuncia: "#E5F6E0", nombreArchivoRenuncia: 'modificar'
                        })
                    }
                    if (prospectoConexion.doc_examen_cei != null && prospectoConexion.doc_examen_cei.length > 0) {
                        this.setState({
                            urlComprobantePagoCEI: prospectoConexion.doc_examen_cei, muestraDocComprobantePagoCEI: true, colorSubirArchivoComprobantePagoCEI: "#78CB5A",
                            colorTextoSubirArchivoComprobantePagoCEI: "#FFFFFF", colorBotonSubirArchivoComprobantePagoCEI: "#E5F6E0", nombreArchivoComprobantePagoCEI: 'modificar'
                        })
                    }

                    if (prospectoConexion.doc_aprobacion_cei != null && prospectoConexion.doc_aprobacion_cei.length > 0) {
                        this.setState({
                            urlAprobacionExamenCEI: prospectoConexion.doc_aprobacion_cei, muestraDocAprobacionExamenCEI: true, colorSubirArchivoAprobacionExamenCEI: "#78CB5A",
                            colorTextoSubirArchivoAprobacionExamenCEI: "#FFFFFF", colorBotonSubirArchivoAprobacionExamenCEI: "#E5F6E0", nombreArchivoAprobacionExamenCEI: 'modificar'
                        })
                    }

                    if (prospectoConexion.doc_convenio != null && prospectoConexion.doc_convenio.length > 0) {
                        this.setState({
                            urlConvenioFirmado: prospectoConexion.doc_convenio, muestraDocConvenioFirmado: true, colorSubirArchivoConvenioFirmado: "#78CB5A",
                            colorTextoSubirArchivoConvenioFirmado: "#FFFFFF", colorBotonSubirArchivoConvenioFirmado: "#E5F6E0", nombreArchivoConvenioFirmado: 'modificar'
                        })
                    }
                    if (prospectoConexion.doc_capacitacion != null && prospectoConexion.doc_capacitacion.length > 0) {
                        this.setState({
                            urlCapacitacion: prospectoConexion.doc_capacitacion, muestraDocCapacitacion: true, colorSubirArchivoCapacitacion: "#78CB5A",
                            colorTextoSubirArchivoCapacitacion: "#FFFFFF", colorBotonSubirArchivoCapacitacion: "#E5F6E0", nombreArchivoCapacitacion: 'modificar'
                        })
                    }

                    console.log("el props ", prospectoConexion)
                    if (prospectoConexion.doc_foto != null && prospectoConexion.doc_foto.length > 0) {
                        console.log("la puta foto")
                        this.setState({
                            imagenEnCambio: prospectoConexion.doc_foto,
                            urlIdentificacion: prospectoConexion.doc_foto
                        })
                    }



                }
                this.construyeSelect(datos.urlServicePy + "parametros/api_cat_causas_baja/0", "selectMotivosBaja")
                this.setState({ mostrarReporteCapacitacion: false, mostrarConvenioArranque: false })
            })
    }

    onChangeFileConvenioFirmado(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file de puesto", file);
        this.setState({ archivoConvenioFirmado: file, colorSubirArchivoConvenioFirmado: "#78CB5A", colorTextoSubirArchivoConvenioFirmado: "#FFFFFF", colorBotonSubirArchivoConvenioFirmado: "#E5F6E0", nombreArchivoConvenioFirmado: file.name })

    }

    construyeSelect(url, selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json
                let options = []
                let ventaCarrera = this.state.prospecto_ventaCarrera;
                console.log("Entro en selector");
                if (selector == "selectTipoConexion") {
                    if (ventaCarrera.conexion_id != null) {
                        if (ventaCarrera.conexion_id == 1) {
                            filas.push({ "id": 1, "descripcion": "Agente definitivo" })
                            filas.push({ "id": 2, "descripcion": "Agente provisional" })
                        } else {
                            filas.push({ "id": 2, "descripcion": "Agente provisional" })
                            filas.push({ "id": 1, "descripcion": "Agente definitivo" })
                        }
                    } else if (this.state.prospecto_explorando1.fuiste_agente == 0 && this.state.prospecto_explorando1.grupo_bal == 0
                        && this.state.prospecto_explorando1.gobierno == 0 && this.state.prospecto_explorando1.infonavit == 0) {
                        ventaCarrera.conexion_id = 2
                        filas.push({ "id": 2, "descripcion": "Agente provisional" })
                        filas.push({ "id": 1, "descripcion": "Agente definitivo" })
                    } else {
                        ventaCarrera.conexion_id = 1
                        filas.push({ "id": 1, "descripcion": "Agente definitivo" })
                        filas.push({ "id": 2, "descripcion": "Agente provisional" })
                    }
                    this.setState({ "prospecto_ventaCarrera": ventaCarrera })
                }

                options.push(<option selected value={0}>Seleccionar</option>)
                for (var i = 0; i < filas.length; i++) {

                    let fila = filas[i]
                    if (selector == "selectTipoConexion") {
                        if (this.state.prospecto_ventaCarrera.conexion_id == fila.id) {
                            options.push(<option selected value={fila.id}>{fila.descripcion}</option>)
                        } else {
                            options.push(<option value={fila.id}>{fila.descripcion}</option>)
                        }
                    } else if (selector == "selectGDD") {
                        if (this.state.prospecto_knockout.gdd_id == fila.id) {
                            options.push(<option selected value={fila.id}>{fila.empleado}</option>)
                        } else {
                            options.push(<option value={fila.id}>{fila.empleado}</option>)
                        }
                    } else if (selector == "selectSDDC") {
                        if (this.state.prospecto_knockout.sddc_id == fila.id) {
                            options.push(<option selected value={fila.id}>{fila.empleado}</option>)
                        } else {
                            options.push(<option value={fila.id}>{fila.empleado}</option>)
                        }
                    } else if (selector == "selectGDDVC") {
                        if (this.state.prospecto_ventaCarrera.gdd == fila.id) {
                            options.push(<option selected value={fila.id}>{fila.empleado}</option>)
                        } else {
                            options.push(<option value={fila.id}>{fila.empleado}</option>)
                        }
                    } else if (selector == "selectSAS") {
                        if (this.state.prospecto_ventaCarrera.sas_id == fila.id) {
                            options.push(<option selected value={fila.id}>{fila.empleado}</option>)
                        } else {
                            options.push(<option value={fila.id}>{fila.empleado}</option>)
                        }
                    }
                }
                let salida = []
                salida.push(<select disabled={true} onChange={this.onChange} name={selector} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ [selector]: salida })
            });
    }

    construyeSelectMotivos(url) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                console.log("respuesta  ", json)
                for (var i = 0; i < filas.length; i++) {
                    let fila = filas[i]

                    options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                }
                let salida = []
                salida.push(<select onChange={this.onChangeMotivos} name="selectMotivos" style={{ borderColor: '#F1F3FA' }} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ selectMotivos: salida })


            })
    }


    UNSAFE_componentWillMount() {
        //Obtiene En registro
        fetch(datos.urlServicePy + "parametros/api_recluta_prospectos/" + this.props.idProspecto)
            .then(response => response.json())
            .then(json => {
                if (json != null) {
                    let fila = json.filas[0].fila
                    let columnas = json.columnas
                    let columnasSalida = {}
                    for (var i = 0; i < fila.length; i++) {
                        console.log(fila[i])
                        columnasSalida["" + columnas[i].key + ""] = fila[i].value
                    }
                    let agenteReferidor = columnasSalida.fuente_reclutamiento_id != null ? parseInt(columnasSalida.fuente_reclutamiento_id) : 0;
                    this.setState({ referido: columnasSalida, muestraAgente: agenteReferidor == 14 ? true : false })
                    if (columnasSalida.doc_cv != null && columnasSalida.doc_cv.length > 0) {
                        this.setState({ colorSubirArchivoCV: "#78CB5A", colorTextoSubirArchivoCV: "#FFFFFF", colorBotonSubirArchivoCV: "#E5F6E0", nombreArchivoCV: "Reemplazar...", muestraDocCV: true })
                    }
                    this.construyeSelectReferido(datos.urlServicePy + "parametros/api_cat_empleados/0", "selectEDAT")
                    this.construyeSelectReferido(datos.urlServicePy + "parametros/api_cat_fuente_reclutamiento/0", "selectFuenteReclutamiento")
                    this.construyeSelectReferido(datos.urlServicePy + "parametros/api_cat_fuente_reclutamiento_bonos/0", "selectFuenteReclutamientoBono")
                    this.construyeSelectReferido(datos.urlServicePy + "parametros/api_cat_estadocivil/0", "selectEstadoCivil")
                    this.construyeSelectAgenteReferidor(datos.urlServicePy + "recluta/api_recluta_vagente_referidor/0", "selectAgenteReferidor");

                    this.setState({ referido: columnasSalida, estatus_seccion: 1 })
                    //Obtiene 1-4 Entrevista Profunda
                    //1.-Entrevista Profunda
                    fetch(datos.urlServicePy + "recluta/api_recluta_encuentro01/" + this.props.idProspecto)
                        .then(response => response.json())
                        .then(prospecto_explorando1 => {
                            if (prospecto_explorando1.length > 0) {
                                this.setState({ prospecto_explorando1: prospecto_explorando1[0] })
                                this.construyeSelectEdicion(datos.urlServicePy + "parametros/api_cat_ciudad/0", "selectCiudad")
                                this.construyeSelectEdicion(datos.urlServicePy + "parametros/api_cat_municipio/0", "selectMunicipio")
                                this.construyeSelectEdicion(datos.urlServicePy + "parametros/api_cat_colonia/" + prospecto_explorando1[0].colonia_id, "selectColonia")

                                fetch(datos.urlServicePy + "parametros/api_recluta_prospectos/" + this.props.idProspecto)
                                    .then(response => response.json())
                                    .then(recluta_prospecto => {
                                        console.log("recluta_propspecto", recluta_prospecto)
                                        fetch(datos.urlServicePy + "parametros/api_cat_empleados/" + recluta_prospecto.filas[0].fila[12].value)
                                            .then(response => response.json())
                                            .then(entrevistador => {
                                                console.log("entrevistador ", entrevistador)
                                                this.setState({ entrevistador: entrevistador.filas[0].fila[2].value + " " + entrevistador.filas[0].fila[3].value + " " + (entrevistador.filas[0].fila[4].value != null ? entrevistador.filas[0].fila[4].value : '') })
                                            })
                                    })
                                this.setState({ estatus_seccion: 4, muestra_encuentro: 1 })
                                //2.-Entrevista Profunda
                                fetch(datos.urlServicePy + "recluta/api_recluta_encuentro02/" + this.props.idProspecto)
                                    .then(response => response.json())
                                    .then(prospecto_explorando2 => {
                                        if (prospecto_explorando2.length > 0) {
                                            this.setState({ prospecto_explorando2: prospecto_explorando2[0], estatus_seccion: 4, muestra_encuentro: 2 })
                                            //3.-Entrevista Profunda
                                            fetch(datos.urlServicePy + "recluta/api_recluta_encuentro03/" + this.props.idProspecto)
                                                .then(response => response.json())
                                                .then(prospecto_explorando3 => {
                                                    if (prospecto_explorando3.length > 0) {
                                                        this.setState({ prospecto_explorando3: prospecto_explorando3[0], estatus_seccion: 4, muestra_encuentro: 3 })
                                                        //4.-Entrevista Profunda
                                                        fetch(datos.urlServicePy + "recluta/api_recluta_encuentro04/" + this.props.idProspecto)
                                                            .then(response => response.json())
                                                            .then(prospecto_explorando4 => {
                                                                if (prospecto_explorando4.length > 0) {
                                                                    this.setState({ prospecto_explorando4: prospecto_explorando4[0], estatus_seccion: 4, muestra_encuentro: 4 })
                                                                    //Obtiene 5 Fuiste Agente
                                                                    fetch(datos.urlServicePy + "recluta/api_recluta_expagente05/" + this.props.idProspecto)
                                                                        .then(response => response.json())
                                                                        .then(prospecto_agente => {
                                                                            if (prospecto_agente.length > 0) {
                                                                                this.setState({ prospecto_agente: prospecto_agente[0], estatus_seccion: 5 })
                                                                            }
                                                                        })

                                                                    //Obtiene 6 Áreas de vida
                                                                    fetch(datos.urlServicePy + "recluta/api_recluta_areasvida06/" + this.props.idProspecto)
                                                                        .then(response => response.json())
                                                                        .then(prospecto_areas => {
                                                                            if (prospecto_areas.length > 0) {
                                                                                this.setState({ prospecto_areas: prospecto_areas[0], estatus_seccion: 6 })
                                                                            }
                                                                        })
                                                                    //Obtiene 7 Evaluación del entrevistador
                                                                    fetch(datos.urlServicePy + "recluta/api_recluta_evaluacion07/" + this.props.idProspecto)
                                                                        .then(response => response.json())
                                                                        .then(prospecto_entrevistador => {
                                                                            if (prospecto_entrevistador.length > 0) {
                                                                                this.setState({ prospecto_entrevistador: prospecto_entrevistador[0], estatus_seccion: 7 })
                                                                                //Obtiene 8 Factores Vitales
                                                                                fetch(datos.urlServicePy + "recluta/api_recluta_vitales08/" + this.props.idProspecto)
                                                                                    .then(response => response.json())
                                                                                    .then(jsonData => {
                                                                                        if (jsonData.length > 0) {
                                                                                            let jsonProspecto = jsonData[0]
                                                                                            jsonProspecto.total_gdd = parseInt(jsonProspecto.orientacion_logro_gdd) + parseInt(jsonProspecto.perceverancia_gdd) + parseInt(jsonProspecto.integridad_gdd) + parseInt(jsonProspecto.sentido_comun_gdd) + parseInt(jsonProspecto.energia_gdd) + parseInt(jsonProspecto.motivacion_gdd)
                                                                                            this.setState({ prospecto: jsonProspecto, estatus_seccion: 8 })
                                                                                            //Obtiene 9 Factores Importantes
                                                                                            fetch(datos.urlServicePy + "recluta/api_recluta_factoresimp09/" + this.props.idProspecto)
                                                                                                .then(response => response.json())
                                                                                                .then(prospecto_importantes => {
                                                                                                    if (prospecto_importantes.length > 0) {
                                                                                                        this.setState({ prospecto_importantes: prospecto_importantes[0], estatus_seccion: 9 })
                                                                                                        //Obtiene 10 Knockout
                                                                                                        fetch(datos.urlServicePy + "recluta/api_recluta_knockout10/" + this.props.idProspecto)
                                                                                                            .then(response => response.json())
                                                                                                            .then(prospecto_knockout => {
                                                                                                                if (prospecto_knockout.length > 0) {
                                                                                                                    this.construyeSelect(datos.urlServicePy + "recluta/api_vcat_gdd/" + prospecto_knockout[0].gdd_id, "selectGDD")
                                                                                                                    this.construyeSelect(datos.urlServicePy + "recluta/api_vcat_sddc/" + prospecto_knockout[0].sddc_id, "selectSDDC")
                                                                                                                    fetch(datos.urlServicePy + "recluta/api_recluta_vcargapsppdf14/" + this.props.idProspecto)
                                                                                                                        .then(response => response.json())
                                                                                                                        .then(jsonRespuestaCarga => {
                                                                                                                            console.log("respuesta de la carga del pdf ", jsonRespuestaCarga)
                                                                                                                            if (jsonRespuestaCarga.length > 0) {
                                                                                                                                let interpretacionConcatenada = jsonRespuestaCarga[6] === undefined ? "" : jsonRespuestaCarga[6].respuesta
                                                                                                                                this.setState({
                                                                                                                                    muestraInfoPSP: true,
                                                                                                                                    rutaPSP: jsonRespuestaCarga[0].ironpsp,
                                                                                                                                    fechaRespPSP: jsonRespuestaCarga[0].respuesta,
                                                                                                                                    eficaciaVentas: jsonRespuestaCarga[1].porcentaje,
                                                                                                                                    eficaciaEmpresarial: jsonRespuestaCarga[2].porcentaje,
                                                                                                                                    rendimientoVentas: jsonRespuestaCarga[3].porcentaje,
                                                                                                                                    ventaDominante: jsonRespuestaCarga[4].respuesta,
                                                                                                                                    interpretacion: jsonRespuestaCarga[5].respuesta + " " + interpretacionConcatenada,
                                                                                                                                    muestraDocPSP: true,
                                                                                                                                    colorSubirArchivoPSP: "#78CB5A", colorTextoSubirArchivoPSP: "#FFFFFF", colorBotonSubirArchivoPSP: "#78CB5A"
                                                                                                                                })
                                                                                                                            }
                                                                                                                        })
                                                                                                                    this.setState({ prospecto_knockout: prospecto_knockout[0], estatus_seccion: 10 })
                                                                                                                    fetch(datos.urlServicePy + "recluta/api_conociendo_candidato/" + this.props.idProspecto)
                                                                                                                        .then(response => response.json())
                                                                                                                        .then(prospectoCandidato => {
                                                                                                                            if (prospectoCandidato.length > 0) {
                                                                                                                                this.setState({ prospectoCandidato: prospectoCandidato[0], estatus_seccion: 11 })
                                                                                                                                //Obtiene Venta de Carrera
                                                                                                                                fetch(datos.urlServicePy + "recluta/api_recluta_vitales08/" + this.props.idProspecto)
                                                                                                                                    .then(response => response.json())
                                                                                                                                    .then(jsonData => {
                                                                                                                                        if (jsonData.length > 0) {
                                                                                                                                            let jsonProspecto = jsonData[0]
                                                                                                                                            jsonProspecto.total_gdd = parseInt(jsonProspecto.orientacion_logro_gdd) + parseInt(jsonProspecto.perceverancia_gdd) + parseInt(jsonProspecto.integridad_gdd) + parseInt(jsonProspecto.sentido_comun_gdd) + parseInt(jsonProspecto.energia_gdd) + parseInt(jsonProspecto.motivacion_gdd)
                                                                                                                                            this.setState({ prospecto: jsonProspecto, estatus_seccion: 12, muestra_venta_carrera: 1 })
                                                                                                                                            this.construyeSelect(datos.urlServicePy + "recluta/api_cat_tipoconexion/0", "selectTipoConexion")
                                                                                                                                            fetch(datos.urlServicePy + "recluta/api_recluta_ventacarrera16/" + this.props.idProspecto)
                                                                                                                                                .then(response => response.json())
                                                                                                                                                .then(jsonVenta => {
                                                                                                                                                    if (jsonVenta.length > 0) {
                                                                                                                                                        this.setState({ prospecto_ventaCarrera: jsonVenta[0], estatus_seccion: 12, muestra_venta_carrera: 2 })
                                                                                                                                                        this.construyeSelect(datos.urlServicePy + "recluta/api_vcat_gdd/0", "selectGDDVC")
                                                                                                                                                        //this.construyeSelect(datos.urlServicePy+"recluta/api_vcat_sddc/" + jsonVenta[0].sddc_id, "selectSDDC")
                                                                                                                                                        this.construyeSelect(datos.urlServicePy + "recluta/api_vcat_sas/" + jsonVenta[0].sas_id, "selectSAS")
                                                                                                                                                    }
                                                                                                                                                })
                                                                                                                                            //Obtiene Conexión y Conectado
                                                                                                                                            fetch(datos.urlServicePy + "recluta/api_recluta_conexion17/" + this.props.idProspecto)
                                                                                                                                                .then(response => response.json())
                                                                                                                                                .then(existeProspecto => {
                                                                                                                                                    if (existeProspecto.length > 0) {
                                                                                                                                                        this.setState({ prospectoConexion: existeProspecto[0] })
                                                                                                                                                        let prospectoConexion = existeProspecto[0]
                                                                                                                                                        if (prospectoConexion.doc_identificacion != null && prospectoConexion.doc_identificacion.length > 0) {
                                                                                                                                                            this.setState({
                                                                                                                                                                urlIdentificacion: prospectoConexion.doc_identificacion, muestraDocIdentificacion: true, colorSubirArchivoIdentificacion: "#78CB5A",
                                                                                                                                                                colorTextoSubirArchivoIdentificacion: "#FFFFFF", colorBotonSubirArchivoIdentificacion: "#E5F6E0", nombreArchivoIdentificacion: 'modificar'
                                                                                                                                                            })
                                                                                                                                                        }
                                                                                                                                                        if (prospectoConexion.doc_acta_nacimiento != null && prospectoConexion.doc_acta_nacimiento.length > 0) {
                                                                                                                                                            this.setState({
                                                                                                                                                                urlActaNacimiento: prospectoConexion.doc_acta_nacimiento, muestraDocActaNacimiento: true, colorSubirArchivoActaNacimiento: "#78CB5A",
                                                                                                                                                                colorTextoSubirArchivoActaNacimiento: "#FFFFFF", colorBotonSubirArchivoActaNacimiento: "#E5F6E0", nombreArchivoActaNacimiento: 'modificar'
                                                                                                                                                            })
                                                                                                                                                        }
                                                                                                                                                        if (prospectoConexion.doc_cerfificado != null && prospectoConexion.doc_cerfificado.length > 0) {
                                                                                                                                                            this.setState({
                                                                                                                                                                urlCertificadoEstudios: prospectoConexion.doc_cerfificado, muestraDocCertificadoEstudios: true, colorSubirArchivoCertificadoEstudios: "#78CB5A",
                                                                                                                                                                colorTextoSubirArchivoCertificadoEstudios: "#FFFFFF", colorBotonSubirArchivoCertificadoEstudios: "#E5F6E0", nombreArchivoCertificadoEstudios: 'modificar'
                                                                                                                                                            })
                                                                                                                                                        }
                                                                                                                                                        if (prospectoConexion.doc_curp != null && prospectoConexion.doc_curp.length > 0) {
                                                                                                                                                            this.setState({
                                                                                                                                                                urlCurp: prospectoConexion.doc_curp, muestraDocCurp: true, colorSubirArchivoCurp: "#78CB5A",
                                                                                                                                                                colorTextoSubirArchivoCurp: "#FFFFFF", colorBotonSubirArchivoCurp: "#E5F6E0", nombreArchivoCurp: 'modificar'
                                                                                                                                                            })
                                                                                                                                                        }
                                                                                                                                                        if (prospectoConexion.doc_rfc != null && prospectoConexion.doc_rfc.length > 0) {
                                                                                                                                                            this.setState({
                                                                                                                                                                urlRFC: prospectoConexion.doc_rfc, muestraDocRFC: true, colorSubirArchivoRFC: "#78CB5A",
                                                                                                                                                                colorTextoSubirArchivoRFC: "#FFFFFF", colorBotonSubirArchivoRFC: "#E5F6E0", nombreArchivoRFC: 'modificar'
                                                                                                                                                            })
                                                                                                                                                        }
                                                                                                                                                        if (prospectoConexion.doc_cedula != null && prospectoConexion.doc_cedula.length > 0) {
                                                                                                                                                            this.setState({
                                                                                                                                                                urlCedula: prospectoConexion.doc_cedula, muestraDocCedula: true, colorSubirArchivoCedula: "#78CB5A",
                                                                                                                                                                colorTextoSubirArchivoCedula: "#FFFFFF", colorBotonSubirArchivoCedula: "#E5F6E0", nombreArchivoCedula: 'modificar'
                                                                                                                                                            })
                                                                                                                                                        }
                                                                                                                                                        if (prospectoConexion.doc_carta_renuncia != null && prospectoConexion.doc_carta_renuncia.length > 0) {
                                                                                                                                                            this.setState({
                                                                                                                                                                urlRenuncia: prospectoConexion.doc_carta_renuncia, muestraDocRenuncia: true, colorSubirArchivoRenuncia: "#78CB5A",
                                                                                                                                                                colorTextoSubirArchivoRenuncia: "#FFFFFF", colorBotonSubirArchivoRenuncia: "#E5F6E0", nombreArchivoRenuncia: 'modificar'
                                                                                                                                                            })
                                                                                                                                                        }
                                                                                                                                                        if (prospectoConexion.doc_examen_cei != null && prospectoConexion.doc_examen_cei.length > 0) {
                                                                                                                                                            this.setState({
                                                                                                                                                                urlComprobantePagoCEI: prospectoConexion.doc_examen_cei, muestraDocComprobantePagoCEI: true, colorSubirArchivoComprobantePagoCEI: "#78CB5A",
                                                                                                                                                                colorTextoSubirArchivoComprobantePagoCEI: "#FFFFFF", colorBotonSubirArchivoComprobantePagoCEI: "#E5F6E0", nombreArchivoComprobantePagoCEI: 'modificar'
                                                                                                                                                            })
                                                                                                                                                        }
                                                                                                                                                        if (prospectoConexion.doc_aprobacion_cei != null && prospectoConexion.doc_aprobacion_cei.length > 0) {
                                                                                                                                                            this.setState({
                                                                                                                                                                urlAprobacionExamenCEI: prospectoConexion.doc_aprobacion_cei, muestraDocAprobacionExamenCEI: true, colorSubirArchivoAprobacionExamenCEI: "#78CB5A",
                                                                                                                                                                colorTextoSubirArchivoAprobacionExamenCEI: "#FFFFFF", colorBotonSubirArchivoAprobacionExamenCEI: "#E5F6E0", nombreArchivoAprobacionExamenCEI: 'modificar'
                                                                                                                                                            })
                                                                                                                                                        }
                                                                                                                                                        if (prospectoConexion.doc_convenio != null && prospectoConexion.doc_convenio.length > 0) {
                                                                                                                                                            this.setState({
                                                                                                                                                                urlConvenioFirmado: prospectoConexion.doc_convenio, muestraDocConvenioFirmado: true, colorSubirArchivoConvenioFirmado: "#78CB5A",
                                                                                                                                                                colorTextoSubirArchivoConvenioFirmado: "#FFFFFF", colorBotonSubirArchivoConvenioFirmado: "#E5F6E0", nombreArchivoConvenioFirmado: 'modificar'
                                                                                                                                                            })
                                                                                                                                                        }
                                                                                                                                                        if (prospectoConexion.doc_capacitacion != null && prospectoConexion.doc_capacitacion.length > 0) {
                                                                                                                                                            this.setState({
                                                                                                                                                                urlCapacitacion: prospectoConexion.doc_capacitacion, muestraDocCapacitacion: true, colorSubirArchivoCapacitacion: "#78CB5A",
                                                                                                                                                                colorTextoSubirArchivoCapacitacion: "#FFFFFF", colorBotonSubirArchivoCapacitacion: "#E5F6E0", nombreArchivoCapacitacion: 'modificar'
                                                                                                                                                            })
                                                                                                                                                        }
                                                                                                                                                        if (prospectoConexion.doc_foto != null && prospectoConexion.doc_foto.length > 0) {
                                                                                                                                                            this.setState({
                                                                                                                                                                imagenEnCambio: prospectoConexion.doc_foto
                                                                                                                                                            })
                                                                                                                                                        }
                                                                                                                                                        fetch(datos.urlServicePy + "recluta/api_recluta_ventacarrera16/" + this.props.idProspecto)
                                                                                                                                                            .then(response => response.json())
                                                                                                                                                            .then(jsonVenta => {
                                                                                                                                                                if (jsonVenta.length > 0) {
                                                                                                                                                                    if (jsonVenta[0].curso_cedula != null) {
                                                                                                                                                                        if (jsonVenta[0].curso_cedula == 1) {
                                                                                                                                                                            this.setState({ ban_ventaCarrera: true })
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                    if (jsonVenta[0].conexion_id == 1) {
                                                                                                                                                                        this.setState({ tipoConexion: "Agente definitivo", definitivo: true })
                                                                                                                                                                    } else {
                                                                                                                                                                        this.setState({ tipoConexion: "Agente provisional", provisional: true })
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                            })

                                                                                                                                                        fetch(datos.urlServicePy + "parametros/api_recluta_prospectos/" + this.props.idProspecto)
                                                                                                                                                            .then(response => response.json())
                                                                                                                                                            .then(json => {
                                                                                                                                                                let fila = json.filas[0].fila
                                                                                                                                                                let columnas = json.columnas
                                                                                                                                                                let columnasSalida = {}
                                                                                                                                                                for (var i = 0; i < fila.length; i++) {
                                                                                                                                                                    console.log(fila[i])
                                                                                                                                                                    columnasSalida["" + columnas[i].key + ""] = fila[i].value
                                                                                                                                                                }
                                                                                                                                                                this.setState({ conexion: columnasSalida })
                                                                                                                                                            })

                                                                                                                                                        fetch(datos.urlServicePy + "parametros/api_recluta_prospectos/" + this.props.idProspecto)
                                                                                                                                                            .then(response => response.json())
                                                                                                                                                            .then(recluta_prospecto => {
                                                                                                                                                                console.log("recluta_propspecto", recluta_prospecto)
                                                                                                                                                                fetch(datos.urlServicePy + "parametros/api_cat_empleados/" + recluta_prospecto.filas[0].fila[12].value)
                                                                                                                                                                    .then(response => response.json())
                                                                                                                                                                    .then(entrevistador => {
                                                                                                                                                                        console.log("entrevistador ", entrevistador)
                                                                                                                                                                        this.setState({ entrevistadorConexion: entrevistador.filas[0].fila[2].value + " " + entrevistador.filas[0].fila[3].value + " " + (entrevistador.filas[0].fila[4].value != null ? entrevistador.filas[0].fila[4].value : '') })
                                                                                                                                                                    })
                                                                                                                                                            })
                                                                                                                                                        this.setState({ estatus_seccion: 13 })
                                                                                                                                                        this.construyeSelectConexion(datos.urlServicePy + "parametros/api_cat_causas_baja/0", "selectMotivosBaja")
                                                                                                                                                        if (existeProspecto[0].agente_consolidado_id != null) {
                                                                                                                                                            this.construyeSelectConexion(datos.urlServicePy + "parametros/api_cat_manejo_agentes_consolidado/" + existeProspecto[0].agente_consolidado_id, "selectAgentesConsolidados")
                                                                                                                                                        }
                                                                                                                                                        fetch(datos.urlServicePy + "recluta/api_agente_expediente/" + this.props.idProspecto)
                                                                                                                                                            .then(response => response.json())
                                                                                                                                                            .then(jsonExpediente => {
                                                                                                                                                                if (jsonExpediente.length > 0) {
                                                                                                                                                                    let prospectoAgente = jsonExpediente;
                                                                                                                                                                    let contadorHijos = this.state.contadorHijos
                                                                                                                                                                    //contadorHijos
                                                                                                                                                                    if (prospectoAgente[0].hijo_dos != null) {
                                                                                                                                                                        contadorHijos = contadorHijos + 1
                                                                                                                                                                    }
                                                                                                                                                                    if (prospectoAgente[0].hijo_tres != null) {
                                                                                                                                                                        contadorHijos = contadorHijos + 1
                                                                                                                                                                    }
                                                                                                                                                                    if (prospectoAgente[0].hijo_cuatro != null) {
                                                                                                                                                                        contadorHijos = contadorHijos + 1
                                                                                                                                                                    }
                                                                                                                                                                    if (prospectoAgente[0].hijo_cinco != null) {
                                                                                                                                                                        contadorHijos = contadorHijos + 1
                                                                                                                                                                    }
                                                                                                                                                                    this.construyeSelectConectado(datos.urlServicePy + "parametros/api_cat_parentesco/0", "selectParentesco")
                                                                                                                                                                    this.construyeSelectConectado(datos.urlServicePy + "parametros/api_cat_causas_baja/0", "selectMotivosBaja")
                                                                                                                                                                    this.setState({ estatus_seccion: 14, prospectoAgente: prospectoAgente[0], contadorHijos: contadorHijos })
                                                                                                                                                                }
                                                                                                                                                            });
                                                                                                                                                    }
                                                                                                                                                })
                                                                                                                                        }
                                                                                                                                    })
                                                                                                                            }
                                                                                                                        })
                                                                                                                }
                                                                                                            })

                                                                                                    }
                                                                                                })
                                                                                        }
                                                                                    })

                                                                            }
                                                                        })
                                                                }
                                                            })
                                                    }
                                                })

                                        }
                                    })

                            }
                        })

                }
            })





        this.setState({ mostrarReporteCapacitacion: false, mostrarConvenioArranque: false })
    }


    render() {
        return (
            <div>
                {
                    this.state.mostrarReporteCapacitacion == true &&
                    <ReporteCapacitacion mostrarConectado={this.mostrarConectado} idProspecto={this.props.idProspecto} tipoUsuario={this.props.tipoUsuario} nombreUsuario={this.props.nombreUsuario} tipoPantalla={"conectado"} ocultarSeccion={this.props.ocultarSeccion} />
                }
                {
                    this.state.mostrarConvenioArranque == true &&
                    <ConvenioArranque mostrarConectado={this.mostrarConectado} idProspecto={this.props.idProspecto} tipoUsuario={this.props.tipoUsuario} nombreUsuario={this.props.nombreUsuario} tipoPantalla={"conectado"} ocultarSeccion={this.props.ocultarSeccion} />
                }

                {
                    this.state.mostrarReporteCapacitacion == false && this.state.mostrarConvenioArranque == false &&
                    <div>
                        {((this.props.presentarSeccion == "EP14") && this.state.estatus_seccion >= 14) &&
                            <><div>
                                <div class="accordion" id="conectado">
                                    <div id="headingconectado">
                                        <h2 class="mb-0">
                                            <button class="btn " type="button" data-toggle="collapse" data-target="#collapsconectado" aria-expanded="true" aria-controls="collapsconectado" ref={(ref) => this.conectadoCapacitacion = ref}>
                                                <h6 class="mb-0 font-weight-semibold">Conectado</h6>
                                            </button>
                                        </h2>
                                    </div>
                                    <div class="card">
                                        <div id="collapsconectado" class="collapse " aria-labelledby="headingconectado" data-parent="#conectado">
                                            <div className="card-body">
                                                <div className="row">
                                                    <div className="col-xs-2 col-lg-4">
                                                        <h7 class="mb-0 font-weight-semibold">Datos Personales </h7>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div className="card-body">
                                                        <div className="row">
                                                            <div className="col-xs-2 col-lg-4">
                                                                <h7 class="mb-0 font-weight-semibold">Registro conectado</h7>
                                                            </div>
                                                        </div>
                                                        <div class="card">
                                                            <div className="card-body">
                                                                <div className="row">
                                                                    <div className="col-xs-6 col-lg-6">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Clave de correo APS asignada al prospecto</span>
                                                                                </span> <input type="text" className="form-control " name="correo_aps" disabled={true} onChange={this.onChange} value={this.state.prospectoConexion.correo_aps} />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-xs-6 col-lg-6">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Finalización tarjetas de presentación</span>
                                                                                </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " name="fecha_fin_tarjetas" disabled={true} onChange={this.onChange} value={this.state.prospectoConexion.fecha_fin_tarjetas} />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="row">
                                                                    <div className="col-xs-4 col-lg-4">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Fin de alta en boletín</span>
                                                                                </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " name="fecha_alta_boletin" disabled={true} onChange={this.onChange} value={this.state.prospectoConexion.fecha_alta_boletin} />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-xs-4 col-lg-4">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Fin a bienvenida a plenarias APS</span>
                                                                                </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " name="fecha_plenarias_aps" disabled={true} onChange={this.onChange} value={this.state.prospectoConexion.fecha_plenarias_aps} />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-xs-4 col-lg-4">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Invitación a grupo de facebook compartamos APS</span>
                                                                                </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " name="fecha_alta_chats" disabled={true} onChange={this.onChange} value={this.state.prospectoConexion.fecha_alta_chats} />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="row">
                                                                    <div className="col-xs-4 col-lg-4">
                                                                        <div className="custom-control custom-switch  mb-2" >
                                                                            <span className="input-group-prepend"> <span
                                                                                className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Alta en chats de Agentes noveles</span>
                                                                                <div className="col-xs-2 col-lg-2">
                                                                                    <input type="checkbox" class="custom-control-input" id="alta_chats_noveles" name="alta_chats_noveles" disabled={true} onChange={this.onChange} checked={this.state.prospectoConexion.alta_chats_noveles}></input>
                                                                                    <label class="custom-control-label" for="alta_chats_noveles"></label>
                                                                                </div></span>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-xs-4 col-lg-4">
                                                                        <div className="custom-control custom-switch  mb-2" >
                                                                            <span className="input-group-prepend"> <span className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Alta en Agenda APS</span>
                                                                                <div className="col-xs-2 col-lg-2">
                                                                                    <input type="checkbox" class="custom-control-input" id="alta_agenda_aps" name="alta_agenda_aps" disabled={true} onChange={this.onChange} checked={this.state.prospectoConexion.alta_agenda_aps}></input>
                                                                                    <label class="custom-control-label" for="alta_agenda_aps"></label>
                                                                                </div>
                                                                            </span>
                                                                        </div>
                                                                    </div>

                                                                    <div className="col-xs-2 col-lg-2">
                                                                        <a href="#"><ScrollTo selector={'#convenio_arranque'} onClick={this.onChangeConvenio} style={
                                                                            this.state.urlConvenioFirmado == "" || this.state.nombreArchivoConvenioFirmado == "" ?
                                                                                { color: "red" } : { color: "#2196f3" }} disabled={true}>
                                                                            <h6>Convenio de arranque</h6></ScrollTo></a>
                                                                    </div>
                                                                    <div className="col-xs-2 col-lg-2">
                                                                        <a href="#"><ScrollTo selector={'#scroll_end'} onClick={this.onChangeCapacitacion} style={
                                                                            this.state.urlCapacitacion == "" || this.state.nombreArchivoCapacitacion == "" ?
                                                                                { color: "red" } : { color: "#2196f3" }} disabled={true}>
                                                                            <h6>Registro de Capacitación</h6></ScrollTo></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card-body">
                                                <div className="row">
                                                    <div className="col-xs-2 col-lg-4">
                                                        <h7 class="mb-0 font-weight-semibold">Convenio de arranque</h7>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div className="card-body">
                                                        <div className="row">
                                                            <div className="col-xs-4 col-lg-4">
                                                                <h6 class="mb-0 font-weight-semibold">Convenio de arranque</h6>
                                                            </div>
                                                            <div className="col-xs-4 col-lg-4">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <form encType="multipart/form" style={{ display: 'none' }} >
                                                                            <input type="file" style={{ display: 'none' }} ref={(ref) => this.convenioFirmado = ref} onChange={this.onChangeFileConvenioFirmado.bind(this)}></input>
                                                                        </form>
                                                                        <input type="text" className="form-control " disabled={(this.props.tipoUsuario != "CAP" && this.props.tipoUsuario != "GDD") &&
                                                                            this.state.archivoConvenioFirmado == undefined && this.state.urlConvenioFirmado.length == 0
                                                                        } placeholder="Convenio firmado" value={this.state.nombreArchivoConvenioFirmado} />
                                                                        <span className="input-group-prepend">

                                                                            {
                                                                                this.state.muestraDocConvenioFirmado == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoConvenioFirmado }}>
                                                                                    <a href={this.state.urlConvenioFirmado} target="_blank" rel="noopener noreferrer" >
                                                                                        <i className="fas fa-eye"
                                                                                            style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                                    </a>
                                                                                </button> : ''


                                                                            }
                                                                            <button type="button" class="btn text-white" disabled={(this.props.tipoUsuario != "CAP" && this.props.tipoUsuario != "GDD") && this.state.archivoConvenioFirmado == undefined && this.state.urlConvenioFirmado.length == 0}
                                                                                onClick={this.onClickBotonArchivoConvenioFirmado} style={{ backgroundColor: this.state.colorBotonSubirArchivoConvenioFirmado }}>
                                                                                <h10 style={{ color: "white" }}>+</h10>
                                                                            </button>

                                                                            <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >Convenio firmado</span>
                                                                        </span>
                                                                    </div>
                                                                    {
                                                                        this.state.urlConvenioFirmado == "" || this.state.nombreArchivoConvenioFirmado == "" ?
                                                                            <span style={{ color: "red" }}>Convenio de arranque, es requerido</span> : ''
                                                                    }
                                                                </div>
                                                            </div>

                                                            <div className="col-xs-4 col-lg-4">
                                                                <div className="row">
                                                                    <div className="col-xs-2 col-lg-2">
                                                                        <>&nbsp;&nbsp;&nbsp;&nbsp;</>
                                                                    </div><div className="col-xs-2 col-lg-2">
                                                                        <>&nbsp;&nbsp;&nbsp;&nbsp;</>
                                                                    </div>
                                                                    <div className="col-xs-2 col-lg-2">
                                                                        {/*<button class="btn " style={{ borderColor: 'black' }} type="button" disabled = {this.props.tipoUsuario != "GDD"}><i className="icon-download"></i></button>*/}
                                                                        {
                                                                            this.state.muestraDocConvenioFirmado == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorSubirArchivoConvenioFirmado }}>
                                                                                <a href={this.state.urlConvenioFirmado} target="_blank" rel="noopener noreferrer" >
                                                                                    <i className="fas fa-eye"
                                                                                        style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                                </a>
                                                                            </button> : <a href="#">
                                                                                <i className="fas fa-eye"
                                                                                    style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Documento no existe" ></i>
                                                                            </a>
                                                                        }
                                                                    </div>
                                                                    <div className="col-xs-6 col-lg-6">
                                                                        <button class="btn " type="button" disabled={this.props.tipoUsuario != "GDD"} onClick={this.mostrarConvenioArranque} style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Generar documento</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card-body">
                                                <div className="row">
                                                    <div className="col-xs-2 col-lg-4">
                                                        <h7 class="mb-0 font-weight-semibold">Campos Expediente Agente</h7>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div className="card-body">
                                                        <div className="row">
                                                            <div className="col-xs-4 col-lg-4">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <span className="input-group-prepend"> <span
                                                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Aniversario de bodas del agente</span>
                                                                        </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " name="fecha_aniversario" disabled={true} value={this.state.prospectoAgente.fecha_aniversario} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-xs-8 col-lg-8">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <span className="input-group-prepend"> <span
                                                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre hijo 1 agente</span>
                                                                        </span> <input type="text" style={{ borderColor: '#E1E5F0' }} name="hijo_uno" disabled={true} className="form-control" value={this.state.prospectoAgente.hijo_uno} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xs-3 col-lg-3">
                                                                < div className="form-group">
                                                                    <div className="input-group">
                                                                        <span className="input-group-prepend"> <span
                                                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Fecha de nacimiento hijo 1 del agente</span>
                                                                        </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled={true} name="fecha_hijo_uno" value={this.state.prospectoAgente.fecha_hijo_uno} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        {
                                                            this.state.contadorHijos == 2 || this.state.contadorHijos > 2 ?
                                                                < div className="row">
                                                                    <div className="col-xs-8 col-lg-8">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre hijo 2 agente</span>
                                                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} disabled={true} name="hijo_dos" className="form-control " value={this.state.prospectoAgente.hijo_dos} />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-xs-3 col-lg-3">
                                                                        < div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Fecha de nacimiento hijo 2 del agente</span>
                                                                                </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled={true} name="fecha_hijo_dos" value={this.state.prospectoAgente.fecha_hijo_dos} />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div> :
                                                                ''
                                                        }
                                                        {
                                                            this.state.contadorHijos == 3 || this.state.contadorHijos > 3 ?
                                                                < div className="row">
                                                                    <div className="col-xs-8 col-lg-8">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre hijo 3 agente</span>
                                                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled={true} name="hijo_tres" value={this.state.prospectoAgente.hijo_tres} />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-xs-3 col-lg-3">
                                                                        < div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Fecha de nacimiento hijo 3 del agente</span>
                                                                                </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled={true} name="fecha_hijo_tres" value={this.state.prospectoAgente.fecha_hijo_tres} />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div> :
                                                                ''
                                                        }
                                                        {
                                                            this.state.contadorHijos == 4 || this.state.contadorHijos > 4 ?
                                                                < div className="row">
                                                                    <div className="col-xs-8 col-lg-8">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre hijo 4 agente</span>
                                                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled={true} name="hijo_cuatro" value={this.state.prospectoAgente.hijo_cuatro} />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-xs-3 col-lg-3">
                                                                        < div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Fecha de nacimiento hijo 4 del agente</span>
                                                                                </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled={true} name="fecha_hijo_cuatro" value={this.state.prospectoAgente.fecha_hijo_cuatro} />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div> :
                                                                ''
                                                        }
                                                        {
                                                            this.state.contadorHijos == 5 ?
                                                                < div className="row">
                                                                    <div className="col-xs-8 col-lg-8">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre hijo 5 agente</span>
                                                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled={true} name="hijo_cinco" value={this.state.prospectoAgente.hijo_cinco} />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-xs-3 col-lg-3">
                                                                        < div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Fecha de nacimiento hijo 5 del agente</span>
                                                                                </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled={true} name="fecha_hijo_cinco" value={this.state.prospectoAgente.fecha_hijo_cinco} />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div> :
                                                                ''
                                                        }
                                                        <div className="row">
                                                            <div className="col-xs-3 col-lg-3">
                                                                <button type="button" class="btn text-white" disabled={true} style={{ backgroundColor: "#0F69B8" }}>+ Agregar Hijo</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <button type="button" class="btn text-white" disabled={true} style={{ backgroundColor: "#0F69B8" }}>- Eliminar Hijo</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card-body">
                                                <div className="row">
                                                    <div className="col-xs-2 col-lg-4">
                                                        <h7 class="mb-0 font-weight-semibold">Campos Expediente Agente</h7>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div className="card-body">
                                                        <div className="row">
                                                            <div className="col-xs-12 col-lg-12">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <span className="input-group-prepend"> <span
                                                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre asistente del agente</span>
                                                                        </span> <input type="text" style={{ borderColor: '#E1E5F0' }} disabled={true} name="asistente" className="form-control " value={this.state.prospectoAgente.asistente} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-xs-6 col-lg-6">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <span className="input-group-prepend"> <span
                                                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Teléfono asistente del agente</span>
                                                                        </span> <input type="number" style={{ borderColor: '#E1E5F0' }} className="form-control " maxLength={10} disabled={true} name="telefono_asistente" value={this.state.prospectoAgente.telefono_asistente} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xs-6 col-lg-6">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <span className="input-group-prepend"> <span
                                                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Correo asistente del agente</span>
                                                                        </span> <input type="text" style={{ borderColor: '#E1E5F0' }} disabled={true} name="correo_asistente" className="form-control " value={this.state.prospectoAgente.correo_asistente} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-xs-6 col-lg-6">
                                                                < div className="form-group">
                                                                    <div className="input-group">
                                                                        <span className="input-group-prepend"> <span
                                                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Fecha de nacimiento asistente del agente</span>
                                                                        </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled={true} name="fecha_nacimiento_asistente" value={this.state.prospectoAgente.fecha_nacimiento_asistente} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-xs-12 col-lg-12">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <span className="input-group-prepend"> <span
                                                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre contacto</span>
                                                                        </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled={true} name="contacto" value={this.state.prospectoAgente.contacto} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-xs-6 col-lg-6">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <span className="input-group-prepend"> <span
                                                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Parentesco</span>
                                                                        </span>
                                                                        {this.state.selectParentesco}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xs-6 col-lg-6">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <span className="input-group-prepend"> <span
                                                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Teléfono de contacto</span>
                                                                        </span> <input type="number" style={{ borderColor: '#E1E5F0' }} className="form-control " maxLength={10} disabled={true} name="telefono_contacto" value={this.state.prospectoAgente.telefono_contacto} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div></>
                        }
                        {((this.props.presentarSeccion == "EP13" || this.props.presentarSeccion == "EP14") && this.state.estatus_seccion >= 12) &&
                            <><div>
                                <div class="accordion" id="conexion_capacitacion">
                                    <div id="headingconexion_capacitacion">
                                        <h2 class="mb-0">
                                            <button class="btn " type="button" data-toggle="collapse" data-target="#collapsconexion_capacitacion" aria-expanded="true" aria-controls="collapsconexion_capacitacion" ref={(ref) => this.conectadoCapacitacion = ref}>
                                                <h6 class="mb-0 font-weight-semibold">En Conexión / Capacitación </h6>
                                            </button>
                                        </h2>
                                    </div>
                                    <div class="card">
                                        <div id="collapsconexion_capacitacion" class="collapse " aria-labelledby="headingconexion_capacitacion" data-parent="#conexion_capacitacion">
                                            <div class="card">
                                                <div className="card-body">
                                                    <div className="row">
                                                        <div className="col-xs-2 col-lg-4">
                                                            <h7 class="mb-0 font-weight-semibold">Juego de Conexión</h7>
                                                        </div>
                                                    </div>
                                                    <div class="card">
                                                        <div className="card-body">
                                                            <div className="row">
                                                                <div className="col-xs-4 col-lg-4">
                                                                    <div className="row">
                                                                        <div className="col-xs-2 col-lg-4">
                                                                            <h8 class="mb-0 font-weight-semibold">Fotografía conexión</h8>
                                                                        </div>
                                                                        <div className="col-xs-2 col-lg-4">
                                                                            <div>
                                                                                <input type="image" disabled={false} src={this.state.imagenEnCambio == undefined ? avatar : this.state.imagenEnCambio}
                                                                                    class="img-fluid rounded-circle" width="40" height="40" alt="" />
                                                                                <form encType="multipart/form">
                                                                                    <input type="file" disabled={true} style={{ display: 'none' }}></input>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="col-xs-4 col-lg-4">
                                                                    <div className="form-group">
                                                                        <div className="input-group">
                                                                            <form encType="multipart/form" style={{ display: 'none' }} >
                                                                                <input type="file" style={{ display: 'none' }} disabled={true}></input>
                                                                            </form>
                                                                            <input type="text" className="form-control " disabled={true} placeholder="Identificación Oficial" value={this.state.nombreArchivoIdentificacion} />
                                                                            <span className="input-group-prepend">
                                                                                {
                                                                                    this.state.muestraDocIdentificacion == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoIdentificacion }}>
                                                                                        <a href={this.state.urlIdentificacion} target="_blank" rel="noopener noreferrer" >
                                                                                            <i className="fas fa-eye"
                                                                                                style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                                        </a>
                                                                                    </button> : ''
                                                                                }
                                                                                <button type="button" class="btn text-white" disabled={true} style={{ backgroundColor: this.state.colorBotonSubirArchivoIdentificacion }}>
                                                                                    <h10 style={{ color: "white" }}>+</h10>
                                                                                </button>
                                                                                <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >Identificación Oficial</span>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="col-xs-12 col-lg-4">
                                                                    <div className="form-group">
                                                                        <div className="input-group">
                                                                            <form encType="multipart/form" style={{ display: 'none' }} >
                                                                                <input type="file" style={{ display: 'none' }} disabled={true}></input>
                                                                            </form>
                                                                            <input type="text" className="form-control " disabled={true} placeholder="Acta de nacimiento" value={this.state.nombreArchivoActaNacimiento} />
                                                                            <span className="input-group-prepend">

                                                                                {
                                                                                    this.state.muestraDocActaNacimiento == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoActaNacimiento }}>
                                                                                        <a href={this.state.urlActaNacimiento} target="_blank" rel="noopener noreferrer" >
                                                                                            <i className="fas fa-eye"
                                                                                                style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                                        </a>
                                                                                    </button> : ''
                                                                                }
                                                                                <button type="button" class="btn text-white" disabled={true} style={{ backgroundColor: this.state.colorBotonSubirArchivoActaNacimiento }}>
                                                                                    <h10 style={{ color: "white" }}>+</h10>
                                                                                </button>

                                                                                <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >Acta de nacimiento</span>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                {/* aqui esta el conyge */}
                                                            </div>
                                                            <div className="row">
                                                                <div className="col-xs-12 col-lg-4">
                                                                    <div className="form-group">
                                                                        <div className="input-group">

                                                                            <form encType="multipart/form" style={{ display: 'none' }} >
                                                                                <input type="file" style={{ display: 'none' }} ref={(ref) => this.certificado = ref}></input>
                                                                            </form>
                                                                            <input type="text" className="form-control " disabled={true} placeholder="Certificado de estudios" value={this.state.nombreArchivoCertificadoEstudios} />
                                                                            <span className="input-group-prepend">

                                                                                {
                                                                                    this.state.muestraDocCertificadoEstudios == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoCertificadoEstudios }}>
                                                                                        <a href={this.state.urlCertificadoEstudios} target="_blank" rel="noopener noreferrer" >
                                                                                            <i className="fas fa-eye"
                                                                                                style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                                        </a>
                                                                                    </button> : ''


                                                                                }
                                                                                <button type="button" class="btn text-white" disabled={true} style={{ backgroundColor: this.state.colorBotonSubirArchivoCertificadoEstudios }}>
                                                                                    <h10 style={{ color: "white" }}>+</h10>
                                                                                </button>
                                                                                <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >Certificado de estudios</span>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="col-xs-12 col-lg-4">
                                                                    <div className="form-group">
                                                                        <div className="input-group">
                                                                            <form encType="multipart/form" style={{ display: 'none' }} >
                                                                                <input type="file" style={{ display: 'none' }} ref={(ref) => this.curp = ref}></input>
                                                                            </form>
                                                                            <input type="text" className="form-control " disabled={true} placeholder="CURP" value={this.state.nombreArchivoCurp} />
                                                                            <span className="input-group-prepend">

                                                                                {
                                                                                    this.state.muestraDocCurp == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoCurp }}>
                                                                                        <a href={this.state.urlCurp} target="_blank" rel="noopener noreferrer" >
                                                                                            <i className="fas fa-eye"
                                                                                                style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                                        </a>
                                                                                    </button> : ''


                                                                                }
                                                                                <button type="button" class="btn text-white" disabled={true} style={{ backgroundColor: this.state.colorBotonSubirArchivoCurp }}>
                                                                                    <h10 style={{ color: "white" }}>+</h10>
                                                                                </button>

                                                                                <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >CURP</span>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="col-xs-12 col-lg-4">
                                                                    <div className="form-group">
                                                                        <div className="input-group">
                                                                            <form encType="multipart/form" style={{ display: 'none' }} >
                                                                                <input type="file" style={{ display: 'none' }} ref={(ref) => this.rfc = ref}></input>
                                                                            </form>
                                                                            <input type="text" className="form-control " disabled={true} placeholder="RFC" value={this.state.nombreArchivoRFC} />
                                                                            <span className="input-group-prepend">

                                                                                {
                                                                                    this.state.muestraDocRFC == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoRFC }}>
                                                                                        <a href={this.state.urlRFC} target="_blank" rel="noopener noreferrer" >
                                                                                            <i className="fas fa-eye"
                                                                                                style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                                        </a>
                                                                                    </button> : ''


                                                                                }
                                                                                <button type="button" class="btn text-white" disabled={true} style={{ backgroundColor: this.state.colorBotonSubirArchivoRFC }}>
                                                                                    <h10 style={{ color: "white" }}>+</h10>
                                                                                </button>

                                                                                <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >RFC</span>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="row">
                                                                <div className="col-xs-12 col-lg-4">
                                                                    <div className="form-group">
                                                                        <div className="input-group">
                                                                            <form encType="multipart/form" style={{ display: 'none' }} >
                                                                                <input type="file" style={{ display: 'none' }} ref={(ref) => this.cedula = ref}></input>
                                                                            </form>
                                                                            <input type="text" className="form-control " disabled={true} placeholder="Cédula de agente" value={this.state.nombreArchivoCedula} />
                                                                            <span className="input-group-prepend">

                                                                                {
                                                                                    this.state.muestraDocCedula == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoCedula }}>
                                                                                        <a href={this.state.urlCedula} target="_blank" rel="noopener noreferrer" >
                                                                                            <i className="fas fa-eye"
                                                                                                style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                                        </a>
                                                                                    </button> : ''


                                                                                }
                                                                                <button type="button" class="btn text-white" disabled={true} style={{ backgroundColor: this.state.colorBotonSubirArchivoCedula }}>
                                                                                    <h10 style={{ color: "white" }}>+</h10>
                                                                                </button>

                                                                                <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >Cédula de agente</span>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="col-xs-12 col-lg-4">
                                                                    <div className="form-group">
                                                                        <div className="input-group">
                                                                            <form encType="multipart/form" style={{ display: 'none' }} >
                                                                                <input type="file" style={{ display: 'none' }} ref={(ref) => this.renuncia = ref}></input>
                                                                            </form>
                                                                            <input type="text" className="form-control " disabled={true} placeholder="Carta de renuncia" value={this.state.nombreArchivoRenuncia} />
                                                                            <span className="input-group-prepend">
                                                                                {
                                                                                    this.state.muestraDocRenuncia == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoRenuncia }}>
                                                                                        <a href={this.state.urlRenuncia} target="_blank" rel="noopener noreferrer" >
                                                                                            <i className="fas fa-eye"
                                                                                                style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                                        </a>
                                                                                    </button> : ''


                                                                                }
                                                                                <button type="button" class="btn text-white" disabled={true} style={{ backgroundColor: this.state.colorBotonSubirArchivoRenuncia }}>
                                                                                    <h10 style={{ color: "white" }}>+</h10>
                                                                                </button>
                                                                                <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >Carta de renuncia</span>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {this.state.provisional == true &&
                                                        <div id="datos_Conexion" name="datos_Conexion">
                                                            <div class="card">
                                                                <div class="card-header  header-elements-sm-inline">
                                                                    <h5 class="mb-0 font-weight-semibold" >Datos Conexión</h5>
                                                                </div>
                                                            </div>
                                                            <div className="row">
                                                                <div className="col-xs-2 col-lg-4">
                                                                    <h7 class="mb-0 font-weight-semibold">Servicio al socio</h7>
                                                                </div>
                                                            </div>
                                                            <div class="card">
                                                                <div className="card-body">
                                                                    <div className="row">
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="row">
                                                                                <div className="col-xs-8 col-lg-8">
                                                                                    <h8 class="mb-0 font-weight-semibold">Finalizó Curso IDEAS</h8>
                                                                                </div>
                                                                                <div className="col-xs-2 col-lg-4">
                                                                                    <div className="custom-control custom-switch  mb-2" >
                                                                                        <input type="checkbox" class="custom-control-input" id="curso_ideas" name="curso_ideas" disabled={true} checked={this.state.prospectoConexion.curso_ideas}></input>
                                                                                        <label class="custom-control-label" for="curso_ideas"></label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="form-group">
                                                                                <div className="input-group">
                                                                                    <span className="input-group-prepend"> <span
                                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Clave única de agente CUA definitivo</span>
                                                                                    </span> <input type="number" placeholder="" name="clave_cua" disabled={true} value={this.state.prospectoConexion.clave_cua} className="form-control " />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="form-group">
                                                                                <div className="input-group">
                                                                                    <span className="input-group-prepend"> <span
                                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Clave única de agente CUA provisional</span>
                                                                                    </span> <input type="number" placeholder="" name="clave_cua_provisional" disabled={true} value={this.state.prospectoConexion.clave_cua_provisional} className="form-control " />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="row">
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="form-group">
                                                                                <div className="input-group">
                                                                                    <span className="input-group-prepend"> <span
                                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Conexión GNP agente provisional</span>
                                                                                    </span> <input type="date" placeholder="" name="fecha_conexion_provicional" disabled={true} value={this.state.prospectoConexion.fecha_conexion_provicional} className="form-control " />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="form-group">
                                                                                <div className="input-group">
                                                                                    <span className="input-group-prepend"> <span
                                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Antigüedad del agente</span>
                                                                                    </span> <input type="number" placeholder="" name="antiguedad_agente" disabled={true} value={this.state.prospectoConexion.antiguedad_agente} className="form-control " />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="row">
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="row">
                                                                                {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>} <label >Generación de la figura comercial</label>
                                                                            </div>
                                                                            <div className="row">
                                                                                {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                                <div class="form-check">
                                                                                    <input class="form-check-input" type="checkbox" value={1} disabled={true} id="G1" name="generacion" checked={this.state.prospectoConexion.generacion == 1} />
                                                                                    <label class="form-check-label" for="G1">
                                                                                        G1
                                                                                    </label>
                                                                                </div>
                                                                                {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                                <div class="form-check">
                                                                                    <input class="form-check-input" type="checkbox" value={2} disabled={true} id="G2" name="generacion" checked={this.state.prospectoConexion.generacion == 2} />
                                                                                    <label class="form-check-label" for="G2">
                                                                                        G2
                                                                                    </label>
                                                                                </div>

                                                                                {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                                <div class="form-check">
                                                                                    <input class="form-check-input" type="checkbox" value={3} disabled={true} id="G3" name="generacion" checked={this.state.prospectoConexion.generacion == 3} />
                                                                                    <label class="form-check-label" for="G3">
                                                                                        G3
                                                                                    </label>
                                                                                </div>
                                                                                {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                                <div class="form-check">
                                                                                    <input class="form-check-input" type="checkbox" value={4} disabled={true} id="G4" name="generacion" checked={this.state.prospectoConexion.generacion == 4} />
                                                                                    <label class="form-check-label" for="G4">
                                                                                        G4
                                                                                    </label>
                                                                                </div>
                                                                                {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                                <div class="form-check">
                                                                                    <input class="form-check-input" type="checkbox" value={5} disabled={true} id="C" name="generacion" checked={this.state.prospectoConexion.generacion == 5} />
                                                                                    <label class="form-check-label" for="C">
                                                                                        C
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div className="col-xs-8 col-lg-8">
                                                                            <div className="form-group">
                                                                                <div className="input-group">
                                                                                    <span className="input-group-prepend"> <span
                                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Manejo y consideraciones de Agentes Consolidados</span>
                                                                                    </span> {this.state.selectAgentesConsolidados}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    }
                                                    {this.state.provisional == true &&
                                                        <div>
                                                            <div className="row">
                                                                <div className="col-xs-2 col-lg-4">
                                                                    <h7 class="mb-0 font-weight-semibold">Capacitación</h7>
                                                                </div>
                                                            </div>
                                                            <div class="card">
                                                                <div className="card-body">
                                                                    <div className="row">
                                                                        <div className="col-xs-2 col-lg-2">
                                                                            <h8 class="mb-0 font-weight-semibold">Alta del prospecto del Curso IDEAS</h8>
                                                                        </div>
                                                                        <div className="col-xs-2 col-lg-4">
                                                                            <div className="custom-control custom-switch  mb-2" >
                                                                                <input type="checkbox" class="custom-control-input" id="alta_prospecto_ideas" name="alta_prospecto_ideas" disabled={true} checked={this.state.prospectoConexion.alta_prospecto_ideas}></input>
                                                                                <label class="custom-control-label" for="alta_prospecto_ideas"></label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>}

                                                    {this.state.definitivo == true &&
                                                        <div>
                                                            <div class="card">
                                                                <div class="card-header  header-elements-sm-inline">
                                                                    <h5 class="mb-0 font-weight-semibold" >Datos Conexión</h5>
                                                                </div>
                                                            </div>
                                                            <div className="row">
                                                                <div className="col-xs-2 col-lg-4">
                                                                    <h7 class="mb-0 font-weight-semibold">Servicio al socio</h7>
                                                                </div>
                                                            </div>
                                                            <div class="card">
                                                                <div className="card-body">
                                                                    <div className="row">
                                                                        <div className="col-xs-8 col-lg-8">
                                                                            <div className="row">
                                                                                <div className="col-xs-8 col-lg-8">
                                                                                    <h8 class="mb-0 font-weight-semibold">Se envió correo electrónico solicitando pago de cedula y gestión de certificación al prospecto</h8>
                                                                                </div>
                                                                                <div className="col-xs-2 col-lg-4">
                                                                                    <div className="custom-control custom-switch  mb-2" >
                                                                                        <input type="checkbox" class="custom-control-input" id="envio_correo" name="envio_correo" disabled={true} checked={this.state.prospectoConexion.envio_correo}></input>
                                                                                        <label class="custom-control-label" for="envio_correo"></label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <br></br>
                                                                    <div className="row">
                                                                        <div className="col-xs-8 col-lg-8">
                                                                            <div className="form-group">
                                                                                <div className="input-group">
                                                                                    <form encType="multipart/form" style={{ display: 'none' }} >
                                                                                        <input type="file" style={{ display: 'none' }} ref={(ref) => this.pagocei = ref} disabled={true}></input>
                                                                                    </form>
                                                                                    <input type="text" className="form-control " placeholder="Comprobante de pago del examen en el CEI" value={this.state.nombreArchivoComprobantePagoCEI} />
                                                                                    <span className="input-group-prepend">

                                                                                        {
                                                                                            this.state.muestraDocComprobantePagoCEI == true ? <button type="button" class="btn text-white" disabled={this.props.tipoUsuario != "SAS"} style={{ backgroundColor: this.state.colorBotonSubirArchivoComprobantePagoCEI }}>
                                                                                                <a href={this.state.urlComprobantePagoCEI} target="_blank" rel="noopener noreferrer" >
                                                                                                    <i className="fas fa-eye"
                                                                                                        style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                                                </a>
                                                                                            </button> : ''


                                                                                        }
                                                                                        <button type="button" class="btn text-white" disabled={true} style={{ backgroundColor: this.state.colorBotonSubirArchivoComprobantePagoCEI }}>
                                                                                            <h10 style={{ color: "white" }}>+</h10>
                                                                                        </button>

                                                                                        <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >Comprobante de pago del examen en el CEI</span>
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="form-group">
                                                                                <div className="input-group">
                                                                                    <span className="input-group-prepend"> <span
                                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Examen en el CEI</span>
                                                                                    </span> <input type="date" placeholder="" name="fecha_examen_cei" disabled={true} onChange={this.onChange} value={this.state.prospectoConexion.fecha_examen_cei} className="form-control " />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <br></br>
                                                                    <div className="row">
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="row">
                                                                                <div className="col-xs-8 col-lg-8">
                                                                                    <h8 class="mb-0 font-weight-semibold">Aprobación del examen en el CEI</h8>
                                                                                </div>
                                                                                <div className="col-xs-2 col-lg-4">
                                                                                    <div className="custom-control custom-switch  mb-2" >
                                                                                        <input type="checkbox" class="custom-control-input" id="aprobacion_examen_cei" name="aprobacion_examen_cei" disabled={true} checked={this.state.prospectoConexion.aprobacion_examen_cei}></input>
                                                                                        <label class="custom-control-label" for="aprobacion_examen_cei"></label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-xs-8 col-lg-8">
                                                                            <div className="form-group">
                                                                                <div className="input-group">
                                                                                    <form encType="multipart/form" style={{ display: 'none' }} >
                                                                                        <input type="file" style={{ display: 'none' }} ref={(ref) => this.aprobacioncei = ref}></input>
                                                                                    </form>
                                                                                    <input type="text" className="form-control " disabled={true} placeholder="Evidencia de aprobación del examen en el CEI" value={this.state.nombreArchivoAprobacionExamenCEI} />
                                                                                    <span className="input-group-prepend">

                                                                                        {
                                                                                            this.state.muestraDocAprobacionExamenCEI == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoAprobacionExamenCEI }}>
                                                                                                <a href={this.state.urlAprobacionExamenCEI} target="_blank" rel="noopener noreferrer" >
                                                                                                    <i className="fas fa-eye"
                                                                                                        style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                                                </a>
                                                                                            </button> : ''


                                                                                        }
                                                                                        <button type="button" class="btn text-white" disabled={true} style={{ backgroundColor: this.state.colorBotonSubirArchivoAprobacionExamenCEI }}>
                                                                                            <h10 style={{ color: "white" }}>+</h10>
                                                                                        </button>
                                                                                        <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >Evidencia de aprobación del examen en el CEI</span>
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="row">
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="row">
                                                                                <div className="col-xs-8 col-lg-8">
                                                                                    <h8 class="mb-0 font-weight-semibold">Alta en SAT</h8>
                                                                                </div>

                                                                                <div className="col-xs-2 col-lg-4">
                                                                                    <div className="custom-control custom-switch  mb-2" >
                                                                                        <input type="checkbox" class="custom-control-input" id="alta_sat" name="alta_sat" disabled={true} checked={this.state.prospectoConexion.alta_sat}></input>
                                                                                        <label class="custom-control-label" for="alta_sat"></label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="form-group">
                                                                                <div className="input-group">
                                                                                    <span className="input-group-prepend"> <span
                                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Clave única de agente CUA definitivo</span>
                                                                                    </span> <input type="number" placeholder="" name="clave_cua" disabled={true} value={this.state.prospectoConexion.clave_cua} className="form-control " />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="form-group">
                                                                                <div className="input-group">
                                                                                    <span className="input-group-prepend"> <span
                                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Clave única de agente CUA provisional</span>
                                                                                    </span> <input type="number" placeholder="" name="clave_cua_provisional" disabled={true} value={this.state.prospectoConexion.clave_cua_provisional} className="form-control " />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="row">
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="form-group">
                                                                                <div className="input-group">
                                                                                    <span className="input-group-prepend"> <span
                                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Entrega de cédula en CNCF</span>
                                                                                    </span> <input type="date" placeholder="" name="fecha_entrega_cedula" disabled={true} value={this.state.prospectoConexion.fecha_entrega_cedula} className="form-control " />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="form-group">
                                                                                <div className="input-group">
                                                                                    <span className="input-group-prepend"> <span
                                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Alta en CNSF</span>
                                                                                    </span> <input type="date" placeholder="" name="fecha_alta_cnsf" disabled={true} value={this.state.prospectoConexion.fecha_alta_cnsf} className="form-control " />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="row">
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="form-group">
                                                                                <div className="input-group">
                                                                                    <span className="input-group-prepend"> <span
                                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fecha inicio vigencia de cédula</span>
                                                                                    </span> <input type="date" placeholder="" name="fecha_ini_cedula" disabled={true} value={this.state.prospectoConexion.fecha_ini_cedula} className="form-control " />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="form-group">
                                                                                <div className="input-group">
                                                                                    <span className="input-group-prepend"> <span
                                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fecha fin vigencia de cédula</span>
                                                                                    </span> <input type="date" placeholder="" name="fecha_fin_cedula" disabled={true} value={this.state.prospectoConexion.fecha_fin_cedula} className="form-control " />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="row">
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="form-group">
                                                                                <div className="input-group">
                                                                                    <span className="input-group-prepend"> <span
                                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Conexión GNP agente definitivo</span>
                                                                                    </span> <input type="date" placeholder="" name="fecha_conexion_definitiva" disabled={true} value={this.state.prospectoConexion.fecha_conexion_definitiva} className="form-control " />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="form-group">
                                                                                <div className="input-group">
                                                                                    <span className="input-group-prepend"> <span
                                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Antigüedad del agente</span>
                                                                                    </span> <input type="number" placeholder="" name="antiguedad_agente" disabled={true} value={this.state.prospectoConexion.antiguedad_agente} className="form-control " />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="row">
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="row">
                                                                                {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>} <label >Generación de la figura comercial</label>
                                                                            </div>
                                                                            <div className="row">
                                                                                {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                                <div class="form-check">
                                                                                    <input class="form-check-input" type="checkbox" value={1} disabled={true} id="G1" name="generacion" checked={this.state.prospectoConexion.generacion == 1} />
                                                                                    <label class="form-check-label" for="G1">
                                                                                        G1
                                                                                    </label>
                                                                                </div>
                                                                                {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                                <div class="form-check">
                                                                                    <input class="form-check-input" type="checkbox" value={2} disabled={true} id="G2" name="generacion" checked={this.state.prospectoConexion.generacion == 2} />
                                                                                    <label class="form-check-label" for="G2">
                                                                                        G2
                                                                                    </label>
                                                                                </div>

                                                                                {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                                <div class="form-check">
                                                                                    <input class="form-check-input" type="checkbox" value={3} disabled={true} id="G3" name="generacion" checked={this.state.prospectoConexion.generacion == 3} />
                                                                                    <label class="form-check-label" for="G3">
                                                                                        G3
                                                                                    </label>
                                                                                </div>
                                                                                {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                                <div class="form-check">
                                                                                    <input class="form-check-input" type="checkbox" value={4} disabled={true} id="G4" name="generacion" checked={this.state.prospectoConexion.generacion == 4} />
                                                                                    <label class="form-check-label" for="G4">
                                                                                        G4
                                                                                    </label>
                                                                                </div>
                                                                                {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                                <div class="form-check">
                                                                                    <input class="form-check-input" type="checkbox" value={5} disabled={true} id="C" name="generacion" checked={this.state.prospectoConexion.generacion == 5} />
                                                                                    <label class="form-check-label" for="C">
                                                                                        C
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div className="col-xs-8 col-lg-8">
                                                                            <div className="form-group">
                                                                                <div className="input-group">
                                                                                    <span className="input-group-prepend"> <span
                                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Manejo y consideraciones de Agentes Consolidados</span>
                                                                                    </span> {this.state.selectAgentesConsolidados}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    }
                                                    {this.state.definitivo == true &&
                                                        <div>
                                                            <div className="row">
                                                                <div className="col-xs-2 col-lg-4">
                                                                    <h7 class="mb-0 font-weight-semibold">Capacitación</h7>
                                                                </div>
                                                            </div>
                                                            <div class="card">
                                                                <div className="card-body">
                                                                    <div className="row">
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="form-group">
                                                                                <div className="input-group">
                                                                                    <span className="input-group-prepend"> <span
                                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Curso cédula A</span>
                                                                                    </span> <input type="date" placeholder="" disabled={true} name="fecha_curso_cedula" onChange={this.onChange} value={this.state.prospectoConexion.fecha_curso_cedula} className="form-control " />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="row">
                                                                                <div className="col-xs-8 col-lg-8">
                                                                                    <h8 class="mb-0 font-weight-semibold">Calificaciones y comprobante de pago de cédula</h8>
                                                                                </div>

                                                                                <div className="col-xs-2 col-lg-4">
                                                                                    <div className="custom-control custom-switch  mb-2" >
                                                                                        <input type="checkbox" class="custom-control-input" id="comprobante_cedula" name="comprobante_cedula" disabled={true} onChange={this.onChange} checked={this.state.prospectoConexion.comprobante_cedula}></input>
                                                                                        <label class="custom-control-label" for="comprobante_cedula"></label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="row">
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="row">
                                                                                <div className="col-xs-8 col-lg-8">
                                                                                    <h8 class="mb-0 font-weight-semibold">Envío GNP multa</h8>
                                                                                </div>
                                                                                <div className="col-xs-2 col-lg-4">
                                                                                    <div className="custom-control custom-switch  mb-2" >
                                                                                        <input type="checkbox" class="custom-control-input" id="envio_gnp_multa" name="envio_gnp_multa" disabled={true} onChange={this.onChange} checked={this.state.prospectoConexion.envio_gnp_multa}></input>
                                                                                        <label class="custom-control-label" for="envio_gnp_multa"></label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    }
                                                    <div className="row">
                                                        <div className="col-xs-2 col-lg-4">
                                                            <h7 class="mb-0 font-weight-semibold">Datos Capacitación</h7>
                                                        </div>
                                                    </div>
                                                    <div class="card" id="datos_Capacitacion" name="datos_Capacitacion">
                                                        <div className="card-body">
                                                            <div className="row">
                                                                <div className="col-xs-8 col-lg-8">
                                                                    <div className="row">
                                                                        <div className="col-xs-8 col-lg-8">
                                                                            <h8 class="mb-0 font-weight-semibold">Se envió correo electrónico con detalles para el Curso de Inmersión APS al prospecto</h8>
                                                                        </div>

                                                                        <div className="col-xs-2 col-lg-4">
                                                                            <div className="custom-control custom-switch  mb-2" >
                                                                                <input type="checkbox" class="custom-control-input" id="curso_inmersion_aps" name="curso_inmersion_aps" disabled={true} onChange={this.onChange} checked={this.state.prospectoConexion.curso_inmersion_aps}></input>
                                                                                <label class="custom-control-label" for="curso_inmersion_aps"></label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="row">
                                                                <div className="col-xs-2 col-lg-2">
                                                                    <h6 class="mb-0 font-weight-semibold">Baja</h6>
                                                                </div>
                                                                <div className="col-xs-4 col-lg-4">
                                                                    <div className="custom-control custom-switch  mb-2" >
                                                                        <input type="checkbox" class="custom-control-input" id="baja_conexion" name="baja_conexion" disabled={true} onChange={this.onChange} checked={this.state.prospectoConexion.baja_conexion}></input>
                                                                        <label class="custom-control-label" for="baja_conexion"></label>
                                                                    </div>
                                                                </div>
                                                                {
                                                                    this.state.ban_baja_conexion ?
                                                                        <div className="col-xs-6 col-lg-6">
                                                                            <div className="form-group">
                                                                                <div className="input-group">
                                                                                    <span className="input-group-prepend"> <span
                                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Si es el caso, especificar el motivo de baja del Curso de Inmersión APS</span>
                                                                                    </span>{this.state.selectMotivosBaja}
                                                                                </div>
                                                                            </div>
                                                                        </div> : ''
                                                                }
                                                            </div>
                                                            <div className="row">
                                                                <div className="col-xs-4 col-lg-4">
                                                                    <h6 class="mb-0 font-weight-semibold">Reporte de Capacitación</h6>
                                                                </div>
                                                                <div className="col-xs-4 col-lg-4">
                                                                    <div className="form-group">
                                                                        <div className="input-group">
                                                                            <form encType="multipart/form" style={{ display: 'none' }} >
                                                                                <input type="file" style={{ display: 'none' }}></input>
                                                                            </form>
                                                                            <input type="text" className="form-control " disabled={true} placeholder="Capacitación" value={this.state.nombreArchivoCapacitacion} />
                                                                            <span className="input-group-prepend">

                                                                                {
                                                                                    this.state.muestraDocCapacitacion == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoCapacitacion }}>
                                                                                        <a href={this.state.urlCapacitacion} target="_blank" rel="noopener noreferrer" >
                                                                                            <i className="fas fa-eye"
                                                                                                style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                                        </a>
                                                                                    </button> : ''


                                                                                }
                                                                                <button type="button" class="btn text-white" disabled={true} style={{ backgroundColor: this.state.colorBotonSubirArchivoCapacitacion }}>
                                                                                    <h10 style={{ color: "white" }}>+</h10>
                                                                                </button>

                                                                                <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >Capacitación</span>
                                                                            </span>
                                                                        </div>
                                                                        {
                                                                            this.state.urlCapacitacion == "" || this.state.nombreArchivoCapacitacion == "" ?
                                                                                <span style={{ color: "red" }}>Convenio firmado, es requerido</span> : ''
                                                                        }
                                                                    </div>
                                                                </div>
                                                                <div className="col-xs-4 col-lg-4">
                                                                    <div className="row">
                                                                        <div className="col-xs-2 col-lg-2">
                                                                            <>&nbsp;&nbsp;&nbsp;&nbsp;</>
                                                                        </div><div className="col-xs-2 col-lg-2">
                                                                            <>&nbsp;&nbsp;&nbsp;&nbsp;</>
                                                                        </div>
                                                                        <div className="col-xs-2 col-lg-2">
                                                                            {/*<button class="btn " style={{ borderColor: 'black' }} type="button" disabled = {this.props.tipoUsuario != "CAP" && this.props.tipoUsuario != "SAS" && this.props.tipoUsuario != "GDD"} ><i className="icon-download"></i></button>*/}
                                                                            {
                                                                                this.state.muestraDocCapacitacion == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorSubirArchivoCapacitacion }}>
                                                                                    <a href={this.state.urlCapacitacion} target="_blank" rel="noopener noreferrer" >
                                                                                        <i className="fas fa-eye"
                                                                                            style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Consultar Reporte Capacitación" ></i>
                                                                                    </a>
                                                                                </button> : <a href="#">
                                                                                    <i className="fas fa-eye"
                                                                                        style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Documento no existe" ></i>
                                                                                </a>
                                                                            }
                                                                        </div>
                                                                        <div className="col-xs-6 col-lg-6">
                                                                            <button class="btn " type="button" style={{ backgroundColor: '#D5D9E8', color: '#617187' }}
                                                                                disabled={true}
                                                                            >Generar documento</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="row">
                                                                <div className="col-xs-2 col-lg-4">
                                                                    <h7 class="mb-0 font-weight-semibold">Convenio de arranque</h7>
                                                                </div>
                                                            </div>
                                                            <div class="card">
                                                                <div className="card-body">
                                                                    <div className="row">
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <h6 class="mb-0 font-weight-semibold">Convenio de arranque</h6>
                                                                        </div>

                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="form-group">
                                                                                <div className="input-group">
                                                                                    <form encType="multipart/form" style={{ display: 'none' }} >
                                                                                        <input type="file" style={{ display: 'none' }}></input>
                                                                                    </form>
                                                                                    <input type="text" className="form-control " disabled={true} placeholder="Convenio firmado" value={this.state.nombreArchivoConvenioFirmado} />
                                                                                    <span className="input-group-prepend">

                                                                                        {
                                                                                            this.state.muestraDocConvenioFirmado == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoConvenioFirmado }}>
                                                                                                <a href={this.state.urlConvenioFirmado} target="_blank" rel="noopener noreferrer" >
                                                                                                    <i className="fas fa-eye"
                                                                                                        style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                                                </a>
                                                                                            </button> : ''


                                                                                        }
                                                                                        <button type="button" class="btn text-white" disabled={(this.props.tipoUsuario != "CAP" && this.props.tipoUsuario != "GDD") && this.state.archivoConvenioFirmado == undefined && this.state.urlConvenioFirmado.length == 0}
                                                                                            onClick={this.onClickBotonArchivoConvenioFirmado} style={{ backgroundColor: this.state.colorBotonSubirArchivoConvenioFirmado }}>
                                                                                            <h10 style={{ color: "white" }}>+</h10>
                                                                                        </button>

                                                                                        <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >Convenio firmado</span>
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="row">
                                                                                <div className="col-xs-2 col-lg-2">
                                                                                    <>&nbsp;&nbsp;&nbsp;&nbsp;</>
                                                                                </div><div className="col-xs-2 col-lg-2">
                                                                                    <>&nbsp;&nbsp;&nbsp;&nbsp;</>
                                                                                </div>
                                                                                <div className="col-xs-2 col-lg-2">
                                                                                    {/*<button class="btn " style={{ borderColor: 'black' }} type="button" disabled = {this.props.tipoUsuario != "GDD"}><i className="icon-download"></i></button>*/}
                                                                                    {
                                                                                        this.state.muestraDocConvenioFirmado == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorSubirArchivoConvenioFirmado }}>
                                                                                            <a href={this.state.urlConvenioFirmado} target="_blank" rel="noopener noreferrer" >
                                                                                                <i className="fas fa-eye"
                                                                                                    style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                                            </a>
                                                                                        </button> : <a href="#">
                                                                                            <i className="fas fa-eye"
                                                                                                style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Documento no existe" ></i>
                                                                                        </a>
                                                                                    }
                                                                                </div>
                                                                                <div className="col-xs-6 col-lg-6">
                                                                                    <button class="btn " type="button" disabled={true} style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Generar documento</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div></>
                        }
                        {((this.props.presentarSeccion == "EP12" || this.props.presentarSeccion == "EP13" || this.props.presentarSeccion == "EP14") && this.state.estatus_seccion >= 11) &&
                            <><div>
                                {/* Venta de Carrera */}
                                <div class="accordion" id="venta_carrera">
                                    <div id="headingventa_carrera">
                                        <h2 class="mb-0">
                                            <button class="btn " type="button" data-toggle="collapse" data-target="#collapsventa_carrera" aria-expanded="true" aria-controls="collapsventa_carrera">
                                                <h6 class="mb-0 font-weight-semibold">Venta de Carrera </h6>
                                            </button>
                                        </h2>
                                    </div>
                                    <div class="card">
                                        <div id="collapsventa_carrera" class="collapse " aria-labelledby="headingventa_carrera" data-parent="#venta_carrera">
                                            <div class="card">
                                                <div className="card-body">
                                                    {(this.state.muestra_encuentro >= 1 && (this.props.presentarSeccion == "EP12" || this.props.presentarSeccion == "EP13" || this.props.presentarSeccion == "EP14")) &&
                                                        <div>
                                                            <div className="row" id="inicioGDD">
                                                                <div className="col-xs-10 col-lg-10">
                                                                    <h7 class="mb-0 font-weight-semibold">GDD</h7>
                                                                </div>
                                                            </div>
                                                            <div class="card">
                                                                <div className="card-body">
                                                                    <div className="row">
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="form-group">
                                                                                <div className="input-group">
                                                                                    <span className="input-group-prepend"> <span
                                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fecha de cita</span>
                                                                                    </span> <input type="date" placeholder="" onChange={this.onChange} disabled={true} value={this.state.prospecto_ventaCarrera.fecha_cita} name="fecha_cita" className="form-control " />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="form-group">
                                                                                <div className="input-group">
                                                                                    <span className="input-group-prepend"> <span
                                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>GDD</span>
                                                                                    </span> {this.state.selectGDDVC}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="row">
                                                                                {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>} <label >Generación de la figura comercial</label>
                                                                            </div>
                                                                            <div className="row">
                                                                                {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                                <div class="form-check">
                                                                                    <input class="form-check-input" type="checkbox" value={1} onChange={this.onChange} id="G1" name="generacion" checked={this.state.prospecto_ventaCarrera.generacion == 1} disabled={true} />
                                                                                    <label class="form-check-label" for="G1">
                                                                                        G1
                                                                                    </label>
                                                                                </div>
                                                                                {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                                <div class="form-check">
                                                                                    <input class="form-check-input" type="checkbox" value={2} onChange={this.onChange} id="G2" name="generacion" checked={this.state.prospecto_ventaCarrera.generacion == 2} disabled={true} />
                                                                                    <label class="form-check-label" for="G2">
                                                                                        G2
                                                                                    </label>
                                                                                </div>

                                                                                {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                                <div class="form-check">
                                                                                    <input class="form-check-input" type="checkbox" value={3} onChange={this.onChange} id="G3" name="generacion" checked={this.state.prospecto_ventaCarrera.generacion == 3} disabled={true} />
                                                                                    <label class="form-check-label" for="G3">
                                                                                        G3
                                                                                    </label>
                                                                                </div>
                                                                                {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                                <div class="form-check">
                                                                                    <input class="form-check-input" type="checkbox" value={4} onChange={this.onChange} id="G4" name="generacion" checked={this.state.prospecto_ventaCarrera.generacion == 4} disabled={true} />
                                                                                    <label class="form-check-label" for="G4">
                                                                                        G4
                                                                                    </label>
                                                                                </div>
                                                                                {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                                <div class="form-check">
                                                                                    <input class="form-check-input" type="checkbox" value={5} onChange={this.onChange} id="C" name="generacion" checked={this.state.prospecto_ventaCarrera.generacion == 5} disabled={true} />
                                                                                    <label class="form-check-label" for="C">
                                                                                        C
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <div className="row">
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="row">
                                                                                <div className="col-xs-2 col-lg-4">
                                                                                    <h8 class="mb-0 font-weight-semibold">Curso de Inmersión APS</h8>
                                                                                </div>
                                                                                <div className="col-xs-2 col-lg-4">
                                                                                    <div className="custom-control custom-switch  mb-2" >
                                                                                        <input type="checkbox" class="custom-control-input" id="curso_inmersion" onChange={this.onChange} disabled={true} checked={this.state.prospecto_ventaCarrera.curso_inmersion} name="curso_inmersion"></input>
                                                                                        <label class="custom-control-label" for="curso_inmersion"></label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div className="col-xs-8 col-lg-8">
                                                                            <div className="form-group">
                                                                                <div className="input-group">
                                                                                    <span className="input-group-prepend"> <span
                                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Si es el caso, motivo por el cual no ingresa a Curso de Inmersión APS</span>
                                                                                    </span> <input type="text" placeholder="" onChange={this.onChange} disabled={this.state.prospecto_ventaCarrera.curso_inmersion} value={this.state.prospecto_ventaCarrera.motivo_curso_inmersion} name="motivo_curso_inmersion" className="form-control " />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div className="row">
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="form-group">
                                                                                <div className="input-group">
                                                                                    <span className="input-group-prepend"> <span
                                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Ingreso curso de inmersion</span>
                                                                                    </span> <input type="date" placeholder="" onChange={this.onChange} disabled={true} value={this.state.prospecto_ventaCarrera.ingreso_curso_inmersion} name="ingreso_curso_inmersion" className="form-control " />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-xs-2 col-lg-2">
                                                                            <a href="#"><ScrollTo selector={this.state.clickScroll ? '#accordionExample' : '#inicioGDD'} onClick={this.onChangeFocus} style={
                                                                                (this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "EDAT") && (this.state.prospecto.orientacion_logro_gdd == 0 || this.state.prospecto.orientacion_logro_gdd == null) ||
                                                                                    (this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "EDAT") && (this.state.prospecto.perceverancia_gdd == 0 || this.state.prospecto.perceverancia_gdd == null) ||
                                                                                    (this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "EDAT") && (this.state.prospecto.integridad_gdd == 0 || this.state.prospecto.integridad_gdd == null) ||
                                                                                    (this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "EDAT") && (this.state.prospecto.sentido_comun_gdd == 0 || this.state.prospecto.sentido_comun_gdd == null) ||
                                                                                    (this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "EDAT") && (this.state.prospecto.energia_gdd == 0 || this.state.prospecto.energia_gdd == null) ||
                                                                                    (this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "EDAT") && (this.state.prospecto.motivacion_gdd == 0 || this.state.prospecto.motivacion_gdd == null) ?
                                                                                    { color: "red" } : { color: "#2196f3" }} ><h6>Registro de factores vitales</h6></ScrollTo></a>
                                                                        </div>
                                                                        <div className="col-xs-2 col-lg-2">
                                                                            <a href="#"><ScrollTo selector={this.state.clickScroll ? '#accordionExample' : '#inicioGDD'} onClick={this.onChangeFacImport} style={{ color: "#2196f3" }} >
                                                                                <h6>Registro de factores importantes</h6></ScrollTo></a>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>}
                                                    {(this.state.muestra_encuentro >= 2 && (this.props.presentarSeccion == "EP12" || this.props.presentarSeccion == "EP13" || this.props.presentarSeccion == "EP14")) &&
                                                        <div>
                                                            <div className="row">
                                                                <div className="col-xs-2 col-lg-4">
                                                                    <h7 class="mb-0 font-weight-semibold">EDAT</h7>
                                                                </div>
                                                            </div>
                                                            <div class="card">
                                                                <div className="card-body">
                                                                    <div className="row">

                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="row">
                                                                                <div className="col-xs-2 col-lg-4">
                                                                                    <h8 class="mb-0 font-weight-semibold">Participación del EDAT</h8>
                                                                                </div>
                                                                                <div className="col-xs-2 col-lg-4">
                                                                                    <div className="custom-control custom-switch  mb-2" >
                                                                                        <input type="checkbox" class="custom-control-input" id="participacion_edat" onChange={this.onChange} name="participacion_edat" disabled={this.props.tipoUsuario != "EDAT"} checked={this.state.prospecto_ventaCarrera.participacion_edat}></input>
                                                                                        <label class="custom-control-label" for="participacion_edat"></label>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                        <div className="col-xs-8 col-lg-8">
                                                                            <div className="form-group">
                                                                                <div className="input-group">
                                                                                    <span className="input-group-prepend"> <span
                                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Descripción breve</span>
                                                                                    </span> <input type="text" placeholder="" onChange={this.onChange} disabled={this.props.tipoUsuario != "EDAT"} value={this.state.prospecto_ventaCarrera.descripcion_edat} name="descripcion_edat" className="form-control " />
                                                                                </div>
                                                                                {
                                                                                    this.props.tipoUsuario == "EDAT" && (this.state.prospecto_ventaCarrera.descripcion_edat == "" || this.state.prospecto_ventaCarrera.descripcion_edat == null) ?
                                                                                        <span style={{ color: "red" }}>Descripción debe ser un campo requerido</span> : ''
                                                                                }
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div className="row">
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="row">
                                                                                <div className="col-xs-2 col-lg-4">
                                                                                    <h8 class="mb-0 font-weight-semibold">Correo APS</h8>
                                                                                </div>
                                                                                <div className="col-xs-2 col-lg-4">
                                                                                    <div className="custom-control custom-switch  mb-2" >
                                                                                        <input type="checkbox" class="custom-control-input" id="correo_aps" disabled={this.props.tipoUsuario != "EDAT"} onChange={this.onChange} name="correo_aps" checked={this.state.prospecto_ventaCarrera.correo_aps}></input>
                                                                                        <label class="custom-control-label" for="correo_aps"></label>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="row">
                                                                                <div className="col-xs-2 col-lg-4">
                                                                                    <h8 class="mb-0 font-weight-semibold">Curso de Cédula A</h8>
                                                                                </div>
                                                                                <div className="col-xs-2 col-lg-4">
                                                                                    <div className="custom-control custom-switch  mb-2" >
                                                                                        <input type="checkbox" class="custom-control-input" id="curso_cedula" disabled={this.props.tipoUsuario != "EDAT"} onChange={this.onChange} name="curso_cedula" checked={this.state.prospecto_ventaCarrera.curso_cedula}></input>
                                                                                        <label class="custom-control-label" for="curso_cedula"></label>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                        <div className="col-xs-4 col-lg-4">
                                                                            <div className="row">
                                                                                <div className="col-xs-12 col-lg-12">
                                                                                    <div className="form-group">
                                                                                        <div className="input-group">
                                                                                            <span className="input-group-prepend"> <span
                                                                                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Selecciona un SAS</span>
                                                                                            </span> {this.state.selectSAS}
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div className="row">
                                                                        <div className="col-xs-6 col-lg-6">
                                                                            <div className="form-group">
                                                                                <div className="input-group">
                                                                                    <span className="input-group-prepend"> <span
                                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Tipo de conexión</span>
                                                                                    </span> {this.state.selectTipoConexion}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-xs-6 col-lg-6">
                                                                            <div className="row">
                                                                                <div className="col-xs-3 col-lg-3">
                                                                                    <h8 class="mb-0 font-weight-semibold">Creación de Grupo explorando</h8>
                                                                                </div>
                                                                                <div className="col-xs-4 col-lg-4">
                                                                                    <div className="row">
                                                                                        {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                                        <div class="form-check">
                                                                                            <input class="form-check-input" disabled={this.props.tipoUsuario != "EDAT"} type="checkbox" id="grupo_explorando" name="grupo_explorando" onChange={this.onChange} value={1} checked={this.state.prospecto_ventaCarrera.grupo_explorando == 1} />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            {
                                                                                this.props.tipoUsuario == "EDAT" && (this.state.prospecto_ventaCarrera.grupo_explorando == 0 || this.state.prospecto_ventaCarrera.grupo_explorando == null) ?
                                                                                    <span className="" style={{ color: "red" }}>El campo Creación de Grupo debe ser seleccionado </span> : ''
                                                                            }
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div></>
                        }
                        {((this.props.presentarSeccion == "EP11" || this.props.presentarSeccion == "EP12" || this.props.presentarSeccion == "EP13" || this.props.presentarSeccion == "EP14") && this.state.estatus_seccion >= 11) &&
                            <><div>
                                {/* conoce a tu candidato */}
                                <div class="accordion" id="conoce_candidato">
                                    <div id="headingconoce_candidato">
                                        <h2 class="mb-0">
                                            <button class="btn " type="button" data-toggle="collapse" data-target="#collapsconoce_candidato" aria-expanded="true" aria-controls="collapsconoce_candidato">
                                                <h6 class="mb-0 font-weight-semibold">Conoce a tu candidato </h6>
                                            </button>
                                        </h2>
                                    </div>
                                    <div class="card">
                                        <div id="collapsconoce_candidato" class="collapse " aria-labelledby="headingconoce_candidato" data-parent="#conoce_candidato">
                                            <div class="card">
                                                <div className="card-body">
                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-12">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>¿Es alguien a quien invitarías a una comida el fin de semana a tu casa?</span>
                                                                    </span>
                                                                    <div className="row">
                                                                        {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                        <div class="form-check">
                                                                            <input class="form-check-input" type="radio" value={1} id="invitar_comida1" disabled={true} onChange={this.onChange} name="invitar_comida" checked={this.state.prospectoCandidato.invitar_comida == 1} />
                                                                            <label class="form-check-label" for="invitar_comida1">
                                                                                Si
                                                                            </label>
                                                                        </div>
                                                                        {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                        <div class="form-check">
                                                                            <input class="form-check-input" type="radio" value={0} id="invitar_comida0" disabled={true} onChange={this.onChange} name="invitar_comida" checked={this.state.prospectoCandidato.invitar_comida == 0} />
                                                                            <label class="form-check-label" for="invitar_comida0">
                                                                                No
                                                                            </label>
                                                                        </div>
                                                                        {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xs-12 col-lg-12">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Por que?</span>
                                                                    </span> <input type="text" placeholder="" onChange={this.onChange} value={this.state.prospectoCandidato.invitar_comida_prque} disabled={true} name="invitar_comida_prque" className="form-control " />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xs-12 col-lg-12">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>¿Que consideras que aportará a tu equipo y que el equipo a el/ella?</span>
                                                                    </span> <input type="text" placeholder="" onChange={this.onChange} value={this.state.prospectoCandidato.aportar_equipo} disabled={true} name="aportar_equipo" className="form-control " />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xs-12 col-lg-12">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>¿Describe los rasgos más importantes de su mercado?</span>
                                                                    </span> <input type="text" placeholder="" onChange={this.onChange} value={this.state.prospectoCandidato.rasgos_importantes} disabled={true} name="rasgos_importantes" className="form-control " />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xs-12 col-lg-12">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Define el tiempo que dedicará a esta carrera - con precisión - número de horas al día, semana - año</span>
                                                                    </span> <input type="text" placeholder="" onChange={this.onChange} value={this.state.prospectoCandidato.tiempo_carrera} disabled={true} name="tiempo_carrera" className="form-control " />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xs-12 col-lg-12">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Necesidades financieras personales y familiares. (Poner Montos)</span>
                                                                    </span> <input type="text" placeholder="" onChange={this.onChange} value={this.state.prospectoCandidato.necesidades_financieras} disabled={true} name="necesidades_financieras" className="form-control " />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xs-12 col-lg-12">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Que te dicen sus redes sociales. (Especificar cuales tiene)</span>
                                                                    </span> <input type="text" placeholder="" onChange={this.onChange} value={this.state.prospectoCandidato.redes_sociales} disabled={true} name="redes_sociales" className="form-control " />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xs-12 col-lg-12">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>¿Cuenta con algún colchón económico? (Preferencia poner cuanto aprox)</span>
                                                                    </span> <input type="text" placeholder="" onChange={this.onChange} value={this.state.prospectoCandidato.colchon_economico} disabled={true} name="colchon_economico" className="form-control " />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xs-12 col-lg-12">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Es alguien a quien entregarías la responsabilidad tu patrimonio/dinero /retiro?</span>
                                                                    </span>
                                                                    <div className="row">
                                                                        {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                        <div class="form-check">
                                                                            <input class="form-check-input" type="radio" value={1} onChange={this.onChange} disabled={true} id="resp_patrimonio1" name="resp_patrimonio" checked={this.state.prospectoCandidato.resp_patrimonio == 1} />
                                                                            <label class="form-check-label" for="resp_patrimonio1">
                                                                                Si
                                                                            </label>
                                                                        </div>
                                                                        {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                        <div class="form-check">
                                                                            <input class="form-check-input" type="radio" value={0} onChange={this.onChange} disabled={true} id="resp_patrimonio0" name="resp_patrimonio" checked={this.state.prospectoCandidato.resp_patrimonio == 0} />
                                                                            <label class="form-check-label" for="resp_patrimonio0">
                                                                                No
                                                                            </label>
                                                                        </div>
                                                                        {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xs-12 col-lg-12">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Por que?</span>
                                                                    </span> <input type="text" placeholder="" onChange={this.onChange} value={this.state.prospectoCandidato.resp_patrimonio_porque} disabled={true} name="resp_patrimonio_porque" className="form-control " />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xs-12 col-lg-12">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Cuál consideras que es su debilidad y que deberías trabajar de más cerca con el candidato?</span>
                                                                    </span> <input type="text" placeholder="" onChange={this.onChange} value={this.state.prospectoCandidato.debilidad} disabled={true} name="debilidad" className="form-control " />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div></>
                        }
                        {((this.props.presentarSeccion == "EP10" || this.props.presentarSeccion == "EP11" || this.props.presentarSeccion == "EP12"
                            || this.props.presentarSeccion == "EP13" || this.props.presentarSeccion == "EP14") && this.state.estatus_seccion >= 10) &&
                            <><div>
                                {/* factores knockout */}
                                <div class="accordion" id="factores_knockout">
                                    <div id="headingKnockout">
                                        <h2 class="mb-0">
                                            <button class="btn " type="button" data-toggle="collapse" data-target="#collapsKnockout" aria-expanded="true" aria-controls="collapsKnockout">
                                                <h6 class="mb-0 font-weight-semibold">10. Factores knockout </h6>
                                            </button>
                                        </h2>
                                    </div>
                                    <div class="card">
                                        <div id="collapsKnockout" class="collapse " aria-labelledby="headingKnockout" data-parent="#factores_knockout">
                                            <div class="card">
                                                <div className="card-body">
                                                    <div className="row">
                                                        <div className="col-xs-6 col-lg-6">
                                                            <div class="card">
                                                                <div className="card-body">
                                                                    <div className="row">
                                                                        <div className="col-xs-2 col-lg-11">
                                                                            <h8 class="mb-0 font-weight-semibold">Falta de patrón de éxito</h8>
                                                                        </div>
                                                                        <div className="col-xs-2 col-lg-1">
                                                                            <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="falta_patron" disabled={true} onChange={this.onChange} checked={this.state.prospecto_knockout.falta_patron} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div className="col-xs-6 col-lg-6">
                                                            <div class="card">
                                                                <div className="card-body">
                                                                    <div className="row">
                                                                        <div className="col-xs-2 col-lg-11">
                                                                            <h8 class="mb-0 font-weight-semibold">Incapacidad para aceptar capacitación</h8>
                                                                        </div>
                                                                        <div className="col-xs-2 col-lg-1">
                                                                            <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="incapacidad_aceptar" disabled={true} onChange={this.onChange} checked={this.state.prospecto_knockout.incapacidad_aceptar} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-xs-6 col-lg-6">
                                                            <div class="card">
                                                                <div className="card-body">
                                                                    <div className="row">
                                                                        <div className="col-xs-2 col-lg-11">
                                                                            <h8 class="mb-0 font-weight-semibold">Bajo nivel de energía</h8>
                                                                        </div>
                                                                        <div className="col-xs-2 col-lg-1">
                                                                            <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="bajo_nivel" disabled={true} onChange={this.onChange} checked={this.state.prospecto_knockout.bajo_nivel} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="col-xs-6 col-lg-6">
                                                            <div class="card">
                                                                <div className="card-body">
                                                                    <div className="row">
                                                                        <div className="col-xs-2 col-lg-11">
                                                                            <h8 class="mb-0 font-weight-semibold">Recientemente divorciado</h8>
                                                                        </div>
                                                                        <div className="col-xs-2 col-lg-1">
                                                                            <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="recientemente_divorciado" disabled={true} onChange={this.onChange} checked={this.state.prospecto_knockout.recientemente_divorciado} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-xs-6 col-lg-6">
                                                            <div class="card">
                                                                <div className="card-body">
                                                                    <div className="row">
                                                                        <div className="col-xs-2 col-lg-11">
                                                                            <h8 class="mb-0 font-weight-semibold">Malos hábitos de vida</h8>
                                                                        </div>
                                                                        <div className="col-xs-2 col-lg-1">
                                                                            <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="malos_habitos" disabled={true} onChange={this.onChange} checked={this.state.prospecto_knockout.malos_habitos} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="col-xs-6 col-lg-6">
                                                            <div class="card">
                                                                <div className="card-body">
                                                                    <div className="row">
                                                                        <div className="col-xs-2 col-lg-11">
                                                                            <h8 class="mb-0 font-weight-semibold">Demasiados cambios de trabajo / inestabilidad</h8>
                                                                        </div>
                                                                        <div className="col-xs-2 col-lg-1">
                                                                            <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="demasiados_cambios" disabled={true} onChange={this.onChange} checked={this.state.prospecto_knockout.demasiados_cambios} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-xs-6 col-lg-6">
                                                            <div class="card">
                                                                <div className="card-body">
                                                                    <div className="row">
                                                                        <div className="col-xs-2 col-lg-11">
                                                                            <h8 class="mb-0 font-weight-semibold">Mercado natural débil-falta de contactos</h8>
                                                                        </div>
                                                                        <div className="col-xs-2 col-lg-1">
                                                                            <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="mercado_natural" disabled={true} onChange={this.onChange} checked={this.state.prospecto_knockout.mercado_natural} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="col-xs-6 col-lg-6">
                                                            <div class="card">
                                                                <div className="card-body">
                                                                    <div className="row">
                                                                        <div className="col-xs-2 col-lg-11">
                                                                            <h8 class="mb-0 font-weight-semibold">Benefactor profesional</h8>
                                                                        </div>
                                                                        <div className="col-xs-2 col-lg-1">
                                                                            <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="benefactor_profecional" disabled={true} onChange={this.onChange} checked={this.state.prospecto_knockout.benefactor_profecional} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-xs-6 col-lg-6">
                                                            <div class="card">
                                                                <div className="card-body">
                                                                    <div className="row">
                                                                        <div className="col-xs-2 col-lg-11">
                                                                            <h8 class="mb-0 font-weight-semibold">Falta de movilidad social</h8>
                                                                        </div>
                                                                        <div className="col-xs-2 col-lg-1">
                                                                            <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="falta_movilidad" disabled={true} onChange={this.onChange} checked={this.state.prospecto_knockout.falta_movilidad} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="col-xs-6 col-lg-6">
                                                            <div class="card">
                                                                <div className="card-body">
                                                                    <div className="row">
                                                                        <div className="col-xs-2 col-lg-11">
                                                                            <h8 class="mb-0 font-weight-semibold">Problemas de salud</h8>
                                                                        </div>
                                                                        <div className="col-xs-2 col-lg-1">
                                                                            <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="problemas_salud" disabled={true} onChange={this.onChange} checked={this.state.prospecto_knockout.problemas_salud} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div className="row">
                                                        <div className="col-xs-6 col-lg-6">
                                                            <div class="card">
                                                                <div className="card-body">
                                                                    <div className="row">
                                                                        <div className="col-xs-2 col-lg-11">
                                                                            <h8 class="mb-0 font-weight-semibold">Problemas financieros serios / Problemas crediticios / Bancarrota reciente</h8>
                                                                        </div>
                                                                        <div className="col-xs-2 col-lg-1">
                                                                            <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="problemas_financieros" disabled={true} onChange={this.onChange} checked={this.state.prospecto_knockout.problemas_financieros} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="col-xs-6 col-lg-6">
                                                            <div class="card">
                                                                <div className="card-body">
                                                                    <div className="row">
                                                                        <div className="col-xs-2 col-lg-11">
                                                                            <h8 class="mb-0 font-weight-semibold">Ve las ventas como un retroceso en lugar de una oportunidad</h8>
                                                                        </div>
                                                                        <div className="col-xs-2 col-lg-1">
                                                                            <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="ventas_retroceso" disabled={true} onChange={this.onChange} checked={this.state.prospecto_knockout.ventas_retroceso} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-xs-6 col-lg-6">
                                                            <div class="card">
                                                                <div className="card-body">
                                                                    <div className="row">
                                                                        <div className="col-xs-2 col-lg-11">
                                                                            <h8 class="mb-0 font-weight-semibold">Culpa a los demás por su falta de éxito</h8>
                                                                        </div>
                                                                        <div className="col-xs-2 col-lg-1">
                                                                            <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="culpa_demas" disabled={true} onChange={this.onChange} checked={this.state.prospecto_knockout.culpa_demas} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div className="col-xs-6 col-lg-6">
                                                            <div class="card">
                                                                <div className="card-body">
                                                                    <div className="row">
                                                                        <div className="col-xs-2 col-lg-11">
                                                                            <h8 class="mb-0 font-weight-semibold">Necesita cualquier trabajo inmediata y desesperadamente</h8>
                                                                        </div>
                                                                        <div className="col-xs-2 col-lg-1">
                                                                            <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="cualquier_trabajo" disabled={true} onChange={this.onChange} checked={this.state.prospecto_knockout.cualquier_trabajo} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-xs-6 col-lg-6">
                                                            <div class="card">
                                                                <div className="card-body">
                                                                    <div className="row">
                                                                        <div className="col-xs-2 col-lg-11">
                                                                            <h8 class="mb-0 font-weight-semibold">No cree en el seguro de vida</h8>
                                                                        </div>
                                                                        <div className="col-xs-2 col-lg-1">
                                                                            <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="seguros_vida" disabled={true} onChange={this.onChange} checked={this.state.prospecto_knockout.seguros_vida} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="col-xs-6 col-lg-6">
                                                            <div class="card">
                                                                <div className="card-body">
                                                                    <div className="row">
                                                                        <div className="col-xs-2 col-lg-11">
                                                                            <h8 class="mb-0 font-weight-semibold">Satisfecho con el status Quo</h8>
                                                                        </div>
                                                                        <div className="col-xs-2 col-lg-1">
                                                                            <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="checkbox" name="status_quo" disabled={true} onChange={this.onChange} checked={this.state.prospecto_knockout.status_quo} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-12">
                                                            <h5 class="mb-0 font-weight-semibold">Resultado de la entrevista</h5>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-12">
                                                            <div className="row">
                                                                <div className="col-xs-2 col-lg-2">
                                                                    <div class="card">
                                                                        <div class="form-check form-check-inline" style={{ paddingLeft: '8px' }}>
                                                                            <input class="form-check-input" type="radio" name="resultado_entrevista" id="descarta" value={0} disabled={true} onChange={this.onChange} checked={this.state.prospecto_knockout.resultado_entrevista == 0} />
                                                                            <h6 class="form-check-label mb-0 font-weight-semibold" for="descarta">Se descarta</h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="col-xs-2 col-lg-2">
                                                                    <div class="card">
                                                                        <div class="form-check form-check-inline" style={{ paddingLeft: '8px' }}>
                                                                            <input class="form-check-input" type="radio" name="resultado_entrevista" id="esperaPSP" disabled={true} value={1} onChange={this.onChange} checked={this.state.prospecto_knockout.resultado_entrevista == 1} />
                                                                            <h6 class="form-check-label mb-0 font-weight-semibold" for="esperaPSP">En espera de PSP</h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="col-xs-2 col-lg-2">
                                                                    <div class="card">
                                                                        <div class="form-check form-check-inline" style={{ paddingLeft: '8px' }}>
                                                                            <input class="form-check-input" type="radio" name="resultado_entrevista" id="rechazo" disabled={true} value={2} onChange={this.onChange} checked={this.state.prospecto_knockout.resultado_entrevista == 2} />
                                                                            <h6 class="form-check-label mb-0 font-weight-semibold" for="rechazo">Rechazó</h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="col-xs-2 col-lg-2">
                                                                    <div class="card">
                                                                        <div class="form-check form-check-inline" style={{ paddingLeft: '8px' }}>
                                                                            <input class="form-check-input" type="radio" name="resultado_entrevista" id="avanza" disabled={true} value={3} onChange={this.onChange} checked={this.state.prospecto_knockout.resultado_entrevista == 3} />
                                                                            <h6 class="form-check-label mb-0 font-weight-semibold" for="avanza">Avanza</h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="col-xs-2 col-lg-2">
                                                                    <div class="card">
                                                                        <div class="form-check form-check-inline" style={{ paddingLeft: '8px' }}>
                                                                            <input class="form-check-input" type="radio" name="resultado_entrevista" id="reserva" disabled={true} value={4} onChange={this.onChange} checked={this.state.prospecto_knockout.resultado_entrevista == 4} />
                                                                            <h6 class="form-check-label mb-0 font-weight-semibold" for="reserva">Enviar a reserva</h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {
                                                        this.state.prospecto_knockout.resultado_entrevista == 2 &&
                                                        <div className="row">
                                                            <div className="col-xs-12 col-lg-12">
                                                                <div className="form-group">
                                                                    <span >
                                                                        <span className="font-weight-semibold">Comentarios de rechazo</span>
                                                                    </span>
                                                                    <div className="input-group">
                                                                        <textarea type="text" placeholder="Escribir" name="causas_rechazo" onChange={this.onChange} disabled={this.props.tipoUsuario != "EDATJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                                            && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.causas_rechazo} className="form-control " style={{ borderColor: '#C8CDF6', backgroundColor: '#FFFF' }} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    }

                                                    {
                                                        this.state.prospecto_knockout.resultado_entrevista == 3 &&
                                                        <div className="row">
                                                            <div className="col-xs-12 col-lg-12">
                                                                <div className="form-group">
                                                                    <span >
                                                                        <span className="font-weight-semibold">Selecciona un GDD</span>
                                                                    </span>
                                                                    <div className="input-group">
                                                                        {this.state.selectGDD}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    }
                                                    {
                                                        this.state.prospecto_knockout.resultado_entrevista == 3 &&
                                                        <div className="row">
                                                            <div className="col-xs-12 col-lg-12">
                                                                <div className="form-group">
                                                                    <span >
                                                                        <span className="font-weight-semibold">Selecciona un SDDC</span>
                                                                    </span>
                                                                    <div className="input-group">
                                                                        {this.state.selectSDDC}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    }
                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-12">
                                                            <button data-toggle="modal" data-target="#modal-psp"
                                                                disabled={this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} class="btn  mr-1" style={{ backgroundColor: "#78CB5A", color: '#FFFF' }}> <>&nbsp;&nbsp;</> Ver archivo PSP*<>&nbsp;&nbsp;</> </button>
                                                        </div>
                                                    </div>
                                                    <div id="modal-psp" className="modal fade" tabindex="-1">
                                                        <div className="modal-dialog modal-full">
                                                            <div className="modal-content text-white" style={{ backgroundColor: "#313A46" }}>
                                                                <div className="modal-header text-white" style={{ backgroundColor: "#8189D4" }}>
                                                                    <h6 className="modal-title">Documento PSP </h6>
                                                                    <button type="button" className="close" data-dismiss="modal">&times;</button>
                                                                </div>
                                                                <div className="modal-body">
                                                                    <div className="card-body">
                                                                        <div className="row">
                                                                            <div className="col-xs-12 col-lg-4"><>&nbsp;&nbsp;</></div>
                                                                            <div className="col-xs-12 col-lg-4">
                                                                                <div className="input-group">
                                                                                    <form encType="multipart/form" style={{ display: 'none' }} >
                                                                                        <input type="file" style={{ display: 'none' }} ref={(ref) => this.upload = ref} /*onChange={this.onChangeFilePSP.bind(this)}*/></input>
                                                                                    </form>
                                                                                    <input type="text" style={{ borderColor: 'black', backgroundColor: "#313A46", color: 'white' }} className="form-control " placeholder="Archivo PSP" value={this.state.nombreArchivoPSP} />
                                                                                    <span className="input-group-prepend" style={{ backgroundColor: this.state.colorBotonSubirArchivoPSP }}>
                                                                                        {
                                                                                            this.state.muestraDocPSP == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoPSP }}>
                                                                                                <a href={this.state.rutaPSP} target="_blank" rel="noopener noreferrer" >
                                                                                                    <i className="fas fa-eye"
                                                                                                        style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                                                </a>
                                                                                            </button> : <button type="button" class="btn text-white" onClick={this.onClickBotonArchivoPSP} style={{ backgroundColor: this.state.colorBotonSubirArchivoPSP }}>
                                                                                                <h10 style={{ color: "white" }}>+</h10>
                                                                                            </button>

                                                                                        }

                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                            <div className="col-xs-12 col-lg-4"><>&nbsp;&nbsp;</></div>
                                                                        </div>

                                                                        <br></br>
                                                                        <br></br>
                                                                        {this.state.muestraInfoPSP == true &&
                                                                            <div>
                                                                                <div className="row">
                                                                                    <div className="col-xs-6 col-lg-6">
                                                                                        <h8 class="mb-0 font-weight-semibold text-white"> Resultados PSP</h8>
                                                                                    </div>
                                                                                    <div className="col-xs-4 col-lg-4">
                                                                                        <table className="table datatable-sorting  table-striped table-hover" style={{ backgroundColor: "#313A46", borderColor: '#313A46' }}>
                                                                                            <thead style={{ backgroundColor: "#313A46" }}>
                                                                                                <tr>
                                                                                                    <th className="text-left font-weight-bold text-white" style={{ backgroundColor: '#313A46' }} >
                                                                                                        <a>ALTO</a>
                                                                                                    </th>
                                                                                                    <th className="text-center font-weight-bold text-white" style={{ backgroundColor: '#313A46' }} >
                                                                                                        <a>MODERADO</a>
                                                                                                    </th>
                                                                                                    <th className="text-right font-weight-bold text-white" style={{ backgroundColor: '#313A46' }} >
                                                                                                        <a>BAJO</a>
                                                                                                    </th>
                                                                                                </tr>
                                                                                            </thead>

                                                                                        </table>
                                                                                    </div>
                                                                                </div>

                                                                                <div className="row">
                                                                                    <div className="col-xs-4 col-lg-4">
                                                                                        <div className="form-group">
                                                                                            <div className="input-group">
                                                                                                <span className="input-group-prepend"> <span
                                                                                                    className="input-group-text text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }}>Fecha de respuesta del PSP</span>
                                                                                                </span> <input type="text" placeholder="" value={this.state.fechaRespPSP} className="form-control text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }} />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="col-xs-2 col-lg-2">
                                                                                        <h8 class=" text-white"> Eficacia en ventas</h8>
                                                                                    </div>
                                                                                    <div className="col-xs-4 col-lg-4">
                                                                                        <input type="range" disabled={true} class="custom-range" min="0" name="salud" max="100" step={50} value={this.state.eficaciaVentas} id="salud" />
                                                                                    </div>
                                                                                    <div className="col-xs-2 col-lg-2">
                                                                                        {
                                                                                            this.state.eficaciaVentas == 0 &&
                                                                                            <div class="mr-3 ">
                                                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                                                        class="letter-icon text-white"></span>
                                                                                                </a>
                                                                                            </div>
                                                                                        }
                                                                                        {
                                                                                            this.state.eficaciaVentas == 50 &&
                                                                                            <div className="row">
                                                                                                <div class="mr-3 ">
                                                                                                    <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                                                        class="btn rounded-pill btn-icon btn-sm "><span
                                                                                                            class="letter-icon text-white"></span>
                                                                                                    </a>
                                                                                                </div>
                                                                                            </div>
                                                                                        }
                                                                                        {
                                                                                            this.state.eficaciaVentas == 100 &&
                                                                                            <div className="row">
                                                                                                <div class="mr-3 ">
                                                                                                    <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FD0303', borderColor: '#FD0303' }}
                                                                                                        class="btn rounded-pill btn-icon btn-sm "><span
                                                                                                            class="letter-icon text-white"></span>
                                                                                                    </a>
                                                                                                </div>
                                                                                            </div>
                                                                                        }
                                                                                    </div>
                                                                                </div>
                                                                                <div className="row">
                                                                                    <div className="col-xs-12 col-lg-4">
                                                                                        <div className="form-group">
                                                                                            <div className="input-group">
                                                                                                <span className="input-group-prepend"> <span
                                                                                                    className="input-group-text text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }}>Estilo de venta dominante&ensp;</span>
                                                                                                </span> <input type="text" disabled={true} placeholder="" value={this.state.ventaDominante} className="form-control text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }} />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="col-xs-12 col-lg-2">
                                                                                        <h8 class=" text-white"> Eficacia empresarial</h8>
                                                                                    </div>
                                                                                    <div className="col-xs-12 col-lg-4">
                                                                                        <input type="range" disabled={true} class="custom-range" min="0" name="salud" max="100" step={50} value={this.state.eficaciaEmpresarial} id="salud" />
                                                                                    </div>
                                                                                    <div className="col-xs-2 col-lg-2">
                                                                                        {
                                                                                            this.state.eficaciaEmpresarial == 0 &&
                                                                                            <div class="mr-3 ">
                                                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                                                        class="letter-icon text-white"></span>
                                                                                                </a>
                                                                                            </div>
                                                                                        }
                                                                                        {
                                                                                            this.state.eficaciaEmpresarial == 50 &&
                                                                                            <div className="row">
                                                                                                <div class="mr-3 ">
                                                                                                    <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                                                        class="btn rounded-pill btn-icon btn-sm "><span
                                                                                                            class="letter-icon text-white"></span>
                                                                                                    </a>
                                                                                                </div>
                                                                                            </div>
                                                                                        }
                                                                                        {
                                                                                            this.state.eficaciaEmpresarial == 100 &&
                                                                                            <div className="row">
                                                                                                <div class="mr-3 ">
                                                                                                    <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FD0303', borderColor: '#FD0303' }}
                                                                                                        class="btn rounded-pill btn-icon btn-sm "><span
                                                                                                            class="letter-icon text-white"></span>
                                                                                                    </a>
                                                                                                </div>
                                                                                            </div>
                                                                                        }
                                                                                    </div>
                                                                                </div>
                                                                                <div className="row">
                                                                                    <div className="col-xs-12 col-lg-4">
                                                                                        <div className="form-group">
                                                                                            <div className="input-group">
                                                                                                <span className="input-group-prepend"> <span
                                                                                                    className="input-group-text text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }}>Interpretación de precisión</span>
                                                                                                </span> <textarea disabled={true} type="text" placeholder="" value={this.state.interpretacion} className="form-control text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }} />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="col-xs-12 col-lg-2">
                                                                                        <h8 class=" text-white">Rendimiento de ventas general esperado</h8>
                                                                                    </div>
                                                                                    <div className="col-xs-12 col-lg-4">
                                                                                        <input type="range" disabled={true} class="custom-range" min="0" name="salud" max="100" step={50} value={this.state.rendimientoVentas} id="salud" />
                                                                                    </div>
                                                                                    <div className="col-xs-2 col-lg-2">
                                                                                        {
                                                                                            this.state.rendimientoVentas == 0 &&
                                                                                            <div class="mr-3 ">
                                                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                                                        class="letter-icon text-white"></span>
                                                                                                </a>
                                                                                            </div>
                                                                                        }

                                                                                        {
                                                                                            this.state.rendimientoVentas == 50 &&
                                                                                            <div className="row">
                                                                                                <div class="mr-3 ">
                                                                                                    <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                                                        class="btn rounded-pill btn-icon btn-sm "><span
                                                                                                            class="letter-icon text-white"></span>
                                                                                                    </a>
                                                                                                </div>
                                                                                            </div>
                                                                                        }
                                                                                        {
                                                                                            this.state.rendimientoVentas == 100 &&
                                                                                            <div className="row">
                                                                                                <div class="mr-3 ">
                                                                                                    <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FD0303', borderColor: '#FD0303' }}
                                                                                                        class="btn rounded-pill btn-icon btn-sm "><span
                                                                                                            class="letter-icon text-white"></span>
                                                                                                    </a>
                                                                                                </div>
                                                                                            </div>
                                                                                        }
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        }
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer d-flex justify-content-center">
                                                                    <button type="button" class="btn text-white" data-dismiss="modal" style={{ backgroundColor: '#617187', borderColor: '#617187' }}>Cancelar</button>

                                                                    {
                                                                        this.state.muestraDocPSP == true ? <button type="button" class="btn text-white " disabled={false} style={{ backgroundColor: '#617187', borderColor: '#617187' }}>
                                                                            <a href={this.state.rutaPSP} style={{ color: 'white' }} target="_blank" rel="noopener noreferrer" >
                                                                                Ver PSP
                                                                            </a>
                                                                        </button> : <button type="button" class="btn text-white " disabled={true} style={{ backgroundColor: '#617187', borderColor: '#617187' }}>
                                                                            <a href="#" style={{ color: 'white' }} target="_blank" rel="noopener noreferrer" >
                                                                                Ver PSP
                                                                            </a>

                                                                        </button>
                                                                    }

                                                                    <button type="button" class="btn text-white" data-dismiss="modal" style={{ backgroundColor: '#617187', borderColor: '#617187' }}>Aceptar</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div></>
                        }
                        {((this.props.presentarSeccion == "EP9" || this.props.presentarSeccion == "EP10" || this.props.presentarSeccion == "EP11"
                            || this.props.presentarSeccion == "EP12" || this.props.presentarSeccion == "EP13" || this.props.presentarSeccion == "EP14") && this.state.estatus_seccion >= 9) &&
                            <><div>
                                {/* factores importantes */}
                                <div class="accordion" id="factores_importantes">
                                    <div id="headingImportantes">
                                        <h2 class="mb-0">
                                            <button class="btn " type="button" data-toggle="collapse" data-target="#collapsImportantes" aria-expanded="true" aria-controls="collapsImportantes" ref={(ref) => this.factoresImportantes = ref}>
                                                <h6 class="mb-0 font-weight-semibold">9. Factores Importantes </h6>
                                            </button>
                                        </h2>
                                    </div>

                                    <div class="card">
                                        <div id="collapsImportantes" class="collapse " aria-labelledby="headingImportantes" data-parent="#factores_importantes">
                                            <div class="card">
                                                <div className="card-body">
                                                    <div className="row">
                                                        <div className="col-xs-6 col-lg-6">
                                                            <div class="card bg-transparent" style={{ border: 'none' }}>
                                                                <div className="card-body bg-transparent" style={{ border: 'none' }}>
                                                                    <div className="row">
                                                                        <div className="col-xs-2 col-lg-4">
                                                                            <h8 class="mb-0 font-weight-semibold"><>&nbsp;&nbsp;</> <>&nbsp;&nbsp;</></h8>
                                                                        </div>
                                                                        <div className="col-xs-2 col-lg-4">
                                                                            <span class="font-weight-semibold">ENTREVISTA RECLUTAMIENTO</span>
                                                                        </div>

                                                                        <div className="col-xs-2 col-lg-4">
                                                                            <span class="font-weight-semibold">ENTREVISTA GDD</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="col-xs-6 col-lg-6">
                                                            <div class="card bg-transparent" style={{ border: 'none' }}>
                                                                <div className="card-body bg-transparent" style={{ border: 'none' }}>
                                                                    <div className="row">
                                                                        <div className="col-xs-2 col-lg-4">
                                                                            <h8 class="mb-0 font-weight-semibold"><>&nbsp;&nbsp;</> <>&nbsp;&nbsp;</></h8>
                                                                        </div>

                                                                        <div className="col-xs-2 col-lg-4">
                                                                            <span class="font-weight-semibold">ENTREVISTA RECLUTAMIENTO</span>
                                                                        </div>

                                                                        <div className="col-xs-2 col-lg-4">
                                                                            <span class="font-weight-semibold">ENTREVISTA GDD</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-xs-6 col-lg-6">
                                                            <div class="card">
                                                                <div className="card-body">
                                                                    <div className="row">

                                                                        <div className="col-xs-2 col-lg-4">
                                                                            <h8 class="mb-0 font-weight-semibold">Espíritu emprendedor</h8>
                                                                        </div>

                                                                        <div className="col-xs-2 col-lg-4">
                                                                            <div className="custom-control custom-switch custom-control-warning mb-2">
                                                                                <input type="checkbox" class="custom-control-input" id="espiritu_emprendedor_edat" name="espiritu_emprendedor_edat" disabled={true} checked={this.state.prospecto_importantes.espiritu_emprendedor_edat} onChange={this.onChange}></input>
                                                                                <label class="custom-control-label" for="espiritu_emprendedor_edat"></label>
                                                                            </div>
                                                                        </div>

                                                                        <div className="col-xs-2 col-lg-4">
                                                                            <div className="custom-control custom-switch custom-control-warning mb-2">
                                                                                <input type="checkbox" class="custom-control-input" id="espiritu_emprendedor_gdd" name="espiritu_emprendedor_gdd" disabled={true} checked={this.state.prospecto_importantes.espiritu_emprendedor_gdd} onChange={this.onChange}></input>
                                                                                <label class="custom-control-label" for="espiritu_emprendedor_gdd"></label>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xs-6 col-lg-6">
                                                            <div class="card">
                                                                <div className="card-body">
                                                                    <div className="row">

                                                                        <div className="col-xs-2 col-lg-4">
                                                                            <h8 class="mb-0 font-weight-semibold">Mercado natural</h8>
                                                                        </div>

                                                                        <div className="col-xs-2 col-lg-4">
                                                                            <div className="custom-control custom-switch custom-control-warning mb-2">
                                                                                <input type="checkbox" class="custom-control-input" id="mercado_natural_edat" name="mercado_natural_edat" disabled={true} checked={this.state.prospecto_importantes.mercado_natural_edat} onChange={this.onChange} ></input>
                                                                                <label class="custom-control-label" for="mercado_natural_edat"></label>
                                                                            </div>
                                                                        </div>

                                                                        <div className="col-xs-2 col-lg-4">
                                                                            <div className="custom-control custom-switch custom-control-warning mb-2">
                                                                                <input type="checkbox" class="custom-control-input" id="mercado_natural_gdd" name="mercado_natural_gdd" disabled={true} checked={this.state.prospecto_importantes.mercado_natural_gdd} onChange={this.onChange} ></input>
                                                                                <label class="custom-control-label" for="mercado_natural_gdd"></label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-xs-6 col-lg-6">
                                                            <div class="card">
                                                                <div className="card-body">
                                                                    <div className="row">
                                                                        <div className="col-xs-2 col-lg-4">
                                                                            <h8 class="mb-0 font-weight-semibold">Apoyo familiar</h8>
                                                                        </div>

                                                                        <div className="col-xs-2 col-lg-4">
                                                                            <div className="custom-control custom-switch custom-control-warning mb-2">
                                                                                <input type="checkbox" class="custom-control-input" id="apoyo_familiar_edat" name="apoyo_familiar_edat" disabled={true} checked={this.state.prospecto_importantes.apoyo_familiar_edat} onChange={this.onChange} ></input>
                                                                                <label class="custom-control-label" for="apoyo_familiar_edat"></label>
                                                                            </div>
                                                                        </div>

                                                                        <div className="col-xs-2 col-lg-4">
                                                                            <div className="custom-control custom-switch custom-control-warning mb-2">
                                                                                <input type="checkbox" class="custom-control-input" id="apoyo_familiar_gdd" name="apoyo_familiar_gdd" disabled={true} checked={this.state.prospecto_importantes.apoyo_familiar_gdd} onChange={this.onChange} ></input>
                                                                                <label class="custom-control-label" for="apoyo_familiar_gdd"></label>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xs-6 col-lg-6">
                                                            <div class="card">
                                                                <div className="card-body">
                                                                    <div className="row">

                                                                        <div className="col-xs-2 col-lg-4">
                                                                            <h8 class="mb-0 font-weight-semibold">Estabilidad financiera</h8>
                                                                        </div>

                                                                        <div className="col-xs-2 col-lg-4">
                                                                            <div className="custom-control custom-switch custom-control-warning mb-2">
                                                                                <input type="checkbox" class="custom-control-input" id="estabilidad_financiera_edat" name="estabilidad_financiera_edat" disabled={true} checked={this.state.prospecto_importantes.estabilidad_financiera_edat} onChange={this.onChange} ></input>
                                                                                <label class="custom-control-label" for="estabilidad_financiera_edat"></label>
                                                                            </div>
                                                                        </div>

                                                                        <div className="col-xs-2 col-lg-4">
                                                                            <div className="custom-control custom-switch custom-control-warning mb-2">
                                                                                <input type="checkbox" class="custom-control-input" id="estabilidad_financiera_gdd" name="estabilidad_financiera_gdd" disabled={true} checked={this.state.prospecto_importantes.estabilidad_financiera_gdd} onChange={this.onChange} ></input>
                                                                                <label class="custom-control-label" for="estabilidad_financiera_gdd"></label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-xs-6 col-lg-6">
                                                            <div class="card">
                                                                <div className="card-body">
                                                                    <div className="row">

                                                                        <div className="col-xs-2 col-lg-4">
                                                                            <h8 class="mb-0 font-weight-semibold">Educación</h8>
                                                                        </div>

                                                                        <div className="col-xs-2 col-lg-4">
                                                                            <div className="custom-control custom-switch custom-control-warning mb-2">
                                                                                <input type="checkbox" class="custom-control-input" id="educacion_edat" name="educacion_edat" disabled={true} checked={this.state.prospecto_importantes.educacion_edat} onChange={this.onChange}></input>
                                                                                <label class="custom-control-label" for="educacion_edat"></label>
                                                                            </div>
                                                                        </div>

                                                                        <div className="col-xs-2 col-lg-4">
                                                                            <div className="custom-control custom-switch custom-control-warning mb-2">
                                                                                <input type="checkbox" class="custom-control-input" id="educacion_gdd" name="educacion_gdd" disabled={true} checked={this.state.prospecto_importantes.educacion_gdd} onChange={this.onChange}></input>
                                                                                <label class="custom-control-label" for="educacion_gdd"></label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xs-6 col-lg-6">
                                                            <div class="card">
                                                                <div className="card-body">
                                                                    <div className="row">
                                                                        <div className="col-xs-2 col-lg-4">
                                                                            <h8 class="mb-0 font-weight-semibold">Movilidad social</h8>
                                                                        </div>
                                                                        <div className="col-xs-2 col-lg-4">
                                                                            <div className="custom-control custom-switch custom-control-warning mb-2">
                                                                                <input type="checkbox" class="custom-control-input" id="movilidad_social_edat" name="movilidad_social_edat" disabled={true} checked={this.state.prospecto_importantes.movilidad_social_edat} onChange={this.onChange} ></input>
                                                                                <label class="custom-control-label" for="movilidad_social_edat"></label>
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-xs-2 col-lg-4">
                                                                            <div className="custom-control custom-switch custom-control-warning mb-2">
                                                                                <input type="checkbox" class="custom-control-input" id="movilidad_social_gdd" name="movilidad_social_gdd" disabled={true} checked={this.state.prospecto_importantes.movilidad_social_gdd} onChange={this.onChange} ></input>
                                                                                <label class="custom-control-label" for="movilidad_social_gdd"></label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div></>
                        }
                        {((this.props.presentarSeccion == "EP8" || this.props.presentarSeccion == "EP9" || this.props.presentarSeccion == "EP10" ||
                            this.props.presentarSeccion == "EP11" || this.props.presentarSeccion == "EP12" || this.props.presentarSeccion == "EP13" || this.props.presentarSeccion == "EP14") && this.state.estatus_seccion >= 7) &&
                            <><div>
                                {/* factores vitales */}
                                <div class="accordion" id="accordionExample">
                                    <div id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn " type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" ref={(ref) => this.factoresVitales = ref}>
                                                <h6 class="mb-0 font-weight-semibold">8- Factores vitales </h6>
                                            </button>
                                        </h2>
                                    </div>
                                    <div class="card">
                                        <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card" >
                                                <div class="card-header text-center">
                                                    <div className="row">
                                                        <div className="col-xs-2 col-lg-2">
                                                            <h6 class="card-title"></h6>
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <h6 class="card-title"><strong>EDAT</strong></h6>
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <h6 class="card-title"><strong>Gerente EDAT</strong></h6>
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <h6 class="card-title"><strong>GDD</strong></h6>
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <h6 class="card-title"><strong>Capacitación</strong></h6>
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <h6 class="card-title"><strong>SDDC</strong></h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div className="card-body text-center">
                                                    <div className="row">
                                                        <div className="col-xs-2 col-lg-2">
                                                            <h8 class="mb-0 font-weight-semibold">Orientación al logros</h8>
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="orientacion_logro_edat" disabled={true} onChange={this.onChangeCapacita} value={this.state.prospecto.orientacion_logro_edat} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>

                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="orientacion_logro_edat2" disabled={true} onChange={this.onChangeEdat2} value={this.state.prospecto.orientacion_logro_edat2} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="orientacion_logro_gdd" disabled={true} ref={(ref) => this.focusFV = ref} onChange={this.onChangeGDD.bind(this)} value={this.state.prospecto.orientacion_logro_gdd} style={this.props.tipoUsuario == "GDD" ? { borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' } : { backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>

                                                        </div>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="orientacion_logro_capacita" disabled={true} onChange={this.onChangeCapacita} value={this.state.prospecto.orientacion_logro_capacita} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="orientacion_logro_direccion" disabled={true} onChange={this.onChangeDireccion} value={this.state.prospecto.orientacion_logro_direccion} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                        <div className="col-xs-2 col-lg-2">
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div className="card-body  text-center">
                                                    <div className="row">
                                                        <div className="col-xs-2 col-lg-2">
                                                            <h8 class="mb-0 font-weight-semibold">Perseverancia</h8>
                                                        </div>

                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="perceverancia_edat" disabled={true} onChange={this.onChange} value={this.state.prospecto.perceverancia_edat} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>

                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="perceverancia_edat2" disabled={true} onChange={this.onChangeEdat2} value={this.state.prospecto.perceverancia_edat2} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="perceverancia_gdd" disabled={true} onChange={this.onChangeGDD.bind(this)} value={this.state.prospecto.perceverancia_gdd} style={this.props.tipoUsuario == "GDD" ? { borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' } : { backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="perceverancia_capacita" disabled={true} onChange={this.onChangeCapacita} value={this.state.prospecto.perceverancia_capacita} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="perceverancia_direccion" disabled={true} onChange={this.onChangeDireccion} value={this.state.prospecto.perceverancia_direccion} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                        <div className="col-xs-2 col-lg-2">
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div className="card-body  text-center">
                                                    <div className="row">
                                                        <div className="col-xs-2 col-lg-2">
                                                            <h8 class="mb-0 font-weight-semibold">Carácter e integridad</h8>
                                                        </div>

                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="integridad_edat" disabled={true} onChange={this.onChange} value={this.state.prospecto.integridad_edat} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>

                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="integridad_edat2" disabled={true} onChange={this.onChangeEdat2} value={this.state.prospecto.integridad_edat2} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="integridad_gdd" disabled={true} onChange={this.onChangeGDD.bind(this)} value={this.state.prospecto.integridad_gdd} style={this.props.tipoUsuario == "GDD" ? { borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' } : { backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="integridad_capacita" disabled={true} onChange={this.onChangeCapacita} value={this.state.prospecto.integridad_capacita} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="integridad_direccion" disabled={true} onChange={this.onChangeDireccion} value={this.state.prospecto.integridad_direccion} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                        <div className="col-xs-2 col-lg-2">
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div className="card-body  text-center">
                                                    <div className="row">
                                                        <div className="col-xs-2 col-lg-2">
                                                            <h8 class="mb-0 font-weight-semibold">Sentido común</h8>
                                                        </div>

                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="sentido_comun_edat" disabled={true} onChange={this.onChange} value={this.state.prospecto.sentido_comun_edat} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>

                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="sentido_comun_edat2" disabled={true} onChange={this.onChangeEdat2} value={this.state.prospecto.sentido_comun_edat2} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="sentido_comun_gdd" disabled={true} onChange={this.onChangeGDD.bind(this)} value={this.state.prospecto.sentido_comun_gdd} style={this.props.tipoUsuario == "GDD" ? { borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' } : { backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="sentido_comun_capacita" disabled={true} onChange={this.onChangeCapacita} value={this.state.prospecto.sentido_comun_capacita} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="sentido_comun_direccion" disabled={true} onChange={this.onChangeDireccion} value={this.state.prospecto.sentido_comun_direccion} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                        <div className="col-xs-2 col-lg-2">
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div className="card-body  text-center">
                                                    <div className="row">
                                                        <div className="col-xs-2 col-lg-2">
                                                            <h8 class="mb-0 font-weight-semibold">Energía</h8>
                                                        </div>

                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="energia_edat" disabled={true} onChange={this.onChange} value={this.state.prospecto.energia_edat} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>

                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="energia_edat2" disabled={true} onChange={this.onChangeEdat2} value={this.state.prospecto.energia_edat2} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="energia_gdd" disabled={true} onChange={this.onChangeGDD.bind(this)} value={this.state.prospecto.energia_gdd} style={this.props.tipoUsuario == "GDD" ? { borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' } : { backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="energia_capacita" disabled={true} onChange={this.onChangeCapacita} value={this.state.prospecto.energia_capacita} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="energia_direccion" disabled={true} onChange={this.onChangeDireccion} value={this.state.prospecto.energia_direccion} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                        <div className="col-xs-2 col-lg-2">
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div className="card-body  text-center">
                                                    <div className="row">
                                                        <div className="col-xs-2 col-lg-2">
                                                            <h8 class="mb-0 font-weight-semibold">Motivación por el dinero</h8>
                                                        </div>

                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="motivacion_edat" disabled={true} onChange={this.onChange} value={this.state.prospecto.motivacion_edat} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>

                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="motivacion_edat2" disabled={true} onChange={this.onChangeEdat2} value={this.state.prospecto.motivacion_edat2} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="motivacion_gdd" disabled={true} onChange={this.onChangeGDD.bind(this)} value={this.state.prospecto.motivacion_gdd} style={this.props.tipoUsuario == "GDD" ? { borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' } : { backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="motivacion_capacita" disabled={true} onChange={this.onChangeCapacita} value={this.state.prospecto.motivacion_capacita} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="motivacion_direccion" disabled={true} onChange={this.onChangeDireccion} value={this.state.prospecto.motivacion_direccion} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                        <div className="col-xs-2 col-lg-2">
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                        <div className="col-xs-2 col-lg-2"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card" style={{ background: '#E1E3F6 ', borderRadius: '3px 3px 0px 0px' }}>
                                                <div className="card-body  text-center">
                                                    <div className="row">
                                                        <div className="col-xs-2 col-lg-2">
                                                            <h8 class="mb-0 font-weight-semibold">TOTAL</h8>
                                                        </div>

                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="total_edat" disabled={true} value={this.state.prospecto.total_edat} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>

                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="total_edat2" disabled={true} value={this.state.prospecto.total_edat2} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="total_gdd" disabled={true} value={this.state.prospecto.total_gdd} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="total_capacita" disabled={true} value={this.state.prospecto.total_capacita} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="number" name="total_direccion" disabled={true} value={this.state.prospecto.total_direccion} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div></>
                        }
                        {((this.props.presentarSeccion == "EP7" || this.props.presentarSeccion == "EP8" || this.props.presentarSeccion == "EP9" ||
                            this.props.presentarSeccion == "EP10" || this.props.presentarSeccion == "EP11" || this.props.presentarSeccion == "EP12" || this.props.presentarSeccion == "EP13" || this.props.presentarSeccion == "EP14") && this.state.estatus_seccion >= 7) &&
                            <><div>
                                {/* Evaluación del entrevistador*/}

                                <div class="accordion" id="factores_EvaluacionEntrevistador">
                                    <div id="headingEvaluacionEntrevistador">
                                        <h2 class="mb-0">
                                            <button class="btn " type="button" data-toggle="collapse" data-target="#collapsEvaluacionEntrevistador" aria-expanded="true" aria-controls="collapsEvaluacionEntrevistador">
                                                <h6 class="mb-0 font-weight-semibold">7. Evaluación del entrevistador </h6>
                                            </button>
                                        </h2>
                                    </div>

                                    <div class="card">
                                        <div id="collapsEvaluacionEntrevistador" class="collapse " aria-labelledby="headingEvaluacionEntrevistador" data-parent="#factores_EvaluacionEntrevistador">
                                            <div class="card">
                                                <div className="card-body">
                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-12">
                                                            <h8 class="mb-0 font-weight-semibold"> Puntualidad para la entrevista:</h8>
                                                        </div>
                                                    </div>
                                                    <br></br>
                                                    <div className="row">
                                                        <div className="col-xs-6 col-lg-12">
                                                            <div className="form-group">
                                                                {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                <input class="form-check-input" type="radio" name="puntualidad" id="noHubo" value={0} disabled={true} onChange={this.onChange} checked={this.state.prospecto_entrevistador.puntualidad == 0} />
                                                                <label class="form-check-label" for="noHubo">
                                                                    No hubo entrevista
                                                                </label>
                                                                {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                <input class="form-check-input" type="radio" name="puntualidad" id="temprano" value={1} disabled={true} onChange={this.onChange} checked={this.state.prospecto_entrevistador.puntualidad == 1} />
                                                                <label class="form-check-label" for="temprano">
                                                                    Llegó temprano
                                                                </label>
                                                                {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                <input class="form-check-input" type="radio" name="puntualidad" id="puntual" value={2} disabled={true} onChange={this.onChange} checked={this.state.prospecto_entrevistador.puntualidad == 2} />
                                                                <label class="form-check-label" for="puntual">
                                                                    Llegó puntual
                                                                </label>
                                                                {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                <label class="form-check-label" for="casado">
                                                                    Llegó
                                                                </label>
                                                                {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                <input type="number" name="minutos_tarde" value={this.state.prospecto_entrevistador.minutos_tarde} disabled={true} onChange={this.onChange} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', borderRadius: '3px', border: '1px' }} />
                                                                {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                <label class="form-check-label" for="casado">
                                                                    minutos tarde
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="row">
                                                <div className="col-xs-3 col-lg-3">
                                                    <h8 class="mb-0 font-weight-semibold text-white"> Resultados PSP</h8>
                                                </div>
                                                <div className="col-xs-9 col-lg-9">
                                                    <table className="table datatable-sorting  table-striped table-hover" >
                                                        <thead >
                                                            <tr>
                                                                <th className="text-left font-weight-bold "  >
                                                                    <a>SOBRESALIENTE</a>
                                                                </th>
                                                                <th className="text-left font-weight-bold " >
                                                                    <a>SOBRE EL PROMEDIO</a>
                                                                </th>
                                                                <th className="text-center font-weight-bold ">
                                                                    <a>PROMEDIO</a>
                                                                </th>
                                                                <th className="text-right font-weight-bold ">
                                                                    <a>BAJO EL PROMEDIO</a>
                                                                </th>
                                                                <th className="text-right font-weight-bold ">
                                                                    <a>INSTISFACTORIO</a>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div className="card-body">
                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-12">
                                                            <h8 class="mb-0 font-weight-semibold"> Habilidad para la comunicación</h8>
                                                        </div>
                                                    </div>
                                                    <br></br>
                                                    <div className="row">
                                                        <div className="col-xs-2 col-lg-3 ">
                                                            <label style={{ color: '#617187' }} className="font-weight-semibold" for="salud">Habilidad para expresarse con palabras y gestos</label>
                                                        </div>
                                                        <div className="col-xs-12 col-lg-9  ">
                                                            <input type="range" style={{ marginLeft: '5px' }} class="custom-range" min="0" name="habilidad_expresarse" max="80" disabled={true} step={20} onChange={this.onChange} value={this.state.prospecto_entrevistador.habilidad_expresarse} />
                                                        </div>
                                                    </div>
                                                    <br></br>
                                                    <div className="row">
                                                        <div className="col-xs-2 col-lg-3 ">
                                                            <label style={{ color: '#617187' }} className="font-weight-semibold" for="salud">Habilidad para sintetizar ideas, en forma clara y directa</label>
                                                        </div>
                                                        <div className="col-xs-12 col-lg-9  ">
                                                            <input type="range" style={{ marginLeft: '5px' }} class="custom-range" min="0" max="80" disabled={true} step={20} onChange={this.onChange} name="habilidad_sintetizar" value={this.state.prospecto_entrevistador.habilidad_sintetizar} />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card">
                                                <div className="card-body">
                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-12">
                                                            <h8 class="mb-0 font-weight-semibold">Habilidad para controlar situaciones</h8>
                                                        </div>
                                                    </div>
                                                    <br></br>
                                                    <div className="row">
                                                        <div className="col-xs-2 col-lg-3 ">
                                                            <label style={{ color: '#617187' }} className="font-weight-semibold" for="salud">Apariencia personal e impresión a primera vista</label>
                                                        </div>

                                                        <div className="col-xs-12 col-lg-9  ">
                                                            <input type="range" style={{ marginLeft: '5px' }} class="custom-range" min="0" max="80" disabled={true} step={20} onChange={this.onChange} name="apariencia_personal" value={this.state.prospecto_entrevistador.apariencia_personal} />
                                                        </div>
                                                    </div>
                                                    <br></br>
                                                    <div className="row">
                                                        <div className="col-xs-2 col-lg-3 ">
                                                            <label style={{ color: '#617187' }} className="font-weight-semibold" for="salud">Demostración de confianza durante la entrevista</label>
                                                        </div>
                                                        <div className="col-xs-12 col-lg-9  ">
                                                            <input type="range" style={{ marginLeft: '5px' }} class="custom-range" min="0" max="80" disabled={true} step={20} onChange={this.onChange} name="demostracion_confianza" value={this.state.prospecto_entrevistador.demostracion_confianza} />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card">
                                                <div className="card-body">
                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-12">
                                                            <h8 class="mb-0 font-weight-semibold"> Interés en el puesto</h8>
                                                        </div>
                                                    </div>
                                                    <br></br>
                                                    <div className="row">
                                                        <div className="col-xs-2 col-lg-3 ">
                                                            <label style={{ color: '#617187' }} className="font-weight-semibold" for="salud">Se muestra seguro con la idea de un cambio de carrera</label>
                                                        </div>
                                                        <div className="col-xs-12 col-lg-9  ">
                                                            <input type="range" style={{ marginLeft: '5px' }} class="custom-range" min="0" max="80" disabled={true} step={20} onChange={this.onChange} name="muestra_seguro" value={this.state.prospecto_entrevistador.muestra_seguro} />
                                                        </div>
                                                    </div>
                                                    <br></br>
                                                    <div className="row">
                                                        <div className="col-xs-2 col-lg-3 ">
                                                            <label style={{ color: '#617187' }} className="font-weight-semibold" for="salud">Se encuentra seriamente interesado en desarrollar una carrera de ventas</label>
                                                        </div>
                                                        <div className="col-xs-12 col-lg-9  ">
                                                            <input type="range" style={{ marginLeft: '5px' }} class="custom-range" min="0" max="80" disabled={true} step={20} onChange={this.onChange} name="encuentra_seriamente" value={this.state.prospecto_entrevistador.muestraencuentra_seriamente_seguro} />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div className="card-body">
                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-12">
                                                            <h8 class="mb-0 font-weight-semibold"> Observaciones</h8>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-12">
                                                            <textarea style={{ width: '100%', height: '100%', backgroundColor: '#F1F3FA', borderColor: '#D5D9E8' }} disabled={true} onChange={this.onChange} name="observaciones" value={this.state.prospecto_entrevistador.observaciones}> </textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div></>
                        }
                        {((this.props.presentarSeccion == "EP6"
                            || this.props.presentarSeccion == "EP7" || this.props.presentarSeccion == "EP8" || this.props.presentarSeccion == "EP9" || this.props.presentarSeccion == "EP10" ||
                            this.props.presentarSeccion == "EP11" || this.props.presentarSeccion == "EP12" || this.props.presentarSeccion == "EP13" || this.props.presentarSeccion == "EP14") && this.state.estatus_seccion >= 6) &&
                            <><div>
                                {/* 6.- Áreas de vida*/}
                                <div class="accordion" id="factores_Areas">
                                    <div id="headingAreas">
                                        <h2 class="mb-0">
                                            <button class="btn " type="button" data-toggle="collapse" data-target="#collapsAreas" aria-expanded="true" aria-controls="collapsAreas">
                                                <h6 class="mb-0 font-weight-semibold">6. Áreas de vida </h6>
                                            </button>
                                        </h2>
                                    </div>
                                    <div class="card">
                                        <div id="collapsAreas" class="collapse " aria-labelledby="headingAreas" data-parent="#factores_Areas">
                                            <div class="card">
                                                <div className="card-body">
                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-12">
                                                            <h8 class="mb-0 font-weight-semibold"> Identifica en qué porcentaje de cada área de tu vida te encuentras en este momento</h8>
                                                        </div>
                                                    </div>
                                                    <br></br>
                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-6">
                                                            <label style={{ color: '#617187' }} className="font-weight-semibold" for="salud">Salud {this.state.prospecto_areas.salud + "%"}</label>
                                                            <input type="range" class="custom-range" min="0" name="salud" max="100" disabled={true} onChange={this.onChange} step={1} value={this.state.prospecto_areas.salud} id="salud" />
                                                        </div>
                                                        <div className="col-xs-12 col-lg-6">
                                                            <label style={{ color: '#617187' }} className="font-weight-semibold" for="amigos">Amigos {this.state.prospecto_areas.amigos + "%"}</label>
                                                            <input type="range" class="custom-range" min="0" name="amigos" max="100" disabled={true} onChange={this.onChange} step={1} value={this.state.prospecto_areas.amigos} id="amigos" />
                                                        </div>
                                                    </div>
                                                    <br></br>
                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-6">
                                                            <label style={{ color: '#617187' }} className="font-weight-semibold" for="dinero">Dinero {this.state.prospecto_areas.dinero + "%"}</label>
                                                            <input type="range" class="custom-range" min="0" name="dinero" max="100" disabled={true} onChange={this.onChange} step={1} value={this.state.prospecto_areas.dinero} id="dinero" />
                                                        </div>
                                                        <div className="col-xs-12 col-lg-6">
                                                            <label style={{ color: '#617187' }} className="font-weight-semibold" for="familia">Familia {this.state.prospecto_areas.familia + "%"}</label>
                                                            <input type="range" class="custom-range" min="0" name="familia" max="100" disabled={true} onChange={this.onChange} step={1} value={this.state.prospecto_areas.familia} id="familia" />
                                                        </div>
                                                    </div>
                                                    <br></br>
                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-6">
                                                            <label style={{ color: '#617187' }} className="font-weight-semibold" for="trabajo">Trabajo {this.state.prospecto_areas.trabajo + "%"}</label>
                                                            <input type="range" class="custom-range" min="0" name="trabajo" max="100" disabled={true} onChange={this.onChange} step={1} value={this.state.prospecto_areas.trabajo} id="trabajo" />
                                                        </div>
                                                        <div className="col-xs-12 col-lg-6">
                                                            <label style={{ color: '#617187' }} className="font-weight-semibold" for="desarrollo">Desarrollo personal {this.state.prospecto_areas.desarrollo_personal + "%"}</label>
                                                            <input type="range" class="custom-range" min="0" name="desarrollo_personal" max="100" disabled={true} onChange={this.onChange} step={1} value={this.state.prospecto_areas.desarrollo_personal} id="desarrollo" />
                                                        </div>
                                                    </div>
                                                    <br></br>
                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-6">
                                                            <label style={{ color: '#617187' }} className="font-weight-semibold" for="diversion">Diversión / ocio {this.state.prospecto_areas.diversion + "%"}</label>
                                                            <input type="range" class="custom-range" min="0" name="diversion" max="100" disabled={true} onChange={this.onChange} step={1} value={this.state.prospecto_areas.diversion} id="diversion" />
                                                        </div>
                                                        <div className="col-xs-12 col-lg-6">
                                                            <label style={{ color: '#617187' }} className="font-weight-semibold" for="pareja">Pareja {this.state.prospecto_areas.pareja + "%"}</label>
                                                            <input type="range" class="custom-range" min="0" name="pareja" max="100" disabled={true} onChange={this.onChange} step={1} value={this.state.prospecto_areas.pareja} id="pareja" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div></>
                        }
                        {((this.props.presentarSeccion == "EP5" || this.props.presentarSeccion == "EP6"
                            || this.props.presentarSeccion == "EP7" || this.props.presentarSeccion == "EP8" || this.props.presentarSeccion == "EP9" || this.props.presentarSeccion == "EP10"
                            || this.props.presentarSeccion == "EP11" || this.props.presentarSeccion == "EP12" || this.props.presentarSeccion == "EP13" || this.props.presentarSeccion == "EP14") && this.state.estatus_seccion >= 5) &&
                            <><div>
                                {/* 5.- Fue Agente*/}
                                <div class="accordion" id="fue_agente">
                                    <div id="headingFue_agente">
                                        <h2 class="mb-0">
                                            <button class="btn " type="button" data-toggle="collapse" data-target="#collapsFue_agente" aria-expanded="true" aria-controls="collapsFue_agente">
                                                <h6 class="mb-0 font-weight-semibold">5. Fue agente </h6>
                                            </button>
                                        </h2>
                                    </div>
                                    <div class="card">
                                        <div id="collapsFue_agente" class="collapse " aria-labelledby="Fue_agente" data-parent="#fue_agente">
                                            <div class="card">
                                                <div className="card-body">
                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <span >
                                                                    <span className="font-weight-semibold"> ¿En qué fecha te diste de alta?</span>
                                                                </span>
                                                                <div className="input-group">
                                                                    <input type="date" placeholder="Escribir" name="fecha_alta" disabled={true} onChange={this.onChange} value={this.state.prospecto_agente.fecha_alta} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <span >
                                                                    <span className="font-weight-semibold">¿Tu cédula continúa vigente?</span>
                                                                </span>
                                                                <div className="input-group">
                                                                    <input type="text" placeholder="Escribir" name="cedula_vigencia" disabled={true} onChange={this.onChange} value={this.state.prospecto_agente.cedula_vigencia} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <span >
                                                                    <span className="font-weight-semibold">¿Eras agente de GNP?</span>
                                                                </span>
                                                                <div className="input-group">
                                                                    <input type="text" placeholder="Escribir" name="agente_gnp" disabled={true} onChange={this.onChange} value={this.state.prospecto_agente.agente_gnp} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <span >
                                                                    <span className="font-weight-semibold">¿Qué tipo de Cédula tienes?</span>
                                                                </span>
                                                                <div className="input-group">
                                                                    <input type="text" placeholder="Escribir" name="tipo_cedula" disabled={true} onChange={this.onChange} value={this.state.prospecto_agente.tipo_cedula} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xs-12 col-lg-8">
                                                            <div className="form-group">
                                                                <span >
                                                                    <span className="font-weight-semibold">¿En qué otras aseguradoras estás dado de alta?</span>
                                                                </span>
                                                                <div className="input-group">
                                                                    <input type="text" placeholder="Escribir" name="aseguradoras" disabled={true} onChange={this.onChange} value={this.state.prospecto_agente.aseguradoras} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <span >
                                                                    <span className="font-weight-semibold">¿En qué DA estás o estuviste?</span>
                                                                </span>
                                                                <div className="input-group">
                                                                    <input type="text" placeholder="Escribir" name="da_estuviste" disabled={true} onChange={this.onChange} value={this.state.prospecto_agente.da_estuviste} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xs-12 col-lg-8">
                                                            <div className="form-group">
                                                                <span >
                                                                    <span className="font-weight-semibold">¿Por qué buscas cambiarte?</span>
                                                                </span>
                                                                <div className="input-group">
                                                                    <input type="text" placeholder="Escribir" name="buscar_cambiarse" disabled={true} onChange={this.onChange} value={this.state.prospecto_agente.buscar_cambiarse} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card">
                                                <div className="card-body">
                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <span >
                                                                    <span className="font-weight-semibold">¿Sabes el monto de tu cartera total en VIDA? (primas)</span>
                                                                </span>
                                                                <div className="input-group">
                                                                    <input type="number" placeholder="Escribir" name="cartera_vida" disabled={true} onChange={this.onChange} value={this.state.prospecto_agente.cartera_vida} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <span >
                                                                    <span className="font-weight-semibold">¿Tu conservación VIDA en último periodo vigente?</span>
                                                                </span>
                                                                <div className="input-group">
                                                                    <input type="text" placeholder="Escribir" name="conservacion_vida" disabled={true} onChange={this.onChange} value={this.state.prospecto_agente.conservacion_vida} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <span >
                                                                    <span className="font-weight-semibold">¿De cuánto es tu Cartera Total GMM?</span>
                                                                </span>
                                                                <div className="input-group">
                                                                    <input type="number" placeholder="Escribir" name="cartera_gmm" disabled={true} onChange={this.onChange} value={this.state.prospecto_agente.cartera_gmm} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>



                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-6">
                                                            <div className="form-group">
                                                                <span >
                                                                    <span className="font-weight-semibold">¿Sabes tú % de Siniestralidad en último periodo vigente en GMM?</span>
                                                                </span>
                                                                <div className="input-group">
                                                                    <input type="number" placeholder="Escribir" name="vigente_gmm" disabled={true} onChange={this.onChange} value={this.state.prospecto_agente.vigente_gmm} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xs-12 col-lg-6">
                                                            <div className="form-group">
                                                                <span >
                                                                    <span className="font-weight-semibold">¿Sabes tú % de Siniestralidad en último periodo vigente en autos?</span>
                                                                </span>
                                                                <div className="input-group">
                                                                    <input type="text" placeholder="Escribir" name="vigente_autos" disabled={true} onChange={this.onChange} value={this.state.prospecto_agente.vigente_autos} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <span >
                                                                    <span className="font-weight-semibold"> ¿Sabes el número de tu Cartera Total Autos?</span>
                                                                </span>
                                                                <div className="input-group">
                                                                    <input type="text" placeholder="Escribir" name="cartera_autos" disabled={true} onChange={this.onChange} value={this.state.prospecto_agente.cartera_autos} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <span >
                                                                    <span className="font-weight-semibold">¿Sabes el número de tu Cartera Total Daños?</span>
                                                                </span>
                                                                <div className="input-group">
                                                                    <input type="number" placeholder="Escribir" name="cartera_danos" disabled={true} onChange={this.onChange} value={this.state.prospecto_agente.cartera_danos} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <span >
                                                                    <span className="font-weight-semibold">Cartera Total Empresariales (en caso de ser cédula B)</span>
                                                                </span>
                                                                <div className="input-group">
                                                                    <input type="number" placeholder="Escribir" name="cartera_empresarial" disabled={true} onChange={this.onChange} value={this.state.prospecto_agente.cartera_empresarial} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div></>
                        }
                        {((this.props.presentarSeccion == "EP1" || this.props.presentarSeccion == "EP2" || this.props.presentarSeccion == "EP3"
                            || this.props.presentarSeccion == "EP4" || this.props.presentarSeccion == "EP5" || this.props.presentarSeccion == "EP6"
                            || this.props.presentarSeccion == "EP7" || this.props.presentarSeccion == "EP8" || this.props.presentarSeccion == "EP9"
                            || this.props.presentarSeccion == "EP10" || this.props.presentarSeccion == "EP11" || this.props.presentarSeccion == "EP12"
                            || this.props.presentarSeccion == "EP13" || this.props.presentarSeccion == "EP14") && this.state.estatus_seccion >= 4) &&
                            <><div>
                                <div class="accordion" id="Explorando4">
                                    <div id="headingExplorando4">
                                        <h2 class="mb-0">
                                            <button class="btn " type="button" data-toggle="collapse" data-target="#collapsExplorando4" aria-expanded="true" aria-controls="collapsExplorando4">
                                                <h6 class="mb-0 font-weight-semibold">1-4. Encuentro </h6>
                                            </button>
                                        </h2>
                                    </div>
                                    <div class="card">
                                        <div id="collapsExplorando4" class="collapse " aria-labelledby="Explorando4" data-parent="#Explorando4">
                                            {(this.state.muestra_encuentro >= 4 && (this.props.presentarSeccion == "EP4" ||
                                                this.props.presentarSeccion == "EP5" || this.props.presentarSeccion == "EP6" ||
                                                this.props.presentarSeccion == "EP7" || this.props.presentarSeccion == "EP8" ||
                                                this.props.presentarSeccion == "EP9" || this.props.presentarSeccion == "EP10" ||
                                                this.props.presentarSeccion == "EP11" || this.props.presentarSeccion == "EP12" ||
                                                this.props.presentarSeccion == "EP13" || this.props.presentarSeccion == "EP14")) &&
                                                <div>
                                                    <div class="card">
                                                        <div className="card-body">
                                                            <div className="row">
                                                                <div className="col-xs-12 col-lg-12">
                                                                    <div className="form-group">
                                                                        <span >
                                                                            <span className="font-weight-semibold"> Menciona los valores con los que más te identificas</span>
                                                                        </span>
                                                                        <div className="input-group">
                                                                            <input type="text" placeholder="Escribir" name="valores_identifica" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando4.valores_identifica} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="row">
                                                                <div className="col-xs-12 col-lg-6">
                                                                    <div className="form-group">
                                                                        <span >
                                                                            <span className="font-weight-semibold"> Logro académico (infancia)</span>
                                                                        </span>
                                                                        <div className="input-group">
                                                                            <input type="text" placeholder="Escribir" name="logro_academico" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando4.logro_academico} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="col-xs-12 col-lg-6">
                                                                    <div className="form-group">
                                                                        <span >
                                                                            <span className="font-weight-semibold">Logro laboral (adulto)</span>
                                                                        </span>
                                                                        <div className="input-group">
                                                                            <input type="text" placeholder="Escribir" name="logro_laboral" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando4.logro_laboral} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="row">
                                                                <div className="col-xs-12 col-lg-12">
                                                                    <div className="form-group">
                                                                        <span >
                                                                            <span className="font-weight-semibold">¿Platícame de una situación difícil en tu vida y como superaste los obstáculos?</span>
                                                                        </span>
                                                                        <div className="input-group">
                                                                            <input type="text" placeholder="Escribir" name="cituacion_dificil" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando4.cituacion_dificil} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div className="row">
                                                                <div className="col-xs-12 col-lg-6">
                                                                    <div className="form-group">
                                                                        <span >
                                                                            <span className="font-weight-semibold"> Financieramente hablando, ¿Tienes alguna deuda que te preocupe?</span>
                                                                        </span>
                                                                        <div className="input-group">
                                                                            <input type="text" placeholder="Escribir" name="deuda_financiera" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando4.deuda_financiera} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-xs-12 col-lg-6">
                                                                    <div className="form-group">
                                                                        <span >
                                                                            <span className="font-weight-semibold">¿A cuánto asciende?</span>
                                                                        </span>
                                                                        <div className="input-group">
                                                                            <input type="number" placeholder="Escribir" name="monto_deuda" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando4.monto_deuda} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div className="row">
                                                                <div className="col-xs-12 col-lg-12">
                                                                    <div className="form-group">
                                                                        <span >
                                                                            <span className="font-weight-semibold">¿Qué aspectos de tu vida deseas mejorar?</span>
                                                                        </span>
                                                                        <div className="input-group">
                                                                            <input type="text" placeholder="Escribir" name="aspectos_mejora" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando4.aspectos_mejora} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card" style={{ backgroundColor: '#E1E3F6' }}>
                                                        <div className="card-body">
                                                            <div className="row">
                                                                <div className="col-xs-12 col-lg-12">
                                                                    <div className="form-group">
                                                                        <span >
                                                                            <span className="font-weight-semibold">Hilo de descontento</span>
                                                                        </span>
                                                                        <div className="input-group">
                                                                            <textarea type="text" placeholder="Escribir" name="hilo_descontento" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando4.hilo_descontento} className="form-control " style={{ borderColor: '#C8CDF6', backgroundColor: '#FFFF' }} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            }

                                            {/*03. Encuentro */}
                                            {(this.state.muestra_encuentro >= 3 && (this.props.presentarSeccion == "EP3"
                                                || this.props.presentarSeccion == "EP4" || this.props.presentarSeccion == "EP5" ||
                                                this.props.presentarSeccion == "EP6" ||
                                                this.props.presentarSeccion == "EP7" || this.props.presentarSeccion == "EP8" ||
                                                this.props.presentarSeccion == "EP9" || this.props.presentarSeccion == "EP10" ||
                                                this.props.presentarSeccion == "EP11" || this.props.presentarSeccion == "EP12"
                                                || this.props.presentarSeccion == "EP13" || this.props.presentarSeccion == "EP14"
                                            )) &&
                                                <div>
                                                    <div class="card">
                                                        <div className="card-body">
                                                            <div className="row">
                                                                <div className="col-xs-12 col-lg-12">
                                                                    <div className="form-group">
                                                                        <span >
                                                                            <span className="font-weight-semibold"> Háblame de la última meta profesional que te fijaste, ¿la alcanzaste? ¿cómo?</span>
                                                                        </span>
                                                                        <div className="input-group">
                                                                            <input type="text" placeholder="Escribir" name="meta_profecional" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando3.meta_profecional} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div className="row">
                                                                <div className="col-xs-12 col-lg-12">
                                                                    <div className="form-group">
                                                                        <span >
                                                                            <span className="font-weight-semibold"> ¿Cuál es la principal fortaleza que posees y que consideres te puede ayudar a tener éxito en una posición comercial?</span>
                                                                        </span>
                                                                        <div className="input-group">
                                                                            <input type="text" placeholder="Escribir" name="fortaleza_posees" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando3.fortaleza_posees} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div className="row">
                                                                <div className="col-xs-12 col-lg-12">
                                                                    <div className="form-group">
                                                                        <span >
                                                                            <span className="font-weight-semibold">Hablando en términos generales, ¿Cuáles piensas que son tus mejores cualidades? ¿Y además de esas?</span>
                                                                        </span>
                                                                        <div className="input-group">
                                                                            <input type="text" placeholder="Escribir" name="mejores_cualidades" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando3.mejores_cualidades} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div className="row">
                                                                <div className="col-xs-6 col-lg-6">
                                                                    <div className="form-group">
                                                                        <span >
                                                                            <span className="font-weight-semibold">¿Qué piensas de los seguros?</span>
                                                                        </span>
                                                                        <div className="input-group">
                                                                            <input type="text" placeholder="Escribir" name="piensas_seguros" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando3.piensas_seguros} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-xs-6 col-lg-6">
                                                                    <div className="form-group">
                                                                        <span >
                                                                            <span className="font-weight-semibold">Continuando con las generalidades ¿En qué áreas considerarías mejorar?</span>
                                                                        </span>
                                                                        <div className="input-group">
                                                                            <input type="text" placeholder="Escribir" name="areas_mejora" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando3.areas_mejora} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="card">
                                                        <div className="card-body">
                                                            <div className="row">

                                                                <div className="col-xs-12 col-lg-6">
                                                                    <div className="form-group">
                                                                        <span >
                                                                            <span className="font-weight-semibold"> ¿Cuándo una persona es exitosa, según tu opinión?</span>
                                                                        </span>
                                                                        <div className="input-group">
                                                                            <input type="text" placeholder="Escribir" name="persona_exitosa" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando3.persona_exitosa} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-xs-12 col-lg-6">
                                                                    <div className="form-group">
                                                                        <span >
                                                                            <span className="font-weight-semibold">¿Cuál es tu principal motivación?</span>
                                                                        </span>
                                                                        <div className="input-group">
                                                                            <input type="text" placeholder="Escribir" name="principal_motivacion" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando3.principal_motivacion} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div className="row">
                                                                <div className="col-xs-12 col-lg-12">
                                                                    <div className="form-group">
                                                                        <span >
                                                                            <span className="font-weight-semibold">¿Qué tipo de actividades disfrutas más en tu tiempo libre? Hobbies</span>
                                                                        </span>
                                                                        <div className="input-group">
                                                                            <input type="text" placeholder="Nombre" name="nombre" className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div className="row">
                                                                <div className="col-xs-12 col-lg-6">
                                                                    <div className="form-group">
                                                                        <span >
                                                                            <span className="font-weight-semibold"> ¿Cuál es tu más grande sueño?</span>
                                                                        </span>
                                                                        <div className="input-group">
                                                                            <input type="text" placeholder="Escribir" name="grande_sueno" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando3.grande_sueno} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-xs-12 col-lg-6">
                                                                    <div className="form-group">
                                                                        <span >
                                                                            <span className="font-weight-semibold">¿Cómo te describiría tu colega más cercano?</span>
                                                                        </span>
                                                                        <div className="input-group">
                                                                            <input type="text" placeholder="Escribir" name="colega_cercano" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando3.colega_cercano} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div className="row">
                                                                <div className="col-xs-12 col-lg-12">
                                                                    <div className="form-group">
                                                                        <span >
                                                                            <span className="font-weight-semibold">¿Si hablamos con tu mejor amigo (a) ¿Cuál dirá que es tu mayor debilidad? (Personalidad)</span>
                                                                        </span>
                                                                        <div className="input-group">
                                                                            <input type="text" placeholder="Escribir" name="mayor_debilidad" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando3.mayor_debilidad} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>}


                                            {/*02. Encuentro */}
                                            {(this.state.muestra_encuentro >= 2 && (this.props.presentarSeccion == "EP2" ||
                                                this.props.presentarSeccion == "EP3" || this.props.presentarSeccion == "EP4" ||
                                                this.props.presentarSeccion == "EP5" || this.props.presentarSeccion == "EP6" ||
                                                this.props.presentarSeccion == "EP7" || this.props.presentarSeccion == "EP8" ||
                                                this.props.presentarSeccion == "EP9" || this.props.presentarSeccion == "EP10" ||
                                                this.props.presentarSeccion == "EP11" || this.props.presentarSeccion == "EP12"
                                                || this.props.presentarSeccion == "EP13" || this.props.presentarSeccion == "EP14")) &&
                                                <div>
                                                    <div className="card-body">
                                                        <div className="row">
                                                            <div className="col-xs-12 col-lg-6">
                                                                <div className="form-group">
                                                                    <span >
                                                                        <span className="font-weight-semibold"> ¿Cuál es tu ocupación actual o la última desempeñada?</span>
                                                                    </span>
                                                                    <div className="input-group">
                                                                        <input type="text" placeholder="Escribir" name="ocupacion_actual" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando2.ocupacion_actual} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xs-12 col-lg-6">
                                                                <div className="form-group">
                                                                    <span >
                                                                        <span className="font-weight-semibold">¿Desde cuándo la desempeñas?</span>
                                                                    </span>
                                                                    <div className="input-group">
                                                                        <input type="text" placeholder="Escribir" name="desde_cuando" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando2.desde_cuando} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="row">
                                                            <div className="col-xs-12 col-lg-6">
                                                                <div className="form-group">
                                                                    <span >
                                                                        <span className="font-weight-semibold">¿Qué tan bien te está yendo en tu trabajo actual?</span>
                                                                    </span>
                                                                    <div className="input-group">
                                                                        <input type="text" placeholder="Escribir" name="bien_trabajo_actual" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando2.bien_trabajo_actual} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xs-12 col-lg-6">
                                                                <div className="form-group">
                                                                    <span >
                                                                        <span className="font-weight-semibold">¿Cuánto estás percibiendo actualmente?</span>
                                                                    </span>
                                                                    <div className="input-group">
                                                                        <input type="number" placeholder="Escribir" name="cuanto_percives" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando2.cuanto_percives} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="row">
                                                            <div className="col-xs-12 col-lg-6">
                                                                <div className="form-group">
                                                                    <span >
                                                                        <span className="font-weight-semibold">¿Qué actividades disfrutas más de tu trabajo y cuáles menos?</span>
                                                                    </span>
                                                                    <div className="input-group">
                                                                        <input type="text" placeholder="Escribir" name="actividades_disfrutas" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando2.actividades_disfrutas} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div className="col-xs-12 col-lg-6">
                                                                <div className="form-group">
                                                                    <span >
                                                                        <span className="font-weight-semibold">¿Cuánto reconocimiento recibes por tu esfuerzo y tus resultados?</span>
                                                                    </span>
                                                                    <div className="input-group">
                                                                        <input type="text" placeholder="Escribir" name="reconocimiento_recibes" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando2.reconocimiento_recibes} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div className="row">
                                                            <div className="col-xs-12 col-lg-6">
                                                                <div className="form-group">
                                                                    <span >
                                                                        <span className="font-weight-semibold">¿Qué te haría considerar un cambio de empleo?</span>
                                                                    </span>
                                                                    <div className="input-group">
                                                                        <input type="text" placeholder="Escribir" name="cambio_empleo" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando2.cambio_empleo} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="card-body">
                                                        <div className="row">
                                                            <div className="col-xs-12 col-lg-6">
                                                                <div className="form-group">
                                                                    <span >
                                                                        <span className="font-weight-semibold"> ¿Qué opinas de las ventas?</span>
                                                                    </span>
                                                                    <div className="input-group">
                                                                        <input type="text" placeholder="Escribir" name="opina_venta" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando2.opina_venta} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xs-12 col-lg-6">
                                                                <div className="form-group">
                                                                    <span >
                                                                        <span className="font-weight-semibold">¿Qué opinas de emprender?</span>
                                                                    </span>
                                                                    <div className="input-group">
                                                                        <input type="text" placeholder="Escribir" name="opina_emprender" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando2.opina_emprender} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="row">
                                                            <div className="col-xs-12 col-lg-6">
                                                                <div className="form-group">
                                                                    <span >
                                                                        <span className="font-weight-semibold">Sobre la base de los valores actuales, ¿Cuánto esperas ganar hoy?</span>
                                                                    </span>
                                                                    <div className="input-group">
                                                                        <input type="number" placeholder="Escribir" name="esperas_ganar_hoy" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando2.esperas_ganar_hoy} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xs-12 col-lg-6">
                                                                <div className="form-group">
                                                                    <span >
                                                                        <span className="font-weight-semibold">¿Cuánto esperas ganar en 5 años?</span>
                                                                    </span>
                                                                    <div className="input-group">
                                                                        <input type="number" placeholder="Escribir" name="esperas_ganar_cinco" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando2.esperas_ganar_cinco} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="row">
                                                            <div className="col-xs-12 col-lg-12">
                                                                <div className="form-group">
                                                                    <span >
                                                                        <span className="font-weight-semibold">Describe una venta que cerraste a pesar de tenerlo todo en tu contra</span>
                                                                    </span>
                                                                    <div className="input-group">
                                                                        <input type="text" placeholder="Escribir" name="describe_venta" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando2.describe_venta} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card">
                                                        <div className="card-body">
                                                            <div className="row">
                                                                <div className="col-xs-12 col-lg-4">
                                                                    <div className="form-group">
                                                                        <div className="input-group">
                                                                            <span className="input-group-prepend"> <span
                                                                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fuente de reclutamiento</span>
                                                                            </span> {this.state.selectFuenteReclutamiento}
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-xs-12 col-lg-4">
                                                                    <div className="form-group">
                                                                        <div className="input-group">
                                                                            <span className="input-group-prepend"> <span
                                                                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Teléfono móvil</span>
                                                                            </span> <input type="number" placeholder="" name="tel_movil" disabled={true} value={this.state.prospecto_explorando1.tel_movil + ""} className="form-control " />
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                                <div className="col-xs-12 col-lg-4">
                                                                    <div className="form-group">
                                                                        <div className="input-group">
                                                                            <span className="input-group-prepend"> <span
                                                                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Edad del prospecto</span>
                                                                            </span> <input type="text" placeholder="Apellido" disabled={true} className="form-control " name="ape_materno" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div className="row">
                                                                <div className="col-xs-12 col-lg-4">
                                                                    <div className="form-group">
                                                                        <div className="input-group">
                                                                            <span className="input-group-prepend"> <span
                                                                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fecha de registro del Iron talent </span>
                                                                            </span> <input type="date" placeholder="" name="" disabled={true} className="form-control " value={"" + this.state.prospecto_explorando1.fec_entrevista_profunda + ""} />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-xs-12 col-lg-4">
                                                                    <div className="form-group">
                                                                        <div className="input-group">
                                                                            <span className="input-group-prepend"> <span
                                                                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Entrevistador</span>
                                                                            </span>{this.state.selectEDAT}
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                                <div className="col-xs-12 col-lg-4">
                                                                    <div className="form-group">
                                                                        <div className="input-group">
                                                                            <span className="input-group-prepend"> <span
                                                                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Carrera</span>
                                                                            </span> <input type="text" placeholder="Escribir" className="form-control " name="carrera" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando1.carrera} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div className="row">
                                                                <div className="col-xs-12 col-lg-4">
                                                                    <div className="form-group">
                                                                        <div className="input-group">
                                                                            <span className="input-group-prepend"> <span
                                                                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Código Postal</span>
                                                                            </span> <input type="text" placeholder="Escribir" name="cp" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando1.cp} className="form-control " />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-xs-12 col-lg-4">
                                                                    <div className="form-group">
                                                                        <div className="input-group">
                                                                            <span className="input-group-prepend"> <span
                                                                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Ciudad</span>
                                                                            </span> {this.state.selectCiudad}
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                                <div className="col-xs-12 col-lg-4">
                                                                    <div className="form-group">
                                                                        <div className="input-group">
                                                                            <span className="input-group-prepend"> <span
                                                                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Alcaldía / Municipio</span>
                                                                            </span> {this.state.selectMunicipio}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div className="row">
                                                                <div className="col-xs-12 col-lg-4">
                                                                    <div className="form-group">
                                                                        <div className="input-group">
                                                                            <span className="input-group-prepend"> <span
                                                                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Colonia</span>
                                                                            </span>  {this.state.selectColonia}
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-xs-12 col-lg-4">
                                                                    <div className="form-group">
                                                                        <div className="input-group">
                                                                            <span className="input-group-prepend"> <span
                                                                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Calle</span>
                                                                            </span> <input type="text" placeholder="Escribir" name="calle" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando1.calle} className="form-control " />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-xs-12 col-lg-4">
                                                                    <div className="form-group">
                                                                        <div className="input-group">
                                                                            <span className="input-group-prepend"> <span
                                                                                className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }}>No. ext.</span>
                                                                            </span>
                                                                            <input type="number" maxlength="10" placeholder="00" name="num_ext" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando1.num_ext} style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " />
                                                                            <span className="input-group-prepend"> <span
                                                                                className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }}>No. int.</span>
                                                                            </span> <input type="number" maxlength="10" placeholder="00" name="num_int" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando1.num_int} style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div className="row">
                                                                <div className="col-xs-12 col-lg-4">
                                                                    <div className="form-group">
                                                                        <div className="input-group">
                                                                            <span className="input-group-prepend"> <span
                                                                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Universidad</span>
                                                                            </span> <input type="text" placeholder="Escribir" name="universidad" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando1.universidad} className="form-control " />
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="col-xs-12 col-lg-4">
                                                                    <div class="form-check form-check-inline">
                                                                        <label class="form-check-label" for="inlineRadio1">¿Cuentas con crédito Infonavit?</label>
                                                                    </div>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" name="infonavit" id="infonavitSi" value={1} disabled={true} onChange={this.onChange} checked={this.state.prospecto_explorando1.infonavit == 1} />
                                                                        <label class="form-check-label" for="infonavitSi">Sí</label>
                                                                    </div>
                                                                    <div class="form-check form-check-inline">
                                                                        <input style={{ backgroundColor: '#8189D4' }} class="form-check-input" type="radio" name="infonavit" id="infonavitNo" value={0} disabled={true} onChange={this.onChange} checked={this.state.prospecto_explorando1.infonavit == 0} />
                                                                        <label class="form-check-label" for="infonavitNo">No</label>
                                                                    </div>
                                                                </div>
                                                                <div className="col-xs-12 col-lg-4">
                                                                    <div className="form-group">
                                                                        <div className="input-group">
                                                                            <span className="input-group-prepend"> <span
                                                                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Credito Monto Infonavit $</span>
                                                                            </span> <input type="text" placeholder="Monto Infonavit $0.00" className="form-control " name="monto_credito" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando1.monto_credito} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>}

                                            {/*01. Encuentro */}
                                            {(this.state.muestra_encuentro >= 1 && (this.props.presentarSeccion == "EP1" ||
                                                this.props.presentarSeccion == "EP2" ||
                                                this.props.presentarSeccion == "EP3" || this.props.presentarSeccion == "EP4" ||
                                                this.props.presentarSeccion == "EP5" || this.props.presentarSeccion == "EP6" ||
                                                this.props.presentarSeccion == "EP7" || this.props.presentarSeccion == "EP8" ||
                                                this.props.presentarSeccion == "EP9" || this.props.presentarSeccion == "EP10" ||
                                                this.props.presentarSeccion == "EP11" || this.props.presentarSeccion == "EP12" ||
                                                this.props.presentarSeccion == "EP13" || this.props.presentarSeccion == "EP14")) &&
                                                <div class="card-body">
                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                    >¿Alguna vez fuiste Agente?</span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="fuiste_agente" id="agenteSiEmpleado" value={1} disabled={true} onChange={this.onChange} checked={this.state.prospecto_explorando1.fuiste_agente == 1} />
                                                                <label class="form-check-label" for="agenteSiEmpleado">
                                                                    Sí, empleado
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="fuiste_agente" id="agenteSiDefinitivo" value={2} disabled={true} onChange={this.onChange} checked={this.state.prospecto_explorando1.fuiste_agente == 2} />
                                                                <label class="form-check-label" for="agenteSiDefinitivo">
                                                                    Sí,definitivo
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="fuiste_agente" id="agenteNo" value={0} disabled={true} onChange={this.onChange} checked={this.state.prospecto_explorando1.fuiste_agente == 0} />
                                                                <label class="form-check-label" for="agenteNo">
                                                                    No
                                                                </label>
                                                            </div>
                                                        </div>

                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                    >¿Tú o algún familiar ha trabajado en grupo BAL?</span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="grupo_bal" id="familiarEnBallSi" value={0} disabled={true} onChange={this.onChange} checked={this.state.prospecto_explorando1.grupo_bal == 1} />
                                                                <label class="form-check-label" for="familiarEnBallSi">
                                                                    Sí
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="grupo_bal" id="familiarEnBallNo" value={1} disabled={true} onChange={this.onChange} checked={this.state.prospecto_explorando1.grupo_bal == 0} />
                                                                <label class="form-check-label" for="familiarEnBallNo">
                                                                    No
                                                                </label>
                                                            </div>
                                                        </div>

                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                    >¿Está trabajando actualmente en Gobierno?</span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="gobierno" id="gobierno2" value={1} disabled={true} onChange={this.onChange} checked={this.state.prospecto_explorando1.gobierno == 1} />
                                                                <label class="form-check-label" for="gobierno2">
                                                                    Sí
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="gobierno" id="gobierno1" value={0} disabled={true} onChange={this.onChange} checked={this.state.prospecto_explorando1.gobierno == 0} />
                                                                <label class="form-check-label" for="gobierno1">
                                                                    No
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr></hr>
                                                    <br></br>

                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                    >¿En qué tipo de vivienda resides?</span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="vivienda_resides" disabled={true} checked={this.state.prospecto_explorando1.vivienda_resides == 0} id="vivienda_resides0" value={0} onChange={this.onChange} />
                                                                <label class="form-check-label" for="vivienda_resides0">
                                                                    Casa propia
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="vivienda_resides" disabled={true} checked={this.state.prospecto_explorando1.vivienda_resides == 1} id="vivienda_resides1" value={1} onChange={this.onChange} />
                                                                <label class="form-check-label" for="vivienda_resides1">
                                                                    Rento casa
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="vivienda_resides" disabled={true} checked={this.state.prospecto_explorando1.vivienda_resides == 2} id="vivienda_resides2" value={2} onChange={this.onChange} />
                                                                <label class="form-check-label" for="vivienda_resides2">
                                                                    Departamento propio
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="vivienda_resides" disabled={true} checked={this.state.prospecto_explorando1.vivienda_resides == 3} id="vivienda_resides3" value={3} onChange={this.onChange} />
                                                                <label class="form-check-label" for="vivienda_resides3">
                                                                    Departamento renta
                                                                </label>
                                                            </div>
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend">
                                                                    </span> <input type="text" placeholder="Otro..." name="vivienda_resides_otro" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando1.vivienda_resides_otro} className="form-control " />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                    >Actualmente, ¿Con quién vives?</span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="conquien_vives" disabled={true} checked={this.state.prospecto_explorando1.conquien_vives == 0} id="conquien_vives0" value={0} onChange={this.onChange} />
                                                                <label class="form-check-label" for="conquien_vives0">
                                                                    Esposo (a)
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="conquien_vives" id="conquien_vives1" disabled={true} checked={this.state.prospecto_explorando1.conquien_vives == 1} value={1} onChange={this.onChange} />
                                                                <label class="form-check-label" for="conquien_vives1">
                                                                    Hijos
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="conquien_vives" id="conquien_vives2" disabled={true} checked={this.state.prospecto_explorando1.conquien_vives == 2} value={2} onChange={this.onChange} />
                                                                <label class="form-check-label" for="conquien_vives2">
                                                                    Papás
                                                                </label>
                                                            </div>
                                                            <br />
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend">
                                                                    </span> <input type="text" placeholder="Otro..." className="form-control" name="conquien_vives_otro" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando1.conquien_vives_otro} />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                    >Generalmente, ¿En qué medio de transporte te trasladas?</span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="medio_transporte" disabled={true} checked={this.state.prospecto_explorando1.medio_transporte == 0} id="medio_transporte0" value={0} onChange={this.onChange} />
                                                                <label class="form-check-label" for="medio_transporte0">
                                                                    Auto propio
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="medio_transporte" disabled={true} checked={this.state.prospecto_explorando1.medio_transporte == 1} id="medio_transporte1" value={1} onChange={this.onChange} />
                                                                <label class="form-check-label" for="medio_transporte1">
                                                                    Transporte público
                                                                </label>
                                                            </div>
                                                            <br /><br />
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend">
                                                                    </span> <input type="text" placeholder="Otro..." className="form-control " name="medio_transporte_otro" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando1.medio_transporte_otro} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <hr></hr>
                                                    <br></br>

                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <span>
                                                                    <span >Si hoy te ganaras 1 millón de pesos,¿Qué harías con ellos?</span>
                                                                </span>
                                                                <div className="input-group">
                                                                    <textarea style={{ height: '150px' }} type="text" placeholder="Apellido" name="ganar_millon" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando1.ganar_millon} className="form-control " />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <span>
                                                                    <span >¿Cómo te podemos encontrar en tus redes sociales?</span>
                                                                </span>
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <span className="input-group-prepend"> <span
                                                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}><i className="icon-facebook"></i></span>
                                                                        </span> <input type="text" placeholder="Apellido" className="form-control" name="facebook" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando1.facebook} />
                                                                    </div>
                                                                </div>
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <span className="input-group-prepend"> <span
                                                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}><i className="icon-linkedin2"></i></span>
                                                                        </span> <input type="text" placeholder="Apellido" className="form-control " name="instagram" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando1.instagram} />
                                                                    </div>
                                                                </div>
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <span className="input-group-prepend"> <span
                                                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}><i className="icon-twitter"></i></span>
                                                                        </span> <input type="text" placeholder="Apellido" className="form-control " name="twitter" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando1.twitter} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <span>
                                                                    <span >Observaciones</span>
                                                                </span>
                                                                <div className="input-group">
                                                                    <textarea style={{ height: '150px' }} type="text" placeholder="Escribir" name="observaciones" disabled={true} onChange={this.onChange} value={this.state.prospecto_explorando1.observaciones} className="form-control " />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>}
                                        </div>
                                    </div>
                                </div>
                            </div></>
                        }
                        {((this.props.presentarSeccion == "REG" || this.props.presentarSeccion == "EP1" || this.props.presentarSeccion == "EP2" || this.props.presentarSeccion == "EP3"
                            || this.props.presentarSeccion == "EP4" || this.props.presentarSeccion == "EP5" || this.props.presentarSeccion == "EP6"
                            || this.props.presentarSeccion == "EP7" || this.props.presentarSeccion == "EP8" || this.props.presentarSeccion == "EP9"
                            || this.props.presentarSeccion == "EP10" || this.props.presentarSeccion == "EP11" || this.props.presentarSeccion == "EP12" ||
                            this.props.presentarSeccion == "EP13" || this.props.presentarSeccion == "EP14") && this.state.estatus_seccion >= 1) &&
                            <>
                                <div class="accordion" id="Prospecto">
                                    <div id="headingProspecto">
                                        <h2 class="mb-0">
                                            <button class="btn " type="button" data-toggle="collapse" data-target="#collapsProspecto" aria-expanded="true" aria-controls="collapsProspecto">
                                                <h6 class="mb-0 font-weight-semibold">Registro de Prospecto </h6>
                                            </button>
                                        </h2>
                                    </div>

                                    <div class="card">
                                        <div id="collapsProspecto" class="collapse " aria-labelledby="headingProspecto" data-parent="#Prospecto">
                                            <div class="card">
                                                <div className="card-body">
                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-2">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <h4 class="card-title py-3 font-weight-bold">Información general</h4>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Nombre (s)</span>
                                                                    </span> <input type="text" placeholder="Nombre" name="nombre" disabled onChange={this.onChange} value={this.state.referido.nombre} className="form-control " />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Apellido paterno</span>
                                                                    </span> <input type="text" placeholder="Apellido" name="ape_paterno" disabled onChange={this.onChange} value={this.state.referido.ape_paterno} className="form-control " />
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Apellido materno</span>
                                                                    </span> <input type="text" placeholder="Apellido" className="form-control " disabled name="ape_materno" onChange={this.onChange} value={this.state.referido.ape_materno} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fecha de nacimiento</span>
                                                                    </span> <input type="date" className="form-control " name="fec_nacimiento" disabled onChange={this.onChange} value={this.state.referido.fec_nacimiento} />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>RFC</span>
                                                                    </span> <input type="text" placeholder="Escribir" className="form-control " disabled name="rfc" onChange={this.onChange} value={this.state.referido.rfc} />
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Estado civil</span>
                                                                    </span> {this.state.selectEstadoCivil}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <form encType="multipart/form" style={{ display: 'none' }}>
                                                                        <input type="file" style={{ display: 'none' }} ref={(ref) => this.cv = ref}></input>
                                                                    </form>
                                                                    <input type="text" style={{ borderColor: '#E1E5F0' }} disabled className="form-control " placeholder="Curriculum Vitae" value={this.state.nombreArchivoCV} />
                                                                    <span className="input-group-prepend">
                                                                        <button type="button" class="btn text-white" disabled onClick={this.onClickBotonArchivoCV} style={{ backgroundColor: this.state.colorBotonSubirArchivoCV }}>
                                                                            <h10 style={{ color: this.state.colorSubirArchivoCV }}>+</h10>
                                                                        </button>

                                                                        {this.state.muestraDocCV == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivo }}>
                                                                            <a href={this.state.referido.doc_cv} target="_blank" rel="noopener noreferrer">
                                                                                <i className="fas fa-eye"
                                                                                    style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Ver"></i>
                                                                            </a>
                                                                        </button>
                                                                            : ''}

                                                                        <span className="input-group-text" style={{ background: this.state.colorSubirArchivoCV, borderColor: this.state.colorSubirArchivoCV, color: this.state.colorTextoSubirArchivoCV }}>Subir archivo</span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Envío de link PSP</span>
                                                                    </span> <input type="date" className="form-control " disabled name="fec_envio_psp" onChange={this.onChange} value={this.state.referido.fec_envio_psp} />
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Correo electrónico</span>
                                                                    </span> <input type="text" placeholder="ejemplo@correo.com" disabled className="form-control" name="correo_electronico" onChange={this.onChange} value={this.state.referido.correo_electronico} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Teléfono móvil</span>
                                                                    </span> <input type="number" placeholder="55 0000 0000" disabled className="form-control " name="tel_movil" onChange={this.onChange} value={this.state.referido.tel_movil} />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Teléfono casa</span>
                                                                    </span> <input type="number" placeholder="10 dígitos" disabled className="form-control" name="tel_casa" onChange={this.onChange} value={this.state.referido.tel_casa} />
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Teléfono oficina</span>
                                                                    </span> <input type="number" placeholder="10 dígitos" disabled className="form-control " name="tel_movil" onChange={this.onChange} value={this.state.referido.tel_movil} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="card-body">
                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-2">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <h4 class="card-title py-3 font-weight-bold">Información complementaría</h4>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fuente de reclutamiento</span>
                                                                    </span> {this.state.selectFuenteReclutamiento}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fuente de reclutamiento para bonos</span>
                                                                    </span> {this.state.selectFuenteReclutamientoBono} {/* mover este state al correcto */}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>EDAT asignado</span>
                                                                    </span> {this.state.selectEDAT}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {this.state.muestraAgente ?
                                                        <div className="row">
                                                            <div className="col-xs-12 col-lg-4">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <span className="input-group-prepend"> <span
                                                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Agente referidor</span>
                                                                        </span> {/*<input type="text" placeholder="Escribir" className="form-control " />*/} {this.state.selectAgenteReferidor}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xs-12 col-lg-4">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <span className="input-group-prepend"> <span
                                                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>CUA referidora para repostar a GNP</span>
                                                                        </span> <input type="text" placeholder="Escribir" className="form-control " name="cua_referidor" disabled={true} onChange={this.onChange} value={this.state.referido.cua_referidor} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xs-12 col-lg-4">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <span className="input-group-prepend"> <span
                                                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Nombre APS</span>
                                                                        </span> <input type="text" placeholder="Escribir" className="form-control " disabled={true} name="nombre_aps" onChange={this.onChange} value={this.state.referido.nombre_aps} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div> : <div className="row">
                                                            <div className="col-xs-12 col-lg-4">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <span className="input-group-prepend"> <span
                                                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Nombre APS</span>
                                                                        </span> <input type="text" placeholder="Escribir" className="form-control " disabled={true} name="nombre_aps" onChange={this.onChange} value={this.state.referido.nombre_aps} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="accordion" id="ProgramacionProfunda">
                                        <div id="headingProgramacionProfunda">
                                            <h2 class="mb-0">
                                                <button class="btn " type="button" data-toggle="collapse" data-target="#collapsProgramacionProfunda" aria-expanded="true" aria-controls="collapsProgramacionProfunda">
                                                    <h6 class="mb-0 font-weight-semibold">Programación de entrevista profunda</h6>
                                                </button>
                                            </h2>
                                        </div>

                                        <div class="card">
                                            <div id="collapsProgramacionProfunda" class="collapse " aria-labelledby="headingProgramacionProfunda" data-parent="#ProgramacionProfunda">
                                                <div class="card">
                                                    <div className="card-body">
                                                        <div className="row">
                                                            <div className="col-xs-12 col-lg-12">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <h4 class="card-title py-3 font-weight-bold">Programación de entrevista profunda</h4>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-xs-12 col-lg-4">
                                                            <div className="form-group">
                                                                <div className="input-group">
                                                                    <span className="input-group-prepend"> <span
                                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fecha alta</span>
                                                                    </span> <input type="date" className="form-control " disabled={true} name="fec_entrevista_profunda" onChange={this.onChange} value={this.state.referido.fec_entrevista_profunda} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div></>
                        }
                    </div>}
            </div>
        )
    }
}