import React, { Component } from "react";
import { findRenderedDOMComponentWithClass } from "react-dom/test-utils";
import { Multiselect } from "multiselect-react-dropdown";
import datos from '../urls/datos.json'

//import MultiSelect from "react-multi-select-component";
//import { CheckBoxSelection, Inject, MultiSelectComponent } from '@syncfusion/ej2-react-dropdowns';

const avatar = require("../imagenes/avatar.png")

export default class ReporteCapacitacion extends Component {

    constructor() {
        super();
        this.onChange = this.onChange.bind(this)
        this.armaPendientes = this.armaPendientes.bind(this)
        this.generaDocumento = this.generaDocumento.bind(this)
        this.guardaReporteCapacitacion = this.guardaReporteCapacitacion.bind(this)
        this.regresarPantalla = this.regresarPantalla.bind(this)
        this.obtenInfoProspecto = this.obtenInfoProspecto.bind(this)
        this.onChangePendientes = this.onChangePendientes.bind(this)
        this.guardaPendientes = this.guardaPendientes.bind(this)
        this.armaPendientesJson = this.armaPendientesJson.bind(this)
        this.onChangeTemas = this.onChangeTemas.bind(this)
        this.onChangeSubTemas = this.onChangeSubTemas.bind(this)
        this.pintaChecksTemas = this.pintaChecksTemas.bind(this)
        this.pintaChecksSubTemas = this.pintaChecksSubTemas.bind(this)
        this.construyeCheckTemas = this.construyeCheckTemas.bind(this)
        this.determinaVerdaderosSubTemas = this.determinaVerdaderosSubTemas.bind(this)
        this.state = {
            filasPendientes: [],
            contenidoJsonPendientes: [],
            segundoFile: false,
            nombreProspecto: '',
            universidad: '',
            carrera: '',
            nombreGDD: '',
            fuenteReferido: '',
            temas: [],
            contenidoJsonCheckTemas: [],
            contenidoJSXTemasDistribucion: [],
            imagenEnCambio: undefined,
            reporteCapacitacion: {
                "id": null,
                "prospecto_id": null,
                "asistencia": null,
                "actitud": null,
                "participacion": null,
                "relacion": null,
                "folleto": null,
                "folleto_obs": null,
                "aviso": null,
                "aviso_obs": null,
                "guion": null,
                "guion_obs": null,
                "comercial": null,
                "comercial_obs": null,
                "mail": null,
                "mail_obs": null,
                "ejercicio": null,
                "ejercicio_obs": null,
                "carta": null,
                "carta_obs": null,
                "roleplay": null,
                "roleplay_obs": null,
                "exposicion": null,
                "exposicion_obs": null,
                "desempenio": null,
                "perspectiva": null,
                "comentarios": null,
                "comentarios_gdd": null
            },
            prospecto:
            {
                "id": null,
                "prospecto_id": null,
                "estatus_id": 12,
                "orientacion_logro_edat": null,
                "orientacion_logro_edat2": null,
                "orientacion_logro_gdd": null,
                "orientacion_logro_capacita": null,
                "orientacion_logro_direccion": null,
                "perceverancia_edat": null,
                "perceverancia_edat2": null,
                "perceverancia_gdd": null,
                "perceverancia_capacita": null,
                "perceverancia_direccion": null,
                "integridad_edat": null,
                "integridad_edat2": null,
                "integridad_gdd": null,
                "integridad_capacita": null,
                "integridad_direccion": null,
                "sentido_comun_edat": null,
                "sentido_comun_edat2": null,
                "sentido_comun_gdd": null,
                "sentido_comun_capacita": null,
                "sentido_comun_direccion": null,
                "energia_edat": null,
                "energia_edat2": null,
                "energia_gdd": null,
                "energia_capacita": null,
                "energia_direccion": null,
                "motivacion_edat": null,
                "motivacion_edat2": null,
                "motivacion_gdd": null,
                "motivacion_capacita": null,
                "motivacion_direccion": null,
                "total_edat": null,
                "total_edat2": null,
                "total_gdd": null,
                "total_capacita": null,
                "total_direccion": null
            },
            prospectoRecuperado:
            {
                "id": null,
                "prospecto_id": null,
                "estatus_id": 12,
                "orientacion_logro_edat": 0,
                "orientacion_logro_edat2": 0,
                "orientacion_logro_gdd": 0,
                "orientacion_logro_capacita": 0,
                "orientacion_logro_direccion": 0,
                "perceverancia_edat": 0,
                "perceverancia_edat2": 0,
                "perceverancia_gdd": 0,
                "perceverancia_capacita": 0,
                "perceverancia_direccion": 0,
                "integridad_edat": 0,
                "integridad_edat2": 0,
                "integridad_gdd": 0,
                "integridad_capacita": 0,
                "integridad_direccion": 0,
                "sentido_comun_edat": 0,
                "sentido_comun_edat2": 0,
                "sentido_comun_gdd": 0,
                "sentido_comun_capacita": 0,
                "sentido_comun_direccion": 0,
                "energia_edat": 0,
                "energia_edat2": 0,
                "energia_gdd": 0,
                "energia_capacita": 0,
                "energia_direccion": 0,
                "motivacion_edat": 0,
                "motivacion_edat2": 0,
                "motivacion_gdd": 0,
                "motivacion_capacita": 0,
                "motivacion_direccion": 0,
                "total_edat": 0,
                "total_edat2": 0,
                "total_gdd": 0,
                "total_capacita": 0,
                "total_direccion": 0
            },
            conexion: {},
            prospecto_ventaCarrera: {},
            reclutaEncuentro03: {},
            vegetableData: [
                { vegetable: 'Cabbage', category: 'Leafy and Salad', id: 'item1' },
                { vegetable: 'Spinach', category: 'Leafy and Salad', id: 'item2' },
                { vegetable: 'Wheat grass', category: 'Leafy and Salad', id: 'item3' },
                { vegetable: 'Yarrow', category: 'Leafy and Salad', id: 'item4' },
                { vegetable: 'Pumpkins', category: 'Leafy and Salad', id: 'item5' },
                { vegetable: 'Chickpea', category: 'Beans', id: 'item6' },
                { vegetable: 'Green bean', category: 'Beans', id: 'item7' },
                { vegetable: 'Horse gram', category: 'Beans', id: 'item8' },
                { vegetable: 'Garlic', category: 'Bulb and Stem', id: 'item9' },
                { vegetable: 'Nopal', category: 'Bulb and Stem', id: 'item10' },
                { vegetable: 'Onion', category: 'Bulb and Stem', id: 'item11' }
            ],
            existe_doc_capacitacion: false
        }

    }

    onChange = e => {

        let reporte = this.state.reporteCapacitacion
        if (e.target.name == "folleto_obs" || e.target.name == "aviso_obs" || e.target.name == "guion_obs" || e.target.name == "comercial_obs" || e.target.name == "mail_obs" || e.target.name == "ejercicio_obs" || e.target.name == "carta_obs" ||
            e.target.name == "roleplay_obs" || e.target.name == "exposicion_obs" || e.target.name == "comentarios" || e.target.name == "comentarios_gdd") {
            reporte["" + e.target.name + ""] = e.target.value
        } else {
            reporte["" + e.target.name + ""] = parseInt(e.target.value)
        }
        console.log("onchange ", e.target.name, e.target.value, e.target.checked)
        this.setState({ reporteCapacitacion: reporte })

    }

    onSelect(event) {
        console.log("Entro");
    }

    onChangeGDD(event) {
        event.stopPropagation();
        event.preventDefault();
        let prospectoGDD = this.state.prospecto;
        console.log("Entro en onChangeGDD");
        if (event.target.value  == "" || (event.target.value >= 1 && event.target.value <= 10)) {
            prospectoGDD["" + event.target.name + ""] = parseInt(event.target.value)
            prospectoGDD.total_gdd = parseInt(prospectoGDD.orientacion_logro_gdd) + parseInt(prospectoGDD.perceverancia_gdd) + parseInt(prospectoGDD.integridad_gdd) + parseInt(prospectoGDD.sentido_comun_gdd) + parseInt(prospectoGDD.energia_gdd) + parseInt(prospectoGDD.motivacion_gdd)
        }
        if (event.target.name == "orientacion_logro_gdd" || event.target.name == "perceverancia_gdd" || event.target.name == "integridad_gdd" || event.target.name == "sentido_comun_gdd" || event.target.name == "energia_gdd" || event.target.name == "motivacion_gdd") {
            let factores_vitales_gdd = event.target.value
            if (event.target.value  == "" || (event.target.value >= 1 && event.target.value <= 10)) {
                prospectoGDD["" + event.target.name + ""] = event.target.value
            }
        }

        this.setState({ prospecto: prospectoGDD })
    }

    onChangeCAP(event) {
        event.stopPropagation();
        event.preventDefault();
        let prospectoLogro = this.state.prospecto;
        console.log("Entro en onChangeCAP");
        if (event.target.value  == "" || (event.target.value >= 1 && event.target.value <= 10)) {
            prospectoLogro["" + event.target.name + ""] = parseInt(event.target.value)
            prospectoLogro.total_capacita = parseInt(prospectoLogro.orientacion_logro_capacita) + parseInt(prospectoLogro.perceverancia_capacita) + parseInt(prospectoLogro.integridad_capacita) + parseInt(prospectoLogro.sentido_comun_capacita) + parseInt(prospectoLogro.energia_capacita) + parseInt(prospectoLogro.motivacion_capacita)
        }
        if (event.target.name == "orientacion_logro_capacita" || event.target.name == "perceverancia_capacita" || event.target.name == "integridad_capacita" || event.target.name == "sentido_comun_capacita" || event.target.name == "energia_capacita" || event.target.name == "motivacion_capacita") {
            let factores_vitales_logro = event.target.value
            if (event.target.value  == "" || (event.target.value >= 1 && event.target.value <= 10)) {
                prospectoLogro["" + event.target.name + ""] = event.target.value
            }
        }

        this.setState({ prospecto: prospectoLogro })
    }

    onChangeSDDC(event) {
        event.stopPropagation();
        event.preventDefault();
        let prospectoLogroDireccion = this.state.prospecto;
        console.log("Entro en onChangeSDDC");
        if (event.target.value  == "" || (event.target.value >= 1 && event.target.value <= 10)) {
            prospectoLogroDireccion["" + event.target.name + ""] = parseInt(event.target.value)
            prospectoLogroDireccion.total_direccion = parseInt(prospectoLogroDireccion.orientacion_logro_direccion) + parseInt(prospectoLogroDireccion.perceverancia_direccion) + parseInt(prospectoLogroDireccion.integridad_direccion) + parseInt(prospectoLogroDireccion.sentido_comun_direccion) + parseInt(prospectoLogroDireccion.energia_direccion) + parseInt(prospectoLogroDireccion.motivacion_direccion)
        }
        if (event.target.name == "orientacion_logro_direccion" || event.target.name == "perceverancia_direccion" || event.target.name == "integridad_direccion" || event.target.name == "sentido_comun_direccion" || event.target.name == "energia_direccion"  || event.target.name == "motivacion_direccion") {
            let factores_vitales_logro_direccion = event.target.value
            if (event.target.value  == "" || (event.target.value >= 1 && event.target.value <= 10)) {
                prospectoLogroDireccion["" + event.target.name + ""] = event.target.value
            }
        }

        this.setState({ prospecto: prospectoLogroDireccion })
    }

    obtenInfoProspecto() {
        //fetch(datos.urlServicePy+"recluta/api_recluta_vprospectos/" + this.props.idProspecto)
        fetch(datos.urlServicePy+"recluta/api_recluta_encuentro01/" + this.props.idProspecto)
            .then(response => response.json())
            .then(arrayContenido => {
                if (arrayContenido.length > 0) {
                    this.setState({
                        nombreProspecto: arrayContenido[0].prospecto,
                        universidad: arrayContenido[0].universidad,
                        carrera: arrayContenido[0].carrera
                    })
                }
            })
        fetch(datos.urlServicePy+"recluta/api_recluta_conexion17/" + this.props.idProspecto)
            .then(response => response.json())
            .then(existeProspecto => {
                let imagenProspecto = existeProspecto[0]
                console.log("Entro en Prospecto");
                if (imagenProspecto.doc_foto != null && imagenProspecto.doc_foto.length > 0) {
                    console.log("existe foto")
                    this.setState({
                        imagenEnCambio: imagenProspecto.doc_foto,
                        urlIdentificacion: imagenProspecto.doc_foto
                    })
                }
                //this.setState({ prospecto: existeProspecto[0] })
            })

        fetch(datos.urlServicePy+"recluta/api_recluta_vitales08/" + this.props.idProspecto)
            .then(response => response.json())
            .then(jsonData => {
                if (jsonData.length > 0) {
                    let jsonProspecto = jsonData[0]
                    jsonProspecto.total_edat=parseInt(jsonProspecto.orientacion_logro_edat) + parseInt(jsonProspecto.perceverancia_edat) + parseInt(jsonProspecto.integridad_edat) + parseInt(jsonProspecto.sentido_comun_edat) + parseInt(jsonProspecto.energia_edat) + parseInt(jsonProspecto.motivacion_edat)
                    jsonProspecto.total_edat2=parseInt(jsonProspecto.orientacion_logro_edat2) + parseInt(jsonProspecto.perceverancia_edat2) + parseInt(jsonProspecto.integridad_edat2) + parseInt(jsonProspecto.sentido_comun_edat2) + parseInt(jsonProspecto.energia_edat2) + parseInt(jsonProspecto.motivacion_edat2)
                    jsonProspecto.total_gdd = parseInt(jsonProspecto.orientacion_logro_gdd) + parseInt(jsonProspecto.perceverancia_gdd) + parseInt(jsonProspecto.integridad_gdd) + parseInt(jsonProspecto.sentido_comun_gdd) + parseInt(jsonProspecto.energia_gdd) + parseInt(jsonProspecto.motivacion_gdd)
                    jsonProspecto.total_capacita = parseInt(jsonProspecto.orientacion_logro_capacita) + parseInt(jsonProspecto.perceverancia_capacita) + parseInt(jsonProspecto.integridad_capacita) + parseInt(jsonProspecto.sentido_comun_capacita) + parseInt(jsonProspecto.energia_capacita) + parseInt(jsonProspecto.motivacion_capacita)
                    jsonProspecto.total_direccion = parseInt(jsonProspecto.orientacion_logro_direccion) + parseInt(jsonProspecto.perceverancia_direccion) + parseInt(jsonProspecto.integridad_direccion) + parseInt(jsonProspecto.sentido_comun_direccion) + parseInt(jsonProspecto.energia_direccion) + parseInt(jsonProspecto.motivacion_direccion)
                    this.setState({ prospecto: jsonProspecto })
                }
            })
        fetch(datos.urlServicePy+"recluta/api_recluta_knockout10/" + this.props.idProspecto)
            .then(response => response.json())
            .then(jsonData => {
                if (jsonData.length > 0) {
                    fetch(datos.urlServicePy+"recluta/api_vcat_gdd/" + jsonData[0].gdd_id)
                        .then(response => response.json())
                        .then(jsonGDD => {
                            for (var i = 0; i < jsonGDD.length; i++) {
                                this.setState({ nombreGDD: jsonGDD[i].empleado })
                            }
                        });
                }

                fetch(datos.urlServicePy+"parametros/api_recluta_prospectos/" + jsonData[0].id)
                    .then(response => response.json())
                    .then(json => {
                        let fila = json.filas[0].fila
                        let columnas = json.columnas
                        let columnasSalida = {}
                        for (var i = 0; i < fila.length; i++) {
                            console.log(fila[i])
                            columnasSalida["" + columnas[i].key + ""] = fila[i].value
                        }
                        let fuente_reclutamiento = columnasSalida.fuente_reclutamiento_id;
                        let agente_referidor = columnasSalida.agente_referidor;
                        if (columnasSalida.fuente_reclutamiento_id == 7) {
                            console.log("Entro en Agente");
                            fetch(datos.urlServicePy+"recluta/api_recluta_vagente_referidor/" + agente_referidor)
                                .then(response => response.json())
                                .then(ageteReferidor => {
                                    console.log("Agente referidor :::" + ageteReferidor);
                                    let fila = ageteReferidor.filas
                                    let nombreAgente = ageteReferidor[0].agente_referidor;
                                    this.setState({ fuenteReferido: nombreAgente })
                                });
                        } else {
                            fetch(datos.urlServicePy+"parametros/api_cat_fuente_reclutamiento/" + fuente_reclutamiento)
                                .then(response => response.json())
                                .then(reclutaProspecto => {
                                    console.log("Recluta Prospecto :::" + reclutaProspecto);
                                    let fila = reclutaProspecto.filas
                                    let nombreReferido = "";
                                    for (var i = 0; i < fila.length; i++) {
                                        if (fila[i].fila[0].value == fuente_reclutamiento) {
                                            nombreReferido = fila[i].fila[1].value
                                        }
                                    }
                                    this.setState({ fuenteReferido: nombreReferido })
                                })
                        }
                    })


            })
        fetch(datos.urlServicePy+"parametros/api_recluta_prospectos/" + this.props.idProspecto)
            .then(response => response.json())
            .then(json => {
                let fila = json.filas[0].fila
                let columnas = json.columnas
                let columnasSalida = {}
                for (var i = 0; i < fila.length; i++) {
                    console.log(fila[i])
                    columnasSalida["" + columnas[i].key + ""] = fila[i].value
                }

                this.setState({ conexion: columnasSalida })
            })
        fetch(datos.urlServicePy+"recluta/api_recluta_ventacarrera16/" + this.props.idProspecto)
            .then(response => response.json())
            .then(jsonVenta => {
                if (jsonVenta.length > 0) {
                    this.setState({ prospecto_ventaCarrera: jsonVenta[0] })
                }
            })
        fetch(datos.urlServicePy+"recluta/api_recluta_encuentro03/" + this.props.idProspecto)
            .then(response => response.json())
            .then(jsonData => {
                if (jsonData.length > 0) {
                    this.setState({ reclutaEncuentro03: jsonData[0] })
                }
            })
    }

    UNSAFE_componentWillMount() {
        if (this.props.tipoPantalla == "conectado") {

        }
        fetch(datos.urlServicePy+"recluta/api_recluta_conexrepcapacitacion/" + this.props.idProspecto)
            .then(response => response.json())
            .then(arrayContenido => {
                if (arrayContenido.length > 0) {
                    this.setState({ reporteCapacitacion: arrayContenido[0] })
                }

                fetch(datos.urlServicePy+"recluta/api_tmp_tema_subtema/" + this.props.idProspecto)
                    .then(response => response.json())
                    .then(temasPersisitidos => {
                        if (temasPersisitidos.length > 0) {

                            fetch(datos.urlServicePy+"recluta_tema/api_cat_curso_ideas/0")
                                .then(response => response.json())
                                .then(temas => {
                                    let contenidoJsonCheckTemas = this.construyeCheckTemas(temas)
                                    for (var i = 0; i < temasPersisitidos.length; i++) {
                                        let temaPersistido = temasPersisitidos[i]
                                        contenidoJsonCheckTemas = this.determinaVerdaderos(temaPersistido.tema, contenidoJsonCheckTemas)
                                    }
                                    console.log("los que segun deerian estar prendidos", contenidoJsonCheckTemas)
                                    this.setState({ temasServicio: temas, contenidoJsonCheckTemas: contenidoJsonCheckTemas })
                                    this.pintaChecksTemas()
                                })

                        } else {
                            fetch(datos.urlServicePy+"recluta_tema/api_cat_curso_ideas/0")
                                .then(response => response.json())
                                .then(temas => {
                                    this.setState({ temasServicio: temas, contenidoJsonCheckTemas: this.construyeCheckTemas(temas) })
                                    this.pintaChecksTemas()
                                })
                        }

                    })



                this.obtenInfoProspecto()


            })

    }

    onChangeTemas = e => {
        let temasState = this.state.contenidoJsonCheckTemas
        console.log(temasState, e.target.name)
        for (var i = 0; i < temasState.length; i++) {
            if (temasState[i].idTema == parseInt(e.target.name)) {

                temasState[i].banderaCheck = !temasState[i].banderaCheck
                if (temasState[i].subTemas.length > 0) {
                    let subTemas = temasState[i].subTemas
                    for (var j = 0; j < subTemas.length; j++) {
                        subTemas[j].banderaCheck = temasState[i].banderaCheck
                    }
                    temasState[i].subTemas = subTemas
                }
                break;
            }
        }
        this.setState({ contenidoJsonCheckTemas: temasState })
        this.pintaChecksTemas()

    }

    onChangeSubTemas = e => {
        let valores = e.target.name
        let separador = valores.split(",")
        let temasState = this.state.contenidoJsonCheckTemas
        for (var i = 0; i < temasState.length; i++) {
            if (temasState[i].idTema == parseInt(separador[0])) {
                let subTemas = temasState[i].subTemas
                let contadorVerdaderos = 0;
                for (var j = 0; j < subTemas.length; j++) {
                    if (subTemas[j].idSubTema == separador[1]) {
                        subTemas[j].banderaCheck = !subTemas[j].banderaCheck
                    }
                    if (subTemas[j].banderaCheck == true) {
                        contadorVerdaderos++;
                    }
                }
                temasState[i].banderaCheck = contadorVerdaderos > 0 ? true : false
                break;
            }
        }
        this.setState({ contenidoJsonCheckTemas: temasState })
        this.pintaChecksTemas()

    }




    pintaChecksTemas() {
        let temas = this.state.contenidoJsonCheckTemas
        let contenidoJSXTemas = []
        let contenidoJSXTemasDistribucion = [];
        const tab = '\u00A0';
        let contador = 0;
        for (var i = 0; i < temas.length; i++) {
            let contenidoJSXSubTemas = this.pintaChecksSubTemas(temas[i])

            contenidoJSXTemas.push(
                <div className="col-xs-6 col-lg-6">
                    <div class="form-check form-check">
                        <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"}  style={{ borderColor: '#FF4874' }} type="checkbox" id={"tema" + temas[i].idTema}
                            checked={temas[i].banderaCheck} onChange={this.onChangeTemas} name={temas[i].idTema} />
                        <label class="form-check-label" id={"tema" + temas[i].idTema}> {temas[i].tituloTema}</label>
                        {contenidoJSXSubTemas}
                    </div>
                </div>
            )
            contador++
            if (contador == 2) {
                contenidoJSXTemasDistribucion.push(
                    <div>
                        <div className="row">
                            {contenidoJSXTemas}
                        </div>
                        <hr></hr>
                    </div>
                )
                contador = 0
                contenidoJSXTemas = []
            }

            if (contador == 1 && i == temas.length - 1) {
                contenidoJSXTemasDistribucion.push(
                    <div>
                        <div className="row">
                            {contenidoJSXTemas}
                        </div>
                        <hr></hr>
                    </div>
                )
            }

        }
        this.setState({ contenidoJSXTemasDistribucion: contenidoJSXTemasDistribucion })
    }

    pintaChecksSubTemas(tema) {
        let subTemas = tema.subTemas
        let contenidoJSXSubTemas = []

        for (var i = 0; i < subTemas.length; i++) {
            contenidoJSXSubTemas.push(
                <div>
                    <>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</><input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} style={{ borderColor: '#FF4874' }} type="checkbox" onChange={this.onChangeSubTemas} id={"subTema" + subTemas[i].idSubTema} name={tema.idTema + "," + subTemas[i].idSubTema} checked={subTemas[i].banderaCheck} />
                    <>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>  <label class="form-check-label" id={"subTema" + subTemas[i].idSubTema}> {subTemas[i].tituloSubTema}</label>

                </div>
            )
        }
        return contenidoJSXSubTemas
    }

    construyeCheckTemas(temas) {
        let contenidoJsonCheckTemas = []
        for (var i = 0; i < temas.length; i++) {
            let jsonTemasCheck = {
                idTema: temas[i].tema[0].idTema,
                tituloTema: temas[i].tema[0].tituloTema,
                banderaCheck: false
            }
            let subTemas = temas[i].tema[0].subtemas
            let contenidoSubTemas = []
            for (var j = 0; j < subTemas.length; j++) {
                let jsonSubTemasCheck = {
                    idSubTema: subTemas[j].idSubTema,
                    tituloSubTema: subTemas[j].tituloSubTema,
                    banderaCheck: false
                }
                contenidoSubTemas.push(jsonSubTemasCheck)
            }
            jsonTemasCheck.subTemas = contenidoSubTemas
            contenidoJsonCheckTemas.push(jsonTemasCheck)
        }
        return contenidoJsonCheckTemas

    }

    determinaVerdaderos(temaPersistido, contenidoJsonCheckTemas) {

        for (var i = 0; i < contenidoJsonCheckTemas.length; i++) {
            let contenidoJsonTema = contenidoJsonCheckTemas[i]
            if (contenidoJsonTema.idTema == temaPersistido[0].idTema) {
                console.log("entrando a la bandera ")
                contenidoJsonCheckTemas[i].banderaCheck = true
                if (temaPersistido[0].subtemas.length > 0) {
                    let subTemasPersisitidos = temaPersistido[0].subtemas
                    for (var j = 0; j < subTemasPersisitidos.length; j++) {
                        contenidoJsonCheckTemas[i].subTemas = this.determinaVerdaderosSubTemas(subTemasPersisitidos[j], contenidoJsonCheckTemas[i].subTemas)
                    }
                }
            }
        }
        return contenidoJsonCheckTemas
    }

    determinaVerdaderosSubTemas(subTemaPersisitido, contenidoJsonSubTemas) {
        for (var i = 0; i < contenidoJsonSubTemas.length; i++) {
            if (subTemaPersisitido.idSubTema == contenidoJsonSubTemas[i].idSubTema) {
                contenidoJsonSubTemas[i].banderaCheck = true
            }
        }
        return contenidoJsonSubTemas
    }

    generaDocumento(tipo) {
        fetch(datos.urlServicePy+"recluta/api_recluta_vitales08/" + this.props.idProspecto)
            .then(response => response.json())
            .then(jsonVitales => {
                if (jsonVitales.length > 0) {
                    let jsonProspecto = jsonVitales[0]
                    jsonProspecto.total_gdd = parseInt(jsonProspecto.orientacion_logro_gdd) + parseInt(jsonProspecto.perceverancia_gdd) + parseInt(jsonProspecto.integridad_gdd) + parseInt(jsonProspecto.sentido_comun_gdd) + parseInt(jsonProspecto.energia_gdd) + parseInt(jsonProspecto.motivacion_gdd)
                    jsonProspecto.total_capacita = parseInt(jsonProspecto.orientacion_logro_capacita) + parseInt(jsonProspecto.perceverancia_capacita) + parseInt(jsonProspecto.integridad_capacita) + parseInt(jsonProspecto.sentido_comun_capacita) + parseInt(jsonProspecto.energia_capacita) + parseInt(jsonProspecto.motivacion_capacita)
                    this.setState({ prospectoRecuperado: jsonProspecto })

                    console.log(document.getElementById("doc_capacitacion").outerHTML)
                    let objetoHTML = document.getElementById("doc_capacitacion")
                    let json = {
                        "id": this.props.idProspecto,
                        "prospecto_id": this.props.idProspecto,
                        "html": objetoHTML.outerHTML,
                        "doc_capacitacion": "capacitacion_" + this.props.idProspecto + ".pdf"
                    }

                    let url = datos.urlServicePy+"recluta/api_doc_conexcapacitacion_pdf/"
                    if (tipo == "nuevo") {
                        url = url + "0"
                    } else {
                        url = url + this.props.idProspecto
                    }

                    const requestOptions = {
                        method: "POST",
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify(json)
                    };

                    fetch(url, requestOptions)
                        .then(response => response.json())
                        .then(data => {
                            console.log("respuesta de armar el doc ", data)
                            if (this.props.tipoPantalla == "conexion") {
                                this.props.mostrarConexionCapacitacion()
                            }
                            if (this.props.tipoPantalla == "conectado") {
                                this.props.mostrarConectado()
                            }
                        })
                }
            })
    }


    guardaPendientes() {
        let temas = this.state.contenidoJsonCheckTemas
        let contenidoJsonPost = []
        for (var i = 0; i < temas.length; i++) {
            if (temas[i].banderaCheck == true) {
                let json = {
                    "prospecto_id": this.props.idProspecto,
                    "tema_id": temas[i].idTema,
                    "estatus": 0
                }
                if (temas[i].subTemas.length > 0) {
                    let subTemas = temas[i].subTemas
                    let concatenacion = ''
                    for (var j = 0; j < subTemas.length; j++) {
                        if (subTemas[j].banderaCheck == true) {
                            if (j == (subTemas.length - 1)) {
                                concatenacion = concatenacion + subTemas[j].idSubTema
                            } else {
                                concatenacion = concatenacion + subTemas[j].idSubTema + '@'
                            }
                        }


                    }
                    json.subtema = concatenacion
                } else {
                    json.subtema = '0'
                }
                contenidoJsonPost.push(json)
            }
        }

        for (var i = 0; i < contenidoJsonPost.length; i++) {
            const requestOptions = {
                method: "POST",
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(contenidoJsonPost[i])
            };
            fetch(datos.urlServicePy+"recluta/api_tmp_tema_subtema/0", requestOptions)
                .then(response => response.json())
                .then(data => {

                })
        }

        console.log("contenido del json post ", contenidoJsonPost)

    }

    regresarPantalla() {
        if (this.props.tipoPantalla == "conexion") {
            this.props.mostrarConexionCapacitacion()
        }
        if (this.props.tipoPantalla == "conectado") {
            this.props.mostrarConectado()
        }
    }

    guardaReporteCapacitacion() {
        // this.guardaPendientes()
        let jsonProspecto = this.state.prospecto;

        //EDAT
        jsonProspecto.orientacion_logro_edat = jsonProspecto.orientacion_logro_edat == "" ? null : jsonProspecto.orientacion_logro_edat
        jsonProspecto.perceverancia_edat = jsonProspecto.perceverancia_edat == "" ? null : jsonProspecto.perceverancia_edat
        jsonProspecto.integridad_edat = jsonProspecto.integridad_edat == "" ? null : jsonProspecto.integridad_edat
        jsonProspecto.sentido_comun_edat = jsonProspecto.sentido_comun_edat == "" ? null : jsonProspecto.sentido_comun_edat
        jsonProspecto.energia_edat = jsonProspecto.energia_edat == "" ? null : jsonProspecto.energia_edat
        jsonProspecto.motivacion_edat = jsonProspecto.motivacion_edat == "" ? null : jsonProspecto.motivacion_edat

        //GERENTE EDAT
        jsonProspecto.orientacion_logro_edat2 = jsonProspecto.orientacion_logro_edat2 == "" ? null : jsonProspecto.orientacion_logro_edat2
        jsonProspecto.perceverancia_edat2 = jsonProspecto.perceverancia_edat2 == "" ? null : jsonProspecto.perceverancia_edat2
        jsonProspecto.integridad_edat2 = jsonProspecto.integridad_edat2 == "" ? null : jsonProspecto.integridad_edat2
        jsonProspecto.sentido_comun_edat2 = jsonProspecto.sentido_comun_edat2 == "" ? null : jsonProspecto.sentido_comun_edat2
        jsonProspecto.energia_edat2 = jsonProspecto.energia_edat2 == "" ? null : jsonProspecto.energia_edat2
        jsonProspecto.motivacion_edat2 = jsonProspecto.motivacion_edat2 == "" ? null : jsonProspecto.motivacion_edat2

        //GDD
        jsonProspecto.orientacion_logro_gdd = jsonProspecto.orientacion_logro_gdd == "" ? null : jsonProspecto.orientacion_logro_gdd
        jsonProspecto.perceverancia_gdd = jsonProspecto.perceverancia_gdd == "" ? null : jsonProspecto.perceverancia_gdd
        jsonProspecto.integridad_gdd = jsonProspecto.integridad_gdd == "" ? null : jsonProspecto.integridad_gdd
        jsonProspecto.sentido_comun_gdd = jsonProspecto.sentido_comun_gdd == "" ? null : jsonProspecto.sentido_comun_gdd
        jsonProspecto.energia_gdd = jsonProspecto.energia_gdd == "" ? null : jsonProspecto.energia_gdd
        jsonProspecto.motivacion_gdd = jsonProspecto.motivacion_gdd == "" ? null : jsonProspecto.motivacion_gdd

        //CAPACITA
        jsonProspecto.orientacion_logro_capacita = jsonProspecto.orientacion_logro_capacita == "" ? null : jsonProspecto.orientacion_logro_capacita
        jsonProspecto.perceverancia_capacita = jsonProspecto.perceverancia_capacita == "" ? null : jsonProspecto.perceverancia_capacita
        jsonProspecto.integridad_capacita = jsonProspecto.integridad_capacita == "" ? null : jsonProspecto.integridad_capacita
        jsonProspecto.sentido_comun_capacita = jsonProspecto.sentido_comun_capacita == "" ? null : jsonProspecto.sentido_comun_capacita
        jsonProspecto.energia_capacita = jsonProspecto.energia_capacita == "" ? null : jsonProspecto.energia_capacita
        jsonProspecto.motivacion_capacita = jsonProspecto.motivacion_capacita == "" ? null : jsonProspecto.motivacion_capacita

        //SUB DIR DESARROLL
        jsonProspecto.orientacion_logro_direccion = jsonProspecto.orientacion_logro_direccion == "" ? null : jsonProspecto.orientacion_logro_direccion
        jsonProspecto.perceverancia_direccion = jsonProspecto.perceverancia_direccion == "" ? null : jsonProspecto.perceverancia_direccion
        jsonProspecto.integridad_direccion = jsonProspecto.integridad_direccion == "" ? null : jsonProspecto.integridad_direccion
        jsonProspecto.sentido_comun_direccion = jsonProspecto.sentido_comun_direccion == "" ? null : jsonProspecto.sentido_comun_direccion
        jsonProspecto.energia_direccion = jsonProspecto.energia_direccion == "" ? null : jsonProspecto.energia_direccion
        jsonProspecto.motivacion_direccion = jsonProspecto.motivacion_direccion == "" ? null : jsonProspecto.motivacion_direccion

        
        let json = this.state.reporteCapacitacion
        if (this.state.reporteCapacitacion.id == null) {
            json.id = this.props.idProspecto
            json.prospecto_id = this.props.idProspecto
            //insertamos primera vez
            const requestOptions = {
                method: "POST",
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(json)
            };
            fetch(datos.urlServicePy+"recluta/api_recluta_conexrepcapacitacion/0", requestOptions)
                .then(response => response.json())
                .then(data => {
                    this.setState({ segundoFile: true })
                    fetch(datos.urlServicePy+"recluta/api_tmp_tema_subtema_limpia/" + this.props.idProspecto)
                        .then(response => response.json())
                        .then(respuesta => {
                            this.guardaPendientes()
                        })
                    this.generaDocumento("nuevo")
                })
        } else {
            json.id = this.props.idProspecto
            json.prospecto_id = this.props.idProspecto
            //actualizamos
            const requestOptions = {
                method: "POST",
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(json)
            };
            fetch(datos.urlServicePy+"recluta/api_recluta_conexrepcapacitacion/" + this.props.idProspecto, requestOptions)
                .then(response => response.json())
                .then(data => { })

            fetch(datos.urlServicePy+"recluta/api_recluta_conexion17/" + this.props.idProspecto)
                .then(response => response.json())
                .then(existeProspecto => {
                    this.setState({ segundoFile: true })
                    fetch(datos.urlServicePy+"recluta/api_tmp_tema_subtema_limpia/" + this.props.idProspecto)
                        .then(response => response.json())
                        .then(respuesta => {
                            this.guardaPendientes()
                        })


                    console.log("existeProspecto ", existeProspecto)

                    fetch(datos.urlServicePy+"recluta/api_doc_conexconvenio_pdf/" + this.props.idProspecto)
                        .then(response => response.json())
                        .then(doc_convenio => {
                            console.log("Entro aquí");
                            console.log("prospecto ::" + this.state.prospecto);
                            if (existeProspecto[0].doc_capacitacion != null && existeProspecto[0].doc_capacitacion.length > 0) {
                                this.generaDocumento("actualiza")
                            } else {
                                this.generaDocumento("nuevo")
                            }
                        });
                })

        }
        let url = datos.urlServicePy+"recluta/api_recluta_vitales08/" + this.props.idProspecto
        const requestOptions = {
            method: "POST",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(jsonProspecto)
        };
        fetch(url, requestOptions)
            .then(response => response.json())
            .then(data => { })
    }

    onChangePendientes = e => {
        let temas = this.state.temas
        let contenidoJsonPendientes = this.state.contenidoJsonPendientes
        for (var i = 0; i < contenidoJsonPendientes.length; i++) {
            if (parseInt(e.target.value) == contenidoJsonPendientes[i].id) {
                contenidoJsonPendientes[i].bandera = !contenidoJsonPendientes[i].bandera

                break;
            }
        }
        this.armaPendientesJson(contenidoJsonPendientes)




        /*if (e.target.checked == true) {
            temas.push({
                prospecto_id: this.props.idProspecto,
                temaspendientes_id: parseInt(e.target.value),
                valor: e.target.checked == true ? 1 : 0
            })
        } else {
            //buscamos 
            for (var i = 0; i < temas.length; i++) {
                if (temas[i].temaspendientes_id == parseInt(e.target.value)) {
                    temas.splice(i, 1);
                    break;
                }
            }
        }

        this.setState({ temas: temas })*/
    }

    armaPendientesJson(pendientes) {


        let contador = 0
        let contenido = []
        let contenidoJsonPendientes = []
        let filasPendientes = []
        for (var i = 0; i < pendientes.length; i++) {

            let jsonPendientes = {
                id: pendientes[i].id,
                descripcion: pendientes[i].descripcion,
                bandera: pendientes[i].bandera
            }
            contenidoJsonPendientes.push(jsonPendientes)

            contenido.push(<div className="col-xs-6 col-lg-6">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" style={{ borderColor: '#FF4874' }} type="checkbox" value={jsonPendientes.id}
                        onChange={this.onChangePendientes} checked={jsonPendientes.bandera} />
                    <label class="form-check-label" >{jsonPendientes.descripcion}</label>
                </div>
            </div>)
            contador++
            if (contador == 2) {
                contador = 0
                filasPendientes.push(
                    <div className="row">
                        {contenido}
                    </div>
                )
                contenido = []
            }

            if (contador == 1 && i == pendientes.length - 1) {
                filasPendientes.push(
                    <div className="row">
                        {contenido}
                    </div>
                )
            }
        }

        this.setState({ filasPendientes: filasPendientes, contenidoJsonPendientes: contenidoJsonPendientes })





    }


    armaPendientes() {

        fetch(datos.urlServicePy+"recluta/api_recluta_conexcat_temaspendientes/0")
            .then(response => response.json())
            .then(pendientes => {
                let contador = 0
                let contenido = []
                let contenidoJsonPendientes = []
                let filasPendientes = []
                for (var i = 0; i < pendientes.length; i++) {

                    let jsonPendientes = {
                        id: pendientes[i].id,
                        descripcion: pendientes[i].descripcion,
                        bandera: false
                    }
                    contenidoJsonPendientes.push(jsonPendientes)

                    contenido.push(<div className="col-xs-6 col-lg-6">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" style={{ borderColor: '#FF4874' }} type="checkbox" name="pendientes" id={"pendiente" + pendientes[i].id} value={pendientes[i].id}
                                onChange={this.onChangePendientes} checked={jsonPendientes.bandera} />
                            <label class="form-check-label" id={"pendiente" + pendientes[i].id}>{pendientes[i].descripcion}</label>
                        </div>
                    </div>)
                    contador++
                    if (contador == 2) {
                        contador = 0
                        filasPendientes.push(
                            <div className="row">
                                {contenido}
                            </div>
                        )
                        contenido = []
                    }

                    if (contador == 1 && i == pendientes.length - 1) {
                        filasPendientes.push(
                            <div className="row">
                                {contenido}
                            </div>
                        )
                    }
                }

                this.setState({ filasPendientes: filasPendientes, contenidoJsonPendientes: contenidoJsonPendientes })

            })



    }


    render() {
        return (
            <div >

                <div class="card" style={{ backgroundColor: "#313A46" }}>

                    <div className="card-body">
                        <div className="row">
                            <div className="col-xs-1 col-lg-1">
                                <span ><a ><i onClick={this.regresarPantalla} style={{ color: 'white' }} className="icon-undo2"></i></a></span>
                            </div>
                            <div className="col-xs-2 col-lg-2">
                                <h6 style={{ color: 'white' }}>Reporte Capacitacion</h6>
                            </div>
                            <div className="col-xs-7 col-lg-7">
                                {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                            </div>
                            <div className="col-xs-2 col-lg-2">
                                <button class="btn " onClick={/*this.props.mostrarConexionCapacitacion*/ this.guardaReporteCapacitacion} type="button" style={{ backgroundColor: '#41B3D1', color: '#FFFFFF' }}
                                    disabled={
                                        this.state.reporteCapacitacion.asistencia == null ||
                                        this.state.reporteCapacitacion.actitud == null ||
                                        this.state.reporteCapacitacion.participacion == null ||
                                        this.state.reporteCapacitacion.relacion == null ||
                                        this.state.reporteCapacitacion.folleto == null ||
                                        this.state.reporteCapacitacion.aviso == null ||
                                        this.state.reporteCapacitacion.guion == null ||
                                        this.state.reporteCapacitacion.comercial == null ||
                                        this.state.reporteCapacitacion.mail == null ||
                                        this.state.reporteCapacitacion.ejercicio == null ||
                                        this.state.reporteCapacitacion.carta == null ||
                                        this.state.reporteCapacitacion.roleplay == null ||
                                        this.state.reporteCapacitacion.exposicion == null ||
                                        this.state.reporteCapacitacion.desempenio == null ||
                                        this.state.reporteCapacitacion.perspectiva == null ||
                                        this.state.prospecto.orientacion_logro_capacita == 0 ||
                                        this.state.prospecto.perceverancia_capacita == 0 ||
                                        this.state.prospecto.integridad_capacita == 0 ||
                                        this.state.prospecto.sentido_comun_capacita == 0 ||
                                        this.state.prospecto.energia_capacita == 0 ||
                                        this.state.prospecto.motivacion_capacita == 0
                                    }
                                >Guardar Cambios Y Generar Documento</button>
                            </div>
                        </div>

                    </div>
                </div>
                <div>
                    <div className="row">
                        <div className="col-xs-6 col-lg-6">
                            <div className="row">
                                <div className="col-xs-2 col-lg-2">
                                    <img src={this.state.imagenEnCambio != undefined ? this.state.imagenEnCambio : avatar} class="img-fluid rounded-circle" width="132px" height="132px" alt="" />
                                </div>

                                <div className="col-xs-10 col-lg-10">
                                    <div className="col-xs-6 col-lg-6">
                                        <h6>Prospecto: <strong >{this.state.conexion.nombre + ' ' + this.state.conexion.ape_paterno + ' ' + (this.state.conexion.ape_materno != null ? this.state.conexion.ape_materno : '')}</strong></h6>
                                    </div>
                                    <div className="col-xs-6 col-lg-6">
                                        <h6>Universidad: <strong>{this.state.universidad}</strong></h6>
                                    </div>
                                    <div className="col-xs-6 col-lg-6">
                                        <h6>Carrera: <strong>{this.state.carrera}</strong></h6>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="col-xs-6 col-lg-6">
                            <div className="row">

                                <div className="col-xs-10 col-lg-10">

                                    <div className="col-xs-6 col-lg-6">
                                        <h6>Gerente de Desarrollo: <strong>{this.state.nombreGDD}</strong></h6>
                                    </div>
                                    <div className="col-xs-6 col-lg-6">
                                        <h6>Fuente referido: <strong>{this.state.fuenteReferido}</strong></h6>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                    <br></br>
                    <hr></hr>

                    <div className="row">
                        <div className="col-xs-6 col-lg-6"> <h6>DESCRIPCIÓN BREVE</h6></div>
                        <div className="col-xs-6 col-lg-6"> <h6>SUEÑO</h6></div>

                    </div>

                    <div className="row">
                        <div className="col-xs-6 col-lg-6">
                            <p>{this.state.prospecto_ventaCarrera.descripcion_edat}</p>
                        </div>
                        <div className="col-xs-6 col-lg-6">
                            <p>{this.state.reclutaEncuentro03.grande_sueno}</p>
                        </div>
                    </div>
                    <br></br>

                    <div className="row">
                        <div className="col-xs-6 col-lg-6">
                            <div className="card">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-xs-6 col-lg-6">
                                            <h5>Asistencia y retardos</h5>
                                        </div>
                                        {this.state.reporteCapacitacion.asistencia == 0 ?
                                            <a href="#" style={{
                                                height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                backgroundColor: '#04C601', borderColor: '#04C601'
                                            }}
                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                    class="letter-icon text-white"></span>
                                            </a> : ''}
                                        {this.state.reporteCapacitacion.asistencia == 1 ?
                                            <a href="#" style={{
                                                height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                backgroundColor: '#FFC300', borderColor: '#FFC300'
                                            }}
                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                    class="letter-icon text-white"></span>
                                            </a> : ''}
                                        {this.state.reporteCapacitacion.asistencia == 2 ?
                                            <a href="#" style={{
                                                height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                backgroundColor: '#C70039', borderColor: '#C70039'
                                            }}
                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                    class="letter-icon text-white"></span>
                                            </a> : ''}
                                    </div>
                                    <div className="row">

                                        <div className="col-xs-10 col-lg-10">

                                            <div className="col-xs-6 col-lg-6">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input " disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} type="radio" name="asistencia" id="asistencia0" value={0} checked={this.state.reporteCapacitacion.asistencia == 0} onChange={this.onChange} />
                                                    <label class="form-check-label" for="asistencia0">Asistió a todas las sesiones puntualmente</label>
                                                </div>
                                            </div>

                                            <div className="col-xs-6 col-lg-6">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} type="radio" name="asistencia" id="asistencia1" value={1} checked={this.state.reporteCapacitacion.asistencia == 1} onChange={this.onChange} />
                                                    <label class="form-check-label" for="asistencia1">Tuvo 1 falta y/o retardo</label>
                                                </div>
                                            </div>

                                            <div className="col-xs-6 col-lg-6">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} type="radio" name="asistencia" id="asistencia2" value={2} checked={this.state.reporteCapacitacion.asistencia == 2} onChange={this.onChange} />
                                                    <label class="form-check-label" for="asistencia2">No respetó asistencia y horario</label>
                                                </div>
                                            </div>
                                        </div>
                                        {
                                            this.state.reporteCapacitacion.asistencia == null ?
                                                <span style={{ color: "red" }}>Asistencia y retardos, debe ser seleccionada</span> : ''
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="col-xs-6 col-lg-6">
                            <div className="card">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-xs-6 col-lg-6">
                                            <h5>Actitud</h5>
                                        </div>
                                        {this.state.reporteCapacitacion.actitud == 0 ?
                                            <a href="#" style={{
                                                height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                backgroundColor: '#04C601', borderColor: '#04C601'
                                            }}
                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                    class="letter-icon text-white"></span>
                                            </a> : ''}
                                        {this.state.reporteCapacitacion.actitud == 1 ?
                                            <a href="#" style={{
                                                height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                backgroundColor: '#FFC300', borderColor: '#FFC300'
                                            }}
                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                    class="letter-icon text-white"></span>
                                            </a> : ''}
                                        {this.state.reporteCapacitacion.actitud == 2 ?
                                            <a href="#" style={{
                                                height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                backgroundColor: '#C70039', borderColor: '#C70039'
                                            }}
                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                    class="letter-icon text-white"></span>
                                            </a> : ''}
                                    </div>

                                    <div className="row">

                                        <div className="col-xs-10 col-lg-10">

                                            <div className="col-xs-6 col-lg-6">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input " disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} type="radio" name="actitud" id="actitud0" value={0} checked={this.state.reporteCapacitacion.actitud == 0} onChange={this.onChange} />
                                                    <label class="form-check-label" for="actitud0">Excelente</label>
                                                </div>
                                            </div>

                                            <div className="col-xs-6 col-lg-6">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} type="radio" name="actitud" id="actitud1" value={1} checked={this.state.reporteCapacitacion.actitud == 1} onChange={this.onChange} />
                                                    <label class="form-check-label" for="actitud1">Buena</label>
                                                </div>
                                            </div>

                                            <div className="col-xs-6 col-lg-6">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} type="radio" name="actitud" id="actitud2" value={2} checked={this.state.reporteCapacitacion.actitud == 2} onChange={this.onChange} />
                                                    <label class="form-check-label" for="actitud2">Deficiente o mala</label>
                                                </div>
                                            </div>
                                        </div>
                                        {
                                            this.state.reporteCapacitacion.actitud == null ?
                                                <span style={{ color: "red" }}>Actitud, debe ser seleccionada</span> : ''
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br></br>

                    <div className="row">
                        <div className="col-xs-6 col-lg-6">
                            <div className="card">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-xs-6 col-lg-6">
                                            <h5>Participación</h5>
                                        </div>
                                        {this.state.reporteCapacitacion.participacion == 0 ?
                                            <a href="#" style={{
                                                height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                backgroundColor: '#04C601', borderColor: '#04C601'
                                            }}
                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                    class="letter-icon text-white"></span>
                                            </a> : ''}
                                        {this.state.reporteCapacitacion.participacion == 1 ?
                                            <a href="#" style={{
                                                height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                backgroundColor: '#FFC300', borderColor: '#FFC300'
                                            }}
                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                    class="letter-icon text-white"></span>
                                            </a> : ''}
                                        {this.state.reporteCapacitacion.participacion == 2 ?
                                            <a href="#" style={{
                                                height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                backgroundColor: '#C70039', borderColsor: '#C70039'
                                            }}
                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                    class="letter-icon text-white"></span>
                                            </a> : ''}
                                    </div>

                                    <div className="row">

                                        <div className="col-xs-10 col-lg-10">

                                            <div className="col-xs-6 col-lg-6">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input " disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} type="radio" name="participacion" id="participacion0" value={0} checked={this.state.reporteCapacitacion.participacion == 0} onChange={this.onChange} />
                                                    <label class="form-check-label" for="participacion0">Alta</label>
                                                </div>
                                            </div>

                                            <div className="col-xs-6 col-lg-6">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} type="radio" name="participacion" id="participacion1" value={1} checked={this.state.reporteCapacitacion.participacion == 1} onChange={this.onChange} />
                                                    <label class="form-check-label" for="participacion1">Media</label>
                                                </div>
                                            </div>

                                            <div className="col-xs-6 col-lg-6">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} type="radio" name="participacion" id="participacion2" value={2} checked={this.state.reporteCapacitacion.participacion == 2} onChange={this.onChange} />
                                                    <label class="form-check-label" for="participacion2">Ninguna</label>
                                                </div>
                                            </div>
                                        </div>
                                        {
                                            this.state.reporteCapacitacion.participacion == null ?
                                                <span style={{ color: "red" }}>Participación, debe ser seleccionada</span> : ''
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="col-xs-6 col-lg-6">
                            <div className="card">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-xs-6 col-lg-6">
                                            <h5>Relación con el grupo</h5>
                                        </div>
                                        {this.state.reporteCapacitacion.relacion == 0 ?
                                            <a href="#" style={{
                                                height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                backgroundColor: '#04C601', borderColor: '#04C601'
                                            }}
                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                    class="letter-icon text-white"></span>
                                            </a> : ''}
                                        {this.state.reporteCapacitacion.relacion == 1 ?
                                            <a href="#" style={{
                                                height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                backgroundColor: '#FFC300', borderColor: '#FFC300'
                                            }}
                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                    class="letter-icon text-white"></span>
                                            </a> : ''}
                                        {this.state.reporteCapacitacion.relacion == 2 ?
                                            <a href="#" style={{
                                                height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                backgroundColor: '#C70039', borderColor: '#C70039'
                                            }}
                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                    class="letter-icon text-white"></span>
                                            </a> : ''}
                                    </div>

                                    <div className="row">

                                        <div className="col-xs-10 col-lg-10">

                                            <div className="col-xs-6 col-lg-6">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input " disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} type="radio" name="relacion" id="relacion0" value={0} checked={this.state.reporteCapacitacion.relacion == 0} onChange={this.onChange} />
                                                    <label class="form-check-label" for="relacion0">Alta</label>
                                                </div>
                                            </div>

                                            <div className="col-xs-6 col-lg-6">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} type="radio" name="relacion" id="relacion1" value={1} checked={this.state.reporteCapacitacion.relacion == 1} onChange={this.onChange} />
                                                    <label class="form-check-label" for="relacion1">Media</label>
                                                </div>
                                            </div>

                                            <div className="col-xs-6 col-lg-6">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} type="radio" name="relacion" id="relacion2" value={2} checked={this.state.reporteCapacitacion.relacion == 2} onChange={this.onChange} />
                                                    <label class="form-check-label" for="relacion2">Baja</label>
                                                </div>
                                            </div>
                                        </div>
                                        {
                                            this.state.reporteCapacitacion.relacion == null ?
                                                <span style={{ color: "red" }}>Relación con el grupo, debe ser seleccionada</span> : ''
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <hr />
                    <br></br>
                    <div className="row">
                        <div className="col-xs-6 col-lg-6"> <h6>Temas pendientes por revisar</h6></div>
                    </div>
                    { /* <div className="arrowdown" style={{ width: '500px' }}>
                        <Multiselect styles = {{padding : '50px'}} onSelect={this.onSelect.bind(this)} showArrow = {true} options={this.state.vegetableData} displayValue="vegetable" groupBy="category" showCheckbox={true}/>
                    </div>*/}
                    <div className="row">
                        <div className="col-xs-6 col-lg-6"> <h6></h6></div>
                    </div>
                    {this.state.contenidoJSXTemasDistribucion}


                    <br>
                    </br>
                    <div className="row">
                        <div className="col-xs-6 col-lg-6"> <h6>Relación de entrega de tareas</h6></div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12 col-lg-12">
                            <div className="card">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-xs-2 col-lg-2"> <h8>Folleto</h8></div>
                                        <div className="col-xs-2 col-lg-2">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} style={{ borderColor: '#FF4874' }} type="radio" name="folleto" id="folleto0" value={0} checked={this.state.reporteCapacitacion.folleto == 0} onChange={this.onChange} />
                                                <label class="form-check-label" for="folleto0">Si</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} style={{ borderColor: '#FF4874' }} type="radio" name="folleto" id="folleto1" value={1} checked={this.state.reporteCapacitacion.folleto == 1} onChange={this.onChange} />
                                                <label class="form-check-label" for="folleto1">No</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} style={{ borderColor: '#FF4874' }} type="radio" name="folleto" id="folleto2" value={2} checked={this.state.reporteCapacitacion.folleto == 2} onChange={this.onChange} />
                                                <label class="form-check-label" for="folleto2">N/A</label>
                                            </div>
                                        </div>
                                        <div className="col-xs-8 col-lg-8">
                                            <input type="text" placeholder="Observaciones" name="folleto_obs" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} value={this.state.reporteCapacitacion.folleto_obs} onChange={this.onChange} className="form-control " />
                                        </div>
                                    </div>
                                    {
                                        this.state.reporteCapacitacion.folleto == null ?
                                            <span style={{ color: "red" }}>Folleto, debe ser seleccionada</span> : ''
                                    }
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-xs-12 col-lg-12">
                            <div className="card">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-xs-2 col-lg-2"> <h8>Aviso de privacidad</h8></div>
                                        <div className="col-xs-2 col-lg-2">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} style={{ borderColor: '#FF4874' }} type="radio" name="aviso" id="aviso0" value={0} checked={this.state.reporteCapacitacion.aviso == 0} onChange={this.onChange} />
                                                <label class="form-check-label" for="aviso0">Si</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} style={{ borderColor: '#FF4874' }} type="radio" name="aviso" id="aviso1" value={1} checked={this.state.reporteCapacitacion.aviso == 1} onChange={this.onChange} />
                                                <label class="form-check-label" for="aviso1">No</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} style={{ borderColor: '#FF4874' }} type="radio" name="aviso" id="aviso2" value={2} checked={this.state.reporteCapacitacion.aviso == 2} onChange={this.onChange} />
                                                <label class="form-check-label" for="aviso2">N/A</label>
                                            </div>
                                        </div>
                                        <div className="col-xs-8 col-lg-8">
                                            <input type="text" placeholder="Observaciones" name="aviso_obs" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} value={this.state.reporteCapacitacion.aviso_obs} onChange={this.onChange} className="form-control " />

                                        </div>
                                    </div>
                                    {
                                        this.state.reporteCapacitacion.aviso == null ?
                                            <span style={{ color: "red" }}>Aviso de privacidad, debe ser seleccionada</span> : ''
                                    }
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-xs-12 col-lg-12">
                            <div className="card">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-xs-2 col-lg-2"> <h8>Guión telefónico</h8></div>
                                        <div className="col-xs-2 col-lg-2">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} style={{ borderColor: '#FF4874' }} type="radio" name="guion" id="guion0" value={0} checked={this.state.reporteCapacitacion.guion == 0} onChange={this.onChange} />
                                                <label class="form-check-label" for="guion0">Si</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} style={{ borderColor: '#FF4874' }} type="radio" name="guion" id="guion1" value={1} checked={this.state.reporteCapacitacion.guion == 1} onChange={this.onChange} />
                                                <label class="form-check-label" for="guion1">No</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} style={{ borderColor: '#FF4874' }} type="radio" name="guion" id="guion2" value={2} checked={this.state.reporteCapacitacion.guion == 2} onChange={this.onChange} />
                                                <label class="form-check-label" for="guion2">N/A</label>
                                            </div>
                                        </div>
                                        <div className="col-xs-8 col-lg-8">
                                            <input type="text" placeholder="Observaciones" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} name="guion_obs" value={this.state.reporteCapacitacion.guion_obs} onChange={this.onChange} className="form-control " />
                                        </div>
                                    </div>
                                    {
                                        this.state.reporteCapacitacion.guion == null ?
                                            <span style={{ color: "red" }}>Guión telefónico, debe ser seleccionada</span> : ''
                                    }
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-xs-12 col-lg-12">
                            <div className="card">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-xs-2 col-lg-2"> <h8>Comercial</h8></div>
                                        <div className="col-xs-2 col-lg-2">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} style={{ borderColor: '#FF4874' }} type="radio" name="comercial" id="comercial0" value={0} checked={this.state.reporteCapacitacion.comercial == 0} onChange={this.onChange} />
                                                <label class="form-check-label" for="comercial0">Si</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} style={{ borderColor: '#FF4874' }} type="radio" name="comercial" id="comercial1" value={1} checked={this.state.reporteCapacitacion.comercial == 1} onChange={this.onChange} />
                                                <label class="form-check-label" for="comercial1">No</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} style={{ borderColor: '#FF4874' }} type="radio" name="comercial" id="comercial2" value={2} checked={this.state.reporteCapacitacion.comercial == 2} onChange={this.onChange} />
                                                <label class="form-check-label" for="comercial2">N/A</label>
                                            </div>
                                        </div>
                                        <div className="col-xs-8 col-lg-8">
                                            <input type="text" placeholder="Observaciones" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} name="comercial_obs" value={this.state.reporteCapacitacion.comercial_obs} onChange={this.onChange} className="form-control " />
                                        </div>
                                    </div>
                                    {
                                        this.state.reporteCapacitacion.comercial == null ?
                                            <span style={{ color: "red" }}>Comercial, debe ser seleccionada</span> : ''
                                    }
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-xs-12 col-lg-12">
                            <div className="card">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-xs-2 col-lg-2"> <h8>Mail entre citas</h8></div>
                                        <div className="col-xs-2 col-lg-2">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} style={{ borderColor: '#FF4874' }} type="radio" name="mail" id="mail0" value={0} checked={this.state.reporteCapacitacion.mail == 0} onChange={this.onChange} />
                                                <label class="form-check-label" for="mail0">Si</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} style={{ borderColor: '#FF4874' }} type="radio" name="mail" id="mail1" value={1} checked={this.state.reporteCapacitacion.mail == 1} onChange={this.onChange} />
                                                <label class="form-check-label" for="mail1">No</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} style={{ borderColor: '#FF4874' }} type="radio" name="mail" id="mail2" value={2} checked={this.state.reporteCapacitacion.mail == 2} onChange={this.onChange} />
                                                <label class="form-check-label" for="mail2">N/A</label>
                                            </div>
                                        </div>
                                        <div className="col-xs-8 col-lg-8">
                                            <input type="text" placeholder="Observaciones" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} name="mail_obs" value={this.state.reporteCapacitacion.mail_obs} onChange={this.onChange} className="form-control " />
                                        </div>
                                    </div>
                                    {
                                        this.state.reporteCapacitacion.mail == null ?
                                            <span style={{ color: "red" }}>Mail entre citas, debe ser seleccionada</span> : ''
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12 col-lg-12">
                            <div className="card">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-xs-2 col-lg-2"> <h8>Ejercicio en Nautilus</h8></div>
                                        <div className="col-xs-2 col-lg-2">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} style={{ borderColor: '#FF4874' }} type="radio" name="ejercicio" id="ejercicio0" value={0} checked={this.state.reporteCapacitacion.ejercicio == 0} onChange={this.onChange} />
                                                <label class="form-check-label" for="ejercicio0">Si</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} style={{ borderColor: '#FF4874' }} type="radio" name="ejercicio" id="ejercicio1" value={1} checked={this.state.reporteCapacitacion.ejercicio == 1} onChange={this.onChange} />
                                                <label class="form-check-label" for="ejercicio1">No</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} style={{ borderColor: '#FF4874' }} type="radio" name="ejercicio" id="ejercicio2" value={2} checked={this.state.reporteCapacitacion.ejercicio == 2} onChange={this.onChange} />
                                                <label class="form-check-label" for="ejercicio2">N/A</label>
                                            </div>
                                        </div>
                                        <div className="col-xs-8 col-lg-8">
                                            <input type="text" placeholder="Observaciones" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} name="ejercicio_obs" value={this.state.reporteCapacitacion.ejercicio_obs} onChange={this.onChange} className="form-control " />
                                        </div>
                                    </div>
                                    {
                                        this.state.reporteCapacitacion.ejercicio == null ?
                                            <span style={{ color: "red" }}>Ejercicio en Nautilus, debe ser seleccionada</span> : ''
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12 col-lg-12">
                            <div className="card">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-xs-2 col-lg-2"> <h8>Carta propuesta</h8></div>
                                        <div className="col-xs-2 col-lg-2">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} style={{ borderColor: '#FF4874' }} type="radio" name="carta" id="carta0" value={0} checked={this.state.reporteCapacitacion.carta == 0} onChange={this.onChange} />
                                                <label class="form-check-label" for="carta0">Si</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} style={{ borderColor: '#FF4874' }} type="radio" name="carta" id="carta1" value={1} checked={this.state.reporteCapacitacion.carta == 1} onChange={this.onChange} />
                                                <label class="form-check-label" for="carta1">No</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} style={{ borderColor: '#FF4874' }} type="radio" name="carta" id="carta2" value={2} checked={this.state.reporteCapacitacion.carta == 2} onChange={this.onChange} />
                                                <label class="form-check-label" for="carta2">N/A</label>
                                            </div>
                                        </div>
                                        <div className="col-xs-8 col-lg-8">
                                            <input type="text" placeholder="Observaciones" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} name="carta_obs" value={this.state.reporteCapacitacion.carta_obs} onChange={this.onChange} className="form-control " />
                                        </div>
                                    </div>
                                    {
                                        this.state.reporteCapacitacion.carta == null ?
                                            <span style={{ color: "red" }}>Carta propuesta, debe ser seleccionada</span> : ''
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12 col-lg-12">
                            <div className="card">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-xs-2 col-lg-2"> <h8>Roleplay de segunda entrevista</h8></div>
                                        <div className="col-xs-2 col-lg-2">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} style={{ borderColor: '#FF4874' }} type="radio" name="roleplay" id="roleplay0" value={0} checked={this.state.reporteCapacitacion.roleplay == 0} onChange={this.onChange} />
                                                <label class="form-check-label" for="roleplay0">Si</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} style={{ borderColor: '#FF4874' }} type="radio" name="roleplay" id="roleplay1" value={1} checked={this.state.reporteCapacitacion.roleplay == 1} onChange={this.onChange} />
                                                <label class="form-check-label" for="roleplay1">No</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} style={{ borderColor: '#FF4874' }} type="radio" name="roleplay" id="roleplay2" value={2} checked={this.state.reporteCapacitacion.roleplay == 2} onChange={this.onChange} />
                                                <label class="form-check-label" for="roleplay2">N/A</label>
                                            </div>
                                        </div>
                                        <div className="col-xs-8 col-lg-8">
                                            <input type="text" placeholder="Observaciones" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} name="roleplay_obs" value={this.state.reporteCapacitacion.roleplay_obs} onChange={this.onChange} className="form-control " />
                                        </div>
                                    </div>
                                    {
                                        this.state.reporteCapacitacion.roleplay == null ?
                                            <span style={{ color: "red" }}>Roleplay llamada, debe ser seleccionada</span> : ''
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12 col-lg-12">
                            <div className="card">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-xs-2 col-lg-2"> <h8>Exposición</h8></div>
                                        <div className="col-xs-2 col-lg-2">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} style={{ borderColor: '#FF4874' }} type="radio" name="exposicion" id="exposicion0" value={0} checked={this.state.reporteCapacitacion.exposicion == 0} onChange={this.onChange} />
                                                <label class="form-check-label" for="exposicion0">Si</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} style={{ borderColor: '#FF4874' }} type="radio" name="exposicion" id="exposicion1" value={1} checked={this.state.reporteCapacitacion.exposicion == 1} onChange={this.onChange} />
                                                <label class="form-check-label" for="exposicion1">No</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} style={{ borderColor: '#FF4874' }} type="radio" name="exposicion" id="exposicion2" value={2} checked={this.state.reporteCapacitacion.exposicion == 2} onChange={this.onChange} />
                                                <label class="form-check-label" for="exposicion2">N/A</label>
                                            </div>
                                        </div>
                                        <div className="col-xs-8 col-lg-8">
                                            <input type="text" placeholder="Observaciones" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} name="exposicion_obs" value={this.state.reporteCapacitacion.exposicion_obs} onChange={this.onChange} className="form-control " />
                                        </div>
                                    </div>
                                    {
                                        this.state.reporteCapacitacion.exposicion == null ?
                                            <span style={{ color: "red" }}>Exposición, debe ser seleccionada</span> : ''
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr></hr>
                    <br></br>

                    <div className="row">
                        <div className="col-xs-6 col-lg-6"> <h6>Factores Vitales</h6></div>
                    </div>
                    <div class="card">
                        <div class="card" >
                            <div class="card-header text-center">
                                <div className="row">
                                    <div className="col-xs-2 col-lg-2">
                                        <h6 class="card-title"></h6>
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        <h6 class="card-title"><strong>EDAT</strong></h6>
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        <h6 class="card-title"><strong>Gerente EDAT</strong></h6>
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        <h6 class="card-title"><strong>GDD</strong></h6>
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        <h6 class="card-title"><strong>Capacitación</strong></h6>
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        <h6 class="card-title"><strong>SDDC</strong></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div className="card-body text-center">
                                <div className="row">
                                    <div className="col-xs-2 col-lg-2">
                                        <h8 class="mb-0 font-weight-semibold">Orientación al logros</h8>
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="orientacion_logro_edat" disabled={true} value={this.state.prospecto.orientacion_logro_edat} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>

                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="orientacion_logro_edat2" disabled={true} value={this.state.prospecto.orientacion_logro_edat2} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="orientacion_logro_gdd" onChange={this.onChangeGDD.bind(this)} disabled={this.props.tipoUsuario != "GDD" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.orientacion_logro_gdd} style={this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" ? { borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' } : { backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>

                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="orientacion_logro_capacita" onChange={this.onChangeCAP.bind(this)} disabled={this.props.tipoUsuario != "CAP" && this.props.tipoUsuario != "GTECAP" && this.props.tipoUsuario != "ASESJRCAP" && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.orientacion_logro_capacita} style={this.props.tipoUsuario == "CAP" || this.props.tipoUsuario == "ASESJRCAP" ? { borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' } : { backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="orientacion_logro_direccion" onChange={this.onChangeSDDC.bind(this)} disabled={this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.orientacion_logro_direccion} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-xs-2 col-lg-2"></div>
                                    <div className="col-xs-2 col-lg-2"></div>
                                    <div className="col-xs-2 col-lg-2"></div>
                                    <div className="col-xs-2 col-lg-2">
                                        {
                                            (this.state.prospecto.orientacion_logro_gdd == 0) ?
                                                <span style={{ color: "red" }}>El campo Orientación al logros (GDD) debe ser ingresado</span> : ''
                                        }
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        {
                                            (this.state.prospecto.orientacion_logro_capacita == 0) ?
                                                <span style={{ color: "red" }}>El campo Orientación al logros (Capacitación) debe ser ingresado</span> : ''
                                        }
                                    </div>
                                    <div className="col-xs-2 col-lg-2"></div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div className="card-body  text-center">
                                <div className="row">
                                    <div className="col-xs-2 col-lg-2">
                                        <h8 class="mb-0 font-weight-semibold">Perseverancia</h8>
                                    </div>

                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="perceverancia_edat" disabled={true} onChange={this.onChange} value={this.state.prospecto.perceverancia_edat} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>

                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="perceverancia_edat2" disabled={true} value={this.state.prospecto.perceverancia_edat2} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="perceverancia_gdd" onChange={this.onChangeGDD.bind(this)} disabled={this.props.tipoUsuario != "GDD" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.perceverancia_gdd} style={this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" ? { borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' } : { backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="perceverancia_capacita" onChange={this.onChangeCAP.bind(this)} disabled={this.props.tipoUsuario != "CAP" && this.props.tipoUsuario != "GTECAP" && this.props.tipoUsuario != "ASESJRCAP" && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.perceverancia_capacita} style={this.props.tipoUsuario == "CAP" || this.props.tipoUsuario == "ASESJRCAP" ? { borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' } : { backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="perceverancia_direccion" onChange={this.onChangeSDDC.bind(this)} disabled={this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"}  value={this.state.prospecto.perceverancia_direccion} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-xs-2 col-lg-2"></div>
                                    <div className="col-xs-2 col-lg-2"></div>
                                    <div className="col-xs-2 col-lg-2"></div>
                                    <div className="col-xs-2 col-lg-2">
                                        {
                                            (this.state.prospecto.perceverancia_gdd == 0) ?
                                                <span style={{ color: "red" }}>El campo Orientación al logro (GDD) debe ser ingresado</span> : ''
                                        }
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        {
                                            (this.state.prospecto.perceverancia_capacita == 0) ?
                                                <span style={{ color: "red" }}>El campo Perseverancia (Capacitación) debe ser ingresado</span> : ''
                                        }
                                    </div>
                                    <div className="col-xs-2 col-lg-2"></div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div className="card-body  text-center">
                                <div className="row">
                                    <div className="col-xs-2 col-lg-2">
                                        <h8 class="mb-0 font-weight-semibold">Carácter e integridad</h8>
                                    </div>

                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="integridad_edat" disabled={true} onChange={this.onChange} value={this.state.prospecto.integridad_edat} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>

                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="integridad_edat2" disabled={true} value={this.state.prospecto.integridad_edat2} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="integridad_gdd" onChange={this.onChangeGDD.bind(this)} disabled={this.props.tipoUsuario != "GDD" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.integridad_gdd} style={this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" ? { borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' } : { backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="integridad_capacita" onChange={this.onChangeCAP.bind(this)} disabled={this.props.tipoUsuario != "CAP" && this.props.tipoUsuario != "GTECAP" && this.props.tipoUsuario != "ASESJRCAP" && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.integridad_capacita} style={this.props.tipoUsuario == "CAP" || this.props.tipoUsuario == "ASESJRCAP" ? { borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' } : { backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="integridad_direccion" onChange={this.onChangeSDDC.bind(this)} disabled={this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"}  value={this.state.prospecto.integridad_direccion} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-xs-2 col-lg-2"></div>
                                    <div className="col-xs-2 col-lg-2"></div>
                                    <div className="col-xs-2 col-lg-2"></div>
                                    <div className="col-xs-2 col-lg-2">
                                        {
                                            (this.state.prospecto.integridad_gdd == 0) ?
                                                <span style={{ color: "red" }}>El campo Orientación al logros (GDD) debe ser ingresado</span> : ''
                                        }
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        {
                                            (this.state.prospecto.integridad_capacita == 0) ?
                                                <span style={{ color: "red" }}>El campo Carácter e integridad (Capacitación) debe ser ingresado</span> : ''
                                        }
                                    </div>
                                    <div className="col-xs-2 col-lg-2"></div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div className="card-body  text-center">
                                <div className="row">
                                    <div className="col-xs-2 col-lg-2">
                                        <h8 class="mb-0 font-weight-semibold">Sentido común</h8>
                                    </div>

                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="sentido_comun_edat" disabled={true} onChange={this.onChange} value={this.state.prospecto.sentido_comun_edat} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>

                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="sentido_comun_edat2" disabled={true} value={this.state.prospecto.sentido_comun_edat2} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="sentido_comun_gdd" onChange={this.onChangeGDD.bind(this)} disabled={this.props.tipoUsuario != "GDD" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.sentido_comun_gdd} style={this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" ? { borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' } : { backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="sentido_comun_capacita" onChange={this.onChangeCAP.bind(this)} disabled={this.props.tipoUsuario != "CAP" && this.props.tipoUsuario != "GTECAP" && this.props.tipoUsuario != "ASESJRCAP" && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.sentido_comun_capacita} style={this.props.tipoUsuario == "CAP" || this.props.tipoUsuario == "ASESJRCAP" ? { borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' } : { backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="sentido_comun_direccion" onChange={this.onChangeSDDC.bind(this)} disabled={this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"}  value={this.state.prospecto.sentido_comun_direccion} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-xs-2 col-lg-2"></div>
                                    <div className="col-xs-2 col-lg-2"></div>
                                    <div className="col-xs-2 col-lg-2"></div>
                                    <div className="col-xs-2 col-lg-2">
                                        {
                                            (this.state.prospecto.sentido_comun_gdd == 0) ?
                                                <span style={{ color: "red" }}>El campo Orientación al logros (GDD) debe ser ingresado</span> : ''
                                        }</div>
                                    <div className="col-xs-2 col-lg-2">
                                        {
                                            (this.state.prospecto.sentido_comun_capacita == 0) ?
                                                <span style={{ color: "red" }}>El campo Sentido común (Capacitación) debe ser ingresado</span> : ''
                                        }
                                    </div>
                                    <div className="col-xs-2 col-lg-2"></div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div className="card-body  text-center">
                                <div className="row">
                                    <div className="col-xs-2 col-lg-2">
                                        <h8 class="mb-0 font-weight-semibold">Energía</h8>
                                    </div>

                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="energia_edat" disabled={true} onChange={this.onChange} value={this.state.prospecto.energia_edat} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>

                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="energia_edat2" disabled={true} value={this.state.prospecto.energia_edat2} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="energia_gdd" onChange={this.onChangeGDD.bind(this)} disabled={this.props.tipoUsuario != "GDD" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.energia_gdd} style={this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" ? { borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' } : { backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="energia_capacita" onChange={this.onChangeCAP.bind(this)} disabled={this.props.tipoUsuario != "CAP" && this.props.tipoUsuario != "GTECAP" && this.props.tipoUsuario != "ASESJRCAP" && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.energia_capacita} style={this.props.tipoUsuario == "CAP" || this.props.tipoUsuario == "ASESJRCAP" ? { borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' } : { backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="energia_direccion" onChange={this.onChangeSDDC.bind(this)} disabled={this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"}  value={this.state.prospecto.energia_direccion} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-xs-2 col-lg-2"></div>
                                    <div className="col-xs-2 col-lg-2"></div>
                                    <div className="col-xs-2 col-lg-2"></div>
                                    <div className="col-xs-2 col-lg-2">
                                        {
                                            (this.state.prospecto.energia_gdd == 0) ?
                                                <span style={{ color: "red" }}>El campo Orientación al logros (GDD) debe ser ingresado</span> : ''
                                        }
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        {
                                            (this.state.prospecto.energia_capacita == 0) ?
                                                <span style={{ color: "red" }}>El campo Energía (Capacitación) debe ser ingresado</span> : ''
                                        }
                                    </div>
                                    <div className="col-xs-2 col-lg-2"></div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div className="card-body  text-center">
                                <div className="row">
                                    <div className="col-xs-2 col-lg-2">
                                        <h8 class="mb-0 font-weight-semibold">Motivación por el dinero</h8>
                                    </div>

                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="motivacion_edat" disabled={true} onChange={this.onChange} value={this.state.prospecto.motivacion_edat} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>

                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="motivacion_edat2" disabled={true} value={this.state.prospecto.motivacion_edat2} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="motivacion_gdd" onChange={this.onChangeGDD.bind(this)} disabled={this.props.tipoUsuario != "GDD" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.motivacion_gdd} style={this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" ? { borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' } : { backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="motivacion_capacita" onChange={this.onChangeCAP.bind(this)} disabled={this.props.tipoUsuario != "CAP" && this.props.tipoUsuario != "GTECAP" && this.props.tipoUsuario != "ASESJRCAP" && this.props.tipoUsuario != "SUPER"} value={this.state.prospecto.motivacion_capacita} style={this.props.tipoUsuario == "CAP" || this.props.tipoUsuario == "ASESJRCAP" ? { borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' } : { backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="motivacion_direccion" onChange={this.onChangeSDDC.bind(this)} disabled={this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"}  value={this.state.prospecto.motivacion_direccion} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-xs-2 col-lg-2"></div>
                                    <div className="col-xs-2 col-lg-2"></div>
                                    <div className="col-xs-2 col-lg-2"></div>
                                    <div className="col-xs-2 col-lg-2">
                                        {
                                            (this.state.prospecto.motivacion_gdd == 0) ?
                                                <span style={{ color: "red" }}>El campo Orientación al logros (GDD) debe ser ingresado</span> : ''
                                        }
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        {
                                            (this.state.prospecto.motivacion_capacita == 0) ?
                                                <span style={{ color: "red" }}>El campo Motivación por el dinero (Capacitación) debe ser ingresado</span> : ''
                                        }
                                    </div>
                                    <div className="col-xs-2 col-lg-2"></div>
                                </div>
                            </div>
                        </div>


                        <div class="card" style={{ background: '#E1E3F6 ', borderRadius: '3px 3px 0px 0px' }}>
                            <div className="card-body  text-center">
                                <div className="row">
                                    <div className="col-xs-2 col-lg-2">
                                        <h8 class="mb-0 font-weight-semibold">TOTAL</h8>
                                    </div>

                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="total_edat" disabled={true} value={this.state.prospecto.total_edat} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>

                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="total_edat2" disabled={true} value={this.state.prospecto.total_edat2} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="total_gdd" disabled={true} value={this.state.prospecto.total_gdd} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="total_capacita" disabled={true} value={this.state.prospecto.total_capacita} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>
                                    <div className="col-xs-2 col-lg-2">
                                        <input type="number" name="total_direccion" disabled={true} value={this.state.prospecto.total_direccion} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', border: '1px solid #D5D9E8', borderRadius: '3px' }}></input>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-xs-6 col-lg-6">
                            <div className="card">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-xs-6 col-lg-6">
                                            <h5>Desempeño en Roleplay</h5>
                                        </div>
                                        {this.state.reporteCapacitacion.desempenio == 0 ?
                                            <a href="#" style={{
                                                height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                backgroundColor: '#04C601', borderColor: '#04C601'
                                            }}
                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                    class="letter-icon text-white"></span>
                                            </a> : ''}
                                        {this.state.reporteCapacitacion.desempenio == 1 ?
                                            <a href="#" style={{
                                                height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                backgroundColor: '#FFC300', borderColor: '#FFC300'
                                            }}
                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                    class="letter-icon text-white"></span>
                                            </a> : ''}
                                        {this.state.reporteCapacitacion.desempenio == 2 ?
                                            <a href="#" style={{
                                                height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                backgroundColor: '#C70039', borderColor: '#C70039'
                                            }}
                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                    class="letter-icon text-white"></span>
                                            </a> : ''}
                                    </div>

                                    <div className="row">

                                        <div className="col-xs-12 col-lg-12">

                                            <div className="col-xs-6 col-lg-6">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input " type="radio" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} name="desempenio" id="desempenio0" value={0} checked={this.state.reporteCapacitacion.desempenio == 0} onChange={this.onChange} />
                                                    <label class="form-check-label" for="desempenio0">Tuvo estructura, siguió indicaciones y aceptó comentarios</label>
                                                </div>
                                            </div>

                                            <div className="col-xs-6 col-lg-6">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} name="desempenio" id="desempenio1" value={1} checked={this.state.reporteCapacitacion.desempenio == 1} onChange={this.onChange} />
                                                    <label class="form-check-label" for="desempenio1">Le hizo falta algún elemento y tuvo disposición a cambiar</label>
                                                </div>
                                            </div>

                                            <div className="col-xs-7 col-lg-7">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} name="desempenio" id="desempenio2" value={2} checked={this.state.reporteCapacitacion.desempenio == 2} onChange={this.onChange} />
                                                    <label class="form-check-label" for="desempenio2">Le hizo falta más de 1 elemento y no aceptó comentarios</label>
                                                </div>
                                            </div>
                                        </div>
                                        {
                                            this.state.reporteCapacitacion.desempenio == null ?
                                                <span style={{ color: "red" }}>Desempeño en Roleplay, debe ser seleccionada</span> : ''
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="col-xs-6 col-lg-6">
                            <div className="card">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-xs-6 col-lg-6">
                                            <h5>Perspectiva de Capacitación</h5>
                                        </div>
                                        {this.state.reporteCapacitacion.perspectiva == 0 ?
                                            <a href="#" style={{
                                                height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                backgroundColor: '#04C601', borderColor: '#04C601'
                                            }}
                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                    class="letter-icon text-white"></span>
                                            </a> : ''}
                                        {this.state.reporteCapacitacion.perspectiva == 1 ?
                                            <a href="#" style={{
                                                height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                backgroundColor: '#FFC300', borderColor: '#FFC300'
                                            }}
                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                    class="letter-icon text-white"></span>
                                            </a> : ''}
                                        {this.state.reporteCapacitacion.perspectiva == 2 ?
                                            <a href="#" style={{
                                                height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                backgroundColor: '#C70039', borderColor: '#C70039'
                                            }}
                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                    class="letter-icon text-white"></span>
                                            </a> : ''}
                                    </div>

                                    <div className="row">

                                        <div className="col-xs-10 col-lg-10">

                                            <div className="col-xs-6 col-lg-6">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input " disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} type="radio" name="perspectiva" id="perspectiva0" value={0} checked={this.state.reporteCapacitacion.perspectiva == 0} onChange={this.onChange} />
                                                    <label class="form-check-label" for="perspectiva0">Cuenta con el perfil y demostró compromiso</label>
                                                </div>
                                            </div>

                                            <div className="col-xs-6 col-lg-6">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} type="radio" name="perspectiva" id="perspectiva1" value={1} checked={this.state.reporteCapacitacion.perspectiva == 1} onChange={this.onChange} />
                                                    <label class="form-check-label" for="perspectiva1">Debe trabajar algunos aspectos</label>
                                                </div>
                                            </div>

                                            <div className="col-xs-6 col-lg-6">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" disabled = {this.props.tipoUsuario == "GDD" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "GTEDES"} type="radio" name="perspectiva" id="perspectiva2" value={2} checked={this.state.reporteCapacitacion.perspectiva == 2} onChange={this.onChange} />
                                                    <label class="form-check-label" for="perspectiva2">No cuenta con el perfil</label>
                                                </div>
                                            </div>

                                        </div>
                                        {
                                            this.state.reporteCapacitacion.perspectiva == null ?
                                                <span style={{ color: "red" }}>Perspectiva de Capacitación, debe ser seleccionada</span> : ''
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-xs-6 col-lg-6"> <h6>Comentarios </h6></div>
                    </div>

                    <div className="row">
                        <div className="col-xs-12 col-lg-12">
                            <textarea style={{ height: '220px' }} name="comentarios" maxlength="300" disabled={this.props.tipoUsuario != "ASESJRCAP" && this.props.tipoUsuario != "GTECAP"} value={this.state.reporteCapacitacion.comentarios} onChange={this.onChange} className="col-xs-12 col-lg-12"></textarea>
                        </div>
                    </div>


                    <div className="row">
                        <div className="col-xs-6 col-lg-6"> <h6>Comentarios GDD</h6></div>
                    </div>

                    <div className="row">
                        <div className="col-xs-12 col-lg-12">
                            <textarea style={{ height: '103px' }} disabled={this.props.tipoUsuario != "GDD" && this.props.tipoUsuario != "GTEDES" } name="comentarios_gdd" maxlength="300" value={this.state.reporteCapacitacion.comentarios_gdd} onChange={this.onChange} className="col-xs-12 col-lg-12"></textarea>
                        </div>
                    </div>

                </div>
                {/*documentoooooo para webb  */}
                {
                    this.state.segundoFile == true &&

                    <div id="doc_capacitacion">
                        <head><meta charset="UTF-8"></meta></head>
                        <div className="row">
                            <div className="col-xs-6 col-lg-6">
                                <div className="row">
                                    <div className="col-xs-2 col-lg-2">
                                        <img src={this.state.imagenEnCambio != undefined ? this.state.imagenEnCambio : avatar} class="img-fluid rounded-circle" width="132px" height="132px" alt="" />
                                    </div>
                                    <div className="col-xs-10 col-lg-10">
                                        <div className="col-xs-6 col-lg-6">
                                            <h3>Prospecto:{this.state.conexion.nombre + ' ' + this.state.conexion.ape_paterno + ' ' + (this.state.conexion.ape_materno != null ? this.state.conexion.ape_materno : '')}</h3>
                                        </div>
                                        <div className="col-xs-6 col-lg-6">
                                            <h6>Universidad: {this.state.universidad}</h6>
                                        </div>
                                        <div className="col-xs-6 col-lg-6">
                                            <h6>Carrera: {this.state.carrera}</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xs-6 col-lg-6">
                                <div className="row">
                                    <div className="col-xs-10 col-lg-10">
                                        <div className="col-xs-6 col-lg-6">
                                            <h6>Gerente de Desarrollo: {this.state.nombreGDD}</h6>
                                        </div>
                                        <div className="col-xs-6 col-lg-6">
                                            <h6>Fuente referido: {this.state.fuenteReferido} </h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr></hr>

                        <div className="row">
                            <h6>DESCRIPCIÓN BREVE</h6>
                            <div className="col-xs-6 col-lg-6">
                                <p>{this.state.prospecto_ventaCarrera.descripcion_edat}</p>
                            </div>
                            <h6>SUEÑO</h6>
                            <div className="col-xs-6 col-lg-6">
                                <p>{this.state.reclutaEncuentro03.grande_sueno}</p>
                            </div>
                        </div>
                        <hr></hr>

                        <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                            <div style={{ display: 'inline-table', textAlign: 'left' }}>
                                <div className="col-xs-6 col-lg-6">
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-xs-6 col-lg-6">
                                                    <h5>Asistencia y retardos</h5>
                                                </div>
                                                {this.state.reporteCapacitacion.asistencia == 0 ?
                                                    <a href="#" style={{
                                                        height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                        backgroundColor: '#04C601', borderColor: '#04C601'
                                                    }}
                                                        class="btn rounded-pill btn-icon btn-sm "><span
                                                            class="letter-icon text-white"></span>
                                                    </a> : ''}
                                                {this.state.reporteCapacitacion.asistencia == 1 ?
                                                    <a href="#" style={{
                                                        height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                        backgroundColor: '#FFC300', borderColor: '#FFC300'
                                                    }}
                                                        class="btn rounded-pill btn-icon btn-sm "><span
                                                            class="letter-icon text-white"></span>
                                                    </a> : ''}
                                                {this.state.reporteCapacitacion.asistencia == 2 ?
                                                    <a href="#" style={{
                                                        height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                        backgroundColor: '#C70039', borderColor: '#C70039'
                                                    }}
                                                        class="btn rounded-pill btn-icon btn-sm "><span
                                                            class="letter-icon text-white"></span>
                                                    </a> : ''}
                                            </div>
                                            <div className="row">
                                                <div className="col-xs-10 col-lg-10">
                                                    <div className="col-xs-6 col-lg-6" style={{ Align: 'left' }}>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input " type="radio" name="asistencia" id="asistencia0" value={0} checked={this.state.reporteCapacitacion.asistencia == 0} onChange={this.onChange} />
                                                            <label class="form-check-label" for="asistencia0">Asistió a todas las sesiones puntualmente</label>
                                                        </div>
                                                    </div>
                                                    <div className="col-xs-6 col-lg-6">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="asistencia" id="asistencia1" value={1} checked={this.state.reporteCapacitacion.asistencia == 1} onChange={this.onChange} />
                                                            <label class="form-check-label" for="asistencia1">Tuvo 1 falta y/o retardo</label>
                                                        </div>
                                                    </div>
                                                    <div className="col-xs-6 col-lg-6">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="asistencia" id="asistencia2" value={2} checked={this.state.reporteCapacitacion.asistencia == 2} onChange={this.onChange} />
                                                            <label class="form-check-label" for="asistencia2">No respetó asistencia y horario</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style={{ display: 'inline-table', textAlign: 'center' }}>{<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}</div>
                            <div style={{ display: 'inline-table', textAlign: 'left' }}>
                                <div className="col-xs-6 col-lg-6">
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-xs-6 col-lg-6">
                                                    <h5>Actitud</h5>
                                                </div>
                                                {this.state.reporteCapacitacion.actitud == 0 ?
                                                    <a href="#" style={{
                                                        height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                        backgroundColor: '#04C601', borderColor: '#04C601'
                                                    }}
                                                        class="btn rounded-pill btn-icon btn-sm "><span
                                                            class="letter-icon text-white"></span>
                                                    </a> : ''}
                                                {this.state.reporteCapacitacion.actitud == 1 ?
                                                    <a href="#" style={{
                                                        height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                        backgroundColor: '#FFC300', borderColor: '#FFC300'
                                                    }}
                                                        class="btn rounded-pill btn-icon btn-sm "><span
                                                            class="letter-icon text-white"></span>
                                                    </a> : ''}
                                                {this.state.reporteCapacitacion.actitud == 2 ?
                                                    <a href="#" style={{
                                                        height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                        backgroundColor: '#C70039', borderColor: '#C70039'
                                                    }}
                                                        class="btn rounded-pill btn-icon btn-sm "><span
                                                            class="letter-icon text-white"></span>
                                                    </a> : ''}
                                            </div>
                                            <div className="row">
                                                <div className="col-xs-10 col-lg-10">
                                                    <div className="col-xs-6 col-lg-6">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input " type="radio" name="actitud" id="actitud0" value={0} checked={this.state.reporteCapacitacion.actitud == 0} onChange={this.onChange} />
                                                            <label class="form-check-label" for="actitud0">Excelente</label>
                                                        </div>
                                                    </div>
                                                    <div className="col-xs-6 col-lg-6">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="actitud" id="actitud1" value={1} checked={this.state.reporteCapacitacion.actitud == 1} onChange={this.onChange} />
                                                            <label class="form-check-label" for="actitud1">Buena</label>
                                                        </div>
                                                    </div>
                                                    <div className="col-xs-6 col-lg-6">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="actitud" id="actitud2" value={2} checked={this.state.reporteCapacitacion.actitud == 2} onChange={this.onChange} />
                                                            <label class="form-check-label" for="actitud2">Deficiente o mala</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>

                        <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                            <div className="col-xs-6 col-lg-6" style={{ display: 'inline-table', textAlign: 'left' }}>
                                <div className="card">
                                    <div className="card-body">
                                        <div className="row">
                                            <div className="col-xs-6 col-lg-6">
                                                <h5>Participación</h5>
                                            </div>
                                            {this.state.reporteCapacitacion.participacion == 0 ?
                                                <a href="#" style={{
                                                    height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                    backgroundColor: '#04C601', borderColor: '#04C601'
                                                }}
                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                        class="letter-icon text-white"></span>
                                                </a> : ''}
                                            {this.state.reporteCapacitacion.participacion == 1 ?
                                                <a href="#" style={{
                                                    height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                    backgroundColor: '#FFC300', borderColor: '#FFC300'
                                                }}
                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                        class="letter-icon text-white"></span>
                                                </a> : ''}
                                            {this.state.reporteCapacitacion.participacion == 2 ?
                                                <a href="#" style={{
                                                    height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                    backgroundColor: '#C70039', borderColor: '#C70039'
                                                }}
                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                        class="letter-icon text-white"></span>
                                                </a> : ''}
                                        </div>
                                        <div className="row">
                                            <div className="col-xs-10 col-lg-10">
                                                <div className="col-xs-6 col-lg-6">
                                                    <div class="form-check form-check-inline" style={{ Align: 'left' }}>
                                                        <input class="form-check-input " type="radio" name="participacion" id="participacion0" value={0} checked={this.state.reporteCapacitacion.participacion == 0} onChange={this.onChange} />
                                                        <label class="form-check-label" for="participacion0">Alta</label>
                                                    </div>
                                                </div>
                                                <div className="col-xs-6 col-lg-6">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="participacion" id="participacion1" value={1} checked={this.state.reporteCapacitacion.participacion == 1} onChange={this.onChange} />
                                                        <label class="form-check-label" for="participacion1">Media</label>
                                                    </div>
                                                </div>
                                                <div className="col-xs-6 col-lg-6">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="participacion" id="participacion2" value={2} checked={this.state.reporteCapacitacion.participacion == 2} onChange={this.onChange} />
                                                        <label class="form-check-label" for="participacion2">Ninguna</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style={{ display: 'inline-table', textAlign: 'center' }}>{<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}</div>
                            <div className="col-xs-6 col-lg-6" style={{ display: 'inline-table', textAlign: 'left' }}>
                                <div className="card" >
                                    <div className="card-body">
                                        <div className="row">
                                            <div className="col-xs-6 col-lg-6">
                                                <h5>Relación con el grupo</h5>
                                            </div>
                                            {this.state.reporteCapacitacion.relacion == 0 ?
                                                <a href="#" style={{
                                                    height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                    backgroundColor: '#04C601', borderColor: '#04C601'
                                                }}
                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                        class="letter-icon text-white"></span>
                                                </a> : ''}
                                            {this.state.reporteCapacitacion.relacion == 1 ?
                                                <a href="#" style={{
                                                    height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                    backgroundColor: '#FFC300', borderColor: '#FFC300'
                                                }}
                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                        class="letter-icon text-white"></span>
                                                </a> : ''}
                                            {this.state.reporteCapacitacion.relacion == 2 ?
                                                <a href="#" style={{
                                                    height: '15px', width: '15px', position: 'center', margin: '0 auto',
                                                    backgroundColor: '#C70039', borderColor: '#C70039'
                                                }}
                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                        class="letter-icon text-white"></span>
                                                </a> : ''}
                                        </div>
                                        <div className="row">
                                            <div className="col-xs-10 col-lg-10">
                                                <div className="col-xs-6 col-lg-6">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input " type="radio" name="relacion" id="relacion0" value={0} checked={this.state.reporteCapacitacion.relacion == 0} onChange={this.onChange} />
                                                        <label class="form-check-label" for="relacion0">Alta</label>
                                                    </div>
                                                </div>
                                                <div className="col-xs-6 col-lg-6">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="relacion" id="relacion1" value={1} checked={this.state.reporteCapacitacion.relacion == 1} onChange={this.onChange} />
                                                        <label class="form-check-label" for="relacion1">Media</label>
                                                    </div>
                                                </div>
                                                <div className="col-xs-6 col-lg-6">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="relacion" id="relacion2" value={2} checked={this.state.reporteCapacitacion.relacion == 2} onChange={this.onChange} />
                                                        <label class="form-check-label" for="relacion2">Baja</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <hr />
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>

                        <div className="row">
                            <div className="col-xs-6 col-lg-6"> <h6>Temas pendientes por revisar</h6></div>
                        </div>

                        {this.state.contenidoJSXTemasDistribucion}

                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <hr></hr>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <div className="row">
                            <div className="col-xs-6 col-lg-6"> <h6>Relación de entrega de tareas</h6></div>
                        </div>
                        <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                            <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                                <div className="col-xs-12 col-lg-12">
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-xs-2 col-lg-2"> <h8>Folleto</h8></div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" style={{ borderColor: '#FF4874' }} type="radio" name="folleto" id="folleto0" value={0} checked={this.state.reporteCapacitacion.folleto == 0} onChange={this.onChange} />
                                                        <label class="form-check-label" for="folleto0">Si</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" style={{ borderColor: '#FF4874' }} type="radio" name="folleto" id="folleto1" value={1} checked={this.state.reporteCapacitacion.folleto == 1} onChange={this.onChange} />
                                                        <label class="form-check-label" for="folleto1">No</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" style={{ borderColor: '#FF4874' }} type="radio" name="folleto" id="folleto2" value={2} checked={this.state.reporteCapacitacion.folleto == 2} onChange={this.onChange} />
                                                        <label class="form-check-label" for="folleto2">N/A</label>
                                                    </div>
                                                </div>
                                                <div className="col-xs-8 col-lg-8">
                                                    <input type="text" placeholder="Observaciones" name="folleto_obs" value={this.state.reporteCapacitacion.folleto_obs} onChange={this.onChange} className="form-control " />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                                <div className="col-xs-12 col-lg-12">
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-xs-2 col-lg-2"> <h8>Aviso de privacidad</h8></div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" style={{ borderColor: '#FF4874' }} type="radio" name="aviso" id="aviso0" value={0} checked={this.state.reporteCapacitacion.aviso == 0} onChange={this.onChange} />
                                                        <label class="form-check-label" for="aviso0">Si</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" style={{ borderColor: '#FF4874' }} type="radio" name="aviso" id="aviso1" value={1} checked={this.state.reporteCapacitacion.aviso == 1} onChange={this.onChange} />
                                                        <label class="form-check-label" for="aviso1">No</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" style={{ borderColor: '#FF4874' }} type="radio" name="aviso" id="aviso2" value={2} checked={this.state.reporteCapacitacion.aviso == 2} onChange={this.onChange} />
                                                        <label class="form-check-label" for="aviso2">N/A</label>
                                                    </div>
                                                </div>
                                                <div className="col-xs-8 col-lg-8">
                                                    <input type="text" placeholder="Observaciones" name="aviso_obs" value={this.state.reporteCapacitacion.aviso_obs} onChange={this.onChange} className="form-control " />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                                <div className="col-xs-12 col-lg-12">
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-xs-2 col-lg-2"> <h8>Guión telefónico</h8></div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" style={{ borderColor: '#FF4874' }} type="radio" name="guion" id="guion0" value={0} checked={this.state.reporteCapacitacion.guion == 0} onChange={this.onChange} />
                                                        <label class="form-check-label" for="guion0">Si</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" style={{ borderColor: '#FF4874' }} type="radio" name="guion" id="guion1" value={1} checked={this.state.reporteCapacitacion.guion == 1} onChange={this.onChange} />
                                                        <label class="form-check-label" for="guion1">No</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" style={{ borderColor: '#FF4874' }} type="radio" name="guion" id="guion2" value={2} checked={this.state.reporteCapacitacion.guion == 2} onChange={this.onChange} />
                                                        <label class="form-check-label" for="guion2">N/A</label>
                                                    </div>
                                                </div>
                                                <div className="col-xs-8 col-lg-8">
                                                    <input type="text" placeholder="Observaciones" name="guion_obs" value={this.state.reporteCapacitacion.guion_obs} onChange={this.onChange} className="form-control " />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                                <div className="col-xs-12 col-lg-12">
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-xs-2 col-lg-2"> <h8>Comercial</h8></div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" style={{ borderColor: '#FF4874' }} type="radio" name="comercial" id="comercial0" value={0} checked={this.state.reporteCapacitacion.comercial == 0} onChange={this.onChange} />
                                                        <label class="form-check-label" for="comercial0">Si</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" style={{ borderColor: '#FF4874' }} type="radio" name="comercial" id="comercial1" value={1} checked={this.state.reporteCapacitacion.comercial == 1} onChange={this.onChange} />
                                                        <label class="form-check-label" for="comercial1">No</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" style={{ borderColor: '#FF4874' }} type="radio" name="comercial" id="comercial2" value={2} checked={this.state.reporteCapacitacion.comercial == 2} onChange={this.onChange} />
                                                        <label class="form-check-label" for="comercial2">N/A</label>
                                                    </div>
                                                </div>
                                                <div className="col-xs-8 col-lg-8">
                                                    <input type="text" placeholder="Observaciones" name="comercial_obs" value={this.state.reporteCapacitacion.comercial_obs} onChange={this.onChange} className="form-control " />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                            <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                                <div className="col-xs-12 col-lg-12">
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-xs-2 col-lg-2"> <h8>Mail entre citas</h8></div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" style={{ borderColor: '#FF4874' }} type="radio" name="mail" id="mail0" value={0} checked={this.state.reporteCapacitacion.mail == 0} onChange={this.onChange} />
                                                        <label class="form-check-label" for="mail0">Si</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" style={{ borderColor: '#FF4874' }} type="radio" name="mail" id="mail1" value={1} checked={this.state.reporteCapacitacion.mail == 1} onChange={this.onChange} />
                                                        <label class="form-check-label" for="mail1">No</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" style={{ borderColor: '#FF4874' }} type="radio" name="mail" id="mail2" value={2} checked={this.state.reporteCapacitacion.mail == 2} onChange={this.onChange} />
                                                        <label class="form-check-label" for="mail2">N/A</label>
                                                    </div>
                                                </div>
                                                <div className="col-xs-8 col-lg-8">
                                                    <input type="text" placeholder="Observaciones" name="mail_obs" value={this.state.reporteCapacitacion.mail_obs} onChange={this.onChange} className="form-control " />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                                <div className="col-xs-12 col-lg-12">
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-xs-2 col-lg-2"> <h8>Ejercicio en Nautilus</h8></div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" style={{ borderColor: '#FF4874' }} type="radio" name="ejercicio" id="ejercicio0" value={0} checked={this.state.reporteCapacitacion.ejercicio == 0} onChange={this.onChange} />
                                                        <label class="form-check-label" for="ejercicio0">Si</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" style={{ borderColor: '#FF4874' }} type="radio" name="ejercicio" id="ejercicio1" value={1} checked={this.state.reporteCapacitacion.ejercicio == 1} onChange={this.onChange} />
                                                        <label class="form-check-label" for="ejercicio1">No</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" style={{ borderColor: '#FF4874' }} type="radio" name="ejercicio" id="ejercicio2" value={2} checked={this.state.reporteCapacitacion.ejercicio == 2} onChange={this.onChange} />
                                                        <label class="form-check-label" for="ejercicio2">N/A</label>
                                                    </div>
                                                </div>
                                                <div className="col-xs-8 col-lg-8">
                                                    <input type="text" placeholder="Observaciones" name="ejercicio_obs" value={this.state.reporteCapacitacion.ejercicio_obs} onChange={this.onChange} className="form-control " />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                                <div className="col-xs-12 col-lg-12">
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-xs-2 col-lg-2"> <h8>Carta propuesta</h8></div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" style={{ borderColor: '#FF4874' }} type="radio" name="carta" id="carta0" value={0} checked={this.state.reporteCapacitacion.carta == 0} onChange={this.onChange} />
                                                        <label class="form-check-label" for="carta0">Si</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" style={{ borderColor: '#FF4874' }} type="radio" name="carta" id="carta1" value={1} checked={this.state.reporteCapacitacion.carta == 1} onChange={this.onChange} />
                                                        <label class="form-check-label" for="carta1">No</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" style={{ borderColor: '#FF4874' }} type="radio" name="carta" id="carta2" value={2} checked={this.state.reporteCapacitacion.carta == 2} onChange={this.onChange} />
                                                        <label class="form-check-label" for="carta2">N/A</label>
                                                    </div>
                                                </div>
                                                <div className="col-xs-8 col-lg-8">
                                                    <input type="text" placeholder="Observaciones" name="carta_obs" value={this.state.reporteCapacitacion.carta_obs} onChange={this.onChange} className="form-control " />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                                <div className="col-xs-12 col-lg-12">
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-xs-2 col-lg-2"> <h8>Roleplay de segunda entrevista</h8></div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" style={{ borderColor: '#FF4874' }} type="radio" name="roleplay" id="roleplay0" value={0} checked={this.state.reporteCapacitacion.roleplay == 0} onChange={this.onChange} />
                                                        <label class="form-check-label" for="roleplay0">Si</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" style={{ borderColor: '#FF4874' }} type="radio" name="roleplay" id="roleplay1" value={2} checked={this.state.reporteCapacitacion.roleplay == 1} onChange={this.onChange} />
                                                        <label class="form-check-label" for="roleplay1">No</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" style={{ borderColor: '#FF4874' }} type="radio" name="roleplay" id="roleplay2" value={2} checked={this.state.reporteCapacitacion.roleplay == 2} onChange={this.onChange} />
                                                        <label class="form-check-label" for="roleplay2">N/A</label>
                                                    </div>
                                                </div>
                                                <div className="col-xs-8 col-lg-8">
                                                    <input type="text" placeholder="Observaciones" name="roleplay_obs" value={this.state.reporteCapacitacion.roleplay_obs} onChange={this.onChange} className="form-control " />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                            <div className="col-xs-12 col-lg-12">
                                <div className="card">
                                    <div className="card-body">
                                        <div className="row">
                                            <div className="col-xs-2 col-lg-2"> <h8>Exposición</h8></div>
                                            <div className="col-xs-2 col-lg-2">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" style={{ borderColor: '#FF4874' }} type="radio" name="exposicion" id="exposicion0" value={0} checked={this.state.reporteCapacitacion.exposicion == 0} onChange={this.onChange} />
                                                    <label class="form-check-label" for="exposicion0">Si</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" style={{ borderColor: '#FF4874' }} type="radio" name="exposicion" id="exposicion1" value={1} checked={this.state.reporteCapacitacion.exposicion == 1} onChange={this.onChange} />
                                                    <label class="form-check-label" for="exposicion1">No</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" style={{ borderColor: '#FF4874' }} type="radio" name="exposicion" id="exposicion2" value={2} checked={this.state.reporteCapacitacion.exposicion == 2} onChange={this.onChange} />
                                                    <label class="form-check-label" for="exposicion2">N/A</label>
                                                </div>
                                            </div>
                                            <div className="col-xs-8 col-lg-8">
                                                <input type="text" placeholder="Observaciones" name="exposicion_obs" value={this.state.reporteCapacitacion.exposicion_obs} onChange={this.onChange} className="form-control " />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <hr></hr>

                        <div className="row">
                            <div className="col-xs-6 col-lg-6"> <h6>Factores Vitales</h6></div>
                        </div>
                        <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                            <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                                <div className="col-xs-12 col-lg-12">
                                    <div className="card">
                                        <div className="card-body">
                                            <div class="card-header text-center">
                                                <div className="row">
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6 class="card-title">-</h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6 class="card-title"><strong>Orientación al logro</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6 class="card-title"><strong>Perseverancia Gerente</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6 class="card-title"><strong>Carácter e integridad</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6 class="card-title"><strong>Sentido común</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6 class="card-title"><strong>Energía</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6 class="card-title"><strong>Motivación por el dinero</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6 class="card-title">Total</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                            <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                                <div className="col-xs-12 col-lg-12">
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                            <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                                <div className="col-xs-12 col-lg-12">
                                    <div className="card">
                                        <div className="card-body">
                                            <div class="card-header text-center">
                                                <div className="row">
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h8 class="mb-0 font-weight-semibold">EDAT</h8>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.orientacion_logro_edat}</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.perceverancia_edat}</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.integridad_edat}</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.sentido_comun_edat}</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.energia_edat}</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.motivacion_edat}</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.total_edat}</strong></h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                            <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                                <div className="col-xs-12 col-lg-12">
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                            <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                                <div className="col-xs-12 col-lg-12">
                                    <div className="card">
                                        <div className="card-body">
                                            <div class="card-header text-center">
                                                <div className="row">
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h8 class="mb-0 font-weight-semibold">Gerente EDAT</h8>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.orientacion_logro_edat2}</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.perceverancia_edat2}</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.integridad_edat2}</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.sentido_comun_edat2}</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.energia_edat2}</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.motivacion_edat2}</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.total_edat2}</strong></h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                            <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                                <div className="col-xs-12 col-lg-12">
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                            <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                                <div className="col-xs-12 col-lg-12">
                                    <div className="card">
                                        <div className="card-body">
                                            <div class="card-header text-center">
                                                <div className="row">
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h8 class="mb-0 font-weight-semibold">GDD</h8>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.orientacion_logro_gdd} </strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.perceverancia_gdd}</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.integridad_gdd}</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.sentido_comun_gdd}</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.energia_gdd}</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.motivacion_gdd}</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.total_gdd}</strong></h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                            <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                                <div className="col-xs-12 col-lg-12">
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                            <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                                <div className="col-xs-12 col-lg-12">
                                    <div className="card">
                                        <div className="card-body">
                                            <div class="card-header text-center">
                                                <div className="row">
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h8 class="mb-0 font-weight-semibold">Capacitación</h8>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.orientacion_logro_capacita}</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.perceverancia_capacita}</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.integridad_capacita}</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.sentido_comun_capacita}</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.energia_capacita}</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.motivacion_capacita}</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.total_capacita}</strong></h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                            <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                                <div className="col-xs-12 col-lg-12">
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                                <div className="col-xs-2 col-lg-2">
                                                    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                            <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                                <div className="col-xs-12 col-lg-12">
                                    <div className="card">
                                        <div className="card-body">
                                            <div class="card-header text-center">
                                                <div className="row">
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h8 class="mb-0 font-weight-semibold">SDDC</h8>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.orientacion_logro_direccion}</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.perceverancia_direccion}</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6 ><strong>{this.state.prospectoRecuperado.integridad_direccion}</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.sentido_comun_direccion}</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.energia_direccion}</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.motivacion_direccion}</strong></h6>
                                                    </div>
                                                    <div className="col-xs-2 col-lg-2">
                                                        <h6><strong>{this.state.prospectoRecuperado.total_direccion}</strong></h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <hr></hr>
                        <div className="row">
                            <div className="col-xs-6 col-lg-6">
                                <div className="card">
                                    <div className="card-body">
                                        <div className="row">
                                            <div className="col-xs-6 col-lg-6">
                                                <h5>Desempeño en Roleplay</h5>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-xs-12 col-lg-12">
                                                <div className="col-xs-6 col-lg-6">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input " type="radio" name="desempenio" id="desempenio0" value={0} checked={this.state.reporteCapacitacion.desempenio == 0} onChange={this.onChange} />
                                                        <label class="form-check-label" for="desempenio0">Tuvo estructura, siguió indicaciones y aceptó comentarios</label>
                                                    </div>
                                                </div>
                                                <div className="col-xs-6 col-lg-6">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="desempenio" id="desempenio1" value={1} checked={this.state.reporteCapacitacion.desempenio == 1} onChange={this.onChange} />
                                                        <label class="form-check-label" for="desempenio1">Le hizo falta algún elemento y tuvo disposición a cambiar</label>
                                                    </div>
                                                </div>
                                                <div className="col-xs-7 col-lg-7">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="desempenio" id="desempenio2" value={2} checked={this.state.reporteCapacitacion.desempenio == 2} onChange={this.onChange} />
                                                        <label class="form-check-label" for="desempenio2">Le hizo falta más de 1 elemento y no aceptó comentarios</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xs-6 col-lg-6">
                                <div className="card">
                                    <div className="card-body">
                                        <div className="row">
                                            <div className="col-xs-6 col-lg-6">
                                                <h5>Perspectiva de Capacitación</h5>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-xs-10 col-lg-10">
                                                <div className="col-xs-6 col-lg-6">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input " type="radio" name="perspectiva" id="perspectiva0" value={0} checked={this.state.reporteCapacitacion.perspectiva == 0} onChange={this.onChange} />
                                                        <label class="form-check-label" for="perspectiva0">Cuenta con el perfil y demostró compromiso</label>
                                                    </div>
                                                </div>
                                                <div className="col-xs-6 col-lg-6">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="perspectiva" id="perspectiva1" value={1} checked={this.state.reporteCapacitacion.perspectiva == 1} onChange={this.onChange} />
                                                        <label class="form-check-label" for="perspectiva1">Debe trabajar algunos aspectos</label>
                                                    </div>
                                                </div>
                                                <div className="col-xs-6 col-lg-6">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="perspectiva" id="perspectiva2" value={2} checked={this.state.reporteCapacitacion.perspectiva == 2} onChange={this.onChange} />
                                                        <label class="form-check-label" for="perspectiva2">No cuenta con el perfil</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br></br>
                        <br></br>
                        <hr></hr>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <br></br>
                        <div className="row">
                            <div className="col-xs-6 col-lg-6"> <h6>Comentarios</h6></div>
                        </div>
                        <div className="row">
                            <div className="col-xs-12 col-lg-12">
                                <textarea style={{ height: '220px', width: '800px' }} name="comentarios" value={this.state.reporteCapacitacion.comentarios} onChange={this.onChange} className="col-xs-12 col-lg-12"></textarea>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-6 col-lg-6"> <h6>Comentarios GDD</h6></div>
                        </div>
                        <div className="row">
                            <div className="col-xs-12 col-lg-12">
                                <textarea style={{ height: '103px', width: '800px' }} name="comentarios_gdd" value={this.state.reporteCapacitacion.comentarios_gdd} onChange={this.onChange} className="col-xs-12 col-lg-12"></textarea>
                            </div>
                        </div>
                    </div>
                }


            </div>

        )
    }
}