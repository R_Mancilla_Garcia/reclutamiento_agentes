import React, { Component } from "react";
import datos from '../urls/datos.json'

const logoConvenio = require("../imagenes/logoConvenio.png")
const logoEstimadoInicial = require("../imagenes/estimado_Inicial.png")
const logoConvenioPDF = datos.urlServicePyPDF+"documentos/empleados/fotografia/logoConvenio.png"
const logoEstimadoInicialPDF = datos.urlServicePyPDF+"documentos/empleados/fotografia/estimado_Inicial.png"

export default class ConvenioArranque extends Component {

    constructor() {
        super();
        this.generaDocumento = this.generaDocumento.bind(this)
        this.invocaDoc = this.invocaDoc.bind(this)
        this.regresarPantalla = this.regresarPantalla.bind(this)
        this.state = {
            segundoFile: false,
            nombreProspecto: '',
            nombreGDD: '',
            nombreSDDC: '',
            fuenteReferido: '',
            fecha: null,
            capParametros: [],
            parametros: [],
            variable1: "",
            usuario: "",
        }
    }


    UNSAFE_componentWillMount() {
        //fetch(datos.urlServicePy+"recluta/api_recluta_vprospectos/" + this.props.idProspecto)
        fetch(datos.urlServicePy+"parametros/api_recluta_prospectos/" + this.props.idProspecto)
            .then(response => response.json())
            .then(json => {
                let fila = json.filas[0].fila
                let columnas = json.columnas
                let columnasSalida = {}
                let date = new Date().getDate();
                let month = new Date().getMonth() + 1;
                let year = new Date().getFullYear();
                let fechaSalida = date + '-' + month + '-' + year;
                for (var i = 0; i < fila.length; i++) {
                    console.log(fila[i])
                    columnasSalida["" + columnas[i].key + ""] = fila[i].value
                }

                this.setState({
                    nombreProspecto: columnasSalida.nombre + ' ' + columnasSalida.ape_paterno + ' ' + (columnasSalida.ape_materno == null ? '' : columnasSalida.ape_materno),
                    fecha: fechaSalida
                })
                this.setState({ usuario: this.props.nombreUsuario })
            })
        fetch(datos.urlServicePy+"recluta/api_recluta_knockout10/" + this.props.idProspecto)
            .then(response => response.json())
            .then(jsonData => {
                if (jsonData.length > 0) {
                    fetch(datos.urlServicePy+"recluta/api_vcat_gdd/" + jsonData[0].gdd_id)
                        .then(response => response.json())
                        .then(jsonGDD => {
                            for (var i = 0; i < jsonGDD.length; i++) {
                                this.setState({ nombreGDD: jsonGDD[i].empleado })
                            }
                        });
                    fetch(datos.urlServicePy+"recluta/api_vcat_sddc/" + jsonData[0].sddc_id)
                    .then(response => response.json())
                        .then(jsonGDD => {
                            if(jsonGDD.length > 0){
                                this.setState({ nombreSDDC: jsonGDD[0].empleado })
                            }
                        });
                }
            });
        fetch(datos.urlServicePy+"parametros/api_cat_parametros_sistema/0")
            .then(response => response.json())
            .then(jsonParametros => {
                let filas = jsonParametros.filas
                let columnas = jsonParametros.columnas
                let columnasSalida = {};
                let listaParameros = [];
                let listaOrden = [];
                for (var i = 0; i < filas.length; i++) {
                    for (var j = 0; j < filas[i].fila.length; j++) {
                        columnasSalida["" + columnas[j].key + ""] = filas[i].fila[j].value
                    }
                    if (columnasSalida.filler == "17-CON") {
                        listaOrden.push({ "valor": columnasSalida.id.toString().length == 1 ? '0' + columnasSalida.id : columnasSalida.id.toString(), "descripcion": columnasSalida.descripcion.replace("XXX@",columnasSalida.valor) });
                        //listaParameros.push({"valor":columnasSalida.valor,"descripcion":columnasSalida.descripcion})
                        //listaParameros.push(<h3 className={columnasSalida.valor+"-"+columnasSalida.descripcion}>{columnasSalida.descripcion}</h3>)
                    }
                }
                listaOrden = listaOrden.sort((a, b) => (a.valor > b.valor ? 1 : a.valor < b.valor ? -1 : 0))
                listaOrden.forEach((data) => {
                    listaParameros.push(<span className="font-weight-semibold" name={data.valor + "-" + data.descripcion}>{data.descripcion}</span>)
                })
                this.setState({ capParametros: listaParameros })
            });
    }// 


    generaDocumento() {
        console.log(document.getElementById("convenio_arranque").outerHTML)
        let objetoHTML = document.getElementById("convenio_arranque")
        let tipo = "";
        let json = {
            "id": this.props.idProspecto,
            "prospecto_id": this.props.idProspecto,
            "html": objetoHTML.outerHTML,
            "doc_convenio": "convenio_" + this.props.idProspecto + ".pdf"
        }

        fetch(datos.urlServicePy+"recluta/api_recluta_conexion17/" + this.props.idProspecto)
            .then(response => response.json())
            .then(existeProspecto => {
                let url = datos.urlServicePy+"recluta/api_doc_conexconvenio_pdf/"

                if (existeProspecto[0].doc_convenio != null && existeProspecto[0].doc_convenio.length > 0) {
                    url = url + this.props.idProspecto
                } else {
                    url = url + "0"
                }
                const requestOptions = {
                    method: "POST",
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(json)
                };

                fetch(url, requestOptions)
                    .then(response => response.json())
                    .then(data => {
                        console.log("respuesta de armar el doc ", data)
                        if (data.id != undefined && data.id != null) {
                            if (this.props.tipoPantalla == "conexion") {
                                this.props.mostrarConexionCapacitacion()
                            }
                            if (this.props.tipoPantalla == "conectado") {
                                this.props.mostrarConectado()
                            }
                        }
                    })
            });

        //let url=datos.urlServicePy+"recluta/api_doc_conexconvenio_pdf/0"
        //let url=datos.urlServicePy+"recluta/api_doc_conexconvenio_pdf/"
        //    url=url+this.props.idProspecto
    }

    regresarPantalla() {
        if (this.props.tipoPantalla == "conexion") {
            this.props.mostrarConexionCapacitacion()
        }
        if (this.props.tipoPantalla == "conectado") {
            this.props.mostrarConectado()
        }
    }

    invocaDoc() {
        fetch(datos.urlServicePy+"recluta/api_recluta_vprospectos/" + this.props.idProspecto)
            .then(response => response.json())
            .then(arrayContenido => {
                this.setState({ segundoFile: true })
                this.generaDocumento()
            })

    }

    render() {
        return (
            <div >
                <div class="card" style={{ backgroundColor: "#313A46" }}>
                    <div className="card-body">
                        <div className="row">
                            <div className="col-xs-1 col-lg-1">
                                <span ><a ><i onClick={this.regresarPantalla} style={{ color: 'white' }} className="icon-undo2"></i></a></span>
                            </div>
                            <div className="col-xs-2 col-lg-2">
                                <h6 style={{ color: 'white' }}>Convenio de arranque</h6>
                            </div>
                            <div className="col-xs-7 col-lg-7">
                                {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                            </div>
                            <div className="col-xs-2 col-lg-2">
                                <button class="btn " onClick={/**/this.invocaDoc} type="button" style={{ backgroundColor: '#41B3D1', color: '#FFFFFF' }}>Generar</button>
                            </div>
                        </div>

                    </div>
                </div>
                <br></br>

                <div >

                    <div className="row">
                        <div className="col-xs-3 col-lg-3">
                            <div className="col-xs-12 col-lg-12">
                                {<img src={logoConvenio} class="img-fluid " width="200px" height="132px" alt="" />}
                            </div>
                            <br />
                            <div className="col-xs-12 col-lg-12">
                                Fecha: {this.state.fecha}
                            </div>

                            <div className="col-xs-12 col-lg-12">
                                Usuario que captura: {this.props.nombreUsuario}
                            </div>
                        </div>
                        <div className="col-xs-9 col-lg-9">
                            <div className="col-xs-12 col-lg-12">
                                <h6 className="font-weight-semibold">Convenio de Arranque</h6>
                            </div>
                            <br></br>
                            <div className="col-xs-12 col-lg-12">
                                <p className="font-weight-semibold" style={{ color: '#617187' }}>PRESENTE:</p>
                            </div>
                            <br></br>
                            <div className="col-xs-12 col-lg-12">
                                <p ><span style={{ color: '#617187' }}>Estimad@ </span> <span className="font-weight-semibold">{this.state.nombreProspecto}: </span></p>
                            </div>

                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}> Es un privilegio para APS arrancar contigo este proyecto y acompañarte para el logro de todos tus objetivos; para lograrlo, es muy importante que establezcamos metas, avances y compromisos mutuos.  </p>
                            </div>

                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}> Como lo hemos platicado, parte importante de tu arranque y crecimiento en APS, implica que formes parte de un equipo de desarrollo que te acompañará en tu formación como Agente Novel y <span className="font-weight-semibold"><strong>{this.state.nombreGDD}</strong></span> será tu Gerente de Desarrollo.  </p>
                            </div>
                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>Nuestro compromiso, es ser un factor clave en tu formación y consolidación como Agente de Seguros en GNP, apoyándonos en dos pilares o filosofías fundamentales:</p>
                            </div>
                            <br />
                            <div className="col-xs-12 col-lg-12">
                                <p ><h6 className="font-weight-semibold">1o Transformamos la vida de nuestros Agentes para cambiar la historia de nuestros clientes:</h6></p>
                            </div>
                            <br />
                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>Nuestra Misión es generar las mejores condiciones posibles para que logres un excelente futuro personal, profesional y económico, y esperamos que tú actúes de la misma forma, ayudando a tus prospectos y clientes a lograr sus objetivos de desarrollo económico y financiero a través de los productos de GNP.</p>
                            </div>
                            <br />
                            <div className="col-xs-12 col-lg-12">
                                <p> <h6 className="font-weight-semibold">2o APS, el Mejor Equipo de Aseguradores de México:</h6></p>
                            </div>
                            <br />
                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>Aun teniendo prácticas sobresalientes dentro de la industria, ser los mejores, es nuestra más alta, genuina y sincera aspiración. Por ello nos esforzamos todos los días, convencidos de que necesitamos altos estándares de desempeño de todos los que formamos parte del equipo APS, y especialmente de los que como tú, se incorporan a nuestro “ejército de ángeles sin alas” en los equipos de ventas.</p>
                            </div>
                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>Sabemos que parte importante del atractivo de tu nueva carrera como Agente de Seguros es la independencia; el rendirte cuentas solo a ti mismo y a tus clientes; tomarás tus propias decisiones de negocios y vivirás con el resultado de esas decisiones. Queremos que tengas el privilegio de ser tu propio jefe, <span className="font-weight-semibold">y una vez que logres calificar dos veces a los Congresos de GNP, serás libre</span>, en la mayor parte, para tomar tus propias decisiones sobre cómo deseas dirigir tu negocio. Hasta ese entonces, sabemos que nuestra supervisión y experiencia en el desarrollo de profesionales de ventas de Seguro de Vida, nos pone en una mejor posición, para juzgar qué patrones de actividad y desempeño necesitas seguir para alcanzar tú potencial.</p>
                            </div>
                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>Para lograrlo hemos fijado guías específicas que te pediremos sigas puntualmente:</p>
                            </div>
                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>Dedicarte de  <span className="font-weight-semibold">tiempo completo y con todo tu entusiasmo</span>, a la promoción, asesoría y venta de los productos de Líneas Personales de GNP, condición para el éxito en nuestro negocio.</p>
                            </div>

                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>{this.state.capParametros[0]}</p>
                            </div>

                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>Con el claro objetivo de establecer tu propia fórmula de esfuerzo semanal, que será una de tus herramientas estratégicas de planeación y productividad, deberás reportar periódicamente las cifras específicas de tus actividades clave:</p>
                            </div>
                            <br />
                            <div className="col-xs-12 col-lg-12">
                                <img src={logoEstimadoInicial} class="img-fluid " width="1350px" height="132px" alt="" />
                            </div>
                            <br />
                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}><span className="font-weight-semibold">Asistir puntualmente a todas las juntas </span> a las que te convoquen APS y GNP, pues son parte integral de tu capacitación, entrenamiento, integración y arraigo.</p>
                            </div>
                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>Tener un <span className="font-weight-semibold">alto nivel de efectividad y productividad</span> que garanticen que tu actividad es rentable, siempre buscando la obtención de todos los bonos de productividad trimestrales y cuatrimestrales, las campañas y promociones de GNP y de APS, así como asistir a los Congresos de GNP desde el inicio de tu actividad, todo esto a través de los ramos de Líneas Personales.</p>
                            </div>
                            <br></br>
                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>Las fases de tu desarrollo como Agente Profesional en Seguros, son las siguientes:</p>
                            </div>
                            <br></br>
                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>{this.state.capParametros[1]}</p>
                            </div>
                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>{this.state.capParametros[2]}{this.state.capParametros[3]}</p>
                            </div>
                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>{this.state.capParametros[4]}{this.state.capParametros[5]}</p>
                            </div>
                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>{this.state.capParametros[6]} ganar Congreso Platino / Calificar a la MDRT como aspirante</p>
                            </div>
                            <br />
                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>No lograr el requisito mínimo en cada una de las 3 primeras fases, normalmente es un indicador de que tu desempeño tenderá a ser pobre, y por ello, será causa para tu separación definitiva de nuestra Agencia y por lo tanto, de GNP.</p>
                            </div>
                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>A criterio del Director de Agencia y de tu Gerente de Desarrollo, se podrá aplicar un solo periodo adicional para el logro de los objetivos en estas 3 Fases.</p>
                            </div>
                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>APS buscará ser en todo momento, un factor que te impulse al logro de tus más altas aspiraciones personales, profesionales y económicas. Pondremos a tu disposición a tu Gerente de Desarrollo, responsable al 100% de tu capacitación, entrenamiento y supervisión, y a todo un equipo para darte apoyo en Operaciones, Seguimiento, Comunicación y Soporte a Ventas.</p>
                            </div>
                            <br />
                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>Tu plan de desarrollo con tu Gerente se integra, de forma general, de lo siguiente:</p>
                            </div>
                            <br />
                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>Capacitación en la metodología de ventas Ironsale</p>
                            </div>

                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>Acompañamiento a entrevistas de ventas como parte de tu programa de arranque, que implica: (6 semanas)</p>
                            </div>
                            <br />
                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>{this.state.capParametros[7]}</p>
                            </div>
                            <br></br>
                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>{this.state.capParametros[8]}</p>
                            </div>
                            <br />
                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>{this.state.capParametros[9]}</p>
                            </div>
                            <br />
                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>Es importante dar seguimiento a tu plan de capacitación y desarrollo en IDEAS GNP (Instituto de Especialidad a Agentes de Seguros):</p>
                            </div>

                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>Recibirás mensualmente tu boleta de IDEAS y el calendario de cursos</p>
                            </div>
                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>{this.state.capParametros[10]}{this.state.capParametros[11]}</p>
                            </div>
                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>Es indispensable que el equipo de capacitación APS te de a conocer los beneficios de ser congresista y saber la estategia para avanzar rápidamente en las designaciones</p>
                            </div>
                            <br></br>
                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>Además del apoyo de tu Gerente de Desarrollo, contarás con el soporte de los expertos en Gastos Médicos y Patrimoniales y del acompañamiento de nuestros Directivos para asesorarte en casos especiales, los cuales deberán ser cuidadosamente seleccionados por su complejidad, especialidad o cuantía.</p>
                            </div>
                            <br></br>
                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>Asimismo, APS dará seguimiento, asesoría y monitoreo de toda tu actividad y resultados.</p>
                            </div>
                            <br></br>
                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>Tendrás acceso a las oficinas APS en donde tendrás acceso a áreas comunes de trabajo y de capacitación, y contarás con servicios de conexión a internet, scanner, soporte técnico de cómputo y aplicaciones GNP, entre otros.</p>
                            </div>
                            <br></br>
                            <div className="col-xs-12 col-lg-12">
                                <p style={{ color: '#617187' }}>Nosotros creemos en ti, sabemos que tienes todo para ser un Agente de Seguros exitoso, por lo que queremos continuar trabajando contigo para demostrarnos que juntos, podemos construir una base que te permita participar de las múltiples recompensas que brinda nuestra profesión.</p>
                            </div>
                        </div>
                    </div>

                    <br />
                    <br />
                    <br />
                    <br />

                    <div className="row">
                        <div className="col-xs-3 col-lg-3">
                            <div className="col-xs-12 col-lg-12">
                            </div>
                        </div>
                        <div className="col-xs-3 col-lg-3">
                            <div className="col-xs-12 col-lg-12">
                            </div>
                            <div className="col-xs-12 col-lg-12 text-center">
                                <h6 className="font-weight-semibold"><strong>{this.state.nombreProspecto}</strong></h6>
                            </div>
                        </div>
                        <div className="col-xs-3 col-lg-3">
                            <div className="col-xs-12 col-lg-12">
                            </div>
                            <div className="col-xs-12 col-lg-12 text-center">
                                <h6 className="font-weight-semibold"><strong>{this.state.nombreSDDC}</strong></h6>
                            </div>
                        </div>
                        <div className="col-xs-3 col-lg-3">
                            <div className="col-xs-12 col-lg-12 ">
                            </div>
                            <div className="col-xs-12 col-lg-12 text-center">
                                <h6 className="font-weight-semibold"><strong>{this.state.nombreGDD}</strong></h6>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-3 col-lg-3">
                            <div className="col-xs-12 col-lg-12">
                            </div>
                        </div>
                        <div className="col-xs-3 col-lg-3">
                            <div className="col-xs-12 col-lg-12">
                                <hr class="my-4" />
                            </div>
                            <div className="col-xs-12 col-lg-12 text-center">
                                <h6 className="font-weight-semibold">AGENTE </h6>
                            </div>
                        </div>
                        <div className="col-xs-3 col-lg-3">
                            <div className="col-xs-12 col-lg-12">
                                <hr class="my-4" />
                            </div>
                            <div className="col-xs-12 col-lg-12 text-center">
                                <h6 className="font-weight-semibold"> SUBDIRECCIÓN DE DESARROLLO</h6>
                            </div>
                        </div>
                        <div className="col-xs-3 col-lg-3">
                            <div className="col-xs-12 col-lg-12 ">
                                <hr class="my-4" />
                            </div>
                            <div className="col-xs-12 col-lg-12 text-center">
                                <h6 className="font-weight-semibold">GERENTE DE DESARROLLO</h6>
                            </div>
                        </div>

                    </div>

                </div>

                {/*html para archivo web */}
                {this.state.segundoFile == true &&

                    <div id="convenio_arranque" >
                        <head><meta charset="UTF-8"></meta></head>
                        <div className="row">
                            <div className="col-xs-3 col-lg-3">
                                <div className="col-xs-12 col-lg-12">
                                    {/*<img src={logoConvenio} class="img-fluid " width="200px" height="132px" alt="" />*/}
                                    {<img src={logoConvenioPDF} class="img-fluid rounded-circle" width="80px" height="60px" alt="" />}
                                </div>
                                <br />
                                <div className="col-xs-12 col-lg-12">
                                    Fecha: {this.state.fecha}
                                </div>

                                <div className="col-xs-12 col-lg-12">
                                    Usuario que captura: {this.state.usuario}
                                </div>
                            </div>
                            <div className="col-xs-9 col-lg-9">
                                <div className="col-xs-12 col-lg-12">
                                    <h6 className="font-weight-semibold">Convenio de Arranque</h6>
                                </div>

                                <div className="col-xs-12 col-lg-12">
                                    <p className="font-weight-semibold" style={{ color: '#617187' }}>PRESENTE </p>
                                </div>
                                <div className="col-xs-12 col-lg-12">
                                    <p ><span style={{ color: '#617187' }}>Estimad@ </span> <span className="font-weight-semibold">{this.state.nombreProspecto}: </span></p>
                                </div>

                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}> Es un privilegio para APS arrancar contigo este proyecto y acompañarte para el logro de todos tus objetivos; para lograrlo, es muy importante que establezcamos metas, avances y compromisos mutuos.  </p>
                                </div>

                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}> Como lo hemos platicado, parte importante de tu arranque y crecimiento en APS, implica que formes parte de un equipo de desarrollo que te acompañará en tu formación como Agente Novel y <span className="font-weight-semibold"><strong>{this.state.nombreGDD}</strong></span> será tu Gerente de Desarrollo.  </p>
                                </div>
                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>Nuestro compromiso, es ser un factor clave en tu formación y consolidación como Agente de Seguros en GNP, apoyándonos en dos pilares o filosofías fundamentales:</p>
                                </div>

                                <div className="col-xs-12 col-lg-12">
                                    <p ><h6 className="font-weight-semibold">1o Transformamos la vida de nuestros Agentes para cambiar la historia de nuestros clientes:</h6></p>
                                </div>
                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>Nuestra Misión es generar las mejores condiciones posibles para que logres un excelente futuro personal, profesional y económico, y esperamos que tú actúes de la misma forma, ayudando a tus prospectos y clientes a lograr sus objetivos de desarrollo económico y financiero a través de los productos de GNP.</p>
                                </div>
                                <div className="col-xs-12 col-lg-12">
                                    <p> <h6 className="font-weight-semibold">2o APS, el Mejor Equipo de Aseguradores de México:</h6></p>
                                </div>
                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>Aun teniendo prácticas sobresalientes dentro de la industria, ser los mejores, es nuestra más alta, genuina y sincera aspiración. Por ello nos esforzamos todos los días, convencidos de que necesitamos altos estándares de desempeño de todos los que formamos parte del equipo APS, y especialmente de los que como tú, se incorporan a nuestro “ejército de ángeles sin alas” en los equipos de ventas.</p>
                                </div>
                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>Sabemos que parte importante del atractivo de tu nueva carrera como Agente de Seguros es la independencia; el rendirte cuentas solo a ti mismo y a tus clientes; tomarás tus propias decisiones de negocios y vivirás con el resultado de esas decisiones. Queremos que tengas el privilegio de ser tu propio jefe, <span className="font-weight-semibold">y una vez que logres calificar dos veces a los Congresos de GNP, serás libre</span>, en la mayor parte, para tomar tus propias decisiones sobre cómo deseas dirigir tu negocio. Hasta ese entonces, sabemos que nuestra supervisión y experiencia en el desarrollo de profesionales de ventas de Seguro de Vida, nos pone en una mejor posición, para juzgar qué patrones de actividad y desempeño necesitas seguir para alcanzar tú potencial.</p>
                                </div>
                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>Para lograrlo hemos fijado guías específicas que te pediremos sigas puntualmente:</p>
                                </div>
                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>Dedicarte de  <span className="font-weight-semibold">tiempo completo y con todo tu entusiasmo</span>, a la promoción, asesoría y venta de los productos de Líneas Personales de GNP, condición para el éxito en nuestro negocio.</p>
                                </div>

                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>{this.state.capParametros[0]}</p>
                                </div>

                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>Con el claro objetivo de establecer tu propia fórmula de esfuerzo semanal, que será una de tus herramientas estratégicas de planeación y productividad, deberás reportar periódicamente las cifras específicas de tus actividades clave:</p>
                                </div>
                                <br />
                                <div className="col-xs-12 col-lg-12">
                                    {/*<img src={logoEstimadoInicial} class="img-fluid " width="1350px" height="132px" alt="" />*/}
                                    {<img src={logoEstimadoInicialPDF} style={{ textAlign: 'center' }} class="img-fluid rounded-circle" height="320px" width="960px" alt="" />}
                                </div>
                                <br></br>
                                <br></br>
                                <br></br>
                                <br></br>
                                <br></br>
                                <br></br>
                                <br></br>
                                <br></br>
                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}><span className="font-weight-semibold">Asistir puntualmente a todas las juntas </span> a las que te convoquen APS y GNP, pues son parte integral de tu capacitación, entrenamiento, integración y arraigo.</p>
                                </div>
                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>Tener un <span className="font-weight-semibold">alto nivel de efectividad y productividad</span> que garanticen que tu actividad es rentable, siempre buscando la obtención de todos los bonos de productividad trimestrales y cuatrimestrales, las campañas y promociones de GNP y de APS, así como asistir a los Congresos de GNP desde el inicio de tu actividad, todo esto a través de los ramos de Líneas Personales.</p>
                                </div>
                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>Las fases de tu desarrollo como Agente Profesional en Seguros, son las siguientes:</p>
                                </div>
                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>{this.state.capParametros[1]}</p>
                                </div>
                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>{this.state.capParametros[2]}{this.state.capParametros[3]}</p>
                                </div>
                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>{this.state.capParametros[4]}{this.state.capParametros[5]}</p>
                                </div>
                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>{this.state.capParametros[6]} ganar Congreso Platino / Calificar a la MDRT como aspirante</p>
                                </div>

                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>No lograr el requisito mínimo en cada una de las 3 primeras fases, normalmente es un indicador de que tu desempeño tenderá a ser pobre, y por ello, será causa para tu separación definitiva de nuestra Agencia y por lo tanto, de GNP.</p>
                                </div>
                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>A criterio del Director de Agencia y de tu Gerente de Desarrollo, se podrá aplicar un solo periodo adicional para el logro de los objetivos en estas 3 Fases.</p>
                                </div>
                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>APS buscará ser en todo momento, un factor que te impulse al logro de tus más altas aspiraciones personales, profesionales y económicas. Pondremos a tu disposición a tu Gerente de Desarrollo, responsable al 100% de tu capacitación, entrenamiento y supervisión, y a todo un equipo para darte apoyo en Operaciones, Seguimiento, Comunicación y Soporte a Ventas.</p>
                                </div>

                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>Tu plan de desarrollo con tu Gerente se integra, de forma general, de lo siguiente:</p>
                                </div>

                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>Capacitación en la metodología de ventas Ironsale</p>
                                </div>

                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>Acompañamiento a entrevistas de ventas como parte de tu programa de arranque, que implica: (6 semanas)</p>
                                </div>

                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>{this.state.capParametros[7]}</p>
                                </div>
                                <br></br>
                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>{this.state.capParametros[8]}</p>
                                </div>

                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>{this.state.capParametros[9]}</p>
                                </div>

                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>Es importante dar seguimiento a tu plan de capacitación y desarrollo en IDEAS GNP (Instituto de Especialidad a Agentes de Seguros):</p>
                                </div>

                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>Recibirás mensualmente tu boleta de IDEAS y el calendario de cursos</p>
                                </div>
                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>{this.state.capParametros[10]}{this.state.capParametros[11]}</p>
                                </div>
                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>Es indispensable que el equipo de capacitación APS te de a conocer los beneficios de ser congresista y saber la estategia para avanzar rápidamente en las designaciones</p>
                                </div>

                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>Además del apoyo de tu Gerente de Desarrollo, contarás con el soporte de los expertos en Gastos Médicos y Patrimoniales y del acompañamiento de nuestros Directivos para asesorarte en casos especiales, los cuales deberán ser cuidadosamente seleccionados por su complejidad, especialidad o cuantía.</p>
                                </div>
                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>Asimismo, APS dará seguimiento, asesoría y monitoreo de toda tu actividad y resultados.</p>
                                </div>
                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>Tendrás acceso a las oficinas APS en donde tendrás acceso a áreas comunes de trabajo y de capacitación, y contarás con servicios de conexión a internet, scanner, soporte técnico de cómputo y aplicaciones GNP, entre otros.</p>
                                </div>
                                <div className="col-xs-12 col-lg-12">
                                    <p style={{ color: '#617187' }}>Nosotros creemos en ti, sabemos que tienes todo para ser un Agente de Seguros exitoso, por lo que queremos continuar trabajando contigo para demostrarnos que juntos, podemos construir una base que te permita participar de las múltiples recompensas que brinda nuestra profesión.</p>
                                </div>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br></br>
                        <br></br>
                        <br></br>
                        <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                            <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                                <div className="col-xs-12 col-lg-12">
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-xs-12 col-lg-12">
                                                    <h6 className="font-weight-semibold"><strong>{this.state.nombreProspecto}</strong></h6>
                                                </div>
                                                <div className="col-xs-12 col-lg-12">
                                                    <hr class="my-4" />
                                                </div>
                                                <div className="col-xs-12 col-lg-12">
                                                    <h6 className="font-weight-semibold">AGENTE</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                            <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                                <div className="col-xs-12 col-lg-12">
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-xs-12 col-lg-12">
                                                    <h6 className="font-weight-semibold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                                <div className="col-xs-12 col-lg-12">
                                                    <hr class="my-4" />
                                                </div>
                                                <div className="col-xs-12 col-lg-12">
                                                    <h6 className="font-weight-semibold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row" style={{ display: 'inline-table', textAlign: 'center' }}>
                            <div className="row" style={{ display: 'inline-table', textAlign: 'center' }}>
                                <div className="col-xs-12 col-lg-12">
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-xs-12 col-lg-12">
                                                    <h6 className="font-weight-semibold"><strong>{this.state.nombreSDDC}</strong></h6>
                                                </div>
                                                <div className="col-xs-12 col-lg-12">
                                                    <hr class="my-4" />
                                                </div>
                                                <div className="col-xs-12 col-lg-12">
                                                    <h6 className="font-weight-semibold">SUBDIRECCIÓN DE DESARROLLO</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                            <div className="row" style={{ display: 'inline-table', textAlign: 'left' }}>
                                <div className="col-xs-12 col-lg-12">
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-xs-12 col-lg-12">
                                                    <h6 className="font-weight-semibold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                                <div className="col-xs-12 col-lg-12">
                                                    <hr class="my-4" />
                                                </div>
                                                <div className="col-xs-12 col-lg-12">
                                                    <h6 className="font-weight-semibold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row" style={{ display: 'inline-table', textAlign: 'right' }}>
                            <div className="row" style={{ display: 'inline-table', textAlign: 'right' }}>
                                <div className="col-xs-12 col-lg-12">
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-xs-12 col-lg-12">
                                                    <h6 className="font-weight-semibold"><strong>{this.state.nombreGDD}</strong></h6>
                                                </div>
                                                <div className="col-xs-12 col-lg-12">
                                                    <hr class="my-4" />
                                                </div>
                                                <div className="col-xs-12 col-lg-12">
                                                    <h6 className="font-weight-semibold">GERENTE DE DESARROLLO</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                }

            </div>
        )

    }
}