import React, { Component } from "react";
import axios from 'axios';
import HistoricoGeneral from "./HistoricoGeneral";
import datos from '../urls/datos.json'

export default class EntrevistaProfundaExplorando3 extends Component {

    constructor() {
        super();
        this.onChange = this.onChange.bind(this)
        this.lanzaModalContinuar = this.lanzaModalContinuar.bind(this)
        this.lanzaModalReserva = this.lanzaModalReserva.bind(this)
        this.construyeSelectMotivos = this.construyeSelectMotivos.bind(this)
        this.onChangeMotivos = this.onChangeMotivos.bind(this)
        this.peticionAReserva = this.peticionAReserva.bind(this)
        this.lanzaModalBaja=this.lanzaModalBaja.bind(this)
        this.peticionABaja=this.peticionABaja.bind(this)
        this.onClickBotonArchivoPuesto = this.onClickBotonArchivoPuesto.bind(this)
        this.onClickBotonArchivoPSP = this.onClickBotonArchivoPSP.bind(this)
        this.cargaInfoPSP = this.cargaInfoPSP.bind(this)
        this.cargaPdf14 = this.cargaPdf14.bind(this)
        this.state = {
            entrevistador:'',
            prospecto: {
                "id": null,
                "prospecto_id": null,
                "estatus_id": 7,
                "meta_profecional": null,
                "fortaleza_posees": null,
                "mejores_cualidades": null,
                "piensas_seguros": null,
                "areas_mejora": null,
                "persona_exitosa": null,
                "principal_motivacion": null,
                "hobbi_lectura": null,
                "hobbi_deportes": null,
                "hobbi_cine": null,
                "hobbi_teatro": null,
                "hobbi_viajes": null,
                "grande_sueno": null,
                "colega_cercano": null,
                "mayor_debilidad": null
            },
            selectMotivos: [],
            idMotivoBaja: 0,
            muestraInfoPSP: false,
            archivoPSP: undefined,
            muestraDocPSP: false,
            colorSubirArchivoPSP: "#E1E5F0",
            colorTextoSubirArchivoPSP: "#617187",
            colorBotonSubirArchivoPSP: "#313A46",
            nombreArchivoPSP: "",
            archivoPSP: undefined,
            textoPuesto: 'Subir archivo',

        };
    }

    UNSAFE_componentWillMount() {
        console.log("entrando es render de explorando ", this)
        this.construyeSelectMotivos(datos.urlServicePy+"parametros/api_cat_causas_reserva/0")

        fetch(datos.urlServicePy+"recluta/api_recluta_encuentro03/" + this.props.idProspecto)
            .then(response => response.json())
            .then(jsonData => {
                if (jsonData.length > 0) {
                    this.setState({ prospecto: jsonData[0] })
                }
            })
            fetch(datos.urlServicePy+"recluta/api_recluta_vcargapsppdf14/" + this.props.idProspecto)
            .then(response => response.json())
            .then(jsonRespuestaCarga => {
                console.log("respuesta de la carga ", jsonRespuestaCarga)
                if (jsonRespuestaCarga.length > 0) {
                    let interpretacionConcatenada=jsonRespuestaCarga[6] === undefined ?  "":jsonRespuestaCarga[6].respuesta
                    this.setState({
                        muestraInfoPSP: true,
                        rutaPSP: jsonRespuestaCarga[0].ironpsp,
                        fechaRespPSP: jsonRespuestaCarga[0].respuesta,
                        eficaciaVentas: jsonRespuestaCarga[1].porcentaje,
                        eficaciaEmpresarial: jsonRespuestaCarga[2].porcentaje,
                        rendimientoVentas: jsonRespuestaCarga[3].porcentaje,
                        muestraDocPSP: true,
                        ventaDominante:jsonRespuestaCarga[4].respuesta,
                        interpretacion:jsonRespuestaCarga[5].respuesta + " "+interpretacionConcatenada ,
                        colorSubirArchivoPSP: "#78CB5A", colorTextoSubirArchivoPSP: "#FFFFFF", colorBotonSubirArchivoPSP: "#78CB5A"
                    })
                }
            })
            fetch(datos.urlServicePy+"parametros/api_recluta_prospectos/" + this.props.idProspecto)
            .then(response => response.json())
            .then(recluta_prospecto => {
                console.log("recluta_propspecto", recluta_prospecto)
                fetch(datos.urlServicePy+"parametros/api_cat_empleados/" + recluta_prospecto.filas[0].fila[12].value)
                .then(response => response.json())
                .then(entrevistador => {
                        console.log("entrevistador ", entrevistador)
                        this.setState({entrevistador:entrevistador.filas[0].fila[2].value + " "+ entrevistador.filas[0].fila[3].value + " " + (entrevistador.filas[0].fila[4].value != null ? entrevistador.filas[0].fila[4].value:'') })
                })
            })
    }

    onChange = e => {
        let pros = this.state.prospecto
        pros["" + e.target.name + ""] = e.target.value
        this.setState({ prospecto: pros })
    }

    lanzaModalContinuar() {
        this.modalInicio.click()
    }
    lanzaModalReserva() {
        this.reserva.click()
    }


    lanzaModalBaja() {
        this.baja.click()
    }

    construyeSelectMotivos(url) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                console.log("respuesta  ", json)
                for (var i = 0; i < filas.length; i++) {
                    let fila = filas[i]

                    options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                }
                let salida = []
                salida.push(<select onChange={this.onChangeMotivos} name="selectMotivos" style={{ borderColor: '#F1F3FA' }} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ selectMotivos: salida })


            })
    }

    onChangeMotivos = e => {
        if (e.target.name == 'selectMotivos') {
            this.setState({ idMotivoBaja: parseInt(e.target.value) })
        } else {
            this.setState({ [e.target.name]: e.target.value })

        }
    }


    peticionAReserva() {
        let json = {
            "estatus_id": 18,
            "causas_baja_id": this.state.idMotivoBaja

        }
        const requestOptions = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };
        console.log("enviando a reserva")

        fetch(datos.urlServicePy+"parametros_upd/api_recluta_prospectos/" + this.props.idProspecto, requestOptions)
            .then(response => response.json())
            .then(data => {
                //  this.construyeTarjetas()
                this.props.botonCancelar()
            })

    }

    peticionABaja() {
        let json = {
            "estatus_id": 19,
            "causas_baja_id": this.state.idMotivoBaja
        }
        const requestOptions = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };
        console.log("enviando a reserva")

        fetch(datos.urlServicePy+"parametros_upd/api_recluta_prospectos/" + this.props.idProspecto, requestOptions)
            .then(response => response.json())
            .then(data => {
                //this.construyeTarjetas()
                this.props.botonCancelar()
            })
    }


    onChangeFilePuesto(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file de puesto", file);
        this.setState({ archivoPuesto: file, colorSubirArchivo: "#78CB5A", colorTextoSubirArchivo: "#FFFFFF", colorBotonSubirArchivo: "#E5F6E0", nombreArchivoPuesto: file.name })
        // this.props.guardaArchivo(file, "archivoPuesto")

    }


    onClickBotonArchivoPuesto() {
        console.log("entrnaod a la imagen")
        this.upload.click()
    }



    onClickBotonArchivoPSP() {
        console.log("entrnaod a la imagen")
        this.upload.click()
    }


    cargaInfoPSP(json) {
        const requestOptions = {
            method: "POST",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };

        fetch(datos.urlServicePy+"recluta/api_recluta_cargapsppdf/0", requestOptions)
        .then(response => response.json())
        .then(data => {
            console.log("la respuesta de la carga", data)
            this.cargaPdf14()
        });
    }

    cargaPdf14(){
        fetch(datos.urlServicePy+"recluta/api_recluta_vcargapsppdf14/" + this.props.idProspecto)
        .then(response => response.json())
        .then(jsonRespuestaCarga => {
            console.log("respuesta de la carga ", jsonRespuestaCarga)
            let interpretacionConcatenada=jsonRespuestaCarga[6] === undefined ?  "":jsonRespuestaCarga[6].respuesta

            this.setState({
                muestraInfoPSP: true,
                fechaRespPSP: jsonRespuestaCarga[0].respuesta,
                eficaciaVentas: jsonRespuestaCarga[1].porcentaje,
                eficaciaEmpresarial: jsonRespuestaCarga[2].porcentaje,
                rendimientoVentas: jsonRespuestaCarga[3].porcentaje,
                ventaDominante:jsonRespuestaCarga[4].respuesta,
                interpretacion:jsonRespuestaCarga[5].respuesta + " "+interpretacionConcatenada,
            })
        })
    }

    onChangeFilePSP(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file de puesto", file);

        //lanzamos el envio del psp 
        let formData = new FormData();
        const config = {
            headers: { 'content-type': 'multipart/form-data' }
        }
        
        if(this.state.muestraDocPSP == true){
            formData.append('ironpsp', file);
            axios.put(datos.urlServicePy+"api/doc_ironpsp/"+this.props.idProspecto+"/", formData, config)
            .then(res => {
                console.log("respuesta  del archivo cargado", res.data);
                this.cargaInfoPSP(res.data)
                let ruta = res.data.ironpsp
                let rutaCorrecta = ruta.replace("8000/media", "8001/documentos")
                rutaCorrecta = rutaCorrecta.replace("http", "https")
                console.log(rutaCorrecta)
                this.setState({ rutaPSP: rutaCorrecta, archivoPSP: file, colorSubirArchivoPSP: "#78CB5A", colorTextoSubirArchivoPSP: "#FFFFFF", colorBotonSubirArchivoPSP: "#78CB5A", nombreArchivoPSP: file.name, muestraDocPSP: true })
            })
        } else {
            formData.append('id', this.props.idProspecto);
            formData.append('ironpsp', file);
            formData.append('prospecto_id', this.props.idProspecto);
            axios.post(datos.urlServicePy+"api/doc_ironpsp/", formData, config)
            .then(res => {
                console.log("respuesta  del archivo cargado", res.data);
                this.cargaInfoPSP(res.data)
                let ruta = res.data.ironpsp
                let rutaCorrecta = ruta.replace("8000/media", "8001/documentos")
                rutaCorrecta = rutaCorrecta.replace("http", "https")
                console.log(rutaCorrecta)
                this.setState({ rutaPSP: rutaCorrecta, archivoPSP: file, colorSubirArchivoPSP: "#78CB5A", colorTextoSubirArchivoPSP: "#FFFFFF", colorBotonSubirArchivoPSP: "#78CB5A", nombreArchivoPSP: file.name, muestraDocPSP: true })
            })
        }
        
    }


    render() {
        return (
            <div>
                <button type="button" style={{ display: 'none' }} ref={(ref) => this.modalInicio = ref} data-toggle="modal" data-target="#modal-Continuar"   ></button>
                <button type="button" style={{ display: 'none' }} ref={(ref) => this.reserva = ref} data-toggle="modal" data-target="#modal-reserva"   ></button>
                <button type="button" style={{ display: 'none' }} ref={(ref) => this.baja = ref} data-toggle="modal" data-target="#modal-baja"   ></button>


                <div id="modal-psp" className="modal fade" tabindex="-1">
                    <div className="modal-dialog modal-full">
                        <div className="modal-content text-white" style={{ backgroundColor: "#313A46" }}>
                            <div className="modal-header text-white" style={{ backgroundColor: "#8189D4" }}>
                                <h6 className="modal-title">Documento PSP </h6>
                                <button type="button" className="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div className="modal-body">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-xs-12 col-lg-4"><>&nbsp;&nbsp;</></div>
                                        <div className="col-xs-12 col-lg-4">
                                            <div className="input-group">
                                                <form encType="multipart/form" style={{ display: 'none' }} >
                                                    <input type="file" style={{ display: 'none' }} ref={(ref) => this.upload = ref} onChange={this.onChangeFilePSP.bind(this)}></input>
                                                </form>
                                                <input type="text" style={{ borderColor: 'black', backgroundColor: "#313A46", color: 'white' }} className="form-control " placeholder="Archivo PSP" value={this.state.nombreArchivoPSP} />
                                                <span className="input-group-prepend" style={{ backgroundColor: this.state.colorBotonSubirArchivoPSP }}>
                                                    {/*<button type="button" class="btn text-white" onClick={this.onClickBotonArchivoPSP} style={{ backgroundColor: this.state.colorBotonSubirArchivoPSP }}>
                                                        <h10 style={{ color: "white" }}>+</h10>
                                                         </button>*/}

                                                    {
                                                                this.state.muestraDocPSP == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoPSP }}>
                                                                <a href={this.state.rutaPSP} target="_blank" rel="noopener noreferrer" >
                                                                    <i className="fas fa-eye"
                                                                        style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h10 onClick={this.onClickBotonArchivoPSP} style={{ color: "white" }}>+</h10>
                                                            </button> : <button type="button" class="btn text-white" onClick={this.onClickBotonArchivoPSP} style={{ backgroundColor: this.state.colorBotonSubirArchivoPSP }}>
                                                                <h10 style={{ color: "white" }}>+</h10>
                                                            </button>

                                                    }



                                                    <span className="input-group-text" style={{ background: this.state.colorBotonSubirArchivoPSP, borderColor: this.state.colorBotonSubirArchivoPSP, color: "white" }} >{this.state.textoPuesto}</span>
                                                </span>
                                            </div>
                                        </div>
                                        <div className="col-xs-12 col-lg-4"><>&nbsp;&nbsp;</></div>
                                    </div>

                                    <br></br>
                                    <br></br>
                                    {this.state.muestraInfoPSP == true &&
                                        <div>
                                            <div className="row">
                                                <div className="col-xs-6 col-lg-6">
                                                    <h8 class="mb-0 font-weight-semibold text-white"> Resultados PSP</h8>
                                                </div>
                                                <div className="col-xs-4 col-lg-4">
                                                    <table className="table datatable-sorting  table-striped table-hover" style={{ backgroundColor: "#313A46", borderColor: '#313A46' }}>
                                                        <thead style={{ backgroundColor: "#313A46" }}>
                                                            <tr>
                                                                <th className="text-left font-weight-bold text-white" style={{ backgroundColor: '#313A46' }} >
                                                                    <a>ALTO</a>
                                                                </th>
                                                                <th className="text-center font-weight-bold text-white" style={{ backgroundColor: '#313A46' }} >
                                                                    <a>MODERADO</a>
                                                                </th>
                                                                <th className="text-right font-weight-bold text-white" style={{ backgroundColor: '#313A46' }} >
                                                                    <a>BAJO</a>
                                                                </th>
                                                            </tr>
                                                        </thead>

                                                    </table>
                                                </div>
                                            </div>

                                            <div className="row">
                                                <div className="col-xs-4 col-lg-4">
                                                    <div className="form-group">
                                                        <div className="input-group">
                                                            <span className="input-group-prepend"> <span
                                                                className="input-group-text text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }}>Fecha de respuesta del PSP</span>
                                                            </span> <input type="text" placeholder="" value={this.state.fechaRespPSP} className="form-control text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }} />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-xs-2 col-lg-2">
                                                    <h8 class=" text-white"> Eficacia en ventas</h8>
                                                </div>

                                                <div className="col-xs-4 col-lg-4">

                                                    <input type="range" class="custom-range" min="0" name="salud" max="100" step={50} value={this.state.eficaciaVentas} id="salud" />
                                                </div>


                                                <div className="col-xs-2 col-lg-2">




                                                    {
                                                        this.state.eficaciaVentas == 0 &&
                                                        <div class="mr-3 ">
                                                            <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                                    class="letter-icon text-white"></span>
                                                            </a>
                                                        </div>

                                                    }

                                                    {
                                                        this.state.eficaciaVentas == 50 &&
                                                        <div className="row">
                                                          
                                                          {/*  <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                    </div>*/}

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                        </div>
                                                    }


                                                    {
                                                        this.state.eficaciaVentas == 100 &&
                                                        <div className="row">

                                                           {/*<div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                    </div>*/}

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FD0303', borderColor: '#FD0303' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                        </div>
                                                    }





                                                </div>


                                            </div>

                                            <div className="row">

                                                <div className="col-xs-12 col-lg-4">
                                                    <div className="form-group">
                                                        <div className="input-group">
                                                            <span className="input-group-prepend"> <span
                                                                className="input-group-text text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }}>Estilo de venta dominante&ensp;</span>
                                                            </span> <input type="text" placeholder="" value={this.state.ventaDominante} className="form-control text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }} />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-xs-12 col-lg-2">
                                                    <h8 class=" text-white"> Eficacia empresarial</h8>
                                                </div>

                                                <div className="col-xs-12 col-lg-4">
                                                    <input type="range" class="custom-range" min="0" name="salud" max="100" step={50} value={this.state.eficaciaEmpresarial} id="salud" />
                                                </div>


                                                <div className="col-xs-2 col-lg-2">

                                                    {
                                                        this.state.eficaciaEmpresarial == 0 &&
                                                        <div class="mr-3 ">
                                                            <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                                    class="letter-icon text-white"></span>
                                                            </a>
                                                        </div>

                                                    }

                                                    {
                                                        this.state.eficaciaEmpresarial == 50 &&
                                                        <div className="row">
                                                           
                                                           {/* <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>*/}

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                        </div>
                                                    }


                                                    {
                                                        this.state.eficaciaEmpresarial == 100 &&
                                                        <div className="row">

                                                          {/*  <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                    </div>*/}

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FD0303', borderColor: '#FD0303' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                        </div>
                                                    }



                                                </div>



                                            </div>

                                            <div className="row">
                                                <div className="col-xs-12 col-lg-4">
                                                    <div className="form-group">
                                                        <div className="input-group">
                                                            <span className="input-group-prepend"> <span
                                                                className="input-group-text text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }}>Interpretación de precisión</span>
                                                            </span> <textarea disabled={true} type="text" placeholder="" value={this.state.interpretacion} className="form-control text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }} />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-xs-12 col-lg-2">
                                                    <h8 class=" text-white">Rendimiento de ventas general esperado</h8>
                                                </div>

                                                <div className="col-xs-12 col-lg-4">
                                                    <input type="range" class="custom-range" min="0" name="salud" max="100" step={50} value={this.state.rendimientoVentas} id="salud" />
                                                </div>

                                                <div className="col-xs-2 col-lg-2">

                                                    {
                                                        this.state.rendimientoVentas == 0 &&
                                                        <div class="mr-3 ">
                                                            <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                                    class="letter-icon text-white"></span>
                                                            </a>
                                                        </div>

                                                    }

                                                    {
                                                        this.state.rendimientoVentas == 50 &&
                                                        <div className="row">
                                                          
                                                          {/*  <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                    </div>*/}

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                        </div>
                                                    }


                                                    {
                                                        this.state.rendimientoVentas == 100 &&
                                                        <div className="row">

                                                         {/*   <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>*/}

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FD0303', borderColor: '#FD0303' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                        </div>
                                                    }



                                                </div>


                                            </div>
                                        </div>

                                    }







                                </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-center">
                                <button type="button" class="btn text-white" data-dismiss="modal" style={{ backgroundColor: '#617187', borderColor: '#617187' }}>Cancelar</button>

                                {
                                    this.state.muestraDocPSP == true ? <button type="button" class="btn text-white " disabled={false} style={{ backgroundColor: '#617187', borderColor: '#617187' }}>
                                        <a href={this.state.rutaPSP} style={{ color: 'white' }} target="_blank" rel="noopener noreferrer" >
                                            Ver PSP
                                        </a>
                                    </button> : <button type="button" class="btn text-white " disabled={true} style={{ backgroundColor: '#617187', borderColor: '#617187' }}>
                                        <a href="#" style={{ color: 'white' }} target="_blank" rel="noopener noreferrer" >
                                            Ver PSP
                                        </a>

                                    </button>
                                }

                                <button type="button" class="btn text-white" data-dismiss="modal" style={{ backgroundColor: '#617187', borderColor: '#617187' }}>Aceptar</button>
                            </div>
                        </div>
                    </div>
                </div>


                <div id="modal-Continuar" className="modal fade" tabindex="-1">
                    <div className="modal-dialog ">
                        <div className="modal-content text-white" style={{ backgroundColor: "#FFFFF" }}>
                            <div className="modal-header text-white text-center" style={{ backgroundColor: "#FFFFF" }}>
                            </div>
                            <div className="modal-body">
                                <div className="card-body">

                                    <h6 className="col-12 text-center text-dark font-weight-semibold">Desea continuar con la prueba?</h6>

                                </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-center">
                                <div className="col-12">
                                    <div className="row">
                                        <button type="button" style={{ width: '100%', backgroundColor: '#8189D4' }} class="btn text-white" onClick={(e) => this.props.muestraForm4Explorando(this.state.prospecto)} data-dismiss="modal"  >Sí, continuar </button>
                                    </div>
                                </div>

                                <br></br>
                                <br></br>
                                <br></br>
                                <div className="col-12">
                                    <div className="row">
                                        <div className="col-6">
                                            <button type="button" style={{ width: '100%', backgroundColor: "#FFFF", borderColor: 'red', textEmphasisColor: 'red' }} class="btn text-red" onClick={(e) => { this.lanzaModalReserva() /*this.props.enviarReserva(this.props.idProspecto)*/ }} data-dismiss="modal"  ><span style={{ color: 'red' }}>Enviar a reserva</span></button>
                                        </div>

                                        <div className="col-6">
                                            <button type="button" style={{ width: '100%', backgroundColor: "#FFFF", borderColor: '#617187' }} class="btn text-white" onClick={(e) => { this.lanzaModalBaja()/*this.props.enviarBaja(this.props.idProspecto) */}} data-dismiss="modal"><span style={{ color: '#617187' }}>Dar de baja</span></button>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>
                </div>


                <div id="modal-reserva" className="modal fade" tabindex="-1">
                    <div className="modal-dialog ">
                        <div className="modal-content text-white" style={{ backgroundColor: "#FFFFF" }}>
                            <div className="modal-header text-white text-center" style={{ backgroundColor: "#617187" }}>
                                <h6 className="modal-title col-12 text-center">Enviar a reserva</h6>

                            </div>
                            <div className="modal-body">
                                <div className="card-body">
                                    <h8 style={{ color: '#8F9EB3' }}>Si está seguro de enviar a reserva el registro, por favor seleccione el motivo</h8>
                                    {this.state.selectMotivos}
                                    <br></br>
                                    <h8 style={{ color: '#8F9EB3' }}>Fecha acordada para retomar el proceso</h8>
                                    <input type="date" placeholder="Escribir" name="fecha_alta" className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />

                                </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-center">
                                <div className="col-12">
                                    <div className="row">
                                        <button type="button" style={{ width: '100%', backgroundColor: "#617187" }} class="btn text-white" onClick={this.peticionAReserva} data-dismiss="modal"  >Enviar a reserva</button>
                                    </div>
                                    <br></br>
                                    <div className="row">
                                        <button type="button" style={{ width: '100%', backgroundColor: '#8F9EB3' }} class="btn text-white" id="btnAgregar" data-dismiss="modal"  >Cancelar</button>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                <div id="modal-baja" className="modal fade" tabindex="-1">
                    <div className="modal-dialog ">
                        <div className="modal-content text-white" style={{ backgroundColor: "#FFFFF" }}>
                            <div className="modal-header text-white text-center" style={{ backgroundColor: "#617187" }}>
                                <h6 className="modal-title col-12 text-center">Dar baja el registro</h6>

                            </div>
                            <div className="modal-body">
                                <div className="card-body">
                                    <h8 style={{ color: '#8F9EB3' }}>Si está seguro de descartar el registro, por favor seleccione el motivo</h8>
                                    {this.state.selectMotivos}


                                </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-center">
                                <div className="col-12">
                                    <div className="row">
                                        <button type="button" style={{ width: '100%', backgroundColor: "#617187" }} class="btn text-white" onClick={this.peticionABaja} data-dismiss="modal" >Dar de baja registro</button>
                                    </div>
                                    <br></br>
                                    <div className="row">
                                        <button type="button" style={{ width: '100%', backgroundColor: '#8F9EB3' }} class="btn text-white" id="btnAgregar" data-dismiss="modal"  >Cancelar</button>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>




                <div class="card">

                    <div class="row mb-2 mt-2 ml-1">
                        <div class="col-12 col-lg-9">
                            <div class="row">

                                <div class="col-6 col-lg-1 d-flex align-items-center pb-sm-1">
                                    <div class="mr-2">
                                        <a href="#"
                                            class="btn rounded-pill btn-icon btn-sm" style={{ backgroundColor: '#78CB5A' }}> <span
                                                class="letter-icon text-white"><i className="icon-checkmark2"></i></span>
                                        </a>
                                    </div>
                                </div>

                                <div class="col-6 col-lg-1 d-flex align-items-center pb-sm-1">
                                    <div class="mr-2">
                                        <a href="#"
                                            class="btn  rounded-pill btn-icon btn-sm " style={{ backgroundColor: '#8189D4', borderColor: '#8189D4' }}> <span style={{ color: 'white' }}
                                                class="letter-icon">EP</span>
                                        </a>
                                    </div>

                                </div>



                                <div class="col-6 col-lg-7 d-flex align-items-center pb-sm-1">

                                    <div class="mr-5 ">
                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8189D4', borderColor: '#8189D4' }}
                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                class="letter-icon text-white">1</span>
                                        </a>
                                    </div>

                                    <div class="mr-5 ">
                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8189D4', borderColor: '#8189D4' }}
                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                class="letter-icon text-white">2</span>
                                        </a>
                                    </div>

                                    <div class="mr-5 ">
                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8189D4', borderColor: '#8189D4' }}
                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                class="letter-icon text-white">3</span>
                                        </a>
                                    </div>

                                    <div class="mr-5">
                                        <a href="#" style={{ height: '12px', width: '12px', borderColor: '#C4D3E8' }}
                                            class="btn rounded-pill btn-icon btn-sm "> <span
                                                class="letter-icon"></span>
                                        </a>
                                    </div>

                                    <div class="mr-5">
                                        <a href="#" style={{ height: '12px', width: '12px', borderColor: '#C4D3E8' }}
                                            class="btn rounded-pill btn-icon btn-sm "> <span
                                                class="letter-icon"></span>
                                        </a>
                                    </div>
                                    <div class="mr-5">
                                        <a href="#" style={{ height: '12px', width: '12px', borderColor: '#C4D3E8' }}
                                            class="btn rounded-pill btn-icon btn-sm "> <span
                                                class="letter-icon"></span>
                                        </a>
                                    </div>

                                    <div class="mr-5">
                                        <a href="#" style={{ height: '12px', width: '12px', borderColor: '#C4D3E8' }}
                                            class="btn rounded-pill btn-icon btn-sm "> <span
                                                class="letter-icon"></span>
                                        </a>
                                    </div>

                                    <div class="mr-5">
                                        <a href="#" style={{ height: '12px', width: '12px', borderColor: '#C4D3E8' }}
                                            class="btn rounded-pill btn-icon btn-sm "> <span
                                                class="letter-icon"></span>
                                        </a>
                                    </div>
                                    <div class="mr-5">
                                        <a href="#" style={{ height: '12px', width: '12px', borderColor: '#C4D3E8' }}
                                            class="btn rounded-pill btn-icon btn-sm "> <span
                                                class="letter-icon"></span>
                                        </a>
                                    </div>

                                    <div class="mr-5">
                                        <a href="#" style={{ height: '12px', width: '12px', borderColor: '#C4D3E8' }}
                                            class="btn rounded-pill btn-icon btn-sm "> <span
                                                class="letter-icon"></span>
                                        </a>
                                    </div>





                                </div>


                                &ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;    
                                        <div class="d-flex justify-content-between">
                                            <div class="mr-2">
                                                <a href="#"
                                                    class="btn  rounded-pill btn-icon btn-sm" style={{ borderColor: '#5B96E2' }}> <span style={{ color: '#5B96E2' }}
                                                        class="letter-icon">VC</span>
                                                </a>
                                            </div>

                                        </div>
                                        <div class="d-flex justify-content-between">
                                            <div class="mr-2">
                                                <a href="#"
                                                    class="btn  rounded-pill btn-icon btn-sm" style={{ borderColor: '#41B3D1' }}> <span style={{ color: '#41B3D1' }}
                                                        class="letter-icon">CC</span>
                                                </a>
                                            </div>

                                        </div>
                                        <div class="d-flex justify-content-between">
                                            <div class="mr-2">
                                                <a href="#"
                                                    class="btn  rounded-pill btn-icon btn-sm" style={{ borderColor: '#78CB5A' }}> <span style={{ color: '#78CB5A' }}
                                                        class="letter-icon">C</span>
                                                </a>
                                            </div>
                                        </div>
                            </div>
                        </div>


                        <div class="col-12 col-lg-3">
                            <div class="float-right mr-3">
                                <button type="button" title="Guardar y Regresar" onClick={(e) => this.props.regresaForm2explorando()} class="btn rounded-pill mr-1" style={{ backgroundColor: '#8F9EB3', borderColor: '#8F9EB3' }}><span><i style={{ color: 'white' }} className="icon-backward2"></i></span></button>
                                <button type="button" title="Registrar entrevista" onClick={(e) => this.props.muestraForm4Explorando(this.state.prospecto) /*this.lanzaModalContinuar()*/} disabled={
                                    this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                    && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER" || (
                                    this.state.prospecto.meta_profecional == null || this.state.prospecto.meta_profecional == '' ||
                                    this.state.prospecto.fortaleza_posees == null || this.state.prospecto.fortaleza_posees == '' ||
                                    this.state.prospecto.mejores_cualidades == null || this.state.prospecto.mejores_cualidades == '' ||
                                    this.state.prospecto.piensas_seguros == null || this.state.prospecto.piensas_seguros == '' ||
                                    this.state.prospecto.areas_mejora == null || this.state.prospecto.areas_mejora == '' ||
                                    this.state.prospecto.persona_exitosa == null || this.state.prospecto.persona_exitosa == '' ||
                                    this.state.prospecto.principal_motivacion == null || this.state.prospecto.principal_motivacion == '' ||
                                    this.state.prospecto.grande_sueno == null || this.state.prospecto.grande_sueno == '' ||
                                    this.state.prospecto.colega_cercano == null || this.state.prospecto.colega_cercano == '' ||
                                    this.state.prospecto.mayor_debilidad == null || this.state.prospecto.mayor_debilidad == '')


                                } class="btn rounded-pill mr-1" style={{ backgroundColor: '#8F9EB3', borderColor: '#8F9EB3' }}><span><i style={{ color: 'white' }} className="icon-forward3"></i></span></button>
                            </div>

                        </div>
                    </div>

                </div>
                <div class="card-header bg-transparent header-elements-sm-inline">
                    <h3 class="mb-0 font-weight-semibold" style={{ color: '#617187' }}>Entrevista Profunda - PSP & IRON TALENT </h3>
                    <div class="header-elements">
                        <ul class="list-inline mb-0">
                        <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Prospecto: <strong >{this.props.referido.nombre+' '+this.props.referido.ape_paterno + ' ' + (this.props.referido.ape_materno != null ? this.props.referido.ape_materno:'')}</strong></li>
                     <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Fecha de alta: <strong >{this.props.referido.ts_alta_audit}</strong></li>
                            <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Usuario creador: <strong >{this.state.entrevistador}</strong></li>
                            <li class="list-inline-item">
                                <h6 style={{ backgroundColor: "#8189D4", color: '#FFFF', borderRadius: '8px' }}> <>&nbsp;&nbsp;</> Entevista Profunda  <>&nbsp;&nbsp;</> </h6>
                            </li>
                        </ul>
                    </div>
                </div>
                <br></br>



                <div class="card-header bg-transparent header-elements-sm-inline" style={{ border: 'none' }}>
                    <h4 class="mb-0 font-weight-semibold" >IronTalent 2022</h4>
                    <div class="header-elements">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item">
                            {this.state.muestraInfoPSP == false &&
                                    <button data-toggle="modal" data-target="#modal-psp" 
                                    disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} class="btn  mr-1" style={{ backgroundColor: "#8189D4", color: '#FFFF' }}> <>&nbsp;&nbsp;</> Subir archivo PSP<>&nbsp;&nbsp;</> </button>
                                }
                                {this.state.muestraInfoPSP == true &&
                                    <button data-toggle="modal" data-target="#modal-psp" 
                                    disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} class="btn  mr-1" style={{ backgroundColor: "#78CB5A", color: '#FFFF' }}> <>&nbsp;&nbsp;</> Ver archivo PSP<>&nbsp;&nbsp;</> </button>
                                }
                            </li>
                        </ul>
                    </div>
                </div>

                <br></br>

                <div class="card">
                    <div class="card-header bg-transparent header-elements-sm-inline">
                        <h6 class="mb-0 font-weight-semibold">03. Explorando</h6>
                        <div class="header-elements">
                            <ul class="list-inline mb-0">
                                <li class="list-inline-item">
                                    <h6 class="mb-0 font-weight-semibold"> <>&nbsp;&nbsp;</> 03/10  <>&nbsp;&nbsp;</> </h6>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="card-body">


                        <div className="row">
                            <div className="col-xs-12 col-lg-12">
                                <div className="form-group">
                                    <span >
                                        <span className="font-weight-semibold"> Háblame de la última meta profesional que te fijaste, ¿la alcanzaste? ¿cómo?</span>
                                    </span>
                                    <div className="input-group">
                                        <input type="text" placeholder="Escribir" name="meta_profecional" 
                                        disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                        && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.meta_profecional} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                    </div>
                                    {
                                            this.state.prospecto.meta_profecional == null || this.state.prospecto.meta_profecional == "" ?
                                                <span style={{ color: "red" }}>El campo Háblame de la última meta profesional que te fijaste, debe ser ingresado</span> : ''
                                        }
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-xs-12 col-lg-12">
                                <div className="form-group">
                                    <span >
                                        <span className="font-weight-semibold"> ¿Cuál es la principal fortaleza que posees y que consideres te puede ayudar a tener éxito en una posición comercial?</span>
                                    </span>
                                    <div className="input-group">
                                        <input type="text" placeholder="Escribir" name="fortaleza_posees" 
                                        disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                        && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.fortaleza_posees} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                    </div>
                                    {
                                            this.state.prospecto.fortaleza_posees == null || this.state.prospecto.fortaleza_posees == "" ?
                                                <span style={{ color: "red" }}>La pregunta ¿Cuál es la principal fortaleza que posees y que consideres te puede ayudar a tener éxito en una posición comercial?, debe ser ingresada</span> : ''
                                        }
                                </div>
                            </div>
                        </div>


                        <div className="row">
                            <div className="col-xs-12 col-lg-12">
                                <div className="form-group">
                                    <span >
                                        <span className="font-weight-semibold">Hablando en términos generales, ¿Cuáles piensas que son tus mejores cualidades? ¿Y además de esas?</span>
                                    </span>
                                    <div className="input-group">
                                        <input type="text" placeholder="Escribir" name="mejores_cualidades" 
                                        disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.mejores_cualidades} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                    </div>
                                    {
                                            this.state.prospecto.mejores_cualidades == null || this.state.prospecto.mejores_cualidades == "" ?
                                                <span style={{ color: "red" }}>La pregunta ¿Cuáles piensas que son tus mejores cualidades?, debe ser ingresada</span> : ''
                                        }
                                </div>
                            </div>
                        </div>




                        <div className="row">
                            <div className="col-xs-6 col-lg-6">
                                <div className="form-group">
                                    <span >
                                        <span className="font-weight-semibold">¿Qué piensas de los seguros?</span>
                                    </span>
                                    <div className="input-group">
                                        <input type="text" placeholder="Escribir" name="piensas_seguros" 
                                        disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.piensas_seguros} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                    </div>
                                    {
                                            this.state.prospecto.piensas_seguros == null || this.state.prospecto.piensas_seguros == "" ?
                                                <span style={{ color: "red" }}>La pregunta ¿Qué piensas de los seguros?, debe ser ingresada</span> : ''
                                        }
                                </div>
                            </div>

                            <div className="col-xs-6 col-lg-6">
                                <div className="form-group">
                                    <span >
                                        <span className="font-weight-semibold">Continuando con las generalidades ¿En qué áreas considerarías mejorar?</span>
                                    </span>
                                    <div className="input-group">
                                        <input type="text" placeholder="Escribir" name="areas_mejora" 
                                        disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.areas_mejora} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                    </div>
                                    {
                                            this.state.prospecto.areas_mejora == null || this.state.prospecto.areas_mejora == "" ?
                                                <span style={{ color: "red" }}>La pregunta ¿En qué áreas considerarías mejorar?, debe ser ingresada</span> : ''
                                        }
                                </div>
                            </div>
                        </div>




                    </div>






                </div>


                <div class="card">
                    <div className="card-body">
                        <div className="row">

                            <div className="col-xs-12 col-lg-6">
                                <div className="form-group">
                                    <span >
                                        <span className="font-weight-semibold"> ¿Cuándo una persona es exitosa, según tu opinión?</span>
                                    </span>
                                    <div className="input-group">
                                        <input type="text" placeholder="Escribir" name="persona_exitosa" 
                                        disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                        && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.persona_exitosa} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                    </div>
                                    {
                                            this.state.prospecto.persona_exitosa == null || this.state.prospecto.persona_exitosa == "" ?
                                                <span style={{ color: "red" }}>La pregunta ¿Cuándo una persona es exitosa, según tu opinión?, debe ser ingresada</span> : ''
                                        }
                                </div>
                            </div>

                            <div className="col-xs-12 col-lg-6">
                                <div className="form-group">
                                    <span >
                                        <span className="font-weight-semibold">¿Cuál es tu principal motivación?</span>
                                    </span>
                                    <div className="input-group">
                                        <input type="text" placeholder="Escribir" name="principal_motivacion" 
                                        disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                        && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.principal_motivacion} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                    </div>
                                    {
                                            this.state.prospecto.principal_motivacion == null || this.state.prospecto.principal_motivacion == "" ?
                                                <span style={{ color: "red" }}>La pregunta ¿Cuál es tu principal motivación?, debe ser ingresada</span> : ''
                                        }
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-xs-12 col-lg-12">
                                <div className="form-group">
                                    <span >
                                        <span className="font-weight-semibold">¿Qué tipo de actividades disfrutas más en tu tiempo libre? Hobbies</span>
                                    </span>
                                    <div className="input-group">
                                        <input type="text" placeholder="Nombre" name="hobbi_lectura" 
                                        disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                        && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.hobbi_lectura} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                    </div>
                                    {
                                            this.state.prospecto.hobbi_lectura == null || this.state.prospecto.hobbi_lectura == "" ?
                                                <span style={{ color: "red" }}>La pregunta ¿Qué tipo de actividades disfrutas más en tu tiempo libre?, debe ser ingresada</span> : ''
                                        }
                                </div>
                            </div>
                        </div>

                        <div className="row">

                            <div className="col-xs-12 col-lg-6">
                                <div className="form-group">
                                    <span >
                                        <span className="font-weight-semibold"> ¿Cuál es tu más grande sueño?</span>
                                    </span>
                                    <div className="input-group">
                                        <input type="text" placeholder="Escribir" name="grande_sueno" 
                                        disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                        && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.grande_sueno} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                    </div>
                                    {
                                            this.state.prospecto.grande_sueno == null || this.state.prospecto.grande_sueno == "" ?
                                                <span style={{ color: "red" }}>La pregunta ¿Cuál es tu más grande sueño?, debe ser ingresada</span> : ''
                                        }
                                </div>
                            </div>

                            <div className="col-xs-12 col-lg-6">
                                <div className="form-group">
                                    <span >
                                        <span className="font-weight-semibold">¿Cómo te describiría tu colega más cercano?</span>
                                    </span>
                                    <div className="input-group">
                                        <input type="text" placeholder="Escribir" name="colega_cercano" 
                                        disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                        && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.colega_cercano} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                    </div>
                                    {
                                            this.state.prospecto.colega_cercano == null || this.state.prospecto.colega_cercano == "" ?
                                                <span style={{ color: "red" }}>La pregunta ¿Cómo te describiría tu colega más cercano?, debe ser ingresada</span> : ''
                                        }
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-xs-12 col-lg-12">
                                <div className="form-group">
                                    <span >
                                        <span className="font-weight-semibold">¿Si hablamos con tu mejor amigo (a) ¿Cuál dirá que es tu mayor debilidad? (Personalidad)</span>
                                    </span>
                                    <div className="input-group">
                                        <input type="text" placeholder="Escribir" name="mayor_debilidad" 
                                        disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                        && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.mayor_debilidad} className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                    </div>
                                    {
                                            this.state.prospecto.mayor_debilidad == null || this.state.prospecto.mayor_debilidad == "" ?
                                                <span style={{ color: "red" }}>La pregunta ¿Si hablamos con tu mejor amigo (a) ¿Cuál dirá que es tu mayor debilidad?, debe ser ingresada</span> : ''
                                        }
                                </div>
                            </div>
                        </div>
                    </div>
                    <HistoricoGeneral idProspecto={this.props.idProspecto} tipoUsuario={this.state.tipoUsuario} presentarSeccion = {"EP2"} />
                </div>
            </div>
        )
    }
}