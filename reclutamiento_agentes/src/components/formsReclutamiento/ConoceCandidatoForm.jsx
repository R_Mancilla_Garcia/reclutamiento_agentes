import React, { Component } from "react";
import HistoricoGeneral from "./HistoricoGeneral";
import datos from '../urls/datos.json'

export default class ConoceCandidatoForm extends Component {

    constructor() {
        super();
        this.onChange = this.onChange.bind(this)
        this.guardaConociendoProspecto = this.guardaConociendoProspecto.bind(this)
        this.peticionAVentaDeCarrera = this.peticionAVentaDeCarrera.bind(this)
        this.state = {
            referido: {},
            prospecto: {
                "id": null,
                "prospecto_id": null,
                "invitar_comida": null,
                "invitar_comida_prque": null,
                "aportar_equipo": null,
                "rasgos_importantes": null,
                "tiempo_carrera": null,
                "necesidades_financieras": null,
                "redes_sociales": null,
                "colchon_economico": null,
                "resp_patrimonio": null,
                "resp_patrimonio_porque": null,
                "debilidad": null
            }
        }
    }

    UNSAFE_componentWillMount() {
        fetch(datos.urlServicePy+"parametros/api_recluta_prospectos/" + this.props.idProspecto)
            .then(response => response.json())
            .then(json => {
                let fila = json.filas[0].fila
                let columnas = json.columnas
                let columnasSalida = {}
                for (var i = 0; i < fila.length; i++) {
                    console.log(fila[i])
                    columnasSalida["" + columnas[i].key + ""] = fila[i].value
                }
                this.setState({ referido: columnasSalida })

                fetch(datos.urlServicePy+"recluta/api_conociendo_candidato/" + this.props.idProspecto)
                    .then(response => response.json())
                    .then(prospecto => {
                        if (prospecto.length > 0) {
                            this.setState({ prospecto: prospecto[0] })
                        }

                    })


            })
    }


    onChange = e => {
        let prospecto = this.state.prospecto
        if (e.target.name == 'invitar_comida') {
            prospecto.invitar_comida = parseInt(e.target.value)
        } else if (e.target.name == 'resp_patrimonio') {
            prospecto.resp_patrimonio = parseInt(e.target.value)
        } else {
            prospecto["" + e.target.name + ""] = e.target.value
        }

        this.setState({ prospecto: prospecto })


    }


    guardaConociendoProspecto() {
        let jsonGuardar = this.state.prospecto
        jsonGuardar.id = this.props.idProspecto
        jsonGuardar.prospecto_id = this.props.idProspecto
        const requestOptions = {
            method: "POST",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(jsonGuardar)
        };
        console.log("enviando a reserva")

        fetch(datos.urlServicePy+"recluta/api_conociendo_candidato/0", requestOptions)
            .then(response => response.json())
            .then(data => {
                this.peticionAVentaDeCarrera()
                //this.props.botonCancelar()
            })

    }

    peticionAVentaDeCarrera() {
        let json = {
            "estatus_id": 16,
        }
        const requestOptions = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };
        fetch(datos.urlServicePy+"parametros_upd/api_recluta_prospectos/" + this.props.idProspecto, requestOptions)
            .then(response => response.json())
            .then(data => {

                let jsonNotificacion = {
                    "mail_id": 5,
                    "prospecto_id": this.props.idProspecto
                }
                const requestOptions = {
                    method: "POST",
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(jsonNotificacion)
                };

                fetch(datos.urlServicePy+"recluta/api_notificador/0", requestOptions)
                    .then(response => response.json())
                    .then(data => {
                        this.props.botonCancelar()
                })

            })

    }


    render() {
        return (
            <div>
                <div class="card">
                    <div class="row mb-2 mt-2 ml-1">
                        <div class="col-12 col-lg-9">
                        </div>
                        <div class="col-12 col-lg-3">
                            <div class="float-right mr-3">
                                <button type="button" title="Guardar y Regresar" onClick={(e) => this.props.botonCancelar()} class="btn rounded-pill mr-1" style={{ backgroundColor: '#8F9EB3', borderColor: '#8F9EB3' }}><span><i style={{ color: 'white' }} className="icon-backward2"></i></span></button>
                                <button type="button" title="Registrar conoce a tu candidato" onClick={this.guardaConociendoProspecto} class="btn rounded-pill mr-1"
                                    style={{ backgroundColor: '#8F9EB3', borderColor: '#8F9EB3' }}
                                    disabled={
                                        this.state.prospecto.invitar_comida == null || this.state.prospecto.invitar_comida_prque == "" || this.state.prospecto.invitar_comida_prque == null ||
                                        this.state.prospecto.aportar_equipo == "" || this.state.prospecto.aportar_equipo == null || this.state.prospecto.rasgos_importantes == "" || this.state.prospecto.rasgos_importantes == null ||
                                        this.state.prospecto.tiempo_carrera == "" || this.state.prospecto.tiempo_carrera == null || this.state.prospecto.necesidades_financieras == "" || this.state.prospecto.necesidades_financieras == null ||
                                        this.state.prospecto.redes_sociales == "" || this.state.prospecto.redes_sociales == null || this.state.prospecto.colchon_economico == "" || this.state.prospecto.colchon_economico == null ||
                                        this.state.prospecto.resp_patrimonio == null || this.state.prospecto.resp_patrimonio_porque == "" || this.state.prospecto.resp_patrimonio_porque == null ||
                                        this.state.prospecto.debilidad == "" || this.state.prospecto.debilidad == null
                                    }

                                ><span><i style={{ color: 'white' }} className="icon-forward3"></i></span></button>
                            </div>

                        </div>
                    </div>
                </div>


                <div class="card-header bg-transparent header-elements-sm-inline">
                    <h3 class="mb-0 font-weight-semibold" style={{ color: '#617187' }}>CONOCIENDO A TU CANDIDATO</h3>
                    <div class="header-elements">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Prospecto: <strong >{this.state.referido.nombre + ' ' + this.state.referido.ape_paterno + ' ' + (this.state.referido.ape_materno != null ? this.state.referido.ape_materno : '')}</strong></li>
                            <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Fecha de registro:<strong >{this.state.referido.ts_alta_audit}</strong></li>
                            {/*<li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Entrevistador: <strong >{this.state.entrevistador}</strong></li>*/}
                            <li class="list-inline-item">
                                <h6 style={{ backgroundColor: "#5B96E2", color: '#FFFF', borderRadius: '8px' }}> <>&nbsp;&nbsp;</>Conoce a tu candidato<>&nbsp;&nbsp;</> </h6>
                            </li>
                        </ul>
                    </div>
                </div>

                <br></br>

                <div class="card">
                    <div className="card-body">
                        <div className="row">
                            <div className="col-xs-12 col-lg-12">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>¿Es alguien a quien invitarías a una comida el fin de semana a tu casa?</span>
                                        </span>
                                        <div className="row">
                                            {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" value={1} id="invitar_comida1" disabled={this.props.tipoUsuario != "GTEDES" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} name="invitar_comida" checked={this.state.prospecto.invitar_comida == 1} />
                                                <label class="form-check-label" for="invitar_comida1">
                                                    Si
                                                </label>
                                            </div>
                                            {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" value={0} id="invitar_comida0" disabled={this.props.tipoUsuario != "GTEDES" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} name="invitar_comida" checked={this.state.prospecto.invitar_comida == 0} />
                                                <label class="form-check-label" for="invitar_comida0">
                                                    No
                                                </label>
                                            </div>
                                            {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                            {
                                                this.state.prospecto.invitar_comida == null ?
                                                    <span className="" style={{ color: "red" }}>La pregunta es requerida</span> : ''
                                            }

                                        </div>
                                    </div>

                                </div>

                            </div>
                            <div className="col-xs-12 col-lg-12">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>¿Por qué?</span>
                                        </span> <input type="text" placeholder="" onChange={this.onChange} value={this.state.prospecto.invitar_comida_prque} disabled={this.props.tipoUsuario != "GTEDES" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} name="invitar_comida_prque" className="form-control " />
                                    </div>
                                    {
                                        this.state.prospecto.invitar_comida_prque == "" || this.state.prospecto.invitar_comida_prque == null ?
                                            <span className="" style={{ color: "red" }}>La pregunta es requerida</span> : ''
                                    }

                                </div>
                            </div>

                            <div className="col-xs-12 col-lg-12">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>¿Qué consideras que aportará a tu equipo y qué el equipo a él/ella?</span>
                                        </span> <input type="text" placeholder="" onChange={this.onChange} value={this.state.prospecto.aportar_equipo} disabled={this.props.tipoUsuario != "GTEDES" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} name="aportar_equipo" className="form-control " />
                                    </div>
                                    {
                                        this.state.prospecto.aportar_equipo == "" || this.state.prospecto.aportar_equipo == null ?
                                            <span className="" style={{ color: "red" }}>La pregunta es requerida</span> : ''
                                    }

                                </div>
                            </div>


                            <div className="col-xs-12 col-lg-12">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Describe los rasgos más importantes de su mercado</span>
                                        </span> <input type="text" placeholder="" onChange={this.onChange} value={this.state.prospecto.rasgos_importantes} disabled={this.props.tipoUsuario != "GTEDES" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} name="rasgos_importantes" className="form-control " />
                                    </div>
                                    {
                                        this.state.prospecto.rasgos_importantes == "" || this.state.prospecto.rasgos_importantes == null ?
                                            <span className="" style={{ color: "red" }}>La pregunta es requerida</span> : ''
                                    }

                                </div>
                            </div>


                            <div className="col-xs-12 col-lg-12">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>¿Define el tiempo que dedicará a esta carrera - con precisión - número de horas al día, semana - año?</span>
                                        </span> <input type="text" placeholder="" onChange={this.onChange} value={this.state.prospecto.tiempo_carrera} disabled={this.props.tipoUsuario != "GTEDES" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} name="tiempo_carrera" className="form-control " />
                                    </div>
                                    {
                                        this.state.prospecto.tiempo_carrera == "" || this.state.prospecto.tiempo_carrera == null ?
                                            <span className="" style={{ color: "red" }}>La pregunta es requerida</span> : ''
                                    }

                                </div>
                            </div>

                            <div className="col-xs-12 col-lg-12">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Necesidades financieras personales y familiares (Indicar Montos)</span>
                                        </span> <input type="text" placeholder="" onChange={this.onChange} value={this.state.prospecto.necesidades_financieras} disabled={this.props.tipoUsuario != "GTEDES" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} name="necesidades_financieras" className="form-control " />
                                    </div>
                                    {
                                        this.state.prospecto.necesidades_financieras == "" || this.state.prospecto.necesidades_financieras == null ?
                                            <span className="" style={{ color: "red" }}>La pregunta es requerida</span> : ''
                                    }

                                </div>
                            </div>


                            <div className="col-xs-12 col-lg-12">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>¿Qué te dicen sus redes sociales? (Especificar cuáles tiene)</span>
                                        </span> <input type="text" placeholder="" onChange={this.onChange} value={this.state.prospecto.redes_sociales} disabled={this.props.tipoUsuario != "GTEDES" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} name="redes_sociales" className="form-control " />
                                    </div>
                                    {
                                        this.state.prospecto.redes_sociales == "" || this.state.prospecto.redes_sociales == null ?
                                            <span className="" style={{ color: "red" }}>La pregunta es requerida</span> : ''
                                    }

                                </div>
                            </div>


                            <div className="col-xs-12 col-lg-12">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>¿Cuenta con algún colchón económico? (Preferencia indicar cuánto aprox)</span>
                                        </span> <input type="text" placeholder="" onChange={this.onChange} value={this.state.prospecto.colchon_economico} disabled={this.props.tipoUsuario != "GTEDES" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} name="colchon_economico" className="form-control " />
                                    </div>
                                    {
                                        this.state.prospecto.colchon_economico == "" || this.state.prospecto.colchon_economico == null ?
                                            <span className="" style={{ color: "red" }}>La pregunta es requerida</span> : ''
                                    }

                                </div>
                            </div>





                            <div className="col-xs-12 col-lg-12">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>¿Es alguien a quien entregarías la responsabilidad de tu patrimonio/dinero /retiro?</span>
                                        </span>
                                        <div className="row">
                                            {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" value={1} onChange={this.onChange} disabled={this.props.tipoUsuario != "GTEDES" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} id="resp_patrimonio1" name="resp_patrimonio" checked={this.state.prospecto.resp_patrimonio == 1} />
                                                <label class="form-check-label" for="resp_patrimonio1">
                                                    Si
                                                </label>
                                            </div>
                                            {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" value={0} onChange={this.onChange} disabled={this.props.tipoUsuario != "GTEDES" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} id="resp_patrimonio0" name="resp_patrimonio" checked={this.state.prospecto.resp_patrimonio == 0} />
                                                <label class="form-check-label" for="resp_patrimonio0">
                                                    No
                                                </label>
                                            </div>
                                            {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                            {
                                                this.state.prospecto.resp_patrimonio == null ?
                                                    <span className="" style={{ color: "red" }}>La pregunta es requerida</span> : ''
                                            }
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <div className="col-xs-12 col-lg-12">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>¿Por qué?</span>
                                        </span> <input type="text" placeholder="" onChange={this.onChange} value={this.state.prospecto.resp_patrimonio_porque} disabled={this.props.tipoUsuario != "GTEDES" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} name="resp_patrimonio_porque" className="form-control " />
                                    </div>
                                    {
                                        this.state.prospecto.resp_patrimonio_porque == "" || this.state.prospecto.resp_patrimonio_porque == null ?
                                            <span className="" style={{ color: "red" }}>La pregunta es requerida</span> : ''
                                    }

                                </div>
                            </div>



                            <div className="col-xs-12 col-lg-12">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>¿Cuál consideras que es su debilidad y que deberías trabajar más de cerca con el candidato?</span>
                                        </span> <input type="text" placeholder="" onChange={this.onChange} value={this.state.prospecto.debilidad} disabled={this.props.tipoUsuario != "GTEDES" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} name="debilidad" className="form-control " />
                                    </div>
                                    {
                                        this.state.prospecto.debilidad == "" || this.state.prospecto.debilidad == null ?
                                            <span className="" style={{ color: "red" }}>La pregunta es requerida</span> : ''
                                    }

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <HistoricoGeneral idProspecto={this.props.idProspecto} tipoUsuario={this.state.tipoUsuario} presentarSeccion = {"EP11"} />
            </div>
        )
    }

}