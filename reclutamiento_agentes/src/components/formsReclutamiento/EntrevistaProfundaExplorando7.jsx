import React, { Component } from "react";
import HistoricoGeneral from "./HistoricoGeneral";
import axios from 'axios';
import datos from '../urls/datos.json'

export default class EntrevistaProfundaExplorando7 extends Component {

    constructor() {
        super();
        this.onChange = this.onChange.bind(this)
        this.onClickBotonArchivoPuesto = this.onClickBotonArchivoPuesto.bind(this)
        this.onClickBotonArchivoPSP = this.onClickBotonArchivoPSP.bind(this)
        this.cargaInfoPSP = this.cargaInfoPSP.bind(this)
        this.cargaPdf14 = this.cargaPdf14.bind(this)

        this.state = {
            entrevistador:'',
            prospecto:
            {
                "id": null,
                "prospecto_id": null,
                "estatus_id": 11,
                "puntualidad": null,
                "minutos_tarde": null,
                "habilidad_expresarse": null,
                "habilidad_sintetizar": null,
                "apariencia_personal": null,
                "demostracion_confianza": null,
                "muestra_seguro": null,
                "encuentra_seriamente": null,
                "observaciones": null
            },
            muestraInfoPSP: false,
            archivoPSP: undefined,
            muestraDocPSP: false,
            colorSubirArchivoPSP: "#E1E5F0",
            colorTextoSubirArchivoPSP: "#617187",
            colorBotonSubirArchivoPSP: "#313A46",
            nombreArchivoPSP: "",
            archivoPSP: undefined,
            textoPuesto: 'Subir archivo',

        };
    }

    UNSAFE_componentWillMount() {
        console.log("entrando es render de explorando ", this)
        fetch(datos.urlServicePy+"recluta/api_recluta_evaluacion07/" + this.props.idProspecto)
            .then(response => response.json())
            .then(jsonData => {
                if (jsonData.length > 0) {
                    this.setState({ prospecto: jsonData[0] })
                }
                fetch(datos.urlServicePy+"parametros/api_recluta_prospectos/" + this.props.idProspecto)
                .then(response => response.json())
                .then(recluta_prospecto => {
                    console.log("recluta_propspecto", recluta_prospecto)
                    fetch(datos.urlServicePy+"parametros/api_cat_empleados/" + recluta_prospecto.filas[0].fila[12].value)
                    .then(response => response.json())
                    .then(entrevistador => {
                            console.log("entrevistador ", entrevistador)
                            this.setState({entrevistador:entrevistador.filas[0].fila[2].value + " "+ entrevistador.filas[0].fila[3].value + " " + (entrevistador.filas[0].fila[4].value != null ? entrevistador.filas[0].fila[4].value:'') })
                    })
                })
            })
        fetch(datos.urlServicePy+"recluta/api_recluta_vcargapsppdf14/" + this.props.idProspecto)
            .then(response => response.json())
            .then(jsonRespuestaCarga => {
                console.log("respuesta de la carga ", jsonRespuestaCarga)
                if (jsonRespuestaCarga.length > 0) {
                    let interpretacionConcatenada=jsonRespuestaCarga[6] === undefined ?  "":jsonRespuestaCarga[6].respuesta

                    this.setState({
                        muestraInfoPSP: true,
                        rutaPSP: jsonRespuestaCarga[0].ironpsp,
                        fechaRespPSP: jsonRespuestaCarga[0].respuesta,
                        eficaciaVentas: jsonRespuestaCarga[1].porcentaje,
                        eficaciaEmpresarial: jsonRespuestaCarga[2].porcentaje,
                        rendimientoVentas: jsonRespuestaCarga[3].porcentaje,
                        ventaDominante:jsonRespuestaCarga[4].respuesta,
                    interpretacion:jsonRespuestaCarga[5].respuesta + " "+interpretacionConcatenada ,
                        muestraDocPSP: true,
                        colorSubirArchivoPSP: "#78CB5A", colorTextoSubirArchivoPSP: "#FFFFFF", colorBotonSubirArchivoPSP: "#78CB5A"
                    })
                }


            })

    }
    onChange = e => {
        console.log(e.target.value)
        let pros = this.state.prospecto
        if (e.target.name == "observaciones") {
            pros["" + e.target.name + ""] = e.target.value

        } else {
            pros["" + e.target.name + ""] = parseInt(e.target.value)
        }
        this.setState({ prospecto: pros })

    }

    onChangeFilePuesto(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file de puesto", file);
        this.setState({ archivoPuesto: file, colorSubirArchivo: "#78CB5A", colorTextoSubirArchivo: "#FFFFFF", colorBotonSubirArchivo: "#E5F6E0", nombreArchivoPuesto: file.name })
        // this.props.guardaArchivo(file, "archivoPuesto")

    }


    onClickBotonArchivoPuesto() {
        console.log("entrnaod a la imagen")
        this.upload.click()
    }



    onClickBotonArchivoPSP() {
        console.log("entrnaod a la imagen")
        this.upload.click()
    }


    cargaInfoPSP(json) {
        const requestOptions = {
            method: "POST",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };

        fetch(datos.urlServicePy+"recluta/api_recluta_cargapsppdf/0", requestOptions)
        .then(response => response.json())
        .then(data => {
            console.log("la respuesta de la carga", data)
            this.cargaPdf14()
        });
    }

    cargaPdf14(){
        fetch(datos.urlServicePy+"recluta/api_recluta_vcargapsppdf14/" + this.props.idProspecto)
        .then(response => response.json())
        .then(jsonRespuestaCarga => {
            console.log("respuesta de la carga ", jsonRespuestaCarga)
            let interpretacionConcatenada=jsonRespuestaCarga[6] === undefined ?  "":jsonRespuestaCarga[6].respuesta

            this.setState({
                muestraInfoPSP: true,
                fechaRespPSP: jsonRespuestaCarga[0].respuesta,
                eficaciaVentas: jsonRespuestaCarga[1].porcentaje,
                eficaciaEmpresarial: jsonRespuestaCarga[2].porcentaje,
                rendimientoVentas: jsonRespuestaCarga[3].porcentaje,
                ventaDominante:jsonRespuestaCarga[4].respuesta,
                interpretacion:jsonRespuestaCarga[5].respuesta + " "+interpretacionConcatenada,
            })
        })
    }

    onChangeFilePSP(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file de puesto", file);

        //lanzamos el envio del psp 
        let formData = new FormData();
        const config = {
            headers: { 'content-type': 'multipart/form-data' }
        }
        
        if(this.state.muestraDocPSP == true){
            formData.append('ironpsp', file);
            axios.put(datos.urlServicePy+"api/doc_ironpsp/"+this.props.idProspecto+"/", formData, config)
            .then(res => {
                console.log("respuesta  del archivo cargado", res.data);
                this.cargaInfoPSP(res.data)
                let ruta = res.data.ironpsp
                let rutaCorrecta = ruta.replace("8000/media", "8001/documentos")
                rutaCorrecta = rutaCorrecta.replace("http", "https")
                console.log(rutaCorrecta)
                this.setState({ rutaPSP: rutaCorrecta, archivoPSP: file, colorSubirArchivoPSP: "#78CB5A", colorTextoSubirArchivoPSP: "#FFFFFF", colorBotonSubirArchivoPSP: "#78CB5A", nombreArchivoPSP: file.name, muestraDocPSP: true })
            })
        } else {
            formData.append('id', this.props.idProspecto);
            formData.append('ironpsp', file);
            formData.append('prospecto_id', this.props.idProspecto);
            axios.post(datos.urlServicePy+"api/doc_ironpsp/", formData, config)
            .then(res => {
                console.log("respuesta  del archivo cargado", res.data);
                this.cargaInfoPSP(res.data)
                let ruta = res.data.ironpsp
                let rutaCorrecta = ruta.replace("8000/media", "8001/documentos")
                rutaCorrecta = rutaCorrecta.replace("http", "https")
                console.log(rutaCorrecta)
                this.setState({ rutaPSP: rutaCorrecta, archivoPSP: file, colorSubirArchivoPSP: "#78CB5A", colorTextoSubirArchivoPSP: "#FFFFFF", colorBotonSubirArchivoPSP: "#78CB5A", nombreArchivoPSP: file.name, muestraDocPSP: true })
            })
        }
        
    }

    render() {
        return (
            <div>
                <button type="button" style={{ display: 'none' }} ref={(ref) => this.modalInicio = ref} data-toggle="modal" data-target="#modal-Continuar"   ></button>

                <div id="modal-psp" className="modal fade" tabindex="-1">
                    <div className="modal-dialog modal-full">
                        <div className="modal-content text-white" style={{ backgroundColor: "#313A46" }}>
                            <div className="modal-header text-white" style={{ backgroundColor: "#8189D4" }}>
                                <h6 className="modal-title">Documento PSP </h6>
                                <button type="button" className="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div className="modal-body">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-xs-12 col-lg-4"><>&nbsp;&nbsp;</></div>
                                        <div className="col-xs-12 col-lg-4">
                                            <div className="input-group">
                                                <form encType="multipart/form" style={{ display: 'none' }} >
                                                    <input type="file" style={{ display: 'none' }} ref={(ref) => this.upload = ref} onChange={this.onChangeFilePSP.bind(this)}></input>
                                                </form>
                                                <input type="text" style={{ borderColor: 'black', backgroundColor: "#313A46", color: 'white' }} className="form-control " placeholder="Archivo PSP" value={this.state.nombreArchivoPSP} />
                                                <span className="input-group-prepend" style={{ backgroundColor: this.state.colorBotonSubirArchivoPSP }}>
                                                    {/*<button type="button" class="btn text-white" onClick={this.onClickBotonArchivoPSP} style={{ backgroundColor: this.state.colorBotonSubirArchivoPSP }}>
                                                        <h10 style={{ color: "white" }}>+</h10>
                                                         </button>*/}

                                                    {
                                                                this.state.muestraDocPSP == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoPSP }}>
                                                                <a href={this.state.rutaPSP} target="_blank" rel="noopener noreferrer" >
                                                                    <i className="fas fa-eye"
                                                                        style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h10 onClick={this.onClickBotonArchivoPSP} style={{ color: "white" }}>+</h10>
                                                            </button> : <button type="button" class="btn text-white" onClick={this.onClickBotonArchivoPSP} style={{ backgroundColor: this.state.colorBotonSubirArchivoPSP }}>
                                                                <h10 style={{ color: "white" }}>+</h10>
                                                            </button>

                                                    }



                                                    <span className="input-group-text" style={{ background: this.state.colorBotonSubirArchivoPSP, borderColor: this.state.colorBotonSubirArchivoPSP, color: "white" }} >{this.state.textoPuesto}</span>
                                                </span>
                                            </div>
                                        </div>
                                        <div className="col-xs-12 col-lg-4"><>&nbsp;&nbsp;</></div>
                                    </div>

                                    <br></br>
                                    <br></br>
                                    {this.state.muestraInfoPSP == true &&
                                        <div>
                                            <div className="row">
                                                <div className="col-xs-6 col-lg-6">
                                                    <h8 class="mb-0 font-weight-semibold text-white"> Resultados PSP</h8>
                                                </div>
                                                <div className="col-xs-4 col-lg-4">
                                                    <table className="table datatable-sorting  table-striped table-hover" style={{ backgroundColor: "#313A46", borderColor: '#313A46' }}>
                                                        <thead style={{ backgroundColor: "#313A46" }}>
                                                            <tr>
                                                                <th className="text-left font-weight-bold text-white" style={{ backgroundColor: '#313A46' }} >
                                                                    <a>ALTO</a>
                                                                </th>
                                                                <th className="text-center font-weight-bold text-white" style={{ backgroundColor: '#313A46' }} >
                                                                    <a>MODERADO</a>
                                                                </th>
                                                                <th className="text-right font-weight-bold text-white" style={{ backgroundColor: '#313A46' }} >
                                                                    <a>BAJO</a>
                                                                </th>
                                                            </tr>
                                                        </thead>

                                                    </table>
                                                </div>
                                            </div>

                                            <div className="row">
                                                <div className="col-xs-4 col-lg-4">
                                                    <div className="form-group">
                                                        <div className="input-group">
                                                            <span className="input-group-prepend"> <span
                                                                className="input-group-text text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }}>Fecha de respuesta del PSP</span>
                                                            </span> <input type="text" placeholder="" value={this.state.fechaRespPSP} className="form-control text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }} />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-xs-2 col-lg-2">
                                                    <h8 class=" text-white"> Eficacia en ventas</h8>
                                                </div>

                                                <div className="col-xs-4 col-lg-4">

                                                    <input type="range" class="custom-range" min="0" name="salud" max="100" step={50} value={this.state.eficaciaVentas} id="salud" />
                                                </div>


                                                <div className="col-xs-2 col-lg-2">




                                                    {
                                                        this.state.eficaciaVentas == 0 &&
                                                        <div class="mr-3 ">
                                                            <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                                    class="letter-icon text-white"></span>
                                                            </a>
                                                        </div>

                                                    }

                                                    {
                                                        this.state.eficaciaVentas == 50 &&
                                                        <div className="row">
                                                            
                                                            {/*<div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>*/}

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                        </div>
                                                    }


                                                    {
                                                        this.state.eficaciaVentas == 100 &&
                                                        <div className="row">

                                                           {/* <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>*/}

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FD0303', borderColor: '#FD0303' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                        </div>
                                                    }





                                                </div>


                                            </div>

                                            <div className="row">

                                                <div className="col-xs-12 col-lg-4">
                                                    <div className="form-group">
                                                        <div className="input-group">
                                                            <span className="input-group-prepend"> <span
                                                                className="input-group-text text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }}>Estilo de venta dominante&ensp;</span>
                                                            </span> <input type="text" placeholder="" value={this.state.ventaDominante} className="form-control text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }} />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-xs-12 col-lg-2">
                                                    <h8 class=" text-white"> Eficacia empresarial</h8>
                                                </div>

                                                <div className="col-xs-12 col-lg-4">
                                                    <input type="range" class="custom-range" min="0" name="salud" max="100" step={50} value={this.state.eficaciaEmpresarial} id="salud" />
                                                </div>


                                                <div className="col-xs-2 col-lg-2">

                                                    {
                                                        this.state.eficaciaEmpresarial == 0 &&
                                                        <div class="mr-3 ">
                                                            <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                                    class="letter-icon text-white"></span>
                                                            </a>
                                                        </div>

                                                    }

                                                    {
                                                        this.state.eficaciaEmpresarial == 50 &&
                                                        <div className="row">
                                                            
                                                            {/*<div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>*/}

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                        </div>
                                                    }


                                                    {
                                                        this.state.eficaciaEmpresarial == 100 &&
                                                        <div className="row">

                                                           {/* <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                    </div>*/}

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FD0303', borderColor: '#FD0303' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                        </div>
                                                    }



                                                </div>



                                            </div>

                                            <div className="row">
                                                <div className="col-xs-12 col-lg-4">
                                                    <div className="form-group">
                                                        <div className="input-group">
                                                            <span className="input-group-prepend"> <span
                                                                className="input-group-text text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }}>Interpretación de precisión</span>
                                                            </span> <textarea disabled={true} type="text" placeholder="" value={this.state.interpretacion} className="form-control text-white " style={{ borderColor: '#232931', backgroundColor: '#313A46', color: '#313A46' }} />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-xs-12 col-lg-2">
                                                    <h8 class=" text-white">Rendimiento de ventas general esperado</h8>
                                                </div>

                                                <div className="col-xs-12 col-lg-4">
                                                    <input type="range" class="custom-range" min="0" name="salud" max="100" step={50} value={this.state.rendimientoVentas} id="salud" />
                                                </div>

                                                <div className="col-xs-2 col-lg-2">

                                                    {
                                                        this.state.rendimientoVentas == 0 &&
                                                        <div class="mr-3 ">
                                                            <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                class="btn rounded-pill btn-icon btn-sm "><span
                                                                    class="letter-icon text-white"></span>
                                                            </a>
                                                        </div>

                                                    }

                                                    {
                                                        this.state.rendimientoVentas == 50 &&
                                                        <div className="row">
                                                            
                                                            {/*<div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>*/}

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                        </div>
                                                    }


                                                    {
                                                        this.state.rendimientoVentas == 100 &&
                                                        <div className="row">

                                                           {/* <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#04C601', borderColor: '#04C601' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FDDB03', borderColor: '#FDDB03' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                    </div>*/}

                                                            <div class="mr-3 ">
                                                                <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#FD0303', borderColor: '#FD0303' }}
                                                                    class="btn rounded-pill btn-icon btn-sm "><span
                                                                        class="letter-icon text-white"></span>
                                                                </a>
                                                            </div>

                                                        </div>
                                                    }



                                                </div>


                                            </div>
                                        </div>

                                    }







                                </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-center">
                                <button type="button" class="btn text-white" data-dismiss="modal" style={{ backgroundColor: '#617187', borderColor: '#617187' }}>Cancelar</button>

                                {
                                    this.state.muestraDocPSP == true ? <button type="button" class="btn text-white " disabled={false} style={{ backgroundColor: '#617187', borderColor: '#617187' }}>
                                        <a href={this.state.rutaPSP} style={{ color: 'white' }} target="_blank" rel="noopener noreferrer" >
                                            Ver PSP
                                        </a>
                                    </button> : <button type="button" class="btn text-white " disabled={true} style={{ backgroundColor: '#617187', borderColor: '#617187' }}>
                                        <a href="#" style={{ color: 'white' }} target="_blank" rel="noopener noreferrer" >
                                            Ver PSP
                                        </a>

                                    </button>
                                }

                                <button type="button" class="btn text-white" data-dismiss="modal" style={{ backgroundColor: '#617187', borderColor: '#617187' }}>Aceptar</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="modal-Continuar" className="modal fade" tabindex="-1">
                    <div className="modal-dialog ">
                        <div className="modal-content text-white" style={{ backgroundColor: "#FFFFF" }}>
                            <div className="modal-header text-white text-center" style={{ backgroundColor: "#FFFFF" }}>
                            </div>
                            <div className="modal-body">
                                <div className="card-body">

                                    <h6 className="col-12 text-center text-dark font-weight-semibold">Desea continuar con la prueba?</h6>

                                </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-center">
                                <div className="col-12">
                                    <div className="row">
                                        <button type="button" style={{ width: '100%', backgroundColor: '#8189D4' }}
                                            class="btn text-white" onClick={(e) => console.log("")} data-dismiss="modal"  >Sí, continuar </button>
                                    </div>
                                </div>

                                <br></br>
                                <br></br>
                                <br></br>
                                <div className="col-12">
                                    <div className="row">
                                        <div className="col-6">
                                            <button type="button" style={{ width: '100%', backgroundColor: "#FFFF", borderColor: 'red', textEmphasisColor: 'red' }} class="btn text-red" onClick={(e) => console.log("")} data-dismiss="modal"  ><span style={{ color: 'red' }}>Enviar a reserva</span></button>
                                        </div>

                                        <div className="col-6">
                                            <button type="button" style={{ width: '100%', backgroundColor: "#FFFF", borderColor: '#617187' }} class="btn text-white" onClick={(e) => console.log("")} data-dismiss="modal"><span style={{ color: '#617187' }}>Dar de baja</span></button>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>
                </div>

                <div class="card">

                    <div class="row mb-2 mt-2 ml-1">
                        <div class="col-12 col-lg-9">
                            <div class="row">

                                <div class="col-6 col-lg-1 d-flex align-items-center pb-sm-1">
                                    <div class="mr-2">
                                        <a href="#"
                                            class="btn rounded-pill btn-icon btn-sm" style={{ backgroundColor: '#78CB5A' }}> <span
                                                class="letter-icon text-white"><i className="icon-checkmark2"></i></span>
                                        </a>
                                    </div>
                                </div>

                                <div class="col-6 col-lg-1 d-flex align-items-center pb-sm-1">
                                    <div class="mr-2">
                                        <a href="#"
                                            class="btn  rounded-pill btn-icon btn-sm " style={{ backgroundColor: '#8189D4', borderColor: '#8189D4' }}> <span style={{ color: 'white' }}
                                                class="letter-icon">EP</span>
                                        </a>
                                    </div>

                                </div>



                                <div class="col-6 col-lg-7 d-flex align-items-center pb-sm-1">

                                    <div class="mr-5 ">
                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8189D4', borderColor: '#8189D4' }}
                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                class="letter-icon text-white">1</span>
                                        </a>
                                    </div>

                                    <div class="mr-5 ">
                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8189D4', borderColor: '#8189D4' }}
                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                class="letter-icon text-white">2</span>
                                        </a>
                                    </div>

                                    <div class="mr-5 ">
                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8189D4', borderColor: '#8189D4' }}
                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                class="letter-icon text-white">3</span>
                                        </a>
                                    </div>

                                    <div class="mr-5 ">
                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8189D4', borderColor: '#8189D4' }}
                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                class="letter-icon text-white">4</span>
                                        </a>
                                    </div>

                                    <div class="mr-5 ">
                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8189D4', borderColor: '#8189D4' }}
                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                class="letter-icon text-white">5</span>
                                        </a>
                                    </div>

                                    <div class="mr-5 ">
                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8189D4', borderColor: '#8189D4' }}
                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                class="letter-icon text-white">6</span>
                                        </a>
                                    </div>

                                    <div class="mr-5 ">
                                        <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#8189D4', borderColor: '#8189D4' }}
                                            class="btn rounded-pill btn-icon btn-sm "><span
                                                class="letter-icon text-white">7</span>
                                        </a>
                                    </div>


                                    <div class="mr-5">
                                        <a href="#" style={{ height: '12px', width: '12px', borderColor: '#C4D3E8' }}
                                            class="btn rounded-pill btn-icon btn-sm "> <span
                                                class="letter-icon"></span>
                                        </a>
                                    </div>
                                    <div class="mr-5">
                                        <a href="#" style={{ height: '12px', width: '12px', borderColor: '#C4D3E8' }}
                                            class="btn rounded-pill btn-icon btn-sm "> <span
                                                class="letter-icon"></span>
                                        </a>
                                    </div>

                                    <div class="mr-5">
                                        <a href="#" style={{ height: '12px', width: '12px', borderColor: '#C4D3E8' }}
                                            class="btn rounded-pill btn-icon btn-sm "> <span
                                                class="letter-icon"></span>
                                        </a>
                                    </div>





                                </div>

                                &ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;    
                                        <div class="d-flex justify-content-between">
                                            <div class="mr-2">
                                                <a href="#"
                                                    class="btn  rounded-pill btn-icon btn-sm" style={{ borderColor: '#5B96E2' }}> <span style={{ color: '#5B96E2' }}
                                                        class="letter-icon">VC</span>
                                                </a>
                                            </div>

                                        </div>
                                        <div class="d-flex justify-content-between">
                                            <div class="mr-2">
                                                <a href="#"
                                                    class="btn  rounded-pill btn-icon btn-sm" style={{ borderColor: '#41B3D1' }}> <span style={{ color: '#41B3D1' }}
                                                        class="letter-icon">CC</span>
                                                </a>
                                            </div>

                                        </div>
                                        <div class="d-flex justify-content-between">
                                            <div class="mr-2">
                                                <a href="#"
                                                    class="btn  rounded-pill btn-icon btn-sm" style={{ borderColor: '#78CB5A' }}> <span style={{ color: '#78CB5A' }}
                                                        class="letter-icon">C</span>
                                                </a>
                                            </div>
                                        </div>
                            </div>
                        </div>


                        <div class="col-12 col-lg-3">
                            <div class="float-right mr-3">
                                <button type="button" title="Guardar y Regresar" onClick={(e) => this.props.regresaForm6explorando()} class="btn rounded-pill mr-1" style={{ backgroundColor: '#8F9EB3', borderColor: '#8F9EB3' }}><span><i style={{ color: 'white' }} className="icon-backward2"></i></span></button>
                                <button type="button" title="Registrar entrevista" onClick={(e) => this.props.muestraForm8Explorando(this.state.prospecto)} disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"}  class="btn rounded-pill mr-1" style={{ backgroundColor: '#8F9EB3', borderColor: '#8F9EB3' }}><span><i style={{ color: 'white' }} className="icon-forward3"></i></span></button>
                            </div>

                        </div>
                    </div>

                </div>
                <div class="card-header bg-transparent header-elements-sm-inline">
                    <h3 class="mb-0 font-weight-semibold" style={{ color: '#617187' }}>Entrevista Profunda - PSP & IRON TALENT </h3>
                    <div class="header-elements">
                        <ul class="list-inline mb-0">
                        <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Prospecto: <strong >{this.props.referido.nombre+' '+this.props.referido.ape_paterno + ' ' + (this.props.referido.ape_materno != null ? this.props.referido.ape_materno:'')}</strong></li>
                     <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Fecha de alta: <strong >{this.props.referido.ts_alta_audit}</strong></li>

                            <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Usuario creador: <strong >{this.state.entrevistador}</strong></li>
                            <li class="list-inline-item">
                                <h6 style={{ backgroundColor: "#8189D4", color: '#FFFF', borderRadius: '8px' }}> <>&nbsp;&nbsp;</> Entevista Profunda  <>&nbsp;&nbsp;</> </h6>
                            </li>
                        </ul>
                    </div>
                </div>
                <br></br>



                <div class="card-header bg-transparent header-elements-sm-inline" style={{ border: 'none' }}>
                    <h4 class="mb-0 font-weight-semibold" >IronTalent 2022</h4>
                    <div class="header-elements">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item">
                            {this.state.muestraInfoPSP == false &&
                                    <button data-toggle="modal" data-target="#modal-psp" 
                                    disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} class="btn  mr-1" style={{ backgroundColor: "#8189D4", color: '#FFFF' }}> <>&nbsp;&nbsp;</> Subir archivo PSP*  <>&nbsp;&nbsp;</> </button>
                                }
                                {this.state.muestraInfoPSP == true &&
                                    <button data-toggle="modal" data-target="#modal-psp" 
                                    disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} class="btn  mr-1" style={{ backgroundColor: "#78CB5A", color: '#FFFF' }}> <>&nbsp;&nbsp;</> Ver archivo PSP*  <>&nbsp;&nbsp;</> </button>
                                }
                            </li>
                        </ul>
                    </div>
                </div>

                <br></br>

                <div class="card">
                    <div class="card-header bg-transparent header-elements-sm-inline">
                        <h6 class="mb-0 font-weight-semibold">07. Evaluación del entrevistador</h6>
                        <div class="header-elements">
                            <ul class="list-inline mb-0">
                                <li class="list-inline-item">
                                    <h6 class="mb-0 font-weight-semibold"> <>&nbsp;&nbsp;</> 07/10  <>&nbsp;&nbsp;</> </h6>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="card-body">
                        <div className="row">
                            <div className="col-xs-12 col-lg-12">
                                <h8 class="mb-0 font-weight-semibold"> Puntualidad para la entrevista:</h8>
                            </div>
                        </div>
                        <br></br>
                        <div className="row">
                            <div className="col-xs-6 col-lg-12">
                                <div className="form-group">
                                    {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                    <input class="form-check-input" type="radio" name="puntualidad" id="noHubo" value={0} 
                                    disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                    && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospecto.puntualidad == 0} />
                                    <label class="form-check-label" for="noHubo">
                                        No hubo entrevista
                                    </label>
                                    {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                    <input class="form-check-input" type="radio" name="puntualidad" id="temprano" value={1} 
                                    disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                    && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospecto.puntualidad == 1} />
                                    <label class="form-check-label" for="temprano">
                                        Llegó temprano
                                    </label>
                                    {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                    <input class="form-check-input" type="radio" name="puntualidad" id="puntual" value={2} 
                                    disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                    && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospecto.puntualidad == 2} />
                                    <label class="form-check-label" for="puntual">
                                        Llegó puntual
                                    </label>
                                    {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                    <label class="form-check-label" for="casado">
                                        Llegó
                                    </label>
                                    {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                    <input type="number" name="minutos_tarde" value={this.state.prospecto.minutos_tarde} 
                                    disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8', borderRadius: '3px', border: '1px' }} />
                                    {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                    <label class="form-check-label" for="casado">
                                        minutos tarde
                                    </label>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-xs-12 col-lg-12">
                        <table className="table datatable-sorting  table-striped table-hover" >
                            <thead >
                                <tr>
                                    <th className="text-right font-weight-bold "  >
                                        <a style={{ color: '#e9f5fe' }}>-------------------</a>
                                    </th>
                                    <th className="text-right font-weight-bold "  >
                                        <a>SOBRESALIENTE</a>
                                    </th>
                                    <th className="text-right font-weight-bold " >
                                        <a>SOBRE EL PROMEDIO</a>
                                    </th>
                                    <th className="text-right font-weight-bold ">
                                        <a>PROMEDIO</a>
                                    </th>
                                    <th className="text-right font-weight-bold ">
                                        <a>BAJO EL PROMEDIO</a>
                                    </th>
                                    <th className="text-right font-weight-bold ">
                                        <a>INSATISFACTORIO</a>
                                    </th>
                                </tr>
                            </thead>

                        </table>
                    </div>
                </div>
                <div class="card">
                    <div className="card-body">
                        <div className="row">
                            <div className="col-xs-12 col-lg-12">
                                <h8 class="mb-0 font-weight-semibold"> Habilidad para la comunicación</h8>
                            </div>
                        </div>

                        <br></br>
                        <div className="row">
                            <div className="col-xs-2 col-lg-3 ">
                                <label style={{ color: '#617187' }} className="font-weight-semibold" for="salud">Habilidad para expresarse con palabras y gestos</label>
                            </div>

                            <div className="col-xs-12 col-lg-9  ">
                                <input type="range" style={{ marginLeft: '5px' }} class="custom-range" min="0" name="habilidad_expresarse" max="80" step={20} 
                                disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospecto.habilidad_expresarse} />
                            </div>
                        </div>
                        <br></br>
                        <div className="row">
                            <div className="col-xs-2 col-lg-3 ">
                                <label style={{ color: '#617187' }} className="font-weight-semibold" for="salud">Habilidad para sintetizar ideas, en forma clara y directa</label>
                            </div>

                            <div className="col-xs-12 col-lg-9  ">
                                <input type="range" style={{ marginLeft: '5px' }} class="custom-range" min="0" max="80" step={20} onChange={this.onChange} 
                                disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} name="habilidad_sintetizar" value={this.state.prospecto.habilidad_sintetizar} />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div className="card-body">
                        <div className="row">
                            <div className="col-xs-12 col-lg-12">
                                <h8 class="mb-0 font-weight-semibold">Habilidad para controlar situaciones</h8>
                            </div>
                        </div>

                        <br></br>
                        <div className="row">
                            <div className="col-xs-2 col-lg-3 ">
                                <label style={{ color: '#617187' }} className="font-weight-semibold" for="salud">Apariencia personal e impresión a primera vista</label>
                            </div>

                            <div className="col-xs-12 col-lg-9  ">
                                <input type="range" style={{ marginLeft: '5px' }} class="custom-range" min="0" max="80" step={20} 
                                disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} name="apariencia_personal" value={this.state.prospecto.apariencia_personal} />
                            </div>
                        </div>
                        <br></br>
                        <div className="row">
                            <div className="col-xs-2 col-lg-3 ">
                                <label style={{ color: '#617187' }} className="font-weight-semibold" for="salud">Demostración de confianza durante la entrevista</label>
                            </div>

                            <div className="col-xs-12 col-lg-9  ">
                                <input type="range" style={{ marginLeft: '5px' }} class="custom-range" min="0" max="80" step={20} 
                                disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} name="demostracion_confianza" value={this.state.prospecto.demostracion_confianza} />
                            </div>
                        </div>
                    </div>
                </div>



                <div class="card">
                    <div className="card-body">
                        <div className="row">
                            <div className="col-xs-12 col-lg-12">
                                <h8 class="mb-0 font-weight-semibold"> Interés en el puesto</h8>
                            </div>
                        </div>

                        <br></br>
                        <div className="row">
                            <div className="col-xs-2 col-lg-3 ">
                                <label style={{ color: '#617187' }} className="font-weight-semibold" for="salud">Se muestra seguro con la idea de un cambio de carrera</label>
                            </div>

                            <div className="col-xs-12 col-lg-9  ">
                                <input type="range" style={{ marginLeft: '5px' }} class="custom-range" min="0" max="80" step={20} disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} name="muestra_seguro" value={this.state.prospecto.muestra_seguro} />
                            </div>
                        </div>
                        <br></br>
                        <div className="row">
                            <div className="col-xs-2 col-lg-3 ">
                                <label style={{ color: '#617187' }} className="font-weight-semibold" for="salud">Se encuentra seriamente interesado en desarrollar una carrera de ventas</label>
                            </div>

                            <div className="col-xs-12 col-lg-9  ">
                                <input type="range" style={{ marginLeft: '5px' }} class="custom-range" min="0" max="80" step={20} 
                                disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} name="encuentra_seriamente" value={this.state.prospecto.muestraencuentra_seriamente_seguro} />
                            </div>
                        </div>
                    </div>
                </div>



                <div class="card">
                    <div className="card-body">
                        <div className="row">
                            <div className="col-xs-12 col-lg-12">
                                <h8 class="mb-0 font-weight-semibold"> Observaciones</h8>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-xs-12 col-lg-12">
                                <textarea style={{ width: '100%', height: '100%', backgroundColor: '#F1F3FA', borderColor: '#D5D9E8' }} 
                                disabled = {this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR" && this.props.tipoUsuario != "GDD"
                                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} name="observaciones" value={this.state.prospecto.observaciones}> </textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <HistoricoGeneral idProspecto={this.props.idProspecto} tipoUsuario={this.state.tipoUsuario} presentarSeccion = {"EP6"} />
            </div>
        )
    }
}