import React, { Component } from "react";
import ReporteCapacitacion from "./ReporteCapacitacion";
import ConvenioArranque from "./ConvenioArranque";
import HistoricoGeneral from "./HistoricoGeneral";
import axios from 'axios';
import ScrollTo from "react-scroll-into-view";
import datos from '../urls/datos.json'

const avatar = require("../imagenes/avatar.png")

export default class Conectado extends Component {
    constructor() {
        super();
        this.construyeSelect = this.construyeSelect.bind(this)
        this.construyeSelectMotivos = this.construyeSelectMotivos.bind(this)
        this.onClickBotonArchivoCapacitacion = this.onClickBotonArchivoCapacitacion.bind(this)
        this.guardarFormulario = this.guardarFormulario.bind(this)
        this.mostrarReporteCapacitacion = this.mostrarReporteCapacitacion.bind(this)
        this.onClickBotonArchivoConvenioFirmado = this.onClickBotonArchivoConvenioFirmado.bind(this)
        this.mostrarConectado = this.mostrarConectado.bind(this)
        this.onChangeCapacitacion = this.onChangeCapacitacion.bind(this)
        this.onChangeConvenio = this.onChangeConvenio.bind(this)
        this.mostrarConvenioArranque = this.mostrarConvenioArranque.bind(this)
        this.regresaryGuardar = this.regresaryGuardar.bind(this)
        //this.onChangeHijos = this.onChangeHijos.bind(this)
        this.onChangeAgente = this.onChangeAgente.bind(this)
        this.peticionAReserva = this.peticionAReserva.bind(this)
        this.peticionADefinitivo = this.peticionADefinitivo.bind(this)
        this.peticionABaja = this.peticionABaja.bind(this)

        this.state = {
            selectParentesco: undefined,
            bancorreoaps:false,
            prospectoConectado: {
                "id": null,
                "prospecto_id": null,
                "estatus_id": 17,
                "doc_foto": null,
                "conyuge": null,
                "fecha_conyuge": null,
                "doc_identificacion": null,
                "doc_acta_nacimiento": null,
                "doc_cerfificado": null,
                "doc_curp": null,
                "doc_rfc": null,
                "doc_cedula": null,
                "doc_carta_renuncia": null,
                "envio_correo": null,
                "fecha_examen_cei": null,
                "doc_examen_cei": null,
                "aprobacion_examen_cei": null,
                "doc_aprobacion_cei": null,
                "fecha_curso_cedula": null,
                "comprobante_cedula": null,
                "fecha_entrega_cedula": null,
                "fecha_alta_cnsf": null,
                "fecha_ini_cedula": null,
                "fecha_fin_cedula": null,
                "clave_cua": null,
                "clave_cua_provisional": null,
                "fecha_conexion_provicional": null,
                "fecha_conexion_definitiva": null,
                "antiguedad_agente": null,
                "alta_sat": null,
                "envio_gnp_multa": null,
                "curso_ideas": null,
                "generacion": null,
                "agente_consolidado_id": null,
                "correo_aps": null,
                "fecha_fin_tarjetas": null,
                "fecha_alta_boletin": null,
                "fecha_plenarias_aps": null,
                "fecha_alta_chats": null,
                "alta_chats_noveles": null,
                "aviso_gansos":null,
                "alta_agenda_aps": null,
                "alta_prospecto_ideas": null,
                "curso_inmersion_aps": null,
                "baja_curso_inmersion_aps": null,
                "doc_capacitacion": null,
                "doc_convenio": null,
                "baja_conexion": null
            },
            prospectoAgente: {
                "id": null,
                "prospecto_id": null,
                "fecha_aniversario": null,
                "hijo_uno": null,
                "fecha_hijo_uno": null,
                "hijo_dos": null,
                "fecha_hijo_dos": null,
                "hijo_tres": null,
                "fecha_hijo_tres": null,
                "hijo_cuatro": null,
                "fecha_hijo_cuatro": null,
                "hijo_cinco": null,
                "fecha_hijo_cinco": null,
                "asistente": null,
                "telefono_asistente": null,
                "correo_asistente": null,
                "fecha_nacimiento_asistente": null,
                "contacto": null,
                "parentesco_id": null,
                "telefono_contacto": null
            },
            archivoIdentificacion: undefined, nombreArchivoIdentificacion: "", urlIdentificacion: "",
            muestraDocIdentificacion: false,// se activa al edittar 
            colorSubirArchivoIdentificacion: "#78CB5A", colorTextoSubirArchivoIdentificacion: "#FFFFFF", colorBotonSubirArchivoIdentificacion: "#41B3D1",

            archivoActaNacimiento: undefined, nombreArchivoActaNacimiento: "", urlActaNacimiento: "",
            muestraDocActaNacimiento: false,// se activa al edittar 
            colorSubirArchivoActaNacimiento: "#78CB5A", colorTextoSubirArchivoActaNacimiento: "#FFFFFF", colorBotonSubirArchivoActaNacimiento: "#41B3D1",

            archivoCertificadoEstudios: undefined, nombreArchivoCertificadoEstudios: "", urlCertificadoEstudios: "",
            muestraDocCertificadoEstudios: false,// se activa al edittar 
            colorSubirArchivoCertificadoEstudios: "#78CB5A", colorTextoSubirArchivoCertificadoEstudios: "#FFFFFF", colorBotonSubirArchivoCertificadoEstudios: "#41B3D1",

            archivoCurp: undefined, nombreArchivoCurp: "", urlCurp: "",
            muestraDocCurp: false,// se activa al edittar 
            colorSubirArchivoCurp: "#78CB5A", colorTextoSubirArchivoCurp: "#FFFFFF", colorBotonSubirArchivoCurp: "#41B3D1",

            archivoRFC: undefined, nombreArchivoRFC: "", urlRFC: "",
            muestraDocRFC: false,// se activa al edittar 
            colorSubirArchivoRFC: "#78CB5A", colorTextoSubirArchivoRFC: "#FFFFFF", colorBotonSubirArchivoRFC: "#41B3D1",

            archivoCedula: undefined, nombreArchivoCedula: "", urlCedula: "",
            muestraDocCedula: false,// se activa al edittar 
            colorSubirArchivoCedula: "#78CB5A", colorTextoSubirArchivoCedula: "#FFFFFF", colorBotonSubirArchivoCedula: "#41B3D1",

            archivoRenuncia: undefined, nombreArchivoRenuncia: "", urlRenuncia: "",
            muestraDocRenuncia: false,// se activa al edittar 
            colorSubirArchivoRenuncia: "#78CB5A", colorTextoSubirArchivoRenuncia: "#FFFFFF", colorBotonSubirArchivoRenuncia: "#41B3D1",

            archivoComprobantePagoCEI: undefined, nombreArchivoComprobantePagoCEI: "", urlComprobantePagoCEI: "",
            muestraDocComprobantePagoCEI: false,// se activa al edittar 
            colorSubirArchivoComprobantePagoCEI: "#78CB5A", colorTextoSubirArchivoComprobantePagoCEI: "#FFFFFF", colorBotonSubirArchivoComprobantePagoCEI: "#41B3D1",

            archivoAprobacionExamenCEI: undefined, nombreArchivoAprobacionExamenCEI: "", urlAprobacionExamenCEI: "",
            muestraDocAprobacionExamenCEI: false,// se activa al edittar 
            colorSubirArchivoAprobacionExamenCEI: "#78CB5A", colorTextoSubirArchivoAprobacionExamenCEI: "#FFFFFF", colorBotonSubirArchivoAprobacionExamenCEI: "#41B3D1",

            archivoConvenioFirmado: undefined, nombreArchivoConvenioFirmado: "", urlConvenioFirmado: "",
            muestraDocConvenioFirmado: false,// se activa al edittar 
            colorSubirArchivoConvenioFirmado: "#78CB5A", colorTextoSubirArchivoConvenioFirmado: "#FFFFFF", colorBotonSubirArchivoConvenioFirmado: "#41B3D1",

            archivoCapacitacion: undefined, nombreArchivoCapacitacion: "", urlCapacitacion: "",
            muestraDocCapacitacion: false,// se activa al edittar 
            colorSubirArchivoCapacitacion: "#78CB5A", colorTextoSubirArchivoCapacitacion: "#FFFFFF", colorBotonSubirArchivoCapacitacion: "#41B3D1",
            expresionEmail: /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
            validaEmail: false,
            entrevistador: '',
            conectado: {},
            ban_baja_conexion: false,
            selectMotivosReserva: [],
            selectMotivosBaja: [],
            idMotivoBaja: 0,
            idMotivoReserva: 0,
            mostrarReporteCapacitacion: false,
            mostrarConvenioArranque: false,
            tipoConexion: '',
            definitivo: false,
            provisional: false,
            ban_baja_conexion: false,
            ban_ventaCarrera: false,
            clickScroll: true,
            contadorHijos: 1,
            hijo1: {},
            hijo1Edicion: false,
            hijo2: {},
            hijo2Edicion: false,
            hijo3: {},
            hijo3Edicion: false,
            hijo4: {},
            hijo4Edicion: false,
            hijo5: {},
            hijo5Edicion: false,
            hijo_Eliminado2: false,
            hijo_Eliminado3: false,
            hijo_Eliminado4: false,
            hijo_Eliminado5: false
        };
    }


    construyeSelect(url, selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)
                for (var i = 0; i < filas.length; i++) {
                    let fila = filas[i]
                    if (selector == "selectMotivosBaja") {
                        if (this.state.prospectoConectado.baja_curso_inmersion_aps == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }
                    } else if (selector == "selectParentesco") {
                        if (this.state.prospectoAgente.parentesco_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }
                    } 
                }
                let salida = []
                let banBloquear = false;
                /*if (selector == "selectMotivosBaja") {
                    banBloquear = true;
                }*/
                salida.push(<select disabled={banBloquear} onChange={this.onChange} name={selector} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ [selector]: salida })
            });
    }

    inyectaHijos(banInyecta) {
        console.log("inyeccion")
        let contadorHijos = this.state.contadorHijos
        let eliminarHijos = this.state.prospectoAgente

        if (contadorHijos >= 1 && contadorHijos < 5) {
            if (banInyecta) {
                contadorHijos = this.state.contadorHijos + 1
                this.setState({ contadorHijos: contadorHijos })
                if (this.state.hijo_Eliminado2) {
                    this.setState({ hijo_Eliminado2: false })
                }
                if (this.state.hijo_Eliminado3) {
                    this.setState({ hijo_Eliminado3: false })
                }
                if (this.state.hijo_Eliminado4) {
                    this.setState({ hijo_Eliminado4: false })
                }
                if (this.state.hijo_Eliminado5) {
                    this.setState({ hijo_Eliminado5: false })
                }
            } else {
                if (contadorHijos == 2) {
                    contadorHijos = this.state.contadorHijos - 1
                    this.setState({ contadorHijos: contadorHijos, hijo_Eliminado2: true })
                    eliminarHijos.hijo_dos = null;
                    eliminarHijos.fecha_hijo_dos = null;
                } else if (contadorHijos == 3) {
                    contadorHijos = this.state.contadorHijos - 1
                    this.setState({ contadorHijos: contadorHijos, hijo_Eliminado3: true })
                    eliminarHijos.hijo_tres = null;
                    eliminarHijos.fecha_hijo_tres = null;
                } else if (contadorHijos == 4) {
                    contadorHijos = this.state.contadorHijos - 1
                    this.setState({ contadorHijos: contadorHijos, hijo_Eliminado4: true })
                    eliminarHijos.hijo_cuatro = null;
                    eliminarHijos.fecha_hijo_cuatro = null;
                }

            }
        } else {
            if (contadorHijos == 5 && !banInyecta) {
                contadorHijos = this.state.contadorHijos - 1
                this.setState({ contadorHijos: contadorHijos, hijo_Eliminado5: true })
                eliminarHijos.hijo_cinco = null;
                eliminarHijos.fecha_hijo_cinco = null;
            }
        }
    }

    onChange = e => {
        let campoConectado = e.target.name
        let prospectoConectado = this.state.prospectoConectado
        let prospectoAgente = this.state.prospectoAgente;
        console.log("name :"+campoConectado);

        if (e.target.name == "envio_correo" || e.target.name == "aprobacion_examen_cei" || e.target.name == "comprobante_cedula" || e.target.name == "alta_sat"
            || e.target.name == "envio_gnp_multa" || e.target.name == "curso_ideas" || e.target.name == "alta_chats_noveles" || e.target.name == "alta_agenda_aps" || e.target.name == "alta_prospecto_ideas"
            || e.target.name == "curso_inmersion_aps") {
            if (e.target.checked == true) {
                prospectoConectado["" + e.target.name + ""] = 1
            } else {
                prospectoConectado["" + e.target.name + ""] = 0
            }
        } else if (e.target.name == "curso_cedula" || e.target.name == "participacion_edat") {
            if (e.target.checked == true) {
                prospectoConectado["" + e.target.name + ""] = 1
            } else {
                prospectoConectado["" + e.target.name + ""] = 0
            }
        } else if (e.target.name == "curso_inmersion_aps") {
            if (e.target.checked == true) {
                prospectoConectado["" + e.target.name + ""] = 1
            } else {
                prospectoConectado["" + e.target.name + ""] = 0
            }
        } else if (e.target.name == "grupo_explorando") {
            console.log("entro en grupo_explorando :::");
            if (e.target.checked == true) {
                prospectoConectado["" + e.target.name + ""] = parseInt(e.target.value)
            } else {
                prospectoConectado["" + e.target.name + ""] = 0
            }
        } else if (e.target.name == "aviso_gansos") {
            console.log("entro en aviso_gansos :::");
            if (e.target.checked == true) {
                prospectoConectado["" + e.target.name + ""] = 1
            } else {
                prospectoConectado["" + e.target.name + ""] = 0
            }
        } else if (e.target.name == "selectGDD") {
            prospectoConectado.gdd = parseInt(e.target.value)
        } else if (e.target.name == "selectTipoConexion") {
            prospectoConectado.conexion_id = parseInt(e.target.value)
        } else if (e.target.name == "espiritu_emprendedor_gdd" || e.target.name == "mercado_natural_gdd" || e.target.name == "apoyo_familiar_gdd" || e.target.name == "estabilidad_financiera_gdd" ||
            e.target.name == "educacion_gdd" || e.target.name == "movilidad_social_gdd") {
            if (e.target.checked == true) {
                prospectoConectado["" + e.target.name + ""] = 1
            } else {
                prospectoConectado["" + e.target.name + ""] = 0
            }
        } else if (e.target.name == "baja_conexion") {
            if (e.target.checked == true) {
                this.setState({ ban_baja_conexion: true })
                prospectoConectado["" + e.target.name + ""] = 1
            } else {
                prospectoConectado["" + e.target.name + ""] = 0
                prospectoConectado.baja_curso_inmersion_aps = 0
                this.setState({ ["selectMotivosBaja"]: null })
                this.construyeSelect(datos.urlServicePy+"parametros/api_cat_causas_baja/0", "selectMotivosBaja")
                this.setState({ ban_baja_conexion: false })
            }
        } else if (e.target.name == "alta_chats_noveles" || e.target.name == "alta_agenda_aps") {
            if (e.target.checked == true) {
                prospectoConectado["" + e.target.name + ""] = 1
            } else {
                prospectoConectado["" + e.target.name + ""] = 0

            }
        } else if ("correo_aps" == e.target.name) {
            let email = e.target.value;
            if (email.match(this.state.expresionEmail)) {
                this.setState({ validaEmail: true })
            } else {
                this.setState({ validaEmail: false })
            }
            prospectoConectado["" + campoConectado + ""] = e.target.value
        } else if ("selectParentesco" == e.target.name) {
            console.log("entro")
            prospectoAgente.parentesco_id = parseInt(e.target.value)
            this.setState({ prospectoAgente: prospectoAgente })
            //this.setState({ parentesco_id: parseInt(e.target.value) })
        } else if (e.target.name == 'selectMotivosReserva') {
            this.setState({ idMotivoReserva: parseInt(e.target.value) })
        } else if (e.target.name == 'selectMotivosBaja') {
            this.setState({ idMotivoBaja: parseInt(e.target.value) })
        } else {
            prospectoConectado["" + campoConectado + ""] = e.target.value
        }
        console.log("entrando", e.target.name, e.target.value)
        this.setState({ prospectoConectado: prospectoConectado })
    }

    onChangeAgente = e => {
        let campoConectado = e.target.name
        let prospectoAgente = this.state.prospectoAgente;
        if (e.target.name == 'telefono_asistente') {
            let telefono_asistente = e.target.value
            if (telefono_asistente.length <= 10) {
                prospectoAgente["" + campoConectado + ""] = e.target.value
            }

        } else if (e.target.name == 'telefono_contacto') {
            let telefono_contacto = e.target.value
            if (telefono_contacto.length <= 10) {
                prospectoAgente["" + campoConectado + ""] = e.target.value
            }

        } else {
            prospectoAgente["" + campoConectado + ""] = e.target.value
        }
        this.setState({ prospectoAgente: prospectoAgente })
        console.log("Entro en agente")
    }

    onChangeFileCapacitacion(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file de puesto", file);
        this.setState({ archivoCapacitacion: file, colorSubirArchivoCapacitacion: "#78CB5A", colorTextoSubirArchivoCapacitacion: "#FFFFFF", colorBotonSubirArchivoCapacitacion: "#E5F6E0", nombreArchivoCapacitacion: file.name })
    }

    onClickBotonArchivoCapacitacion() {
        console.log("entrnaod a la imagen")
        this.capacitacion.click()
    }

    mostrarReporteCapacitacion() {
        this.guardarFormulario(false)
    }

    onClickBotonArchivoConvenioFirmado() {
        console.log("entrnaod a la imagen")
        this.convenioFirmado.click()
    }

    onChangeFileConvenioFirmado(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file de puesto", file);
        this.setState({ archivoConvenioFirmado: file, colorSubirArchivoConvenioFirmado: "#78CB5A", colorTextoSubirArchivoConvenioFirmado: "#FFFFFF", colorBotonSubirArchivoConvenioFirmado: "#E5F6E0", nombreArchivoConvenioFirmado: file.name })
    }

    regresaryGuardar() {
        if(this.state.prospectoConectado.aviso_gansos && (this.props.tipoUsuario == "COORCAP" || this.props.tipoUsuario == "ASESJRCAP" || this.props.tipoUsuario == "GTECAP" || this.props.tipoUsuario == "SUPER")){
            let jsonNotificacion = {
                "mail_id": 17,
                "prospecto_id": this.props.idProspecto
            }
            const requestOptions = {
                method: "POST",
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(jsonNotificacion)
            };

            fetch(datos.urlServicePy+"recluta/api_notificador/0", requestOptions)
                .then(response => response.json())
                .then(data => {})
        }
        this.guardarFormulario(true)
    }

    mostrarConvenioArranque() {
        this.setState({ mostrarReporteCapacitacion: false, mostrarConvenioArranque: true })
    }

    onChangeCapacitacion() {
        if (this.state.clickScroll) {
            this.setState({ clickScroll: false })
        } else {
            this.setState({ clickScroll: true })
        }
        this.conectadoCapacitacion.click();
    }

    onChangeConvenio(){
        this.conectadoConvenio.click();
    }

    peticionADefinitivo() {
        let json = {
            "estatus_id": 22
        }
        const requestOptions = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };
        console.log("enviando a peticion Definitiva")

        fetch(datos.urlServicePy+"parametros_upd/api_recluta_prospectos/" + this.props.idProspecto, requestOptions)
            .then(response => response.json())
            .then(data => {
                fetch(datos.urlServicePy+"recluta/api_recluta_ventacarrera16/" + this.props.idProspecto)
                .then(response => response.json())
                .then(jsonVenta => {
                    if(jsonVenta.length > 0){
                        let jsonRequest = jsonVenta[0];
                        jsonRequest.conexion_id = 1;
                        const requestOptions = {
                            method: "POST",
                            headers: { 'Content-Type': 'application/json' },
                            body: JSON.stringify(jsonRequest)
                        };
                        fetch(datos.urlServicePy+"recluta/api_recluta_ventacarrera16/" + this.props.idProspecto, requestOptions)
                            .then(response => response.json())
                            .then(data => {
                                this.props.botonCancelar()
                            })
        
                    }
                })
            })
    }

    peticionAReserva() {
        let json = {
            "estatus_id": 18,
            "causas_baja_id": this.state.idMotivoBaja

        }
        const requestOptions = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };
        console.log("enviando a reserva")

        fetch(datos.urlServicePy+"parametros_upd/api_recluta_prospectos/" + this.props.idProspecto, requestOptions)
            .then(response => response.json())
            .then(data => {
                this.props.botonCancelar()
            })

    }

    peticionABaja() {
        let json = {
            "estatus_id": 19,
            "causas_baja_id": this.state.idMotivoBaja
        }
        const requestOptions = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };
        console.log("enviando a reserva")

        fetch(datos.urlServicePy+"parametros_upd/api_recluta_prospectos/" + this.props.idProspecto, requestOptions)
            .then(response => response.json())
            .then(data => {
                //this.construyeTarjetas()
                // this.props.guardaFormulario10(this.state.prospecto)
                this.props.botonCancelar()
            })
    }
    mostrarConectado() {
        fetch(datos.urlServicePy+"recluta/api_recluta_conexion17/" + this.props.idProspecto)
            .then(response => response.json())
            .then(existeProspecto => {
                this.setState({ mostrarReporteCapacitacion: false, mostrarConvenioArranque: true })

            })

        fetch(datos.urlServicePy+"recluta/api_recluta_conexion17/" + this.props.idProspecto)
            .then(response => response.json())
            .then(existeProspecto => {
                if (existeProspecto.length > 0) {
                    this.setState({ prospectoConectado: existeProspecto[0] })

                    let prospectoConectado = existeProspecto[0]

                    if (prospectoConectado.doc_identificacion != null && prospectoConectado.doc_identificacion.length > 0) {
                        this.setState({
                            urlIdentificacion: prospectoConectado.doc_identificacion, muestraDocIdentificacion: true, colorSubirArchivoIdentificacion: "#78CB5A",
                            colorTextoSubirArchivoIdentificacion: "#FFFFFF", colorBotonSubirArchivoIdentificacion: "#E5F6E0", nombreArchivoIdentificacion: 'modificar'
                        })
                    }
                    if (prospectoConectado.doc_acta_nacimiento != null && prospectoConectado.doc_acta_nacimiento.length > 0) {
                        this.setState({
                            urlActaNacimiento: prospectoConectado.doc_acta_nacimiento, muestraDocActaNacimiento: true, colorSubirArchivoActaNacimiento: "#78CB5A",
                            colorTextoSubirArchivoActaNacimiento: "#FFFFFF", colorBotonSubirArchivoActaNacimiento: "#E5F6E0", nombreArchivoActaNacimiento: 'modificar'
                        })
                    }
                    if (prospectoConectado.doc_cerfificado != null && prospectoConectado.doc_cerfificado.length > 0) {
                        this.setState({
                            urlCertificadoEstudios: prospectoConectado.doc_cerfificado, muestraDocCertificadoEstudios: true, colorSubirArchivoCertificadoEstudios: "#78CB5A",
                            colorTextoSubirArchivoCertificadoEstudios: "#FFFFFF", colorBotonSubirArchivoCertificadoEstudios: "#E5F6E0", nombreArchivoCertificadoEstudios: 'modificar'
                        })
                    }
                    if (prospectoConectado.doc_curp != null && prospectoConectado.doc_curp.length > 0) {
                        this.setState({
                            urlCurp: prospectoConectado.doc_curp, muestraDocCurp: true, colorSubirArchivoCurp: "#78CB5A",
                            colorTextoSubirArchivoCurp: "#FFFFFF", colorBotonSubirArchivoCurp: "#E5F6E0", nombreArchivoCurp: 'modificar'
                        })
                    }
                    if (prospectoConectado.doc_rfc != null && prospectoConectado.doc_rfc.length > 0) {
                        this.setState({
                            urlRFC: prospectoConectado.doc_rfc, muestraDocRFC: true, colorSubirArchivoRFC: "#78CB5A",
                            colorTextoSubirArchivoRFC: "#FFFFFF", colorBotonSubirArchivoRFC: "#E5F6E0", nombreArchivoRFC: 'modificar'
                        })
                    }
                    if (prospectoConectado.doc_cedula != null && prospectoConectado.doc_cedula.length > 0) {
                        this.setState({
                            urlCedula: prospectoConectado.doc_cedula, muestraDocCedula: true, colorSubirArchivoCedula: "#78CB5A",
                            colorTextoSubirArchivoCedula: "#FFFFFF", colorBotonSubirArchivoCedula: "#E5F6E0", nombreArchivoCedula: 'modificar'
                        })
                    }
                    if (prospectoConectado.doc_carta_renuncia != null && prospectoConectado.doc_carta_renuncia.length > 0) {
                        this.setState({
                            urlRenuncia: prospectoConectado.doc_carta_renuncia, muestraDocRenuncia: true, colorSubirArchivoRenuncia: "#78CB5A",
                            colorTextoSubirArchivoRenuncia: "#FFFFFF", colorBotonSubirArchivoRenuncia: "#E5F6E0", nombreArchivoRenuncia: 'modificar'
                        })
                    }
                    if (prospectoConectado.doc_examen_cei != null && prospectoConectado.doc_examen_cei.length > 0) {
                        this.setState({
                            urlComprobantePagoCEI: prospectoConectado.doc_examen_cei, muestraDocComprobantePagoCEI: true, colorSubirArchivoComprobantePagoCEI: "#78CB5A",
                            colorTextoSubirArchivoComprobantePagoCEI: "#FFFFFF", colorBotonSubirArchivoComprobantePagoCEI: "#E5F6E0", nombreArchivoComprobantePagoCEI: 'modificar'
                        })
                    }

                    if (prospectoConectado.doc_aprobacion_cei != null && prospectoConectado.doc_aprobacion_cei.length > 0) {
                        this.setState({
                            urlAprobacionExamenCEI: prospectoConectado.doc_aprobacion_cei, muestraDocAprobacionExamenCEI: true, colorSubirArchivoAprobacionExamenCEI: "#78CB5A",
                            colorTextoSubirArchivoAprobacionExamenCEI: "#FFFFFF", colorBotonSubirArchivoAprobacionExamenCEI: "#E5F6E0", nombreArchivoAprobacionExamenCEI: 'modificar'
                        })
                    }

                    if (prospectoConectado.doc_convenio != null && prospectoConectado.doc_convenio.length > 0) {
                        this.setState({
                            urlConvenioFirmado: prospectoConectado.doc_convenio, muestraDocConvenioFirmado: true, colorSubirArchivoConvenioFirmado: "#78CB5A",
                            colorTextoSubirArchivoConvenioFirmado: "#FFFFFF", colorBotonSubirArchivoConvenioFirmado: "#E5F6E0", nombreArchivoConvenioFirmado: 'modificar'
                        })
                    }
                    if (prospectoConectado.doc_capacitacion != null && prospectoConectado.doc_capacitacion.length > 0) {
                        this.setState({
                            urlCapacitacion: prospectoConectado.doc_capacitacion, muestraDocCapacitacion: true, colorSubirArchivoCapacitacion: "#78CB5A",
                            colorTextoSubirArchivoCapacitacion: "#FFFFFF", colorBotonSubirArchivoCapacitacion: "#E5F6E0", nombreArchivoCapacitacion: 'modificar'
                        })
                    }

                    console.log("el props ", prospectoConectado)
                    if (prospectoConectado.doc_foto != null && prospectoConectado.doc_foto.length > 0) {
                        console.log("la puta foto")
                        this.setState({
                            imagenEnCambio: prospectoConectado.doc_foto,
                            urlIdentificacion: prospectoConectado.doc_foto
                        })
                    }



                }
                this.construyeSelect(datos.urlServicePy+"parametros/api_cat_causas_baja/0", "selectMotivosBaja")
                this.setState({ mostrarReporteCapacitacion: false, mostrarConvenioArranque: false })
            })
    }

    guardarFormulario(banFormulario) {
        console.log("json ", this.state.prospectoConectado)
        let json = this.state.prospectoConectado
        let jsonAgente = this.state.prospectoAgente
        jsonAgente.id = this.props.idProspecto
        jsonAgente.prospecto_id = this.props.idProspecto
        json.id = this.props.idProspecto
        json.prospecto_id = this.props.idProspecto
        json.doc_foto = null
        json.doc_identificacion = null
        json.doc_acta_nacimiento = null
        json.doc_curp = null
        json.doc_rfc = null
        json.doc_cedula = null
        json.doc_carta_renuncia = null
        json.doc_examen_cei = null
        json.doc_aprobacion_cei = null
        json.doc_cerfificado = null

        const requestOptions = {
            method: "POST",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        }
        fetch(datos.urlServicePy+"recluta/api_recluta_conexion17/" + this.props.idProspecto, requestOptions)
            .then(response => response.json())
            .then(data => {
                let formData = new FormData();
                formData.append('id', this.props.idProspecto);
                formData.append('prospecto_id', this.props.idProspecto);

                if (this.state.fotografiaFile != undefined) { formData.append('doc_foto', this.state.fotografiaFile) } else {/*formData.append('doc_foto', null)*/ }
                if (this.state.archivoIdentificacion != undefined) { formData.append('doc_identificacion', this.state.archivoIdentificacion) } else {/*formData.append('doc_identificacion', null)*/ }
                if (this.state.archivoActaNacimiento != undefined) { formData.append('doc_acta_nacimiento', this.state.archivoActaNacimiento) } else {/*formData.append('doc_acta_nacimiento', null)*/ }
                if (this.state.archivoCertificadoEstudios != undefined) { formData.append('doc_cerfificado', this.state.archivoCertificadoEstudios) } else { }
                if (this.state.archivoCurp != undefined) { formData.append('doc_curp', this.state.archivoCurp) } else {/*formData.append('doc_curp',null)*/ }
                if (this.state.archivoRFC != undefined) { formData.append('doc_rfc', this.state.archivoRFC) } else {/*formData.append('doc_rfc', this.state.archivoRFC)*/ }
                if (this.state.archivoCedula != undefined) { formData.append('doc_cedula', this.state.archivoCedula) } else {/*formData.append('doc_cedula', null)*/ }
                if (this.state.archivoRenuncia != undefined) { formData.append('doc_carta_renuncia', this.state.archivoRenuncia) } else {/*formData.append('doc_carta_renuncia', null)*/ }
                if (this.state.archivoComprobantePagoCEI != undefined) { formData.append('doc_examen_cei', this.state.archivoComprobantePagoCEI) } else {/*formData.append('doc_examen_cei', null)*/ }
                if (this.state.archivoAprobacionExamenCEI != undefined) { formData.append('doc_aprobacion_cei', this.state.archivoAprobacionExamenCEI) } else {/*formData.append('doc_aprobacion_cei',null)*/ }
                
                const config = {
                    headers: { 'content-type': 'multipart/form-data' }
                }

                axios.put(datos.urlServicePy+"api/doc_conexion/" + this.props.idProspecto + "/", formData, config)
                    .then(res => {
                        console.log(res.data);
                    })
            })

        fetch(datos.urlServicePy+"recluta/api_agente_expediente/" + this.props.idProspecto)
            .then(response => response.json())
            .then(jsonExpediente => {
                let urlAgente = datos.urlServicePy+"recluta/api_agente_expediente/";
                if (jsonExpediente.length > 0) {
                    urlAgente = urlAgente + this.props.idProspecto;
                } else {
                    urlAgente = urlAgente + "0";
                }
                const requestAgente = {
                    method: "POST",
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(jsonAgente)
                }
                fetch(urlAgente, requestAgente)
                    .then(response => response.json())
                    .then(data => {
                        console.log("Se agrego y/o actualizo agente ");
                    });
            });

        if (banFormulario) {
            this.props.botonCancelar()
        } else {
            this.setState({ mostrarReporteCapacitacion: true, mostrarConvenioArranque: false })
        }
    }

    UNSAFE_componentWillMount() {
        fetch(datos.urlServicePy+"recluta/api_recluta_conexion17/" + this.props.idProspecto)
            .then(response => response.json())
            .then(existeProspecto => {
                if (existeProspecto.length > 0) {
                    this.setState({ prospectoConectado: existeProspecto[0] })

                    let prospectoConectado = existeProspecto[0]

                    if (prospectoConectado.doc_identificacion != null && prospectoConectado.doc_identificacion.length > 0) {
                        this.setState({
                            urlIdentificacion: prospectoConectado.doc_identificacion, muestraDocIdentificacion: true, colorSubirArchivoIdentificacion: "#78CB5A",
                            colorTextoSubirArchivoIdentificacion: "#FFFFFF", colorBotonSubirArchivoIdentificacion: "#E5F6E0", nombreArchivoIdentificacion: 'modificar'
                        })
                    }
                    if (prospectoConectado.doc_acta_nacimiento != null && prospectoConectado.doc_acta_nacimiento.length > 0) {
                        this.setState({
                            urlActaNacimiento: prospectoConectado.doc_acta_nacimiento, muestraDocActaNacimiento: true, colorSubirArchivoActaNacimiento: "#78CB5A",
                            colorTextoSubirArchivoActaNacimiento: "#FFFFFF", colorBotonSubirArchivoActaNacimiento: "#E5F6E0", nombreArchivoActaNacimiento: 'modificar'
                        })
                    }
                    if (prospectoConectado.doc_cerfificado != null && prospectoConectado.doc_cerfificado.length > 0) {
                        this.setState({
                            urlCertificadoEstudios: prospectoConectado.doc_cerfificado, muestraDocCertificadoEstudios: true, colorSubirArchivoCertificadoEstudios: "#78CB5A",
                            colorTextoSubirArchivoCertificadoEstudios: "#FFFFFF", colorBotonSubirArchivoCertificadoEstudios: "#E5F6E0", nombreArchivoCertificadoEstudios: 'modificar'
                        })
                    }
                    if (prospectoConectado.doc_curp != null && prospectoConectado.doc_curp.length > 0) {
                        this.setState({
                            urlCurp: prospectoConectado.doc_curp, muestraDocCurp: true, colorSubirArchivoCurp: "#78CB5A",
                            colorTextoSubirArchivoCurp: "#FFFFFF", colorBotonSubirArchivoCurp: "#E5F6E0", nombreArchivoCurp: 'modificar'
                        })
                    }
                    if (prospectoConectado.doc_rfc != null && prospectoConectado.doc_rfc.length > 0) {
                        this.setState({
                            urlRFC: prospectoConectado.doc_rfc, muestraDocRFC: true, colorSubirArchivoRFC: "#78CB5A",
                            colorTextoSubirArchivoRFC: "#FFFFFF", colorBotonSubirArchivoRFC: "#E5F6E0", nombreArchivoRFC: 'modificar'
                        })
                    }
                    if (prospectoConectado.doc_cedula != null && prospectoConectado.doc_cedula.length > 0) {
                        this.setState({
                            urlCedula: prospectoConectado.doc_cedula, muestraDocCedula: true, colorSubirArchivoCedula: "#78CB5A",
                            colorTextoSubirArchivoCedula: "#FFFFFF", colorBotonSubirArchivoCedula: "#E5F6E0", nombreArchivoCedula: 'modificar'
                        })
                    }
                    if (prospectoConectado.doc_carta_renuncia != null && prospectoConectado.doc_carta_renuncia.length > 0) {
                        this.setState({
                            urlRenuncia: prospectoConectado.doc_carta_renuncia, muestraDocRenuncia: true, colorSubirArchivoRenuncia: "#78CB5A",
                            colorTextoSubirArchivoRenuncia: "#FFFFFF", colorBotonSubirArchivoRenuncia: "#E5F6E0", nombreArchivoRenuncia: 'modificar'
                        })
                    }
                    if (prospectoConectado.doc_examen_cei != null && prospectoConectado.doc_examen_cei.length > 0) {
                        this.setState({
                            urlComprobantePagoCEI: prospectoConectado.doc_examen_cei, muestraDocComprobantePagoCEI: true, colorSubirArchivoComprobantePagoCEI: "#78CB5A",
                            colorTextoSubirArchivoComprobantePagoCEI: "#FFFFFF", colorBotonSubirArchivoComprobantePagoCEI: "#E5F6E0", nombreArchivoComprobantePagoCEI: 'modificar'
                        })
                    }

                    if (prospectoConectado.doc_aprobacion_cei != null && prospectoConectado.doc_aprobacion_cei.length > 0) {
                        this.setState({
                            urlAprobacionExamenCEI: prospectoConectado.doc_aprobacion_cei, muestraDocAprobacionExamenCEI: true, colorSubirArchivoAprobacionExamenCEI: "#78CB5A",
                            colorTextoSubirArchivoAprobacionExamenCEI: "#FFFFFF", colorBotonSubirArchivoAprobacionExamenCEI: "#E5F6E0", nombreArchivoAprobacionExamenCEI: 'modificar'
                        })
                    }

                    if (prospectoConectado.doc_convenio != null && prospectoConectado.doc_convenio.length > 0) {
                        this.setState({
                            urlConvenioFirmado: prospectoConectado.doc_convenio, muestraDocConvenioFirmado: true, colorSubirArchivoConvenioFirmado: "#78CB5A",
                            colorTextoSubirArchivoConvenioFirmado: "#FFFFFF", colorBotonSubirArchivoConvenioFirmado: "#E5F6E0", nombreArchivoConvenioFirmado: 'modificar'
                        })
                    }
                    if (prospectoConectado.doc_capacitacion != null && prospectoConectado.doc_capacitacion.length > 0) {
                        this.setState({
                            urlCapacitacion: prospectoConectado.doc_capacitacion, muestraDocCapacitacion: true, colorSubirArchivoCapacitacion: "#78CB5A",
                            colorTextoSubirArchivoCapacitacion: "#FFFFFF", colorBotonSubirArchivoCapacitacion: "#E5F6E0", nombreArchivoCapacitacion: 'modificar'
                        })
                    }
                    if (prospectoConectado.doc_foto != null && prospectoConectado.doc_foto.length > 0) {
                        this.setState({
                            imagenEnCambio: prospectoConectado.doc_foto
                        })
                    }

                    if (prospectoConectado.correo_aps != null) {
                        this.setState({ validaEmail: true })
                    }
                }

                this.setState({ mostrarReporteCapacitacion: false, mostrarConvenioArranque: false })
            })
        fetch(datos.urlServicePy+"parametros/api_recluta_prospectos/" + this.props.idProspecto)
            .then(response => response.json())
            .then(recluta_prospecto => {
                fetch(datos.urlServicePy+"parametros/api_cat_empleados/" + recluta_prospecto.filas[0].fila[12].value)
                    .then(response => response.json())
                    .then(entrevistador => {
                        console.log("entrevistador ", entrevistador)
                        this.setState({ entrevistador: entrevistador.filas[0].fila[2].value + " " + entrevistador.filas[0].fila[3].value + " " + (entrevistador.filas[0].fila[4].value != null ? entrevistador.filas[0].fila[4].value : '') })
                    })

            })
        fetch(datos.urlServicePy+"parametros/api_recluta_prospectos/" + this.props.idProspecto)
            .then(response => response.json())
            .then(json => {
                let fila = json.filas[0].fila
                let columnas = json.columnas
                let columnasSalida = {}
                for (var i = 0; i < fila.length; i++) {
                    console.log(fila[i])
                    columnasSalida["" + columnas[i].key + ""] = fila[i].value
                }

                this.setState({ conectado: columnasSalida })
            })
        fetch(datos.urlServicePy+"recluta/api_recluta_ventacarrera16/" + this.props.idProspecto)
            .then(response => response.json())
            .then(jsonVenta => {
                if (jsonVenta.length > 0) {
                    this.setState({ prospecto_ventaCarrera: jsonVenta[0] })
                    if (jsonVenta[0].curso_cedula != null) {
                        if (jsonVenta[0].curso_cedula == 1) {
                            this.setState({ ban_ventaCarrera: true })
                        }
                    }
                    if (jsonVenta[0].correo_aps == 1) {
                        this.setState({bancorreoaps:true})
                    }
                    if (jsonVenta[0].conexion_id == 1) {
                        this.setState({ tipoConexion: "Agente definitivo", definitivo: true })
                    } else {
                        this.setState({ tipoConexion: "Agente provisional", provisional: true })
                    }
                }
            })

        fetch(datos.urlServicePy+"recluta/api_agente_expediente/" + this.props.idProspecto)
            .then(response => response.json())
            .then(jsonExpediente => {
                if (jsonExpediente.length > 0) {
                    console.log("existe agente expediente");
                    let prospectoAgente = jsonExpediente;
                    let contadorHijos = this.state.contadorHijos
                    //contadorHijos
                    if (prospectoAgente[0].hijo_dos != null) {
                        contadorHijos = contadorHijos + 1
                    }
                    if (prospectoAgente[0].hijo_tres != null) {
                        contadorHijos = contadorHijos + 1
                    }
                    if (prospectoAgente[0].hijo_cuatro != null) {
                        contadorHijos = contadorHijos + 1
                    }
                    if (prospectoAgente[0].hijo_cinco != null) {
                        contadorHijos = contadorHijos + 1
                    }
                    this.setState({ prospectoAgente: prospectoAgente[0], contadorHijos: contadorHijos })
                }
            });

        this.construyeSelect(datos.urlServicePy+"parametros/api_cat_parentesco/0", "selectParentesco")
        this.construyeSelectMotivos(datos.urlServicePy+"parametros/api_cat_motivo_envio_reserva/0","selectMotivosReserva")
        this.construyeSelectMotivos(datos.urlServicePy+"parametros/api_cat_causas_baja/0","selectMotivosBaja")

        this.setState({ mostrarReporteCapacitacion: false, mostrarConvenioArranque: false })
    }

    construyeSelectMotivos(url,selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                console.log("respuesta  ", json)
                for (var i = 0; i < filas.length; i++) {
                    let fila = filas[i]

                    options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                }
                let salida = []

                if (selector == "selectMotivosReserva") {
                    salida.push(<select onChange={this.onChange} name="selectMotivosReserva" style={{ borderColor: '#F1F3FA' }} class="form-control" aria-label="Default select example">{options}</select>)
                    this.setState({ selectMotivosReserva: salida })
                }else if(selector == "selectMotivosBaja"){
                    salida.push(<select onChange={this.onChange} name="selectMotivosBaja" style={{ borderColor: '#F1F3FA' }} class="form-control" aria-label="Default select example">{options}</select>)
                    this.setState({ selectMotivosBaja: salida })
                }
            })
    }


    render() {
        return (
            <div>
                <div id="modal-reserva" className="modal fade" tabindex="-1">
                    <div className="modal-dialog ">
                        <div className="modal-content text-white" style={{ backgroundColor: "#FFFFF" }}>
                            <div className="modal-header text-white text-center" style={{ backgroundColor: "#617187" }}>
                                <h6 className="modal-title col-12 text-center">Enviar a reserva</h6>
                            </div>
                            <div className="modal-body">
                                <div className="card-body">
                                    <h8 style={{ color: '#8F9EB3' }}>Si está seguro de enviar a reserva el registro, por favor seleccione el motivo</h8>
                                    {this.state.selectMotivosReserva}
                                    <br></br>
                                    <h8 style={{ color: '#8F9EB3' }}>Fecha acordada para retomar el proceso</h8>
                                    <input type="date" placeholder="Escribir" name="fecha_alta" className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />
                                </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-center">
                                <div className="col-12">
                                    <div className="row">
                                        <button type="button" style={{ width: '100%', backgroundColor: "#617187" }} class="btn text-white" onClick={this.peticionAReserva} data-dismiss="modal"  >Enviar a reserva</button>
                                    </div>
                                    <br></br>
                                    <div className="row">
                                        <button type="button" style={{ width: '100%', backgroundColor: '#8F9EB3' }} class="btn text-white" id="btnAgregar" data-dismiss="modal"  >Cancelar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="modal-baja" className="modal fade" tabindex="-1">
                    <div className="modal-dialog ">
                        <div className="modal-content text-white" style={{ backgroundColor: "#FFFFF" }}>
                            <div className="modal-header text-white text-center" style={{ backgroundColor: "#617187" }}>
                                <h6 className="modal-title col-12 text-center">Dar baja el registro</h6>

                            </div>
                            <div className="modal-body">
                                <div className="card-body">
                                    <h8 style={{ color: '#8F9EB3' }}>Si está seguro de descartar el registro, por favor seleccione el motivo</h8>
                                    {this.state.selectMotivosBaja}
                                </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-center">
                                <div className="col-12">
                                    <div className="row">
                                        <button type="button" style={{ width: '100%', backgroundColor: "#617187" }} class="btn text-white" onClick={this.peticionABaja} data-dismiss="modal" >Dar de baja registro</button>
                                    </div>
                                    <br></br>
                                    <div className="row">
                                        <button type="button" style={{ width: '100%', backgroundColor: '#8F9EB3' }} class="btn text-white" id="btnAgregar" data-dismiss="modal"  >Cancelar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {
                    this.state.mostrarReporteCapacitacion == true &&
                    <ReporteCapacitacion mostrarConectado={this.mostrarConectado} idProspecto={this.props.idProspecto} tipoUsuario={this.props.tipoUsuario} nombreUsuario={this.props.nombreUsuario} tipoPantalla={"conectado"} />
                }
                {
                    this.state.mostrarConvenioArranque == true &&
                    <ConvenioArranque mostrarConectado={this.mostrarConectado} idProspecto={this.props.idProspecto} tipoUsuario={this.props.tipoUsuario} nombreUsuario={this.props.nombreUsuario} tipoPantalla={"conectado"} />
                }
                {
                    this.state.mostrarReporteCapacitacion == false && this.state.mostrarConvenioArranque == false &&
                    <div>
                        <div>
                            <div class="card">
                                <div class="row mb-2 mt-2 ml-1">
                                    <div class="col-12 col-lg-9">
                                        <div class="row">
                                            <div class="col-6 col-lg-1 d-flex align-items-center pb-sm-1">
                                                <div class="mr-2">
                                                    <a href="#"
                                                        class="btn rounded-pill btn-icon btn-sm" style={{ backgroundColor: '#78CB5A' }}> <span
                                                            class="letter-icon text-white"><i className="icon-checkmark2"></i></span>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-6 col-lg-1 d-flex align-items-center pb-sm-1">
                                                <div class="mr-2">
                                                    <a href="#"
                                                        class="btn rounded-pill btn-icon btn-sm" style={{ backgroundColor: '#78CB5A' }}> <span
                                                            class="letter-icon text-white"><i className="icon-checkmark2"></i></span>
                                                    </a>
                                                </div>
                                            </div>


                                            <div class="col-6 col-lg-1 d-flex align-items-center pb-sm-1">
                                                <div class="mr-2">
                                                    <a href="#"
                                                        class="btn rounded-pill btn-icon btn-sm" style={{ backgroundColor: '#78CB5A' }}> <span
                                                            class="letter-icon text-white"><i className="icon-checkmark2"></i></span>
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="col-6 col-lg-2 d-flex align-items-center pb-sm-1">
                                                <div class="mr-5 ">
                                                    <a href="#" style={{ height: '15px', width: '15px', backgroundColor: '#41B3D1', borderColor: '#41B3D1' }}
                                                        class="btn rounded-pill btn-icon btn-sm "><span
                                                            class="letter-icon text-white">1</span>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-6 col-lg-1 d-flex align-items-center pb-sm-1">
                                                <div class="mr-2">
                                                    <a href="#"
                                                        class="btn  rounded-pill btn-icon btn-sm" style={{ borderColor: '#78CB5A' }}> <span style={{ color: '#78CB5A' }}
                                                            class="letter-icon">C</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-3">
                                        <div class="float-right mr-3">
                                            <button type="button" title="Guardar y Regresar" onClick={this.regresaryGuardar} class="btn rounded-pill mr-1" style={{ backgroundColor: '#8F9EB3', borderColor: '#8F9EB3' }}><span><i style={{ color: 'white' }} className="icon-backward2"></i></span></button>
                                            <button type="button" title="Registrar Conectado" disabled = {
                                                this.state.urlCapacitacion == "" || this.state.nombreArchivoCapacitacion == "" ||
                                                this.state.urlConvenioFirmado == "" || this.state.nombreArchivoConvenioFirmado == "" ||
                                                this.state.bancorreoaps && (this.state.prospectoConectado.correo_aps == null || this.state.prospectoConectado.correo_aps == "") ||
                                                this.state.prospectoConectado.fecha_fin_tarjetas == null || this.state.prospectoConectado.fecha_fin_tarjetas == "" ||
                                                this.state.prospectoConectado.fecha_alta_boletin == null || this.state.prospectoConectado.fecha_alta_boletin == "" ||
                                                this.state.prospectoConectado.fecha_plenarias_aps == null || this.state.prospectoConectado.fecha_plenarias_aps == "" ||
                                                this.state.prospectoConectado.fecha_alta_chats == null || this.state.prospectoConectado.fecha_alta_chats == "" ||
                                                this.state.prospectoConectado.alta_chats_noveles == null || this.state.prospectoConectado.alta_chats_noveles == 0 ||
                                                this.state.prospectoConectado.alta_agenda_aps == null || this.state.prospectoConectado.alta_agenda_aps == 0
                                            }
                                            onClick={(e) => this.guardarFormulario(true)} class="btn rounded-pill mr-1"
                                                style={{ backgroundColor: '#8F9EB3', borderColor: '#8F9EB3' }}><span><i style={{ color: 'white' }} className="icon-forward3"></i></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-header bg-transparent header-elements-sm-inline">
                            <h3 class="mb-0 font-weight-semibold" style={{ color: '#617187' }}>CONECTADO</h3>
                            <div class="header-elements">
                                <ul class="list-inline mb-0">
                                    <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Tipo de conexión:<strong >{this.state.tipoConexion}</strong></li>
                                    <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Prospecto: <strong >{this.state.conectado.nombre + ' ' + this.state.conectado.ape_paterno + ' ' + (this.state.conectado.ape_materno != null ? this.state.conectado.ape_materno : '')}</strong></li>
                                    <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Fecha de registro Conectado: <strong >{this.state.conectado.ts_alta_audit}</strong></li>
                                    <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Entrevistador: <strong >{this.state.entrevistador}</strong></li>
                                    <li class="list-inline-item">
                                        <h6 style={{ backgroundColor: "#41B3D1", color: '#FFFF', borderRadius: '8px' }}> <>&nbsp;&nbsp;</>CONECTADO<>&nbsp;&nbsp;</> </h6>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <br />
                        <div className="row">
                            <div className="col-xs-9 col-lg-9">
                                <>&nbsp;&nbsp;</>
                            </div>
                            <div className="col-xs-3 col-lg-3">
                                <div className="row">
                                    <div className="col-xs-4 col-lg-4">
                                        { this.state.provisional ?
                                        <button class="btn  mr-1" onClick={this.peticionADefinitivo} style={{ backgroundColor: "#617187", color: '#FFFF' }}> Iniciar conexión definitiva</button>
                                        : ''
                                        }
                                    </div>
                                    <div className="col-xs-4 col-lg-4">
                                        <button data-toggle="modal" data-target="#modal-reserva" class="btn  mr-1" style={{ backgroundColor: "#617187", color: '#FFFF' }}> Enviar a reserva</button>
                                    </div>
                                    <div className="col-xs-4 col-lg-4">
                                        <button data-toggle="modal" data-target="#modal-baja" class="btn  mr-1" style={{ backgroundColor: "#617187", color: '#FFFF' }}> Enviar a baja</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-2 col-lg-4">
                                <h7 class="mb-0 font-weight-semibold">Datos Personales </h7>
                            </div>
                        </div>
                        <div class="card">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-xs-2 col-lg-4">
                                        <h7 class="mb-0 font-weight-semibold">Registro conectado</h7>
                                    </div>
                                </div>
                                <div class="card">
                                    <div className="card-body">
                                        <div className="row">
                                            <div className="col-xs-6 col-lg-6">
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        <span className="input-group-prepend"> <span
                                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Clave de correo APS asignada al prospecto</span>
                                                        </span> <input type="text" className="form-control " name="correo_aps" disabled = {this.props.tipoUsuario != "ASRSIS" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospectoConectado.correo_aps} />
                                                    </div>
                                                    {
                                                        this.state.bancorreoaps && (this.state.prospectoConectado.correo_aps == null || this.state.prospectoConectado.correo_aps == "")?
                                                            <span className="" style={{ color: "red" }}>Clave de correo APS asignada al prospecto, es un campo requerido</span> : ''
                                                    }
                                                </div>
                                            </div>
                                            <div className="col-xs-6 col-lg-6">
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        <span className="input-group-prepend"> <span
                                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Finalización tarjetas de presentación</span>
                                                        </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " name="fecha_fin_tarjetas" max="2100-01-01" disabled = {this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospectoConectado.fecha_fin_tarjetas} />
                                                    </div>
                                                    {
                                                        this.state.prospectoConectado.fecha_fin_tarjetas == null || this.state.prospectoConectado.fecha_fin_tarjetas == "" ?
                                                            <span className="" style={{ color: "red" }}>Finalización tarjetas de presentación, es un campo requerido</span> : ''
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-xs-4 col-lg-4">
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        <span className="input-group-prepend"> <span
                                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Fin de alta en boletín</span>
                                                        </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " name="fecha_alta_boletin" max="2100-01-01" disabled = {this.props.tipoUsuario != "ASESJRCAP" && this.props.tipoUsuario != "COORCAP" && this.props.tipoUsuario != "GTECAP" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospectoConectado.fecha_alta_boletin} />
                                                    </div>
                                                    {
                                                        this.state.prospectoConectado.fecha_alta_boletin == null || this.state.prospectoConectado.fecha_alta_boletin == "" ?
                                                            <span className="" style={{ color: "red" }}>Fin de alta en boletín, es un campo requerido</span> : ''
                                                    }
                                                </div>
                                            </div>
                                            <div className="col-xs-4 col-lg-4">
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        <span className="input-group-prepend"> <span
                                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Fin a bienvenida a plenarias APS</span>
                                                        </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " name="fecha_plenarias_aps" max="2100-01-01" disabled = {this.props.tipoUsuario != "ASESJRCAP" && this.props.tipoUsuario != "COORCAP" && this.props.tipoUsuario != "GTECAP" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospectoConectado.fecha_plenarias_aps} />
                                                    </div>
                                                    {
                                                        this.state.prospectoConectado.fecha_plenarias_aps == null || this.state.prospectoConectado.fecha_plenarias_aps == "" ?
                                                            <span className="" style={{ color: "red" }}>Fin a bienvenida a plenarias APS, es un campo requerido</span> : ''
                                                    }
                                                </div>
                                            </div>
                                            <div className="col-xs-4 col-lg-4">
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        <span className="input-group-prepend"> <span
                                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Invitación a grupo de facebook compartamos APS</span>
                                                        </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " name="fecha_alta_chats" max="2100-01-01" disabled = {this.props.tipoUsuario != "ASESJRCAP" && this.props.tipoUsuario != "COORCAP" && this.props.tipoUsuario != "GTECAP" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} value={this.state.prospectoConectado.fecha_alta_chats} />
                                                    </div>
                                                    {
                                                        this.state.prospectoConectado.fecha_alta_chats == null || this.state.prospectoConectado.fecha_alta_chats == "" ?
                                                            <span className="" style={{ color: "red" }}>Invitación a grupo de facebook compartamos APS, es un campo requerido</span> : ''
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-xs-4 col-lg-4">
                                                <div className="custom-control custom-switch  mb-2" >
                                                    <span className="input-group-prepend"> <span
                                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Alta en chats de Agentes noveles</span>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="checkbox" class="custom-control-input" id="alta_chats_noveles" name="alta_chats_noveles" disabled = {this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "SASJR"&& this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospectoConectado.alta_chats_noveles}></input>
                                                            <label class="custom-control-label" for="alta_chats_noveles"></label>
                                                        </div></span>
                                                        {
                                                        this.state.prospectoConectado.alta_chats_noveles == null || this.state.prospectoConectado.alta_chats_noveles == 0 ?
                                                            <span className="" style={{ color: "red" }}>Alta en chats de Agentes noveles, es un campo requerido</span> : ''
                                                    }
                                                </div>
                                            </div>
                                            <div className="col-xs-4 col-lg-4">
                                                <div className="custom-control custom-switch  mb-2" >
                                                    <span className="input-group-prepend"> <span className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Alta en Agenda APS</span>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="checkbox" class="custom-control-input" id="alta_agenda_aps" name="alta_agenda_aps" disabled = {this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "SASJR"&& this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospectoConectado.alta_agenda_aps}></input>
                                                            <label class="custom-control-label" for="alta_agenda_aps"></label>
                                                        </div>
                                                    </span>
                                                    {
                                                        this.state.prospectoConectado.alta_agenda_aps == null || this.state.prospectoConectado.alta_agenda_aps == 0 ?
                                                            <span className="" style={{ color: "red" }}>Alta en Agenda APS, es un campo requerido</span> : ''
                                                    }
                                                </div>
                                            </div>
                                            <div className="col-xs-4 col-lg-4">
                                                <div className="custom-control custom-switch  mb-2" >
                                                    <span className="input-group-prepend"> <span className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Aviso masivo a todos los Gansos de agente nuevo</span>
                                                        <div className="col-xs-2 col-lg-2">
                                                            <input type="checkbox" class="custom-control-input" id="aviso_gansos" name="aviso_gansos" disabled = {this.props.tipoUsuario != "COORCAP" && this.props.tipoUsuario != "ASESJRCAP" && this.props.tipoUsuario != "GTECAP" && this.props.tipoUsuario != "SUPER"} onChange={this.onChange} checked={this.state.prospectoConectado.aviso_gansos}></input>
                                                            <label class="custom-control-label" for="aviso_gansos"></label>
                                                        </div>
                                                    </span>
                                                    {/*
                                                        this.state.prospectoConectado.alta_agenda_aps == null || this.state.prospectoConectado.alta_agenda_aps == 0 ?
                                                            <span className="" style={{ color: "red" }}>Alta en Agenda APS, es un campo requerido</span> : ''
                                                    */}
                                                </div>
                                            </div>
                                            <br></br><br></br>
                                            <div className="row">
                                                <div className="col-xs-4 col-lg-4">
                                                    <a href="#"><ScrollTo selector={'#convenio_arranque'} onClick={this.onChangeConvenio} style={
                                                            this.state.urlConvenioFirmado == "" || this.state.nombreArchivoConvenioFirmado == "" ?
                                                            { color: "red" } : { color: "#2196f3" }} >
                                                        <h6>Convenio de arranque</h6></ScrollTo></a>
                                                </div>
                                                <div className="col-xs-4 col-lg-4">
                                                    <a href="#"><ScrollTo selector={'#scroll_end'} onClick={this.onChangeCapacitacion} style={
                                                        this.state.urlCapacitacion == "" || this.state.nombreArchivoCapacitacion == "" ?
                                                            { color: "red" } : { color: "#2196f3" }} >
                                                        <h6>Registro de Capacitación</h6></ScrollTo></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="accordion" id="convenio_arranque">
                                <div id="headingconvenio_arranque">
                                    <h2 class="mb-0">
                                        <button class="btn " type="button" data-toggle="collapse" data-target="#collapsconvenio_arranque" aria-expanded="true" aria-controls="collapsconvenio_arranque" ref={(ref) => this.conectadoConvenio = ref}>
                                            <h6 class="mb-0 font-weight-semibold">Convenio de Arranque</h6>
                                        </button>
                                    </h2>
                                </div>
                                <div class="card">
                                    <div id="collapsconvenio_arranque" class="collapse " aria-labelledby="headingconvenio_arranque" data-parent="#convenio_arranque">
                                        <div class="card">
                                            <div className="card-body">
                                                <div className="row">
                                                    <div className="col-xs-2 col-lg-4">
                                                        <h7 class="mb-0 font-weight-semibold">Convenio de arranque</h7>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div className="card-body">
                                                        <div className="row">
                                                            <div className="col-xs-4 col-lg-4">
                                                                <h6 class="mb-0 font-weight-semibold">Convenio de arranque</h6>
                                                            </div>

                                                            <div className="col-xs-4 col-lg-4">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <form encType="multipart/form" style={{ display: 'none' }} >
                                                                            <input type="file" style={{ display: 'none' }} ref={(ref) => this.convenioFirmado = ref} onChange={this.onChangeFileConvenioFirmado.bind(this)}></input>
                                                                        </form>
                                                                        <input type="text" className="form-control " disabled={(this.props.tipoUsuario != "GTEDES" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER") &&
                                                                            this.state.archivoConvenioFirmado == undefined && this.state.urlConvenioFirmado.length == 0
                                                                        } placeholder="Convenio firmado" value={this.state.nombreArchivoConvenioFirmado} />
                                                                        <span className="input-group-prepend">

                                                                            {
                                                                                this.state.muestraDocConvenioFirmado == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoConvenioFirmado }}>
                                                                                    <a href={this.state.urlConvenioFirmado} target="_blank" rel="noopener noreferrer" >
                                                                                        <i className="fas fa-eye"
                                                                                            style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                                    </a>
                                                                                </button> : ''


                                                                            }
                                                                            <button type="button" class="btn text-white" disabled={(this.props.tipoUsuario != "GTEDES" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER") && this.state.archivoConvenioFirmado == undefined && this.state.urlConvenioFirmado.length == 0}
                                                                                onClick={this.onClickBotonArchivoConvenioFirmado} style={{ backgroundColor: this.state.colorBotonSubirArchivoConvenioFirmado }}>
                                                                                <h10 style={{ color: "black" }}>+</h10>
                                                                            </button>

                                                                            <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >Convenio firmado</span>
                                                                        </span>
                                                                    </div>
                                                                    {
                                                                        this.state.urlConvenioFirmado == "" || this.state.nombreArchivoConvenioFirmado == "" ?
                                                                            <span style={{ color: "red" }}>Convenio de arranque, es requerido</span> : ''
                                                                    }
                                                                </div>
                                                            </div>

                                                            <div className="col-xs-4 col-lg-4">
                                                                <div className="row">
                                                                    <div className="col-xs-2 col-lg-2">
                                                                        <>&nbsp;&nbsp;&nbsp;&nbsp;</>
                                                                    </div><div className="col-xs-2 col-lg-2">
                                                                        <>&nbsp;&nbsp;&nbsp;&nbsp;</>
                                                                    </div>
                                                                    <div className="col-xs-2 col-lg-2">
                                                                        {/*<button class="btn " style={{ borderColor: 'black' }} type="button" disabled = {this.props.tipoUsuario != "GTEDES"}><i className="icon-download"></i></button>*/}
                                                                        {
                                                                            this.state.muestraDocConvenioFirmado == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorSubirArchivoConvenioFirmado }}>
                                                                                <a href={this.state.urlConvenioFirmado} target="_blank" rel="noopener noreferrer" >
                                                                                    <i className="fas fa-eye"
                                                                                        style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                                </a>
                                                                            </button> : <a href="#">
                                                                                <i className="fas fa-eye"
                                                                                    style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Documento no existe" ></i>
                                                                            </a>
                                                                        }
                                                                    </div>
                                                                    <div className="col-xs-6 col-lg-6">
                                                                        <button class="btn " type="button" disabled={this.props.tipoUsuario != "GTEDES" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onClick={this.mostrarConvenioArranque} style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Generar documento</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion" id="expediente_agente">
                                <div id="headingexpediente_agente">
                                    <h2 class="mb-0">
                                        <button class="btn " type="button" data-toggle="collapse" data-target="#collapsexpediente_agente" aria-expanded="true" aria-controls="collapsexpediente_agente">
                                            <h6 class="mb-0 font-weight-semibold">Campos Post Conexión</h6>
                                        </button>
                                    </h2>
                                </div>
                                <div class="card">
                                    <div id="collapsexpediente_agente" class="collapse " aria-labelledby="headingexpediente_agente" data-parent="#expediente_agente">
                                        <div class="card">
                                            <div className="card-body">
                                                <div className="row">
                                                    <div className="col-xs-2 col-lg-4">
                                                        <h7 class="mb-0 font-weight-semibold">Campos Expediente Agente</h7>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div className="card-body">
                                                        <div className="row">
                                                            <div className="col-xs-4 col-lg-4">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <span className="input-group-prepend"> <span
                                                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Aniversario de bodas del agente</span>
                                                                        </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " name="fecha_aniversario" max="2100-01-01" onChange={this.onChangeAgente} value={this.state.prospectoAgente.fecha_aniversario} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-xs-6 col-lg-6">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <span className="input-group-prepend"> <span
                                                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre hijo 1 agente</span>
                                                                        </span> <input type="text" style={{ borderColor: '#E1E5F0' }} onChange={this.onChangeAgente} name="hijo_uno" className="form-control" value={this.state.prospectoAgente.hijo_uno} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xs-5 col-lg-5">
                                                                < div className="form-group">
                                                                    <div className="input-group">
                                                                        <span className="input-group-prepend"> <span
                                                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Fecha de nacimiento hijo 1 del agente</span>
                                                                        </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " onChange={this.onChangeAgente} max="2100-01-01" name="fecha_hijo_uno" value={this.state.prospectoAgente.fecha_hijo_uno} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        {
                                                            this.state.contadorHijos == 2 || this.state.contadorHijos > 2 ?
                                                                < div className="row">
                                                                    <div className="col-xs-6 col-lg-6">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre hijo 2 agente</span>
                                                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} onChange={this.onChangeAgente} name="hijo_dos" className="form-control " value={this.state.prospectoAgente.hijo_dos} />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-xs-5 col-lg-5">
                                                                        < div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Fecha de nacimiento hijo 2 del agente</span>
                                                                                </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " onChange={this.onChangeAgente} max="2100-01-01" name="fecha_hijo_dos" value={this.state.prospectoAgente.fecha_hijo_dos} />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div> :
                                                                ''
                                                        }
                                                        {
                                                            this.state.contadorHijos == 3 || this.state.contadorHijos > 3 ?
                                                                < div className="row">
                                                                    <div className="col-xs-6 col-lg-6">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre hijo 3 agente</span>
                                                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " onChange={this.onChangeAgente} name="hijo_tres" value={this.state.prospectoAgente.hijo_tres} />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-xs-5 col-lg-5">
                                                                        < div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Fecha de nacimiento hijo 3 del agente</span>
                                                                                </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " onChange={this.onChangeAgente} max="2100-01-01" name="fecha_hijo_tres" value={this.state.prospectoAgente.fecha_hijo_tres} />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div> :
                                                                ''
                                                        }
                                                        {
                                                            this.state.contadorHijos == 4 || this.state.contadorHijos > 4 ?
                                                                < div className="row">
                                                                    <div className="col-xs-6 col-lg-6">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre hijo 4 agente</span>
                                                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " onChange={this.onChangeAgente} name="hijo_cuatro" value={this.state.prospectoAgente.hijo_cuatro} />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-xs-5 col-lg-5">
                                                                        < div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Fecha de nacimiento hijo 4 del agente</span>
                                                                                </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " onChange={this.onChangeAgente} max="2100-01-01" name="fecha_hijo_cuatro" value={this.state.prospectoAgente.fecha_hijo_cuatro} />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div> :
                                                                ''
                                                        }
                                                        {
                                                            this.state.contadorHijos == 5 ?
                                                                < div className="row">
                                                                    <div className="col-xs-6 col-lg-6">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre hijo 5 agente</span>
                                                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " onChange={this.onChangeAgente} name="hijo_cinco" value={this.state.prospectoAgente.hijo_cinco} />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-xs-5 col-lg-5">
                                                                        < div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Fecha de nacimiento hijo 5 del agente</span>
                                                                                </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " onChange={this.onChangeAgente} max="2100-01-01" name="fecha_hijo_cinco" value={this.state.prospectoAgente.fecha_hijo_cinco} />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div> :
                                                                ''
                                                        }
                                                        <div className="row">
                                                            <div className="col-xs-3 col-lg-3">
                                                                <button type="button" class="btn text-white" onClick={() => this.inyectaHijos(true)} style={{ backgroundColor: "#0F69B8" }}>+ Agregar Hijo</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <button type="button" class="btn text-white" onClick={() => this.inyectaHijos(false)} style={{ backgroundColor: "#0F69B8" }}>- Eliminar Hijo</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card-body">
                                                <div className="row">
                                                    <div className="col-xs-2 col-lg-4">
                                                        <h7 class="mb-0 font-weight-semibold">Campos Expediente Agente</h7>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div className="card-body">
                                                        <div className="row">
                                                            <div className="col-xs-12 col-lg-12">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <span className="input-group-prepend"> <span
                                                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre asistente del agente</span>
                                                                        </span> <input type="text" style={{ borderColor: '#E1E5F0' }} onChange={this.onChangeAgente} name="asistente" className="form-control " value={this.state.prospectoAgente.asistente} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-xs-6 col-lg-6">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <span className="input-group-prepend"> <span
                                                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Teléfono asistente del agente</span>
                                                                        </span> <input type="number" style={{ borderColor: '#E1E5F0' }} className="form-control " maxLength={10} onChange={this.onChangeAgente} name="telefono_asistente" value={this.state.prospectoAgente.telefono_asistente} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xs-6 col-lg-6">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <span className="input-group-prepend"> <span
                                                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Correo asistente del agente</span>
                                                                        </span> <input type="text" style={{ borderColor: '#E1E5F0' }} onChange={this.onChangeAgente} name="correo_asistente" className="form-control " value={this.state.prospectoAgente.correo_asistente} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-xs-6 col-lg-6">
                                                                < div className="form-group">
                                                                    <div className="input-group">
                                                                        <span className="input-group-prepend"> <span
                                                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Fecha de nacimiento asistente del agente</span>
                                                                        </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " onChange={this.onChangeAgente} max="2100-01-01" name="fecha_nacimiento_asistente" value={this.state.prospectoAgente.fecha_nacimiento_asistente} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-xs-12 col-lg-12">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <span className="input-group-prepend"> <span
                                                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre contacto</span>
                                                                        </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " onChange={this.onChangeAgente} name="contacto" value={this.state.prospectoAgente.contacto} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-xs-6 col-lg-6">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <span className="input-group-prepend"> <span
                                                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Parentesco</span>
                                                                        </span>
                                                                        {this.state.selectParentesco}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xs-6 col-lg-6">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <span className="input-group-prepend"> <span
                                                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Teléfono de contacto</span>
                                                                        </span> <input type="number" style={{ borderColor: '#E1E5F0' }} className="form-control " maxLength={10} onChange={this.onChangeAgente} name="telefono_contacto" value={this.state.prospectoAgente.telefono_contacto} />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion" id="conexion_capacitacion">
                                <div id="headingconexion_capacitacion">
                                    <h2 class="mb-0">
                                        <button class="btn " type="button" data-toggle="collapse" data-target="#collapsconexion_capacitacion" aria-expanded="true" aria-controls="collapsconexion_capacitacion" ref={(ref) => this.conectadoCapacitacion = ref}>
                                            <h6 class="mb-0 font-weight-semibold">En Conexión / Capacitación </h6>
                                        </button>
                                    </h2>
                                </div>
                                <div class="card">
                                    <div id="collapsconexion_capacitacion" class="collapse " aria-labelledby="headingconexion_capacitacion" data-parent="#conexion_capacitacion">
                                        <div class="card">
                                            <div className="card-body">
                                                <div className="row">
                                                    <div className="col-xs-2 col-lg-4">
                                                        <h7 class="mb-0 font-weight-semibold">Juego de Conexión</h7>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div className="card-body">
                                                        <div className="row">
                                                            <div className="col-xs-4 col-lg-4">
                                                                <div className="row">
                                                                    <div className="col-xs-2 col-lg-4">
                                                                        <h8 class="mb-0 font-weight-semibold">Fotografía conexión</h8>
                                                                    </div>
                                                                    <div className="col-xs-2 col-lg-4">
                                                                        <div>
                                                                            <input type="image" disabled={true} src={this.state.imagenEnCambio == undefined ? avatar : this.state.imagenEnCambio}
                                                                                class="img-fluid rounded-circle" width="40" height="40" alt="" />
                                                                            <form encType="multipart/form">
                                                                                <input type="file" disabled={true} style={{ display: 'none' }} ref={(ref) => this.imagen = ref}></input>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xs-4 col-lg-4">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <form encType="multipart/form" style={{ display: 'none' }} >
                                                                            <input type="file" style={{ display: 'none' }} disabled={true}></input>
                                                                        </form>
                                                                        <input type="text" className="form-control " disabled={true} placeholder="Identificación Oficial" value={this.state.nombreArchivoIdentificacion} />
                                                                        <span className="input-group-prepend">
                                                                            {
                                                                                this.state.muestraDocIdentificacion == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoIdentificacion }}>
                                                                                    <a href={this.state.urlIdentificacion} target="_blank" rel="noopener noreferrer" >
                                                                                        <i className="fas fa-eye"
                                                                                            style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                                    </a>
                                                                                </button> : ''
                                                                            }
                                                                            <button type="button" class="btn text-white" disabled={true} style={{ backgroundColor: this.state.colorBotonSubirArchivoIdentificacion }}>
                                                                                <h10 style={{ color: "white" }}>+</h10>
                                                                            </button>
                                                                            <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >Identificación Oficial</span>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xs-12 col-lg-4">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <form encType="multipart/form" style={{ display: 'none' }} >
                                                                            <input type="file" style={{ display: 'none' }} disabled={true}></input>
                                                                        </form>
                                                                        <input type="text" className="form-control " disabled={true} placeholder="Acta de nacimiento" value={this.state.nombreArchivoActaNacimiento} />
                                                                        <span className="input-group-prepend">

                                                                            {
                                                                                this.state.muestraDocActaNacimiento == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoActaNacimiento }}>
                                                                                    <a href={this.state.urlActaNacimiento} target="_blank" rel="noopener noreferrer" >
                                                                                        <i className="fas fa-eye"
                                                                                            style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                                    </a>
                                                                                </button> : ''
                                                                            }
                                                                            <button type="button" class="btn text-white" disabled={true} style={{ backgroundColor: this.state.colorBotonSubirArchivoActaNacimiento }}>
                                                                                <h10 style={{ color: "white" }}>+</h10>
                                                                            </button>

                                                                            <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >Acta de nacimiento</span>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            {/* aqui esta el conyge */}
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-xs-12 col-lg-4">
                                                                <div className="form-group">
                                                                    <div className="input-group">

                                                                        <form encType="multipart/form" style={{ display: 'none' }} >
                                                                            <input type="file" style={{ display: 'none' }} ref={(ref) => this.certificado = ref}></input>
                                                                        </form>
                                                                        <input type="text" className="form-control " disabled={true} placeholder="Certificado de estudios" value={this.state.nombreArchivoCertificadoEstudios} />
                                                                        <span className="input-group-prepend">

                                                                            {
                                                                                this.state.muestraDocCertificadoEstudios == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoCertificadoEstudios }}>
                                                                                    <a href={this.state.urlCertificadoEstudios} target="_blank" rel="noopener noreferrer" >
                                                                                        <i className="fas fa-eye"
                                                                                            style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                                    </a>
                                                                                </button> : ''


                                                                            }
                                                                            <button type="button" class="btn text-white" disabled={true} style={{ backgroundColor: this.state.colorBotonSubirArchivoCertificadoEstudios }}>
                                                                                <h10 style={{ color: "white" }}>+</h10>
                                                                            </button>
                                                                            <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >Certificado de estudios</span>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xs-12 col-lg-4">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <form encType="multipart/form" style={{ display: 'none' }} >
                                                                            <input type="file" style={{ display: 'none' }} ref={(ref) => this.curp = ref}></input>
                                                                        </form>
                                                                        <input type="text" className="form-control " disabled={true} placeholder="CURP" value={this.state.nombreArchivoCurp} />
                                                                        <span className="input-group-prepend">

                                                                            {
                                                                                this.state.muestraDocCurp == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoCurp }}>
                                                                                    <a href={this.state.urlCurp} target="_blank" rel="noopener noreferrer" >
                                                                                        <i className="fas fa-eye"
                                                                                            style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                                    </a>
                                                                                </button> : ''


                                                                            }
                                                                            <button type="button" class="btn text-white" disabled={true} style={{ backgroundColor: this.state.colorBotonSubirArchivoCurp }}>
                                                                                <h10 style={{ color: "white" }}>+</h10>
                                                                            </button>

                                                                            <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >CURP</span>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xs-12 col-lg-4">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <form encType="multipart/form" style={{ display: 'none' }} >
                                                                            <input type="file" style={{ display: 'none' }} ref={(ref) => this.rfc = ref}></input>
                                                                        </form>
                                                                        <input type="text" className="form-control " disabled={true} placeholder="RFC" value={this.state.nombreArchivoRFC} />
                                                                        <span className="input-group-prepend">

                                                                            {
                                                                                this.state.muestraDocRFC == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoRFC }}>
                                                                                    <a href={this.state.urlRFC} target="_blank" rel="noopener noreferrer" >
                                                                                        <i className="fas fa-eye"
                                                                                            style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                                    </a>
                                                                                </button> : ''


                                                                            }
                                                                            <button type="button" class="btn text-white" disabled={true} style={{ backgroundColor: this.state.colorBotonSubirArchivoRFC }}>
                                                                                <h10 style={{ color: "white" }}>+</h10>
                                                                            </button>

                                                                            <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >RFC</span>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-xs-12 col-lg-4">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <form encType="multipart/form" style={{ display: 'none' }} >
                                                                            <input type="file" style={{ display: 'none' }} ref={(ref) => this.cedula = ref}></input>
                                                                        </form>
                                                                        <input type="text" className="form-control " disabled={true} placeholder="Cédula de agente" value={this.state.nombreArchivoCedula} />
                                                                        <span className="input-group-prepend">

                                                                            {
                                                                                this.state.muestraDocCedula == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoCedula }}>
                                                                                    <a href={this.state.urlCedula} target="_blank" rel="noopener noreferrer" >
                                                                                        <i className="fas fa-eye"
                                                                                            style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                                    </a>
                                                                                </button> : ''


                                                                            }
                                                                            <button type="button" class="btn text-white" disabled={true} style={{ backgroundColor: this.state.colorBotonSubirArchivoCedula }}>
                                                                                <h10 style={{ color: "white" }}>+</h10>
                                                                            </button>

                                                                            <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >Cédula de agente</span>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="col-xs-12 col-lg-4">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <form encType="multipart/form" style={{ display: 'none' }} >
                                                                            <input type="file" style={{ display: 'none' }} ref={(ref) => this.renuncia = ref}></input>
                                                                        </form>
                                                                        <input type="text" className="form-control " disabled={true} placeholder="Carta de renuncia" value={this.state.nombreArchivoRenuncia} />
                                                                        <span className="input-group-prepend">
                                                                            {
                                                                                this.state.muestraDocRenuncia == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoRenuncia }}>
                                                                                    <a href={this.state.urlRenuncia} target="_blank" rel="noopener noreferrer" >
                                                                                        <i className="fas fa-eye"
                                                                                            style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                                    </a>
                                                                                </button> : ''


                                                                            }
                                                                            <button type="button" class="btn text-white" disabled={true} style={{ backgroundColor: this.state.colorBotonSubirArchivoRenuncia }}>
                                                                                <h10 style={{ color: "white" }}>+</h10>
                                                                            </button>
                                                                            <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >Carta de renuncia</span>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {this.state.provisional == true &&
                                                    <div id="datos_Conexion" name="datos_Conexion">
                                                        <div class="card">
                                                            <div class="card-header  header-elements-sm-inline">
                                                                <h5 class="mb-0 font-weight-semibold" >Datos Conexión</h5>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-xs-2 col-lg-4">
                                                                <h7 class="mb-0 font-weight-semibold">Servicio al socio</h7>
                                                            </div>
                                                        </div>
                                                        <div class="card">
                                                            <div className="card-body">
                                                                <div className="row">
                                                                    <div className="col-xs-4 col-lg-4">
                                                                        <div className="row">
                                                                            <div className="col-xs-8 col-lg-8">
                                                                                <h8 class="mb-0 font-weight-semibold">Finalizó Curso IDEAS</h8>
                                                                            </div>
                                                                            <div className="col-xs-2 col-lg-4">
                                                                                <div className="custom-control custom-switch  mb-2" >
                                                                                    <input type="checkbox" class="custom-control-input" id="curso_ideas" name="curso_ideas" disabled={true} checked={this.state.prospectoConectado.curso_ideas}></input>
                                                                                    <label class="custom-control-label" for="curso_ideas"></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-xs-4 col-lg-4">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Clave única de agente CUA definitivo</span>
                                                                                </span> <input type="number" placeholder="" name="clave_cua" disabled={true} value={this.state.prospectoConectado.clave_cua} className="form-control " />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-xs-4 col-lg-4">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Clave única de agente CUA provisional</span>
                                                                                </span> <input type="number" placeholder="" name="clave_cua_provisional" disabled={true} value={this.state.prospectoConectado.clave_cua_provisional} className="form-control " />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="row">
                                                                    <div className="col-xs-4 col-lg-4">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Conexión GNP agente provisional</span>
                                                                                </span> <input type="date" placeholder="" name="fecha_conexion_provicional" disabled={true} value={this.state.prospectoConectado.fecha_conexion_provicional} className="form-control " />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-xs-4 col-lg-4">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Antigüedad del agente</span>
                                                                                </span> <input type="number" placeholder="" name="antiguedad_agente" disabled={true} value={this.state.prospectoConectado.antiguedad_agente} className="form-control " />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="row">
                                                                    <div className="col-xs-4 col-lg-4">
                                                                        <div className="row">
                                                                            {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>} <label >Generación de la figura comercial</label>
                                                                        </div>
                                                                        <div className="row">
                                                                            {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                            <div class="form-check">
                                                                                <input class="form-check-input" type="checkbox" value={1} disabled={true} id="G1" name="generacion" checked={this.state.prospectoConectado.generacion == 1} />
                                                                                <label class="form-check-label" for="G1">
                                                                                    G1
                                                                                </label>
                                                                            </div>
                                                                            {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                            <div class="form-check">
                                                                                <input class="form-check-input" type="checkbox" value={2} disabled={true} id="G2" name="generacion" checked={this.state.prospectoConectado.generacion == 2} />
                                                                                <label class="form-check-label" for="G2">
                                                                                    G2
                                                                                </label>
                                                                            </div>

                                                                            {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                            <div class="form-check">
                                                                                <input class="form-check-input" type="checkbox" value={3} disabled={true} id="G3" name="generacion" checked={this.state.prospectoConectado.generacion == 3} />
                                                                                <label class="form-check-label" for="G3">
                                                                                    G3
                                                                                </label>
                                                                            </div>
                                                                            {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                            <div class="form-check">
                                                                                <input class="form-check-input" type="checkbox" value={4} disabled={true} id="G4" name="generacion" checked={this.state.prospectoConectado.generacion == 4} />
                                                                                <label class="form-check-label" for="G4">
                                                                                    G4
                                                                                </label>
                                                                            </div>
                                                                            {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                            <div class="form-check">
                                                                                <input class="form-check-input" type="checkbox" value={5} disabled={true} id="C" name="generacion" checked={this.state.prospectoConectado.generacion == 5} />
                                                                                <label class="form-check-label" for="C">
                                                                                    C
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div className="col-xs-8 col-lg-8">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Manejo y consideraciones de Agentes Consolidados</span>
                                                                                </span> {this.state.selectAgentesConsolidados}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                }
                                                {this.state.provisional == true &&
                                                    <div>
                                                        <div className="row">
                                                            <div className="col-xs-2 col-lg-4">
                                                                <h7 class="mb-0 font-weight-semibold">Capacitación</h7>
                                                            </div>
                                                        </div>
                                                        <div class="card">
                                                            <div className="card-body">
                                                                <div className="row">
                                                                    <div className="col-xs-2 col-lg-2">
                                                                        <h8 class="mb-0 font-weight-semibold">Alta del prospecto del Curso IDEAS</h8>
                                                                    </div>
                                                                    <div className="col-xs-2 col-lg-4">
                                                                        <div className="custom-control custom-switch  mb-2" >
                                                                            <input type="checkbox" class="custom-control-input" id="alta_prospecto_ideas" name="alta_prospecto_ideas" disabled={true} checked={this.state.prospectoConectado.alta_prospecto_ideas}></input>
                                                                            <label class="custom-control-label" for="alta_prospecto_ideas"></label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>}

                                                {this.state.definitivo == true &&
                                                    <div>
                                                        <div class="card">
                                                            <div class="card-header  header-elements-sm-inline">
                                                                <h5 class="mb-0 font-weight-semibold" >Datos Conexión</h5>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-xs-2 col-lg-4">
                                                                <h7 class="mb-0 font-weight-semibold">Servicio al socio</h7>
                                                            </div>
                                                        </div>
                                                        <div class="card">
                                                            <div className="card-body">
                                                                <div className="row">
                                                                    <div className="col-xs-8 col-lg-8">
                                                                        <div className="row">
                                                                            <div className="col-xs-8 col-lg-8">
                                                                                <h8 class="mb-0 font-weight-semibold">Se envió correo electrónico solicitando pago de cedula y gestión de certificación al prospecto</h8>
                                                                            </div>
                                                                            <div className="col-xs-2 col-lg-4">
                                                                                <div className="custom-control custom-switch  mb-2" >
                                                                                    <input type="checkbox" class="custom-control-input" id="envio_correo" name="envio_correo" disabled={true} checked={this.state.prospectoConectado.envio_correo}></input>
                                                                                    <label class="custom-control-label" for="envio_correo"></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <br></br>
                                                                <div className="row">
                                                                    <div className="col-xs-8 col-lg-8">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <form encType="multipart/form" style={{ display: 'none' }} >
                                                                                    <input type="file" style={{ display: 'none' }} ref={(ref) => this.pagocei = ref} disabled={true}></input>
                                                                                </form>
                                                                                <input type="text" className="form-control " placeholder="Comprobante de pago del examen en el CEI" value={this.state.nombreArchivoComprobantePagoCEI} />
                                                                                <span className="input-group-prepend">

                                                                                    {
                                                                                        this.state.muestraDocComprobantePagoCEI == true ? <button type="button" class="btn text-white" disabled={this.props.tipoUsuario != "SASSR"} style={{ backgroundColor: this.state.colorBotonSubirArchivoComprobantePagoCEI }}>
                                                                                            <a href={this.state.urlComprobantePagoCEI} target="_blank" rel="noopener noreferrer" >
                                                                                                <i className="fas fa-eye"
                                                                                                    style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                                            </a>
                                                                                        </button> : ''


                                                                                    }
                                                                                    <button type="button" class="btn text-white" disabled={true} style={{ backgroundColor: this.state.colorBotonSubirArchivoComprobantePagoCEI }}>
                                                                                        <h10 style={{ color: "white" }}>+</h10>
                                                                                    </button>

                                                                                    <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >Comprobante de pago del examen en el CEI</span>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-xs-4 col-lg-4">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Examen en el CEI</span>
                                                                                </span> <input type="date" placeholder="" name="fecha_examen_cei" disabled={true} onChange={this.onChange} value={this.state.prospectoConectado.fecha_examen_cei} className="form-control " />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <br></br>
                                                                <div className="row">
                                                                    <div className="col-xs-4 col-lg-4">
                                                                        <div className="row">
                                                                            <div className="col-xs-8 col-lg-8">
                                                                                <h8 class="mb-0 font-weight-semibold">Aprobación del examen en el CEI</h8>
                                                                            </div>
                                                                            <div className="col-xs-2 col-lg-4">
                                                                                <div className="custom-control custom-switch  mb-2" >
                                                                                    <input type="checkbox" class="custom-control-input" id="aprobacion_examen_cei" name="aprobacion_examen_cei" disabled={true} checked={this.state.prospectoConectado.aprobacion_examen_cei}></input>
                                                                                    <label class="custom-control-label" for="aprobacion_examen_cei"></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-xs-8 col-lg-8">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <form encType="multipart/form" style={{ display: 'none' }} >
                                                                                    <input type="file" style={{ display: 'none' }} ref={(ref) => this.aprobacioncei = ref}></input>
                                                                                </form>
                                                                                <input type="text" className="form-control " disabled={true} placeholder="Evidencia de aprobación del examen en el CEI" value={this.state.nombreArchivoAprobacionExamenCEI} />
                                                                                <span className="input-group-prepend">

                                                                                    {
                                                                                        this.state.muestraDocAprobacionExamenCEI == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoAprobacionExamenCEI }}>
                                                                                            <a href={this.state.urlAprobacionExamenCEI} target="_blank" rel="noopener noreferrer" >
                                                                                                <i className="fas fa-eye"
                                                                                                    style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                                            </a>
                                                                                        </button> : ''


                                                                                    }
                                                                                    <button type="button" class="btn text-white" disabled={true} style={{ backgroundColor: this.state.colorBotonSubirArchivoAprobacionExamenCEI }}>
                                                                                        <h10 style={{ color: "white" }}>+</h10>
                                                                                    </button>
                                                                                    <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >Evidencia de aprobación del examen en el CEI</span>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="row">
                                                                    <div className="col-xs-4 col-lg-4">
                                                                        <div className="row">
                                                                            <div className="col-xs-8 col-lg-8">
                                                                                <h8 class="mb-0 font-weight-semibold">Alta en SAT</h8>
                                                                            </div>

                                                                            <div className="col-xs-2 col-lg-4">
                                                                                <div className="custom-control custom-switch  mb-2" >
                                                                                    <input type="checkbox" class="custom-control-input" id="alta_sat" name="alta_sat" disabled={true} checked={this.state.prospectoConectado.alta_sat}></input>
                                                                                    <label class="custom-control-label" for="alta_sat"></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-xs-4 col-lg-4">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Clave única de agente CUA definitivo</span>
                                                                                </span> <input type="number" placeholder="" name="clave_cua" disabled={true} value={this.state.prospectoConectado.clave_cua} className="form-control " />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-xs-4 col-lg-4">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Clave única de agente CUA provisional</span>
                                                                                </span> <input type="number" placeholder="" name="clave_cua_provisional" disabled={true} value={this.state.prospectoConectado.clave_cua_provisional} className="form-control " />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="row">
                                                                    <div className="col-xs-4 col-lg-4">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Entrega de cédula en CNCF</span>
                                                                                </span> <input type="date" placeholder="" name="fecha_entrega_cedula" disabled={true} value={this.state.prospectoConectado.fecha_entrega_cedula} className="form-control " />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-xs-4 col-lg-4">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Alta en CNSF</span>
                                                                                </span> <input type="date" placeholder="" name="fecha_alta_cnsf" disabled={true} value={this.state.prospectoConectado.fecha_alta_cnsf} className="form-control " />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="row">
                                                                    <div className="col-xs-4 col-lg-4">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fecha inicio vigencia de cédula</span>
                                                                                </span> <input type="date" placeholder="" name="fecha_ini_cedula" disabled={true} value={this.state.prospectoConectado.fecha_ini_cedula} className="form-control " />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-xs-4 col-lg-4">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fecha fin vigencia de cédula</span>
                                                                                </span> <input type="date" placeholder="" name="fecha_fin_cedula" disabled={true} value={this.state.prospectoConectado.fecha_fin_cedula} className="form-control " />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="row">
                                                                    <div className="col-xs-4 col-lg-4">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Conexión GNP agente definitivo</span>
                                                                                </span> <input type="date" placeholder="" name="fecha_conexion_definitiva" disabled={true} value={this.state.prospectoConectado.fecha_conexion_definitiva} className="form-control " />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-xs-4 col-lg-4">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Antigüedad del agente</span>
                                                                                </span> <input type="number" placeholder="" name="antiguedad_agente" disabled={true} value={this.state.prospectoConectado.antiguedad_agente} className="form-control " />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="row">
                                                                    <div className="col-xs-4 col-lg-4">
                                                                        <div className="row">
                                                                            {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>} <label >Generación de la figura comercial</label>
                                                                        </div>
                                                                        <div className="row">
                                                                            {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                            <div class="form-check">
                                                                                <input class="form-check-input" type="checkbox" value={1} disabled={true} id="G1" name="generacion" checked={this.state.prospectoConectado.generacion == 1} />
                                                                                <label class="form-check-label" for="G1">
                                                                                    G1
                                                                                </label>
                                                                            </div>
                                                                            {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                            <div class="form-check">
                                                                                <input class="form-check-input" type="checkbox" value={2} disabled={true} id="G2" name="generacion" checked={this.state.prospectoConectado.generacion == 2} />
                                                                                <label class="form-check-label" for="G2">
                                                                                    G2
                                                                                </label>
                                                                            </div>

                                                                            {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                            <div class="form-check">
                                                                                <input class="form-check-input" type="checkbox" value={3} disabled={true} id="G3" name="generacion" checked={this.state.prospectoConectado.generacion == 3} />
                                                                                <label class="form-check-label" for="G3">
                                                                                    G3
                                                                                </label>
                                                                            </div>
                                                                            {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                            <div class="form-check">
                                                                                <input class="form-check-input" type="checkbox" value={4} disabled={true} id="G4" name="generacion" checked={this.state.prospectoConectado.generacion == 4} />
                                                                                <label class="form-check-label" for="G4">
                                                                                    G4
                                                                                </label>
                                                                            </div>
                                                                            {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                                                            <div class="form-check">
                                                                                <input class="form-check-input" type="checkbox" value={5} disabled={true} id="C" name="generacion" checked={this.state.prospectoConectado.generacion == 5} />
                                                                                <label class="form-check-label" for="C">
                                                                                    C
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div className="col-xs-8 col-lg-8">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Manejo y consideraciones de Agentes Consolidados</span>
                                                                                </span> {this.state.selectAgentesConsolidados}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                }
                                                {this.state.definitivo == true &&
                                                    <div>
                                                        <div className="row">
                                                            <div className="col-xs-2 col-lg-4">
                                                                <h7 class="mb-0 font-weight-semibold">Capacitación</h7>
                                                            </div>
                                                        </div>
                                                        <div class="card">
                                                            <div className="card-body">
                                                                <div className="row">
                                                                    <div className="col-xs-4 col-lg-4">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Curso cédula A</span>
                                                                                </span> <input type="date" placeholder="" disabled={true} name="fecha_curso_cedula" onChange={this.onChange} value={this.state.prospectoConectado.fecha_curso_cedula} className="form-control " />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div className="col-xs-4 col-lg-4">
                                                                        <div className="row">
                                                                            <div className="col-xs-8 col-lg-8">
                                                                                <h8 class="mb-0 font-weight-semibold">Calificaciones y comprobante de pago de cédula</h8>
                                                                            </div>

                                                                            <div className="col-xs-2 col-lg-4">
                                                                                <div className="custom-control custom-switch  mb-2" >
                                                                                    <input type="checkbox" class="custom-control-input" id="comprobante_cedula" name="comprobante_cedula" disabled={true} onChange={this.onChange} checked={this.state.prospectoConectado.comprobante_cedula}></input>
                                                                                    <label class="custom-control-label" for="comprobante_cedula"></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="row">
                                                                    <div className="col-xs-4 col-lg-4">
                                                                        <div className="row">
                                                                            <div className="col-xs-8 col-lg-8">
                                                                                <h8 class="mb-0 font-weight-semibold">Envío GNP multa</h8>
                                                                            </div>
                                                                            <div className="col-xs-2 col-lg-4">
                                                                                <div className="custom-control custom-switch  mb-2" >
                                                                                    <input type="checkbox" class="custom-control-input" id="envio_gnp_multa" name="envio_gnp_multa" disabled={true} onChange={this.onChange} checked={this.state.prospectoConectado.envio_gnp_multa}></input>
                                                                                    <label class="custom-control-label" for="envio_gnp_multa"></label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                }
                                                <div className="row">
                                                    <div className="col-xs-2 col-lg-4">
                                                        <h7 class="mb-0 font-weight-semibold">Datos Capacitación</h7>
                                                    </div>
                                                </div>
                                                <div class="card" id="datos_Capacitacion" name="datos_Capacitacion">
                                                    <div className="card-body">
                                                        <div className="row">
                                                            <div className="col-xs-8 col-lg-8">
                                                                <div className="row">
                                                                    <div className="col-xs-8 col-lg-8">
                                                                        <h8 class="mb-0 font-weight-semibold">Se envió correo electrónico con detalles para el Curso de Inmersión APS al prospecto</h8>
                                                                    </div>

                                                                    <div className="col-xs-2 col-lg-4">
                                                                        <div className="custom-control custom-switch  mb-2" >
                                                                            <input type="checkbox" class="custom-control-input" id="curso_inmersion_aps" name="curso_inmersion_aps" disabled={true} onChange={this.onChange} checked={this.state.prospectoConectado.curso_inmersion_aps}></input>
                                                                            <label class="custom-control-label" for="curso_inmersion_aps"></label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-xs-2 col-lg-2">
                                                                <h6 class="mb-0 font-weight-semibold">Baja</h6>
                                                            </div>
                                                            <div className="col-xs-4 col-lg-4">
                                                                <div className="custom-control custom-switch  mb-2" >
                                                                    <input type="checkbox" class="custom-control-input" id="baja_conexion" name="baja_conexion" disabled={true} onChange={this.onChange} checked={this.state.prospectoConectado.baja_conexion}></input>
                                                                    <label class="custom-control-label" for="baja_conexion"></label>
                                                                </div>
                                                            </div>
                                                            {
                                                                this.state.ban_baja_conexion ?
                                                                    <div className="col-xs-6 col-lg-6">
                                                                        <div className="form-group">
                                                                            <div className="input-group">
                                                                                <span className="input-group-prepend"> <span
                                                                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Si es el caso, especificar el motivo de baja del Curso de Inmersión APS</span>
                                                                                </span>{this.state.selectMotivosBaja}
                                                                            </div>
                                                                        </div>
                                                                    </div> : ''
                                                            }
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-xs-4 col-lg-4">
                                                                <h6 class="mb-0 font-weight-semibold">Reporte de Capacitación</h6>
                                                            </div>
                                                            <div className="col-xs-4 col-lg-4">
                                                                <div className="form-group">
                                                                    <div className="input-group">
                                                                        <form encType="multipart/form" style={{ display: 'none' }} >
                                                                            <input type="file" style={{ display: 'none' }} ref={(ref) => this.capacitacion = ref} onChange={this.onChangeFileCapacitacion.bind(this)}></input>
                                                                        </form>
                                                                        <input type="text" className="form-control " disabled={this.props.tipoUsuario != "GTEDES"  && this.props.tipoUsuario != "ASESJRCAP" && this.props.tipoUsuario != "COORCAP" && this.props.tipoUsuario != "GTECAP" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} placeholder="Capacitación" value={this.state.nombreArchivoCapacitacion} />
                                                                        <span className="input-group-prepend">

                                                                            {
                                                                                this.state.muestraDocCapacitacion == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivoCapacitacion }}>
                                                                                    <a href={this.state.urlCapacitacion} target="_blank" rel="noopener noreferrer" >
                                                                                        <i className="fas fa-eye"
                                                                                            style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                                                    </a>
                                                                                </button> : ''


                                                                            }
                                                                            <button type="button" class="btn text-black" disabled={this.props.tipoUsuario != "GTEDES"  && this.props.tipoUsuario != "ASESJRCAP" && this.props.tipoUsuario != "COORCAP" && this.props.tipoUsuario != "GTECAP" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onClick={this.onClickBotonArchivoCapacitacion} style={{ backgroundColor: this.state.colorBotonSubirArchivoCapacitacion }}>
                                                                                <h10 style={{ color: "black" }}>+</h10>
                                                                            </button>

                                                                            <span className="input-group-text" style={{ backgroundColor: '#D5D9E8', color: '#617187' }} >Capacitación</span>
                                                                        </span>
                                                                    </div>
                                                                    {
                                                                        this.state.urlCapacitacion == "" || this.state.nombreArchivoCapacitacion == "" ?
                                                                            <span style={{ color: "red" }}>Convenio firmado, es requerido</span> : ''
                                                                    }
                                                                </div>
                                                            </div>
                                                            <div className="col-xs-4 col-lg-4">
                                                                <div className="row">
                                                                    <div className="col-xs-2 col-lg-2">
                                                                        <>&nbsp;&nbsp;&nbsp;&nbsp;</>
                                                                    </div><div className="col-xs-2 col-lg-2">
                                                                        <>&nbsp;&nbsp;&nbsp;&nbsp;</>
                                                                    </div>
                                                                    <div className="col-xs-2 col-lg-2">
                                                                        {/*<button class="btn " style={{ borderColor: 'black' }} type="button" disabled = {this.props.tipoUsuario != "COORCAP" && this.props.tipoUsuario != "SASSR" && this.props.tipoUsuario != "SASJR"&& this.props.tipoUsuario != "GTEDES"} ><i className="icon-download"></i></button>*/}
                                                                        {
                                                                            this.state.muestraDocCapacitacion == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorSubirArchivoCapacitacion }}>
                                                                                <a href={this.state.urlCapacitacion} target="_blank" rel="noopener noreferrer" >
                                                                                    <i className="fas fa-eye"
                                                                                        style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Consultar Reporte Capacitación" ></i>
                                                                                </a>
                                                                            </button> : <a href="#">
                                                                                <i className="fas fa-eye"
                                                                                    style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Documento no existe" ></i>
                                                                            </a>
                                                                        }
                                                                    </div>
                                                                    <div className="col-xs-6 col-lg-6">
                                                                        <button class="btn " type="button" style={{ backgroundColor: '#D5D9E8', color: '#617187' }}
                                                                            disabled={this.props.tipoUsuario != "GTEDES" && this.props.tipoUsuario != "ASESJRCAP" && this.props.tipoUsuario != "COORCAP" && this.props.tipoUsuario != "GTECAP" && this.props.tipoUsuario != "SASJR" && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} onClick={this.mostrarReporteCapacitacion}
                                                                        >Generar documento</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="scroll_end" name="scroll_end"></div>
                        <HistoricoGeneral idProspecto={this.props.idProspecto} tipoUsuario={this.props.tipoUsuario} presentarSeccion={"EP12"} />
                    </div>
                }
            </div>
        )
    }
}