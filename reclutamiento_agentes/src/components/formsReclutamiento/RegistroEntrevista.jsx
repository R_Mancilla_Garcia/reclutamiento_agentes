import React, { Component } from "react";
import datos from '../urls/datos.json'

export default class RegistroEntrevista extends Component {

    constructor() {
        super();
        this.onChange = this.onChange.bind(this)
        this.cambiaEstatusDelProspecto = this.cambiaEstatusDelProspecto.bind(this)
        this.cambiaDescartarProspecto = this.cambiaDescartarProspecto.bind(this)
        this.construyeSelectMotivos = this.construyeSelectMotivos.bind(this)
        this.construyeSelect = this.construyeSelect.bind(this)
        this.state = {
            fec_entrevista_profunda: '',
            selectMotivos: [],
            idMotivoBaja: 0,


            selectEDAT: [],
            selectFuenteReclutamiento: [],
            selectFuenteReclutamientoBono: [],
            selectAgenteReferidor: [],
            selectEstadoCivil: [],
            referido: {},
            colorSubirArchivoCV: "#E1E5F0",
            colorTextoSubirArchivoCV: "#617187",
            colorBotonSubirArchivoCV: "#0F69B8",
            nombreArchivoCV: "",
            archivoCV: undefined,
            muestraDocCV: false,

        };
    }

    onChange = e => {
        if (e.target.name == 'selectMotivos') {
            this.setState({ idMotivoBaja: parseInt(e.target.value) })
        } else {
            this.setState({ [e.target.name]: e.target.value })

        }
    }

    cambiaEstatusDelProspecto() {
        let jsonActualiza = {
            "estatus_id": 4,
            "fec_entrevista_profunda": this.state.fec_entrevista_profunda
        }
        const requestOptions = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(jsonActualiza)
        };
        fetch(datos.urlServicePy+"parametros_upd/api_recluta_prospectos/" + this.props.idProspecto, requestOptions)
            .then(response => response.json())
            .then(data => {
                this.props.botonCancelar()
            })


    }

    cambiaDescartarProspecto() {
        let jsonActualiza = {
            "estatus_id": 2,
            "causas_baja_id": this.state.idMotivoBaja

        }
        const requestOptions = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(jsonActualiza)
        };
        fetch(datos.urlServicePy+"parametros_upd/api_recluta_prospectos/" + this.props.idProspecto, requestOptions)
            .then(response => response.json())
            .then(data => {
                this.props.botonCancelar()
            })


    }


    construyeSelect(url, selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                for (var i = 0; i < filas.length; i++) {
                    let fila = filas[i]
                    if (selector == "selectEDAT") {
                            if (this.state.referido.empleado_id == fila.fila[0].value) {
                                options.push(<option selected value={fila.fila[0].value}>{fila.fila[2].value + ' ' + fila.fila[3].value + ' ' + fila.fila[4].value}</option>)
                            } else {
                                options.push(<option value={fila.fila[0].value}>{fila.fila[2].value + ' ' + fila.fila[3].value + ' ' + fila.fila[4].value}</option>)
                            }
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[2].value + ' ' + fila.fila[3].value + ' ' + fila.fila[4].value}</option>)
                    }

                    if (selector == "selectEstadoCivil") {
                            if (this.state.referido.estadocivil_id == fila.fila[0].value) {
                                options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            } else {
                                options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            }
                    }

                    if (selector == "selectFuenteReclutamientoBono") {
                            if (this.state.referido.fuente_recluta_bonos_id == fila.fila[0].value) {
                                options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            } else {
                                options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            }
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                    }

                    if (selector == "selectFuenteReclutamiento") {
                            if (this.state.referido.fuente_reclutamiento_id == fila.fila[0].value) {
                                options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            } else {
                                options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            }
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }
                }
                let salida = []
                salida.push(<select disabled onChange={this.onChange} name={selector} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ [selector]: salida /*,selectParentesco2: salida,selectParentesco3: salida*/ })
            });
    }

    construyeSelectAgenteReferidor(url, selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                for (var i = 0; i < filas.length; i++) {
                    let fila = filas[i]
                    if (selector == "selectAgenteReferidor") {
                        console.log("referido aqui" + this.state.referido.agente_referidor);
                        if (this.state.referido.agente_referidor == fila.id) {
                            options.push(<option selected value={fila.id + "," + fila.agente_referidor + "," + fila.cua_referidor}>{fila.agente_referidor}</option>)
                        } else {
                            options.push(<option value={fila.id + "," + fila.agente_referidor + "," + fila.cua_referidor}>{fila.agente_referidor}</option>)
                        }
                    }

                }
                let salida = []
                salida.push(<select onChange={this.onChange} disabled = {true} name={selector} style={{ borderColor: '#E1E5F0' }} className="form-control " aria-label="Default select example">{options}</select>)
                this.setState({ selectAgenteReferidor: salida })
            })
    }


    UNSAFE_componentWillMount() {
        this.construyeSelectMotivos(datos.urlServicePy+"parametros/api_cat_causas_baja_prospecto/0")
        fetch(datos.urlServicePy+"parametros/api_recluta_prospectos/" + this.props.idProspecto)
            .then(response => response.json())
            .then(json => {
                let fila = json.filas[0].fila
                let columnas = json.columnas
                let columnasSalida = {}
                for (var i = 0; i < fila.length; i++) {
                    console.log(fila[i])
                    columnasSalida["" + columnas[i].key + ""] = fila[i].value
                }

                this.setState({ referido: columnasSalida })
                if (columnasSalida.doc_cv != null && columnasSalida.doc_cv.length > 0) {
                    this.setState({ colorSubirArchivoCV: "#78CB5A", colorTextoSubirArchivoCV: "#FFFFFF", colorBotonSubirArchivoCV: "#E5F6E0", nombreArchivoCV: "Reemplazar...", muestraDocCV: true })
                }
                this.construyeSelect(datos.urlServicePy+"parametros/api_cat_empleados/0", "selectEDAT")
                this.construyeSelect(datos.urlServicePy+"parametros/api_cat_fuente_reclutamiento/0", "selectFuenteReclutamiento")
                this.construyeSelect(datos.urlServicePy+"parametros/api_cat_fuente_reclutamiento_bonos/0", "selectFuenteReclutamientoBono")
                this.construyeSelect(datos.urlServicePy+"parametros/api_cat_estadocivil/0", "selectEstadoCivil")
            })
    }


    construyeSelectMotivos(url) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                console.log("respuesta  ", json)
                for (var i = 0; i < filas.length; i++) {
                    let fila = filas[i]
                    options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                }
                let salida = []
                salida.push(<select onChange={this.onChange} name="selectMotivos" style={{ borderColor: '#F1F3FA' }} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ selectMotivos: salida })


            })
    }


    render() {
        return (
            <div>
                <div id="modal-Descartar" className="modal fade" tabindex="-1">
                    <div className="modal-dialog ">
                        <div className="modal-content text-white" style={{ backgroundColor: "#FFFFF" }}>
                            <div className="modal-header text-white text-center" style={{ backgroundColor: "#617187" }}>
                                <h6 className="modal-title col-12 text-center">Descartar registro</h6>

                            </div>
                            <div className="modal-body">
                                <div className="card-body">
                                    <h8 style={{ color: '#8F9EB3' }}>Si está seguro de descartar el registro, por favor seleccione el motivo</h8>
                                    {this.state.selectMotivos}
                                    {/* <select style={{ backgroundColor: '#F1F3FA', borderColor: '#D5D9E8' }} class="form-control">
                                        <option selected >Seleccionar</option>
                                     </select>*/}

                                </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-center">
                                <div className="col-12">
                                    <div className="row">
                                        <button type="button" style={{ width: '100%', backgroundColor: "#617187" }} class="btn text-white" onClick={this.cambiaDescartarProspecto} data-dismiss="modal"  >Descartar registro</button>
                                    </div>
                                    <br></br>
                                    <div className="row">
                                        <button type="button" style={{ width: '100%', backgroundColor: '#8F9EB3' }} class="btn text-white" id="btnAgregar" data-dismiss="modal"  >Cancelar</button>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="card">
                    <div class="row mb-2 mt-2 ml-1">
                        <div class="col-12 col-lg-9">
                            <div class="row">

                                <div class="col-6 col-lg-3 d-flex align-items-center pb-sm-1">
                                    <div class="mr-2">
                                        <a href="#"
                                            class="btn rounded-pill btn-icon btn-sm" style={{ backgroundColor: '#C28EE3' }}> <span
                                                class="letter-icon text-white">R</span>
                                        </a>
                                    </div>
                                </div>

                                <div class="col-6 col-lg-3 d-flex align-items-center pb-sm-1">
                                    <div class="mr-2">
                                        <a href="#" style={{ height: '12px', width: '12px', borderColor: '#C4D3E8' }}
                                            class="btn rounded-pill btn-icon btn-sm "> <span
                                                class="letter-icon"></span>
                                        </a>
                                    </div>

                                </div>


                                <div class="col-6 col-lg-1 d-flex align-items-center pb-sm-1">
                                    <div class="mr-2">
                                        <a href="#"
                                            class="btn  rounded-pill btn-icon btn-sm" style={{ borderColor: '#8189D4' }}> <span style={{ color: '#8189D4' }}
                                                class="letter-icon">EP</span>
                                        </a>
                                    </div>

                                </div>
                                <div class="col-6 col-lg-1 d-flex align-items-center pb-sm-1">
                                    <div class="mr-2">
                                        <a href="#"
                                            class="btn  rounded-pill btn-icon btn-sm" style={{ borderColor: '#5B96E2' }}> <span style={{ color: '#5B96E2' }}
                                                class="letter-icon">VC</span>
                                        </a>
                                    </div>

                                </div>
                                <div class="col-6 col-lg-1 d-flex align-items-center pb-sm-1">
                                    <div class="mr-2">
                                        <a href="#"
                                            class="btn  rounded-pill btn-icon btn-sm" style={{ borderColor: '#41B3D1' }}> <span style={{ color: '#41B3D1' }}
                                                class="letter-icon">CC</span>
                                        </a>
                                    </div>

                                </div>
                                <div class="col-6 col-lg-1 d-flex align-items-center pb-sm-1">
                                    <div class="mr-2">
                                        <a href="#"
                                            class="btn  rounded-pill btn-icon btn-sm" style={{ borderColor: '#78CB5A' }}> <span style={{ color: '#78CB5A' }}
                                                class="letter-icon">C</span>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>


                        <div class="col-12 col-lg-3">
                            <div class="float-right mr-3">
                                <button type="button"  title="Guardar y Regresar" onClick={(e) => this.props.botonCancelar()} class="btn rounded-pill mr-1" style={{ backgroundColor: '#8F9EB3', borderColor: '#8F9EB3' }}><span><i style={{ color: 'white' }} className="icon-backward2"></i></span></button>
                                <button type="button" title="Descartar" data-toggle="modal" data-target="#modal-Descartar" class="btn rounded-pill mr-1" style={{ backgroundColor: '#8F9EB3', borderColor: '#8F9EB3' }}><span><i style={{ color: 'white' }} className="icon-blocked"></i></span></button>
                                <button type="button" title="Registrar entrevista" onClick={this.cambiaEstatusDelProspecto} disabled={
                                    this.props.tipoUsuario != "EDATJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "GTEDR"
                                    && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER" ||
                                    (this.state.fec_entrevista_profunda == '')
                                } class="btn rounded-pill mr-1" style={{ backgroundColor: '#8F9EB3', borderColor: '#8F9EB3' }}><span><i style={{ color: 'white' }} className="icon-forward3"></i></span></button>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="card-header bg-transparent header-elements-sm-inline">
                    <h2 class="mb-0 font-weight-semibold" style={{ color: '#617187' }}>Registro - Programación de entrevista </h2>
                    <div class="header-elements">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Prospecto: <strong >{this.state.referido.nombre+' '+this.state.referido.ape_paterno + ' ' + (this.state.referido.ape_materno != null ? this.state.referido.ape_materno:'')}</strong></li>
                            <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Fecha de alta de Registro: <strong >{this.state.referido.ts_alta_audit}</strong></li>
                            <li class="list-inline-item font-size-sm" style={{ color: '#617187' }}>Usuario creador: <strong >{this.state.referido.user_id}</strong></li>
                            <li class="list-inline-item">
                                <h6 style={{ backgroundColor: "#C28EE3", color: '#FFFF', borderRadius: '8px' }}> <>&nbsp;&nbsp;</> En registro  <>&nbsp;&nbsp;</> </h6>
                            </li>
                        </ul>
                    </div>
                </div>

                <br></br>
                <div class="card">
                    <div class="card-header  header-elements-sm-inline">
                        <h5 class="mb-0 font-weight-semibold" >Programación de entrevista profunda</h5>
                    </div>
                    <hr></hr>
                    <div class="card-body">
                        <div className="col-xs-12 col-lg-4">
                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fecha de entrevista</span>
                                    </span> <input type="date" className="form-control " max="2100-01-01" disabled = { this.props.tipoUsuario != "ASESRJR" && this.props.tipoUsuario != "ASESRSR" && this.props.tipoUsuario != "GTEDR"
                                && this.props.tipoUsuario != "SDDC" && this.props.tipoUsuario != "SUPER"} name="fec_entrevista_profunda" onChange={this.onChange} value={this.state.fec_entrevista_profunda} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="accordion" id="accordionExample">

                    <div id="headingOne">
                        <h2 class="mb-0">
                            <button class="btn " type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <h6 class="mb-0 font-weight-semibold">Registro de prospecto </h6>
                            </button>
                        </h2>
                    </div>
                    <div class="card">
                        <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">

                               

                                {/* empieza el form  desplegable */}
                                
                                    <div className="row">
                                        <div className="col-xs-12 col-lg-2">
                                            <div className="form-group">
                                                <div className="input-group">
                                                    <h4 class="card-title py-3 font-weight-bold" >Información general</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-xs-12 col-lg-4">
                                            <div className="form-group">
                                                <div className="input-group">
                                                    <span className="input-group-prepend"> <span
                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Nombre (s)</span>
                                                    </span> <input type="text" placeholder="Nombre" name="nombre"  disabled onChange={this.onChange} value={this.state.referido.nombre} className="form-control " />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-xs-12 col-lg-4">
                                            <div className="form-group">
                                                <div className="input-group">
                                                    <span className="input-group-prepend"> <span
                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Apellido paterno</span>
                                                    </span> <input type="text" placeholder="Apellido" name="ape_paterno" disabled onChange={this.onChange} value={this.state.referido.ape_paterno} className="form-control " />
                                                </div>
                                            </div>
                                        </div>


                                        <div className="col-xs-12 col-lg-4">
                                            <div className="form-group">
                                                <div className="input-group">
                                                    <span className="input-group-prepend"> <span
                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Apellido materno</span>
                                                    </span> <input type="text" placeholder="Apellido" className="form-control " disabled name="ape_materno" onChange={this.onChange} value={this.state.referido.ape_materno} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-xs-12 col-lg-4">
                                            <div className="form-group">
                                                <div className="input-group">
                                                    <span className="input-group-prepend"> <span
                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fecha de nacimiento</span>
                                                    </span> <input type="date" className="form-control " name="fec_nacimiento" disabled onChange={this.onChange} value={this.state.referido.fec_nacimiento} />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-xs-12 col-lg-4">
                                            <div className="form-group">
                                                <div className="input-group">
                                                    <span className="input-group-prepend"> <span
                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>RFC</span>
                                                    </span> <input type="text" placeholder="Escribir" className="form-control "disabled name="rfc" onChange={this.onChange} value={this.state.referido.rfc} />
                                                </div>
                                            </div>
                                        </div>


                                        <div className="col-xs-12 col-lg-4">
                                            <div className="form-group">
                                                <div className="input-group">
                                                    <span className="input-group-prepend"> <span
                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Estado civil</span>
                                                    </span> {this.state.selectEstadoCivil}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-xs-12 col-lg-4">
                                            <div className="form-group">
                                                <div className="input-group">
                                                    <form encType="multipart/form" style={{ display: 'none' }} >
                                                        <input type="file" style={{ display: 'none' }} ref={(ref) => this.cv = ref} ></input>
                                                    </form>
                                                    <input type="text" style={{ borderColor: '#E1E5F0' }} disabled className="form-control " placeholder="Curriculum Vitae" value={this.state.nombreArchivoCV} />
                                                    <span className="input-group-prepend">
                                                        <button type="button" class="btn text-white" disabled onClick={this.onClickBotonArchivoCV} style={{ backgroundColor: this.state.colorBotonSubirArchivoCV }}>
                                                            <h10 style={{ color: this.state.colorSubirArchivoCV }}>+</h10>
                                                        </button>

                                                        {
                                                            this.state.muestraDocCV == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivo }}>
                                                                <a href={this.state.referido.doc_cv} target="_blank" rel="noopener noreferrer" >
                                                                    <i className="fas fa-eye"
                                                                        style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Ver" ></i>
                                                                </a>
                                                            </button>
                                                                : ''
                                                        }

                                                        <span className="input-group-text" style={{ background: this.state.colorSubirArchivoCV, borderColor: this.state.colorSubirArchivoCV, color: this.state.colorTextoSubirArchivoCV }} >Subir archivo</span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-xs-12 col-lg-4">
                                            <div className="form-group">
                                                <div className="input-group">
                                                    <span className="input-group-prepend"> <span
                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Envío de link PSP</span>
                                                    </span> <input type="date" className="form-control " disabled name="fec_envio_psp" onChange={this.onChange} value={this.state.referido.fec_envio_psp} />
                                                </div>
                                            </div>
                                        </div>


                                        <div className="col-xs-12 col-lg-4">
                                            <div className="form-group">
                                                <div className="input-group">
                                                    <span className="input-group-prepend"> <span
                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Correo electrónico</span>
                                                    </span> <input type="text" placeholder="ejemplo@correo.com" disabled className="form-control" name="correo_electronico" onChange={this.onChange} value={this.state.referido.correo_electronico} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-xs-12 col-lg-4">
                                            <div className="form-group">
                                                <div className="input-group">
                                                    <span className="input-group-prepend"> <span
                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Teléfono móvil</span>
                                                    </span> <input type="number" placeholder="55 0000 0000" disabled className="form-control " name="tel_movil" onChange={this.onChange} value={this.state.referido.tel_movil} />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-xs-12 col-lg-4">
                                            <div className="form-group">
                                                <div className="input-group">
                                                    <span className="input-group-prepend"> <span
                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Teléfono casa</span>
                                                    </span> <input type="number" placeholder="10 dígitos" disabled className="form-control" name="tel_casa" onChange={this.onChange} value={this.state.referido.tel_casa} />
                                                </div>
                                            </div>
                                        </div>


                                        <div className="col-xs-12 col-lg-4">
                                            <div className="form-group">
                                                <div className="input-group">
                                                    <span className="input-group-prepend"> <span
                                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Teléfono oficina</span>
                                                    </span> <input type="number" placeholder="10 dígitos" disabled className="form-control " name="tel_movil" onChange={this.onChange} value={this.state.referido.tel_movil} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="card-body">
                                        <div className="row">
                                            <div className="col-xs-12 col-lg-2">
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        <h4 class="card-title py-3 font-weight-bold">Información complementaría</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-xs-12 col-lg-4">
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        <span className="input-group-prepend"> <span
                                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fuente de reclutamiento</span>
                                                        </span> {this.state.selectFuenteReclutamiento}
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-xs-12 col-lg-4">
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        <span className="input-group-prepend"> <span
                                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fuente de reclutamiento para bonos</span>
                                                        </span> {this.state.selectFuenteReclutamientoBono} {/* mover este state al correcto */}
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-xs-12 col-lg-4">
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        <span className="input-group-prepend"> <span
                                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>EDAT asignado</span>
                                                        </span> {this.state.selectEDAT}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {this.state.muestraAgente ?
                                            <div className="row">
                                                <div className="col-xs-12 col-lg-4">
                                                    <div className="form-group">
                                                        <div className="input-group">
                                                            <span className="input-group-prepend"> <span
                                                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Agente referidor</span>
                                                            </span> {/*<input type="text" placeholder="Escribir" className="form-control " />*/} {this.state.selectAgenteReferidor}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xs-12 col-lg-4">
                                                    <div className="form-group">
                                                        <div className="input-group">
                                                            <span className="input-group-prepend"> <span
                                                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>CUA referidora para repostar a GNP</span>
                                                            </span> <input type="text" placeholder="Escribir" className="form-control " name="cua_referidor" disabled={true} onChange={this.onChange} value={this.state.referido.cua_referidor} />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xs-12 col-lg-4">
                                                    <div className="form-group">
                                                        <div className="input-group">
                                                            <span className="input-group-prepend"> <span
                                                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Nombre APS</span>
                                                            </span> <input type="text" placeholder="Escribir" className="form-control " disabled={true} name="nombre_aps" onChange={this.onChange} value={this.state.referido.nombre_aps} />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> : <div className="row">
                                                <div className="col-xs-12 col-lg-4">
                                                    <div className="form-group">
                                                        <div className="input-group">
                                                            <span className="input-group-prepend"> <span
                                                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Nombre APS</span>
                                                            </span> <input type="text" placeholder="Escribir" className="form-control " disabled={true} name="nombre_aps" onChange={this.onChange} value={this.state.referido.nombre_aps} />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        }
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}