import { toHaveAccessibleDescription } from "@testing-library/jest-dom/dist/matchers";
import React, { Component } from "react";
import Dashboard from "../InicioMenu/Dashboard";
const imageEncabezado = require("../imagenes/logo2.png")

export default class LoginComponent extends Component {


    constructor() {
        super();
        this.iniciaSesion = this.iniciaSesion.bind(this)

        this.state = {
            usuario: {},
            nombreUsuario: '',
            contrasenaUsuario: '',
            esLogin: true
        };
    }


    UNSAFE_componentWillMount() {

    }


    ingresaUsuario(e) {
        console.log(e.target.value)
        this.setState({ nombreUsuario: e.target.value })
    }


    ingresaContrasena(e) {
        console.log(e.target.value)
        this.setState({ contrasenaUsuario: e.target.value })
    }


    iniciaSesion() {
        fetch("http://desktop-5o8tke8:8080/usuarios/value/" + this.state.nombreUsuario)
            .then(response => response.json())
            .then(json => {
                console.log(json)
                this.setState({ usuario: json });
                if (json.password == this.state.contrasenaUsuario && json.value) {
                    this.setState({ esLogin: false })
                    localStorage.setItem('usuario',json);
                    window.location.replace("/inicio?usuario="+json.value)
                }

            });


    }




    render() {
        return (
            <div style={{ height: '100%', width: '100%' }}>
                {
                    this.state.esLogin === true ?
                        

                            <div>
                                <div className="navbar navbar-expand-lg  navbar-static">
                                    <div className="navbar-brand ml-2 ml-lg-0">

                                    </div>
                                </div>
                                <br /><br /><br /> <br /> <br /> <br /><br /> <br />
                                <div className="page-content" >
                                    <div className="content-wrapper">
                                        <div className="content-inner">
                                            <div className="content d-flex justify-content-center align-items-center">

                                                <div >
                                                    <div className="row justify-content-center">
                                                        <div className="col-xs-4">
                                                            <div className="tarjeta mb-0">
                                                                <div className="card-body">
                                                                    <div class="text-center mb-3">
                                                                        <img className="imagen" src={imageEncabezado} alt="" />
                                                                        <br /><br /><br />
                                                                        <h5 className="mb-0" style={{ color: '#313A46' }}><b>Bienvenid@ a ITZI</b></h5>
                                                                        <span className="d-block text-muted" style={{ color: '#617187' }}>Sistema de reclutamiento</span>
                                                                        <br /><br />
                                                                    </div>

                                                                    <div className="form-group" >
                                                                        <input type="text" placeholder="Usuario" onChange={this.ingresaUsuario.bind(this)} />
                                                                    </div>
                                                                    <br />
                                                                    <div className="form-group">
                                                                        <input type="password" placeholder="Contraseña" onChange={this.ingresaContrasena.bind(this)} />
                                                                    </div>
                                                                    <br />
                                                                    <div className="form-group" >
                                                                        <input type="button" value="Iniciar sesión" onClick={this.iniciaSesion} style={{ backgroundColor: '#f58646', borderColor: '#f58646', color: 'white' }} />
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                       
                        : ''
                }

                {
                 //     this.state.esLogin === false ? <Dashboard usuario={this.state.usuario}/>:''
                }




            </div>



        )
    }

}
