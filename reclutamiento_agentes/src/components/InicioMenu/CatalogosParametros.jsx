import React, { Component } from "react";
import Tabla from "../tablas/Tabla";
import Modal from '../modal/Modal'
import TabCatalogoParametros from "../Tabs/TabCatologoParametros";
const queryParams = new URLSearchParams(window.location.search)

export default class CatalogoParametros extends Component {

    constructor() {
        super();
    }


    render() {
        return (

            <div className="page-content" style={{ backgroundColor: '#f1f3fa', height: '100%', width: '100%' }}>
                <div className="content-wrapper">
                    <div className="content-inner">

                        <div className="content justify-content-center align-items-center">
                            <TabCatalogoParametros />
                        </div>
                    </div>
                </div>
            </div>


        )
    }

}