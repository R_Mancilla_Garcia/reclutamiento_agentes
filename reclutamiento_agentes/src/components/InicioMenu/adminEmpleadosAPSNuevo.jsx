import React, { Component } from "react";
import Tabla from "../tablas/Tabla";
import FormAdminEmpleadosAPSNuevo from "../modal/formAdminEmpleadosAPSNuevo";
import FormAdminEmpleadosAPS2Nuevo from "../modal/formAdminEmpleadosAPS2Nuevo";
import FormAdminEmpleadosAPS3 from "../modal/formAdminEmpleadosAPS3";
import FormAdminEmpleadosAPS3Nuevo from "../modal/formAdminEmpleadosAPS3Nuevo";
import datos from '../urls/datos.json'

const queryParams = new URLSearchParams(window.location.search)

export default class AdminEmpleadosAPSNuevo extends Component {
    constructor() {
        super();
        this.muestraFormulario1 = this.muestraFormulario1.bind(this)
        this.botonCancelar = this.botonCancelar.bind(this)
        this.muestraFormulario2 = this.muestraFormulario2.bind(this)
         this.muestraFormulario3 = this.muestraFormulario3.bind(this)
        this.state = {
            muestraTabla: true,
            muestraFormularioBandera: false,
            muestraFormularioBandera2: false,
            muestraFormularioBandera3:false,
            idEmpleado:null,
            idUsuario:null,
            tipoUsuario: null,
        };
    }

    UNSAFE_componentWillMount() {
        let tipoUsuario = queryParams.get("tipoUsuario")
        let idUsuario=queryParams.get("idUsuario")
        this.setState({ tipoUsuario: tipoUsuario, idUsuario: idUsuario })
        
    }

    muestraFormulario1(idEdicion) {
        if(idEdicion == undefined){
            this.setState({ muestraTabla: false, muestraFormularioBandera: true ,idEmpleado:null})// clicl al aregar nuevo
        }else{
            this.setState({ muestraTabla: false, muestraFormularioBandera: true, idEmpleado:idEdicion })// clicl al aregar nuevo
        }

    }

    muestraFormulario2(idEmpleado) {
        this.setState({ idEmpleado:idEmpleado,muestraTabla: false, muestraFormularioBandera: false,muestraFormularioBandera2:true })
    }
    muestraFormulario3(idEmpleado) {
        this.setState({ idEmpleado:idEmpleado,muestraTabla: false, muestraFormularioBandera: false,muestraFormularioBandera2:false,muestraFormularioBandera3:true })
    }


    botonCancelar() {
        console.log("boton cancelar")
        this.setState({ muestraFormularioBandera: false,muestraFormularioBandera2:false,muestraFormularioBandera3:false, muestraTabla: true, esEdicionFormulario: false,idEmpleado:null })

    }



    render() {
        return (
            <div className="page-content" style={{ backgroundColor: '#f1f3fa', height: '100%', width: '100%' }}>
                <div className="content-wrapper">
                    <div className="content-inner">


                        <div className="content justify-content-center align-items-center">

                            {
                                this.state.muestraTabla &&
                                <Tabla
                                    url={datos.urlServicePy+'parametros/api_vcat_empleados/0'}
                                    tituloTabla={'Administrar empleados APS'} tabla="api_vcat_empleados"
                                    muestraFormulario={this.muestraFormulario1} modalNombre={""} colorAgregar="#ED6C26" nonModal={true} />
                            }

                            {
                                this.state.muestraFormularioBandera && !this.state.muestraTabla &&
                                <FormAdminEmpleadosAPSNuevo idUsuario={this.state.idUsuario} botonCancelar={this.botonCancelar} muestraFormulario2={this.muestraFormulario2} idEmpleado={this.state.idEmpleado}  muestraFormulario3={this.muestraFormulario3} tipoUsuario = {this.state.tipoUsuario}/>
                            }

                            {
                                !this.state.muestraFormularioBandera && !this.state.muestraTabla && this.state.muestraFormularioBandera2 &&
                                <FormAdminEmpleadosAPS2Nuevo  idUsuario={this.state.idUsuario} botonCancelar={this.botonCancelar} idEmpleado={this.state.idEmpleado}  muestraFormulario={this.muestraFormulario1} muestraFormulario3={this.muestraFormulario3} tipoUsuario = {this.state.tipoUsuario}/>
                            }
                             {
                                !this.state.muestraFormularioBandera  && !this.state.muestraTabla && !this.state.muestraFormularioBandera2 && this.state.muestraFormularioBandera3 &&
                                <FormAdminEmpleadosAPS3Nuevo  idUsuario={this.state.idUsuario} botonCancelar={this.botonCancelar} idEmpleado={this.state.idEmpleado}  muestraFormulario={this.muestraFormulario1} muestraFormulario2={this.muestraFormulario2} tipoUsuario = {this.state.tipoUsuario}/>
                            }

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}