import React, { Component } from "react";
import TablaCombinada from "../tablas/TablaCombinada";
import Modal from "../modal/Modal";
import FormEditaReferido from "../modal/fromEditaReferido";
import FormCompletarRegistroReferido from "../modal/formCompletarRegistroReferido";
import datos from '../urls/datos.json'

const queryParams = new URLSearchParams(window.location.search)

export default class RegistroReferido extends Component {
    constructor() {
        super();
        this.muestraFormulario = this.muestraFormulario.bind(this)
        this.completarRegistro=this.completarRegistro.bind(this)
        this.botonCancelar=this.botonCancelar.bind(this)
        this.guardaElementoNuevo=this.guardaElementoNuevo.bind(this)
        this.enviaNotificacion=this.enviaNotificacion.bind(this)
        this.state = {
            muestraTabla: true,
            muestraFormularioBandera: false,
            muestraFormularioBandera2: false,
            muestraFormularioBandera3: false,
            idEmpleado:null,
            idUsuario:null,
            usuario:null
        };
    }


    UNSAFE_componentWillMount() {
        let idUsuario=queryParams.get("idUsuario")
        let usuario=queryParams.get("usuario")        
        
        this.setState({idUsuario:idUsuario,usuario:usuario})
    }

    muestraFormulario(idEmpleado) { // editaReferido
        this.setState({ muestraTabla: false, muestraFormularioBandera: true , idEmpleado:idEmpleado})
    }


    completarRegistro() {
        this.setState({ muestraTabla: false, muestraFormularioBandera: false,muestraFormularioBandera2:true })
    }

    botonCancelar(){
        this.muestraFormulario(null)
        this.setState({ muestraTabla: true, muestraFormularioBandera: false,muestraFormularioBandera2:false,idEmpleado:null })
    }

    enviaNotificacion(idReferido){
        let jsonNotificacion={
            "mail_id": 1,    
            "prospecto_id": idReferido    
        }
        const requestOptions = {
            method: "POST",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(jsonNotificacion)
        };
        
        fetch(datos.urlServicePy+"recluta/api_notificador/0" , requestOptions)
            .then(response => response.json())
            .then(data => { })
    }

    guardaElementoNuevo(jsonReferido){
        jsonReferido.user_id=this.state.idUsuario
        const requestOptions = {
            method: "POST",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(jsonReferido)
        };

        fetch(datos.urlServicePy+"parametros/api_recluta_referidos/0" , requestOptions)
            .then(response => response.json())
            .then(data => { 

                console.log(data)
                this.enviaNotificacion(data.id)
                this.muestraFormulario(null)
                this.botonCancelar()
            })
    }


    render() {
        return (
            <div className="page-content" style={{ backgroundColor: '#f1f3fa', height: '100%', width: '100%' }}>
                <div className="content-wrapper">
                    <div className="content-inner">


                        <div className="content justify-content-center align-items-center">
                            {
                                this.state.muestraTabla == true ?
                                <div>
                                    <TablaCombinada
                                        url={datos.urlServicePy+'parametros/api_recluta_vreferidos/0'}
                                        urltoUrl = {datos.urlServicePy+"refepros_edat/api_recluta_referidos/0"}
                                        tituloTabla={'Registro de referidos'}
                                        mostrarModal={0}
                                        modalNombre="modal-creaReferido"
                                        colorAgregar="#ED6C26"
                                        muestraFormulario={this.muestraFormulario}
                                        tabla="api_recluta_vreferidos"
                                        usuario= {this.state.usuario}
                                        pivote = "referido"
                                        />
                                    <Modal   modalNombre='modal-creaReferido' guardaElementoNuevo={this.guardaElementoNuevo} tituloModal={"Agregar nuevo referido"}/>
                                </div>:''
                            }

                            {
                                !this.state.muestraTabla && this.state.muestraFormularioBandera &&
                                <FormEditaReferido  idUsuario={this.state.idUsuario} idEmpleado={this.state.idEmpleado} completarRegistro={this.completarRegistro} botonCancelar={this.botonCancelar}/>
                            }


                            {
                                !this.state.muestraTabla && !this.state.muestraFormularioBandera && this.state.muestraFormularioBandera2 &&
                                <FormCompletarRegistroReferido idUsuario={this.state.idUsuario} idEmpleado={this.state.idEmpleado} botonCancelar={this.botonCancelar} />
                            }



                        </div>
                    </div>
                </div>
            </div>

        )

    }


}