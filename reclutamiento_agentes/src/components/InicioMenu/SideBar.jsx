//import * as React from "react";
import React, { useEffect, useState } from "react";

import Tabla from "../tablas/Tabla.jsx";
const logoSideBar = require("../imagenes/logoSideBar.png")
const logoAps = require("../imagenes/logo-aps.png")
const itziIco = require("../imagenes/itzi_ico.png")
const avatar = require("../imagenes/avatar.png")
export default function SideBar(props) {


    let [listaEdat, setListaEdat] = useState([])
    const [edat,setEdat]=useState([]);

    useEffect(() => {
        console.log("props de side ", props)
        for (var i = 0; i < props.permisos.length; i++) {
            let permiso = props.permisos[i]
            if (permiso.seccion_prefijo == 'EDAT') {
                console.log(permiso,"EDAT")
                setListaEdat(listaEdat.push(permiso))
                setEdat(<li className="nav-item nav-item-submenu">
                <li className="nav-item-header"><div className="text-uppercase font-size-xs line-height-xs">Main</div> <i className="icon-menu" title="Main"></i></li>

                <a href="#" class="nav-link"><i class="icon-copy"></i> <span>Layouts</span></a>
                                <ul class="nav nav-group-sub" data-submenu-title="Layouts">
                                    <li class="nav-item"><a href="index.html" class="nav-link active">Default layout</a></li>
                                    <li class="nav-item"><a href="../../../../layout_2/LTR/dark/full/index.html" class="nav-link">Layout 2</a></li>
                                    <li class="nav-item"><a href="../../../../layout_7/LTR/dark/full/index.html" class="nav-link disabled">Layout 7 <span class="badge bg-transparent align-self-center ml-auto">Coming soon</span></a></li>
                                </ul>

            </li>)
            }

        }

    }, [props.usuario, props.permisos]);





    return (
        <div>
            <div id="sideBar" name="sideBar" className="sidebar sidebar-dark sidebar-main sidebar-expand-lg sidebar-main-resized  ">
                <div className="sidebar-content">
                    <div className="sidebar-section sidebar-user my-1">
                        <div className="sidebar-section-body">
                            <div className="media">
                                <a href="#" class="mr-3">
                                    <img src={avatar} className="rounded-circle" alt="" />
                                </a>
                                {/*                           <img className="media-body" src={logoSideBar} style={{ height: '25px;', width: '150px;', position: ' relative;', left: '-17px;' }} />*/}

                                <div className="media-body">
                                    <div className="font-weight-semibold">{props.usuario.nombre}</div>
                                    <div className="font-size-sm line-height-sm opacity-50">
                                        Senior developer
                                    </div>
                                </div>


                                <div className="ml-3 align-self-center">

                                    <button type="button" name="toggleSideBar" id="toggleSideBar"
                                        className="btn btn-outline-light-100 text-white border-transparent btn-icon rounded-pill btn-sm sidebar-control sidebar-main-resized   d-none d-lg-inline-flex">
                                        <i className="icon-transmission"></i>
                                    </button>

                                    <button type="button" className="btn btn-outline-light-100 text-white border-transparent btn-icon rounded-pill sidebar-main-resized  btn-sm sidebar-mobile-main-toggle d-lg-none">
                                        <i className="icon-cross2"></i>
                                    </button>
                                </div>
                            </div>
                        </div>


                    </div>


                    <div class="sidebar-section">
                        <ul class="nav nav-sidebar" data-nav-type="accordion">
                            <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Inicio</div> <i class="icon-menu" title="Main"></i></li>
                            <li class="nav-item">
                                <a href="#" class="nav-link active">
                                    <i class="icon-home4"></i>
                                    <span>
                                        Dashboard
                                    </span>
                                </a>
                            </li>


                                { 
                                  edat
                                }
                                   
                            

                            <li class="nav-item nav-item-submenu">
                                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>

                                <a href="#" class="nav-link"><i class="icon-copy"></i> <span>Layouts</span></a>
                                <ul class="nav nav-group-sub" data-submenu-title="Layouts">
                                    <li class="nav-item"><a href="index.html" class="nav-link active">Default layout</a></li>
                                    <li class="nav-item"><a href="../../../../layout_2/LTR/dark/full/index.html" class="nav-link">Layout 2</a></li>
                                    <li class="nav-item"><a href="../../../../layout_7/LTR/dark/full/index.html" class="nav-link disabled">Layout 7 <span class="badge bg-transparent align-self-center ml-auto">Coming soon</span></a></li>
                                </ul>
                            </li>

                            <li class="nav-item nav-item-submenu">
                                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>

                                <a href="#" class="nav-link"><i class="icon-copy"></i> <span>Layouts</span></a>
                                <ul class="nav nav-group-sub" data-submenu-title="Layouts">
                                    <li class="nav-item"><a href="index.html" class="nav-link active">Default layout</a></li>
                                    <li class="nav-item"><a href="../../../../layout_2/LTR/dark/full/index.html" class="nav-link">Layout 2</a></li>
                                    <li class="nav-item"><a href="../../../../layout_7/LTR/dark/full/index.html" class="nav-link disabled">Layout 7 <span class="badge bg-transparent align-self-center ml-auto">Coming soon</span></a></li>
                                </ul>
                            </li>

                            <li class="nav-item nav-item-submenu">
                                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>

                                <a href="#" class="nav-link"><i class="icon-copy"></i> <span>Layouts</span></a>
                                <ul class="nav nav-group-sub" data-submenu-title="Layouts">
                                    <li class="nav-item"><a href="index.html" class="nav-link active">Default layout</a></li>
                                    <li class="nav-item"><a href="../../../../layout_2/LTR/dark/full/index.html" class="nav-link">Layout 2</a></li>
                                    <li class="nav-item"><a href="../../../../layout_7/LTR/dark/full/index.html" class="nav-link disabled">Layout 7 <span class="badge bg-transparent align-self-center ml-auto">Coming soon</span></a></li>
                                </ul>
                            </li>


                        </ul>
                    </div>
                </div>
            </div>







        </div>

    )


}
