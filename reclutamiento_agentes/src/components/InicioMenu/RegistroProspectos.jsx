import React, { Component } from "react";
import TablaCombinada from "../tablas/TablaCombinada";
import Modal from "../modal/Modal";
import FormEditaProspectos from "../modal/formEditaProspecto";
import axios from 'axios';
import datos from '../urls/datos.json'

const queryParams = new URLSearchParams(window.location.search)

export default class RegistroProspecto extends Component {
    constructor() {
        super();
        this.guardaElementoNuevo=this.guardaElementoNuevo.bind(this)
        this.muestraFormulario = this.muestraFormulario.bind(this)
        this.botonCancelar=this.botonCancelar.bind(this)

        this.state = {
            muestraTabla: true,
            muestraFormularioBandera: false,
            muestraFormularioBandera2: false,
            muestraFormularioBandera3: false,
            idEmpleado:null,
            idUsuario:null,
            usuario:null
        };
    }


    UNSAFE_componentWillMount() {
        let idUsuario=queryParams.get("idUsuario")
        let usuario=queryParams.get("usuario")
        
        this.setState({idUsuario:idUsuario,usuario:usuario})
    }

    guardaElementoNuevo(jsonReferido,cvDocumento){
        jsonReferido.estatus_id=1
        jsonReferido.estadocivil_id=jsonReferido.estadocivil_id == 0 ? null:jsonReferido.estadocivil_id
        jsonReferido.user_id=this.state.idUsuario
        const requestOptions = {
            method: "POST",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(jsonReferido)
        };

    

        fetch(datos.urlServicePy+"parametros/api_recluta_prospectos/0" , requestOptions)
            .then(response => response.json())
            .then(data => { 
                console.log("el cv ", cvDocumento, "el data ",data)
                if (cvDocumento != undefined) {
                    let formData = new FormData();
                    formData.append('id',data.id)
                    formData.append('cv', cvDocumento)
                    formData.append('prospecto_id', data.id)
                    const config = {
                        headers: { 'content-type': 'multipart/form-data' }
                    }
                    axios.post(datos.urlServicePy+"api/doc_prospectos/", formData, config)
                        .then(res => {
                            console.log(res.data);
                            console.log(this.state.filename);
                            console.log(formData);
                        })

                }
                this.muestraFormulario(null)
                this.botonCancelar()
            })
    }

    muestraFormulario(idEmpleado) { // editaReferido
        this.setState({ muestraTabla: false, muestraFormularioBandera: true , idEmpleado:idEmpleado})
    }

    botonCancelar(){
        this.setState({ muestraTabla: true, muestraFormularioBandera: false,muestraFormularioBandera2:false,idEmpleado:null })
    }
    render() {
        return (
            <div className="page-content" style={{ backgroundColor: '#f1f3fa', height: '100%', width: '100%' }}>
                <div className="content-wrapper">
                    <div className="content-inner">


                        <div className="content justify-content-center align-items-center">
                            {
                                this.state.muestraTabla &&
                                <div>
                                    <TablaCombinada
                                        url={datos.urlServicePy+'parametros/api_recluta_vprospectos/0'}
                                        urltoUrl = {datos.urlServicePy+"refepros_edat/api_recluta_prospectos/0"}
                                        tituloTabla={'Registro de prospectos'}
                                        mostrarModal={0}
                                        modalNombre="modal-creaProspecto"
                                        colorAgregar="#ED6C26"
                                        muestraFormulario={this.muestraFormulario}
                                        tabla="api_recluta_vprospectos"
                                        usuario= {this.state.usuario}
                                        pivote = "prospecto"
                                    />
                                    <Modal modalNombre='modal-creaProspecto' guardaElementoNuevo={this.guardaElementoNuevo} tituloModal={"Agregar nuevo prospecto"}/>
                                </div>
                            }

                            {
                                !this.state.muestraTabla && this.state.muestraFormularioBandera &&
                                <FormEditaProspectos idUsuario={this.state.idUsuario} idEmpleado={this.state.idEmpleado} botonCancelar={this.botonCancelar} />
                            }


                            {
                               /* !this.state.muestraTabla && !this.state.muestraFormularioBandera && this.state.muestraFormularioBandera2 &&
                                <FormCompletarRegistroReferido idEmpleado={this.state.idEmpleado} botonCancelar={this.botonCancelar} />*/
                            }



                        </div>
                    </div>
                </div>
            </div>

        )
    }
}