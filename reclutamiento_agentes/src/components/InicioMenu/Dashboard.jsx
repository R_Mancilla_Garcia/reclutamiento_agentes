import React, { Component } from "react";
import NavBar from "./NavBar";
import SideBar from "./SideBar";
import Footer from "./Footer"
import Tabla from "../tablas/Tabla";
import modelo from '../tablas/modelo.json'
import Modal from '../modal/Modal'
import ProcesoReclutamiento from '../procesoReclutamiento/ProcesoReclutamiento'
import TabCatalogoReclutamiento from '../Tabs/TabCatalogoReclutamiento'
import datos from '../urls/datos.json'

const queryParams = new URLSearchParams(window.location.search)

export default class Dashboard extends Component {

    constructor() {
        super();
        this.obtenPermisos = this.obtenPermisos.bind(this)
        this.guardaElementoNuevo=this.guardaElementoNuevo.bind(this)
        this.mostrarModalParametrosReclutamiento=this.mostrarModalParametrosReclutamiento.bind(this)
        this.state = {
            usuario: {},
            permisosSideBar: [],
            mostrarTablaParametrosReclutamiento:true,
            mostrarModalParametrosReclutamiento:false
        };
    }

    UNSAFE_componentWillMount() {
        /* console.log(queryParams.get("usuario"))
         let user = queryParams.get("usuario")
         if (user != undefined && user.length != 0) {
             console.log("entro a la peticion")
             fetch("http://desktop-5o8tke8:8080/usuarios/value/" + user)
                 .then(response => response.json())
                 .then(json => {
                     this.setState({ usuario: json });
                    this.obtenPermisos(json.id)
 
                 });
         } else {
             console.log("revento por no tener usuario")
         }*/

    }

    obtenPermisos(idUsuario) {
        /* fetch("http://desktop-5o8tke8:8080/usuarios/permisos/" + idUsuario)
         .then(response => response.json())
         .then(json => {
             console.log("los permisos ",json)
             this.setState({ permisosSideBar: json });
         });*/
    }

    guardaElementoNuevo(url,json,verboHTTP){
        this.setState({mostrarTablaParametrosReclutamiento:false})
        console.log("la url al persistir ",url) 
        console.log("el json al persistir ",json) 


        const requestOptions = {
            method: verboHTTP,
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };
        fetch(url, requestOptions)
            .then(response => response.json())
            .then(data =>{
                console.log("la respuesta ",data)
                this.setState({mostrarTablaParametrosReclutamiento:true})
            });
    }

    mostrarModalParametrosReclutamiento(){
        console.log("entrando a prender")
        this.setState({mostrarModalParametrosReclutamiento:true})
    }




    render() {
        return (

            <div className="page-content" style={{ backgroundColor: '#f1f3fa', height: '100%', width: '100%' }}>
                {/*<SideBar usuario={this.state.usuario} permisos={this.state.permisosSideBar} ></SideBar>*/}
                <div className="content-wrapper">
                    <div className="content-inner">
                        {/*<NavBar usuario={this.state.usuario}></NavBar>*/}
                        <div className="content justify-content-center align-items-center">

                            {/*
                             <ProcesoReclutamiento/>

                             <iframe src="http://desktop-5o8tke8:8081/edat/reclutamientoAnual/"style={{marginLeft: '40px', border: '0', height:"100%", width:"100%" }}name="demo">
                            </iframe> 
                            
                                <TabCatalogoReclutamiento/>
                           */
                            } 
                            
                          {
                              this.state.mostrarTablaParametrosReclutamiento ?  
                              <div>
                              <Tabla   url={datos.urlServiceDashboard+'parametros/api_cat_parametros/0'} tituloTabla={'Parametros reclutamiento'}  tabla="api_cat_parametros"
                              guardaElementoNuevo={this.guardaElementoNuevo} colorAgregar="#ED6C26" mostrarModal={this.mostrarModalParametrosReclutamiento}  modalNombre="modal-parametroReclutamiento" modalNombreEdicion="modal-parametroReclutamientoEdicion" />
                              <Modal modalNombre='modal-parametroReclutamiento' guardaElementoNuevo={this.guardaElementoNuevo}  />
                              </div>
                              :''
                        }
                        
                       {/* <Tabla  modelo={modelo} url={''} tituloTabla={'Parametros reclutamiento'}/>*/}
                                                    
                                
                           
                            
                        

                            
                            






                        </div>
                        {/*<Footer />*/}
                    </div>
                </div>
            </div>

        )

    }

}




