import * as React from "react";
export default function NavBar(props) {
    const avatar = require("../imagenes/avatar.png")
    const logoSideBar = require("../imagenes/logoSideBar.png")

    return (
        <div className="card" style={{ backgroundColor: '#f1f3fa' }}>
            <div className="row mb-2 mt-2 ml-1">
                <div className="col-9 d-flex align-items-center">
                    <button className="navbar-toggler sidebar-mobile-main-toggle text-body d-lg-none" type="button">
                        <i className="icon-three-bars"></i>
                    </button>
                    <div className="breadcrumb" >
                    </div>
                </div>
                <div className="col-3">
                    <div className="float-right pt-1 pr-2">
                        <a href="#" className="dropdown-toggle d-inline-flex align-items-center" data-toggle="dropdown">
                            <img src={avatar} className="rounded-pill mr-lg-2 mt-0" height="34" alt />
                            <div className="media-body d-none d-md-block">
                                <span className="text-muted">{ props.usuario.nombre +' '+ props.usuario.apaterno}</span>
                            </div>
                        </a>
                        <div className="dropdown-menu dropdown-menu-right">
                            <a href="/perfilUsuario" className="dropdown-item"><i className="icon-user-plus"></i>Mi Perfil</a>
                            <a href="#" className="dropdown-item d-lg-none"><i className="icon-bell3"></i>Notificaciones</a>
                            <a href="/login" className="dropdown-item"><i className="icon-switch2"></i>Salir</a>
                        </div>
                    </div>
                    <div className="float-right pt-1 pr-2">
                        <a href="#" class="navbar-nav-link" data-toggle="dropdown">
                            <i class="icon-git-compare"></i>
                            <span class="d-lg-none ml-3">Git updates</span>
                            <span class="badge badge-warning badge-pill ml-auto ml-lg-0">1</span>
                        </a>
                        <div className="dropdown-menu dropdown-menu-right">
                            <div class="dropdown-content-header">
                                <span class="font-weight-semibold">Notificaciones</span>
                                <a href="#" class="text-body"><i class="icon-sync"></i></a>
                            </div>
                            <div class="dropdown-content-body dropdown-scrollable">
                                <ul class="media-list">
                                    <li class="media">
                                        <div class="mr-3">
                                            <a href="#" class="btn btn-outline-primary rounded-pill border-2 btn-icon"><i class="icon-git-pull-request"></i></a>
                                        </div>

                                        <div class="media-body">
                                            Drop the IE <a href="#">specific hacks</a> for temporal inputs
                                            <div class="text-muted font-size-sm">4 minutes ago</div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}