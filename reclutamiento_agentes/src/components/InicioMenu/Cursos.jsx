import React, { Component } from "react";
import TabCatalogoCursos from "../Tabs/TabCatalogoCursos";


export default class Cursos extends Component {
    constructor() {
        super();
        this.state = {
            usuario: {},
            permisosSideBar: [],
            mostrarTablaParametrosReclutamiento:true,
            mostrarModalParametrosReclutamiento:false
        };
    }


  
    render() {
        return (
            
            <div className="page-content" style={{ backgroundColor: '#f1f3fa', height: '100%', width: '100%' }}>
            <div className="content-wrapper">
                <div className="content-inner">
                    
                    <div className="content justify-content-center align-items-center">
                    <TabCatalogoCursos />
                    </div>
                </div>
            </div>
        </div>


        )
    }

}