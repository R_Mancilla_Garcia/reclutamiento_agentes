import React, { Component } from "react";
import Tabla from "../tablas/Tabla";
import modelo from '../tablas/modelo.json'
import BarraAdminEmpleadosAPS from "../barrasPorPasos/barraAdminEmpleadosAps";
import FormAdminEmpleadosAPS from "../modal/formAdminEmpleadosAPS";
import FormAdminEmpleadosAPS2 from "../modal/formAdminEmpleadosAPS2";
import FormAdminEmpleadosAPS3 from "../modal/formAdminEmpleadosAPS3";
import axios from 'axios';
import datos from '../urls/datos.json'

export default class AdminEmpleadosAPS extends Component {
    constructor() {
        super();
        this.muestraFormulario1 = this.muestraFormulario1.bind(this)
        this.muestraFormulario2 = this.muestraFormulario2.bind(this)
        this.muestraFormulario3 = this.muestraFormulario3.bind(this)
        this.seleccionaColumnasPrimerForm = this.seleccionaColumnasPrimerForm.bind(this)
        this.seleccionaColumnasSegundoForm = this.seleccionaColumnasSegundoForm.bind(this)
        this.botonCancelar = this.botonCancelar.bind(this)
        this.guardaFormulario = this.guardaFormulario.bind(this)
        this.guardaFormulario2 = this.guardaFormulario2.bind(this)
        this.enviaPrimerFormulario = this.enviaPrimerFormulario.bind(this)
        this.guardaElementoNuevo = this.guardaElementoNuevo.bind(this)
        this.guardaElementoNuevoPOST = this.guardaElementoNuevoPOST.bind(this)
        this.enviaFotografia= this.enviaFotografia.bind(this)
        this.guardaFotografia = this.guardaFotografia.bind(this)
        this.guardaArchivo=this.guardaArchivo.bind(this)
        this.enviaSegundoFormulario = this.enviaSegundoFormulario.bind(this)
        this.state = {
            muestraTabla: true,
            muestraFormularioBandera: false,
            esEdicionFormulario: false,
            muestraFormularioBandera2: false,
            muestraFormularioBandera3: false,
            contenidoEdicionPrimerForm: undefined,
            contenidoEdicionSegundoForm: undefined,
            idEditando: 0,
            jsonPrimerFormulario: {},
            jsonSegundoFormulario: {},
            fotografia: undefined,
            archivoPuesto:undefined
        };
    }





    muestraFormulario1(idEdicion) {
        console.log("entrando a cambiar bandera")
        if (idEdicion == undefined) {
            fetch(datos.urlServicePy+"parametros/api_cat_empleados/0")
                .then(response => response.json())
                .then(json => {
                    this.seleccionaColumnasPrimerForm(json.columnas, json.filas[0].fila, false)

                    this.setState({ muestraTabla: false, muestraFormularioBandera: true })// clicl al aregar nuevo
                });
        } else {
            console.log("idEdicion ", idEdicion)
            fetch(datos.urlServicePy+"parametros/api_cat_empleados/" + idEdicion)
                .then(response => response.json())
                .then(json => {
                    console.log("entrando a la peticion", json)
                    this.seleccionaColumnasPrimerForm(json.columnas, json.filas[0].fila, true,)
                    this.setState({ muestraTabla: false, muestraFormularioBandera: true, esEdicionFormulario: true, idEditando: idEdicion })// clicl al editar uno
                });


        }

    }

    seleccionaColumnasPrimerForm(columnas, fila, esEdicion) {
        let columnasSalida = {}
        console.log(fila)
        for (var i = 0; i < fila.length; i++) {
            console.log(fila[i])
            columnasSalida["" + columnas[i].key + ""] = esEdicion ? fila[i].value : ''
        }
        this.setState({ contenidoEdicionPrimerForm: columnasSalida })
        console.log("datos pimer form ", columnasSalida)
    }


    seleccionaColumnasSegundoForm(columnas, fila, esEdicion) {
        let columnasSalida = {}
        console.log(fila)
        for (var i = 0; i < fila.length; i++) {
            console.log(fila[i])
            columnasSalida["" + columnas[i].key + ""] = esEdicion ? fila[i].value : ''
        }
        this.setState({ contenidoEdicionSegundoForm: columnasSalida })
        console.log("datos segundo form ", columnasSalida)
    }


    guardaArchivo(archivo,selector){
        console.log("guardando ",selector,archivo)
        this.setState({[selector]:archivo})
    }

    muestraFormulario2(idEdicion) {
        if (idEdicion == undefined) {
            fetch(datos.urlServicePy+"parametros/api_cat_empleados/0")
                .then(response => response.json())
                .then(json => {
                    this.seleccionaColumnasSegundoForm(json.columnas, json.filas[0].fila, false)

                    this.setState({ muestraTabla: false, muestraFormularioBandera: false, muestraFormularioBandera2: true })// clicl al aregar nuevo
                });
        } else {
            console.log("idEdicion ", this.state.idEditando)
            fetch(datos.urlServicePy+"parametros/api_cat_empleados/" + idEdicion)
                .then(response => response.json())
                .then(json => {
                    console.log("entrando a la peticion", json)
                    this.seleccionaColumnasSegundoForm(json.columnas, json.filas[0].fila, true,)
                    this.setState({ muestraTabla: false, muestraFormularioBandera: false, muestraFormularioBandera2: true, esEdicionFormulario: true, idEditando: idEdicion })// clicl al editar uno
                });


        }

    }

    enviaSegundoFormulario(verboHTTP) {
        var id = this.state.idEditando
        console.log("el json sedungo from es ", this.state.jsonSegundoFormulario)
        var url = datos.urlServicePy+"parametros_upd/api_cat_empleados/" + id


        this.guardaElementoNuevo(url,this.state.jsonSegundoFormulario,verboHTTP)
        if (this.state.archivoPuesto != undefined) {
            console.log("entro a enviar la foto")
            let formData = new FormData();
            formData.append('empleado', id);
            formData.append('puesto', this.state.archivoPuesto);
            const config = {
                headers: { 'content-type': 'multipart/form-data' }
            }

            axios.post(datos.urlServicePy+"api/doc_empleado/", formData, config)
                .then(res => {
                    console.log(res.data);
                    console.log(this.state.filename);
                    console.log(formData);
                })
        }

    }


    muestraFormulario3() {
        console.log("entrando a cambiar bandera3")
        this.setState({ muestraTabla: false, muestraFormularioBandera: false, muestraFormularioBandera2: false, muestraFormularioBandera3: true })
    }

    botonCancelar() {
        console.log("boton cancelar")
        this.setState({ muestraTabla: false, muestraFormularioBandera: false, muestraFormularioBandera2: false, muestraFormularioBandera3: false, muestraTabla: true, esEdicionFormulario: false })

    }


    guardaFotografia(fotografia) {
        console.log("la fotografia ", fotografia)
        this.setState({ fotografia: fotografia })


    }
    guardaFormulario(json) {
        console.log("guardando el jjosn ", json)
        this.setState({ jsonPrimerFormulario: json })
    }

    guardaFormulario2(json) {
        console.log("guardando el jjosn ", json)
        this.setState({ jsonSegundoFormulario: json })
    }

    enviaPrimerFormulario(verboHTTP) {

        let json = this.state.jsonPrimerFormulario
        if (json.ape_materno != null) {
            if (json.ape_materno.length == 0) {
                json.ape_materno = null
            }
        }
    if(!this.state.esEdicionFormulario){
        json.nombre_conyugue= null
        json.fec_nacimiento_conyugue= null
        json.fec_aniversario= null
        json.estatusbaja_id= null
        json.fec_baja= null
        json.causabaja_id= null
        json.fec_ingreso= null
        json.escolaridad_id= null
        json.profesion_id= null
        json.puesto_id= null
        json.doc_puesto= ""
        json.acceso_pai= 0
        json.figura_id= null
        json.area_id= null
        json.supervisor= null
        json.doc_cv= ""
        json.doc_psp= ""
        json.sueldo= 0
        json.banco_id= null
        json.cta_clabe= null
        json.sociedad_id= null
        json.activo= 0
        json.correo_electronico= null
        json.otros_correos= null
        json.fecha_baja_correos= null
        json.num_extension= 0
        json.dias_vacaciones= 0
        json.acceso_impresion= 0
        json.acceso_copiadora= 0
        json.ts_alta_audit= null
        json.ts_actualiza_audit= null
        json.user_id= 0
    }
        

       
        console.log("enviando primer form  ", json);
        var id = this.state.idEditando//this.state.jsonPrimerFormulario.id  
        var url = verboHTTP == "POST" ? datos.urlServicePy+"parametros/api_cat_empleados/0" : datos.urlServicePy+"parametros_upd/api_cat_empleados/" + id
       


        verboHTTP == "POST" ? this.guardaElementoNuevoPOST(url,json,verboHTTP):  this.guardaElementoNuevo(url,json,verboHTTP)
        /* if (this.state.fotografia != undefined) {
              console.log("entro a enviar la foto")
              let formData = new FormData();
              formData.append('empleado', id);
              formData.append('fotografia', this.state.fotografia);
              const config = {
                  headers: { 'content-type': 'multipart/form-data' }
              }
  
              axios.post(datos.urlServicePy+"api/doc_empleado/", formData, config)
                  .then(res => {
                      console.log(res.data);
                      console.log(this.state.filename);
                      console.log(formData);
                  })
          }*/


    }



    enviaFotografia(id){
        if (this.state.fotografia != undefined) {
            console.log("entro a enviar la foto")
            let formData = new FormData();
            formData.append('empleado', id);
            formData.append('fotografia', this.state.fotografia);
            const config = {
                headers: { 'content-type': 'multipart/form-data' }
            }

            axios.post(datos.urlServicePy+"api/doc_empleado/", formData, config)
                .then(res => {
                    console.log(res.data);
                    console.log(this.state.filename);
                    console.log(formData);
                })
        }
    }

    guardaElementoNuevoPOST(url, json, verboHTTP) {
        // this.setState({ mostrarTablaParametrosReclutamiento: false })
         console.log("la url al persistir ", url)
         console.log("el json al persistir ", json)
 
 
         const requestOptions = {
             method: verboHTTP,
             headers: { 'Content-Type': 'application/json' },
             body: JSON.stringify(json)
         };
         fetch(url, requestOptions)
             .then(response => response.json())
             .then(data => {
                 console.log("la respuesta ", data)
                 //if(!this.state.esEdicionFormulario){
                     this.setState({ idEditando: data.id })
                     this.enviaFotografia(data.id)
                     this.muestraFormulario2(data.id)
                 //}
                 
             });
     }


    guardaElementoNuevo(url, json, verboHTTP) {
       // this.setState({ mostrarTablaParametrosReclutamiento: false })
        console.log("la url al persistir ", url)
        console.log("el json al persistir ", json)


        const requestOptions = {
            method: verboHTTP,
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };
        fetch(url, requestOptions)
            .then(response => response.json())
            .then(data => {
                console.log("la respuesta ", data)
                //if(!this.state.esEdicionFormulario){
                    this.setState({ idEditando: data.id })
                    this.enviaFotografia(data.id)
                   // this.muestraFormulario2(data.id)
                //}
                
            });
    }

    render() {
        return (

            <div className="page-content" style={{ backgroundColor: '#f1f3fa', height: '100%', width: '100%' }}>
                <div className="content-wrapper">
                    <div className="content-inner">

                        {
                            (this.state.muestraFormularioBandera == true || this.state.muestraFormularioBandera2 == true || this.state.muestraFormularioBandera3 == true) && !this.state.muestraTabla ?
                                <BarraAdminEmpleadosAPS
                                    muestraFormulario={this.muestraFormulario1}
                                    idEditando={this.state.idEditando}
                                    muestraFormulario3={this.muestraFormulario3}
                                    muestraFormulario2={this.muestraFormulario2}
                                    muestraTabla={this.botonCancelar}
                                    muestraFormularioBandera={this.state.muestraFormularioBandera}
                                    muestraFormularioBandera2={this.state.muestraFormularioBandera2}
                                    muestraFormularioBandera3={this.state.muestraFormularioBandera3}
                                    jsonPrimerFormulario={this.state.jsonPrimerFormulario}
                                    enviaPrimerFormulario={this.enviaPrimerFormulario}
                                    enviaSegundoFormulario={this.enviaSegundoFormulario}
                                    esEdicion={this.state.esEdicionFormulario} /> : ''
                        }



                        <div className="content justify-content-center align-items-center">

                            {
                                this.state.muestraTabla &&
                                <Tabla
                                    url={datos.urlServicePy+'parametros/api_vcat_empleados/0'}
                                    tituloTabla={'Administrar empleados APS'}
                                    muestraFormulario={this.muestraFormulario1} modalNombre={""} colorAgregar="#ED6C26" nonModal={true} />
                            }

                            {
                                this.state.muestraFormularioBandera && !this.state.muestraTabla &&
                                <FormAdminEmpleadosAPS guardaFotografia={this.guardaFotografia} guardaFormulario={this.guardaFormulario} contenidoEdicion={this.state.contenidoEdicionPrimerForm} esEdicion={this.state.esEdicionFormulario} />
                            }

                            {
                                !this.state.muestraFormularioBandera && !this.state.muestraTabla && this.state.muestraFormularioBandera2 &&
                                <FormAdminEmpleadosAPS2   guardaArchivo={this.guardaArchivo} idEditando={this.state.idEditando} enviaSegundoFormulario={this.enviaSegundoFormulario} contenidoEdicion={this.state.contenidoEdicionSegundoForm} guardaFormulario2={this.guardaFormulario2} esEdicion={this.state.esEdicionFormulario} />
                            }

                            {
                                !this.state.muestraFormularioBandera  && !this.state.muestraTabla && !this.state.muestraFormularioBandera2 && this.state.muestraFormularioBandera3 &&
                                <FormAdminEmpleadosAPS3 />
                            }


                        </div>
                    </div>
                </div>
            </div>


        )
    }

}