import React, { Component } from "react";
import Tabla from "../tablas/Tabla";
import Modal from '../modal/Modal'
import datos from '../urls/datos.json'

const queryParams = new URLSearchParams(window.location.search)

export default class CursoCedulaA extends Component {

    constructor() {
        super();
        this.guardaElementoNuevo=this.guardaElementoNuevo.bind(this)

        this.state = {
            mostrarTablaCursosCedulaA:true,
            mostrarModalParametrosReclutamiento:false,
            idUsuario:0
        };
    }


    UNSAFE_componentWillMount() {
        let idUsuario=queryParams.get("idUsuario")
        this.setState({idUsuario:idUsuario})

    }

    guardaElementoNuevo(url,json,verboHTTP){
        this.setState({mostrarTablaCursosCedulaA:false})
        console.log("la url al persistir ",url) 
        console.log("el json al persistir ",json) 


        const requestOptions = {
            method: verboHTTP,
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };
        fetch(url, requestOptions)
            .then(response => response.json())
            .then(data =>{
                console.log("la respuesta ",data)
                this.setState({mostrarTablaCursosCedulaA:true})
            });
    }


    render() {
        return (

            <div className="page-content" style={{ backgroundColor: '#f1f3fa', height: '100%', width: '100%' }}>
                <div className="content-wrapper">
                    <div className="content-inner">
                        <div className="content justify-content-center align-items-center">   
                          {
                              this.state.mostrarTablaCursosCedulaA ?  
                              <div>
                              <Tabla   url={datos.urlServicePy+'parametros/api_cat_curso_cedula/0'} tituloTabla={'Cursos Cédula A de GNP disponibles'}  tabla="api_cat_curso_cedula" idUsuario={this.state.idUsuario}
                              guardaElementoNuevo={this.guardaElementoNuevo} colorAgregar="#ED6C26" mostrarModal={this.mostrarModalParametrosReclutamiento}  modalNombre="modal-CursoCedulaA" modalNombreEdicion="modal-CursoCedulaAEdicion" />
                              <Modal modalNombre='modal-CursoCedulaA' guardaElementoNuevo={this.guardaElementoNuevo}  idUsuario={this.state.idUsuario} />
                              </div>
                              :''
                        }
                        </div>
                    </div>
                </div>
            </div>

        )

    }



}