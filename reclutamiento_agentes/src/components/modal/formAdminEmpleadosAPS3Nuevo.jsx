import { toHaveFocus } from "@testing-library/jest-dom/dist/matchers";
import React, { Component } from "react";
import datos from '../urls/datos.json'

//const avatar = require("../imagenes/avatar.png")
export default class FormAdminEmpleadosAPS3Nuevo extends Component {

    constructor() {
        super();
        this.inyectaBeneficiarioSeguro = this.inyectaBeneficiarioSeguro.bind(this)
        this.inyectaBeneficiarioFondo = this.inyectaBeneficiarioFondo.bind(this)
        this.onChange = this.onChange.bind(this)
        this.clickFinalizar = this.clickFinalizar.bind(this)
        this.onChangeFondo=this.onChangeFondo.bind(this)
        this.changeBeneficiario=this.changeBeneficiario.bind(this)
        this.state = {
            totalProcentajeSeguro:0,
            actvaBotonFinalizar:true,
            activaModal: true,
            contadorBeneficiariosSeguro: 1,
            contadorBeneficiariosFondo: 1,
            selectParentesco1: [],
            selectParentesco2: [],
            selectParentesco3: [],
            banParentesco: false,
            banFondo: false,

            beneficiarioSeguro1Edicion: false,
            beneficiarioSeguro2Edicion: false,
            beneficiarioSeguro3Edcion: false,

            beneficiarioEliminado2 : false,
            beneficiarioEliminado3 : false,

            beneficiarioFonEliminado2 : false,
            beneficiarioFonEliminado3 : false,


            beneficiarioSeguro1: {},
            beneficiarioSeguro2: {},
            beneficiarioSeguro3: {},


            beneficiarioFondo1Edicion: false,
            beneficiarioFondo2Edicion: false,
            beneficiarioFondo3Edicion: false,

            beneficiarioFondo1: {},
            beneficiarioFondo2: {},
            beneficiarioFondo3: {},

            porcentaje1: 0,
            porcentaje2: 0,
            porcentaje3: 0

        }

    }


    onChange = e => {
        let campoBeneficiario = e.target.name
        let beneficiarioCampo = campoBeneficiario.split('%')[0]
        let beneficiarioNumero = campoBeneficiario.split('%')[1]
        console.log("beneficiario ", beneficiarioNumero, e.target.value)
        if (e.target.name == 'selectParentesco1') {
            let beneficiarioSeguro1 = this.state.beneficiarioSeguro1
            beneficiarioSeguro1.parentesco_id = e.target.value
            this.setState({ beneficiarioSeguro1: beneficiarioSeguro1 })
            if(e.target.value == 0){
                this.setState({ banParentesco : true });
            }else{
                this.setState({ banParentesco : false });
            }
            console.log("parentesc change :::"+this.state.banParentesco);
        }
        if (e.target.name == 'selectParentesco2') {
            let beneficiarioSeguro2 = this.state.beneficiarioSeguro2
            beneficiarioSeguro2.parentesco_id = e.target.value
            this.setState({ beneficiarioSeguro2: beneficiarioSeguro2 })
        }
        if (e.target.name == 'selectParentesco3') {
            let beneficiarioSeguro3 = this.state.beneficiarioSeguro3
            beneficiarioSeguro3.parentesco_id = e.target.value
            this.setState({ beneficiarioSeguro3: beneficiarioSeguro3 })
        }


        if (beneficiarioNumero == '1') {
            let beneficiarioSeguro1 = this.state.beneficiarioSeguro1
            beneficiarioSeguro1["" + beneficiarioCampo + ""] = e.target.value
           
            this.setState({ beneficiarioSeguro1: beneficiarioSeguro1 })



        }
        if (beneficiarioNumero == '2') {
            let beneficiarioSeguro2 = this.state.beneficiarioSeguro2
            beneficiarioSeguro2["" + beneficiarioCampo + ""] = e.target.value
            
            this.setState({ beneficiarioSeguro2: beneficiarioSeguro2 })

        }
        if (beneficiarioNumero == '3') {
            let beneficiarioSeguro3 = this.state.beneficiarioSeguro3
            beneficiarioSeguro3["" + beneficiarioCampo + ""] = e.target.value
            this.setState({ beneficiarioSeguro3: beneficiarioSeguro3 })
        }

        console.log("name :::"+e.target.name);
        if(e.target.name == 'porcentaje%1'){
            let valorporcentaje1 =e.target.value
            if(valorporcentaje1.length ==3){
                this.setState({porcentaje1:valorporcentaje1})
            }
            if(valorporcentaje1.length > 3){
                this.state.beneficiarioSeguro1.porcentaje = this.state.porcentaje1
            }
        }
        if(e.target.name == 'porcentaje%2'){
            let valorporcentaje2 =e.target.value
            if(valorporcentaje2.length ==3){
                this.setState({porcentaje2:valorporcentaje2})
            }
            if(valorporcentaje2.length > 3){
                this.state.beneficiarioSeguro2.porcentaje = this.state.porcentaje2
            }
        }
        if(e.target.name == 'porcentaje%3'){
            let valorporcentaje3 =e.target.value
            if(valorporcentaje3.length ==3){
                this.setState({porcentaje3:valorporcentaje3})
            }
            if(valorporcentaje3.length > 3){
                this.state.beneficiarioSeguro3.porcentaje = this.state.porcentaje3
            }
        }
        console.log("valor  ",e.target.value)
        if(  beneficiarioCampo =='porcentaje' ){
            if(e.target.value.length > 0 ){
                let porcentaje1=this.state.beneficiarioSeguro1.porcentaje
                let porcentaje2=this.state.beneficiarioSeguro2.porcentaje
                let porcentaje3=this.state.beneficiarioSeguro3.porcentaje
                console.log("porcentaje 1",porcentaje1)
                
                if(porcentaje1 != undefined && porcentaje2 == undefined && porcentaje3 == undefined){
                    if(parseInt(porcentaje1) == 100){
                        this.setState({actvaBotonFinalizar:false})
                        this.setState({activaModal:false})
                    }else{
                        this.setState({actvaBotonFinalizar:true})
                        this.setState({activaModal:true})
                    }
                }else if(porcentaje1 != undefined && porcentaje2 != undefined && porcentaje3 == undefined){
                    let total=parseInt(porcentaje1) +parseInt(porcentaje2)
                    if(total == 100){
                        this.setState({actvaBotonFinalizar:false})
                        this.setState({activaModal:false})
                    }else{
                        this.setState({actvaBotonFinalizar:true})
                        this.setState({activaModal:true})
                    }
                }else if(porcentaje1 != undefined && porcentaje2 != undefined && porcentaje3 != undefined){
                    let total=parseInt(porcentaje1) +parseInt(porcentaje2)+parseInt(porcentaje3)
                    if(total == 100){
                        this.setState({actvaBotonFinalizar:false})
                        this.setState({activaModal:false})
                    }else{
                        this.setState({actvaBotonFinalizar:true})
                        this.setState({activaModal:true})
                    }
                }



                console.log("porcentaje 2",porcentaje2)
                console.log("porcentaje 3",porcentaje3)
            }else{
                this.setState({actvaBotonFinalizar:true})

            }

        }

    }


    onChangeFondo = e => {
        let campoBeneficiario = e.target.name
        let beneficiarioCampo = campoBeneficiario.split('%')[0]
        let beneficiarioNumero = campoBeneficiario.split('%')[1]
        console.log("beneficiario ", beneficiarioNumero, e.target.value)
        if (beneficiarioNumero == '1') {
            let beneficiarioFondo1 = this.state.beneficiarioFondo1
            beneficiarioFondo1["" + beneficiarioCampo + ""] = e.target.value
            if(beneficiarioFondo1.nombre == "" || beneficiarioFondo1.nombre == null || beneficiarioFondo1.ape_paterno == "" || beneficiarioFondo1.ape_paterno == null){
                this.setState({ banFondo : true })
            }else{
                this.setState({ banFondo : false })
            }
            this.setState({ beneficiarioFondo1: beneficiarioFondo1 })
        }
        if (beneficiarioNumero == '2') {
            let beneficiarioFondo2 = this.state.beneficiarioFondo2
            beneficiarioFondo2["" + beneficiarioCampo + ""] = e.target.value
            if(beneficiarioFondo2.nombre == "" || beneficiarioFondo2.nombre == null || beneficiarioFondo2.ape_paterno == "" || beneficiarioFondo2.ape_paterno == null ){
                this.setState({ banFondo : true })
            }else{
                this.setState({ banFondo : false })
            }
            this.setState({ beneficiarioFondo2: beneficiarioFondo2 })

        }
        if (beneficiarioNumero == '3') {
            let beneficiarioFondo3 = this.state.beneficiarioFondo3
            beneficiarioFondo3["" + beneficiarioCampo + ""] = e.target.value
            if(beneficiarioFondo3.nombre == "" || beneficiarioFondo3.nombre == null || beneficiarioFondo3.ape_paterno == "" || beneficiarioFondo3.ape_paterno == null){
                this.setState({ banFondo : true })
            }else{
                this.setState({ banFondo : false })
            }
            this.setState({ beneficiarioFondo3: beneficiarioFondo3 })
        }


    }

    onBlur = e => {
        this.modalPorcentaje.click()
        console.log("EntradaonBlur ::: ");
    }

    changeBeneficiario() {
        let porcentaje1=this.state.beneficiarioSeguro1.porcentaje
        let porcentaje2=this.state.beneficiarioSeguro2.porcentaje
        let porcentaje3=this.state.beneficiarioSeguro3.porcentaje
        console.log("porcentaje 1",porcentaje1)
        
        if(porcentaje1 != undefined && porcentaje2 == undefined && porcentaje3 == undefined){
            if(parseInt(porcentaje1) == 100){
                this.setState({actvaBotonFinalizar:false})
                this.setState({activaModal:false})
            }else{
                this.setState({actvaBotonFinalizar:true})
                this.setState({activaModal:true})
            }
        }else if(porcentaje1 != undefined && porcentaje2 != undefined && porcentaje3 == undefined){
            let total=parseInt(porcentaje1) +parseInt(porcentaje2)
            if(total == 100){
                this.setState({actvaBotonFinalizar:false})
                this.setState({activaModal:false})
            }else{
                this.setState({actvaBotonFinalizar:true})
                this.setState({activaModal:true})
            }
        }else if(porcentaje1 != undefined && porcentaje2 != undefined && porcentaje3 != undefined){
            let total=parseInt(porcentaje1) +parseInt(porcentaje2)+parseInt(porcentaje3)
            if(total == 100){
                this.setState({actvaBotonFinalizar:false})
                this.setState({activaModal:false})
            }else{
                this.setState({actvaBotonFinalizar:true})
                this.setState({activaModal:true})
            }
        }
        /*if(this.state.beneficiarioSeguro2Edicion){
            if(this.state.beneficiarioSeguro3.nombre != ""){
                this.setState({actvaBotonFinalizar:false})
            }
        }

        if(this.state.beneficiarioSeguro3Edcion){
            if(this.state.beneficiarioSeguro3.nombre != ""){
                this.setState({actvaBotonFinalizar:true})
            }
        }*/

    }

    UNSAFE_componentWillMount() {

        console.log("los props ", this.props)

        fetch(datos.urlServicePy+"parametros/api_cat_beneficiarios/" + this.props.idEmpleado)
            .then(response => response.json())
            .then(json => {
                console.log("json con la respues ", json.filas.length)
                if (json.filas.length > 0) {
                    this.setState({ contadorBeneficiariosSeguro: json.filas.length })
                    let columnas = json.columnas
                    let filaBeneSeguro1 = json.filas[0].fila != undefined ? json.filas[0].fila : undefined
                    let filaBeneSeguro2 = json.filas[1] != undefined ? json.filas[1].fila : undefined
                    let filaBeneSeguro3 = json.filas[2] != undefined ? json.filas[2].fila : undefined

                    let columnasSalidaBeneSeguro1 = {}
                    if (filaBeneSeguro1 != undefined) {
                        for (var i = 0; i < filaBeneSeguro1.length; i++) {
                            console.log(filaBeneSeguro1[i])
                            columnasSalidaBeneSeguro1["" + columnas[i].key + ""] = filaBeneSeguro1[i].value
                        }
                        this.setState({ beneficiarioSeguro1: columnasSalidaBeneSeguro1, beneficiarioSeguro1Edicion: true })
                        this.construyeSelect(datos.urlServicePy+"parametros/api_cat_parentesco/0", "selectParentesco1")

                    } else {
                        this.setState({ beneficiarioSeguro1: columnasSalidaBeneSeguro1, beneficiarioSeguro1Edicion: false })
                        this.construyeSelect(datos.urlServicePy+"parametros/api_cat_parentesco/0", "selectParentesco1")
                    }

                    let columnasSalidaBeneSeguro2 = {}
                    if (filaBeneSeguro2 != undefined) {
                        for (var i = 0; i < filaBeneSeguro2.length; i++) {
                            console.log(filaBeneSeguro2[i])
                            columnasSalidaBeneSeguro2["" + columnas[i].key + ""] = filaBeneSeguro2[i].value
                        }
                        this.construyeSelect(datos.urlServicePy+"parametros/api_cat_parentesco/0", "selectParentesco2")
                        this.setState({ beneficiarioSeguro2: columnasSalidaBeneSeguro2, beneficiarioSeguro2Edicion: true })
                    } else {
                        this.construyeSelect(datos.urlServicePy+"parametros/api_cat_parentesco/0", "selectParentesco2")
                        this.setState({ beneficiarioSeguro2: columnasSalidaBeneSeguro2, beneficiarioSeguro2Edicion: false })
                    }


                    let columnasSalidaBeneSeguro3 = {}
                    if (filaBeneSeguro3 != undefined) {
                        for (var i = 0; i < filaBeneSeguro3.length; i++) {
                            console.log(filaBeneSeguro3[i])
                            columnasSalidaBeneSeguro3["" + columnas[i].key + ""] = filaBeneSeguro3[i].value
                        }
                        this.construyeSelect(datos.urlServicePy+"parametros/api_cat_parentesco/0", "selectParentesco3")
                        this.setState({ beneficiarioSeguro3: columnasSalidaBeneSeguro3, beneficiarioSeguro3Edcion: true })
                    } else {
                        this.construyeSelect(datos.urlServicePy+"parametros/api_cat_parentesco/0", "selectParentesco3")
                        this.setState({ beneficiarioSeguro3: columnasSalidaBeneSeguro3, beneficiarioSeguro3Edcion: false })

                    }

                    let porcentaje1=this.state.beneficiarioSeguro1.porcentaje
                    let porcentaje2=this.state.beneficiarioSeguro2.porcentaje
                    let porcentaje3=this.state.beneficiarioSeguro3.porcentaje
                    console.log("porcentaje 1",porcentaje1)
                    
                    if(porcentaje1 != undefined && porcentaje2 == undefined && porcentaje3 == undefined){
                        if(parseInt(porcentaje1) == 100){
                            this.setState({actvaBotonFinalizar:false})
                            this.setState({activaModal:false})
                        }else{
                            this.setState({actvaBotonFinalizar:true})
                            this.setState({activaModal:true})
                        }
                    }else if(porcentaje1 != undefined && porcentaje2 != undefined && porcentaje3 == undefined){
                        let total=parseInt(porcentaje1) +parseInt(porcentaje2)
                        if(total == 100){
                            this.setState({actvaBotonFinalizar:false})
                            this.setState({activaModal:false})
                        }else{
                            this.setState({actvaBotonFinalizar:true})
                            this.setState({activaModal:true})
                        }
                    }else if(porcentaje1 != undefined && porcentaje2 != undefined && porcentaje3 != undefined){
                        let total=parseInt(porcentaje1) +parseInt(porcentaje2)+parseInt(porcentaje3)
                        if(total == 100){
                            this.setState({actvaBotonFinalizar:false})
                            this.setState({activaModal:false})
                        }else{
                            this.setState({actvaBotonFinalizar:true})
                            this.setState({activaModal:true})
                        }
                    }

                } else {
                    let beneficiarioSeguro1 =
                    {
                        "empleado_id": this.props.idEmpleado,
                        "fec_nacimiento": "",
                        "nombre": "",
                        "ape_paterno": "",
                        "ape_materno": null,
                        "parentesco_id": 0,
                        "especificar": "",
                        "porcentaje": 0
                    }
                    this.setState({ beneficiarioSeguro1: beneficiarioSeguro1, beneficiarioSeguro1Edicion: false })
                    this.construyeSelect(datos.urlServicePy+"parametros/api_cat_parentesco/0", "selectParentesco1")
                }
                if(this.state.selectParentesco1 ==null || this.state.selectParentesco1 == undefined || this.state.selectParentesco1 == 'undefined'){
                    this.setState({ banParentesco : true });
                    console.log("parentesc unsafe :::"+this.state.banParentesco);
                }
            })



        fetch(datos.urlServicePy+"parametros/api_cat_beneficiarios_fondo/" + this.props.idEmpleado)
            .then(response => response.json())
            .then(json => {
                console.log("json con la respues ", json.filas.length)
                if (json.filas.length > 0) {
                    this.setState({ contadorBeneficiariosFondo: json.filas.length })
                    let columnas = json.columnas
                    let filaBeneFondo1 = json.filas[0].fila != undefined ? json.filas[0].fila : undefined
                    let filaBeneFondo2 = json.filas[1] != undefined ? json.filas[1].fila : undefined
                    let filaBeneFondo3 = json.filas[2] != undefined ? json.filas[2].fila : undefined

                    let columnasSalidaBeneFondo1 = {}
                    if (filaBeneFondo1 != undefined) {
                        for (var i = 0; i < filaBeneFondo1.length; i++) {
                            console.log(filaBeneFondo1[i])
                            columnasSalidaBeneFondo1["" + columnas[i].key + ""] = filaBeneFondo1[i].value
                        }
                        this.setState({ beneficiarioFondo1: columnasSalidaBeneFondo1, beneficiarioFondo1Edicion: true })
                        this.construyeSelect(datos.urlServicePy+"parametros/api_cat_parentesco/0", "selectParentesco1")

                    } else {
                        this.setState({ beneficiarioFondo1: columnasSalidaBeneFondo1, beneficiarioFondo1Edicion: false })
                        this.construyeSelect(datos.urlServicePy+"parametros/api_cat_parentesco/0", "selectParentesco1")
                    }

                    let columnasSalidaBeneFondo2 = {}
                    if (filaBeneFondo2 != undefined) {
                        for (var i = 0; i < filaBeneFondo2.length; i++) {
                            console.log(filaBeneFondo2[i])
                            columnasSalidaBeneFondo2["" + columnas[i].key + ""] = filaBeneFondo2[i].value
                        }
                        this.setState({ beneficiarioFondo2: columnasSalidaBeneFondo2, beneficiarioFondo2Edicion: true })
                        this.construyeSelect(datos.urlServicePy+"parametros/api_cat_parentesco/0", "selectParentesco2")

                    } else {
                        this.setState({ beneficiarioFondo2: columnasSalidaBeneFondo2, beneficiarioFondo2Edicion: false })
                        this.construyeSelect(datos.urlServicePy+"parametros/api_cat_parentesco/0", "selectParentesco2")
                    }

                    let columnasSalidaBeneFondo3 = {}
                    if (filaBeneFondo3 != undefined) {
                        for (var i = 0; i < filaBeneFondo3.length; i++) {
                            console.log(filaBeneFondo3[i])
                            columnasSalidaBeneFondo3["" + columnas[i].key + ""] = filaBeneFondo3[i].value
                        }
                        this.setState({ beneficiarioFondo3: columnasSalidaBeneFondo3, beneficiarioFondo3Edicion: true })
                        this.construyeSelect(datos.urlServicePy+"parametros/api_cat_parentesco/0", "selectParentesco3")

                    } else {
                        this.setState({ beneficiarioFondo3: columnasSalidaBeneFondo3, beneficiarioFondo3Edicion: false })
                        this.construyeSelect(datos.urlServicePy+"parametros/api_cat_parentesco/0", "selectParentesco3")
                    }


                }
            })



    }


    construyeSelect(url, selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                console.log("respuesta de las ciudades ", json)
                for (var i = 0; i < filas.length; i++) {

                    let fila = filas[i]
                    if (selector == "selectParentesco1") {
                        if (this.props.idEmpleado) {

                            if (this.state.beneficiarioSeguro1.parentesco_id == fila.fila[0].value) {
                                options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            } else {
                                options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            }
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }

                    }
                    if (selector == "selectParentesco2") {
                        if (this.props.idEmpleado) {

                            if (this.state.beneficiarioSeguro2.parentesco_id == fila.fila[0].value) {
                                options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            } else {
                                options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            }
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }

                    }

                    if (selector == "selectParentesco3") {
                        if (this.props.idEmpleado) {

                            if (this.state.beneficiarioSeguro3.parentesco_id == fila.fila[0].value) {
                                options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            } else {
                                options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            }
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }

                    }

                }
                let salida = []
                let salida2 = []
                let salida3 = []

                salida.push(<select disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name={selector} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
                if(this.state.beneficiarioSeguro2.id == null && this.state.beneficiarioSeguro3.id == null){
                    salida2.push(<select disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                    && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                    && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name={"selectParentesco2"} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
                    salida3.push(<select disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                    && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                    && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name={"selectParentesco3"} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
                    if(selector != "selectParentesco2" || selector != "selectParentesco3"){
                        this.setState({ [selector]: salida ,selectParentesco2: salida2,selectParentesco3: salida3})
                    }
                }
                else if(this.state.beneficiarioSeguro3.id == null){
                    salida3.push(<select disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                    && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                    && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name={"selectParentesco3"} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
                    this.setState({ [selector]: salida ,selectParentesco3: salida3})
                    if(selector != "selectParentesco3"){
                        this.setState({ [selector]: salida ,selectParentesco2: salida2,selectParentesco3: salida3})
                    }
                }else{
                    this.setState({ [selector]: salida })
                }
                this.setState({ [selector]: salida })
            });
    }

    inyectaBeneficiarioSeguro(banInyecta) {
        console.log("inyeccion")

        let contBeneficiariosSeguro = this.state.contadorBeneficiariosSeguro
        if(contBeneficiariosSeguro >= 1 && contBeneficiariosSeguro < 3){
            if(banInyecta){
                contBeneficiariosSeguro = this.state.contadorBeneficiariosSeguro + 1
                let id = "beneficiarioSeguro" + contBeneficiariosSeguro
                this.setState({ contadorBeneficiariosSeguro: contBeneficiariosSeguro,actvaBotonFinalizar:true })
                if(this.state.beneficiarioEliminado2){
                    this.setState({ beneficiarioEliminado2 : false })
                }
                if(this.state.beneficiarioEliminado3){
                    this.setState({ beneficiarioEliminado3 : false })
                }
            }else{
                if(contBeneficiariosSeguro > 1){
                    contBeneficiariosSeguro = this.state.contadorBeneficiariosSeguro - 1
                    let id = "beneficiarioSeguro" + contBeneficiariosSeguro
                    this.setState({ contadorBeneficiariosSeguro: contBeneficiariosSeguro,actvaBotonFinalizar:false, beneficiarioEliminado2 : true , beneficiarioSeguro2Edicion : false })
                    this.state.beneficiarioSeguro2.porcentaje = undefined
                    this.state.beneficiarioSeguro2.nombre = ""
                    this.state.beneficiarioSeguro2.ape_paterno = ""
                    this.state.beneficiarioSeguro2.ape_materno = null
                    this.state.beneficiarioSeguro2.fec_nacimiento = ""
                    this.state.beneficiarioSeguro2.parentesco_id = 0
                    this.eliminarBeneficiarioclick.click()
                }
            }
        }else{
            if(contBeneficiariosSeguro == 3 && !banInyecta){
                contBeneficiariosSeguro = this.state.contadorBeneficiariosSeguro - 1
                let id = "beneficiarioSeguro" + contBeneficiariosSeguro
                this.setState({ contadorBeneficiariosSeguro: contBeneficiariosSeguro,actvaBotonFinalizar:false, beneficiarioEliminado3 : true , beneficiarioSeguro3Edicion : false})
                this.state.beneficiarioSeguro3.porcentaje = undefined
                this.state.beneficiarioSeguro3.nombre = ""
                this.state.beneficiarioSeguro3.ape_paterno = ""
                this.state.beneficiarioSeguro3.ape_materno = null
                this.state.beneficiarioSeguro3.fec_nacimiento = ""
                this.state.beneficiarioSeguro3.parentesco_id = 0
                this.eliminarBeneficiarioclick.click()
            }
        }
    }

    inyectaBeneficiarioFondo(banInyecta) {
        console.log("inyeccion")
        let contBeneficiariosFondo = this.state.contadorBeneficiariosFondo

        if(contBeneficiariosFondo >= 1 && contBeneficiariosFondo < 3){
            if(banInyecta){
                contBeneficiariosFondo = this.state.contadorBeneficiariosFondo + 1
                let id = "beneficiarioSeguro" + contBeneficiariosFondo
                this.setState({ contadorBeneficiariosFondo: contBeneficiariosFondo ,banFondo : true})
                if(this.state.beneficiarioFonEliminado2){
                    this.setState({ beneficiarioFonEliminado2 : false })
                }
                if(this.state.beneficiarioFonEliminado3){
                    this.setState({ beneficiarioFonEliminado3 : false })
                }
            }else{
                if(contBeneficiariosFondo > 1){
                    let id = "beneficiarioSeguro" + contBeneficiariosFondo
                    contBeneficiariosFondo = this.state.contadorBeneficiariosFondo - 1
                    this.setState({ contadorBeneficiariosFondo: contBeneficiariosFondo , actvaBotonFinalizar:false , beneficiarioFonEliminado2 : true, banFondo : false, beneficiarioFondo2Edicion: false})
                    this.state.beneficiarioFondo2.nombre = ""
                    this.state.beneficiarioFondo2.ape_paterno = ""
                    this.state.beneficiarioFondo2.ape_materno = ""
                }
            }
        }else{
            if(contBeneficiariosFondo == 3 && !banInyecta){
                contBeneficiariosFondo = this.state.contadorBeneficiariosFondo - 1
                let id = "beneficiarioSeguro" + contBeneficiariosFondo
                this.setState({ contadorBeneficiariosFondo: contBeneficiariosFondo, actvaBotonFinalizar:false, beneficiarioFonEliminado3 : true, banFondo : false, beneficiarioFondo3Edicion: false})
                this.state.beneficiarioFondo3.nombre = ""
                this.state.beneficiarioFondo3.ape_paterno = ""
                this.state.beneficiarioFondo3.ape_materno = ""
            }
        }

    }

    clickFinalizar(determinaclick) {
        console.log(this.state)

        if (this.props.idEmpleado != null) {

            if (JSON.stringify(this.state.beneficiarioSeguro1) != '{}') {

                if (this.state.beneficiarioSeguro1Edicion) {
                    this.state.beneficiarioSeguro1.ape_materno = this.state.beneficiarioSeguro1.ape_materno == "" ? null : this.state.beneficiarioSeguro1.ape_materno;
                    const requestOptions = {
                        method: "PUT",
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify(this.state.beneficiarioSeguro1)
                    };

                    fetch(datos.urlServicePy+"parametros_upd/api_cat_beneficiarios/" + this.state.beneficiarioSeguro1.id, requestOptions)
                        .then(response => response.json())
                        .then(data => { })
                } else {
                    //   this.state.beneficiarioSeguro1.parentesco_id = 5
                    this.state.beneficiarioSeguro1.empleado_id = this.props.idEmpleado
                    this.state.beneficiarioSeguro1.ape_materno = this.state.beneficiarioSeguro1.ape_materno == "" ? null : this.state.beneficiarioSeguro1.ape_materno;
                    this.state.beneficiarioSeguro1.especificar = "especificar"
                    const requestOptions = {
                        method: "POST",
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify(this.state.beneficiarioSeguro1)
                    };

                    fetch(datos.urlServicePy+"parametros/api_cat_beneficiarios/0", requestOptions)
                        .then(response => response.json())
                        .then(data => { })
                }

            }

            


            if (JSON.stringify(this.state.beneficiarioSeguro2) != '{}') {
                if(this.state.beneficiarioEliminado2){
                    if(this.state.beneficiarioSeguro2.id != undefined){
                        console.log("Implementacion para eliminar beneficiario 2");
                        const requestOptions = {
                            method: "DELETE",
                            headers: { 'Content-Type': 'application/json' }
                        };

                        fetch(datos.urlServicePy+"parametros_upd/api_cat_beneficiarios/"+this.state.beneficiarioSeguro2.id, requestOptions)
                            .then(response => response.json())
                            .then(data => { })
                    }
                }else{
                    if (this.state.beneficiarioSeguro2Edicion) {
                        this.state.beneficiarioSeguro2.ape_materno = this.state.beneficiarioSeguro2.ape_materno == "" ? null : this.state.beneficiarioSeguro2.ape_materno;
                        const requestOptions = {
                            method: "PUT",
                            headers: { 'Content-Type': 'application/json' },
                            body: JSON.stringify(this.state.beneficiarioSeguro2)
                        };
    
                        fetch(datos.urlServicePy+"parametros_upd/api_cat_beneficiarios/" + this.state.beneficiarioSeguro2.id, requestOptions)
                            .then(response => response.json())
                            .then(data => { })
    
                    } else {
                        //  this.state.beneficiarioSeguro2.parentesco_id = 5
                        this.state.beneficiarioSeguro2.empleado_id = this.props.idEmpleado
                        this.state.beneficiarioSeguro2.ape_materno = this.state.beneficiarioSeguro2.ape_materno == "" ? null : this.state.beneficiarioSeguro2.ape_materno;
                        this.state.beneficiarioSeguro2.especificar = "especificar"
                        const requestOptions = {
                            method: "POST",
                            headers: { 'Content-Type': 'application/json' },
                            body: JSON.stringify(this.state.beneficiarioSeguro2)
                        };
    
                        fetch(datos.urlServicePy+"parametros/api_cat_beneficiarios/0", requestOptions)
                            .then(response => response.json())
                            .then(data => { })
                    }
                }
            }

            if (JSON.stringify(this.state.beneficiarioSeguro3) != '{}') {
                if(this.state.beneficiarioEliminado3){
                    if(this.state.beneficiarioSeguro3.id != undefined){
                        console.log("Implementacion para eliminar beneficiario 3");
                        const requestOptions = {
                            method: "DELETE",
                            headers: { 'Content-Type': 'application/json' }
                        };

                        fetch(datos.urlServicePy+"parametros_upd/api_cat_beneficiarios/"+this.state.beneficiarioSeguro3.id, requestOptions)
                            .then(response => response.json())
                            .then(data => { })
                    }
                }else{
                    if (this.state.beneficiarioSeguro3Edcion && this.state.beneficiarioSeguro3.id != undefined) {
                        this.state.beneficiarioSeguro3.ape_materno = this.state.beneficiarioSeguro3.ape_materno == "" ? null : this.state.beneficiarioSeguro3.ape_materno;
                        const requestOptions = {
                            method: "PUT",
                            headers: { 'Content-Type': 'application/json' },
                            body: JSON.stringify(this.state.beneficiarioSeguro3)
                        };

                        fetch(datos.urlServicePy+"parametros_upd/api_cat_beneficiarios/" + this.state.beneficiarioSeguro3.id, requestOptions)
                            .then(response => response.json())
                            .then(data => { })

                    } else {
                        // this.state.beneficiarioSeguro3.parentesco_id = 5
                        this.state.beneficiarioSeguro3.empleado_id = this.props.idEmpleado
                        this.state.beneficiarioSeguro3.ape_materno = this.state.beneficiarioSeguro3.ape_materno == "" ? null : this.state.beneficiarioSeguro3.ape_materno;
                        this.state.beneficiarioSeguro3.especificar = "especificar"
                        const requestOptions = {
                            method: "POST",
                            headers: { 'Content-Type': 'application/json' },
                            body: JSON.stringify(this.state.beneficiarioSeguro3)
                        };

                        fetch(datos.urlServicePy+"parametros/api_cat_beneficiarios/0", requestOptions)
                            .then(response => response.json())
                            .then(data => { })

                    }
                }
            }

            /* benificiarios de fondo */
            if (JSON.stringify(this.state.beneficiarioFondo1) != '{}') {
                if (this.state.beneficiarioFondo1Edicion) {
                    this.state.beneficiarioFondo1.ape_materno = this.state.beneficiarioFondo1.ape_materno == "" ? null : this.state.beneficiarioFondo1.ape_materno;
                    const requestOptions = {
                        method: "PUT",
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify(this.state.beneficiarioFondo1)
                    };

                    fetch(datos.urlServicePy+"parametros_upd/api_cat_beneficiarios_fondo/" + this.state.beneficiarioFondo1.id, requestOptions)
                        .then(response => response.json())
                        .then(data => { })
                } else {
                    //   this.state.beneficiarioSeguro1.parentesco_id = 5
                    this.state.beneficiarioFondo1.empleado_id = this.props.idEmpleado
                    this.state.beneficiarioFondo1.ape_materno = this.state.beneficiarioFondo1.ape_materno == "" ? null : this.state.beneficiarioFondo1.ape_materno;
                 //   this.state.beneficiarioFondo1.especificar = "especificar"
                    const requestOptions = {
                        method: "POST",
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify(this.state.beneficiarioFondo1)
                    };

                    fetch(datos.urlServicePy+"parametros/api_cat_beneficiarios_fondo/0", requestOptions)
                        .then(response => response.json())
                        .then(data => { })
                }


            }


            if (JSON.stringify(this.state.beneficiarioFondo2) != '{}') {
                if(this.state.beneficiarioFonEliminado2){
                    if(this.state.beneficiarioFondo2.id != undefined){
                        console.log("Implementacion para eliminar beneficiario 3");
                        const requestOptions = {
                            method: "DELETE",
                            headers: { 'Content-Type': 'application/json' }
                        };

                        fetch(datos.urlServicePy+"parametros_upd/api_cat_beneficiarios_fondo/"+this.state.beneficiarioFondo2.id, requestOptions)
                            .then(response => response.json())
                            .then(data => { })
                    }
                }else{
                    if (this.state.beneficiarioFondo2Edicion) {
                        this.state.beneficiarioFondo2.ape_materno = this.state.beneficiarioFondo2.ape_materno == "" ? null : this.state.beneficiarioFondo2.ape_materno;
                        const requestOptions = {
                            method: "PUT",
                            headers: { 'Content-Type': 'application/json' },
                            body: JSON.stringify(this.state.beneficiarioFondo2)
                        };
                        fetch(datos.urlServicePy+"parametros_upd/api_cat_beneficiarios_fondo/" + this.state.beneficiarioFondo2.id, requestOptions)
                            .then(response => response.json())
                            .then(data => { })
                    } else {
                        //   this.state.beneficiarioSeguro1.parentesco_id = 5
                        this.state.beneficiarioFondo2.empleado_id = this.props.idEmpleado
                        this.state.beneficiarioFondo2.ape_materno = this.state.beneficiarioFondo2.ape_materno == "" ? null : this.state.beneficiarioFondo2.ape_materno;
                    // this.state.beneficiarioFondo2.especificar = "especificar"
                        const requestOptions = {
                            method: "POST",
                            headers: { 'Content-Type': 'application/json' },
                            body: JSON.stringify(this.state.beneficiarioFondo2)
                        };
                        fetch(datos.urlServicePy+"parametros/api_cat_beneficiarios_fondo/0", requestOptions)
                            .then(response => response.json())
                            .then(data => { })
                    }
                }
            }   

            if (JSON.stringify(this.state.beneficiarioFondo3) != '{}') {
                if(this.state.beneficiarioFonEliminado3){
                    if(this.state.beneficiarioFondo3.id != undefined){
                        console.log("Implementacion para eliminar beneficiario 3");
                        const requestOptions = {
                            method: "DELETE",
                            headers: { 'Content-Type': 'application/json' }
                        };

                        fetch(datos.urlServicePy+"parametros_upd/api_cat_beneficiarios_fondo/"+this.state.beneficiarioFondo3.id, requestOptions)
                            .then(response => response.json())
                            .then(data => { })
                    }
                }else{
                    if (this.state.beneficiarioFondo3Edicion) {
                        this.state.beneficiarioFondo3.ape_materno = this.state.beneficiarioFondo3.ape_materno == "" ? null : this.state.beneficiarioFondo3.ape_materno;
                        const requestOptions = {
                            method: "PUT",
                            headers: { 'Content-Type': 'application/json' },
                            body: JSON.stringify(this.state.beneficiarioFondo3)
                        };

                        fetch(datos.urlServicePy+"parametros_upd/api_cat_beneficiarios_fondo/" + this.state.beneficiarioFondo3.id, requestOptions)
                            .then(response => response.json())
                            .then(data => { })
                    } else {
                        //   this.state.beneficiarioSeguro1.parentesco_id = 5
                        this.state.beneficiarioFondo3.empleado_id = this.props.idEmpleado
                        this.state.beneficiarioFondo3.ape_materno = this.state.beneficiarioFondo3.ape_materno == "" ? null : this.state.beneficiarioFondo3.ape_materno;
                    // this.state.beneficiarioFondo2.especificar = "especificar"
                        const requestOptions = {
                            method: "POST",
                            headers: { 'Content-Type': 'application/json' },
                            body: JSON.stringify(this.state.beneficiarioFondo3)
                        };

                        fetch(datos.urlServicePy+"parametros/api_cat_beneficiarios_fondo/0", requestOptions)
                            .then(response => response.json())
                            .then(data => { })
                    }
                }
            }




        }
        console.log("Entro en finalizar");
        if(determinaclick == "1"){
            this.props.muestraFormulario(this.props.idEmpleado)
        }else if(determinaclick == "2"){
            this.props.muestraFormulario2(this.props.idEmpleado)
        }else if(determinaclick == "3"){
            this.props.botonCancelar()
        } else {
            console.log("Entro en pre recarga y guardado");
        }
    }

    render() {
        return (
            <div>
                <div class="card">
                    <div class="row mb-2 mt-2 ml-1">
                        <div class="col-12 col-lg-9">
                            <div class="row">
                                <div class="col-6 col-lg-3 d-flex align-items-center pb-sm-1">
                                    <div class="mr-2">
                                        {
                                            this.props.idEmpleado == null || this.state.actvaBotonFinalizar || this.state.banFondo || this.state.beneficiarioFondo1.nombre == null || this.state.beneficiarioFondo1.ape_paterno == null ?
                                        <a href="#" class="btn btn-light rounded-pill btn-icon btn-sm disabled ">
                                            <span class="letter-icon">1</span>
                                        </a>
                                        :
                                        <a href="#"
                                            class="btn btn-primary rounded-pill btn-icon btn-sm"> <span
                                                class="letter-icon" onClick={() => this.clickFinalizar("1")} >1 </span>
                                        </a>
                                        }
                                    </div>

                                    <div>
                                    {
                                        this.props.idEmpleado == null  || this.state.actvaBotonFinalizar || this.state.banFondo || this.state.beneficiarioFondo1.nombre == null || this.state.beneficiarioFondo1.ape_paterno == null ?
                                    <a href="#" class="text-body font-weight-semibold  letter-icon-title text-muted ">Información general</a>
                                    :
                                        <a href="#" class="text-body font-weight-semibold letter-icon-title" onClick={() => this.clickFinalizar("1")}>Información general</a>
                                    }
                                    </div>
                                </div>


                                <div class="col-6 col-lg-3 d-flex align-items-center pb-sm-1">
                                    <div class="mr-2">

                                        {
                                            this.props.idEmpleado == null || this.state.actvaBotonFinalizar || this.state.banFondo || this.state.beneficiarioFondo1.nombre == null || this.state.beneficiarioFondo1.ape_paterno == null ?
                                                <a href="#" class="btn btn-light rounded-pill btn-icon btn-sm disabled ">
                                                    <span class="letter-icon">2</span>
                                                </a>
                                                :
                                                <a href="#" onClick={() => this.clickFinalizar("2")}
                                                    class="btn btn-primary rounded-pill btn-icon btn-sm"> <span
                                                        class="letter-icon">2</span>
                                                </a>
                                        }
                                    </div>
                                    <div>

                                        {
                                            this.props.idEmpleado == null  || this.state.actvaBotonFinalizar || this.state.banFondo || this.state.beneficiarioFondo1.nombre == null || this.state.beneficiarioFondo1.ape_paterno == null ?
                                                <a href="#" class="text-body font-weight-semibold  letter-icon-title text-muted ">Información laboral</a>
                                                :
                                                <a href="#" onClick={() => this.clickFinalizar("2")} class="text-body font-weight-semibold letter-icon-title">Información laboral</a>

                                        }

                                    </div>
                                </div>




                                <div class="col-6 col-lg-3 d-flex align-items-center pb-sm-1">
                                    <div class="mr-2">

                                        {
                                            this.props.idEmpleado == null ?
                                                <a href="#" class="btn btn-light rounded-pill btn-icon btn-sm disabled">
                                                    <span class="letter-icon">3</span>
                                                </a>
                                                :
                                                <a href="#" onClick={() => this.clickFinalizar("4")} 
                                                    class="btn btn-primary rounded-pill btn-icon btn-sm"> <span
                                                        class="letter-icon">3</span>
                                                </a>

                                        }


                                    </div>
                                    <div>
                                        {
                                            this.props.idEmpleado == null ?
                                                <a href="#" class="text-body font-weight-semibold  letter-icon-title text-muted">Beneficiarios</a>
                                                :
                                                <a href="#" onClick={() => this.clickFinalizar("4")}  class="text-body font-weight-semibold letter-icon-title">Beneficiarios</a>

                                        }

                                    </div>
                                </div>




                            </div>
                        </div>
                        <div class="col-12 col-lg-3">

                            <div class="float-right mr-3">

                                <button type="button" class="btn btn-light mr-1" onClick={(e) => this.props.botonCancelar()}>Cancelar</button>
                            {
                                (this.state.contadorBeneficiariosSeguro == 0 || this.state.contadorBeneficiariosSeguro == 1) && (this.state.contadorBeneficiariosFondo == 0 || this.state.contadorBeneficiariosFondo == 1)? 
                                <button type="button" class="btn btn-warning"
                                    style={{ backgroundColor: '#ED6C26' }} id="siguiente"

                                    disabled={
                                        this.state.actvaBotonFinalizar || this.state.beneficiarioSeguro1.nombre == "" || this.state.beneficiarioSeguro1.nombre == null || this.state.beneficiarioSeguro1.ape_paterno == "" || this.state.beneficiarioSeguro1.ape_paterno == null
                                        || this.state.beneficiarioSeguro1.fec_nacimiento == "" || this.state.beneficiarioSeguro1.fec_nacimiento == null || this.state.banParentesco ||
                                        this.state.beneficiarioFondo1.nombre == "" || this.state.beneficiarioFondo1.nombre == null ||
                                        this.state.beneficiarioFondo1.ape_paterno == "" || this.state.beneficiarioFondo1.ape_paterno == null
                                    }

                                    onClick={() => this.clickFinalizar("3")} > Finalizar</button> : '' }
                            {
                                (this.state.contadorBeneficiariosSeguro == 0 || this.state.contadorBeneficiariosSeguro == 1) && (this.state.contadorBeneficiariosFondo == 2)? 
                                <button type="button" class="btn btn-warning"
                                    style={{ backgroundColor: '#ED6C26' }} id="siguiente"

                                    disabled={
                                        this.state.actvaBotonFinalizar || this.state.beneficiarioSeguro1.nombre == "" || this.state.beneficiarioSeguro1.nombre == null || this.state.beneficiarioSeguro1.ape_paterno == "" || this.state.beneficiarioSeguro1.ape_paterno == null
                                        || this.state.beneficiarioSeguro1.fec_nacimiento == "" || this.state.beneficiarioSeguro1.fec_nacimiento == null || this.state.banParentesco ||
                                        this.state.beneficiarioFondo1.nombre == "" || this.state.beneficiarioFondo1.nombre == null ||
                                        this.state.beneficiarioFondo1.ape_paterno == "" || this.state.beneficiarioFondo1.ape_paterno == null ||
                                        this.state.beneficiarioFondo2.nombre == "" || this.state.beneficiarioFondo2.nombre == null ||
                                        this.state.beneficiarioFondo2.ape_paterno == "" || this.state.beneficiarioFondo2.ape_paterno == null
                                    }

                                    onClick={() => this.clickFinalizar("3")} > Finalizar</button> : '' }
                            {
                                (this.state.contadorBeneficiariosSeguro == 0 || this.state.contadorBeneficiariosSeguro == 1) && (this.state.contadorBeneficiariosFondo == 3)? 
                                <button type="button" class="btn btn-warning"
                                    style={{ backgroundColor: '#ED6C26' }} id="siguiente"

                                    disabled={
                                        this.state.actvaBotonFinalizar || this.state.beneficiarioSeguro1.nombre == "" || this.state.beneficiarioSeguro1.nombre == null || this.state.beneficiarioSeguro1.ape_paterno == "" || this.state.beneficiarioSeguro1.ape_paterno == null
                                        || this.state.beneficiarioSeguro1.fec_nacimiento == "" || this.state.beneficiarioSeguro1.fec_nacimiento == null || this.state.banParentesco ||
                                        this.state.beneficiarioFondo1.nombre == "" || this.state.beneficiarioFondo1.nombre == null ||
                                        this.state.beneficiarioFondo1.ape_paterno == "" || this.state.beneficiarioFondo1.ape_paterno == null ||
                                        this.state.beneficiarioFondo2.nombre == "" || this.state.beneficiarioFondo2.nombre == null ||
                                        this.state.beneficiarioFondo2.ape_paterno == "" || this.state.beneficiarioFondo2.ape_paterno == null    ||
                                        this.state.beneficiarioFondo3.nombre == "" || this.state.beneficiarioFondo3.nombre == null ||
                                        this.state.beneficiarioFondo3.ape_paterno == "" || this.state.beneficiarioFondo3.ape_paterno == null
                                    }

                                    onClick={() => this.clickFinalizar("3")} > Finalizar</button> : '' }
                            {
                                (this.state.contadorBeneficiariosSeguro == 2) && (this.state.contadorBeneficiariosFondo == 0 || this.state.contadorBeneficiariosFondo == 1) ? 
                                <button type="button" class="btn btn-warning"
                                    style={{ backgroundColor: '#ED6C26' }} id="siguiente"

                                    disabled={
                                        this.state.actvaBotonFinalizar || this.state.beneficiarioSeguro1.nombre == "" || this.state.beneficiarioSeguro1.nombre == null || this.state.beneficiarioSeguro1.ape_paterno == "" || this.state.beneficiarioSeguro1.ape_paterno == null
                                        || this.state.beneficiarioSeguro1.fec_nacimiento == "" || this.state.beneficiarioSeguro1.fec_nacimiento == null || this.state.banParentesco ||
                                        this.state.beneficiarioFondo1.nombre == "" || this.state.beneficiarioFondo1.nombre == null ||
                                        this.state.beneficiarioFondo1.ape_paterno == "" || this.state.beneficiarioFondo1.ape_paterno == null ||
                                        this.state.beneficiarioSeguro2.nombre == "" || this.state.beneficiarioSeguro2.nombre == null || this.state.beneficiarioSeguro2.ape_paterno == "" || this.state.beneficiarioSeguro2.ape_paterno == null
                                        || this.state.beneficiarioSeguro2.fec_nacimiento == "" || this.state.beneficiarioSeguro2.fec_nacimiento == null ||
                                        this.state.beneficiarioSeguro2.nombre == "" || this.state.beneficiarioSeguro2.nombre == null ||
                                        this.state.beneficiarioSeguro2.ape_paterno == "" || this.state.beneficiarioSeguro2.ape_paterno == null
                                    }

                                    onClick={() => this.clickFinalizar("3")} > Finalizar</button> : '' }
                            {
                                (this.state.contadorBeneficiariosSeguro == 2) && (this.state.contadorBeneficiariosFondo == 2) ? 
                                <button type="button" class="btn btn-warning"
                                    style={{ backgroundColor: '#ED6C26' }} id="siguiente"

                                    disabled={
                                        this.state.actvaBotonFinalizar || this.state.beneficiarioSeguro1.nombre == "" || this.state.beneficiarioSeguro1.nombre == null || this.state.beneficiarioSeguro1.ape_paterno == "" || this.state.beneficiarioSeguro1.ape_paterno == null
                                        || this.state.beneficiarioSeguro1.fec_nacimiento == "" || this.state.beneficiarioSeguro1.fec_nacimiento == null || this.state.banParentesco ||
                                        this.state.beneficiarioFondo1.nombre == "" || this.state.beneficiarioFondo1.nombre == null ||
                                        this.state.beneficiarioFondo1.ape_paterno == "" || this.state.beneficiarioFondo1.ape_paterno == null ||
                                        this.state.beneficiarioSeguro2.nombre == "" || this.state.beneficiarioSeguro2.nombre == null || this.state.beneficiarioSeguro2.ape_paterno == "" || this.state.beneficiarioSeguro2.ape_paterno == null
                                        || this.state.beneficiarioSeguro2.fec_nacimiento == "" || this.state.beneficiarioSeguro2.fec_nacimiento == null ||
                                        this.state.beneficiarioSeguro2.nombre == "" || this.state.beneficiarioSeguro2.nombre == null ||
                                        this.state.beneficiarioSeguro2.ape_paterno == "" || this.state.beneficiarioSeguro2.ape_paterno == null ||
                                        this.state.beneficiarioFondo2.nombre == "" || this.state.beneficiarioFondo2.nombre == null ||
                                        this.state.beneficiarioFondo2.ape_paterno == "" || this.state.beneficiarioFondo2.ape_paterno == null
                                    }

                                    onClick={() => this.clickFinalizar("3")} > Finalizar</button> : '' }
                            {
                                (this.state.contadorBeneficiariosSeguro == 2) && (this.state.contadorBeneficiariosFondo == 3) ? 
                                <button type="button" class="btn btn-warning"
                                    style={{ backgroundColor: '#ED6C26' }} id="siguiente"

                                    disabled={
                                        this.state.actvaBotonFinalizar || this.state.beneficiarioSeguro1.nombre == "" || this.state.beneficiarioSeguro1.nombre == null || this.state.beneficiarioSeguro1.ape_paterno == "" || this.state.beneficiarioSeguro1.ape_paterno == null
                                        || this.state.beneficiarioSeguro1.fec_nacimiento == "" || this.state.beneficiarioSeguro1.fec_nacimiento == null || this.state.banParentesco ||
                                        this.state.beneficiarioFondo1.nombre == "" || this.state.beneficiarioFondo1.nombre == null ||
                                        this.state.beneficiarioFondo1.ape_paterno == "" || this.state.beneficiarioFondo1.ape_paterno == null ||
                                        this.state.beneficiarioSeguro2.nombre == "" || this.state.beneficiarioSeguro2.nombre == null || this.state.beneficiarioSeguro2.ape_paterno == "" || this.state.beneficiarioSeguro2.ape_paterno == null
                                        || this.state.beneficiarioSeguro2.fec_nacimiento == "" || this.state.beneficiarioSeguro2.fec_nacimiento == null ||
                                        this.state.beneficiarioSeguro2.nombre == "" || this.state.beneficiarioSeguro2.nombre == null ||
                                        this.state.beneficiarioSeguro2.ape_paterno == "" || this.state.beneficiarioSeguro2.ape_paterno == null ||
                                        this.state.beneficiarioFondo2.nombre == "" || this.state.beneficiarioFondo2.nombre == null ||
                                        this.state.beneficiarioFondo2.ape_paterno == "" || this.state.beneficiarioFondo2.ape_paterno == null  ||
                                        this.state.beneficiarioFondo3.nombre == "" || this.state.beneficiarioFondo3.nombre == null ||
                                        this.state.beneficiarioFondo3.ape_paterno == "" || this.state.beneficiarioFondo3.ape_paterno == null
                                    }

                                    onClick={() => this.clickFinalizar("3")} > Finalizar</button> : '' }
                            {
                                this.state.contadorBeneficiariosSeguro == 3 && (this.state.contadorBeneficiariosFondo == 0 || this.state.contadorBeneficiariosFondo == 1) ?
                                <button type="button" class="btn btn-warning"
                                    style={{ backgroundColor: '#ED6C26' }} id="siguiente"

                                    disabled={
                                        this.state.actvaBotonFinalizar || this.state.beneficiarioSeguro1.nombre == "" || this.state.beneficiarioSeguro1.nombre == null || this.state.beneficiarioSeguro1.ape_paterno == "" || this.state.beneficiarioSeguro1.ape_paterno == null
                                        || this.state.beneficiarioSeguro1.fec_nacimiento == "" || this.state.beneficiarioSeguro1.fec_nacimiento == null || this.state.banParentesco ||
                                        this.state.beneficiarioFondo1.nombre == "" || this.state.beneficiarioFondo1.nombre == null ||
                                        this.state.beneficiarioFondo1.ape_paterno == "" || this.state.beneficiarioFondo1.ape_paterno == null ||
                                        this.state.beneficiarioSeguro2.nombre == "" || this.state.beneficiarioSeguro2.nombre == null || this.state.beneficiarioSeguro2.ape_paterno == "" || this.state.beneficiarioSeguro2.ape_paterno == null
                                        || this.state.beneficiarioSeguro2.fec_nacimiento == "" || this.state.beneficiarioSeguro2.fec_nacimiento == null ||
                                        this.state.beneficiarioSeguro2.nombre == "" || this.state.beneficiarioSeguro2.nombre == null ||
                                        this.state.beneficiarioSeguro2.ape_paterno == "" || this.state.beneficiarioSeguro2.ape_paterno == null ||
                                        this.state.beneficiarioSeguro3.nombre == "" || this.state.beneficiarioSeguro3.nombre == null || this.state.beneficiarioSeguro3.ape_paterno == "" || this.state.beneficiarioSeguro3.ape_paterno == null
                                        || this.state.beneficiarioSeguro3.fec_nacimiento == "" || this.state.beneficiarioSeguro3.fec_nacimiento == null ||
                                        this.state.beneficiarioSeguro3.nombre == "" || this.state.beneficiarioSeguro3.nombre == null ||
                                        this.state.beneficiarioSeguro3.ape_paterno == "" || this.state.beneficiarioSeguro3.ape_paterno == null
                                    }
                                    

                                    onClick={() => this.clickFinalizar("3")} > Finalizar</button> : '' }
                            {
                                this.state.contadorBeneficiariosSeguro == 3 && (this.state.contadorBeneficiariosFondo == 2) ?
                                <button type="button" class="btn btn-warning"
                                    style={{ backgroundColor: '#ED6C26' }} id="siguiente"

                                    disabled={
                                        this.state.actvaBotonFinalizar || this.state.beneficiarioSeguro1.nombre == "" || this.state.beneficiarioSeguro1.nombre == null || this.state.beneficiarioSeguro1.ape_paterno == "" || this.state.beneficiarioSeguro1.ape_paterno == null
                                        || this.state.beneficiarioSeguro1.fec_nacimiento == "" || this.state.beneficiarioSeguro1.fec_nacimiento == null || this.state.banParentesco ||
                                        this.state.beneficiarioFondo1.nombre == "" || this.state.beneficiarioFondo1.nombre == null ||
                                        this.state.beneficiarioFondo1.ape_paterno == "" || this.state.beneficiarioFondo1.ape_paterno == null ||
                                        this.state.beneficiarioSeguro2.nombre == "" || this.state.beneficiarioSeguro2.nombre == null || this.state.beneficiarioSeguro2.ape_paterno == "" || this.state.beneficiarioSeguro2.ape_paterno == null
                                        || this.state.beneficiarioSeguro2.fec_nacimiento == "" || this.state.beneficiarioSeguro2.fec_nacimiento == null ||
                                        this.state.beneficiarioSeguro2.nombre == "" || this.state.beneficiarioSeguro2.nombre == null ||
                                        this.state.beneficiarioSeguro2.ape_paterno == "" || this.state.beneficiarioSeguro2.ape_paterno == null ||
                                        this.state.beneficiarioSeguro3.nombre == "" || this.state.beneficiarioSeguro3.nombre == null || this.state.beneficiarioSeguro3.ape_paterno == "" || this.state.beneficiarioSeguro3.ape_paterno == null
                                        || this.state.beneficiarioSeguro3.fec_nacimiento == "" || this.state.beneficiarioSeguro3.fec_nacimiento == null ||
                                        this.state.beneficiarioSeguro3.nombre == "" || this.state.beneficiarioSeguro3.nombre == null ||
                                        this.state.beneficiarioSeguro3.ape_paterno == "" || this.state.beneficiarioSeguro3.ape_paterno == null  ||
                                        this.state.beneficiarioFondo2.nombre == "" || this.state.beneficiarioFondo2.nombre == null ||
                                        this.state.beneficiarioFondo2.ape_paterno == "" || this.state.beneficiarioFondo2.ape_paterno == null
                                    }
                                    

                                    onClick={() => this.clickFinalizar("3")} > Finalizar</button> : '' }
                            {
                                this.state.contadorBeneficiariosSeguro == 3 && (this.state.contadorBeneficiariosFondo == 3) ?
                                <button type="button" class="btn btn-warning"
                                    style={{ backgroundColor: '#ED6C26' }} id="siguiente"

                                    disabled={
                                        this.state.actvaBotonFinalizar || this.state.beneficiarioSeguro1.nombre == "" || this.state.beneficiarioSeguro1.nombre == null || this.state.beneficiarioSeguro1.ape_paterno == "" || this.state.beneficiarioSeguro1.ape_paterno == null
                                        || this.state.beneficiarioSeguro1.fec_nacimiento == "" || this.state.beneficiarioSeguro1.fec_nacimiento == null || this.state.banParentesco ||
                                        this.state.beneficiarioFondo1.nombre == "" || this.state.beneficiarioFondo1.nombre == null ||
                                        this.state.beneficiarioFondo1.ape_paterno == "" || this.state.beneficiarioFondo1.ape_paterno == null ||
                                        this.state.beneficiarioSeguro2.nombre == "" || this.state.beneficiarioSeguro2.nombre == null || this.state.beneficiarioSeguro2.ape_paterno == "" || this.state.beneficiarioSeguro2.ape_paterno == null
                                        || this.state.beneficiarioSeguro2.fec_nacimiento == "" || this.state.beneficiarioSeguro2.fec_nacimiento == null ||
                                        this.state.beneficiarioSeguro2.nombre == "" || this.state.beneficiarioSeguro2.nombre == null ||
                                        this.state.beneficiarioSeguro2.ape_paterno == "" || this.state.beneficiarioSeguro2.ape_paterno == null ||
                                        this.state.beneficiarioSeguro3.nombre == "" || this.state.beneficiarioSeguro3.nombre == null || this.state.beneficiarioSeguro3.ape_paterno == "" || this.state.beneficiarioSeguro3.ape_paterno == null
                                        || this.state.beneficiarioSeguro3.fec_nacimiento == "" || this.state.beneficiarioSeguro3.fec_nacimiento == null ||
                                        this.state.beneficiarioSeguro3.nombre == "" || this.state.beneficiarioSeguro3.nombre == null ||
                                        this.state.beneficiarioSeguro3.ape_paterno == "" || this.state.beneficiarioSeguro3.ape_paterno == null  ||
                                        this.state.beneficiarioFondo2.nombre == "" || this.state.beneficiarioFondo2.nombre == null ||
                                        this.state.beneficiarioFondo2.ape_paterno == "" || this.state.beneficiarioFondo2.ape_paterno == null    ||
                                        this.state.beneficiarioFondo3.nombre == "" || this.state.beneficiarioFondo3.nombre == null ||
                                        this.state.beneficiarioFondo3.ape_paterno == "" || this.state.beneficiarioFondo3.ape_paterno == null
                                    }
                                    

                                    onClick={() => this.clickFinalizar("3")} > Finalizar</button> : '' }
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-header bg-transparent header-elements-sm-inline">
                    <h4 class="mb-0 font-weight-semibold"> Beneficiarios del seguro de vida </h4>

                </div>
                <br></br>
                <br></br>
                <div>
                    <div className="row" id="beneficiarioSeguro1">
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Nombre (s)</span>
                                    </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name="nombre%1" tabIndex={1} value={this.state.beneficiarioSeguro1.nombre} />
                                </div>
                                {
                                    this.state.beneficiarioSeguro1.nombre == "" || this.state.beneficiarioSeguro1.nombre == null ?
                                        <span id="validaNombre" style={{ color: "red" }}>El nombre es un campo requerido</span> : ''
                                }
                            </div>

                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }}>Fecha de nacimiento</span>
                                    </span> <input type="date" style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " max="2100-01-01" disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name="fec_nacimiento%1" tabIndex={4} value={"" + this.state.beneficiarioSeguro1.fec_nacimiento + ""} />
                                </div>
                                {
                                    this.state.beneficiarioSeguro1.fec_nacimiento == "" || this.state.beneficiarioSeguro1.fec_nacimiento == null ?
                                        <span className="" style={{ color: "red" }}>El campo fecha de nacimiento es un campo requerido</span> : ''
                                }
                            </div>
                        </div>

                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Apellido paterno</span>
                                    </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name="ape_paterno%1" tabIndex={2} value={this.state.beneficiarioSeguro1.ape_paterno} />
                                </div>
                                {
                                    this.state.beneficiarioSeguro1.ape_paterno == "" || this.state.beneficiarioSeguro1.ape_paterno == null ?
                                        <span className=" " style={{ color: "red" }}>El apellido paterno es un campo requerido</span> : ''
                                }
                            </div>

                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} tabIndex={5}>Parentesco</span>
                                    </span> {/*<input type="date" style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " />*/}
                                    {this.state.selectParentesco1}
                                </div>
                                {
                                    this.state.beneficiarioSeguro1.parentesco_id  == 0 || this.state.beneficiarioSeguro1.parentesco_id == null  ?
                                        <span className=" " style={{ color: "red" }}>El campo Parentesco es un campo requerido</span> : ''
                                }
                            </div>
                        </div>


                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }}>Materno</span>
                                    </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name="ape_materno%1" tabIndex={3} value={this.state.beneficiarioSeguro1.ape_materno} />
                                </div>
                            </div>

                            <div className="form-group">
                                <div className="row">
                                    <div className="col-xs-6 col-lg-6">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }}>Especificar %</span>
                                                </span>
                                                <input type="number" style={{ borderColor: '#E1E5F0' }} className="form-control "  maxLength={3}  disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} onBlur = {this.onBlur} name="porcentaje%1"  tabIndex={6} value={this.state.beneficiarioSeguro1.porcentaje}/>
                                            </div>
                                            {
                                                this.state.beneficiarioSeguro1.porcentaje == 0 ?
                                                    <span className=" " style={{ color: "red" }}>El campo Especificar % es un campo requerido</span> : ''
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {
                        this.state.contadorBeneficiariosSeguro == 2 || this.state.contadorBeneficiariosSeguro == 3 ?

                            <div>
                                <hr></hr>
                                <div className="row" id="beneficiarioSeguro2">
                                    <div className="col-xs-4 col-lg-4">
                                        <div className="form-group">

                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Nombre (s)</span>
                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name="nombre%2" tabIndex={7} value={this.state.beneficiarioSeguro2.nombre} />
                                            </div>
                                            {
                                                    this.state.beneficiarioSeguro2.nombre == "" || this.state.beneficiarioSeguro2.nombre == null ?
                                                    <span id="validaNombre" style={{ color: "red" }}>El nombre es un campo requerido</span> : ''
                                            }
                                        </div>

                                        <div className="form-group">

                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }}>Fecha de nacimiento</span>
                                                </span> <input type="date" style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " max="2100-01-01" disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name="fec_nacimiento%2" tabIndex={10} value={"" + this.state.beneficiarioSeguro2.fec_nacimiento + ""} />
                                            </div>
                                            {
                                                this.state.beneficiarioSeguro2.fec_nacimiento == "" || this.state.beneficiarioSeguro2.fec_nacimiento == null ?
                                                    <span className="" style={{ color: "red" }}>El campo fecha de nacimiento es un campo requerido</span> : ''
                                            }
                                        </div>
                                    </div>

                                    <div className="col-xs-4 col-lg-4">
                                        <div className="form-group">

                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Apellido paterno</span>
                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name="ape_paterno%2" tabIndex={8} value={this.state.beneficiarioSeguro2.ape_paterno} />
                                            </div>
                                            {
                                                this.state.beneficiarioSeguro2.ape_paterno == "" || this.state.beneficiarioSeguro2.ape_paterno == null ?
                                                    <span className=" " style={{ color: "red" }}>El apellido paterno es un campo requerido</span> : ''
                                            }
                                        </div>

                                        <div className="form-group">

                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} tabIndex={11}>Parentesco</span>
                                                </span> {this.state.selectParentesco2}
                                            </div>
                                            {
                                                this.state.beneficiarioSeguro2.parentesco_id  == 0 || this.state.beneficiarioSeguro2.parentesco_id == null ?
                                                    <span className=" " style={{ color: "red" }}>El campo Parentesco es un campo requerido</span> : ''
                                            }
                                        </div>
                                    </div>


                                    <div className="col-xs-4 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Materno</span>
                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name="ape_materno%2" tabIndex={9} value={this.state.beneficiarioSeguro2.ape_materno} />
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <div className="row">
                                                <div className="col-xs-6 col-lg-6">
                                                    <div className="form-group">
                                                        <div className="input-group">
                                                            <span className="input-group-prepend"> <span
                                                                className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Especificar %</span>
                                                            </span>
                                                            <input type="number" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} onBlur = {this.onBlur} name="porcentaje%2" tabIndex={12} value={this.state.beneficiarioSeguro2.porcentaje}/>
                                                        </div>
                                                        {
                                                            this.state.beneficiarioSeguro2.porcentaje == 0 || this.state.beneficiarioSeguro2.porcentaje == null ?
                                                                <span className=" " style={{ color: "red" }}>El campo Especificar % es un campo requerido</span> : ''
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            : ''
                    }

                    {
                        this.state.contadorBeneficiariosSeguro == 3 ?

                            <div>

                                <hr></hr>

                                <div className="row" id="beneficiarioSeguro3">
                                    <div className="col-xs-4 col-lg-4">
                                        <div className="form-group">

                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Nombre (s)</span>
                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name="nombre%3" tabIndex={13} value={this.state.beneficiarioSeguro3.nombre} />
                                            </div>
                                            {
                                                    this.state.beneficiarioSeguro3.nombre == "" || this.state.beneficiarioSeguro3.nombre == null ?
                                                    <span id="validaNombre" style={{ color: "red" }}>El nombre es un campo requerido</span> : ''
                                            }
                                        </div>

                                        <div className="form-group">

                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }}>Fecha de nacimiento</span>
                                                </span> <input type="date" style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control" max="2100-01-01" disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name="fec_nacimiento%3" tabIndex={16} value={"" + this.state.beneficiarioSeguro3.fec_nacimiento + ""} />
                                            </div>
                                            {
                                                this.state.beneficiarioSeguro3.fec_nacimiento == "" || this.state.beneficiarioSeguro3.fec_nacimiento == null ?
                                                    <span className="" style={{ color: "red" }}>El campo fecha de nacimiento es un campo requerido</span> : ''
                                            }
                                        </div>
                                    </div>

                                    <div className="col-xs-4 col-lg-4">
                                        <div className="form-group">

                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Apellido paterno</span>
                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name="ape_paterno%3" tabIndex={14} value={this.state.beneficiarioSeguro3.ape_paterno} />
                                            </div>
                                            {
                                                this.state.beneficiarioSeguro3.ape_paterno == "" || this.state.beneficiarioSeguro3.ape_paterno == null ?
                                                    <span className=" " style={{ color: "red" }}>El apellido paterno es un campo requerido</span> : ''
                                            }
                                        </div>

                                        <div className="form-group">

                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} tabIndex={17}>Parentesco</span>
                                                </span> {this.state.selectParentesco3}
                                            </div>
                                            {
                                                this.state.beneficiarioSeguro3.parentesco_id  == 0 || this.state.beneficiarioSeguro3.parentesco_id == null ?
                                                    <span className=" " style={{ color: "red" }}>El campo Parentesco es un campo requerido</span> : ''
                                            }
                                        </div>
                                    </div>


                                    <div className="col-xs-4 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Materno</span>
                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name="ape_materno%3" tabIndex={15} value={this.state.beneficiarioSeguro3.ape_materno} />
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <div className="row">
                                                <div className="col-xs-6 col-lg-6">
                                                    <div className="form-group">
                                                        <div className="input-group">
                                                            <span className="input-group-prepend"> <span
                                                                className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Especificar %</span>
                                                            </span>
                                                            <input type="number" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} onBlur = {this.onBlur} name="porcentaje%3" tabIndex={18} value={this.state.beneficiarioSeguro3.porcentaje}/>
                                                        </div>
                                                        {
                                                            this.state.beneficiarioSeguro3.porcentaje == 0 || this.state.beneficiarioSeguro3.porcentaje == null ?
                                                                <span className=" " style={{ color: "red" }}>El campo Especificar % es un campo requerido</span> : ''
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> : ''}


                    <div className="row">
                        <div className="col-xs-1 col-lg-1">
                            <button type="button" class="btn text-white" disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onClick={() => this.inyectaBeneficiarioSeguro(true)} style={{ backgroundColor: "#0F69B8" }}>+ Agregar beneficiario</button>
                        </div>

                        <div className="col-xs-1 col-lg-1">
                            <button type="button" class="btn text-white" disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onClick={() => this.inyectaBeneficiarioSeguro(false)} style={{ backgroundColor: "#0F69B8" }}>- Eliminar beneficiario </button>
                        </div>

                    </div>

                </div>




                <hr></hr>
                <div>
                    <div className="row">
                        < div class="card-header">
                            <h5 class="mb-0">Beneficiarios del fondo de ahorro </h5>
                        </div>
                    </div>



                    <div className="row">
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre (s)</span>
                                    </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChangeFondo} name="nombre%1" tabIndex={19} value={this.state.beneficiarioFondo1.nombre} />
                                </div>
                                {
                                    this.state.beneficiarioFondo1.nombre == "" || this.state.beneficiarioFondo1.nombre == null ?
                                        <span id="validaNombre" style={{ color: "red" }}>El nombre es un campo requerido</span> : ''
                                }
                            </div>
                        </div>
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Apellido paterno</span>
                                    </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChangeFondo} name="ape_paterno%1" tabIndex={20}  value={this.state.beneficiarioFondo1.ape_paterno} />
                                </div>
                                {
                                    this.state.beneficiarioFondo1.ape_paterno == "" || this.state.beneficiarioFondo1.ape_paterno == null ?
                                        <span className=" " style={{ color: "red" }}>El apellido paterno es un campo requerido</span> : ''
                                }
                            </div>
                        </div>
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Apellido materno</span>
                                    </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChangeFondo} name="ape_materno%1" tabIndex={21}  value={this.state.beneficiarioFondo1.ape_materno} />
                                </div>
                            </div>
                        </div>

                    </div>
                    {
                        this.state.contadorBeneficiariosFondo == 2 || this.state.contadorBeneficiariosFondo == 3 ?
                            <div>
                                <hr></hr>
                                <div className="row">
                                    <div className="col-xs-4 col-lg-4">
                                        <div className="form-group">

                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre (s)</span>
                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChangeFondo} name="nombre%2" tabIndex={22}  value={this.state.beneficiarioFondo2.nombre} />
                                            </div>
                                            {
                                                this.state.beneficiarioFondo2.nombre == "" || this.state.beneficiarioFondo2.nombre == null ?
                                                    <span id="validaNombre" style={{ color: "red" }}>El nombre es un campo requerido</span> : ''
                                            }
                                        </div>
                                    </div>
                                    <div className="col-xs-4 col-lg-4">
                                        <div className="form-group">

                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Apellido paterno</span>
                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChangeFondo} name="ape_paterno%2" tabIndex={23} value={this.state.beneficiarioFondo2.ape_paterno} />
                                            </div>
                                            {
                                                this.state.beneficiarioFondo2.ape_paterno == "" || this.state.beneficiarioFondo2.ape_paterno == null ?
                                                    <span className=" " style={{ color: "red" }}>El apellido paterno es un campo requerido</span> : ''
                                            }
                                        </div>
                                    </div>
                                    <div className="col-xs-4 col-lg-4">
                                        <div className="form-group">

                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Apellido materno</span>
                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChangeFondo} name="ape_materno%2" tabIndex={24} value={this.state.beneficiarioFondo2.ape_materno} />
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div> : ''
                    }

                    {
                        this.state.contadorBeneficiariosFondo == 3 ?
                            <div>
                                <hr></hr>
                                <div className="row">
                                    <div className="col-xs-4 col-lg-4">
                                        <div className="form-group">

                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre (s)</span>
                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChangeFondo} name="nombre%3" tabIndex={25} value={this.state.beneficiarioFondo3.nombre} />
                                            </div>
                                            {
                                                this.state.beneficiarioFondo3.nombre == "" || this.state.beneficiarioFondo3.nombre == null ?
                                                    <span id="validaNombre" style={{ color: "red" }}>El nombre es un campo requerido</span> : ''
                                            }
                                        </div>
                                    </div>
                                    <div className="col-xs-4 col-lg-4">
                                        <div className="form-group">

                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Apellido paterno</span>
                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control "  disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChangeFondo} name="ape_paterno%3" tabIndex={26} value={this.state.beneficiarioFondo3.ape_paterno} />
                                            </div>
                                            {
                                                this.state.beneficiarioFondo3.ape_paterno == "" || this.state.beneficiarioFondo3.ape_paterno == null ?
                                                    <span className=" " style={{ color: "red" }}>El apellido paterno es un campo requerido</span> : ''
                                            }
                                        </div>
                                    </div>
                                    <div className="col-xs-4 col-lg-4">
                                        <div className="form-group">

                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Apellido materno</span>
                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control "  disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChangeFondo} name="ape_materno%3" tabIndex={27} value={this.state.beneficiarioFondo3.ape_materno} />
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div> : ''
                    }


                </div>


                <div className="row">
                    <div className="col-xs-1 col-lg-1">
                        <button type="button" disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onClick={() => this.inyectaBeneficiarioFondo(true)} class="btn text-white" style={{ backgroundColor: "#0F69B8" }}>+ Agregar beneficiario</button>
                    </div>
                    <div className="col-xs-1 col-lg-1">
                        <button type="button" disabled = { (this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onClick={() => this.inyectaBeneficiarioFondo(false)} class="btn text-white" style={{ backgroundColor: "#0F69B8" }}>- Eliminar beneficiario</button>
                    </div>
                </div>

                <div className="row">
                <div className="col-xs-12 col-lg-4">
                    <div className="form-group">
                        <div className="input-group">
                                <input type="text" style={{ display: 'none' }} ref={(ref) => this.modalPorcentaje = ref} data-toggle= {this.state.activaModal ? "modal" : ""}  data-target="#modal-Confirmacion"></input>
                        </div>
                    </div>
                </div>
                <div className="col-xs-12 col-lg-4">
                    <div className="form-group">
                        <div className="input-group">
                                <input type="text" style={{ display: 'none' }} ref={(ref) => this.eliminarBeneficiarioclick = ref} onClick={this.changeBeneficiario}></input>
                        </div>
                    </div>
                </div>
            </div>

                <br></br>
                <br></br>
                <div id="modal-Confirmacion" className="modal fade" tabindex="-1">
                    <div className="modal-dialog ">
                        <div className="modal-content text-white" style={{ backgroundColor: "#FFFFF" }}>
                            <div className="modal-header text-white text-center" style={{ backgroundColor: "#0F69B8" }}>
                                <h6 className="modal-title col-11 text-center">Porcentaje Beneficiario</h6>
                            </div>
                            <div className="modal-body">
                                <div className="card-body">
                                    <h8 style={{ color: '#0F69B8' }}>Recuerda que la sumatoria de los porcentajes del Beneficiario debe de dar 100%</h8>
                                </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-center">
                                <div className="col-12">
                                    <div className="row">
                                        <button type="button" style={{ width: '100%', backgroundColor: '#8F9EB3' }} class="btn text-white" id="btnAgregar" data-dismiss="modal"  >Aceptar</button>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>

        )
    }

}