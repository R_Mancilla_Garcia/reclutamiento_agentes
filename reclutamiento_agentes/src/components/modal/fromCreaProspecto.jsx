import React, { Component } from "react";
import datos from '../urls/datos.json'

export default class FormCreaProspecto extends Component {
    constructor() {
        super();
        this.onChange = this.onChange.bind(this)
        this.construyeSelect = this.construyeSelect.bind(this)
        this.onClickBotonArchivoCV = this.onClickBotonArchivoCV.bind(this)
        this.construyeSelectAgenteReferidor = this.construyeSelectAgenteReferidor.bind(this)
        this.guardarCambios = this.guardarCambios.bind(this)
        this.cancelarCambios=this.cancelarCambios.bind(this)
        this.state = {
            selectEDAT: [],
            selectFuenteReclutamiento: [],
            selectFuenteReclutamientoBono: [],
            selectEstadoCivil: [],
            selectAgenteReferidor: [],
            referido: {},
            colorSubirArchivoCV: "#E1E5F0",
            colorTextoSubirArchivoCV: "#617187",
            colorBotonSubirArchivoCV: "#0F69B8",
            nombreArchivoCV: "",
            archivoCV: undefined,
            muestraDocCV: false,
            muestraAgente: false,
            expresionRFC: /^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$/,
            validaRFC: false,
            expresionEmail: /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
            validaEmail: false,
        }
    }

    onChange = e => {
        let campoBeneficiario = e.target.name
        let referido = this.state.referido

        //
        if ("selectEDAT" == e.target.name) {
            referido.empleado_id = parseInt(e.target.value)
        } else if ("selectEstadoCivil" == e.target.name) {
            referido.estadocivil_id = parseInt(e.target.value)
        } else if ("selectFuenteReclutamiento" == e.target.name) {
            console.log("entro en selectFuenteReclutamiento");
            referido.fuente_reclutamiento_id = parseInt(e.target.value)
            if (referido.fuente_reclutamiento_id == 14) {
                console.log("Entro aqui");
                this.setState({ muestraAgente: true })
            } else {
                this.setState({ muestraAgente: false })
            }
        } else if ("selectAgenteReferidor" == e.target.name) {
            if (e.target.value != 0) {
                let contenido = e.target.value
                let separacion = contenido.split(",")
                referido.agente_referidor = parseInt(separacion[0])
                referido.cua_referidor = separacion[2]
            } else {
                referido.agente_referidor = 0
                referido.cua_referidor = ""
            }
            console.log("selector agente referido  ", e.target.value)
        } else if ("selectFuenteReclutamientoBono" == e.target.name) {
            referido.fuente_recluta_bonos_id = parseInt(e.target.value)

        } else if ("rfc" == e.target.name) {
            let rfc = e.target.value;
            if (rfc.match(this.state.expresionRFC)) {
                this.setState({ validaRFC: true })
                var rfcanio = rfc.substring(4, 6);
                var rfcmes = rfc.substring(6, 8);
                var rfcdia = rfc.substring(8, 10);
                var formatDate = "";

                if (rfcanio <= 20) {
                    formatDate = "20" + rfcanio + "-";
                } else {
                    formatDate = "19" + rfcanio + "-";
                }
                formatDate = formatDate + rfcmes + "-" + rfcdia;
                //AUVB9209207Z3
                //referido.fec_nacimiento="2022-06-03"
                referido.fec_nacimiento = formatDate;
                console.log("rfc");
            } else {
                referido.fec_nacimiento = "";
                this.setState({ validaRFC: false })
            }
            //            this.setState({ [e.target.name]: e.target.value })
            referido["" + campoBeneficiario + ""] = e.target.value

        } else if ("correo_electronico" == e.target.name) {
            let email = e.target.value;
            if (email.match(this.state.expresionEmail)) {
                this.setState({ validaEmail: true })
            } else {
                this.setState({ validaEmail: false })
            }
            referido["" + campoBeneficiario + ""] = e.target.value

        } else if (e.target.name == "tel_movil" || e.target.name == "tel_casa" || e.target.name == "tel_oficina") {

            let movil = e.target.value
            if (movil.length <= 10) {
                referido["" + e.target.name + ""] = e.target.value
            }


        } else {
            referido["" + campoBeneficiario + ""] = e.target.value
        }

        this.setState({ referido: referido })

    }

    cancelarCambios() {
        let referido = this.state.referido;
        console.log("Entro en cancelar")
        referido.nombre = "";
        referido.ape_materno = "";
        referido.ape_paterno = "";
        referido.correo_electronico = "";
        referido.tel_movil = "";
        referido.rfc = ""
        referido.fec_nacimiento = ""
        referido.fec_envio_psp = ""
        referido.validaEmail = ""
        referido.tel_casa = 0
        referido.tel_oficina = 0
        referido.nombre_aps = ""
        referido.estadocivil_id = 0
        referido.fuente_recluta_bonos_id = 0
        referido.fuente_reclutamiento_id = 0
        referido.empleado_id = 0
        referido.cua_referidor = "";
        this.state.archivoCV = undefined
        this.state.nombreArchivoCV = ""
        this.state.colorSubirArchivoCV = "#E1E5F0"
        this.state.colorTextoSubirArchivoCV = "#617187"
        this.state.colorBotonSubirArchivoCV = "#0F69B8"
        this.setState({ ["selectEstadoCivil"]: null })
        this.construyeSelect(datos.urlServicePy+"parametros/api_cat_estadocivil/0", "selectEstadoCivil")
        this.setState({ ["selectFuenteReclutamiento"]: null })
        this.construyeSelect(datos.urlServicePy+"parametros/api_cat_fuente_reclutamiento/0", "selectFuenteReclutamiento")
        this.setState({ ["selectFuenteReclutamientoBono"]: null })
        this.construyeSelect(datos.urlServicePy+"parametros/api_cat_fuente_reclutamiento_bonos/0", "selectFuenteReclutamientoBono")
        this.setState({ ["selectEDAT"]: null })
        this.construyeSelect(datos.urlServicePy+"recluta/api_vcat_edat/0", "selectEDAT")
        this.setState({ ["selectAgenteReferidor"]: null })
        this.construyeSelectAgenteReferidor(datos.urlServicePy+"recluta/api_recluta_vagente_referidor/0", "selectAgenteReferidor")
        this.setState ({referido : referido,muestraAgente: false})
    }


    guardarCambios() {
        console.log("json a enviar ", this.state.referido)
    }

    construyeSelectAgenteReferidor(url, selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                for (var i = 0; i < filas.length; i++) {
                    let fila = filas[i]
                    if (selector == "selectAgenteReferidor") {
                        if (this.props.idEmpleado) {

                            if (this.state.referido.agente_referidor == fila.agente_referidor) {
                                options.push(<option selected value={fila.id + "," + fila.agente_referidor + "," + fila.cua_referidor}>{fila.agente_referidor}</option>)
                            } else {
                                options.push(<option value={fila.id + "," + fila.agente_referidor + "," + fila.cua_referidor}>{fila.agente_referidor}</option>)
                            }
                        } else {
                            options.push(<option value={fila.id + "," + fila.agente_referidor + "," + fila.cua_referidor}>{fila.agente_referidor}</option>)
                        }

                    }

                }
                let salida = []
                salida.push(<select onChange={this.onChange} name={selector} style={{ borderColor: '#E1E5F0' }} className="form-control bg-dark border-dark text-white" aria-label="Default select example">{options}</select>)
                this.setState({ selectAgenteReferidor: salida })
            })
    }

    construyeSelect(url, selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas =  selector == "selectEDAT" ? json:json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                for (var i = 0; i < filas.length; i++) {

                    let fila = filas[i]
                    if (selector == "selectEDAT") {
                        if (this.props.idEmpleado) {

                            if (this.state.referido.empleado_id == fila.id) {
                                options.push(<option selected value={fila.id}>{fila.nombre}</option>)
                            } else {
                                options.push(<option value={fila.id}>{fila.nombre}</option>)
                            }
                        } else {
                            options.push(<option value={fila.id}>{fila.nombre}</option>)
                        }
                    }



                    if (selector == "selectEstadoCivil") {
                        if (this.props.idEmpleado) {

                            if (this.state.referido.estadocivil_id == fila.fila[0].value) {
                                options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            } else {
                                options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            }
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }

                    }

                    if (selector == "selectFuenteReclutamientoBono") {
                        if (this.props.idEmpleado) {

                            if (this.state.referido.fuente_recluta_bonos_id == fila.fila[0].value) {
                                options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            } else {
                                options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            }
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }

                    }

                    if (selector == "selectFuenteReclutamiento") {
                        if (this.props.idEmpleado) {

                            if (this.state.referido.fuente_reclutamiento_id == fila.fila[0].value) {
                                options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            } else {
                                options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            }
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }

                    }




                }
                let salida = []
                salida.push(<select onChange={this.onChange} name={selector} style={{ backgroundColor: "#313A46" }} class="form-control text-white border-dark" aria-label="Default select example">{options}</select>)
                this.setState({ [selector]: salida /*,selectParentesco2: salida,selectParentesco3: salida*/ })
            });
    }


    onChangeFileCV(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chingado file de CV ", file);
        this.setState({ archivoCV: file, colorSubirArchivoCV: "#78CB5A", colorTextoSubirArchivoCV: "#FFFFFF", colorBotonSubirArchivoCV: "#E5F6E0", nombreArchivoCV: file.name })


    }

    onClickBotonArchivoCV() {
        console.log("entrnaod a la CV")
        this.cv.click()
    }

    UNSAFE_componentWillMount() {
        this.construyeSelect(datos.urlServicePy+"recluta/api_vcat_edat/0", "selectEDAT")
        this.construyeSelect(datos.urlServicePy+"parametros/api_cat_fuente_reclutamiento/0", "selectFuenteReclutamiento")
        this.construyeSelect(datos.urlServicePy+"parametros/api_cat_fuente_reclutamiento_bonos/0", "selectFuenteReclutamientoBono")
        this.construyeSelect(datos.urlServicePy+"parametros/api_cat_estadocivil/0", "selectEstadoCivil")
        this.construyeSelectAgenteReferidor(datos.urlServicePy+"recluta/api_recluta_vagente_referidor/0", "selectAgenteReferidor")

    }

    render() {
        return (
            <div>
                <div className="modal-body">
                    <div className="card-body">
                        <div className="row">
                            <div className="col-xs-12 col-lg-2">
                                <div className="form-group">
                                    <div className="input-group">
                                        <h4 class="card-title py-3 font-weight-bold" >Información general</h4>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-xs-12 col-lg-4">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white " style={{ backgroundColor: "#313A46" }}>Nombre (s)</span>
                                        </span> <input type="text" placeholder="Nombre" name="nombre" onChange={this.onChange} value={this.state.referido.nombre} className="form-control bg-dark border-dark text-white " />
                                    </div>
                                    {
                                        this.state.referido.nombre == "" || this.state.referido.nombre == null ?
                                            <span className="" style={{ color: "red" }}>El nombre es un campo requerido</span> : ''
                                    }
                                </div>
                            </div>

                            <div className="col-xs-12 col-lg-4">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text  border-dark text-white " style={{ backgroundColor: "#313A46" }}>Apellido paterno</span>
                                        </span> <input type="text" placeholder="Apellido" name="ape_paterno" onChange={this.onChange} value={this.state.referido.ape_paterno} className="form-control bg-dark border-dark text-white " />
                                    </div>
                                    {
                                        this.state.referido.ape_paterno == "" || this.state.referido.ape_paterno == null ?
                                            <span className="" style={{ color: "red" }}>El apellido paterno es un campo requerido</span> : ''
                                    }
                                </div>
                            </div>


                            <div className="col-xs-12 col-lg-4">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text  border-dark text-white " style={{ backgroundColor: "#313A46" }}>Apellido materno</span>
                                        </span> <input type="text" placeholder="Apellido" className="form-control  bg-dark border-dark text-white " name="ape_materno" onChange={this.onChange} value={this.state.referido.ape_materno} />
                                    </div>
                                    {
                                        //   this.state.referido.ape_materno == "" || this.state.referido.ape_materno == null ?
                                        //      <span className="" style={{ color: "red" }}>El apellido materno es un campo requerido</span> : ''
                                    }
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            
                        <div className="col-xs-12 col-lg-4">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white " style={{ backgroundColor: "#313A46" }}>RFC</span>
                                        </span> <input type="text" placeholder="Escribir" className="form-control bg-dark border-dark text-white" name="rfc" onChange={this.onChange} value={this.state.referido.rfc} />
                                    </div>
                                    {
                                        this.state.validaRFC == false ?
                                            <span className="" style={{ color: "red" }}>El rfc es un campo requerido</span> : ''
                                    }
                                </div>
                            </div>

                            
                            
                            <div className="col-xs-12 col-lg-4">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white " style={{ backgroundColor: "#313A46" }}>Fecha de nacimiento</span>
                                        </span> <input type="date" className="form-control  bg-dark border-dark text-white " name="fec_nacimiento" max="2100-01-01" onChange={this.onChange} value={this.state.referido.fec_nacimiento} />
                                    </div>
                                    {
                                        this.state.referido.fec_nacimiento == "" || this.state.referido.fec_nacimiento == null ?
                                            <span className="" style={{ color: "red" }}>La fecha de nacimiento es un campo requerido</span> : ''
                                    }
                                </div>
                            </div>

                            

                            <div className="col-xs-12 col-lg-4">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white " style={{ backgroundColor: "#313A46" }}>Estado civil</span>
                                        </span> {this.state.selectEstadoCivil}
                                    </div>
                                    {
                                        //  this.state.referido.estadocivil_id == 0 || this.state.referido.estadocivil_id == null ?
                                        //     <span className="" style={{ color: "red" }}>El estado civil es un campo requerido</span> : ''
                                    }
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-xs-12 col-lg-4">
                                <div className="form-group">
                                    <div className="input-group">
                                        <form encType="multipart/form" style={{ display: 'none' }} >
                                            <input type="file" style={{ display: 'none' }} ref={(ref) => this.cv = ref} onChange={this.onChangeFileCV.bind(this)}></input>
                                        </form>
                                        <input type="text" style={{ backgroundColor: '#313A46' }} className="form-control border-dark text-white" placeholder="Curriculum Vitae" value={this.state.nombreArchivoCV} />
                                        <span className="input-group-prepend">
                                            <button type="button" class="btn text-white " onClick={this.onClickBotonArchivoCV} style={{ backgroundColor: this.state.colorBotonSubirArchivoCV }}>
                                                <h10 style={{ color: this.state.colorSubirArchivoCV }}>+</h10>
                                            </button>

                                            {
                                                this.state.muestraDocCV == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivo }}>
                                                    <a href={this.state.referido.doc_cv} target="_blank" rel="noopener noreferrer" >
                                                        <i className="fas fa-eye"
                                                            style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                    </a>
                                                </button>
                                                    : ''
                                            }

                                            <span className="input-group-text" style={{ background: this.state.colorSubirArchivoCV, borderColor: this.state.colorSubirArchivoCV, color: this.state.colorTextoSubirArchivoCV }} >Subir archivo</span>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div className="col-xs-12 col-lg-4">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white " style={{ backgroundColor: "#313A46" }}>Envío de link PSP</span>
                                        </span> <input type="date" className="form-control bg-dark border-dark text-white" name="fec_envio_psp" max="2100-01-01" onChange={this.onChange} value={this.state.referido.fec_envio_psp} />
                                    </div>
                                    {/*
                                        this.state.referido.fec_envio_psp == "" || this.state.referido.fec_envio_psp == null ?
                                            <span className="" style={{ color: "red" }}>El envío de link psp es un campo requerido</span> : ''
                                        */}
                                </div>
                            </div>


                            <div className="col-xs-12 col-lg-4">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white " style={{ backgroundColor: "#313A46" }}>Correo electrónico</span>
                                        </span> <input type="text" placeholder="ejemplo@correo.com" className="form-control bg-dark border-dark text-white" name="correo_electronico" onChange={this.onChange} value={this.state.referido.correo_electronico} />
                                    </div>
                                    {
                                        this.state.validaEmail == false ?
                                            <span className="" style={{ color: "red" }}>El correo electrónico es un campo requerido</span> : ''
                                    }
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-xs-12 col-lg-4">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white " style={{ backgroundColor: "#313A46" }}>Teléfono móvil</span>
                                        </span> <input type="number" placeholder="55 0000 0000" className="form-control bg-dark border-dark text-white"

                                            name="tel_movil" onChange={this.onChange} value={this.state.referido.tel_movil} />
                                    </div>
                                    {
                                        (this.state.referido.tel_movil + '').length == 0 || (this.state.referido.tel_movil + '').length <= 9 || this.state.referido.tel_movil == null ?
                                            <span className="" style={{ color: "red" }}>El teléfono móvil es un campo requerido</span> : ''
                                    }
                                </div>
                            </div>

                            <div className="col-xs-12 col-lg-4">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white " style={{ backgroundColor: "#313A46" }}>Teléfono casa</span>
                                        </span> <input type="number" placeholder="10 dígitos" className="form-control bg-dark border-dark text-white" name="tel_casa" onChange={this.onChange} value={this.state.referido.tel_casa} />
                                    </div>
                                </div>
                            </div>


                            <div className="col-xs-12 col-lg-4">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white " style={{ backgroundColor: "#313A46" }}>Teléfono oficina</span>
                                        </span> <input type="number" placeholder="10 dígitos" className="form-control bg-dark border-dark text-white" name="tel_oficina" onChange={this.onChange} value={this.state.referido.tel_oficina} />
                                    </div>
                                </div>
                            </div>
                        </div>

                        {/* informacion general ************************************************************************************** */}

                        <br></br>


                        <div className="row">
                            <div className="col-xs-12 col-lg-2">
                                <div className="form-group">
                                    <div className="input-group">
                                        <h4 class="card-title py-3 font-weight-bold" >Información complementaría</h4>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-xs-12 col-lg-4">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white " style={{ backgroundColor: "#313A46" }}>Fuente de reclutamiento</span>
                                        </span> {this.state.selectFuenteReclutamiento}
                                    </div>
                                    {
                                        this.state.referido.fuente_reclutamiento_id == 0 || this.state.referido.fuente_reclutamiento_id == null ?
                                            <span className="" style={{ color: "red" }}>La fuente de reclutamiento es un campo requerido</span> : ''
                                    }
                                </div>
                            </div>

                            <div className="col-xs-12 col-lg-4">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white " style={{ backgroundColor: "#313A46" }}>Fuente de reclutamiento para bonos</span>
                                        </span> {this.state.selectFuenteReclutamientoBono} {/* mover este state al correcto */}
                                    </div>
                                    {
                                        this.state.referido.fuente_recluta_bonos_id == 0 || this.state.referido.fuente_recluta_bonos_id == null ?
                                            <span className="" style={{ color: "red" }}>La fuente de reclutamiento para bonos es un campo requerido</span> : ''
                                    }
                                </div>
                            </div>


                            <div className="col-xs-12 col-lg-4">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white " style={{ backgroundColor: "#313A46" }}>EDAT asignado</span>
                                        </span> {this.state.selectEDAT}
                                    </div>
                                    {
                                        this.state.referido.empleado_id == 0 || this.state.referido.empleado_id == null ?
                                            <span className="" style={{ color: "red" }}>El EDAT asignado es un campo requerido</span> : ''
                                    }
                                </div>
                            </div>
                        </div>

                        {this.state.muestraAgente ?
                            <div className="row">
                                <div className="col-xs-12 col-lg-4">
                                    <div className="form-group">
                                        <div className="input-group">
                                            <span className="input-group-prepend"> <span
                                                className="input-group-text border-dark text-white " style={{ backgroundColor: "#313A46" }}>Agente referidor</span>
                                            </span> {/*<input type="text" placeholder="Escribir" className="form-control bg-dark border-dark text-white" name="agente_referidor" onChange={this.onChange} value={this.state.referido.agente_referidor} />*/}
                                            {this.state.selectAgenteReferidor}
                                        </div>
                                    </div>
                                </div>

                                <div className="col-xs-12 col-lg-4">
                                    <div className="form-group">
                                        <div className="input-group">
                                            <span className="input-group-prepend"> <span
                                                className="input-group-text border-dark text-white " style={{ backgroundColor: "#313A46" }}>CUA referidora para reportar a GNP</span>
                                            </span> <input type="text" placeholder="Escribir" className="form-control bg-dark border-dark text-white" name="cua_referidor" disabled={true} onChange={this.onChange} value={this.state.referido.cua_referidor} />
                                        </div>
                                    </div>
                                </div>

                                <div className="col-xs-12 col-lg-4">
                                    <div className="form-group">
                                        <div className="input-group">
                                            <span className="input-group-prepend"> <span
                                                className="input-group-text border-dark text-white " style={{ backgroundColor: "#313A46" }}>Nombre APS</span>
                                            </span> <input type="text" placeholder="Escribir" className="form-control  bg-dark border-dark text-white" name="nombre_aps" onChange={this.onChange} value={this.state.referido.nombre_aps} />
                                        </div>
                                        {
                                            this.state.referido.nombre_aps == "" || this.state.referido.nombre_aps == null ?
                                                <span className="" style={{ color: "red" }}>El nombre_aps es un campo requerido</span> : ''
                                        }
                                    </div>
                                </div>
                            </div> :
                            <div className="row">
                                <div className="col-xs-12 col-lg-4">
                                    <div className="form-group">
                                        <div className="input-group">
                                            <span className="input-group-prepend"> <span
                                                className="input-group-text border-dark text-white " style={{ backgroundColor: "#313A46" }}>Nombre APS</span>
                                            </span> <input type="text" placeholder="Escribir" className="form-control  bg-dark border-dark text-white" name="nombre_aps" onChange={this.onChange} value={this.state.referido.nombre_aps} />
                                        </div>
                                        {
                                            this.state.referido.nombre_aps == "" || this.state.referido.nombre_aps == null ?
                                                <span className="" style={{ color: "red" }}>El nombre_aps es un campo requerido</span> : ''
                                        }
                                    </div>
                                </div>
                            </div>
                        }



                    </div>
                </div>

                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-light" onClick={this.cancelarCambios} data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-warning" id="btnAgregar" data-dismiss="modal" disabled={
                        this.state.validaRFC == false || this.state.validaEmail == false ||
                        this.state.referido.nombre == '' || this.state.referido.nombre == null ||
                        this.state.referido.ape_paterno == '' || this.state.referido.ape_paterno == null ||
                        // this.state.referido.ape_materno == '' || this.state.referido.ape_materno == null ||
                        this.state.referido.fec_nacimiento == '' || this.state.referido.fec_nacimiento == null ||
                        this.state.referido.empleado_id == 0 || this.state.referido.empleado_id == null ||
                        //this.state.referido.fec_envio_psp == '' || this.state.referido.fec_envio_psp == null ||
                        (this.state.referido.tel_movil + '').length == 0 || this.state.referido.tel_movil == null ||
                        this.state.referido.fuente_reclutamiento_id == 0 || this.state.referido.fuente_reclutamiento_id == null ||
                        this.state.referido.fuente_recluta_bonos_id == 0 || this.state.referido.fuente_recluta_bonos_id == null ||
                        this.state.referido.empleado_id == 0 || this.state.referido.empleado_id == null ||
                        this.state.referido.nombre_aps == '' || this.state.referido.nombre_aps == null
                    } onClick={(e) => this.props.guardaElementoNuevo(this.state.referido, this.state.archivoCV)} >Agregar</button>
                </div>


            </div>
        )
    }
}
