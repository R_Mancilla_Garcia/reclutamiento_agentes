import React, { Component } from "react";
import datos from '../urls/datos.json'

export default class FormCursoIdeas extends Component {

    constructor() {

        super();
        this.onChange = this.onChange.bind(this)
        this.guardarElemento = this.guardarElemento.bind(this)
        this.changeActivo = this.changeActivo.bind(this)
        this.limpiarElementos = this.limpiarElementos.bind(this)
        this.state = {
            anios: [2016, 2017, 2018, 2019, 2021],
            tema: "",
            desc_tema: null,
            id_subtema: "",
            subtema: "",
            desc_subtema: null,
            id_subtema2: "",
            subtema2: "",
            desc_subtema2: null,
            id_subtema3: "",
            subtema3: "",
            desc_subtema3: null,
            id_subtema4: "",
            subtema4: "",
            desc_subtema4: null,
            id_subtema5: "",
            subtema5: "",
            desc_subtema5: null,
            id_subtema6: "",
            subtema6: "",
            desc_subtema6: null,
            id_subtema7: "",
            subtema7: "",
            desc_subtema7: null,
            id_subtema8: "",
            subtema8: "",
            desc_subtema8: null,
            id_subtema9: "",
            subtema9: "",
            desc_subtema9: null,
            id_subtema10: "",
            subtema10: "",
            desc_subtema10: null,
            activo: true,
            idActualizar: 0,
            contadorSubtema: 1,
            subtemaEliminado1 : false,
            subtemaEliminado2 : false,
            subtemaEliminado3 : false,
            subtemaEliminado4 : false,
            subtemaEliminado5 : false,
            subtemaEliminado6 : false,
            subtemaEliminado7 : false,
            subtemaEliminado8 : false,
            subtemaEliminado9 : false,
            subtemaEliminado10 : false,
            subtemaAgregado1 : false,
            subtemaAgregado2 : false,
            subtemaAgregado3 : false,
            subtemaAgregado4 : false,
            subtemaAgregado5 : false,
            subtemaAgregado6 : false,
            subtemaAgregado7 : false,
            subtemaAgregado8 : false,
            subtemaAgregado9 : false,
            subtemaAgregado10 : false,
            
            subtemas : {}
        };
    }

    UNSAFE_componentWillMount() {
        if (this.props.contenido != undefined) {
            console.log("entrando a form ", this.props.contenido)
            let idTema = this.props.contenido[0].value;
            fetch(datos.urlServicePy+"parametros/api_cat_curso_ideas_subtema/"+idTema)
            .then(response => response.json())
            .then(jsonSubtema => {
                if(jsonSubtema.filas.length> 0){
                    let filas = jsonSubtema.filas
                    let columnas = jsonSubtema.columnas
                    console.log("Entro en cat_curso_ideas");
                    let columnasSalida = {}
                    for (var i = 0; i < filas.length; i++) {
                        for (var j = 0; j < filas[i].fila.length; j++){
                            columnasSalida["" + columnas[j].key + ""] = filas[i].fila[j].value
                        }
                        if(i == 0){
                            this.setState({
                                id_subtema: columnasSalida.id,
                                desc_subtema: columnasSalida.desc_subtema,
                                subtema: columnasSalida.subtema,
                                contadorSubtema: 1
                            })
                        }
                        if(i == 1){
                            this.setState({
                                id_subtema2: columnasSalida.id,
                                desc_subtema2: columnasSalida.desc_subtema,
                                subtema2: columnasSalida.subtema,
                                contadorSubtema: 2
                            })
                        }
                        if(i == 2){
                            this.setState({
                                id_subtema3: columnasSalida.id,
                                desc_subtema3: columnasSalida.desc_subtema,
                                subtema3: columnasSalida.subtema,
                                contadorSubtema: 3
                            })
                        }
                        if(i == 3){
                            this.setState({
                                id_subtema4: columnasSalida.id,
                                desc_subtema4: columnasSalida.desc_subtema,
                                subtema4: columnasSalida.subtema,
                                contadorSubtema: 4
                            })
                        }
                        if(i == 4){
                            this.setState({
                                id_subtema5: columnasSalida.id,
                                desc_subtema5: columnasSalida.desc_subtema,
                                subtema5: columnasSalida.subtema,
                                contadorSubtema: 5
                            })
                        }
                        if(i == 5){
                            this.setState({
                                id_subtema6: columnasSalida.id,
                                desc_subtema6: columnasSalida.desc_subtema,
                                subtema6: columnasSalida.subtema,
                                contadorSubtema: 6
                            })
                        }
                        if(i == 6){
                            this.setState({
                                id_subtema7: columnasSalida.id,
                                desc_subtema7: columnasSalida.desc_subtema,
                                subtema7: columnasSalida.subtema,
                                contadorSubtema: 7
                            })
                        }if(i == 7){
                            this.setState({
                                id_subtema8: columnasSalida.id,
                                desc_subtema8: columnasSalida.desc_subtema,
                                subtema8: columnasSalida.subtema,
                                contadorSubtema: 8
                            })
                        }
                        if(i == 8){
                            this.setState({
                                id_subtema9: columnasSalida.id,
                                desc_subtema9: columnasSalida.desc_subtema,
                                subtema9: columnasSalida.subtema,
                                contadorSubtema: 9
                            })
                        }
                        if(i == 9){
                            this.setState({
                                id_subtema10: columnasSalida.id,
                                desc_subtema10: columnasSalida.desc_subtema,
                                subtema10: columnasSalida.subtema,
                                contadorSubtema: 10
                            })
                        }
                        console.log("conlumnas salida :::"+columnasSalida);
                    }
                }
                this.setState({
                    idActualizar: this.props.contenido[0].value,
                    tema: this.props.contenido[1].value,
                    desc_tema: this.props.contenido[2].value,
                    activo: this.props.contenido[3].value
                })
            });
        }
    }

    inyectaSubtema(banInyecta) {
        console.log("inyeccion")

        let contadorSubtema = this.state.contadorSubtema
        if(contadorSubtema == 0 && banInyecta){
            contadorSubtema = this.state.contadorSubtema + 1
            this.setState({ contadorSubtema: contadorSubtema,subtemaEliminado1 : false,subtemaAgregado1: true})
        }
        if(contadorSubtema >= 1 && contadorSubtema < 10){
            if(banInyecta){
                contadorSubtema = this.state.contadorSubtema + 1
                this.setState({ contadorSubtema: contadorSubtema})
                if(this.state.subtemaEliminado2){
                    this.setState({ subtemaEliminado2 : false})
                }
                if(this.state.subtemaEliminado3){
                    this.setState({ subtemaEliminado3 : false})
                }
                if(this.state.subtemaEliminado4){
                    this.setState({ subtemaEliminado4 : false})
                }
                if(this.state.subtemaEliminado5){
                    this.setState({ subtemaEliminado5 : false})
                }
                if(this.state.subtemaEliminado6){
                    this.setState({ subtemaEliminado6 : false})
                }
                if(this.state.subtemaEliminado7){
                    this.setState({ subtemaEliminado7 : false})
                }
                if(this.state.subtemaEliminado8){
                    this.setState({ subtemaEliminado8 : false})
                }
                if(this.state.subtemaEliminado9){
                    this.setState({ subtemaEliminado9 : false})
                }
                if(this.state.subtemaEliminado10){
                    this.setState({ subtemaEliminado10 : false})
                }
                if(contadorSubtema == 2){
                    this.setState({ subtemaAgregado2: true})
                }
                if(contadorSubtema == 3){
                    this.setState({ subtemaAgregado3: true})
                }
                if(contadorSubtema == 2){
                    this.setState({ subtemaAgregado4: true})
                }
                if(contadorSubtema == 4){
                    this.setState({ subtemaAgregado5: true})
                }
                if(contadorSubtema == 6){
                    this.setState({ subtemaAgregado6: true})
                }
                if(contadorSubtema == 7){
                    this.setState({ subtemaAgregado7: true})
                }
                if(contadorSubtema == 8){
                    this.setState({ subtemaAgregado8: true})
                }
                if(contadorSubtema == 9){
                    this.setState({ subtemaAgregado9: true})
                }
                if(contadorSubtema == 10){
                    this.setState({ subtemaAgregado10: true})
                }
            }else{
                if(contadorSubtema == 1){
                    this.state.subtema = "";
                    this.state.desc_subtema = "";
                    contadorSubtema = this.state.contadorSubtema - 1
                    this.setState({ contadorSubtema: contadorSubtema ,subtemaEliminado1 : true, subtemaAgregado1: false})
                }else if(contadorSubtema == 2){
                    this.state.subtema2 = "";
                    this.state.desc_subtema2 = "";
                    contadorSubtema = this.state.contadorSubtema - 1
                    this.setState({ contadorSubtema: contadorSubtema ,subtemaEliminado2 : true, subtemaAgregado2: false})
                }else if (contadorSubtema == 3){
                    this.state.subtema3 = "";
                    this.state.desc_subtema3 = "";
                    contadorSubtema = this.state.contadorSubtema - 1
                    this.setState({ contadorSubtema: contadorSubtema ,subtemaEliminado3 : true, subtemaAgregado3: false})
                }else if (contadorSubtema == 4){
                    this.state.subtema4 = "";
                    this.state.desc_subtema4 = "";
                    contadorSubtema = this.state.contadorSubtema - 1
                    this.setState({ contadorSubtema: contadorSubtema ,subtemaEliminado4 : true, subtemaAgregado4: false})
                }else if (contadorSubtema == 5){
                    this.state.subtema5 = "";
                    this.state.desc_subtema5 = "";
                    contadorSubtema = this.state.contadorSubtema - 1
                    this.setState({ contadorSubtema: contadorSubtema ,subtemaEliminado5 : true, subtemaAgregado5: false})
                }else if (contadorSubtema == 6){
                    this.state.subtema6 = "";
                    this.state.desc_subtema6 = "";
                    contadorSubtema = this.state.contadorSubtema - 1
                    this.setState({ contadorSubtema: contadorSubtema ,subtemaEliminado6 : true, subtemaAgregado6: false})
                }else if (contadorSubtema == 7){
                    this.state.subtema7 = "";
                    this.state.desc_subtema7 = "";
                    contadorSubtema = this.state.contadorSubtema - 1
                    this.setState({ contadorSubtema: contadorSubtema ,subtemaEliminado7 : true, subtemaAgregado7: false})
                }else if (contadorSubtema == 8){
                    this.state.subtema8 = "";
                    this.state.desc_subtema8 = "";
                    contadorSubtema = this.state.contadorSubtema - 1
                    this.setState({ contadorSubtema: contadorSubtema ,subtemaEliminado8 : true, subtemaAgregado8: false})
                }else if (contadorSubtema == 9){
                    this.state.subtema9 = "";
                    this.state.desc_subtema9 = "";
                    contadorSubtema = this.state.contadorSubtema - 1
                    this.setState({ contadorSubtema: contadorSubtema ,subtemaEliminado9 : true, subtemaAgregado9: false})
                }
            }
        }else{
            if(contadorSubtema == 10 && !banInyecta){
                this.state.subtema10 = "";
                this.state.desc_subtema10 = "";
                contadorSubtema = this.state.contadorSubtema - 1
                this.setState({ contadorSubtema: contadorSubtema,subtemaEliminado10 : true, subtemaAgregado10: false})
            }
        }
    }

    limpiarElementos(){
        console.log("entro en liminar");
        if (!this.props.edicion) {
            this.state.tema = "";
            this.state.desc_tema = null;
            this.state.subtema = "";
            this.state.desc_subtema = null;
            this.state.subtema2 = "";
            this.state.desc_subtema2 = null;
            this.state.subtema3 = "";
            this.state.desc_subtema3 = null;
            this.state.subtema4 = "";
            this.state.desc_subtema4 = null;
            this.state.subtema5 = "";
            this.state.desc_subtema5 = null;
            this.state.subtema6 = "";
            this.state.desc_subtema6 = null;
            this.state.subtema7 = "";
            this.state.desc_subtema7 = null;
            this.state.subtema8 = "";
            this.state.desc_subtema8 = null;
            this.state.subtema9 = "";
            this.state.desc_subtema9 = null;
            this.state.subtema10 = "";
            this.state.desc_subtema10 = null;
            this.setState({ contadorSubtema: 1 ,subtemaEliminado2 : false, subtemaEliminado3 : false, subtemaEliminado4 : false, subtemaEliminado5 : false
                , subtemaEliminado6 : false, subtemaEliminado7 : false, subtemaEliminado8 : false, subtemaEliminado9 : false, subtemaEliminado10 : false })
            }
            this.props.cancelarModal("mostrarTablaCursosIDEAS");
    }

    onChange = e => {
        console.log(this.state.subtema+"1");
        console.log("entrando", e.target.name, e.target.value)
        this.setState({ [e.target.name]: e.target.value })
    }

    changeActivo() {
        this.setState({ activo: !this.state.activo })
    }


    guardarElemento() {
        console.log(this.state)
        let json = {
           // "entidad": "api_cat_curso_ideas",
            "tema": this.state.tema,
            "desc_tema": this.state.desc_tema == "" ? null : this.state.desc_tema,
            //"subtema": this.state.subtema,
            //"desc_subtema":this.state.desc_subtema ,
            "activo": this.state.activo ? 1 : 0,
            "user_id": parseInt(this.props.idUsuario),
            //"ts_alta_audit": "2014-04-20T14:57:00Z"
        }
        if (this.props.contenido != undefined) {
            console.log("via put")

            let url = datos.urlServicePy+"parametros_upd/api_cat_curso_ideas/" + this.state.idActualizar
            const requestOptions = {
                method: "PUT",
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(json)
            };
            fetch(url, requestOptions)
                .then(response => response.json())
                .then(data => { 
                    console.log("Entro en edición");
                    const requestOptionsDelete = {
                        method: "DELETE",
                        headers: { 'Content-Type': 'application/json' }
                    };
                    
                    if(this.state.subtemaEliminado1){
                        if(this.state.id_subtema != ""){
                            fetch(datos.urlServicePy+"parametros_upd/api_cat_curso_ideas_subtema/"+this.state.id_subtema, requestOptionsDelete)
                            .then(response => response.json())
                            .then(data => { })
                        }
                    }
                    if(this.state.subtemaEliminado2){
                        if(this.state.id_subtema2 != ""){
                            fetch(datos.urlServicePy+"parametros_upd/api_cat_curso_ideas_subtema/"+this.state.id_subtema2, requestOptionsDelete)
                            .then(response => response.json())
                            .then(data => { })
                        }
                    }
                    if(this.state.subtemaEliminado3){
                        if(this.state.id_subtema3 != ""){
                            fetch(datos.urlServicePy+"parametros_upd/api_cat_curso_ideas_subtema/"+this.state.id_subtema3, requestOptionsDelete)
                            .then(response => response.json())
                            .then(data => { })
                        }
                    }
                    if(this.state.subtemaEliminado4){
                        if(this.state.id_subtema4 != ""){
                            fetch(datos.urlServicePy+"parametros_upd/api_cat_curso_ideas_subtema/"+this.state.id_subtema4, requestOptionsDelete)
                            .then(response => response.json())
                            .then(data => { })
                        }
                    }
                    if(this.state.subtemaEliminado5){
                        if(this.state.id_subtema5 != ""){
                            fetch(datos.urlServicePy+"parametros_upd/api_cat_curso_ideas_subtema/"+this.state.id_subtema5, requestOptionsDelete)
                            .then(response => response.json())
                            .then(data => { })
                        }
                    }
                    if(this.state.subtemaEliminado6){
                        if(this.state.id_subtema6 != ""){
                            fetch(datos.urlServicePy+"parametros_upd/api_cat_curso_ideas_subtema/"+this.state.id_subtema6, requestOptionsDelete)
                            .then(response => response.json())
                            .then(data => { })
                        }
                    }
                    if(this.state.subtemaEliminado7){
                        if(this.state.id_subtema7 != ""){
                            fetch(datos.urlServicePy+"parametros_upd/api_cat_curso_ideas_subtema/"+this.state.id_subtema7, requestOptionsDelete)
                            .then(response => response.json())
                            .then(data => { })
                        }
                    }
                    if(this.state.subtemaEliminado8){
                        if(this.state.id_subtema8 != ""){
                            fetch(datos.urlServicePy+"parametros_upd/api_cat_curso_ideas_subtema/"+this.state.id_subtema8, requestOptionsDelete)
                            .then(response => response.json())
                            .then(data => { })
                        }
                    }
                    if(this.state.subtemaEliminado9){
                        if(this.state.id_subtema9 != ""){
                            fetch(datos.urlServicePy+"parametros_upd/api_cat_curso_ideas_subtema/"+this.state.id_subtema9, requestOptionsDelete)
                            .then(response => response.json())
                            .then(data => { })
                        }
                    }
                    if(this.state.subtemaEliminado10){
                        if(this.state.id_subtema10 != ""){
                            fetch(datos.urlServicePy+"parametros_upd/api_cat_curso_ideas_subtema/"+this.state.id_subtema10, requestOptionsDelete)
                            .then(response => response.json())
                            .then(data => { })
                        }
                    }

                    if(this.state.contadorSubtema == 0){
                        let jsonSinSubtema = {
                             "tema": this.state.tema,
                             "desc_tema": this.state.desc_tema,
                             "activo": this.state.activo ? 1 : 0,
                             "user_id": parseInt(this.props.idUsuario)
                         }
                        this.props.guardaElementoNuevo(datos.urlServicePy+"parametros_upd/api_cat_curso_ideas/" + this.state.idActualizar, jsonSinSubtema, "PUT", "mostrarTablaCursosIDEAS")
                    }

                    for (var i = 1; i < this.state.contadorSubtema + 1; i++) {
                        let json = {
                            "tema_id": data.id,
                            "subtema": "",
                            "desc_subtema": ""

                        }
                        let url = datos.urlServicePy+"parametros_upd/api_cat_curso_ideas_subtema/"
                        let banderaAgregarSubtema = false
                        if(i == 1){
                             json.subtema = this.state.subtema;
                             json.desc_subtema = this.state.desc_subtema == "" ? null : this.state.desc_subtema;
                             url = url + this.state.id_subtema;
                             banderaAgregarSubtema = this.state.id_subtema != "" ? this.state.subtemaAgregado1 : true;
                        }else if(i == 2){
                            json.subtema = this.state.subtema2;
                            json.desc_subtema = this.state.desc_subtema2 == "" ? null : this.state.desc_subtema2;
                            url = url + this.state.id_subtema2;
                            banderaAgregarSubtema = this.state.id_subtema2 != ""  ? this.state.subtemaAgregado2 : true;
                        }else if(i == 3){
                            json.subtema = this.state.subtema3;
                            json.desc_subtema = this.state.desc_subtema3 == "" ? null : this.state.desc_subtema3;
                            url = url + this.state.id_subtema3;
                            banderaAgregarSubtema = this.state.id_subtema3 != ""  ? this.state.subtemaAgregado3 : true;
                        }else if(i == 4){
                            json.subtema = this.state.subtema4;
                            json.desc_subtema = this.state.desc_subtema4 == "" ? null : this.state.desc_subtema4;
                            url = url + this.state.id_subtema4;
                            banderaAgregarSubtema = this.state.id_subtema4 != ""  ? this.state.subtemaAgregado4 : true;
                        }else if(i == 5){
                            json.subtema = this.state.subtema5;
                            json.desc_subtema = this.state.desc_subtema5 == "" ? null : this.state.desc_subtema5;
                            url = url + this.state.id_subtema5;
                            banderaAgregarSubtema = this.state.id_subtema5 != ""  ? this.state.subtemaAgregado5 : true;
                        }else if(i == 6){
                            json.subtema = this.state.subtema6;
                            json.desc_subtema = this.state.desc_subtema6 == "" ? null : this.state.desc_subtema6;
                            url = url + this.state.id_subtema6;
                            banderaAgregarSubtema = this.state.id_subtema6 != ""  ? this.state.subtemaAgregado6 : true;
                        }else if(i == 7){
                            json.subtema = this.state.subtema7;
                            json.desc_subtema = this.state.desc_subtema7 == "" ? null : this.state.desc_subtema7;
                            url = url + this.state.id_subtema7;
                            banderaAgregarSubtema = this.state.id_subtema7 != ""  ? this.state.subtemaAgregado7 : true;
                        }else if(i == 8){
                            json.subtema = this.state.subtema8;
                            json.desc_subtema = this.state.desc_subtema8 == "" ? null : this.state.desc_subtema8;
                            url = url + this.state.id_subtema8;
                            banderaAgregarSubtema = this.state.id_subtema8 != ""  ? this.state.subtemaAgregado8 : true;
                        }else if(i == 9){
                            json.subtema = this.state.subtema9;
                            json.desc_subtema = this.state.desc_subtema9 == "" ? null : this.state.desc_subtema9;
                            url = url + this.state.id_subtema9;
                            banderaAgregarSubtema = this.state.id_subtema9 != ""  ? this.state.subtemaAgregado9 : true;
                        }else if(i == 10){
                            json.subtema = this.state.subtema10;
                            json.desc_subtema = this.state.desc_subtema10 == "" ? null : this.state.desc_subtema10;
                            url = url + this.state.id_subtema10;
                            banderaAgregarSubtema = this.state.id_subtema10 != ""  ? this.state.subtemaAgregado10 :true;
                        }
                        
                        if(this.state.contadorSubtema == i){
                            if(banderaAgregarSubtema){
                                this.props.guardaElementoNuevo(datos.urlServicePy+"parametros/api_cat_curso_ideas_subtema/0", json, "POST", "mostrarTablaCursosIDEAS")
                            }else{
                                this.props.guardaElementoNuevo(url, json, "PUT", "mostrarTablaCursosIDEAS")
                            }
                        }else{
                            if(banderaAgregarSubtema){
                                const requestOptions = {
                                    method: "POST",
                                    headers: { 'Content-Type': 'application/json' },
                                    body: JSON.stringify(json)
                                };
                                fetch(datos.urlServicePy+"parametros/api_cat_curso_ideas_subtema/0", requestOptions)
                                    .then(response => response.json())
                                    .then(data => { }); 
                            }else{
                                const requestOptions = {
                                    method: "PUT",
                                    headers: { 'Content-Type': 'application/json' },
                                    body: JSON.stringify(json)
                                };
                                fetch(url, requestOptions)
                                    .then(response => response.json())
                                    .then(data => { });  
                            }  
                        }
                    }
                })
        } else {
            console.log("via post")
                if(this.state.contadorSubtema == 0){
                    this.props.guardaElementoNuevo(datos.urlServicePy+"parametros/api_cat_curso_ideas/0", json, "POST", "mostrarTablaCursosIDEAS")
                }else{
                    let url = datos.urlServicePy+"parametros/api_cat_curso_ideas/0"
                    const requestOptions = {
                        method: "POST",
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify(json)
                    };
                    fetch(url, requestOptions)
                    .then(response => response.json())
                    .then(data => { 
                        console.log("Entro en inserción");
                        const requestOptions = {
                            method: "POST",
                            headers: { 'Content-Type': 'application/json' },
                            body: JSON.stringify(json)
                        };
                        
                        for (var i = 1; i < this.state.contadorSubtema + 1; i++) {
                            let json = {
                                "tema_id": data.id,
                                "subtema": "",
                                "desc_subtema": ""
                            }
                            if(i == 1){
                                 json.subtema = this.state.subtema;
                                 json.desc_subtema = this.state.desc_subtema;
                            }else if(i == 2){
                                json.subtema = this.state.subtema2;
                                json.desc_subtema = this.state.desc_subtema2;
                            }else if(i == 3){
                                json.subtema = this.state.subtema3;
                                json.desc_subtema = this.state.desc_subtema3;
                            }else if(i == 4){
                                json.subtema = this.state.subtema4;
                                json.desc_subtema = this.state.desc_subtema4;
                            }else if(i == 5){
                                json.subtema = this.state.subtema5;
                                json.desc_subtema = this.state.desc_subtema5;
                            }else if(i == 6){
                                json.subtema = this.state.subtema6;
                                json.desc_subtema = this.state.desc_subtema6;
                            }else if(i == 7){
                                json.subtema = this.state.subtema7;
                                json.desc_subtema = this.state.desc_subtema7;
                            }else if(i == 8){
                                json.subtema = this.state.subtema8;
                                json.desc_subtema = this.state.desc_subtema8;
                            }else if(i == 9){
                                json.subtema = this.state.subtema9;
                                json.desc_subtema = this.state.desc_subtema9;
                            }else if(i == 10){
                                json.subtema = this.state.subtema10;
                                json.desc_subtema = this.state.desc_subtema10;
                            }
                            
                            if(this.state.contadorSubtema == i){
                                this.props.guardaElementoNuevo(datos.urlServicePy+"parametros/api_cat_curso_ideas_subtema/0", json, "POST", "mostrarTablaCursosIDEAS")
                            }else{
                            const requestOptions = {
                                method: "POST",
                                headers: { 'Content-Type': 'application/json' },
                                body: JSON.stringify(json)
                            };
                            fetch(datos.urlServicePy+"parametros/api_cat_curso_ideas_subtema/0", requestOptions)
                                .then(response => response.json())
                                .then(data => { });
                            }
                        }
                        
                    })
                }
        }
    }


    render() {
        return (
            <div>
                <div className="modal-body">
                    <div className="card-body">
                        <div className="row">
                            <div className="col-xs-5 col-lg-5">
                                <div className="form-group">
                                <div className="input-group">
                                        <span className="input-group-prepend">
                                            <span className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Parámetro activo</span>
                                            <div className="custom-control custom-switch custom-control-warning mr-2">
                                                <input type="checkbox" checked={this.state.activo} name="activo" class="custom-control-input"  ></input>
                                                <label class="custom-control-label" onClick={this.changeActivo} for="swc_warning"></label>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                        <div className="col-xs-5 col-lg-5">
                                <div className="form-group">
                                <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Tema</span>
                                        </span> <input type="text" name="tema" value={this.state.tema} onChange={this.onChange} className="form-control bg-dark border-dark text-white" />
                                    </div>
                                </div>
                            </div>
                            <div className="col-xs-6 col-lg-6">
                                    <div className="form-group">
                                        <div className="input-group">
                                            <span className="input-group-prepend"> <span
                                                className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Descripción del tema</span>
                                            </span> <textarea type="text" name="desc_tema" maxLength={250} value={this.state.desc_tema} onChange={this.onChange} className="form-control bg-dark border-dark text-white" style={{ borderColor: '#C8CDF6', backgroundColor: '#FFFF', height: '150px' }} />
                                        </div>
                                    </div>
                                </div>                        
                        </div>
                        { this.state.contadorSubtema >= 1 ?
                        <div className="row">
                            <div className="col-xs-5 col-lg-5">
                                    <div className="form-group">
                                        <div className="input-group">
                                            <span className="input-group-prepend"> <span
                                                className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Subtema</span>
                                            </span> <input type="text" name="subtema" value={this.state.subtema} onChange={this.onChange} className="form-control bg-dark border-dark text-white"/>
                                        </div>
                                    </div>
                                </div>
                            <div className="col-xs-6 col-lg-6">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Descrición del subtema</span>
                                        </span> <textarea type="text" name="desc_subtema"  maxLength={250} value={this.state.desc_subtema} onChange={this.onChange} className="form-control bg-dark border-dark text-white" style={{ borderColor: '#C8CDF6', backgroundColor: '#FFFF', height: '150px' }}/>
                                    </div>
                                </div>
                            </div>
                        </div> : ''
                        }
                        { this.state.contadorSubtema >= 2 ?
                                                <div className="row">
                                                <div className="col-xs-5 col-lg-5">
                                                        <div className="form-group">
                                                            <div className="input-group">
                                                                <span className="input-group-prepend"> <span
                                                                    className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Subtema</span>
                                                                </span> <input type="text" name="subtema2" value={this.state.subtema2} onChange={this.onChange} className="form-control bg-dark border-dark text-white"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <div className="col-xs-6 col-lg-6">
                                                    <div className="form-group">
                                                        <div className="input-group">
                                                            <span className="input-group-prepend"> <span
                                                                className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Descrición del subtema</span>
                                                            </span> <textarea type="text" name="desc_subtema2"  maxLength={250} value={this.state.desc_subtema2} onChange={this.onChange} className="form-control bg-dark border-dark text-white" style={{ borderColor: '#C8CDF6', backgroundColor: '#FFFF', height: '150px' }}/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> : ''
                        }
                        { this.state.contadorSubtema >= 3 ?
                        <div className="row">
                            <div className="col-xs-5 col-lg-5">
                                    <div className="form-group">
                                        <div className="input-group">
                                            <span className="input-group-prepend"> <span
                                                className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Subtema</span>
                                            </span> <input type="text" name="subtema3" value={this.state.subtema3} onChange={this.onChange} className="form-control bg-dark border-dark text-white"/>
                                        </div>
                                    </div>
                                </div>
                            <div className="col-xs-6 col-lg-6">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Descrición del subtema</span>
                                        </span> <textarea type="text" name="desc_subtema3"  maxLength={250} value={this.state.desc_subtema3} onChange={this.onChange} className="form-control bg-dark border-dark text-white" style={{ borderColor: '#C8CDF6', backgroundColor: '#FFFF', height: '150px' }}/>
                                    </div>
                                </div>
                            </div>
                        </div> : ''
                        }
                        { this.state.contadorSubtema >= 4 ?
                        <div className="row">
                            <div className="col-xs-5 col-lg-5">
                                    <div className="form-group">
                                        <div className="input-group">
                                            <span className="input-group-prepend"> <span
                                                className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Subtema</span>
                                            </span> <input type="text" name="subtema4" value={this.state.subtema4} onChange={this.onChange} className="form-control bg-dark border-dark text-white"/>
                                        </div>
                                    </div>
                                </div>
                            <div className="col-xs-6 col-lg-6">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Descrición del subtema</span>
                                        </span> <textarea type="text" name="desc_subtema4"  maxLength={250} value={this.state.desc_subtema4} onChange={this.onChange} className="form-control bg-dark border-dark text-white" style={{ borderColor: '#C8CDF6', backgroundColor: '#FFFF', height: '150px' }}/>
                                    </div>
                                </div>
                            </div>
                        </div> : ''
                        }
                        { this.state.contadorSubtema >= 5 ?
                        <div className="row">
                            <div className="col-xs-5 col-lg-5">
                                    <div className="form-group">
                                        <div className="input-group">
                                            <span className="input-group-prepend"> <span
                                                className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Subtema</span>
                                            </span> <input type="text" name="subtema5" value={this.state.subtema5} onChange={this.onChange} className="form-control bg-dark border-dark text-white"/>
                                        </div>
                                    </div>
                                </div>
                            <div className="col-xs-6 col-lg-6">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Descrición del subtema</span>
                                        </span> <textarea type="text" name="desc_subtema5"  maxLength={250} value={this.state.desc_subtema5} onChange={this.onChange} className="form-control bg-dark border-dark text-white" style={{ borderColor: '#C8CDF6', backgroundColor: '#FFFF', height: '150px' }}/>
                                    </div>
                                </div>
                            </div>
                        </div> : '' }
                        { this.state.contadorSubtema >= 6 ?
                        <div className="row">
                            <div className="col-xs-5 col-lg-5">
                                    <div className="form-group">
                                        <div className="input-group">
                                            <span className="input-group-prepend"> <span
                                                className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Subtema</span>
                                            </span> <input type="text" name="subtema6" value={this.state.subtema6} onChange={this.onChange} className="form-control bg-dark border-dark text-white"/>
                                        </div>
                                    </div>
                                </div>
                            <div className="col-xs-6 col-lg-6">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Descrición del subtema</span>
                                        </span> <textarea type="text" name="desc_subtema6"  maxLength={250} value={this.state.desc_subtema6} onChange={this.onChange} className="form-control bg-dark border-dark text-white" style={{ borderColor: '#C8CDF6', backgroundColor: '#FFFF', height: '150px' }}/>
                                    </div>
                                </div>
                            </div>
                        </div> : '' }
                        { this.state.contadorSubtema >= 7 ?
                        <div className="row">
                            <div className="col-xs-5 col-lg-5">
                                    <div className="form-group">
                                        <div className="input-group">
                                            <span className="input-group-prepend"> <span
                                                className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Subtema</span>
                                            </span> <input type="text" name="subtema7" value={this.state.subtema7} onChange={this.onChange} className="form-control bg-dark border-dark text-white"/>
                                        </div>
                                    </div>
                                </div>
                            <div className="col-xs-6 col-lg-6">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Descrición del subtema</span>
                                        </span> <textarea type="text" name="desc_subtema7"  maxLength={250} value={this.state.desc_subtema7} onChange={this.onChange} className="form-control bg-dark border-dark text-white" style={{ borderColor: '#C8CDF6', backgroundColor: '#FFFF', height: '150px' }}/>
                                    </div>
                                </div>
                            </div>
                        </div> : '' }
                        { this.state.contadorSubtema >= 8 ?
                        <div className="row">
                            <div className="col-xs-5 col-lg-5">
                                    <div className="form-group">
                                        <div className="input-group">
                                            <span className="input-group-prepend"> <span
                                                className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Subtema</span>
                                            </span> <input type="text" name="subtema8" value={this.state.subtema8} onChange={this.onChange} className="form-control bg-dark border-dark text-white"/>
                                        </div>
                                    </div>
                                </div>
                            <div className="col-xs-6 col-lg-6">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Descrición del subtema</span>
                                        </span> <textarea type="text" name="desc_subtema8"  maxLength={250} value={this.state.desc_subtema8} onChange={this.onChange} className="form-control bg-dark border-dark text-white" style={{ borderColor: '#C8CDF6', backgroundColor: '#FFFF', height: '150px' }}/>
                                    </div>
                                </div>
                            </div>
                        </div> : '' }
                        { this.state.contadorSubtema >= 9 ?
                        <div className="row">
                            <div className="col-xs-5 col-lg-5">
                                    <div className="form-group">
                                        <div className="input-group">
                                            <span className="input-group-prepend"> <span
                                                className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Subtema</span>
                                            </span> <input type="text" name="subtema9" value={this.state.subtema9} onChange={this.onChange} className="form-control bg-dark border-dark text-white"/>
                                        </div>
                                    </div>
                                </div>
                            <div className="col-xs-6 col-lg-6">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Descrición del subtema</span>
                                        </span> <textarea type="text" name="desc_subtema9"  maxLength={250} value={this.state.desc_subtema9} onChange={this.onChange} className="form-control bg-dark border-dark text-white" style={{ borderColor: '#C8CDF6', backgroundColor: '#FFFF', height: '150px' }}/>
                                    </div>
                                </div>
                            </div>
                        </div> : '' }
                        { this.state.contadorSubtema >= 10 ?
                        <div className="row">
                            <div className="col-xs-5 col-lg-5">
                                    <div className="form-group">
                                        <div className="input-group">
                                            <span className="input-group-prepend"> <span
                                                className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Subtema</span>
                                            </span> <input type="text" name="subtema10" value={this.state.subtema10} onChange={this.onChange} className="form-control bg-dark border-dark text-white"/>
                                        </div>
                                    </div>
                                </div>
                            <div className="col-xs-6 col-lg-6">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Descrición del subtema</span>
                                        </span> <textarea type="text" name="desc_subtema10"  maxLength={250} value={this.state.desc_subtema10} onChange={this.onChange} className="form-control bg-dark border-dark text-white" style={{ borderColor: '#C8CDF6', backgroundColor: '#FFFF', height: '150px' }}/>
                                    </div>
                                </div>
                            </div>
                        </div> : '' }

                        <div className="row">
                            <div className="col-xs-1 col-lg-1">
                                <button type="button" class="btn text-white" onClick={() => this.inyectaSubtema(true)} style={{ backgroundColor: "#0F69B8" }}>+ Agregar Subtema</button>
                            </div>
                            <div className="col-xs-1 col-lg-1">
                                <button type="button" class="btn text-white" onClick={() => this.inyectaSubtema(false)} style={{ backgroundColor: "#0F69B8" }}>- Eliminar Subtema</button>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-light" data-dismiss="modal" onClick={this.limpiarElementos}>Cancelar</button>
                    <button type="button" class="btn btn-warning" id="btnAgregar" data-dismiss="modal" disabled={this.state.tema.length < 1} onClick={this.guardarElemento} >Agregar</button>
                </div>

            </div>
        )
    }
}
