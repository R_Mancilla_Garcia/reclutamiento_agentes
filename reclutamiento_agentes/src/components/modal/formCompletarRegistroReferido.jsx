import React, { Component } from "react";
import axios from 'axios';
import datos from '../urls/datos.json'

export default class FormCompletarRegistroReferido extends Component {



    constructor() {
        super();
        this.onChange = this.onChange.bind(this)
        this.construyeSelect = this.construyeSelect.bind(this)
        this.onClickBotonArchivoCV = this.onClickBotonArchivoCV.bind(this)
        this.obtenEstatus=this.obtenEstatus.bind(this)
        this.guardarCambios = this.guardarCambios.bind(this)
        this.state = {
            expresionEmail: /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
            validaEmail: false,
            selectEDAT: [],
            selectFuenteReclutamiento: [],
            selectFuenteReclutamientoBono: [],
            selectAgenteReferidor:[],
            selectEstadoCivil: [],
            referido: {},
            colorSubirArchivoCV: "#E1E5F0",
            colorTextoSubirArchivoCV: "#617187",
            colorBotonSubirArchivoCV: "#0F69B8",
            nombreArchivoCV: "",
            archivoCV: undefined,
            muestraDocCV: false,
            muestraAgente: false,
            expresionRFC: /^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$/,
            validaRFC: false,
            estatus:''

        }
    }

    obtenEstatus(){
        fetch(datos.urlServicePy+"parametros/api_recluta_vreferidos/" + this.props.idEmpleado)
            .then(response => response.json())
            .then(json => {
                    console.log(json , "el estatus ")
                    this.setState({estatus:json.filas[0].fila[4].value})
            })
    }

    guardarCambios() {
        console.log(this.state.referido)
        this.state.referido.estatus_id = 1
        this.state.referido.tipo_persona="P"
        this.state.referido.user_id=this.props.idUsuario
        const requestOptions = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(this.state.referido)
        };

        fetch(datos.urlServicePy+"parametros_upd/api_recluta_referidos/" + this.state.referido.id, requestOptions)
            .then(response => response.json())
            .then(data => {
                
                if(this.state.referido.doc_cv == null || this.state.referido.doc_cv.length == 0 ){
                    if (this.state.archivoCV != undefined) {
                        let formData = new FormData();
                        formData.append('id', this.state.referido.id)
                        formData.append('cv', this.state.archivoCV)
                        formData.append('prospecto_id', this.state.referido.id)
                        const config = {
                            headers: { 'content-type': 'multipart/form-data' }
                        }
                        axios.post(datos.urlServicePy+"api/doc_prospectos/", formData, config)
                            .then(res => {
                                console.log(res.data);
                                console.log(this.state.filename);
                                console.log(formData);
                            })
    
                    }
                }else{
                    if (this.state.archivoCV != undefined) {
                        let formData = new FormData();
                        formData.append('id', this.state.referido.id)
                        formData.append('cv', this.state.archivoCV)
                        formData.append('prospecto_id', this.state.referido.id)
                        const config = {
                            headers: { 'content-type': 'multipart/form-data' }
                        }
                        axios.put(datos.urlServicePy+"api/doc_prospectos/", formData, config)
                            .then(res => {
                                console.log(res.data);
                                console.log(this.state.filename);
                                console.log(formData);
                            })
    
                    }
                }
                this.props.botonCancelar()
                /*let jsonNotificacion = {
                    "mail_id": 1,
                    "prospecto_id": this.state.referido.id
                }
                const requestOptions = {
                    method: "POST",
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(jsonNotificacion)
                };

                fetch(datos.urlServicePy+"recluta/api_notificador/0", requestOptions)
                    .then(response => response.json())
                    .then(data => { this.props.botonCancelar()
                })*/
            })


    }

    onChange = e => {
        let campoBeneficiario = e.target.name
        let referido = this.state.referido
        //
        if ("selectEDAT" == e.target.name) {
            referido.empleado_id = parseInt(e.target.value)
        } else if ("selectEstadoCivil" == e.target.name) {
            referido.estadocivil_id = parseInt(e.target.value)
        } else if("selectAgenteReferidor" == e.target.name){
            if(e.target.value != 0){
                let contenido=e.target.value
                let separacion=contenido.split(",")
                referido.agente_referidor=parseInt(separacion[0])
                referido.cua_referidor=separacion[2]    
            }else{
                referido.agente_referidor=0
                referido.cua_referidor=""
            }
            console.log("selector agente referido  ",e.target.value)
            
        } else if ("rfc" == e.target.name) {
            let rfc = e.target.value;
            if (rfc.match(this.state.expresionRFC)) {
                var rfcanio = rfc.substring(4, 6);
                var rfcmes = rfc.substring(6, 8);
                var rfcdia = rfc.substring(8, 10);
                var formatDate = "";

                if(rfcanio <= 20){
                    formatDate = "20" + rfcanio + "-";
                }else{
                    formatDate = "19" + rfcanio + "-";
                }
                formatDate = formatDate + rfcmes + "-" + rfcdia;
                referido.fec_nacimiento=formatDate;
                console.log("rfc");
                this.setState({ validaRFC: true })
            } else {
                referido.fec_nacimiento= "";
                this.setState({ validaRFC: false })
            }
            referido["" + campoBeneficiario + ""] = e.target.value

        } else if ("selectFuenteReclutamiento" == e.target.name) {
            referido.fuente_reclutamiento_id = parseInt(e.target.value)
            if(referido.fuente_reclutamiento_id == 14){
                console.log("Entro aqui");
                this.setState({ muestraAgente: true })
            }else{
                this.setState({ muestraAgente: false })
            }
        } else if ("selectFuenteReclutamientoBono" == e.target.name) {
            referido.fuente_reclutamiento_bonos_id = parseInt(e.target.value)
            referido.fuente_recluta_bonos_id = parseInt(e.target.value)
        } else if ("correo_electronico" == e.target.name) {
            
            let correo_electronico = e.target.value;
            if (correo_electronico.match(this.state.expresionEmail)) {
                this.setState({ validaEmail: true })
            } else {
                this.setState({ validaEmail: false })
            }
            referido["" + campoBeneficiario + ""] = e.target.value


        } else if (e.target.name == "tel_movil" || e.target.name=="tel_casa" || e.target.name=="tel_oficina" )   {
            
            let movil = e.target.value
            if (movil.length <= 10) {
                referido["" + e.target.name + ""] = e.target.value
            }


        } else{
            referido["" + campoBeneficiario + ""] = e.target.value
        }
        console.log("referido ",referido)
        this.setState({ referido: referido })

    }

    onChangeFileCV(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chingado file de CV ", file);
        this.setState({ archivoCV: file, colorSubirArchivoCV: "#78CB5A", colorTextoSubirArchivoCV: "#FFFFFF", colorBotonSubirArchivoCV: "#E5F6E0", nombreArchivoCV: file.name })


    }

    onClickBotonArchivoCV() {
        console.log("entrnaod a la CV")
        this.cv.click()
    }


    UNSAFE_componentWillMount() {
        fetch(datos.urlServicePy+"parametros/api_recluta_referidos/" + this.props.idEmpleado)
            .then(response => response.json())
            .then(json => {
                this.obtenEstatus()
                let fila = json.filas[0].fila
                let columnas = json.columnas
                let columnasSalida = {}
                for (var i = 0; i < fila.length; i++) {
                    console.log("entrada ",fila[i])
                    columnasSalida["" + columnas[i].key + ""] = fila[i].value
                }
                console.log(columnasSalida," entrada")
                let correo=columnasSalida.correo_electronico
                let rfc =columnasSalida.rfc
                let agenteReferidor = columnasSalida.fuente_reclutamiento_id != null ? parseInt(columnasSalida.fuente_reclutamiento_id) : 0;
                this.setState({ referido: columnasSalida ,validaEmail:correo != null ? correo.match(this.state.expresionEmail):false,validaRFC:rfc != null ? rfc.match(this.state.expresionRFC):false, muestraAgente : agenteReferidor == 14 ?  true : false})
                if (columnasSalida.doc_cv != null && columnasSalida.doc_cv.length > 0) {
                    this.setState({ colorSubirArchivoCV: "#78CB5A", colorTextoSubirArchivoCV: "#FFFFFF", colorBotonSubirArchivoCV: "#E5F6E0", nombreArchivoCV: "Reemplazar...", muestraDocCV: true })
                }
                this.construyeSelect(datos.urlServicePy+"recluta/api_vcat_edat/0", "selectEDAT")
                this.construyeSelect(datos.urlServicePy+"parametros/api_cat_fuente_reclutamiento/0", "selectFuenteReclutamiento")
                this.construyeSelect(datos.urlServicePy+"parametros/api_cat_fuente_reclutamiento_bonos/0", "selectFuenteReclutamientoBono")
                this.construyeSelect(datos.urlServicePy+"parametros/api_cat_estadocivil/0", "selectEstadoCivil")
                this.construyeSelectAgenteReferidor(datos.urlServicePy+"recluta/api_recluta_vagente_referidor/0","selectAgenteReferidor")
            })
    }

    construyeSelect(url, selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas =  selector == "selectEDAT" ? json:json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                for (var i = 0; i < filas.length; i++) {

                    let fila = filas[i]
                    if (selector == "selectEDAT") {
                        if (this.props.idEmpleado) {

                            if (this.state.referido.empleado_id == fila.id) {
                                options.push(<option selected value={fila.id}>{fila.nombre}</option>)
                            } else {
                                options.push(<option value={fila.id}>{fila.nombre}</option>)
                            }
                        } else {
                            options.push(<option value={fila.id}>{fila.nombre}</option>)
                        }

                    }



                    if (selector == "selectEstadoCivil") {
                        if (this.props.idEmpleado) {

                            if (this.state.referido.estadocivil_id == fila.fila[0].value) {
                                options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            } else {
                                options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            }
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }

                    }

                    if (selector == "selectFuenteReclutamientoBono") {
                        if (this.props.idEmpleado) {

                            if (this.state.referido.fuente_recluta_bonos_id == fila.fila[0].value) {
                                options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            } else {
                                options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            }
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }

                    }

                    if (selector == "selectFuenteReclutamiento") {
                        if (this.props.idEmpleado) {

                            if (this.state.referido.fuente_reclutamiento_id == fila.fila[0].value) {
                                options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            } else {
                                options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            }
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }

                    }

                    

 



                }
                let salida = []
                salida.push(<select onChange={this.onChange} name={selector} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ [selector]: salida /*,selectParentesco2: salida,selectParentesco3: salida*/ })
            });
    }

    construyeSelectAgenteReferidor(url, selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                for (var i = 0; i < filas.length; i++) {
                    let fila=filas[i]
                    if (selector == "selectAgenteReferidor") {
                        if (this.props.idEmpleado) {

                            if (this.state.referido.agente_referidor == fila.agente_referidor) {
                                options.push(<option selected value={fila.id+","+fila.agente_referidor+","+fila.cua_referidor}>{fila.agente_referidor}</option>)
                            } else {
                                options.push(<option  value={fila.id+","+fila.agente_referidor+","+fila.cua_referidor}>{fila.agente_referidor}</option>)
                            }
                        } else {
                            options.push(<option  value={fila.id+","+fila.agente_referidor+","+fila.cua_referidor}>{fila.agente_referidor}</option>)
                        }

                    }

                }
                let salida = []
                salida.push(<select onChange={this.onChange} name={selector} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ selectAgenteReferidor: salida  })
            })
        }


    render() {
        return (
            <div>

                <div class="card">
                    <div class="row mb-2 mt-2 ml-1">

                        <div class="col-12 col-lg-12">

                            <div class="float-right mr-3">
                                <button type="button" class="btn btn-light mr-3 text-white" style={{ backgroundColor: '#617187', borderColor: '#617187' }} onClick={(e) => this.props.botonCancelar()} >Cancelar</button>
                                <button type="button" class="btn btn-warning text-white" style={{ backgroundColor: '#ED6C26', borderColor: '#ED6C26' }} onClick={this.guardarCambios} disabled={
                                    this.state.referido.nombre == ''
                                    ||
                                    this.state.referido.ape_paterno == ''
                                    ||
                                    this.state.validaRFC == false
                                    ||
                                    this.state.referido.estadocivil_id == 0 || this.state.referido.estadocivil_id == null 
                                    ||
                                    this.state.referido.fec_envio_psp == "" || this.state.referido.fec_envio_psp == null
                                    ||
                                    this.state.validaEmail == false
                                    ||
                                    (this.state.referido.tel_movil+'').length == "" || this.state.referido.tel_movil == null
                                    ||
                                    this.state.referido.fuente_reclutamiento_id == 0 || this.state.referido.fuente_reclutamiento_id == null
                                    ||
                                    this.state.referido.fuente_recluta_bonos_id == 0 || this.state.referido.fuente_recluta_bonos_id == null
                                    ||
                                    this.state.referido.empleado_id == 0 || this.state.referido.empleado_id == null
                                    ||
                                    this.state.referido.nombre_aps == "" || this.state.referido.nombre_aps == null
                                } >Registrar</button>
                            </div>

                        </div>
                    </div>
                </div>




                <div className="row">
                    <div className="col-xs-12 col-lg-2">
                        <div className="form-group">
                            <div className="input-group">
                                <h4 class="card-title py-3 font-weight-bold" >{"Referido " + this.props.idEmpleado} </h4>

                            </div>
                        </div>
                    </div>
                </div>


                <div className="row">

                    <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fecha alta</span>
                                </span> <input type="text" disabled className="form-control " value={this.state.referido.ts_alta_audit} />
                            </div>
                        </div>
                    </div>

                    <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Usuario creador</span>
                                </span> <input type="text" disabled value={this.state.referido.user_id} className="form-control " />
                            </div>
                        </div>
                    </div>

                    <div className="col-xs-12 col-lg-2">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <h6
                                    className="" style={{ color: '#617187' }} >Estatus del proceso de reclutamiento</h6>
                                </span>
                            </div>
                        </div>
                    </div>


                    <div className="col-xs-12 col-lg-2">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <h6
                                    style={{ backgroundColor: "#C28EE3", color: '#FFFF', borderRadius: '8px' }}> <>&nbsp;&nbsp;</> {this.state.estatus}  <>&nbsp;&nbsp;</> </h6>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                {/* informacion general ************************************************************************************** */}

                <br></br>


                <div className="row">
                    <div className="col-xs-12 col-lg-2">
                        <div className="form-group">
                            <div className="input-group">
                                <h4 class="card-title py-3 font-weight-bold" >Información general</h4>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Nombre (s)</span>
                                </span> <input type="text" placeholder="Nombre" name="nombre" onChange={this.onChange} value={this.state.referido.nombre} className="form-control " />
                            </div>
                            {
                                this.state.referido.nombre == "" || this.state.referido.nombre == null ?
                                    <span className="" style={{ color: "red" }}>El nombre es un campo requerido</span> : ''
                            }
                        </div>
                    </div>

                    <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Apellido paterno</span>
                                </span> <input type="text" placeholder="Apellido" name="ape_paterno" onChange={this.onChange} value={this.state.referido.ape_paterno} className="form-control " />
                            </div>
                            {
                                this.state.referido.ape_paterno == "" || this.state.referido.ape_paterno == null ?
                                    <span className="" style={{ color: "red" }}>El apellido paterno es un campo requerido</span> : ''
                            }
                        </div>
                    </div>


                    <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Apellido materno</span>
                                </span> <input type="text" placeholder="Apellido" className="form-control " name="ape_materno" onChange={this.onChange} value={this.state.referido.ape_materno} />
                            </div>
                            {
                                this.state.referido.ape_materno == "" || this.state.referido.ape_materno == null ?
                                    <span className="" style={{ color: "red" }}>El apellido materno es un campo requerido</span> : ''
                            }
                        </div>
                    </div>
                </div>

                <div className="row">
                   
                   
                <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>RFC</span>
                                </span> <input type="text" placeholder="Escribir" className="form-control " name="rfc" onChange={this.onChange} value={this.state.referido.rfc} />
                            </div>
                            {
                                this.state.validaRFC == false ?
                                    <span className="" style={{ color: "red" }}>El rfc es un campo requerido</span> : ''
                            }
                        </div>
                    </div>

                    <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fecha de nacimiento</span>
                                </span> <input type="date" className="form-control " name="fec_nacimiento" max="2100-01-01" onChange={this.onChange} value={this.state.referido.fec_nacimiento} />
                            </div>
                            {
                                this.state.referido.fec_nacimiento == "" || this.state.referido.fec_nacimiento == null || this.state.referido.fec_nacimiento.length <= 0 ?
                                    <span className="" style={{ color: "red" }}>La fecha de nacimiento es un campo requerido</span> : ''
                            }

                        </div>
                    </div>

                    


                    <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Estado civil</span>
                                </span> {this.state.selectEstadoCivil}
                            </div>
                            {
                                this.state.referido.estadocivil_id == 0 || this.state.referido.estadocivil_id == null ?
                                    <span className="" style={{ color: "red" }}>El estado civil  es un campo requerido</span> : ''
                            }
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <form encType="multipart/form" style={{ display: 'none' }} >
                                    <input type="file" style={{ display: 'none' }} ref={(ref) => this.cv = ref} onChange={this.onChangeFileCV.bind(this)}></input>
                                </form>
                                <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " placeholder="Curriculum Vitae" value={this.state.nombreArchivoCV} />
                                <span className="input-group-prepend">
                                    <button type="button" class="btn text-white" onClick={this.onClickBotonArchivoCV} style={{ backgroundColor: this.state.colorBotonSubirArchivoCV }}>
                                        <h10 style={{ color: this.state.colorSubirArchivoCV }}>+</h10>
                                    </button>

                                    {
                                        this.state.muestraDocCV == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivo }}>
                                            <a href={this.state.referido.doc_cv} target="_blank" rel="noopener noreferrer" >
                                                <i className="fas fa-eye"
                                                    style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                            </a>
                                        </button>
                                            : ''
                                    }

                                    <span className="input-group-text" style={{ background: this.state.colorSubirArchivoCV, borderColor: this.state.colorSubirArchivoCV, color: this.state.colorTextoSubirArchivoCV }} >Subir archivo</span>
                                </span>
                            </div>

                            {
                                //this.state.archivoCV == undefined || this.state.archivoCV == null ?
                                 //   <span className="" style={{ color: "red" }}>El CV  es un campo requerido</span> : ''
                            }

                        </div>
                    </div>

                    <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Envío de link PSP</span>
                                </span> <input type="date" className="form-control " name="fec_envio_psp" max="2100-01-01" onChange={this.onChange} value={this.state.referido.fec_envio_psp} />
                            </div>
                            {
                                this.state.referido.fec_envio_psp == "" || this.state.referido.fec_envio_psp == null ?
                                    <span className="" style={{ color: "red" }}>La fecha de envio del PSP es un campo requerido</span> : ''
                            }
                        </div>
                    </div>


                    <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Correo electrónico</span>
                                </span> <input type="text" placeholder="ejemplo@correo.com" className="form-control" name="correo_electronico" onChange={this.onChange} value={this.state.referido.correo_electronico} />
                            </div>
                            {
                                this.state.validaEmail == false ?
                                    <span className="" style={{ color: "red" }}>El correo electrónico es un campo requerido</span> : ''
                            }
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Teléfono móvil</span>
                                </span> <input type="number" placeholder="55 0000 0000" className="form-control " name="tel_movil" onChange={this.onChange} value={this.state.referido.tel_movil} />
                            </div>
                            {
                                (this.state.referido.tel_movil+'').length == "" || (this.state.referido.tel_movil+'').length <= 9 || this.state.referido.tel_movil == null ?
                                    <span className="" style={{ color: "red" }}>El teléfono móvil es un campo requerido</span> : ''
                            }


                        </div>
                    </div>

                    <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Teléfono casa</span>
                                </span> <input type="number" placeholder="10 dígitos" className="form-control" name="tel_casa" onChange={this.onChange} value={this.state.referido.tel_casa} />
                            </div>
                        </div>
                    </div>


                    <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Teléfono oficina</span>
                                </span> <input type="number" placeholder="10 dígitos" className="form-control " name="tel_oficina" onChange={this.onChange} value={this.state.referido.tel_oficina} />
                            </div>
                        </div>
                    </div>
                </div>


                {/* informacion general ************************************************************************************** */}

                <br></br>


                <div className="row">
                    <div className="col-xs-12 col-lg-2">
                        <div className="form-group">
                            <div className="input-group">
                                <h4 class="card-title py-3 font-weight-bold" >Información complementaría</h4>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fuente de reclutamiento</span>
                                </span> {this.state.selectFuenteReclutamiento}
                            </div>
                            {
                                this.state.referido.fuente_reclutamiento_id == 0 || this.state.referido.fuente_reclutamiento_id == null ?
                                    <span className="" style={{ color: "red" }}>La fuente de reclutamiento es un campo requerido</span> : ''
                            }
                        </div>
                    </div>

                    <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fuente de reclutamiento para bonos</span>
                                </span> {this.state.selectFuenteReclutamientoBono} {/* mover este state al correcto */}
                            </div>
                            {
                                this.state.referido.fuente_recluta_bonos_id == 0 || this.state.referido.fuente_recluta_bonos_id == null ?
                                    <span className="" style={{ color: "red" }}>La fuente de reclutamiento para bonos es un campo requerido</span> : ''
                            }
                        </div>
                    </div>


                    <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>EDAT asignado</span>
                                </span> {this.state.selectEDAT}
                            </div>
                            {
                                this.state.referido.empleado_id == 0 || this.state.referido.empleado_id == null ?
                                    <span className="" style={{ color: "red" }}>El EDAT asignado es un campo requerido</span> : ''
                            }
                        </div>
                    </div>
                </div>

                { this.state.muestraAgente ?
                <div className="row">
                    <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Agente referidor</span>
                                </span> {/*<input type="text" placeholder="Escribir" className="form-control " />*/}{this.state.selectAgenteReferidor}
                            </div>
                            {
                                this.state.referido.agente_referidor == 0 || this.state.referido.agente_referidor == null ?
                                    <span className="" style={{ color: "red" }}>El agente referidor es un campo requerido</span> : ''
                            }
                        </div>

                    </div>
                    
                        <div className="col-xs-12 col-lg-4">
                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>CUA referidora para reportar a GNP</span>
                                    </span> <input type="text" placeholder="Escribir" className="form-control" disabled={true} name="cua_referidor" onChange={this.onChange} value={this.state.referido.cua_referidor} />
                                </div>
                            </div>
                        </div>
                        
                   

                    <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Nombre APS</span>
                                </span> <input type="text" placeholder="Escribir" className="form-control " name="nombre_aps" onChange={this.onChange} value={this.state.referido.nombre_aps} />
                            </div>
                            {
                                this.state.referido.nombre_aps == "" || this.state.referido.nombre_aps == null ?
                                    <span className="" style={{ color: "red" }}>El Nombre APS es un campo requerido</span> : ''
                            }
                        </div>
                    </div>
                </div>
                :
                <div className="row">
                    <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Nombre APS</span>
                                </span> <input type="text" placeholder="Escribir" className="form-control " name="nombre_aps" onChange={this.onChange} value={this.state.referido.nombre_aps} />
                            </div>
                            {
                                this.state.referido.nombre_aps == "" || this.state.referido.nombre_aps == null ?
                                    <span className="" style={{ color: "red" }}>El Nombre APS es un campo requerido</span> : ''
                            }
                        </div>
                    </div>
                </div>

                }

                
                









            </div>
        )
    }

}