import React, { Component } from "react";
import datos from '../urls/datos.json'

export default class FormMailGrupoDestino extends Component {

    constructor() {
        super();
        this.guardarElemento=this.guardarElemento.bind(this)
        this.onChange = this.onChange.bind(this)
        this.construyeSelect=this.construyeSelect.bind(this)
        this.state = {
            validaEmail:false,
            expresionEmail: /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
            observaciones:'',
            imail:'',
            idActualizar:0,
            selectGrupoDestino:[],
            idGrupoDestino:0
            
        }
    }


    UNSAFE_componentWillMount() {
        this.construyeSelect(datos.urlServicePy+"parametros/api_grupo_destino/0","selectGrupoDestino")
        if(this.props.contenido != undefined){
            console.log("entrando a form ",this.props.contenido)
            let correo_electronico=this.props.contenido[3].value
            this.setState({
               idActualizar:this.props.contenido[0].value,
               idGrupoDestino :this.props.contenido[1].value,
               observaciones:this.props.contenido[2].value,
               imail:this.props.contenido[3].value,
               validaEmail: correo_electronico.match(this.state.expresionEmail)
                
            })
        }

    }

    onChange = e => {
        console.log("entrando", e.target.name, e.target.value)
        if( e.target.name == "selectGrupoDestino"){
            this.setState({ idGrupoDestino: parseInt(e.target.value) })
        }else if(e.target.name == "imail"){
            let correo_electronico = e.target.value;
            if (correo_electronico.match(this.state.expresionEmail)) {
                this.setState({ validaEmail: true })
            } else {
                this.setState({ validaEmail: false })
            }
            this.setState({ [e.target.name]: e.target.value })
        }else{
            this.setState({ [e.target.name]: e.target.value })
        }
    }


    guardarElemento(){
        console.log(this.props)
        let json={
            "observaciones":this.state.observaciones,
            "imail":this.state.imail,
            "grupo_destino_id":this.state.idGrupoDestino,
            "user_id":parseInt(this.props.idUsuario),
            }
            if(this.props.contenido != undefined){
                this.props.guardaElementoNuevo(datos.urlServicePy+"parametros_upd/api_mail_grupo_destino/"+this.state.idActualizar,json,"PUT","mostrarTablaParametrosEmailGrupoDestino")
            }else{
            this.props.guardaElementoNuevo(datos.urlServicePy+"parametros/api_mail_grupo_destino/0",json,"POST","mostrarTablaParametrosEmailGrupoDestino")
            }
    }

    construyeSelect(url,selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                console.log("respuesta  ", filas)
                for (var i = 0; i < filas.length; i++) {
                    let fila = filas[i]
                    console.log("fila ", fila.fila[0].value)
                    //options.push(<option value={fila.id}>{fila.empleado}</option>)

                    if (selector == "selectGrupoDestino") {
                        if (this.state.idGrupoDestino == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }
                    }
                }
                let salida = []  
                salida.push(<select onChange={this.onChange} name={selector} style={{ borderColor: '#F1F3FA' }} class="form-control bg-dark border-dark text-white" aria-label="Default select example">{options}</select>)
                this.setState({ selectGrupoDestino: salida })


            })
    }



    render() {
        return (
            <div>
                <div className="modal-body">
                    <div className="card-body">
                        <div className="row">
                            
                            <div className="col-xs-12 col-lg-3">
                                <div className="form-group">

                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Observaciones</span>
                                        </span> <input type="text" name="observaciones" value={this.state.observaciones} onChange={this.onChange} className="form-control bg-dark border-dark text-white" />
                                    </div>
                                </div>
                            </div>


                            <div className="col-xs-12 col-lg-3">
                                <div className="form-group">

                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Email</span>
                                        </span> <input type="text" name="imail" value={this.state.imail} onChange={this.onChange} className="form-control bg-dark border-dark text-white" />
                                    </div>
                                    {
                                        /*this.state.validaEmail == false*/ this.state.imail == "" || this.state.imail == null ?
                                            <span className="" style={{ color: "red" }}>El mail es un campo requerido</span> : ''
                                    }
                                </div>
                            </div>

                            <div className="col-xs-12 col-lg-3">
                                <div className="form-group">

                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Email</span>
                                        </span> {this.state.selectGrupoDestino}
                                    </div>
                                </div>
                            </div>

                            



                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-warning" id="btnAgregar" data-dismiss="modal" disabled={this.state.observaciones.length == 0  || /*this.state.validaEmail == false*/ this.state.imail == "" || this.state.imail == null || this.state.idGrupoDestino == 0 }  onClick={this.guardarElemento} >Agregar</button>
                </div>
            </div>
        )
    }


}