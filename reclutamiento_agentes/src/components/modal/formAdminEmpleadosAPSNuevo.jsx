import React, { Component } from "react";
import axios from 'axios';
import datos from '../urls/datos.json'

const avatar = require("../imagenes/avatar.png")

export default class FormAdminEmpleadosAPSNuevo extends Component {


    constructor() {
        super();
        this.onChange = this.onChange.bind(this)
        this.construyeSelect = this.construyeSelect.bind(this)
        this.construyeSelectDependienteDeCiudad = this.construyeSelectDependienteDeCiudad.bind(this)
        this.construyeSelectDependienteDeMunicipio = this.construyeSelectDependienteDeMunicipio.bind(this)
        this.determinaEstadoCivil = this.determinaEstadoCivil.bind(this)
        this.onClickImagen = this.onClickImagen.bind(this)
        this.onChangeEstadosConyugales = this.onChangeEstadosConyugales.bind(this)
        this.desactivaCheck = this.desactivaCheck.bind(this)
        this.clickSiguiente = this.clickSiguiente.bind(this)
        this.inyectaHijos = this.inyectaHijos.bind(this)
        this.onChangeHijos = this.onChangeHijos.bind(this)
        this.validaExistenciaAlgunArchivo = this.validaExistenciaAlgunArchivo.bind(this)
        this.state = {
            selectCiudad: undefined,
            selectMunicipio: undefined,
            selectColonia: undefined,
            selectParentesco: undefined,
            estadosConyugales: [true, false, false, false, false],
            imagenEnCambio: undefined,
            fotografiaFile: undefined,
            expresionRFC: /^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$/,
            expresionCURP: /^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/,
            validaCURP: false,
            validaRFC: false,
            contadorHijos: 1,
            hijo1: {},
            hijo1Edicion: false,
            hijo2: {},
            hijo2Edicion: false,
            hijo3: {},
            hijo3Edicion: false,
            hijo4: {},
            hijo4Edicion: false,
            hijo5: {},
            hijo5Edicion: false,

            doc_foto: '',
            nombre: null,
            ape_paterno: null,
            ape_materno: null,
            rfc: null,
            curp: null,
            fec_nacimiento: null,
            codigo_postal: null,
            ciudad_id: 0,
            municipio_id: 0,
            colonia_id: 0,
            calle: null,
            num_ext: 0,
            num_int: 0,
            tel_casa: 0,
            tel_movil: 0,
            estadocivil_id: 1,
            nombre_conyugue: null,
            fec_nacimiento_conyugue: null,
            fec_aniversario: null,
            hijos_id: 0,
            nombre_contacto: null,
            parentesco_id: null,
            tel_contacto: 0,
            estatusbaja_id: null,
            fec_baja: null,
            causabaja_id: null,
            fec_ingreso: null,
            escolaridad_id: null,
            profesion_id: null,
            puesto_id: null,
            doc_puesto: "",
            acceso_pai: 0,
            figura_id: null,
            area_id: null,
            supervisor: null,
            doc_cv: "",
            doc_psp: "",
            sueldo: 0,
            banco_id: null,
            cta_clabe: null,
            sociedad_id: null,
            activo: 0,
            correo_electronico: null,
            otros_correos: null,
            fecha_baja_correos: null,
            num_extension: null,
            dias_vacaciones: 0,
            acceso_impresion: 0,
            acceso_copiadora: 0,
            ts_alta_audit: null,
            ts_actualiza_audit: null,
            user_id: 0,
            hijo_Eliminado2: false,
            hijo_Eliminado3: false,
            hijo_Eliminado4: false,
            hijo_Eliminado5: false
        }

    }

    UNSAFE_componentWillMount() {
        console.log("component prmier form", this.props)
        if (this.props.idEmpleado == null) {//agregar nuevo
            this.construyeSelect(datos.urlServicePy+"parametros/api_cat_ciudad/0", "selectCiudad", 8)
            this.construyeSelect(datos.urlServicePy+"parametros/api_cat_parentesco/0", "selectParentesco", 20)
            //this.construyeSelectDependienteDeCiudad(datos.urlServicePy+"parametros/api_cat_municipio/0", "selectMunicipio")
            //this.construyeSelectDependienteDeMunicipio(datos.urlServicePy+"parametros/api_cat_colonia/0", "selectColonia")
        } else {// editar
            fetch(datos.urlServicePy+"parametros/api_cat_empleados/" + this.props.idEmpleado)
                .then(response => response.json())
                .then(json => {
                    console.log("recuperando el id ", json)
                    let fila = json.filas[0].fila
                    let columnas = json.columnas
                    let columnasSalida = {}
                    for (var i = 0; i < fila.length; i++) {
                        console.log(fila[i])
                        columnasSalida["" + columnas[i].key + ""] = fila[i].value
                    }
                    console.log("el json a entrar ", columnasSalida)
                    let curp = columnasSalida.curp
                    let rfc = columnasSalida.rfc
                    let estadocivil = columnasSalida.estadocivil_id
                    
                    let estados = this.state.estadosConyugales;
                    if(estadocivil == 1){
                        estados[0] = true
                        estados[1] = false
                        estados[2] = false
                        estados[3] = false
                        estados[4] = false
                    }else if(estadocivil == 2){
                        estados[0] = false
                        estados[1] = true
                        estados[2] = false
                        estados[3] = false
                        estados[4] = false
                    }else if(estadocivil == 3){
                        estados[0] = false
                        estados[1] = false
                        estados[2] = true
                        estados[3] = false
                        estados[4] = false
                    }else if(estadocivil == 4){
                        estados[0] = false
                        estados[1] = false
                        estados[2] = false
                        estados[3] = true
                        estados[4] = false
                    }else if(estadocivil == 5){
                        estados[0] = false
                        estados[1] = false
                        estados[2] = false
                        estados[3] = false
                        estados[4] = true
                    }

                    this.setState({
                        estadosConyugales: estados ,
                        validaCURP: curp.match(this.state.expresionCURP),
                        validaRFC: rfc.match(this.state.expresionRFC),
                        doc_foto: columnasSalida.doc_foto,
                        nombre: columnasSalida.nombre,
                        ape_paterno: columnasSalida.ape_paterno,
                        ape_materno: columnasSalida.ape_materno,
                        rfc: columnasSalida.rfc,
                        curp: columnasSalida.curp,
                        fec_nacimiento: columnasSalida.fec_nacimiento,
                        codigo_postal: columnasSalida.codigo_postal,
                        ciudad_id: columnasSalida.ciudad_id,
                        municipio_id: columnasSalida.municipio_id,
                        colonia_id: columnasSalida.colonia_id,
                        calle: columnasSalida.calle,
                        num_ext: columnasSalida.num_ext,
                        num_int: columnasSalida.num_int,
                        tel_casa: columnasSalida.tel_casa,
                        tel_movil: columnasSalida.tel_movil,
                        estadocivil_id: columnasSalida.estadocivil_id,
                        nombre_conyugue: columnasSalida.nombre_conyugue,
                        fec_nacimiento_conyugue: columnasSalida.fec_nacimiento_conyugue,
                        fec_aniversario: columnasSalida.fec_aniversario,
                        hijos_id: null,
                        nombre_contacto: columnasSalida.nombre_contacto,
                        parentesco_id: columnasSalida.parentesco_id,
                        tel_contacto: columnasSalida.tel_contacto,
                        estatusbaja_id: columnasSalida.estatusbaja_id,
                        fec_baja: columnasSalida.fec_baja,
                        causabaja_id: columnasSalida.causabaja_id,
                        fec_ingreso: columnasSalida.fec_ingreso,
                        escolaridad_id: columnasSalida.escolaridad_id,
                        profesion_id: columnasSalida.profesion_id,
                        puesto_id: columnasSalida.puesto_id,
                        doc_puesto: columnasSalida.doc_puesto,
                        acceso_pai: columnasSalida.acceso_pai,
                        figura_id: columnasSalida.figura_id,
                        area_id: columnasSalida.area_id,
                        supervisor: columnasSalida.supervisor,
                        doc_cv: columnasSalida.doc_cv,
                        doc_psp: columnasSalida.doc_psp,
                        sueldo: columnasSalida.sueldo,
                        banco_id: columnasSalida.banco_id,
                        cta_clabe: columnasSalida.cta_clabe,
                        sociedad_id: columnasSalida.sociedad_id,
                        activo: columnasSalida.activo,
                        correo_electronico: columnasSalida.correo_electronico,
                        otros_correos: columnasSalida.otros_correos,
                        fecha_baja_correos: columnasSalida.fecha_baja_correos,
                        num_extension: columnasSalida.num_extension,
                        dias_vacaciones: columnasSalida.dias_vacaciones,
                        acceso_impresion: columnasSalida.acceso_impresion,
                        acceso_copiadora: columnasSalida.acceso_copiadora,
                        ts_alta_audit: columnasSalida.ts_alta_audit,
                        ts_actualiza_audit: columnasSalida.ts_actualiza_audit,
                        user_id: columnasSalida.user_id
                    })
                    this.construyeSelect(datos.urlServicePy+"parametros/api_cat_ciudad/0", "selectCiudad", 8)
                    this.construyeSelect(datos.urlServicePy+"parametros/api_cat_parentesco/0", "selectParentesco", 20)
                    this.construyeSelectDependienteDeCiudad(datos.urlServicePy+"parametros/api_cat_municipio/0", "selectMunicipio", 9)
                    fetch(datos.urlServicePy+"parametros/api_cat_empleados_hijos/" + this.props.idEmpleado)
                        .then(response => response.json())
                        .then(json => {
                            console.log(json, 'hijos')
                            if (json.filas.length > 0) {
                                this.setState({ contadorHijos: json.filas.length })
                                let columnas = json.columnas
                                let filaHijo1 = json.filas[0].fila != undefined ? json.filas[0].fila : undefined
                                let filaHijo2 = json.filas[1] != undefined ? json.filas[1].fila : undefined
                                let filaHijo3 = json.filas[2] != undefined ? json.filas[2].fila : undefined

                                let filaHijo4 = json.filas[3] != undefined ? json.filas[3].fila : undefined
                                let filaHijo5 = json.filas[4] != undefined ? json.filas[4].fila : undefined

                                let columnasHijo1 = {}
                                if (filaHijo1 != undefined) {
                                    for (var i = 0; i < filaHijo1.length; i++) {
                                        console.log("hijos ", filaHijo1[i])
                                        columnasHijo1["" + columnas[i].key + ""] = filaHijo1[i].value
                                    }
                                    this.setState({ hijo1: columnasHijo1, hijo1Edicion: true })
                                } else {
                                    this.setState({ hijo1: columnasHijo1, hijo1Edicion: false })
                                }

                                let columnasHijo2 = {}
                                if (filaHijo2 != undefined) {
                                    for (var i = 0; i < filaHijo2.length; i++) {
                                        console.log("hijos ", filaHijo2[i])
                                        columnasHijo2["" + columnas[i].key + ""] = filaHijo2[i].value
                                    }
                                    this.setState({ hijo2: columnasHijo2, hijo2Edicion: true })
                                } else {
                                    this.setState({ hijo2: columnasHijo2, hijo2Edicion: false })
                                }

                                let columnasHijo3 = {}
                                if (filaHijo3 != undefined) {
                                    for (var i = 0; i < filaHijo3.length; i++) {
                                        console.log("hijos ", filaHijo3[i])
                                        columnasHijo3["" + columnas[i].key + ""] = filaHijo3[i].value
                                    }
                                    this.setState({ hijo3: columnasHijo3, hijo3Edicion: true })
                                } else {
                                    this.setState({ hijo3: columnasHijo3, hijo3Edicion: false })
                                }

                                let columnasHijo4 = {}
                                if (filaHijo4 != undefined) {
                                    for (var i = 0; i < filaHijo4.length; i++) {
                                        console.log("hijos ", filaHijo4[i])
                                        columnasHijo4["" + columnas[i].key + ""] = filaHijo4[i].value
                                    }
                                    this.setState({ hijo4: columnasHijo4, hijo4Edicion: true })
                                } else {
                                    this.setState({ hijo4: columnasHijo4, hijo4Edicion: false })
                                }

                                let columnasHijo5 = {}
                                if (filaHijo5 != undefined) {
                                    for (var i = 0; i < filaHijo5.length; i++) {
                                        console.log("hijos ", filaHijo5[i])
                                        columnasHijo5["" + columnas[i].key + ""] = filaHijo5[i].value
                                    }
                                    this.setState({ hijo5: columnasHijo5, hijo5Edicion: true })
                                } else {
                                    this.setState({ hijo5: columnasHijo5, hijo5Edicion: false })
                                }

                            }

                        })
                })
        }

    }


    inyectaHijos(banInyecta) {
        console.log("inyeccion")
        let contadorHijos = this.state.contadorHijos

        if (contadorHijos >= 1 && contadorHijos < 5) {
            if (banInyecta) {
                contadorHijos = this.state.contadorHijos + 1
                this.setState({ contadorHijos: contadorHijos })
                if (this.state.hijo_Eliminado2) {
                    this.setState({ hijo_Eliminado2: false })
                }
                if (this.state.hijo_Eliminado3) {
                    this.setState({ hijo_Eliminado3: false })
                }
                if (this.state.hijo_Eliminado4) {
                    this.setState({ hijo_Eliminado4: false })
                }
                if (this.state.hijo_Eliminado5) {
                    this.setState({ hijo_Eliminado5: false })
                }
            } else {
                if (contadorHijos == 2) {
                    contadorHijos = this.state.contadorHijos - 1
                    this.setState({ contadorHijos: contadorHijos, hijo_Eliminado2: true })
                    this.state.hijo2.nombre_completo = "";
                    this.state.hijo2.fec_nacimiento = "";
                } else if (contadorHijos == 3) {
                    contadorHijos = this.state.contadorHijos - 1
                    this.setState({ contadorHijos: contadorHijos, hijo_Eliminado3: true })
                    this.state.hijo3.nombre_completo = "";
                    this.state.hijo3.fec_nacimiento = "";
                } else if (contadorHijos == 4) {
                    contadorHijos = this.state.contadorHijos - 1
                    this.setState({ contadorHijos: contadorHijos, hijo_Eliminado4: true })
                    this.state.hijo4.nombre_completo = "";
                    this.state.hijo4.fec_nacimiento = "";
                }

            }
        } else {
            if (contadorHijos == 5 && !banInyecta) {
                contadorHijos = this.state.contadorHijos - 1
                this.setState({ contadorHijos: contadorHijos, hijo_Eliminado5: true })
                this.state.hijo5.nombre_completo = "";
                this.state.hijo5.fec_nacimiento = "";
            }
        }

        /*if (contadorHijos <= 5) {

            this.setState({ contadorHijos: contadorHijos })
        }*/
    }

    inyectaBeneficiarioFondo(banInyecta) {
        console.log("inyeccion")
        let contBeneficiariosSeguro = this.state.contadorBeneficiariosFondo

        if (contBeneficiariosSeguro >= 1 && contBeneficiariosSeguro < 3) {
            if (banInyecta) {
                contBeneficiariosSeguro = this.state.contadorBeneficiariosFondo + 1
                let id = "beneficiarioSeguro" + contBeneficiariosSeguro
                this.setState({ contadorBeneficiariosFondo: contBeneficiariosSeguro, banFondo: true })
                if (this.state.beneficiarioFonEliminado2) {
                    this.setState({ beneficiarioFonEliminado2: false })
                }
                if (this.state.beneficiarioFonEliminado3) {
                    this.setState({ beneficiarioFonEliminado3: false })
                }
            } else {
                if (contBeneficiariosSeguro > 1) {
                    let id = "beneficiarioSeguro" + contBeneficiariosSeguro
                    contBeneficiariosSeguro = this.state.contadorBeneficiariosFondo - 1
                    this.setState({ contadorBeneficiariosFondo: contBeneficiariosSeguro, actvaBotonFinalizar: false, beneficiarioFonEliminado2: true, banFondo: false })
                    this.state.beneficiarioFondo2.nombre = ""
                    this.state.beneficiarioFondo2.ape_paterno = ""
                    this.state.beneficiarioFondo2.ape_materno = ""
                }
            }
        } else {
            if (contBeneficiariosSeguro == 3 && !banInyecta) {
                contBeneficiariosSeguro = this.state.contadorBeneficiariosFondo - 1
                let id = "beneficiarioSeguro" + contBeneficiariosSeguro
                this.setState({ contadorBeneficiariosFondo: contBeneficiariosSeguro, actvaBotonFinalizar: false, beneficiarioFonEliminado3: true, banFondo: false })
                this.state.beneficiarioFondo3.nombre = ""
                this.state.beneficiarioFondo3.ape_paterno = ""
                this.state.beneficiarioFondo3.ape_materno = ""
            }
        }

    }


    determinaEstadoCivil() {
        let estados = this.state.estadosConyugales
        estados[this.props.contenidoEdicion.estadocivil_id - 1] = true
        console.log(estados)
        this.setState({ estadosConyugales: estados })
    }

    onChangeEstadosConyugales = e => {
        console.log(e.target.name, e.target.checked)
    }


    construyeSelect(url, selector, tabindex) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)
                console.log("respuesta de las ciudades ", json)
                for (var i = 0; i < filas.length; i++) {

                    let fila = filas[i]
                    if (selector == "selectCiudad") {
                        if (this.props.idEmpleado) {
                            if (this.state.ciudad_id == fila.fila[0].value) {
                                options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            } else {
                                options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            }
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }
                    } else if (selector == "selectParentesco") {
                        if (this.props.idEmpleado) {
                            if (this.state.parentesco_id == fila.fila[0].value) {
                                options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            } else {
                                options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            }
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }
                    }

                }
                let salida = []
                salida.push(<select onChange={this.onChange} disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                    && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                        && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} tabIndex={tabindex} name={selector} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ [selector]: salida })
            });
    }

    construyeSelectDependienteDeCiudad(url, selector, tabindex) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)
                this.setState({ ["selectMunicipio"]: null })
                this.setState({ ["selectColonia"]: null })
                for (var i = 0; i < filas.length; i++) {
                    let fila = filas[i]
                    if (this.state.ciudad_id == fila.fila[1].value) {
                        if (this.state.municipio_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[2].value}</option>)
                            this.construyeSelectDependienteDeMunicipio(datos.urlServicePy+"parametros/api_cat_colonia/"+fila.fila[0].value, "selectColonia", 10)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[2].value}</option>)
                        }
                    }
                }
                let salida = []
                salida.push(<select onChange={this.onChange} disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                    && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                        && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} tabIndex={tabindex} name={selector} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ [selector]: salida })
            });
    }

    construyeSelectDependienteDeMunicipio(url, selector, tabindex) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                 options.push(<option selected value={0}>Seleccionar</option>)
                 this.setState({ ["selectColonia"]: null })
                for (var i = 0; i < filas.length; i++) {
                    let fila = filas[i]
                    if (this.state.municipio_id == fila.fila[1].value) {
                        if (this.state.colonia_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[2].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[2].value}</option>)
                        }
                    }
                }
                let salida = []
                salida.push(<select style={{ borderColor: '#E1E5F0' }} disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                    && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                        && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} tabIndex={tabindex} name={selector} onChange={this.onChange} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ [selector]: salida })
            });
    }


    onChange = e => {
        console.log("entrando", e.target.name, e.target.value)

        let contenidoEdicion = this.state.contenidoEdicion != null ? this.state.contenidoEdicion : this.state.estadosConyugales

        if ("rfc" == e.target.name) {
            let rfc = e.target.value;
            if (rfc.match(this.state.expresionRFC)) {
                var rfcanio = rfc.substring(4, 6);
                var rfcmes = rfc.substring(6, 8);
                var rfcdia = rfc.substring(8, 10);
                var formatDate = "";

                if (rfcanio <= 20) {
                    formatDate = "20" + rfcanio + "-";
                } else {
                    formatDate = "19" + rfcanio + "-";
                }
                formatDate = formatDate + rfcmes + "-" + rfcdia;
                this.state.fec_nacimiento = formatDate;
                this.setState({ validaRFC: true })
            } else {
                this.setState({ validaRFC: false })
            }
            this.setState({ [e.target.name]: e.target.value })

        } else
            if ("curp" == e.target.name) {
                let rfc = e.target.value;
                if (rfc.match(this.state.expresionCURP)) {
                    this.setState({ validaCURP: true })
                } else {
                    this.setState({ validaCURP: false })
                }
                this.setState({ [e.target.name]: e.target.value })
            } else
            if ("codigo_postal" == e.target.name) {
                let codigo_postal = e.target.value
                if (codigo_postal.length <= 5) {
                    this.setState({ [e.target.name]: e.target.value })
                }
            } else
                if ("selectMunicipio" == e.target.name) {
                    console.log("entroa asleect")
                    this.setState({ municipio_id: parseInt(e.target.value) })
                    this.construyeSelectDependienteDeMunicipio(datos.urlServicePy+"parametros/api_cat_colonia/"+parseInt(e.target.value), "selectColonia", 10)

                } else
                    if ("selectCiudad" == e.target.name) {
                        console.log("entro")
                        this.setState({ ciudad_id: parseInt(e.target.value) })
                        this.construyeSelectDependienteDeCiudad(datos.urlServicePy+"parametros/api_cat_municipio/0", "selectMunicipio", 9)

                    } else
                        if ("selectColonia" == e.target.name) {
                            console.log("entro")
                            this.setState({ colonia_id: parseInt(e.target.value) })

                        } else
                            if ("selectParentesco" == e.target.name) {
                                console.log("entro")
                                this.setState({ parentesco_id: parseInt(e.target.value) })

                            } else
                                if (e.target.name == "soltero") {
                                    let estados = this.state.estadosConyugales;
                                    estados[0] = !estados[0]
                                    this.desactivaCheck(0, estados)
                                    this.setState({ estadosConyugales: estados , estadocivil_id:1})
                                    estados[0] == false ? contenidoEdicion.estadocivil_id = 0 : contenidoEdicion.estadocivil_id = 1
                                } else
                                    if (e.target.name == "casado") {
                                        let estados = this.state.estadosConyugales;
                                        estados[1] = !estados[1]
                                        this.desactivaCheck(1, estados)
                                        this.state.estadocivil_id = 2
                                        this.setState({ estadosConyugales: estados, estadocivil_id:2 })
                                        estados[1] == false ? contenidoEdicion.estadocivil_id = 0 : contenidoEdicion.estadocivil_id = 2
                                    } else
                                        if (e.target.name == "divorciado") {
                                            let estados = this.state.estadosConyugales;
                                            estados[2] = !estados[2]
                                            this.desactivaCheck(2, estados)
                                            estados.estadocivil_id = 3
                                            this.setState({ estadosConyugales: estados, estadocivil_id:3 })
                                            estados[2] == false ? contenidoEdicion.estadocivil_id = 0 : contenidoEdicion.estadocivil_id = 3
                                        } else
                                            if (e.target.name == "ulibre") {
                                                let estados = this.state.estadosConyugales;
                                                estados[3] = !estados[3]
                                                this.desactivaCheck(3, estados)
                                                estados.estadocivil_id = 4
                                                this.setState({ estadosConyugales: estados, estadocivil_id:4 })
                                                estados[3] == false ? contenidoEdicion.estadocivil_id = 0 : contenidoEdicion.estadocivil_id = 4
                                            } else
                                                if (e.target.name == "viudo") {
                                                    let estados = this.state.estadosConyugales;
                                                    estados[4] = !estados[4]
                                                    this.desactivaCheck(4, estados)
                                                    estados.estadocivil_id = 5
                                                    this.setState({ estadosConyugales: estados, estadocivil_id:5})
                                                    estados[4] == false ? contenidoEdicion.estadocivil_id = 0 : contenidoEdicion.estadocivil_id = 5
                                                }
                                                else if (e.target.name == 'tel_casa') {
                                                    //let telCasa=this.state.tel_casa+""
                                                    let telCasa = e.target.value
                                                    console.log("la longitud  tel_casa ", telCasa.length, e.target)
                                                    if (telCasa.length <= 10) {
                                                        this.setState({ [e.target.name]: e.target.value })
                                                    }


                                                } else if (e.target.name == 'tel_movil') {
                                                    let telMovil = e.target.value
                                                    if (telMovil.length <= 10) {
                                                        this.setState({ [e.target.name]: e.target.value })
                                                    }

                                                } else if (e.target.name == 'tel_contacto') {
                                                    let telContacto = e.target.value
                                                    if (telContacto.length <= 10) {
                                                        this.setState({ [e.target.name]: e.target.value })
                                                    }

                                                }
                                                else {
                                                    this.setState({ [e.target.name]: e.target.value })
                                                }
    }

    onChangeHijos = e => {
        let campoBeneficiario = e.target.name
        let beneficiarioCampo = campoBeneficiario.split('%')[0]
        let beneficiarioNumero = campoBeneficiario.split('%')[1]
        console.log("beneficiario ", beneficiarioNumero, e.target.value)
        if (beneficiarioNumero == '1') {
            let beneficiarioFondo1 = this.state.hijo1
            beneficiarioFondo1["" + beneficiarioCampo + ""] = e.target.value
            this.setState({ hijo1: beneficiarioFondo1 })
        }
        if (beneficiarioNumero == '2') {
            let beneficiarioFondo2 = this.state.hijo2
            beneficiarioFondo2["" + beneficiarioCampo + ""] = e.target.value
            this.setState({ hijo2: beneficiarioFondo2 })

        }
        if (beneficiarioNumero == '3') {
            let beneficiarioFondo3 = this.state.hijo3
            beneficiarioFondo3["" + beneficiarioCampo + ""] = e.target.value
            this.setState({ hijo3: beneficiarioFondo3 })
        }
        if (beneficiarioNumero == '4') {
            let beneficiarioFondo4 = this.state.hijo4
            beneficiarioFondo4["" + beneficiarioCampo + ""] = e.target.value
            this.setState({ hijo4: beneficiarioFondo4 })
        }
        if (beneficiarioNumero == '5') {
            let beneficiarioFondo5 = this.state.hijo5
            beneficiarioFondo5["" + beneficiarioCampo + ""] = e.target.value
            this.setState({ hijo5: beneficiarioFondo5 })
        }
    }

    desactivaCheck(index, estados) {
        for (var i = 0; i < estados.length; i++) {
            if (index != i) {
                estados[i] = false
            }
        }

    }


    onClickImagen() {
        console.log("entrnaod a la imagen")
        this.upload.click()
    }

    onChangeFile(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file", file);
        this.setState({ imagenEnCambio: URL.createObjectURL(file) })
        //this.props.guardaFormulario(this.state.contenidoEdicion)
        //this.props.guardaFotografia(file)
        this.setState({ fotografiaFile: file }); /// if you want to upload latter
    }

    validaExistenciaAlgunArchivo() {
        let actualizar = false;
        if (this.state.doc_foto.length > 0 || this.state.doc_puesto.length > 0 || this.state.doc_cv.length || this.state.doc_psp.length > 0) {
            actualizar = true
        }
        return actualizar
    }


    clickSiguiente(determinaclick) {
        console.log("json sa disparar ", this.state)
        if (this.props.idEmpleado == null) {
            let jsonDisparar = {
                //  "doc_foto": "",
                "nombre": this.state.nombre,
                "ape_paterno": this.state.ape_paterno,
                "ape_materno": this.state.ape_materno,
                "rfc": this.state.rfc,
                "curp": this.state.curp,
                "fec_nacimiento": this.state.fec_nacimiento,
                "codigo_postal": this.state.codigo_postal,
                "ciudad_id": this.state.ciudad_id,
                "municipio_id": this.state.municipio_id,
                "colonia_id": this.state.colonia_id,
                "calle": this.state.calle,
                "num_ext": this.state.num_ext,
                "num_int": this.state.num_int,
                "tel_casa": this.state.tel_casa,
                "tel_movil": this.state.tel_movil,
                "estadocivil_id": this.state.estadocivil_id,
                "nombre_conyugue": this.state.nombre_conyugue,
                "fec_nacimiento_conyugue": this.state.fec_nacimiento_conyugue,
                "fec_aniversario": this.state.fec_aniversario,
                "hijos_id": null,
                "nombre_contacto": this.state.nombre_contacto,
                "parentesco_id": this.state.parentesco_id,
                "tel_contacto": this.state.tel_contacto,
                "estatusbaja_id": null,
                "fec_baja": null,
                "causabaja_id": null,
                "fec_ingreso": null,
                "escolaridad_id": null,
                "profesion_id": null,
                "puesto_id": null,
                //  "doc_puesto": "",
                "acceso_pai": 0,
                "figura_id": null,
                "area_id": null,
                "supervisor": null,
                //  "doc_cv": "",
                //  "doc_psp": "",
                "sueldo": 0,
                "banco_id": null,
                "cta_clabe": null,
                "sociedad_id": null,
                "activo": 0,
                "correo_electronico": null,
                "otros_correos": null,
                "fecha_baja_correos": null,
                "num_extension": null,
                "dias_vacaciones": 0,
                "acceso_impresion": 0,
                "acceso_copiadora": 0,
                //"ts_alta_audit": null,
                //"ts_actualiza_audit": null,
                "user_id": this.props.idUsuario
            }
            const requestOptions = {
                method: "POST",
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(jsonDisparar)
            };
            fetch(datos.urlServicePy+"parametros/api_cat_empleados/0", requestOptions)
                .then(response => response.json())
                .then(data => {
                    console.log("la respuesta ", data)
                    if (this.state.fotografiaFile != undefined) {
                        console.log("entro a enviar la foto")
                        let formData = new FormData();
                        formData.append('id', data.id);
                        formData.append('fotografia', this.state.fotografiaFile);
                        formData.append('empleado_id', data.id);
                        const config = {
                            headers: { 'content-type': 'multipart/form-data' }
                        }

                        axios.post(datos.urlServicePy+"api/doc_empleado/", formData, config)
                            .then(res => {
                                console.log(res.data);
                                console.log(this.state.filename);
                                console.log(formData);
                            })
                        // 
                    }
                    if (JSON.stringify(this.state.hijo1) != '{}') {
                        this.state.hijo1.empleado_id = data.id
                        const requestOptions = {
                            method: "POST",
                            headers: { 'Content-Type': 'application/json' },
                            body: JSON.stringify(this.state.hijo1)
                        };

                        fetch(datos.urlServicePy+"parametros/api_cat_empleados_hijos/0", requestOptions)
                            .then(response => response.json())
                            .then(data => { })
                    }

                    if (JSON.stringify(this.state.hijo2) != '{}') {
                        if (!this.state.hijo_Eliminado2) {
                            this.state.hijo2.empleado_id = data.id
                            const requestOptions = {
                                method: "POST",
                                headers: { 'Content-Type': 'application/json' },
                                body: JSON.stringify(this.state.hijo2)
                            };

                            fetch(datos.urlServicePy+"parametros/api_cat_empleados_hijos/0", requestOptions)
                                .then(response => response.json())
                                .then(data => { })
                        }
                    }
                    if (JSON.stringify(this.state.hijo3) != '{}') {
                        if (!this.state.hijo_Eliminado3) {
                            this.state.hijo3.empleado_id = data.id
                            const requestOptions = {
                                method: "POST",
                                headers: { 'Content-Type': 'application/json' },
                                body: JSON.stringify(this.state.hijo3)
                            };

                            fetch(datos.urlServicePy+"parametros/api_cat_empleados_hijos/0", requestOptions)
                                .then(response => response.json())
                                .then(data => { })
                        }
                    }
                    if (JSON.stringify(this.state.hijo4) != '{}') {
                        if (!this.state.hijo_Eliminado4) {
                            this.state.hijo4.empleado_id = data.id
                            const requestOptions = {
                                method: "POST",
                                headers: { 'Content-Type': 'application/json' },
                                body: JSON.stringify(this.state.hijo4)
                            };

                            fetch(datos.urlServicePy+"parametros/api_cat_empleados_hijos/0", requestOptions)
                                .then(response => response.json())
                                .then(data => { })
                        }
                    }
                    if (JSON.stringify(this.state.hijo5) != '{}') {
                        if (!this.state.hijo_Eliminado5) {
                            this.state.hijo5.empleado_id = data.id
                            const requestOptions = {
                                method: "POST",
                                headers: { 'Content-Type': 'application/json' },
                                body: JSON.stringify(this.state.hijo5)
                            };

                            fetch(datos.urlServicePy+"parametros/api_cat_empleados_hijos/0", requestOptions)
                                .then(response => response.json())
                                .then(data => { })
                        }
                    }

                    this.props.muestraFormulario2(data.id)
                });
        } else {

            let jsonDisparar = {
                // "doc_foto": "",
                "nombre": this.state.nombre,
                "ape_paterno": this.state.ape_paterno,
                "ape_materno": this.state.ape_materno,
                "rfc": this.state.rfc,
                "curp": this.state.curp,
                "fec_nacimiento": this.state.fec_nacimiento,
                "codigo_postal": this.state.codigo_postal,
                "ciudad_id": this.state.ciudad_id,
                "municipio_id": this.state.municipio_id,
                "colonia_id": this.state.colonia_id,
                "calle": this.state.calle,
                "num_ext": this.state.num_ext,
                "num_int": this.state.num_int,
                "tel_casa": this.state.tel_casa,
                "tel_movil": this.state.tel_movil,
                "estadocivil_id": this.state.estadocivil_id,
                "nombre_conyugue": this.state.nombre_conyugue,
                "fec_nacimiento_conyugue": this.state.fec_nacimiento_conyugue,
                "fec_aniversario": this.state.fec_aniversario,
                "hijos_id": null,
                "nombre_contacto": this.state.nombre_contacto,
                "parentesco_id": this.state.parentesco_id,
                "tel_contacto": this.state.tel_contacto,
                "estatusbaja_id": this.state.estatusbaja_id,
                "fec_baja": this.state.fec_baja,
                "causabaja_id": this.state.causabaja_id,
                "fec_ingreso": this.state.fec_ingreso,
                "escolaridad_id": this.state.escolaridad_id,
                "profesion_id": this.state.profesion_id,
                "puesto_id": this.state.puesto_id,
                "doc_puesto": this.state.doc_puesto,
                "acceso_pai": this.state.acceso_pai,
                "figura_id": this.state.figura_id,
                "area_id": this.state.area_id,
                "supervisor": this.state.supervisor,
                "doc_cv": this.state.doc_cv,
                "doc_psp": this.state.doc_psp,
                "sueldo": this.state.sueldo,
                "banco_id": this.state.banco_id,
                "cta_clabe": this.state.cta_clabe,
                "sociedad_id": this.state.sociedad_id,
                "activo": this.state.activo,
                "correo_electronico": this.state.correo_electronico,
                "otros_correos": this.state.otros_correos,
                "fecha_baja_correos": this.state.fecha_baja_correos,
                "num_extension": this.state.num_extension == 0 ? null : this.state.num_extension,
                "dias_vacaciones": this.state.dias_vacaciones,
                "acceso_impresion": this.state.acceso_impresion,
                "acceso_copiadora": this.state.acceso_copiadora,
                //"ts_alta_audit": this.state.ts_alta_audit,
                //"ts_actualiza_audit": this.state.ts_actualiza_audit,
                "user_id": this.props.idUsuario,
            }

            const requestOptions = {
                method: "PUT",
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(jsonDisparar)
            };
            fetch(datos.urlServicePy+"parametros_upd/api_cat_empleados/" + this.props.idEmpleado, requestOptions)
                .then(response => response.json())
                .then(data => {/*this.props.muestraFormulario2(this.props.idEmpleado)*/
                    if (determinaclick == "1") {
                        console.log("determinaclick 1");
                        this.props.muestraFormulario(this.props.idEmpleado)
                    } else if (determinaclick == "2") {
                        console.log("determinaclick 2");
                        this.props.muestraFormulario2(this.props.idEmpleado)
                    } else if (determinaclick == "3") {
                        console.log("determinaclick 3");
                        this.props.muestraFormulario3(this.props.idEmpleado)
                    } else {
                        console.log("Entro en pre recarga y guardado");
                    }
                })

            if (!this.validaExistenciaAlgunArchivo()) {

                if (this.state.fotografiaFile != undefined) {
                    console.log("entro a enviar la foto")
                    let formData = new FormData();
                    formData.append('id', this.props.idEmpleado);
                    formData.append('fotografia', this.state.fotografiaFile);
                    formData.append('empleado_id', this.props.idEmpleado)
                    const config = {
                        headers: { 'content-type': 'multipart/form-data' }
                    }

                    axios.post(datos.urlServicePy+"api/doc_empleado/", formData, config)
                        .then(res => {
                            console.log(res.data);
                            console.log(this.state.filename);
                            console.log(formData);
                        })

                }

            } else {
                if (this.state.fotografiaFile != undefined) {
                    console.log("entro a enviar la foto")
                    let formData = new FormData();
                    formData.append('id', this.props.idEmpleado);
                    formData.append('fotografia', this.state.fotografiaFile);
                    formData.append('empleado_id', this.props.idEmpleado);
                    const config = {
                        headers: { 'content-type': 'multipart/form-data' }
                    }

                    axios.put(datos.urlServicePy+"api/doc_empleado/" + this.props.idEmpleado + "/", formData, config)
                        .then(res => {
                            console.log(res.data);
                            console.log(this.state.filename);
                            console.log(formData);
                        })

                }
            }





            if (JSON.stringify(this.state.hijo1) != '{}') {
                if (this.state.hijo1Edicion) {
                    const requestOptions = {
                        method: "PUT",
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify(this.state.hijo1)
                    };

                    fetch(datos.urlServicePy+"parametros_upd/api_cat_empleados_hijos/" + this.state.hijo1.id, requestOptions)
                        .then(response => response.json())
                        .then(data => { })
                } else {
                    this.state.hijo1.empleado_id = this.props.idEmpleado
                    const requestOptions = {
                        method: "POST",
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify(this.state.hijo1)
                    };

                    fetch(datos.urlServicePy+"parametros/api_cat_empleados_hijos/0", requestOptions)
                        .then(response => response.json())
                        .then(data => { })
                }
            }


            if (JSON.stringify(this.state.hijo2) != '{}') {
                if (!this.state.hijo_Eliminado2) {
                    if (this.state.hijo2Edicion) {
                        const requestOptions = {
                            method: "PUT",
                            headers: { 'Content-Type': 'application/json' },
                            body: JSON.stringify(this.state.hijo2)
                        };

                        fetch(datos.urlServicePy+"parametros_upd/api_cat_empleados_hijos/" + this.state.hijo2.id, requestOptions)
                            .then(response => response.json())
                            .then(data => { })
                    } else {
                        this.state.hijo2.empleado_id = this.props.idEmpleado
                        const requestOptions = {
                            method: "POST",
                            headers: { 'Content-Type': 'application/json' },
                            body: JSON.stringify(this.state.hijo2)
                        };

                        fetch(datos.urlServicePy+"parametros/api_cat_empleados_hijos/0", requestOptions)
                            .then(response => response.json())
                            .then(data => { })
                    }
                } else {
                    if (this.state.hijo2.id != undefined) {
                        console.log("Implementacion para eliminar hijo 2");
                        const requestOptions = {
                            method: "DELETE",
                            headers: { 'Content-Type': 'application/json' }
                        };

                        fetch(datos.urlServicePy+"parametros_upd/api_cat_empleados_hijos/" + this.state.hijo2.id, requestOptions)
                            .then(response => response.json())
                            .then(data => { })
                    }
                }
            }

            if (JSON.stringify(this.state.hijo3) != '{}') {
                if (!this.state.hijo_Eliminado3) {
                    if (this.state.hijo3Edicion) {
                        const requestOptions = {
                            method: "PUT",
                            headers: { 'Content-Type': 'application/json' },
                            body: JSON.stringify(this.state.hijo3)
                        };

                        fetch(datos.urlServicePy+"parametros_upd/api_cat_empleados_hijos/" + this.state.hijo3.id, requestOptions)
                            .then(response => response.json())
                            .then(data => { })
                    } else {
                        this.state.hijo3.empleado_id = this.props.idEmpleado
                        const requestOptions = {
                            method: "POST",
                            headers: { 'Content-Type': 'application/json' },
                            body: JSON.stringify(this.state.hijo3)
                        };

                        fetch(datos.urlServicePy+"parametros/api_cat_empleados_hijos/0", requestOptions)
                            .then(response => response.json())
                            .then(data => { })
                    }
                } else {
                    if (this.state.hijo3.id != undefined) {
                        console.log("Implementacion para eliminar hijo 3");
                        const requestOptions = {
                            method: "DELETE",
                            headers: { 'Content-Type': 'application/json' }
                        };

                        fetch(datos.urlServicePy+"parametros_upd/api_cat_empleados_hijos/" + this.state.hijo3.id, requestOptions)
                            .then(response => response.json())
                            .then(data => { })
                    }
                }
            }

            if (JSON.stringify(this.state.hijo4) != '{}') {
                if (!this.state.hijo_Eliminado4) {
                    if (this.state.hijo4Edicion) {
                        const requestOptions = {
                            method: "PUT",
                            headers: { 'Content-Type': 'application/json' },
                            body: JSON.stringify(this.state.hijo4)
                        };

                        fetch(datos.urlServicePy+"parametros_upd/api_cat_empleados_hijos/" + this.state.hijo4.id, requestOptions)
                            .then(response => response.json())
                            .then(data => { })
                    } else {
                        this.state.hijo4.empleado_id = this.props.idEmpleado
                        const requestOptions = {
                            method: "POST",
                            headers: { 'Content-Type': 'application/json' },
                            body: JSON.stringify(this.state.hijo4)
                        };

                        fetch(datos.urlServicePy+"parametros/api_cat_empleados_hijos/0", requestOptions)
                            .then(response => response.json())
                            .then(data => { })
                    }
                } else {
                    if (this.state.hijo4.id != undefined) {
                        console.log("Implementacion para eliminar hijo 4");
                        const requestOptions = {
                            method: "DELETE",
                            headers: { 'Content-Type': 'application/json' }
                        };

                        fetch(datos.urlServicePy+"parametros_upd/api_cat_empleados_hijos/" + this.state.hijo4.id, requestOptions)
                            .then(response => response.json())
                            .then(data => { })
                    }
                }
            }

            if (JSON.stringify(this.state.hijo5) != '{}') {
                if (!this.state.hijo_Eliminado5) {
                    if (this.state.hijo5Edicion) {
                        const requestOptions = {
                            method: "PUT",
                            headers: { 'Content-Type': 'application/json' },
                            body: JSON.stringify(this.state.hijo5)
                        };

                        fetch(datos.urlServicePy+"parametros_upd/api_cat_empleados_hijos/" + this.state.hijo5.id, requestOptions)
                            .then(response => response.json())
                            .then(data => { })
                    } else {
                        this.state.hijo5.empleado_id = this.props.idEmpleado
                        const requestOptions = {
                            method: "POST",
                            headers: { 'Content-Type': 'application/json' },
                            body: JSON.stringify(this.state.hijo5)
                        };

                        fetch(datos.urlServicePy+"parametros/api_cat_empleados_hijos/0", requestOptions)
                            .then(response => response.json())
                            .then(data => { })
                    }
                } else {
                    if (this.state.hijo5.id != undefined) {
                        console.log("Implementacion para eliminar hijo 5");
                        const requestOptions = {
                            method: "DELETE",
                            headers: { 'Content-Type': 'application/json' }
                        };

                        fetch(datos.urlServicePy+"parametros_upd/api_cat_empleados_hijos/" + this.state.hijo5.id, requestOptions)
                            .then(response => response.json())
                            .then(data => { })
                    }
                }
            }

            /* if(determinaclick == "1"){
                 console.log("determinaclick 1");
                 this.props.muestraFormulario(this.props.idEmpleado)
             }else if(determinaclick == "2"){
                 console.log("determinaclick 2");
                 this.props.muestraFormulario2(this.props.idEmpleado)
             }else if(determinaclick == "3"){
                 console.log("determinaclick 3");
                 this.props.muestraFormulario3(this.props.idEmpleado)
             } else {
                 console.log("Entro en pre recarga y guardado");
             }*/

            //this.props.muestraFormulario2(this.props.idEmpleado)

        }




    }




    render() {
        return (
            <div>
                <div class="card">
                    <div class="row mb-2 mt-2 ml-1">
                        <div class="col-12 col-lg-9">
                            <div class="row">

                                <div class="col-6 col-lg-3 d-flex align-items-center pb-sm-1">
                                    <div class="mr-2">
                                        <a href="#"
                                            class="btn btn-primary rounded-pill btn-icon btn-sm"> <span
                                                class="letter-icon">1</span>
                                        </a>
                                    </div>
                                    <div>
                                        <a href="#" class="text-body font-weight-semibold letter-icon-title">Información general</a>
                                    </div>
                                </div>


                                <div class="col-6 col-lg-3 d-flex align-items-center pb-sm-1">
                                    <div class="mr-2">

                                        {
                                            this.props.idEmpleado == null ?
                                                <a href="#" class="btn btn-light rounded-pill btn-icon btn-sm disabled ">
                                                    <span class="letter-icon">2</span>
                                                </a>
                                                :
                                                <a href="#" onClick={() => this.clickSiguiente("2")}
                                                    class="btn btn-primary rounded-pill btn-icon btn-sm"> <span
                                                        class="letter-icon">2</span>
                                                </a>
                                        }
                                    </div>
                                    <div>

                                        {
                                            this.props.idEmpleado == null ?
                                                <a href="#" class="text-body font-weight-semibold  letter-icon-title text-muted ">Información laboral</a>
                                                :
                                                <a href="#" onClick={() => this.clickSiguiente("2")} class="text-body font-weight-semibold letter-icon-title">Información laboral</a>

                                        }

                                    </div>
                                </div>




                                <div class="col-6 col-lg-3 d-flex align-items-center pb-sm-1" disabled = {this.state.sociedad_id == 6 || this.state.sociedad_id == 0 || this.state.sociedad_id == null}>
                                    <div class="mr-2" disabled = {this.state.sociedad_id == 6 || this.state.sociedad_id == 0 || this.state.sociedad_id == null}>

                                        {
                                            this.props.idEmpleado == null || (this.state.sociedad_id == 6 || this.state.sociedad_id == 0 || this.state.sociedad_id == null) ?
                                                <a href="#" class="btn btn-light rounded-pill btn-icon btn-sm disabled">
                                                    <span class="letter-icon">3</span>
                                                </a>
                                                :
                                                <a href="#" onClick={() => this.clickSiguiente("3")}
                                                    class="btn btn-primary rounded-pill btn-icon btn-sm"> <span
                                                        class="letter-icon">3</span>
                                                </a>

                                        }


                                    </div>
                                    <div>
                                        {
                                            this.props.idEmpleado == null || (this.state.sociedad_id == 6 || this.state.sociedad_id == 0 || this.state.sociedad_id == null) ?
                                                <a href="#" class="text-body font-weight-semibold  letter-icon-title text-muted">Beneficiarios</a>
                                                :
                                                <a href="#" onClick={() => this.clickSiguiente("3")} class="text-body font-weight-semibold letter-icon-title">Beneficiarios</a>

                                        }

                                    </div>
                                </div>




                            </div>
                        </div>
                        <div class="col-12 col-lg-3">

                            <div class="float-right mr-3">

                                <button type="button" class="btn btn-light mr-1" onClick={(e) => this.props.botonCancelar()}>Cancelar</button>

                                <button type="button" class="btn btn-warning"
                                    style={{ backgroundColor: '#ED6C26' }} id="siguiente"

                                    disabled={
                                        (this.state.nombre == "" || this.state.nombre == null)
                                        ||
                                        this.state.validaRFC == false
                                        ||
                                        (this.state.ape_paterno == "" || this.state.ape_paterno == null)
                                        ||
                                        this.state.validaCURP == false
                                        ||
                                        (this.state.fec_nacimiento == "" || this.state.fec_nacimiento == null)
                                        ||
                                        this.state.codigo_postal == 0
                                        ||
                                        this.state.ciudad_id == 0
                                        ||
                                        (this.state.calle == "" || this.state.calle == null)
                                        ||
                                        (this.state.municipio_id == "" || this.state.municipio_id == null)
                                        ||
                                        this.state.num_ext == null || this.state.num_ext == ""
                                        // ||
                                        // this.state.num_int == 0
                                        ||
                                        this.state.tel_movil == 0
                                        ||
                                        this.state.estadocivil_id == 0
                                        ||
                                        (this.state.nombre_contacto == "" || this.state.nombre_contacto == null)
                                        ||
                                        this.state.parentesco_id == 0
                                        ||
                                        this.state.tel_contacto == 0

                                    }

                                    onClick={() => this.clickSiguiente("2")}> Siguiente</button>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="card-header bg-transparent header-elements-sm-inline">
                    <h2 class="mb-0 font-weight-semibold">1. Información general de empleado </h2>
                    <div class="header-elements">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item font-size-sm">Fecha de alta: <strong >{this.state.ts_alta_audit}</strong></li>

                            <li class="list-inline-item font-size-sm">Usuario creador: <strong >{this.state.user_id}</strong></li>
                        </ul>
                    </div>
                </div>
                <br></br>
                <br></br>


                <div className="row">

                    <div className="col-xs-1 col-lg-1">
                        <div class="card-img-actions">
                            {
                                this.props.idEmpleado != null ?

                                    this.state.doc_foto.length == 0 ? <div> <input type="image" onClick={this.onClickImagen} src={this.state.imagenEnCambio == undefined ? avatar : this.state.imagenEnCambio} class="img-fluid rounded-circle" width="170" height="270" alt="" />
                                        <form encType="multipart/form">
                                            <input type="file" style={{ display: 'none' }} ref={(ref) => this.upload = ref} onChange={this.onChangeFile.bind(this)}></input>
                                        </form>
                                    </div> :
                                        <img src={this.state.doc_foto} class="img-fluid rounded-circle" width="170" height="270" alt="" />


                                    :
                                    <div> <input type="image" onClick={this.onClickImagen} src={this.state.imagenEnCambio == undefined ? avatar : this.state.imagenEnCambio} class="img-fluid rounded-circle" width="170" height="270" alt="" />
                                        <form encType="multipart/form">
                                            <input type="file" style={{ display: 'none' }} ref={(ref) => this.upload = ref} onChange={this.onChangeFile.bind(this)}></input>
                                        </form>
                                    </div>

                            }


                        </div>
                    </div>


                    <div className="col-xs-3 col-lg-4 " style={{ marginTop: '20px' }}>
                        <div className="form-group">

                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }}>Nombre (s)</span>
                                </span>
                                <input type="text" name="nombre" maxlength="50" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                    && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                        && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} value={this.state.nombre} onChange={this.onChange} style={{ borderColor: '#E1E5F0' }} className="form-control" tabIndex={1} />
                            </div>

                            {
                                this.state.nombre == "" || this.state.nombre == null ?
                                    <span id="validaNombre" style={{ color: "red" }}>El nombre es un campo requerido</span> : ''
                            }

                        </div>

                        <div className="form-group">

                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >RFC</span>
                                </span> <input type="text" name="rfc" maxlength="13" value={this.state.rfc} disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                    && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                        && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} style={{ borderColor: '#E1E5F0' }} className="form-control " tabIndex={4} />
                            </div>
                            {
                                this.state.validaRFC == false ?
                                    <span className="" style={{ color: "red" }}>El rfc es un campo requerido</span> : ''
                            }

                        </div>
                    </div>

                    <div className="col-xs-3 col-lg-3" style={{ marginTop: '20px' }}>
                        <div className="form-group">

                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Apellido paterno</span>
                                </span> <input type="text" maxlength="50" name="ape_paterno" value={this.state.ape_paterno} disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                    && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                        && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} style={{ borderColor: '#E1E5F0' }} className="form-control " tabIndex={2} />
                            </div>
                            {
                                this.state.ape_paterno == "" || this.state.ape_paterno == null ?
                                    <span className=" " style={{ color: "red" }}>El apellido paterno es un campo requerido</span> : ''
                            }
                        </div>

                        <div className="form-group">

                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }}>CURP</span>
                                </span> <input type="text" name="curp" maxlength="18" value={this.state.curp} disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                    && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                        && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} style={{ borderColor: '#E1E5F0' }} className="form-control " tabIndex={5} />
                            </div>
                            {
                                this.state.validaCURP == false ?
                                    <span className="" style={{ color: "red" }}>El curp es un campo requerido</span> : ''
                            }
                        </div>
                    </div>


                    <div className="col-xs-3 col-lg-4" style={{ marginTop: '20px' }}>
                        <div className="form-group">

                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Apellido materno</span>
                                </span> <input type="text" maxlength="50" style={{ borderColor: '#E1E5F0' }} name="ape_materno" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                    && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                        && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} value={this.state.ape_materno} className="form-control " tabIndex={3} />
                            </div>
                        </div>

                        <div className="form-group">

                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }}>Fecha de nacimiento</span>
                                </span> <input type="date" style={{ borderColor: '#E1E5F0', opacity: 1 }} max="2100-01-01" name="fec_nacimiento" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                    && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                        && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} value={"" + this.state.fec_nacimiento + ""} className="form-control " tabIndex={6} />
                            </div>
                            {
                                this.state.fec_nacimiento == "" || this.state.fec_nacimiento == null ?
                                    <span className=" " style={{ color: "red" }}>La fecha de nacimiento es un campo requerido</span> : ''
                            }
                        </div>
                    </div>





                </div>
                <hr></hr>



                <div>
                    <div className="row">
                        < div class="card-header">
                            <h5 class="mb-0">Domicilio e información de contacto </h5>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Código postal</span>
                                    </span> <input type="number" maxlength="5" style={{ borderColor: '#E1E5F0' }} name="codigo_postal" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                        && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} value={this.state.codigo_postal} className="form-control " tabIndex={7} />
                                </div>
                                {
                                    this.state.codigo_postal == 0 ?
                                        <span className="" style={{ color: "red" }}>El código postal es un campo requerido</span> : ''
                                }
                            </div>

                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }}>Colonia</span>
                                    </span>
                                    {this.state.selectColonia}

                                </div>
                                {
                                    this.state.colonia_id == 0 ?
                                        <span className=" " style={{ color: "red" }}>Colonia es un campo requerido</span> : ''
                                }

                            </div>
                        </div>

                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Ciudad</span>
                                    </span>
                                    {this.state.selectCiudad}
                                </div>
                                {
                                    this.state.ciudad_id == 0 ?
                                        <span className=" " style={{ color: "red" }}>Ciudad es un campo requerido</span> : ''
                                }
                            </div>

                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }}>Calle</span>
                                    </span> <input type="text" maxlength="100" style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                        && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name="calle" value={this.state.calle} tabIndex={11} />
                                </div>
                                {
                                    this.state.calle == null ?
                                        <span className="" style={{ color: "red" }}>El calle es un campo requerido</span> : ''
                                }
                            </div>

                        </div>


                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Alcaldia/Municipio</span>
                                    </span>
                                    {this.state.selectMunicipio}
                                </div>
                                {
                                    this.state.municipio_id == 0 ?
                                        <span className=" " style={{ color: "red" }}>Alcaldia o municipio es un campo requerido</span> : ''
                                }
                            </div>

                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }}>No. ext.</span>
                                    </span>
                                    <input type="text" maxlength="10" style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                        && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name="num_ext" value={this.state.num_ext} tabIndex={12} />


                                    <span className="input-group-prepend"> <span
                                        className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }}>No. int.</span>
                                    </span> <input type="text" maxlength="10" style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                        && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name="num_int" value={this.state.num_int} tabIndex={13} />
                                </div>
                                {
                                    this.state.num_ext == null || this.state.num_ext == "" ?
                                        <span className=" " style={{ color: "red" }}>El número exterior es un campo requerido</span> : ''
                                }
                                {
                                    /* this.state.num_int == 0 ?
                                         <span className="" style={{ color: "red" }}>El número interior es un campo requerido</span> : ''*/
                                }

                            </div>
                        </div>



                    </div>

                    <div className="row">
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Teléfono de casa</span>
                                    </span> <input type="number" style={{ borderColor: '#E1E5F0' }} maxLength={10} className="form-control " disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                        && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name="tel_casa" value={this.state.tel_casa} tabIndex={14} />
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Teléfono móvil</span>
                                    </span> <input type="number" style={{ borderColor: '#E1E5F0' }} className="form-control " maxLength={10} disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                        && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name="tel_movil" value={this.state.tel_movil} tabIndex={15} />
                                </div>
                                {
                                    this.state.tel_movil == 0 || (this.state.tel_movil + "").length <= 9 ?
                                        <span className="" style={{ color: "red" }}>El telefono movíl es un campo requerido</span> : ''
                                }
                                {
                                    /* (this.state.tel_movil+"").length <= 10 ?
                                         <span className="" style={{ color: "red" }}>El telefono movíl es un campo requerido</span> : ''*/
                                }
                            </div>
                        </div>

                    </div>

                </div>


                <hr></hr>

                <div>
                    <div className="row">
                        < div class="card-header">
                            <h5 class="mb-0">Información de situación conyugal </h5>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-xs-6 col-lg-6">
                        <div className="form-group">
                            <span>Estado Civil:</span>
                            {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                            <input class="form-check-input" type="checkbox" name="soltero" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                    && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} id="soltero" checked={this.state.estadosConyugales[0]} />
                            <label class="form-check-label" for="soltero">
                                Soltero (a)
                            </label>
                            {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                            <input class="form-check-input" type="checkbox" name="casado" id="casado" onChange={this.onChange} checked={this.state.estadosConyugales[1]} />
                            <label class="form-check-label" for="casado">
                                Casado (a)
                            </label>
                            {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                            <input class="form-check-input" type="checkbox" name="divorciado" id="divorciado" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                    && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} checked={this.state.estadosConyugales[2]} />
                            <label class="form-check-label" for="divorciado">
                                Divorciado (a)
                            </label>
                            {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                            <input class="form-check-input" type="checkbox" name="ulibre" id="ulibre" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                    && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} checked={this.state.estadosConyugales[3]} />
                            <label class="form-check-label" for="ulibre">
                                Union libre (a)
                            </label>
                            {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                            <input class="form-check-input" type="checkbox" name="viudo" id="viudo" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                    && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} checked={this.state.estadosConyugales[4]} />
                            <label class="form-check-label" for="viudo">
                                Viudo (a)
                            </label>
                        </div>
                        {
                            this.state.estadosConyugales[0] == false && this.state.estadosConyugales[1] == false && this.state.estadosConyugales[2] == false && this.state.estadosConyugales[3] == false && this.state.estadosConyugales[4] == false ?
                                <span style={{ color: "red" }}>Algún estado conyugal debe ser seleccionado</span> : ''
                        }
                    </div>
                </div>
                <br></br>



                < div className="row">
                    <div className="col-xs-6 col-lg-6">
                        <div className="form-group">

                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre completo del cónyuge</span>
                                </span> <input type="text" maxlength="150" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                    && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                        && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name="nombre_conyugue" value={this.state.nombre_conyugue} tabIndex={16} />
                            </div>
                        </div>

                    </div>

                    <div className="col-xs-3 col-lg-3">
                        < div className="form-group">

                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Fecha de nacimiento</span>
                                </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " max="2100-01-01" name="fec_nacimiento_conyugue" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                    && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                        && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} value={"" + this.state.fec_nacimiento_conyugue + ""} tabIndex={17} />
                            </div>
                        </div>

                    </div>

                    <div className="col-xs-3 col-lg-3">
                        < div className="form-group">

                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Fecha de aniversario</span>
                                </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " max="2100-01-01" name="fec_aniversario" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                    && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                        && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} value={"" + this.state.fec_aniversario + ""} tabIndex={18} />
                            </div>
                        </div>

                    </div>

                </div>


                < div className="row">
                    <div className="col-xs-8 col-lg-8">
                        <div className="form-group">

                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre del hijo (a)</span>
                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                    && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                        && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChangeHijos} name="nombre_completo%1" value={this.state.hijo1.nombre_completo} className="form-control " />
                            </div>
                        </div>
                    </div>

                    <div className="col-xs-3 col-lg-3">
                        < div className="form-group">

                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Fecha de nacimiento</span>
                                </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " max="2100-01-01" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                    && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                        && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChangeHijos} name="fec_nacimiento%1" value={"" + this.state.hijo1.fec_nacimiento + ""} />
                            </div>
                        </div>

                    </div>
                </div>

                {
                    this.state.contadorHijos == 2 || this.state.contadorHijos > 2 ?
                        < div className="row">
                            <div className="col-xs-8 col-lg-8">
                                <div className="form-group">

                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre del hijo (a)</span>
                                        </span> <input type="text" style={{ borderColor: '#E1E5F0' }} disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                                && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChangeHijos} name="nombre_completo%2" value={this.state.hijo2.nombre_completo} className="form-control " />
                                    </div>
                                </div>
                            </div>

                            <div className="col-xs-3 col-lg-3">
                                < div className="form-group">

                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Fecha de nacimiento</span>
                                        </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " max="2100-01-01" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                                && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChangeHijos} name="fec_nacimiento%2" value={"" + this.state.hijo2.fec_nacimiento + ""} />
                                    </div>
                                </div>

                            </div>



                        </div> :
                        ''
                }

                {
                    this.state.contadorHijos == 3 || this.state.contadorHijos > 3 ?
                        < div className="row">
                            <div className="col-xs-8 col-lg-8">
                                <div className="form-group">

                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre del hijo (a)</span>
                                        </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                                && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChangeHijos} name="nombre_completo%3" value={this.state.hijo3.nombre_completo} />
                                    </div>
                                </div>
                            </div>

                            <div className="col-xs-3 col-lg-3">
                                < div className="form-group">

                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Fecha de nacimiento</span>
                                        </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " max="2100-01-01" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                                && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChangeHijos} name="fec_nacimiento%3" value={"" + this.state.hijo3.fec_nacimiento + ""} />
                                    </div>
                                </div>

                            </div>



                        </div> :
                        ''
                }
                {
                    this.state.contadorHijos == 4 || this.state.contadorHijos > 4 ?
                        < div className="row">
                            <div className="col-xs-8 col-lg-8">
                                <div className="form-group">

                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre del hijo (a)</span>
                                        </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                                && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChangeHijos} name="nombre_completo%4" value={this.state.hijo4.nombre_completo} />
                                    </div>
                                </div>
                            </div>

                            <div className="col-xs-3 col-lg-3">
                                < div className="form-group">

                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Fecha de nacimiento</span>
                                        </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " max="2100-01-01" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                                && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChangeHijos} name="fec_nacimiento%4" value={"" + this.state.hijo4.fec_nacimiento + ""} />
                                    </div>
                                </div>

                            </div>



                        </div> :
                        ''
                }
                {
                    this.state.contadorHijos == 5 ?
                        < div className="row">
                            <div className="col-xs-8 col-lg-8">
                                <div className="form-group">

                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre del hijo (a)</span>
                                        </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                                && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChangeHijos} name="nombre_completo%5" value={this.state.hijo5.nombre_completo} />
                                    </div>
                                </div>
                            </div>

                            <div className="col-xs-3 col-lg-3">
                                < div className="form-group">

                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Fecha de nacimiento</span>
                                        </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " max="2100-01-01" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                                && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChangeHijos} name="fec_nacimiento%5" value={"" + this.state.hijo5.fec_nacimiento + ""} />
                                    </div>
                                </div>

                            </div>



                        </div> :
                        ''
                }
                <div className="col-xs-3 col-lg-3">
                    <button type="button" class="btn text-white" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                        && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onClick={() => this.inyectaHijos(true)} style={{ backgroundColor: "#0F69B8" }}>+ Agregar Hijo</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <button type="button" class="btn text-white" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                        && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onClick={() => this.inyectaHijos(false)} style={{ backgroundColor: "#0F69B8" }}>- Eliminar Hijo</button>
                </div>

                {/*aqui deberia ir el contenido anidado */}


                <hr></hr>
                <div>
                    <div className="row">
                        < div class="card-header">
                            <h5 class="mb-0">Contacto en caso de emergencia </h5>
                        </div>
                    </div>

                    < div className="row">
                        <div className="col-xs-6 col-lg-6">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre completo</span>
                                    </span> <input type="text" maxlength="150" style={{ borderColor: '#E1E5F0' }} className="form-control " disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                        && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name="nombre_contacto" value={this.state.nombre_contacto} tabIndex={19} />
                                </div>
                                {
                                    this.state.nombre_contacto == "" || this.state.nombre_contacto == null ?
                                        <span className="" style={{ color: "red" }}>El nombre del contacto es un campo requerido</span> : ''
                                }
                            </div>



                        </div>
                        <div className="col-xs-3 col-lg-3">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Parentesco</span>
                                    </span>
                                    {this.state.selectParentesco}
                                </div>
                                {
                                    this.state.parentesco_id == 0 ?
                                        <span className=" " style={{ color: "red" }}>El parentesco es un campo requerido</span> : ''
                                }
                            </div>
                        </div>

                        <div className="col-xs-3 col-lg-3">
                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Teléfono</span>
                                    </span> <input type="number" style={{ borderColor: '#E1E5F0' }} className="form-control " maxLength={10} disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                        && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name="tel_contacto" value={this.state.tel_contacto} tabIndex={21} />
                                </div>
                                {
                                    this.state.tel_contacto == 0 || (this.state.tel_contacto + "").length <= 9 ?
                                        <span className=" " style={{ color: "red" }}>El telefono del contacto es un campo requerido</span> : ''
                                }
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        )
    }

}