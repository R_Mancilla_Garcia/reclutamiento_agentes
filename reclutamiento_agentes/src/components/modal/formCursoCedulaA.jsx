import React, { Component } from "react";
import datos from '../urls/datos.json'

export default class FormCursoCedulaA extends Component {

    constructor() {

        super();
        this.onChange = this.onChange.bind(this)
        this.guardarElemento=this.guardarElemento.bind(this)
         this.changeActivo=this.changeActivo.bind(this)
        this.state = {
            anios: [2016, 2017, 2018, 2019, 2021],
            plaza:'',
            modalidad:'',
            horario:'',
            fecha_inicio:'',
            activo: true,
            idActualizar: 0,
            fechaMinima:''
        };
    }

    onChange = e => {
        console.log("entrando", e.target.name, e.target.value)
        this.setState({ [e.target.name]: e.target.value })
    }

    changeActivo(){
        this.setState({activo:!this.state.activo})
    }

    onKeyPressed(e) {
        e.stopPropagation();
        e.preventDefault();
        e.target.value = null
    }

    guardarElemento(){
        console.log(this.props)
        let json={
            "plaza":this.state.plaza,
            "modalidad":this.state.modalidad,
            "horario":this.state.horario,
            "fecha_inicio":this.state.fecha_inicio,         
            "activo": this.state.activo ? 1:0,
            "user_id":parseInt(this.props.idUsuario),
            }
            if(this.props.contenido != undefined){
                this.props.guardaElementoNuevo(datos.urlServicePy+"parametros_upd/api_cat_curso_cedula/"+this.state.idActualizar,json,"PUT","mostrarTablaParametrosBancos")
            }else{
            this.props.guardaElementoNuevo(datos.urlServicePy+"parametros/api_cat_curso_cedula/0",json,"POST","mostrarTablaParametrosBancos")
            }
    }


    UNSAFE_componentWillMount() {
        console.log("entrando a form ",this.props.contenido)
       this.setState({fechaMinima:new Date().toISOString().split("T")[0]});
        if(this.props.contenido != undefined){
            console.log("entrando a form ",this.props.contenido)
            this.setState({
                idActualizar:this.props.contenido[0].value,
                fecha_inicio:this.props.contenido[1].value,
                horario:this.props.contenido[2].value,
                modalidad:this.props.contenido[3].value,
                plaza:this.props.contenido[4].value,
                activo:this.props.contenido[5].value == 1 ? true:false
                
            })
        }
    }


    render() {
        return (
            <div>
                <div className="modal-body">
                    <div className="card-body">
                        <div className="row">



                            <div className="col-xs-12 col-lg-3">
                                <div className="form-group">

                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Fecha Inicio</span>
                                        </span> <input type="date" name="fecha_inicio" min={this.state.fechaMinima} value={this.state.fecha_inicio} max="2100-01-01" tabIndex={0} onKeyDown={this.onKeyPressed.bind()} onChange={this.onChange} className="form-control bg-dark border-dark text-white" />
                                    </div>
                                </div>
                            </div>


                            <div className="col-xs-12 col-lg-3">

                                <div className="form-group">

                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Horario</span>
                                        </span> <input type="time" name="horario" value={this.state.horario} onChange={this.onChange} className="form-control bg-dark border-dark text-white" />
                                    </div>
                                </div>
                            </div>


                            <div className="col-xs-12 col-lg-3">
                                <div className="form-group">

                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Modalidad</span>
                                        </span> <input type="text" name="modalidad" value={this.state.modalidad} onChange={this.onChange} className="form-control bg-dark border-dark text-white" />
                                    </div>
                                </div>
                            </div>

                            <div className="col-xs-12 col-lg-3">
                                <div className="form-group">

                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Plaza</span>
                                        </span> <input type="text" name="plaza" value={this.state.plaza} onChange={this.onChange} className="form-control bg-dark border-dark text-white" />
                                    </div>
                                </div>
                            </div>







                        </div>
                        <div className="row">
                            <div className="col-xs-12 col-lg-4">
                                <div className="form-group">

                                    <div className="input-group">
                                        <span className="input-group-prepend">
                                            <span className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Parámetro activo</span>
                                            <div className="custom-control custom-switch custom-control-warning mr-2">
                                                <input type="checkbox" checked={this.state.activo} name="activo" class="custom-control-input"  ></input>
                                                <label class="custom-control-label" onClick={this.changeActivo} for="swc_warning"></label>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                            </div>

                        </div>




                    </div>
                </div >


                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-warning" id="btnAgregar" data-dismiss="modal" disabled={this.state.fecha_inicio.length == 0 || this.state.modalidad.length == 0}  onClick={this.guardarElemento} >Agregar</button>
                </div>

            </div >
        )
    }
}