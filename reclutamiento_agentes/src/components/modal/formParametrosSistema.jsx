import React, { Component } from "react";
import datos from '../urls/datos.json'

export default class FormParametrosSistema extends Component {

    constructor() {
        super();
        this.guardarElemento = this.guardarElemento.bind(this)
        this.onChange = this.onChange.bind(this)
        this.state = {
            descripcion: '',
            valor: '',
            filler: '',
            idActualizar: 0
        }
    }


    UNSAFE_componentWillMount() {
        if (this.props.contenido != undefined) {
            console.log("entrando a form ", this.props.contenido)
            this.setState({
                idActualizar: this.props.contenido[0].value,
                descripcion: this.props.contenido[1].value,
                valor: this.props.contenido[2].value,
                filler: this.props.contenido[3].value
            })
        }

    }

    onChange = e => {
        // console.log("entrando select ", e.target.name, e.target.value)
        console.log("Entro aquí")
        this.setState({ [e.target.name]: e.target.value })
    }


    guardarElemento() {
        console.log(this.props)
        let json = {
            "descripcion": this.state.descripcion,
            "valor": this.state.valor,
            "filler": this.state.filler,
            "user_id": parseInt(this.props.idUsuario),
            "id": parseInt(this.state.idActualizar)
        }
        if (this.props.contenido != undefined) {
            this.props.guardaElementoNuevo(datos.urlServicePy+"parametros_upd/api_cat_parametros_sistema/" + parseInt(this.state.idActualizar), json, "PUT", "mostrarTablaParametrosSistema")
        } else {
            this.props.guardaElementoNuevo(datos.urlServicePy+"parametros/api_cat_parametros_sistema/0", json, "POST", "mostrarTablaParametrosSistema")
        }
    }





    render() {
        return (
            <div>
                <div className="modal-body">
                    <div className="card-body">
                        <div className="row">
                            <div className="col-xs-5 col-lg-5">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Descripción</span>
                                        </span> <input type="text" name="descripcion" value={this.state.descripcion} onChange={this.onChange} disabled = { (this.props.contenido != undefined || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.contenido != undefined || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER")} className="form-control bg-dark border-dark text-white" />
                                    </div>
                                </div>
                            </div>
                            <div className="col-xs-6 col-lg-6">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Filtro</span>
                                        </span> <input type="text" name="filler" value={this.state.filler} onChange={this.onChange} disabled = { (this.props.contenido != undefined || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.contenido != undefined || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER")} className="form-control bg-dark border-dark text-white"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                        <div className="col-xs-6 col-lg-6">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Valor</span>
                                        </span> <input type="text" name="valor" value={this.state.valor} onChange={this.onChange} disabled = { (this.props.contenido != undefined || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.contenido != undefined || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER")} className="form-control bg-dark border-dark text-white"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-warning" id="btnAgregar" data-dismiss="modal" disabled={(this.props.contenido != undefined || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.contenido != undefined || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER") || this.state.descripcion.length == 0 || this.state.valor.length == 0 || this.state.filler == 0 } onClick={this.guardarElemento} >Agregar</button>
                </div>
            </div>
        )
    }


}