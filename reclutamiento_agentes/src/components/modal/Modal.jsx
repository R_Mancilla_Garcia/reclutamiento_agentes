import React, { Component } from "react";
import FormParametrosReclutamiento from "./formParametrosReclutamiento";
import FormCausasBaja from "./formCausasBaja";
import FormPuestos from "./formPuestos";
import FormAreas from "./formAreasDeptos";
import FormBancos from "./formBancos";
import FormProfesiones from "./formProfesiones";
import FormCursoInmersion from "./formCursoInmersion";
import FormCursoIdeas from "./formCursoIdeas";
import FormMotivoEnvios from "./FormMotivoEnvios";
import FormFuenteReclutamiento from "./formFuenteReclutamiento";
import FormFuenteRecluBonos from "./formFuenteRecluBonos";
import FormManejoAgentes from "./formManejoAgentes";
import FormEstatusAps from "./formEstatusAps";
import FormCreaReferido from "./formCreaReferido";
import FormCreaProspecto from "./fromCreaProspecto";
import FormCursoCedulaA from "./formCursoCedulaA";
import FormFigura from "./formFigura";
import FormParentesco from "./formParentesco";
import FormEstadoCivil from "./formEstadoCivil";
import FormParametrosSistema from "./formParametrosSistema";
import FormEmailDescripcion from "./formEmailDescripcion";
import FormProcesoNotificacion from "./formProcesoNotificacion";
import FormGrupoDestino from "./formGrupoDestino";
import FormTiposFlujo from "./formTiposFlujo";
import FormMailGrupoDestino from "./formMailGrupoDestino";
import FormMailFlujo from "./formMailFlujo";
import FormCausasBajaProspecto from "./formCausasBajaProspecto";

export default class Modal extends Component {
   
    constructor() {
        super();
        this.state = {
          
        };
    }

    UNSAFE_componentWillMount() {
        console.log("lanzando modal ",this.props.contenido)
        
        
    }

    render() {
        return (
            <div>
                

                <div id={this.props.modalNombre} className="modal fade" tabindex="-1">
                            <div className="modal-dialog modal-full">
                                <div className="modal-content text-white" style={{ backgroundColor: "#313A46" }}>
                                    <div className="modal-header bg-primary text-white">
                                        <h6 className="modal-title">{this.props.tituloModal != undefined ? this.props.tituloModal:'Agregar nuevo parámetro'}</h6>
                                        <button type="button" className="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    
                                            {
                                                this.props.modalNombre == 'modal-parametroReclutamiento'  ?<FormParametrosReclutamiento guardaElementoNuevo={this.props.guardaElementoNuevo}/>:''
                                            }

                                            
                                            {
                                                this.props.modalNombre.includes('modal-parametroReclutamientoEdicion') == true  ?<FormParametrosReclutamiento contenido={this.props.contenido} guardaElementoNuevo={this.props.guardaElementoNuevo}/>:''
                                            }


                                            {
                                                this.props.modalNombre == 'modal-bajaTemporal'  ?<FormCausasBaja idUsuario={this.props.idUsuario} guardaElementoNuevo={this.props.guardaElementoNuevo}/>:''
                                            }

                                            
                                            {
                                                this.props.modalNombre.includes('modal-bajaTemporalEdicion') == true ? <FormCausasBaja idUsuario={this.props.idUsuario} contenido={this.props.contenido} guardaElementoNuevo={this.props.guardaElementoNuevo}/>:''
                                            }


                                            {
                                                this.props.modalNombre == 'modal-bajaProspecto'  ?<FormCausasBajaProspecto idUsuario={this.props.idUsuario} guardaElementoNuevo={this.props.guardaElementoNuevo}/>:''
                                            }

                                            
                                            {
                                                this.props.modalNombre.includes('modal-bajaProspectoTemporalEdicion') == true ? <FormCausasBajaProspecto idUsuario={this.props.idUsuario} contenido={this.props.contenido} guardaElementoNuevo={this.props.guardaElementoNuevo}/>:''
                                            }


                                            {
                                                this.props.modalNombre == 'modal-puestos'  ?<FormPuestos idUsuario={this.props.idUsuario} guardaElementoNuevo={this.props.guardaElementoNuevo}/>:''
                                            }

                                            
                                            {
                                                this.props.modalNombre.includes('modal-puestosEdicion') == true ? <FormPuestos idUsuario={this.props.idUsuario} contenido={this.props.contenido} guardaElementoNuevo={this.props.guardaElementoNuevo}/>:''
                                            }

                                            {
                                                this.props.modalNombre == 'modal-areas'  ?<FormAreas idUsuario={this.props.idUsuario} guardaElementoNuevo={this.props.guardaElementoNuevo}/>:''
                                            }

                                            
                                            {
                                                this.props.modalNombre.includes('modal-areasEdicion') == true ? <FormAreas idUsuario={this.props.idUsuario} contenido={this.props.contenido} guardaElementoNuevo={this.props.guardaElementoNuevo}/>:''
                                            }

                                            {
                                                this.props.modalNombre == 'modal-bancos'  ?<FormBancos idUsuario={this.props.idUsuario} guardaElementoNuevo={this.props.guardaElementoNuevo}/>:''
                                            }

                                            
                                            {
                                                this.props.modalNombre.includes('modal-bancosEdicion') == true ? <FormBancos idUsuario={this.props.idUsuario} contenido={this.props.contenido} guardaElementoNuevo={this.props.guardaElementoNuevo}/>:''
                                            }


                                            
                                            {
                                                this.props.modalNombre == 'modal-profesiones'  ?<FormProfesiones idUsuario={this.props.idUsuario} guardaElementoNuevo={this.props.guardaElementoNuevo}/>:''
                                            }

                                            
                                            {
                                                this.props.modalNombre.includes('modal-profesionesEdicion') == true ? <FormProfesiones idUsuario={this.props.idUsuario} contenido={this.props.contenido} guardaElementoNuevo={this.props.guardaElementoNuevo}/>:''
                                            }



                                            {/*formulario del modulo de cursos */}
                                        
                                            {
                                                this.props.modalNombre == 'modal-inmersion'  ?<FormCursoInmersion idUsuario={this.props.idUsuario} guardaElementoNuevo={this.props.guardaElementoNuevo} mostrarMensajeModal={this.props.mostrarMensajeModal}/>:''
                                            }

                                            
                                            {
                                                this.props.modalNombre.includes('modal-inmersionEdicion') == true ? <FormCursoInmersion idUsuario={this.props.idUsuario} contenido={this.props.contenido} guardaElementoNuevo={this.props.guardaElementoNuevo} mostrarMensajeModal={this.props.mostrarMensajeModal}/>:''
                                            }
                                    
                                            {
                                                this.props.modalNombre == 'modal-cursoIdeas'  ?<FormCursoIdeas idUsuario={this.props.idUsuario} guardaElementoNuevo={this.props.guardaElementoNuevo} cancelarModal={this.props.cancelarModal}/>:''
                                            }

                                            
                                            {
                                                this.props.modalNombre.includes('modal-cursoIdeasEdicion') == true ? <FormCursoIdeas idUsuario={this.props.idUsuario} contenido={this.props.contenido} guardaElementoNuevo={this.props.guardaElementoNuevo} edicion = {true} cancelarModal={this.props.cancelarModal}/>:''
                                            }


                                            {/* formulario reservas */}
                                    
                                            {
                                                this.props.modalNombre == 'modal-motivoEnvio'  ?<FormMotivoEnvios idUsuario={this.props.idUsuario} guardaElementoNuevo={this.props.guardaElementoNuevo}/>:''
                                            }

                                            
                                            {
                                                this.props.modalNombre.includes('modal-motivoEnvioEdicion') == true ? <FormMotivoEnvios idUsuario={this.props.idUsuario} contenido={this.props.contenido} guardaElementoNuevo={this.props.guardaElementoNuevo}/>:''
                                            }

                                            {
                                                this.props.modalNombre == 'modal-fuenteReclutamiento'  ?<FormFuenteReclutamiento idUsuario={this.props.idUsuario} guardaElementoNuevo={this.props.guardaElementoNuevo}/>:''
                                            }

                                            
                                            {
                                                this.props.modalNombre.includes('modal-fuenteReclutamientoEdicion') == true ? <FormFuenteReclutamiento idUsuario={this.props.idUsuario} contenido={this.props.contenido} guardaElementoNuevo={this.props.guardaElementoNuevo}/>:''
                                            }

                                            
                                            {
                                                this.props.modalNombre == 'modal-recluBonos'  ?<FormFuenteRecluBonos idUsuario={this.props.idUsuario} guardaElementoNuevo={this.props.guardaElementoNuevo}/>:''
                                            }

                                            
                                            {
                                                this.props.modalNombre.includes('modal-recluBonosEdicion') == true ? <FormFuenteRecluBonos idUsuario={this.props.idUsuario} contenido={this.props.contenido} guardaElementoNuevo={this.props.guardaElementoNuevo}/>:''
                                            }

                                            {
                                                this.props.modalNombre == 'modal-manejo'  ?<FormManejoAgentes idUsuario={this.props.idUsuario} guardaElementoNuevo={this.props.guardaElementoNuevo}/>:''
                                            }

                                            
                                            {
                                                this.props.modalNombre.includes('modal-manejoEdicion') == true ? <FormManejoAgentes idUsuario={this.props.idUsuario} contenido={this.props.contenido} guardaElementoNuevo={this.props.guardaElementoNuevo}/>:''
                                            }


                                            {
                                                this.props.modalNombre == 'modal-estatus'  ?<FormEstatusAps idUsuario={this.props.idUsuario} guardaElementoNuevo={this.props.guardaElementoNuevo}/>:''
                                            }

                                            
                                            {
                                                this.props.modalNombre.includes('modal-estatusEdicion') == true ? <FormEstatusAps idUsuario={this.props.idUsuario} contenido={this.props.contenido}    guardaElementoNuevo={this.props.guardaElementoNuevo}/>:''
                                            }



                                            {/*aqui vendran datos del referido */}

                                            {
                                                this.props.modalNombre.includes('modal-creaReferido') == true ? <FormCreaReferido  idUsuario={this.props.idUsuario }guardaElementoNuevo={this.props.guardaElementoNuevo} />:''
                                            }

                                             {/*aqui vendran datos del prospecto */}

                                             {
                                                this.props.modalNombre.includes('modal-creaProspecto') == true ? <FormCreaProspecto guardaElementoNuevo={this.props.guardaElementoNuevo} />:''
                                             }
                                             {
                                                 this.props.modalNombre == 'modal-CursoCedulaA' == true ? <FormCursoCedulaA idUsuario={this.props.idUsuario}  guardaElementoNuevo={this.props.guardaElementoNuevo} />:''
                                             }

                                             {
                                                 this.props.modalNombre.includes('modal-CursoCedulaAEdicion') == true ? <FormCursoCedulaA  contenido={this.props.contenido} idUsuario={this.props.idUsuario}  guardaElementoNuevo={this.props.guardaElementoNuevo} />:''
                                             }

                                             {/* formualrios de los catalogos parametros */}

                                             {
                                                 this.props.modalNombre == 'modal-figura' == true ? <FormFigura idUsuario={this.props.idUsuario}  guardaElementoNuevo={this.props.guardaElementoNuevo} tipoUsuario= {this.props.tipoUsuario}/>:''
                                             }

                                             {
                                                 this.props.modalNombre.includes('modal-figuraEdicion') == true ? <FormFigura  contenido={this.props.contenido} idUsuario={this.props.idUsuario}  guardaElementoNuevo={this.props.guardaElementoNuevo} tipoUsuario= {this.props.tipoUsuario}/>:''
                                             }

                                            {
                                                 this.props.modalNombre == 'modal-parentesco' == true ? <FormParentesco idUsuario={this.props.idUsuario}  guardaElementoNuevo={this.props.guardaElementoNuevo} tipoUsuario= {this.props.tipoUsuario}/>:''
                                             }

                                             {
                                                 this.props.modalNombre.includes('modal-parentescoEdicion') == true ? <FormParentesco  contenido={this.props.contenido} idUsuario={this.props.idUsuario}  guardaElementoNuevo={this.props.guardaElementoNuevo} tipoUsuario= {this.props.tipoUsuario}/>:''
                                             }

                                            {
                                                 this.props.modalNombre == 'modal-estadocivil' == true ? <FormEstadoCivil idUsuario={this.props.idUsuario}  guardaElementoNuevo={this.props.guardaElementoNuevo} tipoUsuario= {this.props.tipoUsuario}/>:''
                                             }

                                             {
                                                 this.props.modalNombre.includes('modal-estadocivilEdicion') == true ? <FormEstadoCivil  contenido={this.props.contenido} idUsuario={this.props.idUsuario}  guardaElementoNuevo={this.props.guardaElementoNuevo} tipoUsuario= {this.props.tipoUsuario}/>:''
                                             }

{
                                                 this.props.modalNombre == 'modal-parametrossistema' == true ? <FormParametrosSistema idUsuario={this.props.idUsuario}  guardaElementoNuevo={this.props.guardaElementoNuevo} tipoUsuario= {this.props.tipoUsuario}/>:''
                                             }

                                             {
                                                 this.props.modalNombre.includes('modal-parametrossistemaEdicion') == true ? <FormParametrosSistema  contenido={this.props.contenido} idUsuario={this.props.idUsuario}  guardaElementoNuevo={this.props.guardaElementoNuevo} tipoUsuario= {this.props.tipoUsuario}/>:''
                                             }

                                             {/*  */}

                                             {
                                                 this.props.modalNombre == 'modal-mail_descripcion' == true ? <FormEmailDescripcion idUsuario={this.props.idUsuario}  guardaElementoNuevo={this.props.guardaElementoNuevo} />:''
                                             }

                                             {
                                                 this.props.modalNombre.includes('modal-mail_descripcionEdicion') == true ? <FormEmailDescripcion  contenido={this.props.contenido} idUsuario={this.props.idUsuario}  guardaElementoNuevo={this.props.guardaElementoNuevo} />:''
                                             }

                                             {/*FormProcesoNotificacion */}

                                             {
                                                 this.props.modalNombre == 'modal-proceso' == true ? <FormProcesoNotificacion idUsuario={this.props.idUsuario}  guardaElementoNuevo={this.props.guardaElementoNuevo} />:''
                                             }

                                             {
                                                 this.props.modalNombre.includes('modal-procesoEdicion') == true ? <FormProcesoNotificacion  contenido={this.props.contenido} idUsuario={this.props.idUsuario}  guardaElementoNuevo={this.props.guardaElementoNuevo} />:''
                                             }
                                             

                                             {
                                                 this.props.modalNombre == 'modal-grupoDestino' == true ? <FormGrupoDestino idUsuario={this.props.idUsuario}  guardaElementoNuevo={this.props.guardaElementoNuevo} />:''
                                             }

                                             {
                                                 this.props.modalNombre.includes('modal-grupoDestinoEdicion') == true ? <FormGrupoDestino  contenido={this.props.contenido} idUsuario={this.props.idUsuario}  guardaElementoNuevo={this.props.guardaElementoNuevo} />:''
                                             }
                                             {
                                                 this.props.modalNombre == 'modal-tipoFlujo' == true ? <FormTiposFlujo idUsuario={this.props.idUsuario}  guardaElementoNuevo={this.props.guardaElementoNuevo} />:''
                                             }

                                             {
                                                 this.props.modalNombre.includes('modal-tipoFlujoEdicion') == true ? <FormTiposFlujo  contenido={this.props.contenido} idUsuario={this.props.idUsuario}  guardaElementoNuevo={this.props.guardaElementoNuevo} />:''
                                             }




                                            {
                                                 this.props.modalNombre == 'modal-mailFlujo' == true ? <FormMailFlujo idUsuario={this.props.idUsuario}  guardaElementoNuevo={this.props.guardaElementoNuevo} />:''
                                             }

                                             {
                                                 this.props.modalNombre.includes('modal-mailFlujoEdicion') == true ? <FormMailFlujo  contenido={this.props.contenido} idUsuario={this.props.idUsuario}  guardaElementoNuevo={this.props.guardaElementoNuevo} />:''
                                             }




                                            {
                                                 this.props.modalNombre == 'modal-mailGrupoDestino' == true ? <FormMailGrupoDestino idUsuario={this.props.idUsuario}  guardaElementoNuevo={this.props.guardaElementoNuevo} />:''
                                             }

                                             {
                                                 this.props.modalNombre.includes('modal-mailGrupoDestinoEdicion') == true ? <FormMailGrupoDestino  contenido={this.props.contenido} idUsuario={this.props.idUsuario}  guardaElementoNuevo={this.props.guardaElementoNuevo} />:''
                                             }

                                </div>
                            </div>
                        </div>

            </div>
        )
    }

}