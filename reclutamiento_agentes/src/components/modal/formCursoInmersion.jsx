import React, { Component } from "react";
import datos from '../urls/datos.json'

export default class FormCursoInmersion extends Component {

    constructor() {
       
        super();
        this.onChange=this.onChange.bind(this)
        this.guardarElemento=this.guardarElemento.bind(this)
        this.changeActivo=this.changeActivo.bind(this)
        this.state = {
            anios:[2016,2017,2018,2019,2021],
            generacion:"",
            fecha_inicio:"",
            fechaMinima:'',
            modalidad:"",
            activo:true,
            idActualizar:0,
            activaModal: false,
            banChange: false
        };
    }

    UNSAFE_componentWillMount() {
        this.setState({ fechaMinima:new Date().toISOString().split("T")[0]})
        if(this.props.contenido != undefined){
            console.log("entrando a form ",this.props.contenido)
            this.setState({
                idActualizar:this.props.contenido[0].value,
                generacion:this.props.contenido[1].value,
                fecha_inicio:this.props.contenido[2].value,
                modalidad:this.props.contenido[3].value,
                activo:this.props.contenido[4].value
                
            })
        }
    }


    onChange = e =>{
        console.log("entrando",e.target.name,e.target.value)
        //generacion
        if(this.state.idActualizar > 0 && e.target.name == "generacion"){
            this.setState({banChange:true})
        }
        this.setState({ [e.target.name]: e.target.value })
    } 

    changeActivo(){
      this.setState({activo:!this.state.activo})
    }

    onKeyPressed(e) {
        e.stopPropagation();
        e.preventDefault();
        e.target.value = null
    }

    guardarElemento(){
        console.log(this.state)
        fetch(datos.urlServicePy+"parametros/api_cat_curso_inmersion/0")
            .then(response => response.json())
            .then(jsonCursoInmersion => {
                let fila = jsonCursoInmersion.filas
                let banRepetido = false;
                let valorRepetido = "";
                for (var i = 0; i < fila.length; i++) {
                    if(this.state.generacion == fila[i].fila[1].value){
                        banRepetido = true;
                        valorRepetido = fila[i].fila[1].value;
                    }
                }
                if(!banRepetido || this.state.idActualizar > 0){
                    //this.setState({activaModal:false})
                    if(this.state.banChange && (this.state.generacion == valorRepetido)){
                        this.props.mostrarMensajeModal(2);
                    }else{
                        let json={
                            //   "entidad": "api_cat_curso_inmersion",
                            "generacion": this.state.generacion,
                            "fecha_inicio":this.state.fecha_inicio,
                            "modalidad":this.state.modalidad,
                            "activo": this.state.activo ? 1:0,
                            "user_id":parseInt(this.props.idUsuario),
                            //"ts_alta_audit": "2014-04-20T14:57:00Z"
                        }
                        if(this.props.contenido != undefined){
                            this.props.guardaElementoNuevo(datos.urlServicePy+"parametros_upd/api_cat_curso_inmersion/"+this.state.idActualizar,json,"PUT","mostrarTablaCursosInmersion")
                        }else{
                            this.props.guardaElementoNuevo(datos.urlServicePy+"parametros/api_cat_curso_inmersion/0",json,"POST","mostrarTablaCursosInmersion")
                        }
                    }
                }else{
                    //this.setState({activaModal:true})
                    //this.modalDuplicado.click();
                    this.props.mostrarMensajeModal(1);
                }
            })
    }

    render(){
        return(
            <div>
            <div className="modal-body">
                <div className="card-body">
                    <div className="row">
                        

                    <div className="col-xs-12 col-lg-2">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Generación</span>
                                    </span> <input type="text"  name="generacion" value={this.state.generacion}  onChange={this.onChange}   className="form-control bg-dark border-dark text-white" />
                                </div>
                                {
                                                this.state.generacion.length ==0 ?
                                                <span style={{ color: "red" }}>El campo Generación debe ser ingresado</span> : ''
                                }
                            </div>
                        </div>


                        <div className="col-xs-12 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Fecha probable de inicio</span>
                                    </span> <input type="date" name="fecha_inicio" value={this.state.fecha_inicio.split("T")[0]}  min={this.state.fechaMinima} max="2100-01-01" onChange={this.onChange}  onKeyDown={this.onKeyPressed.bind()} className="form-control bg-dark border-dark text-white" />
                                </div>
                                {
                                                this.state.fecha_inicio.length ==0 ?
                                                <span style={{ color: "red" }}>El campo Fecha probable de inicio debe ser ingresado</span> : ''
                                }
                            </div>
                        </div>


                       

                        <div className="col-xs-12 col-lg-3">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Modalidad</span>
                                    </span> <input type="text" name="modalidad" value={this.state.modalidad} onChange={this.onChange} className="form-control bg-dark border-dark text-white" />
                                </div>
                                {
                                                this.state.modalidad.length ==0 ?
                                                <span style={{ color: "red" }}>El campo Modalidad debe ser ingresado</span> : ''
                                }
                            </div>
                        </div>

                        <div className="col-xs-12 col-lg-3">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend">
                                        <span className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Parámetro activo</span>
                                        <div className="custom-control custom-switch custom-control-warning mr-2">
                                            <input type="checkbox"  checked={this.state.activo} name="activo" class="custom-control-input"  ></input>
                                            <label class="custom-control-label"   onClick={this.changeActivo} for="swc_warning"></label>
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </div>

                    </div>



                    
                </div>
            </div>

            <div class="modal-footer d-flex justify-content-center">
                <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-warning" id="btnAgregar" data-dismiss="modal" disabled={this.state.generacion.length ==0 || this.state.modalidad.length ==0 || this.state.fecha_inicio.length == 0} onClick={this.guardarElemento} >Agregar</button>
            </div>

        </div>
        )
    }
}
