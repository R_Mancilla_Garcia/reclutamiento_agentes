import React, { Component } from "react";
import datos from '../urls/datos.json'

export default class FormPuestos extends Component {

    constructor() {
       
        super();
        this.onChange=this.onChange.bind(this)
        this.guardarElemento=this.guardarElemento.bind(this)
        this.changeActivo=this.changeActivo.bind(this)
        this.state = {
            anios:[2016,2017,2018,2019,2021],
            descripcion:"",
           puesto:"",
            activo:true,
            idActualizar:0
        };
    }

    UNSAFE_componentWillMount() {
        
        if(this.props.contenido != undefined){
            console.log("entrando a form ",this.props.contenido)
            this.setState({
                idActualizar:this.props.contenido[0].value,
                puesto:this.props.contenido[1].value,
                descripcion:this.props.contenido[2].value,
                activo:this.props.contenido[3].value,
                
            })
        }
    }


    onChange = e =>{
        console.log("entrando",e.target.name,e.target.value)
        this.setState({ [e.target.name]: e.target.value })
    } 

      changeActivo(){
        this.setState({activo:!this.state.activo})
      }


    guardarElemento(){
        console.log(this.state)
        let json={
          //  "entidad": "api_cat_puestos",
            "puesto":this.state.puesto,
            "descripcion": this.state.descripcion,
            "activo": this.state.activo ? 1:0,
            "user_id":parseInt(this.props.idUsuario),
           // "ts_alta_audit": "2014-04-20T14:57:00Z"
            }
            if(this.props.contenido != undefined){
                this.props.guardaElementoNuevo(datos.urlServicePy+"parametros_upd/api_cat_puestos/"+this.state.idActualizar,json,"PUT","mostrarTablaParametrosPuestos")
            }else{
            this.props.guardaElementoNuevo(datos.urlServicePy+"parametros/api_cat_puestos/0",json,"POST","mostrarTablaParametrosPuestos")
            }
    }


    render(){
        return(
            <div>
            <div className="modal-body">
                <div className="card-body">
                    <div className="row">
                       
                       {/* <div className="col-xs-12 col-lg-3">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Fecha alta</span>
                                    </span> <input type="date" disabled className="form-control bg-dark border-dark text-white" />
                                </div>
                            </div>
                        </div>*/}

                        <div className="col-xs-12 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Puesto</span>
                                    </span> <input type="text" name="puesto" value={this.state.puesto} onChange={this.onChange} className="form-control bg-dark border-dark text-white" />
                                </div>
                            </div>
                        </div>

                        <div className="col-xs-12 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Descripción</span>
                                    </span> <input type="text" name="descripcion" value={this.state.descripcion} onChange={this.onChange} className="form-control bg-dark border-dark text-white" />
                                </div>
                            </div>
                        </div>

                        <div className="col-xs-12 col-lg-3">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend">
                                        <span className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Parámetro activo</span>
                                        <div className="custom-control custom-switch custom-control-warning mr-2">
                                            <input type="checkbox"  checked={this.state.activo} name="activo" class="custom-control-input"  ></input>
                                            <label class="custom-control-label"   onClick={this.changeActivo} for="swc_warning"></label>
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </div>

                    </div>



                    
                </div>
            </div>


            <div class="modal-footer d-flex justify-content-center">
                <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-warning" id="btnAgregar" data-dismiss="modal" disabled={this.state.descripcion.length <3  || this.state.puesto.length <3}  onClick={this.guardarElemento} >Agregar</button>
            </div>

        </div>
        )
    }
}
