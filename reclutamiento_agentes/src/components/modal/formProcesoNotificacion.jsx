import React, { Component } from "react";
import datos from '../urls/datos.json'

export default class FormProcesoNotificacion extends Component {

    constructor() {
        super();
        this.guardarElemento=this.guardarElemento.bind(this)
        this.onChange = this.onChange.bind(this)
        this.state = {
           
            proceso:'',
            idActualizar:0
        }
    }


    UNSAFE_componentWillMount() {
        if(this.props.contenido != undefined){
            console.log("entrando a form ",this.props.contenido)
            this.setState({
               idActualizar:this.props.contenido[0].value,
               proceso:this.props.contenido[1].value,
               
                
            })
        }

    }

    onChange = e => {
        console.log("entrando", e.target.name, e.target.value)
        this.setState({ [e.target.name]: e.target.value })
    }


    guardarElemento(){
        console.log(this.props)
        let json={
            "proceso":this.state.proceso,
            "user_id":parseInt(this.props.idUsuario),
            }
            if(this.props.contenido != undefined){
                this.props.guardaElementoNuevo(datos.urlServicePy+"parametros_upd/api_mail_proceso/"+this.state.idActualizar,json,"PUT","mostrarTablaParametrosParentesco")
            }else{
            this.props.guardaElementoNuevo(datos.urlServicePy+"parametros/api_mail_proceso/0",json,"POST","mostrarTablaParametrosParentesco")
            }
    }



    render() {
        return (
            <div>
                <div className="modal-body">
                    <div className="card-body">
                        <div className="row">
                            <div className="col-xs-12 col-lg-3">
                                <div className="form-group">

                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Proceso de notificación</span>
                                        </span> <input type="text" name="proceso" value={this.state.proceso} onChange={this.onChange} className="form-control bg-dark border-dark text-white" />
                                    </div>
                                </div>
                            </div>

                            



                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-warning" id="btnAgregar" data-dismiss="modal" disabled={this.state.proceso.length == 0  }  onClick={this.guardarElemento} >Agregar</button>
                </div>
            </div>
        )
    }


}