import React, { Component } from "react";
import datos from '../urls/datos.json'

export default class FormEmailDescripcion extends Component {

    constructor() {
        super();
        this.guardarElemento=this.guardarElemento.bind(this)
        this.onChange = this.onChange.bind(this)
        this.state = {
            mail_descripcion:'',
            mail_cuerpo:'',
            idActualizar:0
        }
    }


    UNSAFE_componentWillMount() {
        if(this.props.contenido != undefined){
            console.log("entrando a form ",this.props.contenido)
            this.setState({
               idActualizar:this.props.contenido[0].value,
               mail_descripcion:this.props.contenido[1].value,
               mail_cuerpo:this.props.contenido[2].value,
                
            })
        }

    }

    onChange = e => {
        console.log("entrando", e.target.name, e.target.value)
        this.setState({ [e.target.name]: e.target.value })
    }


    guardarElemento(){
        console.log(this.props)
        let json={
            "mail_descripcion":this.state.mail_descripcion,
            "mail_cuerpo":this.state.mail_cuerpo,
            "mail_cc":'r.mancilla.garcia@gmail.com',
            "user_id":parseInt(this.props.idUsuario),
            }
            if(this.props.contenido != undefined){
                this.props.guardaElementoNuevo(datos.urlServicePy+"parametros_upd/api_mail_descripcion_mail/"+this.state.idActualizar,json,"PUT","mostrarTablaParametrosFiguras")
            }else{
            this.props.guardaElementoNuevo(datos.urlServicePy+"parametros/api_mail_descripcion_mail/0",json,"POST","mostrarTablaParametrosFiguras")
            }
    }



    render() {
        return (
            <div>
                <div className="modal-body">
                    <div className="card-body">
                        <div className="row">
                            <div className="col-xs-12 col-lg-3">
                                <div className="form-group">

                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Descripción email</span>
                                        </span> <input type="text" name="mail_descripcion" value={this.state.mail_descripcion} onChange={this.onChange} className="form-control bg-dark border-dark text-white" />
                                    </div>
                                </div>
                            </div>

                            <div className="col-xs-12 col-lg-3">
                                <div className="form-group">

                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Contenido email</span>
                                        </span> <input type="text" name="mail_cuerpo" value={this.state.mail_cuerpo} onChange={this.onChange} className="form-control bg-dark border-dark text-white" />
                                    </div>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-warning" id="btnAgregar" data-dismiss="modal" disabled={this.state.mail_descripcion.length == 0  || this.state.mail_cuerpo.length == 0}  onClick={this.guardarElemento} >Agregar</button>
                </div>
            </div>
        )
    }


}