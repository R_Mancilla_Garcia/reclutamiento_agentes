import React, { Component } from "react";
import datos from '../urls/datos.json'

export default class FormCreaReferido extends Component {


    constructor() {
        super(); 
        this.onChange = this.onChange.bind(this)
        this.construyeSelect = this.construyeSelect.bind(this)
        this.cancelarCambios=this.cancelarCambios.bind(this)
        this.state = {
            selectEDAT: [],
            expresionEmail: /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
            validaEmail:false,
            referido: {
                "nombre": null,
                "ape_paterno": null,
                "ape_materno": null,
                "estatus_id": 23,
                "rfc": null,
                "fec_nacimiento": null,
                "tel_casa": null,
                "tel_movil": null,
                "tel_oficina": null,
                "estadocivil_id": null,
                "fec_reclutamiento": null,
                "empleado_id": null,
                "doc_cv": "",
                "fec_envio_psp": null,
                "correo_electronico": null,
                "fuente_recluta_id": null,
                "agente_referidor": null,
                "cua_referidor": null,
                "nombre_aps": null,
                "user_id": 23,
                "tipo_persona":"R"
            }
        }
    }

    onChange = e => {
        let campoBeneficiario = e.target.name
        let referido = this.state.referido
        
        if ("selectEDAT" == e.target.name) {
            referido.empleado_id = parseInt(e.target.value)
        }else if (e.target.name == "correo_electronico") {
            let correo_electronico = e.target.value;
            if (correo_electronico.match(this.state.expresionEmail)) {
                this.setState({ validaEmail: true })
            } else {
                this.setState({ validaEmail: false })
            }
            referido.correo_electronico = e.target.value

        }else if(e.target.name == "tel_movil"){
            let movil = e.target.value
            if (movil.length <= 10) {
                referido["" + e.target.name + ""] = e.target.value
            }
        }else{
            referido["" + campoBeneficiario + ""] = e.target.value
        }
        this.setState({ referido: referido })

    }

    cancelarCambios() {
        let referido = this.state.referido;
        console.log("Entro en cancelar")
        referido.nombre = "";
        referido.ape_materno = "";
        referido.ape_paterno = "";
        referido.correo_electronico = "";
        referido.tel_movil = "";
        referido.empleado_id = 0;
        this.setState({ ["selectEDAT"]: null })
        this.construyeSelect(datos.urlServicePy+"recluta/api_vcat_edat/0", "selectEDAT")
        this.setState ({referido : referido})
    }

    UNSAFE_componentWillMount() {
        console.log("props",this.props)
        this.construyeSelect(datos.urlServicePy+"recluta/api_vcat_edat/0", "selectEDAT")
    }

    construyeSelect(url, selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)
                console.log("Entro EDA");
                for (var i = 0; i < filas.length; i++) {

                    let fila = filas[i]
                    if (selector == "selectEDAT") {
                        if (this.props.idEmpleado) {

                            if (this.state.referido.empleado_id == fila.id) {
                                options.push(<option selected value={fila.id}>{fila.nombre}</option>)
                            } else {
                                options.push(<option value={fila.id}>{fila.nombre}</option>)
                            }
                        } else {
                            options.push(<option value={fila.id}>{fila.nombre}</option>)
                        }

                    }
                }
                let salida = []
                salida.push(<select onChange={this.onChange} name={selector} style={{ borderColor: '#E1E5F0' }} class="form-control bg-dark text-white border-dark" aria-label="Default select example">{options}</select>)
                this.setState({ [selector]: salida /*,selectParentesco2: salida,selectParentesco3: salida*/ })
            });
    }

    render() {
        return (
            <div>
                <div className="modal-body">
                    <div className="card-body">
                        <div className="row">
                            
                            {/*<div className="col-xs-12 col-lg-4">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Fecha alta</span>
                                        </span> <input type="date" disabled className="form-control bg-dark border-dark text-white" />
                                    </div>
                                </div>
                            </div>

                            <div className="col-xs-12 col-lg-4">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Usuaio creador</span>
                                        </span> <input type="text" disabled className="form-control bg-dark border-dark text-white" />
                                    </div>
                                </div>
                            </div>*/}

                            {/*<div className="col-xs-12 col-lg-2">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="text-white" style={{ backgroundColor: "#313A46" }}>Estatus del proceso de reclutamiento:</span>
                                        </span>
                                    </div>
                                </div>
                        </div>*/}


                            {/*<div className="col-xs-12 col-lg-1">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="text-white" style={{ backgroundColor: "#FFFFF", borderRadius: '10px' }}>En registro</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        */}
                        </div>

                        <div className="row">
                            <div className="col-xs-12 col-lg-8">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="text-white" style={{ backgroundColor: "#FFFFF", borderRadius: '10px' }}>Datos del referido</span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-12 col-lg-4">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Nombre (s)</span>
                                        </span> <input type="text" name="nombre" onChange={this.onChange} value={this.state.referido.nombre} className="form-control bg-dark border-dark text-white" />
                                    </div>
                                    {
                                        this.state.referido.nombre == null || this.state.referido.nombre == "" ?
                                            <span id="validaNombre" style={{ color: "red" }}>El nombre es un campo requerido</span> : ''
                                    }
                                </div>
                            </div>

                            <div className="col-xs-12 col-lg-4">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Apellido paterno</span>
                                        </span> <input type="text" name="ape_paterno" onChange={this.onChange}  value={this.state.referido.ape_paterno} className="form-control bg-dark border-dark text-white" />
                                    </div>
                                    {
                                        this.state.referido.ape_paterno == null || this.state.referido.ape_paterno == "" ?
                                            <span className=" " style={{ color: "red" }}>El apellido paterno es un campo requerido</span> : ''
                                    }
                                </div>
                            </div>

                            <div className="col-xs-12 col-lg-4">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Apellido materno</span>
                                        </span> <input type="text" name="ape_materno" onChange={this.onChange}  value={this.state.referido.ape_materno} className="form-control bg-dark border-dark text-white" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-xs-12 col-lg-4">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Correo electrónico</span>
                                        </span> <input type="text" name="correo_electronico" onChange={this.onChange} value={this.state.referido.correo_electronico} className="form-control bg-dark border-dark text-white" />
                                    </div>
                                    {
                                    this.state.validaEmail == false ?
                                        <span className="" style={{ color: "red" }}>El mail es un campo requerido</span> : ''
                                }
                                </div>
                            </div>

                            <div className="col-xs-12 col-lg-4">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Teléfono móvil</span>
                                        </span> <input type="text"  name="tel_movil" onChange={this.onChange} value={this.state.referido.tel_movil} className="form-control bg-dark border-dark text-white" />
                                    </div>
                                    {
                                    (this.state.referido.tel_movil+'').length <=9 ?
                                        <span className="" style={{ color: "red" }}>El teléfono móvil requerido</span> : ''
                                }
                                </div>
                            </div>

                            <div className="col-xs-12 col-lg-4">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>EDAT asignado</span>
                                        </span> {this.state.selectEDAT}
                                    </div>
                                    {
                                        this.state.referido.empleado_id == 0 || this.state.referido.empleado_id == null ?
                                            <span className="" style={{ color: "red" }}>El EDAT asignado es un campo requerido</span> : ''
                                    }
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-light" onClick={this.cancelarCambios} data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-warning" id="btnAgregar"data-dismiss="modal"
                        disabled={
                            this.state.referido.nombre == null ||
                            this.state.referido.nombre == '' ||
                            this.state.referido.ape_paterno == null ||
                            this.state.referido.ape_paterno == '' ||
                            this.state.referido.tel_movil == null ||
                            (this.state.referido.tel_movil+'').length == ''|| 
                            this.state.referido.empleado_id == null ||
                            this.state.referido.empleado_id == 0 ||
                            this.state.validaEmail == false ||
                            (this.state.referido.tel_movil+'').length <=9
                        }
                    
                    onClick={(e) => this.props.guardaElementoNuevo(this.state.referido)} >Agregar</button>
                </div>

            </div>
        )
    }
}