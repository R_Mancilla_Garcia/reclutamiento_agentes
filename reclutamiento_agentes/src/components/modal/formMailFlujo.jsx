import React, { Component } from "react";
import datos from '../urls/datos.json'

export default class FormMailFlujo extends Component {

    constructor() {
        super();
        this.guardarElemento = this.guardarElemento.bind(this)
        this.construyeSelect = this.construyeSelect.bind(this)
        this.onChange = this.onChange.bind(this)
        this.state = {
            idActualizar: 0,
            proceso_id: 0,
            puesto_grupo_origen_id: 0,
            puesto_grupo_destino_id: 0,
            mail_id: 0,
            intervalo: null,
            flujo_activo: 0,
            tipo_flujo_id: 0,
            selectAMP: [],
            selectAMGO: [],
            selectAMGD: [],
            selectAMDM: [],
            selectATF: []
        }
    }


    UNSAFE_componentWillMount() {
        this.construyeSelect(datos.urlServicePy+"parametros/api_mail_proceso/0", "selectAMP")
        this.construyeSelect(datos.urlServicePy+"parametros/api_mail_grupo_destino/0", "selectAMGO")
        this.construyeSelect(datos.urlServicePy+"parametros/api_mail_grupo_destino/0", "selectAMGD")
        this.construyeSelect(datos.urlServicePy+"parametros/api_mail_descripcion_mail/0", "selectAMDM")
        this.construyeSelect(datos.urlServicePy+"parametros/api_tipo_flujo/0", "selectATF")
        if (this.props.contenido != undefined) {
            console.log("entrando a form ", this.props.contenido)
            fetch(datos.urlServicePy+"parametros/api_mail_flujo/" + this.props.contenido[0].value)
                .then(response => response.json())
                .then(jsonData => {
                    this.setState({
                        idActualizar: jsonData.filas[0].fila[0].value,
                        proceso_id: jsonData.filas[0].fila[1].value,
                        puesto_grupo_origen_id: jsonData.filas[0].fila[2].value,
                        puesto_grupo_destino_id: jsonData.filas[0].fila[3].value,
                        mail_id: jsonData.filas[0].fila[4].value,
                        //flujo_activo: jsonData.filas[0].fila[5].value,
                        tipo_flujo_id: jsonData.filas[0].fila[5].value,
                        intervalo: jsonData.filas[0].fila[6].value,
                    })
                })
        }
    }

    construyeSelect(url, selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                for (var i = 0; i < filas.length; i++) {
                    let fila = filas[i]
                    if (selector == "selectAMP") {
                        if (this.state.proceso_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }
                    }
                    if (selector == "selectAMGO") {
                        if (this.state.puesto_grupo_origen_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[2].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[2].value}</option>)
                        }
                    }
                    if (selector == "selectAMGD") {
                        if (this.state.puesto_grupo_destino_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[2].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[2].value}</option>)
                        }
                    }
                    if (selector == "selectAMDM") {
                        if (this.state.mail_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }
                    }
                    if (selector == "selectATF") {
                        if (this.state.tipo_flujo_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }
                    }
                }
                let salida = []
                salida.push(<select onChange={this.onChange} name={selector} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ [selector]: salida })
            });
    }

    onChange = e => {
        console.log(e.target.name, e.target.value)
        if (e.target.name == "selectAMP") {
            this.setState({ proceso_id: parseInt(e.target.value) })
        } else if (e.target.name == "selectAMGO") {
            this.setState({ puesto_grupo_origen_id: parseInt(e.target.value) })
        } else if (e.target.name == "selectAMGD") {
            this.setState({ puesto_grupo_destino_id: parseInt(e.target.value) })
        } else if (e.target.name == "selectAMDM") {
            this.setState({ mail_id: parseInt(e.target.value) })
        } else if (e.target.name == "selectATF") {
            this.setState({ tipo_flujo_id: parseInt(e.target.value) })
        } else if (e.target.checked == true) {
            this.setState({ intervalo: parseInt(e.target.value) })
        } else {
            this.setState({ intervalo: 0 })
        }

    }


    guardarElemento() {
        console.log(this.props)
        let json = {
            "proceso": this.state.proceso_id,
            "puesto_grupo_origen_id": this.state.puesto_grupo_origen_id,
            "puesto_grupo_destino": this.state.puesto_grupo_destino_id,
            "mail": this.state.mail_id,
            "flujo_activo": 1,
            "tipo_flujo_id": 1,
            "intervalo": this.state.intervalo,
            "user_id": parseInt(this.props.idUsuario),
        }
        if (this.props.contenido != undefined) {
            this.props.guardaElementoNuevo(datos.urlServicePy+"parametros_upd/api_mail_flujo/" + this.state.idActualizar, json, "PUT", "mostrarTablaParametrosEmailFlujo")
        } else {
            this.props.guardaElementoNuevo(datos.urlServicePy+"parametros/api_mail_flujo/0", json, "POST", "mostrarTablaParametrosEmailFlujo")
        }
    }

    render() {
        return (
            <div>
                <div className="modal-body">
                    <div className="card-body">
                        <div className="row">
                            <div className="col-xs-12 col-lg-4">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Mail Proceso</span>
                                        </span> {this.state.selectAMP}
                                    </div>
                                </div>
                            </div>
                            <div className="col-xs-12 col-lg-4">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Grupo Origen</span>
                                        </span> {this.state.selectAMGO}
                                    </div>
                                </div>
                            </div>
                            <div className="col-xs-12 col-lg-4">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Grupo Destino</span>
                                        </span> {this.state.selectAMGD}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            {/*<div className="col-xs-12 col-lg-2">
                                <h8 class="mb-0 font-weight-semibold">Flujo Activo</h8>
                            </div>
                            <div className="col-xs-12 col-lg-2">
                                <div className="custom-control custom-switch  mb-2" >
                                    <input type="checkbox" class="custom-control-input" id="flujo_activo" name="flujo_activo" onChange={this.onChange} checked={this.state.flujo_activo}></input>
                                    <label class="custom-control-label" for="flujo_activo"></label>
                                </div>
                            </div>*/}
                            <div className="col-xs-12 col-lg-4">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Descripción Mail</span>
                                        </span> {this.state.selectAMDM}
                                    </div>
                                </div>
                            </div>
                            <div className="col-xs-12 col-lg-4">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Tipo de Flujo</span>
                                        </span> {this.state.selectATF}
                                    </div>
                                </div>
                            </div>
                            {/*<div className="col-xs-12 col-lg-2">
                                <h8 class="mb-0 font-weight-semibold">Intervalo</h8>
                            </div>
                            <div className="col-xs-12 col-lg-2">
                                <div className="custom-control custom-switch  mb-2" >
                                    <input type="checkbox" class="custom-control-input" id="intervalo" name="intervalo" onChange={this.onChange} checked={this.state.intervalo}></input>
                                    <label class="custom-control-label" for="intervalo"></label>
                                </div>
                            </div>*/}
                            <div className="col-xs-12 col-lg-2">
                                <div className="row">
                                    <div className="col-xs-3 col-lg-3">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Intervalo</span>
                                        </span>
                                    </div>
                                    <div className="col-xs-4 col-lg-4">
                                        <div className="row">
                                            {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="intervalo" name="intervalo" onChange={this.onChange} value={1} checked={this.state.intervalo == 1} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-warning" id="btnAgregar" data-dismiss="modal"
                        disabled={
                            (this.state.proceso_id == 0 || this.state.proceso_id == null) ||
                            (this.state.puesto_grupo_destino_id == 0 || this.state.puesto_grupo_destino_id == null) ||
                            (this.state.mail_id == 0 || this.state.mail_id == null) ||
                            (this.state.tipo_flujo_id == 0 || this.state.tipo_flujo_id == null)
                        } onClick={this.guardarElemento} >Agregar</button>
                </div>
            </div>
        )
    }


}