import React, { Component } from "react";
//const avatar = require("../imagenes/avatar.png")
export default class FormAdminEmpleadosAPS3 extends Component {

    constructor() {
        super();
        this.inyectaBeneficiarioSeguro = this.inyectaBeneficiarioSeguro.bind(this)
        this.inyectaBeneficiarioFondo = this.inyectaBeneficiarioFondo.bind(this)
        this.state = {
            contadorBeneficiariosSeguro: 1,
            contadorBeneficiariosFondo: 1,

        }

    }


    inyectaBeneficiarioSeguro() {
        console.log("inyeccion")
        let contBeneficiariosSeguro = this.state.contadorBeneficiariosSeguro + 1

        if (contBeneficiariosSeguro <= 3) {
            let id = "beneficiarioSeguro" + contBeneficiariosSeguro
            this.setState({ contadorBeneficiariosSeguro: contBeneficiariosSeguro })
        }
    }

    inyectaBeneficiarioFondo() {
        console.log("inyeccion")
        let contBeneficiariosSeguro = this.state.contadorBeneficiariosFondo + 1

        if (contBeneficiariosSeguro <= 3) {
            let id = "beneficiarioSeguro" + contBeneficiariosSeguro
            this.setState({ contadorBeneficiariosFondo: contBeneficiariosSeguro })
        }
    }

    render() {
        return (
            <div>
                <div class="card-header bg-transparent header-elements-sm-inline">
                    <h4 class="mb-0 font-weight-semibold"> Beneficiarios del seguro de vida </h4>

                </div>
                <br></br>
                <br></br>
                <div>
                    <div className="row" id="beneficiarioSeguro1">
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Nombre</span>
                                    </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />
                                </div>
                            </div>

                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }}>Fecha de nacimiento</span>
                                    </span> <input type="date" style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " />
                                </div>
                            </div>
                        </div>

                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Apellido paterno</span>
                                    </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />
                                </div>
                            </div>

                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }}>Parentesco</span>
                                    </span> <input type="date" style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " />
                                </div>
                            </div>
                        </div>


                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Materno</span>
                                    </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />
                                </div>
                            </div>

                            <div className="form-group">



                                <div className="row">
                                    <div className="col-xs-2 col-lg-2">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Especificar</span>
                                                </span>
                                            </div>
                                        </div>


                                    </div>

                                    <div className="col-xs-5 col-lg-5">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >%</span>
                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>

                    {
                        this.state.contadorBeneficiariosSeguro == 2 || this.state.contadorBeneficiariosSeguro == 3 ?

                            <div>
                                <hr></hr>
                                <div className="row" id="beneficiarioSeguro2">
                                    <div className="col-xs-4 col-lg-4">
                                        <div className="form-group">

                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Nombre</span>
                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />
                                            </div>
                                        </div>

                                        <div className="form-group">

                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }}>Fecha de nacimiento</span>
                                                </span> <input type="date" style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-xs-4 col-lg-4">
                                        <div className="form-group">

                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Apellido paterno</span>
                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />
                                            </div>
                                        </div>

                                        <div className="form-group">

                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }}>Parentesco</span>
                                                </span> <input type="date" style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " />
                                            </div>
                                        </div>
                                    </div>


                                    <div className="col-xs-4 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Materno</span>
                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />
                                            </div>
                                        </div>

                                        <div className="form-group">



                                            <div className="row">
                                                <div className="col-xs-2 col-lg-2">
                                                    <div className="form-group">
                                                        <div className="input-group">
                                                            <span className="input-group-prepend"> <span
                                                                className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Especificar</span>
                                                            </span>
                                                        </div>
                                                    </div>


                                                </div>

                                                <div className="col-xs-5 col-lg-5">
                                                    <div className="form-group">
                                                        <div className="input-group">
                                                            <span className="input-group-prepend"> <span
                                                                className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >%</span>
                                                            </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                            : ''
                    }

                    {
                        this.state.contadorBeneficiariosSeguro == 3 ?

                            <div>

                                <hr></hr>

                                <div className="row" id="beneficiarioSeguro3">
                                    <div className="col-xs-4 col-lg-4">
                                        <div className="form-group">

                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Nombre</span>
                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />
                                            </div>
                                        </div>

                                        <div className="form-group">

                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }}>Fecha de nacimiento</span>
                                                </span> <input type="date" style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-xs-4 col-lg-4">
                                        <div className="form-group">

                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Apellido paterno</span>
                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />
                                            </div>
                                        </div>

                                        <div className="form-group">

                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }}>Parentesco</span>
                                                </span> <input type="date" style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " />
                                            </div>
                                        </div>
                                    </div>


                                    <div className="col-xs-4 col-lg-4">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Materno</span>
                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />
                                            </div>
                                        </div>

                                        <div className="form-group">



                                            <div className="row">
                                                <div className="col-xs-2 col-lg-2">
                                                    <div className="form-group">
                                                        <div className="input-group">
                                                            <span className="input-group-prepend"> <span
                                                                className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Especificar</span>
                                                            </span>
                                                        </div>
                                                    </div>


                                                </div>

                                                <div className="col-xs-5 col-lg-5">
                                                    <div className="form-group">
                                                        <div className="input-group">
                                                            <span className="input-group-prepend"> <span
                                                                className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >%</span>
                                                            </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div> : ''}


                    <div className="row">
                        <div className="col-xs-4 col-lg-4">
                            <button type="button" class="btn text-white" onClick={this.inyectaBeneficiarioSeguro} style={{ backgroundColor: "#0F69B8" }}>+ Agregar beneficiario</button>
                        </div>

                    </div>

                </div>




                <hr></hr>
                <div>
                    <div className="row">
                        < div class="card-header">
                            <h5 class="mb-0">Beneficiarios del fondo de ahorro </h5>
                        </div>
                    </div>



                    <div className="row">
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre (d)</span>
                                    </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Apellido paterno</span>
                                    </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Apellido materno</span>
                                    </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />
                                </div>
                            </div>
                        </div>

                    </div>
                    {
                        this.state.contadorBeneficiariosFondo == 2 || this.state.contadorBeneficiariosFondo == 3 ?
                            <div>
                                <hr></hr>
                                <div className="row">
                                    <div className="col-xs-4 col-lg-4">
                                        <div className="form-group">

                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre (d)</span>
                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xs-4 col-lg-4">
                                        <div className="form-group">

                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Apellido paterno</span>
                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xs-4 col-lg-4">
                                        <div className="form-group">

                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Apellido materno</span>
                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div> : ''
                    }

                    {
                        this.state.contadorBeneficiariosFondo == 3 ?
                            <div>
                                <hr></hr>
                                <div className="row">
                                    <div className="col-xs-4 col-lg-4">
                                        <div className="form-group">

                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre (d)</span>
                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xs-4 col-lg-4">
                                        <div className="form-group">

                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Apellido paterno</span>
                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xs-4 col-lg-4">
                                        <div className="form-group">

                                            <div className="input-group">
                                                <span className="input-group-prepend"> <span
                                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Apellido materno</span>
                                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div> : ''
                    }


                </div>







                <div className="row">
                    <div className="col-xs-4 col-lg-4">
                        <button type="button" onClick={this.inyectaBeneficiarioFondo} class="btn text-white" style={{ backgroundColor: "#0F69B8" }}>+ Agregar beneficiario</button>
                    </div>

                </div>

            </div>

        )
    }

}