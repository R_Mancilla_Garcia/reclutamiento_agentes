import React, { Component } from "react";
import datos from '../urls/datos.json'

const avatar = require("../imagenes/avatar.png")
export default class FormAdminEmpleadosAPS extends Component {


    constructor() {
        super();
        this.onChange = this.onChange.bind(this)
        this.construyeSelect = this.construyeSelect.bind(this)
        this.construyeSelectDependienteDeCiudad = this.construyeSelectDependienteDeCiudad.bind(this)
        this.construyeSelectDependienteDeMunicipio = this.construyeSelectDependienteDeMunicipio.bind(this)
        this.determinaEstadoCivil = this.determinaEstadoCivil.bind(this)
        this.onClickImagen = this.onClickImagen.bind(this)
        this.onChangeEstadosConyugales = this.onChangeEstadosConyugales.bind(this)
        this.desactivaCheck = this.desactivaCheck.bind(this)
        this.state = {
            contenidoEdicion: undefined,
            selectCiudad: undefined,
            selectMunicipio: undefined,
            selectColonia: undefined,
            selectParentesco: undefined,
            fecha: '1980-2-2',
            estadosConyugales: [false, false, false, false, false],
            imagenEnCambio: undefined,
            soltero: false
        }

    }

    UNSAFE_componentWillMount() {
        console.log("component prmier form", this.props)


        this.construyeSelect(datos.urlServicePy+"parametros/api_cat_ciudad/0", "selectCiudad")
        this.construyeSelect(datos.urlServicePy+"parametros/api_cat_parentesco/0", "selectParentesco")
        this.construyeSelectDependienteDeCiudad(datos.urlServicePy+"parametros/api_cat_municipio/0", "selectMunicipio")
        this.construyeSelectDependienteDeMunicipio(datos.urlServicePy+"parametros/api_cat_colonia/0", "selectColonia")


        if (this.props.esEdicion) {
            this.determinaEstadoCivil()
            this.construyeSelectDependienteDeMunicipio(datos.urlServicePy+"parametros/api_cat_colonia/0", "selectColonia")

        }
        this.setState({ contenidoEdicion: this.props.contenidoEdicion })



    }


    determinaEstadoCivil() {
        let estados = this.state.estadosConyugales
        estados[this.props.contenidoEdicion.estadocivil_id - 1] = true
        console.log(estados)
        this.setState({ estadosConyugales: estados })
    }

    onChangeEstadosConyugales = e => {
        console.log(e.target.name, e.target.checked)
    }


    construyeSelect(url, selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                console.log("respuesta de las ciudades ", json)
                for (var i = 0; i < filas.length; i++) {

                    let fila = filas[i]
                    if (selector == "selectCiudad") {
                        if (this.state.contenidoEdicion.ciudad_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }
                    } else if (selector == "selectParentesco") {
                        if (this.state.contenidoEdicion.parentesco_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }
                    }



                }
                let salida = []
                salida.push(<select onChange={this.onChange} name={selector} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ [selector]: salida })
            });
    }

    construyeSelectDependienteDeCiudad(url, selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                for (var i = 0; i < filas.length; i++) {

                    let fila = filas[i]
                    if (this.props.esEdicion) {
                        if (this.state.contenidoEdicion.ciudad_id == fila.fila[1].value) {
                            if (this.state.contenidoEdicion.municipio_id == fila.fila[0].value) {
                                options.push(<option selected value={fila.fila[0].value}>{fila.fila[2].value}</option>)

                            } else {
                                options.push(<option value={fila.fila[0].value}>{fila.fila[2].value}</option>)
                            }
                        }
                    }

                    if (!this.props.esEdicion) {
                        if (1 == fila.fila[1].value) {
                            if (1 == fila.fila[0].value) {
                                options.push(<option value={fila.fila[0].value}>{fila.fila[2].value}</option>)

                            } else {
                                options.push(<option value={fila.fila[0].value}>{fila.fila[2].value}</option>)
                            }
                        }
                    }


                }
                let salida = []
                salida.push(<select onChange={this.onChange} name={selector} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ [selector]: salida })
            });
    }

    construyeSelectDependienteDeMunicipio(url, selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                // options.push(<option selected value={0}>Seleccionar</option>)

                for (var i = 0; i < filas.length; i++) {

                    let fila = filas[i]
                    //if(this.props.esEdicion){
                    if (this.state.contenidoEdicion.municipio_id == fila.fila[1].value) {
                        if (this.state.contenidoEdicion.colonia_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[2].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[2].value}</option>)
                        }
                    }
                    //}

                    /*if(!this.props.esEdicion){
                        if (1 == fila.fila[1].value) {
                            if (1 == fila.fila[0].value) {
                                options.push(<option selected value={fila.fila[0].value}>{fila.fila[2].value}</option>)
    
                            } else {
                                options.push(<option value={fila.fila[0].value}>{fila.fila[2].value}</option>)
                            }
                        }
                    }*/

                }
                let salida = []
                salida.push(<select style={{ borderColor: '#E1E5F0' }} name={selector} onChange={this.onChange} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ [selector]: salida })
            });
    }


    onChange = e => {
        console.log("entrando", e.target.name, e.target.value)

        let contenidoEdicion = this.state.contenidoEdicion

        if ("selectMunicipio" == e.target.name) {
            console.log("entroa asleect")
            let contenido = this.state.contenidoEdicion
            contenido.municipio_id = parseInt(e.target.value)
            this.setState({ contenidoEdicion: contenido })
            this.construyeSelectDependienteDeMunicipio(datos.urlServicePy+"parametros/api_cat_colonia/0", "selectColonia")

        } else
            if ("selectCiudad" == e.target.name) {
                console.log("entro")
                let contenido = this.state.contenidoEdicion
                contenido.ciudad_id = parseInt(e.target.value)
                this.setState({ contenidoEdicion: contenido })
                this.construyeSelectDependienteDeCiudad(datos.urlServicePy+"parametros/api_cat_municipio/0", "selectMunicipio")

            } else
                if ("selectColonia" == e.target.name) {
                    console.log("entro")
                    let contenido = this.state.contenidoEdicion
                    contenido.colonia_id = parseInt(e.target.value)
                    this.setState({ contenidoEdicion: contenido })
                    // this.construyeSelectDependienteDeCiudad(datos.urlServicePy+"parametros/api_cat_municipio/0", "selectMunicipio")

                } else
                    if ("selectParentesco" == e.target.name) {
                        console.log("entro")
                        let contenido = this.state.contenidoEdicion
                        contenido.parentesco_id = parseInt(e.target.value)
                        this.setState({ contenidoEdicion: contenido })
                        //  this.construyeSelectDependienteDeCiudad(datos.urlServicePy+"parametros/api_cat_parentesco/0", "selectParentesco")

                    } else
                        if (e.target.name == "soltero") {
                            let estados = this.state.estadosConyugales;
                            estados[0] = !estados[0]
                            this.desactivaCheck(0, estados)
                            this.setState({ estadosConyugales: estados })
                            estados[0] == false ? contenidoEdicion.estadocivil_id = 0 : contenidoEdicion.estadocivil_id = 1
                        } else
                            if (e.target.name == "casado") {
                                let estados = this.state.estadosConyugales;
                                estados[1] = !estados[1]
                                this.desactivaCheck(1, estados)
                                this.setState({ estadosConyugales: estados })
                                estados[1] == false ? contenidoEdicion.estadocivil_id = 0 : contenidoEdicion.estadocivil_id = 2
                                // contenidoEdicion.estadocivil_id = 2
                            } else
                                if (e.target.name == "divorciado") {
                                    let estados = this.state.estadosConyugales;
                                    estados[2] = !estados[2]
                                    this.desactivaCheck(2, estados)
                                    this.setState({ estadosConyugales: estados })
                                    estados[2] == false ? contenidoEdicion.estadocivil_id = 0 : contenidoEdicion.estadocivil_id = 3
                                    //contenidoEdicion.estadocivil_id = 3
                                } else
                                    if (e.target.name == "ulibre") {
                                        let estados = this.state.estadosConyugales;
                                        estados[3] = !estados[3]
                                        this.desactivaCheck(3, estados)
                                        this.setState({ estadosConyugales: estados })
                                        estados[3] == false ? contenidoEdicion.estadocivil_id = 0 : contenidoEdicion.estadocivil_id = 4
                                        //contenidoEdicion.estadocivil_id = 4
                                    } else
                                        if (e.target.name == "viudo") {
                                            let estados = this.state.estadosConyugales;
                                            estados[4] = !estados[4]
                                            this.desactivaCheck(4, estados)
                                            this.setState({ estadosConyugales: estados })
                                            estados[4] == false ? contenidoEdicion.estadocivil_id = 0 : contenidoEdicion.estadocivil_id = 5
                                            // contenidoEdicion.estadocivil_id = 5
                                        }
                                        else {
                                            contenidoEdicion["" + e.target.name + ""] = e.target.value
                                        }
        this.setState({ contenidoEdicion: contenidoEdicion })
        this.props.guardaFormulario(contenidoEdicion)
    }


    desactivaCheck(index, estados) {
        for (var i = 0; i < estados.length; i++) {
            if (index != i) {
                estados[i] = false
            }
        }

    }


    onClickImagen() {
        console.log("entrnaod a la imagen")
        this.upload.click()
    }

    onChangeFile(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file", file);
        this.setState({ imagenEnCambio: URL.createObjectURL(file) })
        this.props.guardaFormulario(this.state.contenidoEdicion)
        this.props.guardaFotografia(file)
        //this.setState({file}); /// if you want to upload latter
    }


    render() {
        return (
            <div>
                <div class="card-header bg-transparent header-elements-sm-inline">
                    <h2 class="mb-0 font-weight-semibold">1. Información general de empleado </h2>
                    <div class="header-elements">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item font-size-sm">Fecha de alta: <strong >dd/mm/aaaa</strong></li>

                            <li class="list-inline-item font-size-sm">Usuario creador: <strong >EMANCILL</strong></li>
                        </ul>
                    </div>
                </div>
                <br></br>
                <br></br>


                <div className="row">

                    <div className="col-xs-1 col-lg-1">
                        <div class="card-img-actions">
                            {
                                this.props.esEdicion ?

                                    this.state.contenidoEdicion.doc_foto.length == 0 ? <div> <input type="image" onClick={this.onClickImagen} src={this.state.imagenEnCambio == undefined ? avatar : this.state.imagenEnCambio} class="img-fluid rounded-circle" width="170" height="270" alt="" />
                                        <form encType="multipart/form">
                                            <input type="file" style={{ display: 'none' }} ref={(ref) => this.upload = ref} onChange={this.onChangeFile.bind(this)}></input>
                                        </form>
                                    </div> :
                                        <img src={this.state.contenidoEdicion.doc_foto} class="img-fluid rounded-circle" width="170" height="270" alt="" />


                                    :
                                    <div> <input type="image" onClick={this.onClickImagen} src={this.state.imagenEnCambio == undefined ? avatar : this.state.imagenEnCambio} class="img-fluid rounded-circle" width="170" height="270" alt="" />
                                        <form encType="multipart/form">
                                            <input type="file" style={{ display: 'none' }} ref={(ref) => this.upload = ref} onChange={this.onChangeFile.bind(this)}></input>
                                        </form>
                                    </div>

                            }


                        </div>
                    </div>


                    <div className="col-xs-3 col-lg-4 " style={{ marginTop: '20px' }}>
                        <div className="form-group">

                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }}>Nombre (s)</span>
                                </span>
                                <input type="text" name="nombre" maxlength="10" value={this.state.contenidoEdicion.nombre} onChange={this.onChange} style={{ borderColor: '#E1E5F0' }} className="form-control" />
                            </div>

                            {
                                this.state.contenidoEdicion.nombre == "" ?
                                    <span id="validaNombre" style={{ color: "red" }}>El nombre es un campo requerido</span> : ''
                            }

                        </div>

                        <div className="form-group">

                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >RFC</span>
                                </span> <input type="text" name="rfc" value={this.state.contenidoEdicion.rfc} onChange={this.onChange} style={{ borderColor: '#E1E5F0' }} className="form-control " />
                            </div>
                            {
                                this.state.contenidoEdicion.rfc == "" ?
                                    <span className="" style={{ color: "red" }}>El rfc es un campo requerido</span> : ''
                            }

                        </div>
                    </div>

                    <div className="col-xs-3 col-lg-3" style={{ marginTop: '20px' }}>
                        <div className="form-group">

                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Apellido paterno</span>
                                </span> <input type="text" name="ape_paterno" value={this.state.contenidoEdicion.ape_paterno} onChange={this.onChange} style={{ borderColor: '#E1E5F0' }} className="form-control " />
                            </div>
                            {
                                this.state.contenidoEdicion.ape_paterno == "" ?
                                    <span className=" " style={{ color: "red" }}>El apellido paterno es un campo requerido</span> : ''
                            }
                        </div>

                        <div className="form-group">

                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }}>CURP</span>
                                </span> <input type="text" name="curp" value={this.state.contenidoEdicion.curp} onChange={this.onChange} style={{ borderColor: '#E1E5F0' }} className="form-control " />
                            </div>
                            {
                                this.state.contenidoEdicion.curp == "" ?
                                    <span className="" style={{ color: "red" }}>El curp es un campo requerido</span> : ''
                            }
                        </div>
                    </div>


                    <div className="col-xs-3 col-lg-4" style={{ marginTop: '20px' }}>
                        <div className="form-group">

                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Apellido materno</span>
                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} name="ape_materno" onChange={this.onChange} value={this.state.contenidoEdicion.ape_materno} className="form-control " />
                            </div>
                        </div>

                        <div className="form-group">

                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }}>Fecha de nacimiento</span>
                                </span> <input type="date" style={{ borderColor: '#E1E5F0', opacity: 1 }} name="fec_nacimiento" onChange={this.onChange} value={"" + this.state.contenidoEdicion.fec_nacimiento + ""} className="form-control " />
                            </div>
                            {
                                this.state.contenidoEdicion.fec_nacimiento == "" ?
                                    <span className=" " style={{ color: "red" }}>La fecha de nacimiento es un campo requerido</span> : ''
                            }
                        </div>
                    </div>





                </div>
                <hr></hr>



                <div>
                    <div className="row">
                        < div class="card-header">
                            <h5 class="mb-0">Domicilio e información de contacto </h5>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Codigo postal</span>
                                    </span> <input type="number" style={{ borderColor: '#E1E5F0' }} name="codigo_postal" onChange={this.onChange} value={this.state.contenidoEdicion.codigo_postal} className="form-control " />
                                </div>
                                {
                                    this.state.contenidoEdicion.codigo_postal == 0 ?
                                        <span className="" style={{ color: "red" }}>El codigo postal es un campo requerido</span> : ''
                                }
                            </div>

                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }}>Colonia</span>
                                    </span>
                                    {this.state.selectColonia}
                                </div>

                            </div>
                        </div>

                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Ciudad</span>
                                    </span>
                                    {this.state.selectCiudad}
                                </div>
                                {
                                    this.state.contenidoEdicion.ciudad_id == 0 ?
                                        <span className=" " style={{ color: "red" }}>El curp es un campo requerido</span> : ''
                                }
                            </div>

                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }}>Calle</span>
                                    </span> <input type="text" style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " onChange={this.onChange} name="calle" value={this.state.contenidoEdicion.calle} />
                                </div>
                                {
                                    this.state.contenidoEdicion.calle == '' ?
                                        <span className="" style={{ color: "red" }}>El calle es un campo requerido</span> : ''
                                }
                            </div>

                        </div>


                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Alcaldia/Municipio</span>
                                    </span>
                                    {this.state.selectMunicipio}
                                </div>
                                {
                                    this.state.contenidoEdicion.municipio_id == 0 ?
                                        <span className=" " style={{ color: "red" }}>La alcaldia o municipio es un campo requerido</span> : ''
                                }
                            </div>

                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }}>No. ext.</span>
                                    </span>
                                    <input type="number" style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " onChange={this.onChange} name="num_ext" value={this.state.contenidoEdicion.num_ext} />


                                    <span className="input-group-prepend"> <span
                                        className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }}>No. int.</span>
                                    </span> <input type="number" style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " onChange={this.onChange} name="num_int" value={this.state.contenidoEdicion.num_int} />
                                </div>
                                {
                                    this.state.contenidoEdicion.num_ext == 0 ?
                                        <span className=" " style={{ color: "red" }}>El número exterior es un campo requerido</span> : ''
                                }
                                {
                                    this.state.contenidoEdicion.num_int == 0 ?
                                        <span className="" style={{ color: "red" }}>El número interior es un campo requerido</span> : ''
                                }

                            </div>
                        </div>



                    </div>

                    <div className="row">
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Teléfono de casa</span>
                                    </span> <input type="number" style={{ borderColor: '#E1E5F0' }} className="form-control " onChange={this.onChange} name="tel_casa" value={this.state.contenidoEdicion.tel_casa} />
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Teléfono móvil</span>
                                    </span> <input type="number" style={{ borderColor: '#E1E5F0' }} className="form-control " onChange={this.onChange} name="tel_movil" value={this.state.contenidoEdicion.tel_movil} />
                                </div>
                                {
                                    this.state.contenidoEdicion.tel_movil == 0 ?
                                        <span className="" style={{ color: "red" }}>El telefono movíl es un campo requerido</span> : ''
                                }
                            </div>
                        </div>

                    </div>

                </div>


                <hr></hr>

                <div>
                    <div className="row">
                        < div class="card-header">
                            <h5 class="mb-0">Información de situación conyugal </h5>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-xs-6 col-lg-6">
                        <div className="form-group">
                            <span>Estado Civil:</span>
                            {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                            <input class="form-check-input" type="checkbox" name="soltero" onChange={this.onChange} id="soltero" checked={this.state.estadosConyugales[0]} />
                            <label class="form-check-label" for="soltero">
                                Soltero (a)
                            </label>
                            {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                            <input class="form-check-input" type="checkbox" name="casado" id="casado" onChange={this.onChange} checked={this.state.estadosConyugales[1]} />
                            <label class="form-check-label" for="casado">
                                Casado (a)
                            </label>
                            {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                            <input class="form-check-input" type="checkbox" name="divorciado" id="divorciado" onChange={this.onChange} checked={this.state.estadosConyugales[2]} />
                            <label class="form-check-label" for="divorciado">
                                Divorciado (a)
                            </label>
                            {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                            <input class="form-check-input" type="checkbox" name="ulibre" id="ulibre" onChange={this.onChange} checked={this.state.estadosConyugales[3]} />
                            <label class="form-check-label" for="ulibre">
                                Union libre (a)
                            </label>
                            {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                            <input class="form-check-input" type="checkbox" name="viudo" id="viudo" onChange={this.onChange} checked={this.state.estadosConyugales[4]} />
                            <label class="form-check-label" for="viudo">
                                Viudo (a)
                            </label>
                        </div>
                        {
                            this.state.estadosConyugales[0] == false && this.state.estadosConyugales[1] == false && this.state.estadosConyugales[2] == false && this.state.estadosConyugales[3] == false && this.state.estadosConyugales[4] == false ?
                                <span style={{ color: "red" }}>Algún estado conyugal debe ser seleccionado</span> : ''
                        }
                    </div>
                </div>
                <br></br>



                < div className="row">
                    <div className="col-xs-6 col-lg-6">
                        <div className="form-group">

                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre completo del cónyugue</span>
                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " onChange={this.onChange} name="nombre_conyugue" value={this.state.contenidoEdicion.nombre_conyugue} />
                            </div>
                        </div>

                    </div>

                    <div className="col-xs-3 col-lg-3">
                        < div className="form-group">

                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Fecha de nacimiento</span>
                                </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " name="fec_nacimiento_conyugue" onChange={this.onChange} value={"" + this.state.contenidoEdicion.fec_nacimiento_conyugue + ""} />
                            </div>
                        </div>

                    </div>

                    <div className="col-xs-3 col-lg-3">
                        < div className="form-group">

                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Fecha de aniversario</span>
                                </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " name="fec_aniversario" onChange={this.onChange} value={"" + this.state.contenidoEdicion.fec_aniversario + ""} />
                            </div>
                        </div>

                    </div>

                </div>


                < div className="row">
                    <div className="col-xs-8 col-lg-8">
                        <div className="form-group">

                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre del hijo (a)</span>
                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />
                            </div>
                        </div>
                    </div>

                    <div className="col-xs-3 col-lg-3">
                        < div className="form-group">

                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Fecha de nacimiento</span>
                                </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " />
                            </div>
                        </div>

                    </div>

                    <div className="col-xs-1 col-lg-1">
                        <button type="button" class="btn text-white" style={{ backgroundColor: "#0F69B8" }}>+</button>
                    </div>

                </div>


                <hr></hr>
                <div>
                    <div className="row">
                        < div class="card-header">
                            <h5 class="mb-0">Contacto en caso de emergencia </h5>
                        </div>
                    </div>

                    < div className="row">
                        <div className="col-xs-6 col-lg-6">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Nombre completo</span>
                                    </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " onChange={this.onChange} name="nombre_contacto" value={this.state.contenidoEdicion.nombre_contacto} />
                                </div>
                                {
                                    this.state.contenidoEdicion.nombre_contacto == "" ?
                                        <span className="" style={{ color: "red" }}>El nombre del contacto es un campo requerido</span> : ''
                                }
                            </div>



                        </div>
                        <div className="col-xs-3 col-lg-3">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Parentesco</span>
                                    </span>
                                    {this.state.selectParentesco}
                                </div>
                                {
                                    this.state.contenidoEdicion.parentesco_id == 0 ?
                                        <span className=" " style={{ color: "red" }}>El parentesco es un campo requerido</span> : ''
                                }
                            </div>
                        </div>

                        <div className="col-xs-3 col-lg-3">
                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Telefono</span>
                                    </span> <input type="number" style={{ borderColor: '#E1E5F0' }} className="form-control " onChange={this.onChange} name="tel_contacto" value={this.state.contenidoEdicion.tel_contacto} />
                                </div>
                                {
                                    this.state.contenidoEdicion.tel_contacto == 0 ?
                                        <span className=" " style={{ color: "red" }}>El telefono del contacto es un campo requerido</span> : ''
                                }
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        )
    }

}