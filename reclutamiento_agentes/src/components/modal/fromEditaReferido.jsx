import React, { Component } from "react";
import datos from '../urls/datos.json'

export default class FormEditaReferido extends Component {



    constructor() {
        super();
        this.onChange = this.onChange.bind(this)
        this.construyeSelect = this.construyeSelect.bind(this)
        this.guardarCambios = this.guardarCambios.bind(this)
        this.descartar=this.descartar.bind(this)
        this.obtenEstatus= this.obtenEstatus.bind(this)
        this.enviaNotificacion=this.enviaNotificacion.bind(this)

        this.state = {
            estatus:'',
            selectEDAT: [],
            referido: {},
            clickDescartar: '1',
            banDescartar: false,
            banBloqueaDescartar: false
        }
    }

    descartar() {
        this.state.referido.estatus_id = 2
        this.state.referido.user_id=this.props.idUsuario
        console.log("Entro aquí"+this.state.estatus);
        if(this.state.clickDescartar ==  '3'){
            const requestOptions = {
                method: "PUT",
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(this.state.referido)
            };

            fetch(datos.urlServicePy+"parametros_upd/api_recluta_referidos/" + this.state.referido.id, requestOptions)
                .then(response => response.json())
                .then(data => { this.props.botonCancelar() })
        }  else {
            this.setState({ clickDescartar: '2' })
            this.setState({ banDescartar: true })
        }
    }

    obtenEstatus(){
        fetch(datos.urlServicePy+"parametros/api_recluta_vreferidos/" + this.props.idEmpleado)
            .then(response => response.json())
            .then(json => {
                    console.log(json , "el estatus ")
                    this.setState({estatus:json.filas[0].fila[4].value})
            })
    }

    UNSAFE_componentWillMount() {
        fetch(datos.urlServicePy+"parametros/api_recluta_referidos/" + this.props.idEmpleado)
            .then(response => response.json())
            .then(json => {

                this.obtenEstatus()
                console.log("Estatus de registro");
                let fila = json.filas[0].fila
                let columnas = json.columnas
                let columnasSalida = {}
                for (var i = 0; i < fila.length; i++) {
                   // console.log(fila[i])
                    columnasSalida["" + columnas[i].key + ""] = fila[i].value
                }
                if(columnasSalida.estatus_id == 2){
                    this.setState({ banDescartar: true }) 
                    if(columnasSalida.estatus_id == 2){
                        this.setState({ banBloqueaDescartar: true }) 
                    }
                }
                this.setState({ referido: columnasSalida })
                this.construyeSelect(datos.urlServicePy+"recluta/api_vcat_edat/0", "selectEDAT")
            })
    }


    guardarCambios() {
        console.log(this.state.referido)
        this.state.referido.user_id=this.props.idUsuario
        this.state.referido.tipo_persona="R"
        const requestOptions = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(this.state.referido)
        };

        fetch(datos.urlServicePy+"parametros_upd/api_recluta_referidos/" + this.state.referido.id, requestOptions)
            .then(response => response.json())
            .then(data => { 
                this.enviaNotificacion(data.id)
                this.props.botonCancelar() 
            })
       
    }

    enviaNotificacion(idReferido){
        let jsonNotificacion={
            "mail_id": 1,    
            "prospecto_id": idReferido    
        }
        const requestOptions = {
            method: "POST",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(jsonNotificacion)
        };
        
        fetch(datos.urlServicePy+"recluta/api_notificador/0" , requestOptions)
            .then(response => response.json())
            .then(data => { })
    }

    onChange = e => {
        let campoBeneficiario = e.target.name
        let referido = this.state.referido
        referido["" + campoBeneficiario + ""] = e.target.value

        console.log("Entro onChange");

        if ("selectEDAT" == e.target.name) {
            referido.empleado_id = parseInt(e.target.value)
        }
        if ("comentarios_descarte" == e.target.name) {
            if(e.target.value.length == 0){
                //this.setState({ clickDescartar: '3' })
                //this.state.referido.estatus_id = 1
                //this.state.referido.obs_descarte = e.target.value
                this.setState({ clickDescartar: '2' })
                this.state.referido.estatus_id = 2
                this.state.referido.obs_descarte = e.target.value
            }else{
                this.setState({ clickDescartar: '3' })
                this.state.referido.estatus_id = 1
                this.state.referido.obs_descarte = e.target.value
              //  this.setState({ clickDescartar: '2' })
              //  this.state.referido.estatus_id = 2
            }
        }
        this.setState({ referido: referido })

    }
    construyeSelect(url, selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                for (var i = 0; i < filas.length; i++) {

                    let fila = filas[i]
                    if (selector == "selectEDAT") {
                        if (this.props.idEmpleado) {

                            if (this.state.referido.empleado_id == fila.id) {
                                options.push(<option selected value={fila.id}>{fila.nombre}</option>)
                            } else {
                                options.push(<option value={fila.id}>{fila.nombre}</option>)
                            }
                        } else {
                            options.push(<option value={fila.id}>{fila.nombre}</option>)
                        }

                    }
                }
                let salida = []
                salida.push(<select onChange={this.onChange} name={selector} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ [selector]: salida /*,selectParentesco2: salida,selectParentesco3: salida*/ })
            });
    }




    render() {
        return (
            <div>
                <div class="card">
                    <div class="row mb-2 mt-2 ml-1">

                        <div class="col-12 col-lg-12">
                        <div class="float-left mr-3">
                                <h6 className="font-weight-bold">{'Proceso de reclutamiento'}</h6>
                        </div>
                            <div class="float-right mr-3">
                                <button type="button" class="btn btn-light mr-3 text-white" style={{ backgroundColor: '#617187', borderColor: '#617187' }} onClick={(e) => this.props.botonCancelar()} >Cancelar</button>
                                <button type="button" disabled={ this.state.referido.estatus_id == 2} class="btn btn-warning mr-3" style={{ backgroundColor: '#8F9EB3', borderColor: '#8F9EB3' }}  onClick={this.descartar} > Descartar</button>
                                <button type="button" disabled={ this.state.referido.estatus_id == 2} class="btn btn-light mr-3 text-white" style={{ backgroundColor: '#C28EE3', borderColor: '#C28EE3' }} data-toggle="modal" data-target="#modal-Confirmacion">Completar registro</button>
                                <button type="button" class="btn btn-warning" style={{ backgroundColor: '#DDE0EB', borderColor: '#DDE0EB', color: '#8F9EB3' }} onClick={this.guardarCambios} disabled={
                                    this.state.referido.nombre == null ||
                                    this.state.referido.nombre == ''  ||
                                    this.state.referido.ape_paterno == null ||
                                    this.state.referido.ape_paterno == '' ||
                                    this.state.referido.estatus_id == 2
                                } >Guarda cambios</button>
                            </div>

                        </div>
                    </div>
                </div>
                <br></br>
                <br></br>
                <div id="modal-Confirmacion" className="modal fade" tabindex="-1">
                    <div className="modal-dialog ">
                        <div className="modal-content text-white" style={{ backgroundColor: "#FFFFF" }}>
                            <div className="modal-header text-white text-center" style={{ backgroundColor: "#0F69B8" }}>
                                <h6 className="modal-title col-11 text-center">Registrar como prospecto</h6>

                            </div>
                            <div className="modal-body">
                                <div className="card-body">
                                    <h8 style={{ color: '#0F69B8' }}>¿Desea registrar el referido como nuevo prospecto para iniciar el proceso de reclutamiento?</h8>
                                </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-center">
                                <div className="col-12">
                                    <div className="row">
                                        <button type="button" style={{ width: '100%', backgroundColor: "#0F69B8" }} class="btn text-white" onClick={(e) => this.props.completarRegistro()} data-dismiss="modal"  >Sí, continuar registro</button>
                                    </div>
                                    <br></br>
                                    <div className="row">
                                        <button type="button" style={{ width: '100%', backgroundColor: '#8F9EB3' }} class="btn text-white" id="btnAgregar" data-dismiss="modal"  >Cancelar</button>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>


                <div>

                    <div className="row">
                        <div className="col-xs-12 col-lg-2">
                            <div className="form-group">
                                <div className="input-group">
                                    <h4 class="card-title py-3 font-weight-bold" >Referido {this.props.idEmpleado}</h4>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row">

                        <div className="col-xs-12 col-lg-4">
                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fecha alta</span>
                                    </span> <input type="text" disabled value={this.state.referido.ts_alta_audit} className="form-control " />
                                </div>
                            </div>
                        </div>

                        <div className="col-xs-12 col-lg-4">
                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Usuario creador</span>
                                    </span> <input type="text" disabled value={this.state.referido.user_id} className="form-control " />
                                </div>
                            </div>
                        </div>

                        <div className="col-xs-12 col-lg-2">
                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-prepend"> <h6
                                        className="" style={{ color: '#617187' }} >Estatus del proceso de reclutamiento</h6>
                                    </span>
                                </div>
                            </div>
                        </div>


                        <div className="col-xs-12 col-lg-2">
                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-prepend"> <h6
                                        style={{ backgroundColor: "#FFFF", color: '#C28EE3', borderRadius: '5px' }}> <>&nbsp;&nbsp;</> {this.state.estatus}  <>&nbsp;&nbsp;</> </h6>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <br></br>
                <br></br>


                <div className="row">
                    <div className="col-xs-12 col-lg-8">
                        <div className="form-group">
                            <div className="input-group">
                                <h5 class="card-title py-1 font-weight-bold" >Datos del referido</h5>

                            </div>
                        </div>
                    </div>
                </div>


                <div className="row">

                    <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Nombre (s)</span>
                                </span> <input type="text" className="form-control " name="nombre" onChange={this.onChange} value={this.state.referido.nombre} />
                            </div>
                        </div>
                    </div>

                    <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Apellido paterno</span>
                                </span> <input type="text" className="form-control " name="ape_paterno" onChange={this.onChange} value={this.state.referido.ape_paterno} />
                            </div>
                        </div>
                    </div>

                    <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Apellido materno</span>
                                </span> <input type="text" className="form-control " name="ape_materno" onChange={this.onChange} value={this.state.referido.ape_materno} />
                            </div>
                        </div>
                    </div>



                </div>


                <div className="row">

                    <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Correo electrónico</span>
                                </span> <input type="text" className="form-control " name="correo_electronico" onChange={this.onChange} value={this.state.referido.correo_electronico} />
                            </div>
                        </div>
                    </div>

                    <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Teléfono móvil</span>
                                </span> <input type="text" className="form-control " name="tel_movil" onChange={this.onChange} value={this.state.referido.tel_movil} />
                            </div>
                        </div>
                    </div>

                    <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>EDAT asignado</span>
                                </span> {this.state.selectEDAT}
                            </div>
                        </div>
                    </div>



                </div>
                
                { this.state.banDescartar ? 
                    <div className="row">
                    <div className="col-xs-12 col-lg-6">
                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Comentarios Descarte</span>
                                    </span> <textarea  maxLength={250} type="text" className="form-control " disabled = {this.state.banBloqueaDescartar} name="comentarios_descarte" onChange={this.onChange} style={{ borderColor: '#C8CDF6', backgroundColor: '#FFFF', height: '150px' }} value={this.state.referido.obs_descarte}/>
                                </div>
                            </div>
                        </div>
                    </div> : ''
                }
            </div>
        )
    }
}