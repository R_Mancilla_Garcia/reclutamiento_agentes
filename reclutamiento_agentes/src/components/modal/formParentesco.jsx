import React, { Component } from "react";
import datos from '../urls/datos.json'

export default class FormParentesco extends Component {

    constructor() {
        super();
        this.guardarElemento = this.guardarElemento.bind(this)
        this.onChange = this.onChange.bind(this)
        this.state = {
            parentesco: '',
            idActualizar: 0,
            selectOptions: []
        }
    }


    UNSAFE_componentWillMount() {
        console.log("this.props.tipoUsuario"+this.props.tipoUsuario);
        if (this.props.contenido != undefined) {
            console.log("entrando a form ", this.props.contenido)
            this.setState({
                idActualizar: this.props.contenido[0].value,
                parentesco: this.props.contenido[1].value

            })
        }

    }

    onChange = e => {
       // console.log("entrando select ", e.target.name, e.target.value)
        this.setState({ [e.target.name]: e.target.value })
    }


    guardarElemento() {
        console.log(this.props)
        let json = {
            "parentesco": this.state.parentesco,
            "user_id": parseInt(this.props.idUsuario),
        }
        if (this.props.contenido != undefined) {
            this.props.guardaElementoNuevo(datos.urlServicePy+"parametros_upd/api_cat_parentesco/" + this.state.idActualizar, json, "PUT", "mostrarTablaParametrosParentesco")
        } else {
            this.props.guardaElementoNuevo(datos.urlServicePy+"parametros/api_cat_parentesco/0", json, "POST", "mostrarTablaParametrosParentesco")
        }
    }

    



    render() {
        return (
            <div>
                <div className="modal-body">
                    <div className="card-body">
                        <div className="row">
                            <div className="col-xs-12 col-lg-3">
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text border-dark text-white" style={{ backgroundColor: "#313A46" }}>Parentesco</span>
                                        </span> <input type="text" name="parentesco" value={this.state.parentesco} disabled = { (this.props.contenido != undefined || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.contenido != undefined || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER")} onChange={this.onChange} className="form-control bg-dark border-dark text-white" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-warning" id="btnAgregar" data-dismiss="modal" disabled = { (this.props.contenido != undefined || this.props.tipoUsuario != "ANAJR") 
                            && (this.props.contenido != undefined || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER") || this.state.parentesco.length == 0} onClick={this.guardarElemento} >Agregar</button>
                </div>
            </div>
        )
    }


}