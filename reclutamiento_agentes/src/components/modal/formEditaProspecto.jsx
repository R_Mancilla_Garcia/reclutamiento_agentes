import React, { Component } from "react";
import axios from 'axios';
import datos from '../urls/datos.json'

export default class FormEditaProspectos extends Component {

    constructor() {
        super();
        this.onChange = this.onChange.bind(this)
        this.enviarBaja=this.enviarBaja.bind(this)
        this.construyeSelect = this.construyeSelect.bind(this)
        this.onClickBotonArchivoCV = this.onClickBotonArchivoCV.bind(this)
        this.descartar = this.descartar.bind(this)
        this.guardarCambios = this.guardarCambios.bind(this)
        this.obtenEstatus = this.obtenEstatus.bind(this)
        this.construyeSelectAgenteReferidor = this.construyeSelectAgenteReferidor.bind(this)
        this.construyeSelectMotivos = this.construyeSelectMotivos.bind(this)
        this.onChangeMotivos=this.onChangeMotivos.bind(this)
        this.state = {
            selectEDAT: [],
            selectFuenteReclutamiento: [],
            selectFuenteReclutamientoBono: [],
            selectAgenteReferidor: [],
            selectEstadoCivil: [],
            referido: {},
            colorSubirArchivoCV: "#E1E5F0",
            colorTextoSubirArchivoCV: "#617187",
            colorBotonSubirArchivoCV: "#0F69B8",
            nombreArchivoCV: "",
            archivoCV: undefined,
            muestraDocCV: false,
            estatus: '',
            muestraAgente: false,
            expresionRFC: /^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$/,
            validaRFC: false,
            expresionEmail: /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
            validaEmail: false,
            idMotivoBaja:0,
            banBloquear: false
        }
    }

    descartar() {
        let json = {
            "estatus_id": 19,
            "causas_baja_id": this.state.idMotivoBaja
        }
        const requestOptions = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };
        console.log("enviando a reserva")

        fetch(datos.urlServicePy+"parametros_upd/api_recluta_prospectos/" + this.state.referido.id, requestOptions)
            .then(response => response.json())
            .then(data => {
               // this.construyeTarjetas()
                this.props.botonCancelar()
            })

    }
    obtenEstatus() {
        fetch(datos.urlServicePy+"parametros/api_recluta_vprospectos/" + this.props.idEmpleado)
            .then(response => response.json())
            .then(json => {
                this.setState({ estatus: json.filas[0].fila[4].value })
            })
    }
    guardarCambios() {
        console.log(this.state.referido)
        this.state.referido.user_id = this.props.idUsuario
        this.state.referido.estatus_id = 1
        const requestOptions = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(this.state.referido)
        };

        fetch(datos.urlServicePy+"parametros_upd/api_recluta_prospectos/" + this.state.referido.id, requestOptions)
            .then(response => response.json())
            .then(data => {

                if (this.state.referido.doc_cv != null && this.state.referido.doc_cv.length > 0) {
                    if (this.state.archivoCV != undefined) {
                        let formData = new FormData();
                        formData.append('id', this.state.referido.id)
                        formData.append('cv', this.state.archivoCV)
                        formData.append('prospecto_id', this.state.referido.id)
                        const config = {
                            headers: { 'content-type': 'multipart/form-data' }
                        }
                        axios.put(datos.urlServicePy+"api/doc_prospectos/" + this.state.referido.id + "/", formData, config)
                            .then(res => {
                                console.log(res.data);
                                console.log(this.state.filename);
                                console.log(formData);
                            })

                    }
                } else {
                    if (this.state.archivoCV != undefined) {
                        let formData = new FormData();
                        formData.append('id', this.state.referido.id)
                        formData.append('cv', this.state.archivoCV)
                        formData.append('prospecto_id', this.state.referido.id)
                        const config = {
                            headers: { 'content-type': 'multipart/form-data' }
                        }
                        axios.post(datos.urlServicePy+"api/doc_prospectos/", formData, config)
                            .then(res => {
                                console.log(res.data);
                                console.log(this.state.filename);
                                console.log(formData);
                            })

                    }
                }





                this.props.botonCancelar()

            })


    }

    onChange = e => {
        let campoBeneficiario = e.target.name
        let referido = this.state.referido
        console.log("Entro en onChange");
        //
        if ("selectEDAT" == e.target.name) {
            referido.empleado_id = parseInt(e.target.value)
        } else if ("selectEstadoCivil" == e.target.name) {
            referido.estadocivil_id = parseInt(e.target.value)
        } else if ("selectAgenteReferidor" == e.target.name) {
            console.log("selector agente referido  ", e.target.value)
            let contenido = e.target.value
            if (contenido != 0) {
                let separacion = contenido.split(",")
                referido.agente_referidor = parseInt(separacion[0])
                referido.cua_referidor = separacion[2]
            } else {
                referido.agente_referidor = 0
                referido.cua_referidor = ""
            }
        } else if ("selectFuenteReclutamiento" == e.target.name) {
            referido.fuente_reclutamiento_id = parseInt(e.target.value)
            if (referido.fuente_reclutamiento_id == 14) {
                console.log("Entro aqui");
                this.setState({ muestraAgente: true })
            } else {
                this.setState({ muestraAgente: false })
            }
        } else if ("selectFuenteReclutamientoBono" == e.target.name) {
            referido.fuente_recluta_bonos_id = parseInt(e.target.value)

        } else if ("rfc" == e.target.name) {
            let rfc = e.target.value;
            if (rfc.match(this.state.expresionRFC)) {
                this.setState({ validaRFC: true })
                var rfcanio = rfc.substring(4, 6);
                var rfcmes = rfc.substring(6, 8);
                var rfcdia = rfc.substring(8, 10);
                var formatDate = "";

                if (rfcanio <= 20) {
                    formatDate = "20" + rfcanio + "-";
                } else {
                    formatDate = "19" + rfcanio + "-";
                }
                formatDate = formatDate + rfcmes + "-" + rfcdia;
                referido.fec_nacimiento = formatDate;
                console.log("rfc");
            } else {
                referido.fec_nacimiento = "";
                this.setState({ validaRFC: false })
            }
            //            this.setState({ [e.target.name]: e.target.value })
            referido["" + campoBeneficiario + ""] = e.target.value

        } else if ("correo_electronico" == e.target.name) {
            let email = e.target.value;
            if (email.match(this.state.expresionEmail)) {
                this.setState({ validaEmail: true })
            } else {
                this.setState({ validaEmail: false })
            }
            referido["" + campoBeneficiario + ""] = e.target.value

        } else if (e.target.name == "tel_movil" || e.target.name == "tel_casa" || e.target.name == "tel_oficina") {

            let movil = e.target.value
            if (movil.length <= 10) {
                referido["" + e.target.name + ""] = e.target.value
            }


        } else {
            referido["" + campoBeneficiario + ""] = e.target.value
        }

        this.setState({ referido: referido })

    }

    onChangeFileCV(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chingado file de CV ", file);
        this.setState({ archivoCV: file, colorSubirArchivoCV: "#78CB5A", colorTextoSubirArchivoCV: "#FFFFFF", colorBotonSubirArchivoCV: "#E5F6E0", nombreArchivoCV: file.name })


    }

    onClickBotonArchivoCV() {
        console.log("entrnaod a la CV")
        this.cv.click()
    }


    onChangeMotivos = e => {
        if (e.target.name == 'selectMotivos') {
            this.setState({ idMotivoBaja: parseInt(e.target.value) })
        } else {
            this.setState({ [e.target.name]: e.target.value })

        }
    }


    UNSAFE_componentWillMount() {
        this.construyeSelectMotivos(datos.urlServicePy+"parametros/api_cat_causas_reserva/0")

        fetch(datos.urlServicePy+"parametros/api_recluta_prospectos/" + this.props.idEmpleado)
            .then(response => response.json())
            .then(json => {
                this.obtenEstatus()
                let fila = json.filas[0].fila
                let columnas = json.columnas
                let columnasSalida = {}
                for (var i = 0; i < fila.length; i++) {
                    console.log(fila[i])
                    columnasSalida["" + columnas[i].key + ""] = fila[i].value
                }

                let rfc = columnasSalida.rfc
                let email = columnasSalida.correo_electronico
                let agenteReferidor = columnasSalida.fuente_reclutamiento_id != null ? parseInt(columnasSalida.fuente_reclutamiento_id) : 0;

                console.log(columnasSalida, "columnas salida ")
                this.setState({ referido: columnasSalida, validaRFC: rfc != null ? rfc.match(this.state.expresionRFC) : false, validaEmail: email != null ? email.match(this.state.expresionEmail) : false, muestraAgente: agenteReferidor == 14 ? true : false })
                if (columnasSalida.doc_cv != null && columnasSalida.doc_cv.length > 0) {
                    this.setState({ colorSubirArchivoCV: "#78CB5A", colorTextoSubirArchivoCV: "#FFFFFF", colorBotonSubirArchivoCV: "#E5F6E0", nombreArchivoCV: "Reemplazar...", muestraDocCV: true })
                }
                if(columnasSalida.estatus_id == 2){
                    this.setState({banBloquear: true})
                }

                this.construyeSelect(datos.urlServicePy+"recluta/api_vcat_edat/0", "selectEDAT")
                this.construyeSelect(datos.urlServicePy+"parametros/api_cat_fuente_reclutamiento/0", "selectFuenteReclutamiento")
                this.construyeSelect(datos.urlServicePy+"parametros/api_cat_fuente_reclutamiento_bonos/0", "selectFuenteReclutamientoBono")
                this.construyeSelect(datos.urlServicePy+"parametros/api_cat_estadocivil/0", "selectEstadoCivil")
                this.construyeSelectAgenteReferidor(datos.urlServicePy+"recluta/api_recluta_vagente_referidor/0", "selectAgenteReferidor");
            })
    }


    construyeSelectAgenteReferidor(url, selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                for (var i = 0; i < filas.length; i++) {
                    let fila = filas[i]
                    if (selector == "selectAgenteReferidor") {
                        if (this.props.idEmpleado) {
                            console.log("referido aqui" + this.state.referido.agente_referidor);
                            if (this.state.referido.agente_referidor == fila.id) {
                                options.push(<option selected value={fila.id + "," + fila.agente_referidor + "," + fila.cua_referidor}>{fila.agente_referidor}</option>)
                            } else {
                                options.push(<option value={fila.id + "," + fila.agente_referidor + "," + fila.cua_referidor}>{fila.agente_referidor}</option>)
                            }
                        } else {
                            options.push(<option value={fila.id + "," + fila.agente_referidor + "," + fila.cua_referidor}>{fila.agente_referidor}</option>)
                        }

                    }

                }
                let salida = []
                salida.push(<select onChange={this.onChange} name={selector} style={{ borderColor: '#E1E5F0' }} className="form-control " aria-label="Default select example">{options}</select>)
                this.setState({ selectAgenteReferidor: salida })
            })
    }



    construyeSelect(url, selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = selector == "selectEDAT" ? json : json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                for (var i = 0; i < filas.length; i++) {

                    let fila = filas[i]
                    if (selector == "selectEDAT") {
                        if (this.props.idEmpleado) {

                            if (this.state.referido.empleado_id == fila.id) {
                                options.push(<option selected value={fila.id}>{fila.nombre}</option>)
                            } else {
                                options.push(<option value={fila.id}>{fila.nombre}</option>)
                            }
                        } else {
                            options.push(<option value={fila.id}>{fila.nombre}</option>)
                        }
                    }



                    if (selector == "selectEstadoCivil") {
                        if (this.props.idEmpleado) {

                            if (this.state.referido.estadocivil_id == fila.fila[0].value) {
                                options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            } else {
                                options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            }
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }

                    }

                    if (selector == "selectFuenteReclutamientoBono") {
                        if (this.props.idEmpleado) {

                            if (this.state.referido.fuente_recluta_bonos_id == fila.fila[0].value) {
                                options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            } else {
                                options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            }
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }

                    }

                    if (selector == "selectFuenteReclutamiento") {
                        if (this.props.idEmpleado) {

                            if (this.state.referido.fuente_reclutamiento_id == fila.fila[0].value) {
                                options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            } else {
                                options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            }
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }

                    }




                }
                let salida = []
                salida.push(<select onChange={this.onChange} name={selector} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ [selector]: salida /*,selectParentesco2: salida,selectParentesco3: salida*/ })
            });
    }


    enviarBaja(id) {
        //this.setState({ idCambiaEstatus: id })
        this.baja.click()



    }


    construyeSelectMotivos(url) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                console.log("respuesta  ", json)
                for (var i = 0; i < filas.length; i++) {
                    let fila = filas[i]

                    options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                }
                let salida = []
                salida.push(<select onChange={this.onChangeMotivos} name="selectMotivos" style={{ borderColor: '#F1F3FA' }} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ selectMotivos: salida })


            })
    }

    render() {
        return (
        <div>
            <button type="button" style={{ display: 'none' }} ref={(ref) => this.baja = ref} data-toggle="modal" data-target="#modal-baja"   ></button>
            <div id="modal-baja" className="modal fade" tabindex="-1">
                <div className="modal-dialog ">
                    <div className="modal-content text-white" style={{ backgroundColor: "#FFFFF" }}>
                        <div className="modal-header text-white text-center" style={{ backgroundColor: "#617187" }}>
                            <h6 className="modal-title col-12 text-center">Dar baja el registro</h6>

                        </div>
                        <div className="modal-body">
                            <div className="card-body">
                                <h8 style={{ color: '#8F9EB3' }}>Si está seguro de descartar el registro, por favor seleccione el motivo</h8>
                                {this.state.selectMotivos}


                            </div>
                        </div>
                        <div class="modal-footer d-flex justify-content-center">
                            <div className="col-12">
                                <div className="row">
                                    <button type="button" style={{ width: '100%', backgroundColor: "#617187" }} class="btn text-white" onClick={this.descartar} data-dismiss="modal"  >Dar de baja registro</button>
                                </div>
                                <br></br>
                                <div className="row">
                                    <button type="button" style={{ width: '100%', backgroundColor: '#8F9EB3' }} class="btn text-white" id="btnAgregar" data-dismiss="modal"  >Cancelar</button>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <div class="card">
                <div class="row mb-2 mt-2 ml-1">

                    <div class="col-12 col-lg-12">
                        <div class="float-left mr-3">
                            <h6 className="font-weight-bold">{'Proceso de reclutamiento'}</h6>
                        </div>
                        <div class="float-right mr-3">
                            <button type="button" class="btn btn-light mr-3 text-white" style={{ backgroundColor: '#617187', borderColor: '#617187' }} onClick={(e) => this.props.botonCancelar()} >Cancelar</button>
                            <button type="button" class="btn btn-light mr-3 text-white" style={{ backgroundColor: '#617187', borderColor: '#617187' }} onClick={this.enviarBaja} disabled = {this.state.banBloquear}>Descartar</button>
                            <button type="button" class="btn btn-warning text-white" style={{ backgroundColor: '#ED6C26', borderColor: '#ED6C26' }} onClick={this.guardarCambios} disabled = {this.state.banBloquear}>Guardar cambios</button>
                        </div>
                    </div>
                </div>
            </div>




            <div className="row">
                <div className="col-xs-12 col-lg-2">
                    <div className="form-group">
                        <div className="input-group">
                            <h4 class="card-title py-3 font-weight-bold" >{"Prospecto " + this.state.referido.id}</h4>

                        </div>
                    </div>
                </div>
            </div>


            <div className="row">

                <div className="col-xs-12 col-lg-4">
                    <div className="form-group">
                        <div className="input-group">
                            <span className="input-group-prepend"> <span
                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fecha alta</span>
                            </span> <input type="text" disabled className="form-control " value={this.state.referido.ts_alta_audit} />
                        </div>
                    </div>
                </div>

                <div className="col-xs-12 col-lg-4">
                    <div className="form-group">
                        <div className="input-group">
                            <span className="input-group-prepend"> <span
                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Usuario creador</span>
                            </span> <input type="text" disabled value={this.state.referido.user_id} className="form-control " />
                        </div>
                    </div>
                </div>

                <div className="col-xs-12 col-lg-2">
                    <div className="form-group">
                        <div className="input-group">
                            <span className="input-group-prepend"> <h6
                                className="" style={{ color: '#617187' }} >Estatus del proceso de reclutamiento</h6>
                            </span>
                        </div>
                    </div>
                </div>


                <div className="col-xs-12 col-lg-1">
                    <div className="form-group">
                        <div className="input-group">
                            <span className="input-group-prepend"> <h6
                                style={{ backgroundColor: "#C28EE3", color: '#FFFF', borderRadius: '8px' }}> <>&nbsp;&nbsp;</> {this.state.estatus} <>&nbsp;&nbsp;</> </h6>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            {/* informacion general ************************************************************************************** */}

            <br></br>


            <div className="row">
                <div className="col-xs-12 col-lg-2">
                    <div className="form-group">
                        <div className="input-group">
                            <h4 class="card-title py-3 font-weight-bold" >Información general</h4>
                        </div>
                    </div>
                </div>
            </div>

            <div className="row">
                <div className="col-xs-12 col-lg-4">
                    <div className="form-group">
                        <div className="input-group">
                            <span className="input-group-prepend"> <span
                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Nombre (s)</span>
                            </span> <input type="text" placeholder="Nombre" name="nombre" onChange={this.onChange} value={this.state.referido.nombre} className="form-control " />
                        </div>
                        {
                            this.state.referido.nombre == "" || this.state.referido.nombre == null ?
                                <span className="" style={{ color: "red" }}>El nombre es un campo requerido</span> : ''
                        }
                    </div>
                </div>

                <div className="col-xs-12 col-lg-4">
                    <div className="form-group">
                        <div className="input-group">
                            <span className="input-group-prepend"> <span
                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Apellido paterno</span>
                            </span> <input type="text" placeholder="Apellido" name="ape_paterno" onChange={this.onChange} value={this.state.referido.ape_paterno} className="form-control " />
                        </div>
                        {
                            this.state.referido.ape_paterno == "" || this.state.referido.ape_paterno == null ?
                                <span className="" style={{ color: "red" }}>El apellido paterno es un campo requerido</span> : ''
                        }
                    </div>
                </div>


                <div className="col-xs-12 col-lg-4">
                    <div className="form-group">
                        <div className="input-group">
                            <span className="input-group-prepend"> <span
                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Apellido materno</span>
                            </span> <input type="text" placeholder="Apellido" className="form-control " name="ape_materno" onChange={this.onChange} value={this.state.referido.ape_materno} />
                        </div>
                        {
                            //  this.state.referido.ape_materno == "" || this.state.referido.ape_materno == null ?
                            //     <span className="" style={{ color: "red" }}>El apellido materno es un campo requerido</span> : ''
                        }
                    </div>
                </div>
            </div>

            <div className="row">
                <div className="col-xs-12 col-lg-4">
                    <div className="form-group">
                        <div className="input-group">
                            <span className="input-group-prepend"> <span
                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fecha de nacimiento</span>
                            </span> <input type="date" className="form-control " name="fec_nacimiento" max="2100-01-01" onChange={this.onChange} value={this.state.referido.fec_nacimiento} />
                        </div>
                        {
                            this.state.referido.fec_nacimiento == "" || this.state.referido.fec_nacimiento == null ?
                                <span className="" style={{ color: "red" }}>La fecha de nacimiento es un campo requerido</span> : ''
                        }
                    </div>
                </div>

                <div className="col-xs-12 col-lg-4">
                    <div className="form-group">
                        <div className="input-group">
                            <span className="input-group-prepend"> <span
                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>RFC</span>
                            </span> <input type="text" placeholder="Escribir" className="form-control " name="rfc" onChange={this.onChange} value={this.state.referido.rfc} />
                        </div>
                        {
                            this.state.validaRFC == false ?
                                <span className="" style={{ color: "red" }}>El rfc es un campo requerido</span> : ''
                        }
                    </div>
                </div>


                <div className="col-xs-12 col-lg-4">
                    <div className="form-group">
                        <div className="input-group">
                            <span className="input-group-prepend"> <span
                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Estado civil</span>
                            </span> {this.state.selectEstadoCivil}
                        </div>
                        {
                            //  this.state.referido.estadocivil_id == 0 || this.state.referido.estadocivil_id == null ?
                            //     <span className="" style={{ color: "red" }}>El estado civil es un campo requerido</span> : ''
                        }
                    </div>
                </div>
            </div>

            <div className="row">
                <div className="col-xs-12 col-lg-4">
                    <div className="form-group">
                        <div className="input-group">
                            <form encType="multipart/form" style={{ display: 'none' }} >
                                <input type="file" style={{ display: 'none' }} ref={(ref) => this.cv = ref} onChange={this.onChangeFileCV.bind(this)}></input>
                            </form>
                            <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " placeholder="Curriculum Vitae" value={this.state.nombreArchivoCV} />
                            <span className="input-group-prepend">
                                <button type="button" class="btn text-white" onClick={this.onClickBotonArchivoCV} style={{ backgroundColor: this.state.colorBotonSubirArchivoCV }}>
                                    <h10 style={{ color: this.state.colorSubirArchivoCV }}>+</h10>
                                </button>

                                {
                                    this.state.muestraDocCV == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivo }}>
                                        <a href={this.state.referido.doc_cv} target="_blank" rel="noopener noreferrer" >
                                            <i className="fas fa-eye"
                                                style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                        </a>
                                    </button>
                                        : ''
                                }

                                <span className="input-group-text" style={{ background: this.state.colorSubirArchivoCV, borderColor: this.state.colorSubirArchivoCV, color: this.state.colorTextoSubirArchivoCV }} >Subir archivo</span>
                            </span>
                        </div>
                    </div>
                </div>

                <div className="col-xs-12 col-lg-4">
                    <div className="form-group">
                        <div className="input-group">
                            <span className="input-group-prepend"> <span
                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Envío de link PSP</span>
                            </span> <input type="date" className="form-control " name="fec_envio_psp" max="2100-01-01" onChange={this.onChange} value={this.state.referido.fec_envio_psp} />
                        </div>
                        {/*
                            this.state.referido.fec_envio_psp == "" || this.state.referido.fec_envio_psp == null ?
                                <span className="" style={{ color: "red" }}>El envío de link psp es un campo requerido</span> : ''
                        */}
                    </div>
                </div>


                <div className="col-xs-12 col-lg-4">
                    <div className="form-group">
                        <div className="input-group">
                            <span className="input-group-prepend"> <span
                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Correo electrónico</span>
                            </span> <input type="text" placeholder="ejemplo@correo.com" className="form-control" name="correo_electronico" onChange={this.onChange} value={this.state.referido.correo_electronico} />
                        </div>
                        {
                            this.state.validaEmail == false ?
                                <span className="" style={{ color: "red" }}>El correo electrónico es un campo requerido</span> : ''
                        }
                    </div>
                </div>
            </div>

            <div className="row">
                <div className="col-xs-12 col-lg-4">
                    <div className="form-group">
                        <div className="input-group">
                            <span className="input-group-prepend"> <span
                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Teléfono móvil</span>
                            </span> <input type="number" placeholder="55 0000 0000" className="form-control " name="tel_movil" onChange={this.onChange} value={this.state.referido.tel_movil} />
                        </div>
                        {
                            (this.state.referido.tel_movil + '').length == 0 || (this.state.referido.tel_movil + '').length <= 9 || this.state.referido.tel_movil == null ?
                                <span className="" style={{ color: "red" }}>El teléfono móvil es un campo requerido</span> : ''
                        }
                    </div>
                </div>

                <div className="col-xs-12 col-lg-4">
                    <div className="form-group">
                        <div className="input-group">
                            <span className="input-group-prepend"> <span
                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Teléfono casa</span>
                            </span> <input type="number" placeholder="10 dígitos" className="form-control" name="tel_casa" onChange={this.onChange} value={this.state.referido.tel_casa} />
                        </div>
                    </div>
                </div>


                <div className="col-xs-12 col-lg-4">
                    <div className="form-group">
                        <div className="input-group">
                            <span className="input-group-prepend"> <span
                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Teléfono oficina</span>
                            </span> <input type="number" placeholder="10 dígitos" className="form-control " name="tel_oficina" onChange={this.onChange} value={this.state.referido.tel_oficina} />
                        </div>
                    </div>
                </div>
            </div>


            {/* informacion general ************************************************************************************** */}

            <br></br>


            <div className="row">
                <div className="col-xs-12 col-lg-2">
                    <div className="form-group">
                        <div className="input-group">
                            <h4 class="card-title py-3 font-weight-bold" >Información complementaría</h4>
                        </div>
                    </div>
                </div>
            </div>

            <div className="row">
                <div className="col-xs-12 col-lg-4">
                    <div className="form-group">
                        <div className="input-group">
                            <span className="input-group-prepend"> <span
                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fuente de reclutamiento</span>
                            </span> {this.state.selectFuenteReclutamiento}
                        </div>
                        {
                            this.state.referido.fuente_reclutamiento_id == 0 || this.state.referido.fuente_reclutamiento_id == null ?
                                <span className="" style={{ color: "red" }}>La fuente de reclutamiento es un campo requerido</span> : ''
                        }
                    </div>
                </div>

                <div className="col-xs-12 col-lg-4">
                    <div className="form-group">
                        <div className="input-group">
                            <span className="input-group-prepend"> <span
                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Fuente de reclutamiento para bonos</span>
                            </span> {this.state.selectFuenteReclutamientoBono} {/* mover este state al correcto */}
                        </div>
                        {
                            this.state.referido.fuente_recluta_bonos_id == 0 || this.state.referido.fuente_recluta_bonos_id == null ?
                                <span className="" style={{ color: "red" }}>La fuente de reclutamiento para bonos es un campo requerido</span> : ''
                        }
                    </div>
                </div>


                <div className="col-xs-12 col-lg-4">
                    <div className="form-group">
                        <div className="input-group">
                            <span className="input-group-prepend"> <span
                                className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>EDAT asignado</span>
                            </span> {this.state.selectEDAT}
                        </div>
                        {
                            this.state.referido.empleado_id == 0 || this.state.referido.empleado_id == null ?
                                <span className="" style={{ color: "red" }}>El EDAT asignado es un campo requerido</span> : ''
                        }
                    </div>
                </div>
            </div>
            {this.state.muestraAgente ?
                <div className="row">
                    <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Agente referidor</span>
                                </span> {/*<input type="text" placeholder="Escribir" className="form-control " />*/} {this.state.selectAgenteReferidor}
                            </div>
                            {
                                this.state.referido.agente_referidor == 0 || this.state.referido.agente_referidor == null ?
                                    <span className="" style={{ color: "red" }}>El agente referidor es un campo requerido</span> : ''
                            }
                        </div>
                    </div>


                    <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>CUA referidora para repostar a GNP</span>
                                </span> <input type="text" placeholder="Escribir" className="form-control " name="cua_referidor" disabled={true} onChange={this.onChange} value={this.state.referido.cua_referidor} />
                            </div>
                        </div>
                    </div>

                    <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Nombre APS</span>
                                </span> <input type="text" placeholder="Escribir" className="form-control " name="nombre_aps" onChange={this.onChange} value={this.state.referido.nombre_aps} />
                            </div>
                            {
                                this.state.referido.nombre_aps == "" || this.state.referido.nombre_aps == null ?
                                    <span className="" style={{ color: "red" }}>El nombre_aps es un campo requerido</span> : ''
                            }
                        </div>
                    </div>
                </div> : <div className="row">


                    <div className="col-xs-12 col-lg-4">
                        <div className="form-group">
                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text " style={{ backgroundColor: '#D5D9E8', color: '#617187' }}>Nombre APS</span>
                                </span> <input type="text" placeholder="Escribir" className="form-control " name="nombre_aps" onChange={this.onChange} value={this.state.referido.nombre_aps} />
                            </div>
                            {
                                this.state.referido.nombre_aps == "" || this.state.referido.nombre_aps == null ?
                                    <span className="" style={{ color: "red" }}>El nombre_aps es un campo requerido</span> : ''
                            }
                        </div>
                    </div>
                </div>
            }




        </div>
        )
    }

}