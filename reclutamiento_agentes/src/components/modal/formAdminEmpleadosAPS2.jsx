import React, { Component } from "react";
import datos from '../urls/datos.json'

//const avatar = require("../imagenes/avatar.png")

export default class FormAdminEmpleadosAPS2 extends Component {

    constructor() {
        super();
        this.onChange = this.onChange.bind(this)
        this.construyeSelect = this.construyeSelect.bind(this)
        this.onClickBotonArchivoPuesto = this.onClickBotonArchivoPuesto.bind(this)
        this.onClickBotonArchivoCV = this.onClickBotonArchivoCV.bind(this)
        this.onClickBotonArchivoPSP = this.onClickBotonArchivoPSP.bind(this)
        this.aumentaVacaciones = this.aumentaVacaciones.bind(this)
        this.disminuyeVacaciones = this.disminuyeVacaciones.bind(this)
        this.state = {
            contenidoEdicion: undefined,
            selectEscolaridad: undefined,
            selectProfesion: undefined,
            selectPuesto: undefined,
            selectArea: undefined,
            selectBancos: undefined,
            selectCausasBaja: undefined,
            selectEstatusBaja: undefined,
            selectSociedad: undefined,
            colorSubirArchivo: "#E1E5F0",
            colorTextoSubirArchivo: "#617187",
            colorBotonSubirArchivo: "#0F69B8",
            nombreArchivoPuesto: "",
            archivoPuesto: undefined,

            colorSubirArchivoCV: "#E1E5F0",
            colorTextoSubirArchivoCV: "#617187",
            colorBotonSubirArchivoCV: "#0F69B8",
            nombreArchivoCV: "",
            archivoCV: undefined,

            colorSubirArchivoPSP: "#E1E5F0",
            colorTextoSubirArchivoPSP: "#617187",
            colorBotonSubirArchivoPSP: "#0F69B8",
            nombreArchivoPSP: "",
            archivoPSP: undefined,

            contadorVacaciones: 0



        }
    }

    aumentaVacaciones() {
        let contenido = this.state.contenidoEdicion;
        contenido.dias_vacaciones = contenido.dias_vacaciones + 1
        this.setState({ contenidoEdicion: contenido })
    }
    disminuyeVacaciones() {
        let contenido = this.state.contenidoEdicion;
        contenido.dias_vacaciones = contenido.dias_vacaciones - 1
        this.setState({ contenidoEdicion: contenido })
    }

    construyeSelect(url, selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                console.log("respuesta de las ciudades ", json)
                for (var i = 0; i < filas.length; i++) {

                    let fila = filas[i]
                    if (selector == "selectEscolaridad") {
                        if (this.state.contenidoEdicion.escolaridad_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }
                    } else if (selector == "selectSociedad") {
                        if (this.state.contenidoEdicion.sociedad_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }
                    }
                    else if (selector == "selectPuesto") {
                        if (this.state.contenidoEdicion.puesto_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }
                    } else if (selector == "selectArea") {
                        if (this.state.contenidoEdicion.area_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }
                    } else if (selector == "selectProfesion") {
                        if (this.state.contenidoEdicion.profesion_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }
                    } else if (selector == "selectBancos") {
                        if (this.state.contenidoEdicion.banco_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }
                    } else if (selector == "selectCausasBaja") {
                        if (this.state.contenidoEdicion.causabaja_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }

                    } else if (selector == "selectEstatusBaja") {
                        if (this.state.contenidoEdicion.estatusbaja_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }

                    }



                }
                let salida = []
                salida.push(<select onChange={this.onChange} name={selector} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ [selector]: salida })
            });
    }


  
    UNSAFE_componentWillMount() {
        console.log("component segundo form", this.props)
        this.construyeSelect(datos.urlServicePy+"parametros/api_cat_escolaridad/0", "selectEscolaridad")
        this.construyeSelect(datos.urlServicePy+"parametros/api_vcat_puesto/0", "selectPuesto")
        this.construyeSelect(datos.urlServicePy+"parametros/api_vcat_area/0", "selectArea")
        this.construyeSelect(datos.urlServicePy+"parametros/api_cat_profeciones/0", "selectProfesion")
        this.construyeSelect(datos.urlServicePy+"parametros/api_cat_bancos/0", "selectBancos")
        this.construyeSelect(datos.urlServicePy+"parametros/api_cat_causas_baja/0", "selectCausasBaja")
        this.construyeSelect(datos.urlServicePy+"parametros/api_cat_estatusbaja/0", "selectEstatusBaja")//
        this.construyeSelect(datos.urlServicePy+"parametros/api_cat_sociedad/0", "selectSociedad")

        this.setState({ contenidoEdicion: this.props.contenidoEdicion })

    }

    onChange = e => {
        console.log("entrando", e.target.name, e.target.value)



        let contenidoEdicion = this.state.contenidoEdicion
        if ("selectEscolaridad" == e.target.name) {
            console.log("entroa asleect")
            let contenido = this.state.contenidoEdicion
            contenido.escolaridad_id = parseInt(e.target.value)
            this.setState({ contenidoEdicion: contenido })
           

        }else if ("selectPuesto" == e.target.name) {
            console.log("entroa asleect")
            let contenido = this.state.contenidoEdicion
            contenido.puesto_id = parseInt(e.target.value)
            this.setState({ contenidoEdicion: contenido })
           

        }else if ("selectArea" == e.target.name) {
            console.log("entroa asleect")
            let contenido = this.state.contenidoEdicion
            contenido.area_id= parseInt(e.target.value)
            this.setState({ contenidoEdicion: contenido })

        }else if ("selectProfesion" == e.target.name) {
            console.log("entroa asleect")
            let contenido = this.state.contenidoEdicion
            contenido.profesion_id= parseInt(e.target.value)
            this.setState({ contenidoEdicion: contenido })

        }else if ("selectBancos" == e.target.name) {
            console.log("entroa asleect")
            let contenido = this.state.contenidoEdicion
            contenido.banco_id= parseInt(e.target.value)
            this.setState({ contenidoEdicion: contenido })

        }else if ("selectCausasBaja" == e.target.name) {
            console.log("entroa asleect")
            let contenido = this.state.contenidoEdicion
            contenido.causabaja_id= parseInt(e.target.value)
            this.setState({ contenidoEdicion: contenido })

        }else if ("selectEstatusBaja" == e.target.name) {
            console.log("entroa asleect")
            let contenido = this.state.contenidoEdicion
            contenido.estatusbaja_id= parseInt(e.target.value)
            this.setState({ contenidoEdicion: contenido })

        }else if ("selectSociedad" == e.target.name) {
            console.log("entroa asleect")
            let contenido = this.state.contenidoEdicion
            contenido.sociedad_id= parseInt(e.target.value)
            this.setState({ contenidoEdicion: contenido })

        }else if (e.target.name == "acceso_impresion" || e.target.name == "acceso_copiadora" || e.target.name == "acceso_pai") {
            contenidoEdicion["" + e.target.name + ""] = e.target.checked == true ? 1:0
        }else if(e.target.name == "sueldo") {
            contenidoEdicion["" + e.target.name + ""] = parseInt(e.target.value)

        }else {
            contenidoEdicion["" + e.target.name + ""] = e.target.value
        }

        this.setState({ contenidoEdicion: contenidoEdicion })
         console.log("los props al persisitir ", this.props)
         this.props.guardaFormulario2(contenidoEdicion)


        // this.setState({ [e.target.name]: e.target.value })
    }



    onChangeFilePuesto(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file", file);
        this.setState({ archivoPuesto: file, colorSubirArchivo: "#78CB5A", colorTextoSubirArchivo: "#FFFFFF", colorBotonSubirArchivo: "#E5F6E0", nombreArchivoPuesto: file.name })
        this.props.guardaArchivo(file,"archivoPuesto")

    }
    onChangeFileCV(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file", file);
        this.setState({ archivoCV: file, colorSubirArchivoCV: "#78CB5A", colorTextoSubirArchivoCV: "#FFFFFF", colorBotonSubirArchivoCV: "#E5F6E0", nombreArchivoCV: file.name })


    }

    onChangeFilePSP(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file", file);
        this.setState({ archivoPSP: file, colorSubirArchivoPSP: "#78CB5A", colorTextoSubirArchivoPSP: "#FFFFFF", colorBotonSubirArchivoPSP: "#E5F6E0", nombreArchivoPSP: file.name })


    }

    onClickBotonArchivoPuesto() {
        console.log("entrnaod a la imagen")
        this.upload.click()
    }


    onClickBotonArchivoCV() {
        console.log("entrnaod a la CV")
        this.cv.click()
    }

    onClickBotonArchivoPSP() {
        console.log("entrnaod a la PSP")
        this.psp.click()
    }





    render() {
        return (
            <div>
                <div class="card-header bg-transparent header-elements-sm-inline">
                    <h2 class="mb-0 font-weight-semibold">2. Información laboral </h2>
                    <div class="header-elements">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item font-size-sm">Fecha de alta: <strong >dd/mm/aaaa</strong></li>

                            <li class="list-inline-item font-size-sm">Usuario creador: <strong >EMANCILL</strong></li>
                        </ul>
                    </div>
                </div>
                <br></br>
                <br></br>

                <div>
                    <div className="row">
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Estatus</span>
                                    </span> {this.state.selectEstatusBaja}
                                    {/* <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />*/}
                                </div>
                                {
                                    this.state.contenidoEdicion.estatus_id == 0 ?
                                        <span className="" style={{ color: "red" }}>El estatus es un campo requerido</span> : ''
                                }
                            </div>

                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }}>Fecha de ingreso</span>
                                    </span> <input type="date" style={{ borderColor: '#E1E5F0', opacity: 1 }} name="fec_ingreso" onChange={this.onChange} value={"" + this.state.contenidoEdicion.fec_ingreso + ""} className="form-control " />
                                </div>
                                {
                                    this.state.contenidoEdicion.fec_ingreso == "" ?
                                        <span className="" style={{ color: "red" }}>El campo fecha ingreso es un campo requerido</span> : ''
                                }
                            </div>
                        </div>

                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Fecha baja</span>
                                    </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " name="fec_baja" onChange={this.onChange} value={"" + this.state.contenidoEdicion.fec_baja + ""} />{/* probar value=undefined */}
                                </div>
                                {
                                    this.state.contenidoEdicion.fec_baja == "" ?
                                        <span className="" style={{ color: "red" }}>El campo fecha ingreso es un campo requerido</span> : ''
                                }
                            </div>

                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }}>Escolaridad</span>
                                    </span>
                                    {this.state.selectEscolaridad}
                                    {/*<input type="text" style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " />*/}
                                </div>
                                {
                                    this.state.contenidoEdicion.escolaridad_id == 0 ?
                                        <span className="" style={{ color: "red" }}>El campo escolaridad es un campo requerido</span> : ''
                                }
                            </div>
                        </div>
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Causa de baja</span>
                                    </span>{this.state.selectCausasBaja}
                                    {/*<input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />*/}
                                </div>
                                {
                                    this.state.contenidoEdicion.causabaja_id == 0 ?
                                        <span className="" style={{ color: "red" }}>El campo causas de baja es un campo requerido</span> : ''
                                }
                            </div>

                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }}>Profesión</span>
                                    </span>
                                    {this.state.selectProfesion}
                                    {/* <input type="text" style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " />*/}
                                </div>
                                {
                                    this.state.contenidoEdicion.profesion_id == 0 ?
                                        <span className="" style={{ color: "red" }}>El campo profesión es un campo requerido</span> : ''
                                }
                            </div>
                        </div>


                    </div>
                </div>

                <hr></hr>
                <div>
                    <div className="row">

                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Puesto</span>
                                    </span> {this.state.selectPuesto}
                                    {/*<input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />*/}
                                </div>

                            </div>
                            {
                                this.state.contenidoEdicion.puesto_id == 0 ?
                                    <span className="" style={{ color: "red" }}>El campo puesto es un campo requerido</span> : ''
                            }
                        </div>

                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">


                                <div className="input-group">
                                    <form encType="multipart/form" style={{ display: 'none' }} >
                                        <input type="file" style={{ display: 'none' }} ref={(ref) => this.upload = ref} onChange={this.onChangeFilePuesto.bind(this)}></input>
                                    </form>
                                    <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " placeholder="Descripción del puesto" value={this.state.nombreArchivoPuesto} />
                                    <span className="input-group-prepend">
                                        <button type="button" class="btn text-white" onClick={this.onClickBotonArchivoPuesto} style={{ backgroundColor: this.state.colorBotonSubirArchivo }}>
                                            <h10 style={{ color: this.state.colorSubirArchivo }}>+</h10>
                                        </button>
                                        <span className="input-group-text" style={{ background: this.state.colorSubirArchivo, borderColor: this.state.colorSubirArchivo, color: this.state.colorTextoSubirArchivo }} >Subir archivo</span>
                                    </span>
                                </div>



                            </div>



                        </div>

                        <div className="col-xs-4 col-lg-4">
                            <div className="form-check">
                                <div className="input-group">
                                    <input style={{ borderColor: '#E1E5F0' }} class="form-check-input" type="checkbox" name="acceso_pai" onChange={this.onChange} checked={this.state.contenidoEdicion.acceso_pai} id="flexCheckDisabled" />
                                    <label class="form-check-label" for="flexCheckDisabled">
                                        Accesso a PAI/DA
                                    </label>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <br></br>


                <div className="row">
                    <div className="col-xs-4 col-lg-4">
                        <div className="row">
                            {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>} <label >Generación de la figura comercial</label>
                        </div>
                        <div className="row">
                            {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="G1" />
                                <label class="form-check-label" for="G1">
                                    G1
                                </label>
                            </div>
                            {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="G2" />
                                <label class="form-check-label" for="G2">
                                    G2
                                </label>
                            </div>

                            {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="G3" />
                                <label class="form-check-label" for="G3">
                                    G3
                                </label>
                            </div>
                            {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="G4" />
                                <label class="form-check-label" for="G4">
                                    G4
                                </label>
                            </div>
                            {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="C" />
                                <label class="form-check-label" for="C">
                                    C
                                </label>
                            </div>
                        </div>
                    </div>

                    <div className="col-xs-4 col-lg-4">
                        <div className="form-group">

                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Área o departamento</span>
                                </span>{this.state.selectArea}
                                {/*<input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />*/}
                            </div>
                            {
                                this.state.contenidoEdicion.area_id == 0 ?
                                    <span className="" style={{ color: "red" }}>El campo area es un campo requerido</span> : ''
                            }
                        </div>
                    </div>

                    <div className="col-xs-4 col-lg-4">
                        <div className="form-group">

                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Supervisor directo</span>
                                </span> <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " name="supervisor" onChange={this.onChange} value={this.state.contenidoEdicion.supervisor} />
                            </div>
                        </div>
                    </div>


                </div>

                <br></br>
                <div>
                    <div className="row">
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">


                                <div className="input-group">
                                    <form encType="multipart/form" style={{ display: 'none' }} >
                                        <input type="file" style={{ display: 'none' }} ref={(ref) => this.cv = ref} onChange={this.onChangeFileCV.bind(this)}></input>
                                    </form>
                                    <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " placeholder="Curriculum Vitae" value={this.state.nombreArchivoCV} />
                                    <span className="input-group-prepend">
                                        <button type="button" class="btn text-white" onClick={this.onClickBotonArchivoCV} style={{ backgroundColor: this.state.colorBotonSubirArchivoCV }}>
                                            <h10 style={{ color: this.state.colorSubirArchivoCV }}>+</h10>
                                        </button>
                                        <span className="input-group-text" style={{ background: this.state.colorSubirArchivoCV, borderColor: this.state.colorSubirArchivoCV, color: this.state.colorTextoSubirArchivoCV }} >Subir archivo</span>
                                    </span>
                                </div>


                                {/*<div className="input-group">
                                    <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " placeholder="Curriculum Vitae" /> <span className="input-group-prepend">
                                        <button type="button" class="btn text-white" style={{ backgroundColor: "#0F69B8" }}>+</button>
                                        <span className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Subir archivo</span></span>
                                 </div>*/}






                            </div>

                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }}>Banco</span>
                                    </span>{this.state.selectBancos}
                                    {/* <input type="text" style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " />*/}
                                </div>
                                {
                                    this.state.contenidoEdicion.banco_id == 0?
                                    <span className="" style={{ color: "red" }}>El campo banco es un campo requerido</span> : ''
                                 }
                            </div>
                        </div>

                        <div className="col-xs-4 col-lg-4">

                            <div className="form-group">

                                {/* <div className="input-group">
                                    <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " placeholder="PSP" /> <span className="input-group-prepend">
                                        <button type="button" class="btn text-white" style={{ backgroundColor: "#0F69B8" }}>+</button>
                                        <span className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Subir archivo</span></span>
                                </div>*/}
                                <div className="input-group">
                                    <form encType="multipart/form" style={{ display: 'none' }} >
                                        <input type="file" style={{ display: 'none' }} ref={(ref) => this.psp = ref} onChange={this.onChangeFilePSP.bind(this)}></input>
                                    </form>
                                    <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " placeholder="PSP" value={this.state.nombreArchivoPSP} />
                                    <span className="input-group-prepend">
                                        <button type="button" class="btn text-white" onClick={this.onClickBotonArchivoPSP} style={{ backgroundColor: this.state.colorBotonSubirArchivoPSP }}>
                                            <h10 style={{ color: this.state.colorSubirArchivoPSP }}>+</h10>
                                        </button>
                                        <span className="input-group-text" style={{ background: this.state.colorSubirArchivoPSP, borderColor: this.state.colorSubirArchivoPSP, color: this.state.colorTextoSubirArchivoPSP }} >Subir archivo</span>
                                    </span>
                                </div>




                            </div>

                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }}>Clave interbancaria</span>
                                    </span> <input type="number" style={{ borderColor: '#E1E5F0', opacity: 1 }} placeholder="18 dígitos" name="cta_clabe" onChange={this.onChange} value={this.state.contenidoEdicion.cta_clabe} className="form-control " />
                                </div>
                                {
                                    this.state.contenidoEdicion.cta_clabe == 0?
                                    <span className="" style={{ color: "red" }}>El campo clave interbancaria es un campo requerido</span> : ''
                                 }
                            </div>

                        </div>

                        <div className="col-xs-4 col-lg-4">

                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-prepend">
                                        <span className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >$</span></span>
                                    <input type="number" placeholder="Sueldo nominal mensual $0.00" style={{ borderColor: '#E1E5F0' }} name="sueldo" onChange={this.onChange} value={this.state.contenidoEdicion.sueldo} className="form-control " />
                                </div>
                                {
                                    this.state.contenidoEdicion.sueldo == 0?
                                    <span className="" style={{ color: "red" }}>El campo sueldo es un campo requerido</span> : ''
                                 }
                            </div>

                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }}>Sociedad a la que pertenece</span>
                                    </span>{this.state.selectSociedad}
                                    {/* <input type="text" style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " />*/}
                                </div>
                            </div>

                        </div>


                    </div>
                </div>


                <hr></hr>
                <div>
                    <div className="row">
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >@</span>
                                    </span> <input type="text" style={{ borderColor: '#E1E5F0' }} placeholder="Correo electrónico asignado" name="correo_electronico" onChange={this.onChange} value={this.state.contenidoEdicion.correo_electronico} className="form-control " />
                                </div>
                            </div>

                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }}>No. extensión</span>
                                    </span> <input type="text" style={{ borderColor: '#E1E5F0', opacity: 1 }} name="num_extension" onChange={this.onChange} value={this.state.contenidoEdicion.num_extension} className="form-control " />
                                </div>
                            </div>
                        </div>

                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >@</span>
                                    </span> <input type="text" style={{ borderColor: '#E1E5F0' }} placeholder="Otros correos" className="form-control " name="otros_correos" onChange={this.onChange} value={this.state.contenidoEdicion.otros_correos} />
                                </div>
                            </div>

                            <div className="form-group">

                                <div className="input-group">
                                    <button type="button" onClick={this.disminuyeVacaciones} class="btn text-white" style={{ backgroundColor: "#0F69B8" }}>-</button>
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }}>Días de vacaciones</span>
                                    </span> <input type="number" name="dias_vacaciones" onChange={this.onChange} value={this.state.contenidoEdicion.dias_vacaciones} style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " />
                                    <button type="button" class="btn text-white" onClick={this.aumentaVacaciones} style={{ backgroundColor: "#0F69B8" }}>+</button>
                                </div>
                            </div>
                        </div>


                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Fecha de baja de los correos</span>
                                    </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " name="fec_baja_correos" onChange={this.onChange} value={"" + this.state.contenidoEdicion.fec_baja_correos + ""} />
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-xs-4 col-lg-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" checked={this.state.contenidoEdicion.acceso_impresion} name="acceso_impresion" onChange={this.onChange} id="acceso_impresion" />
                                        <label class="form-check-label" for="acceso_impresion">
                                            Acceso a impresión
                                        </label>
                                    </div>
                                </div>

                                <div className="col-xs-6 col-lg-6">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" checked={this.state.contenidoEdicion.acceso_copiadora} name="acceso_copiadora" onChange={this.onChange} id="acceso_copiadora" />
                                        <label class="form-check-label" for="acceso_copiadora">
                                            Acceso a copiadora
                                        </label>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>


        )
    }
}