import React, { Component } from "react";
import axios from 'axios';
import datos from '../urls/datos.json'

//const avatar = require("../imagenes/avatar.png")

export default class FormAdminEmpleadosAPS2Nuevo extends Component {



    constructor() {
        super();
        this.onChange = this.onChange.bind(this)
        this.construyeSelect = this.construyeSelect.bind(this)
        this.onClickBotonArchivoPuesto = this.onClickBotonArchivoPuesto.bind(this)
        this.onClickBotonArchivoCV = this.onClickBotonArchivoCV.bind(this)
        this.onClickBotonArchivoPSP = this.onClickBotonArchivoPSP.bind(this)
        this.aumentaVacaciones = this.aumentaVacaciones.bind(this)
        this.disminuyeVacaciones = this.disminuyeVacaciones.bind(this)
        this.clickSiguiente = this.clickSiguiente.bind(this)
        this.validaExistenciaAlgunArchivo = this.validaExistenciaAlgunArchivo.bind(this)
        this.clickInfoGeneral = this.clickInfoGeneral.bind(this)
        this.persisteEnInicioDeSesion=this.persisteEnInicioDeSesion.bind(this)
        this.state = {
            contenidoEdicion: {},
            banProfesion: false,
            expresionEmail: /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
            validaEmail: false,
            validaOtrosCorreos: false,
            selectEscolaridad: undefined,
            selectProfesion: undefined,
            selectPuesto: undefined,
            selectArea: undefined,
            selectRol: undefined,
            homoClaveRol:undefined,
            selectBancos: undefined,
            selectCausasBaja: undefined,
            selectEstatusBaja: undefined,
            selectSociedad: undefined,
            selectSupervisor: undefined,
            colorSubirArchivo: "#E1E5F0",
            colorTextoSubirArchivo: "#617187",
            colorBotonSubirArchivo: "#0F69B8",
            nombreArchivoPuesto: "",
            archivoPuesto: undefined,
            muestraDocPuesto: false,
            muestraDocCV: false,
            muestraDocPSP: false,
            banpuesto: false,

            colorSubirArchivoCV: "#E1E5F0",
            colorTextoSubirArchivoCV: "#617187",
            colorBotonSubirArchivoCV: "#0F69B8",
            nombreArchivoCV: "",
            archivoCV: undefined,

            colorSubirArchivoPSP: "#E1E5F0",
            colorTextoSubirArchivoPSP: "#617187",
            colorBotonSubirArchivoPSP: "#0F69B8",
            nombreArchivoPSP: "",
            archivoPSP: undefined,

            contadorVacaciones: 0,

            textoPuesto: 'Subir archivo',



        }
    }

    aumentaVacaciones() {
        let contenido = this.state.contenidoEdicion;
        contenido.dias_vacaciones = parseInt(contenido.dias_vacaciones) + 1
        this.setState({ contenidoEdicion: contenido })
    }
    disminuyeVacaciones() {
        let contenido = this.state.contenidoEdicion;
        contenido.dias_vacaciones = parseInt(contenido.dias_vacaciones) - 1
        this.setState({ contenidoEdicion: contenido })
    }

    construyeSelect(url, selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                console.log("respuesta de las ciudades ", json)
                for (var i = 0; i < filas.length; i++) {

                    let fila = filas[i]
                    if (selector == "selectEscolaridad") {
                        if (this.state.contenidoEdicion.escolaridad_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }
                    } else if (selector == "selectSociedad") {
                        if (this.state.contenidoEdicion.sociedad_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }
                    }
                    else if (selector == "selectPuesto") {
                        if (this.state.contenidoEdicion.puesto_id == fila.fila[0].value) {
                            if (parseInt(this.state.contenidoEdicion.puesto_id) == 10) {
                                this.setState({ banpuesto: true })
                            }
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[3].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[3].value}</option>)
                        }
                    } else if (selector == "selectArea") {
                        if (this.state.contenidoEdicion.area_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }
                    } else if (selector == "selectRol") {
                        if (this.state.contenidoEdicion.rol_id == fila.fila[0].value) {
                            this.setState({homoClaveRol:fila.fila[1].value})
                            options.push(<option selected value={(fila.fila[0].value)+'-'+(fila.fila[1].value)}>{fila.fila[2].value}</option>)
                        } else {
                            options.push(<option value={(fila.fila[0].value)+'-'+(fila.fila[1].value)}>{fila.fila[2].value}</option>)
                        }
                    } else if (selector == "selectProfesion") {
                        if (fila.fila[2].value != 0) {
                            if (this.state.contenidoEdicion.profesion_id == fila.fila[0].value) {
                                options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            } else {
                                options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            }
                        }
                    } else if (selector == "selectBancos") {
                        if (fila.fila[2].value != 0) {
                            if (this.state.contenidoEdicion.banco_id == fila.fila[0].value) {
                                options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            } else {
                                options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            }
                        }
                    } else if (selector == "selectCausasBaja") {
                        if (fila.fila[2].value != 0) {
                            if (this.state.contenidoEdicion.causabaja_id == fila.fila[0].value) {
                                options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            } else {
                                options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                            }
                        }
                    } else if (selector == "selectEstatusBaja") {
                        if (this.state.contenidoEdicion.estatusbaja_id == fila.fila[0].value) {
                            options.push(<option selected value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        } else {
                            options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                        }

                    }



                }
                if (selector == "selectCausasBaja") {
                    let salida = []
                    salida.push(<select disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                        && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name={selector} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
                    this.setState({ [selector]: salida })
                } else {
                    let salida = []
                    salida.push(<select disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                        && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name={selector} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
                    this.setState({ [selector]: salida })
                }
            });
    }



    UNSAFE_componentWillMount() {
        console.log("component segundo form", this.props)
        /*
 
         this.setState({ contenidoEdicion: this.props.contenidoEdicion })*/

        fetch(datos.urlServicePy+"parametros/api_cat_empleados/" + this.props.idEmpleado)
            .then(response => response.json())
            .then(json => {
                let fila = json.filas[0].fila
                let columnas = json.columnas
                let columnasSalida = {}
                for (var i = 0; i < fila.length; i++) {
                    console.log(fila[i])
                    columnasSalida["" + columnas[i].key + ""] = fila[i].value
                }
                /* armando select del supervisor */
                let options = []
                if (columnasSalida.supervisor == "ASESOR SR") {
                    options.push(<option selected value={"ASESOR SR"}>{"ASESOR SR"}</option>)
                    options.push(<option value={"CONSEJO DE ADMINISTRACIÓN"}>{"CONSEJO DE ADMINISTRACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE ADMINISTRACIÓN "}>{"COORDINADOR DE ADMINISTRACIÓN "}</option>)
                    options.push(<option value={"COORDINADOR DE CAPACITACIÓN"}>{"COORDINADOR DE CAPACITACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE SISTEMAS"}>{"COORDINADOR DE SISTEMAS"}</option>)
                    options.push(<option value={"DIRECTOR DE OPERACIONES"}>{"DIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"DIRECTOR GENERAL"}>{"DIRECTOR GENERAL"}</option>)
                    options.push(<option value={"GERENTE DE ADMINISTRACIÓN Y RRHH"}>{"GERENTE DE ADMINISTRACIÓN Y RRHH"}</option>)
                    options.push(<option value={"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}>{"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}</option>)
                    options.push(<option value={"GERENTE DE COBRANZA"}>{"GERENTE DE COBRANZA"}</option>)
                    options.push(<option value={"GERENTE DE EMISIÓN"}>{"GERENTE DE EMISIÓN"}</option>)
                    options.push(<option value={"GERENTE DE PLANEACIÓN ESTRATÉGICA"}>{"GERENTE DE PLANEACIÓN ESTRATÉGICA"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN GMM"}>{"GERENTE DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN PATRIMONIALES"}>{"GERENTE DE PROMOCIÓN PATRIMONIALES"}</option>)
                    options.push(<option value={"GERENTE DE RECLUTAMIENTO"}>{"GERENTE DE RECLUTAMIENTO"}</option>)
                    options.push(<option value={"GERENTE DE SINIESTROS"}>{"GERENTE DE SINIESTROS"}</option>)
                    options.push(<option value={"GERENTE PROMOCIÓN GMM NOVELES"}>{"GERENTE PROMOCIÓN GMM NOVELES"}</option>)
                    options.push(<option value={"SUBDIRECTOR COMERCIAL DE DESARROLLO"}>{"SUBDIRECTOR COMERCIAL DE DESARROLLO"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE OPERACIONES"}>{"SUBDIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN GMM"}>{"SUBDIRECTOR DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN PYMES"}>{"SUBDIRECTOR DE PROMOCIÓN PYMES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN VIDA"}>{"SUBDIRECTOR DE PROMOCIÓN VIDA"}</option>)
                } else if (columnasSalida.supervisor == "CONSEJO DE ADMINISTRACIÓN") {
                    options.push(<option value={"ASESOR SR"}>{"ASESOR SR"}</option>)
                    options.push(<option selected value={"CONSEJO DE ADMINISTRACIÓN"}>{"CONSEJO DE ADMINISTRACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE ADMINISTRACIÓN "}>{"COORDINADOR DE ADMINISTRACIÓN "}</option>)
                    options.push(<option value={"COORDINADOR DE CAPACITACIÓN"}>{"COORDINADOR DE CAPACITACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE SISTEMAS"}>{"COORDINADOR DE SISTEMAS"}</option>)
                    options.push(<option value={"DIRECTOR DE OPERACIONES"}>{"DIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"DIRECTOR GENERAL"}>{"DIRECTOR GENERAL"}</option>)
                    options.push(<option value={"GERENTE DE ADMINISTRACIÓN Y RRHH"}>{"GERENTE DE ADMINISTRACIÓN Y RRHH"}</option>)
                    options.push(<option value={"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}>{"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}</option>)
                    options.push(<option value={"GERENTE DE COBRANZA"}>{"GERENTE DE COBRANZA"}</option>)
                    options.push(<option value={"GERENTE DE EMISIÓN"}>{"GERENTE DE EMISIÓN"}</option>)
                    options.push(<option value={"GERENTE DE PLANEACIÓN ESTRATÉGICA"}>{"GERENTE DE PLANEACIÓN ESTRATÉGICA"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN GMM"}>{"GERENTE DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN PATRIMONIALES"}>{"GERENTE DE PROMOCIÓN PATRIMONIALES"}</option>)
                    options.push(<option value={"GERENTE DE RECLUTAMIENTO"}>{"GERENTE DE RECLUTAMIENTO"}</option>)
                    options.push(<option value={"GERENTE DE SINIESTROS"}>{"GERENTE DE SINIESTROS"}</option>)
                    options.push(<option value={"GERENTE PROMOCIÓN GMM NOVELES"}>{"GERENTE PROMOCIÓN GMM NOVELES"}</option>)
                    options.push(<option value={"SUBDIRECTOR COMERCIAL DE DESARROLLO"}>{"SUBDIRECTOR COMERCIAL DE DESARROLLO"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE OPERACIONES"}>{"SUBDIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN GMM"}>{"SUBDIRECTOR DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN PYMES"}>{"SUBDIRECTOR DE PROMOCIÓN PYMES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN VIDA"}>{"SUBDIRECTOR DE PROMOCIÓN VIDA"}</option>)
                }  else if (columnasSalida.supervisor == "COORDINADOR DE ADMINISTRACIÓN") {
                    options.push(<option value={"ASESOR SR"}>{"ASESOR SR"}</option>)
                    options.push(<option value={"CONSEJO DE ADMINISTRACIÓN"}>{"CONSEJO DE ADMINISTRACIÓN"}</option>)
                    options.push(<option selected value={"COORDINADOR DE ADMINISTRACIÓN "}>{"COORDINADOR DE ADMINISTRACIÓN "}</option>)
                    options.push(<option value={"COORDINADOR DE CAPACITACIÓN"}>{"COORDINADOR DE CAPACITACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE SISTEMAS"}>{"COORDINADOR DE SISTEMAS"}</option>)
                    options.push(<option value={"DIRECTOR DE OPERACIONES"}>{"DIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"DIRECTOR GENERAL"}>{"DIRECTOR GENERAL"}</option>)
                    options.push(<option value={"GERENTE DE ADMINISTRACIÓN Y RRHH"}>{"GERENTE DE ADMINISTRACIÓN Y RRHH"}</option>)
                    options.push(<option value={"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}>{"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}</option>)
                    options.push(<option value={"GERENTE DE COBRANZA"}>{"GERENTE DE COBRANZA"}</option>)
                    options.push(<option value={"GERENTE DE EMISIÓN"}>{"GERENTE DE EMISIÓN"}</option>)
                    options.push(<option value={"GERENTE DE PLANEACIÓN ESTRATÉGICA"}>{"GERENTE DE PLANEACIÓN ESTRATÉGICA"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN GMM"}>{"GERENTE DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN PATRIMONIALES"}>{"GERENTE DE PROMOCIÓN PATRIMONIALES"}</option>)
                    options.push(<option value={"GERENTE DE RECLUTAMIENTO"}>{"GERENTE DE RECLUTAMIENTO"}</option>)
                    options.push(<option value={"GERENTE DE SINIESTROS"}>{"GERENTE DE SINIESTROS"}</option>)
                    options.push(<option value={"GERENTE PROMOCIÓN GMM NOVELES"}>{"GERENTE PROMOCIÓN GMM NOVELES"}</option>)
                    options.push(<option value={"SUBDIRECTOR COMERCIAL DE DESARROLLO"}>{"SUBDIRECTOR COMERCIAL DE DESARROLLO"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE OPERACIONES"}>{"SUBDIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN GMM"}>{"SUBDIRECTOR DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN PYMES"}>{"SUBDIRECTOR DE PROMOCIÓN PYMES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN VIDA"}>{"SUBDIRECTOR DE PROMOCIÓN VIDA"}</option>)
                }  else if (columnasSalida.supervisor == "COORDINADOR DE CAPACITACIÓN") {
                    options.push(<option value={"ASESOR SR"}>{"ASESOR SR"}</option>)
                    options.push(<option value={"CONSEJO DE ADMINISTRACIÓN"}>{"CONSEJO DE ADMINISTRACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE ADMINISTRACIÓN "}>{"COORDINADOR DE ADMINISTRACIÓN "}</option>)
                    options.push(<option selected value={"COORDINADOR DE CAPACITACIÓN"}>{"COORDINADOR DE CAPACITACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE SISTEMAS"}>{"COORDINADOR DE SISTEMAS"}</option>)
                    options.push(<option value={"DIRECTOR DE OPERACIONES"}>{"DIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"DIRECTOR GENERAL"}>{"DIRECTOR GENERAL"}</option>)
                    options.push(<option value={"GERENTE DE ADMINISTRACIÓN Y RRHH"}>{"GERENTE DE ADMINISTRACIÓN Y RRHH"}</option>)
                    options.push(<option value={"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}>{"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}</option>)
                    options.push(<option value={"GERENTE DE COBRANZA"}>{"GERENTE DE COBRANZA"}</option>)
                    options.push(<option value={"GERENTE DE EMISIÓN"}>{"GERENTE DE EMISIÓN"}</option>)
                    options.push(<option value={"GERENTE DE PLANEACIÓN ESTRATÉGICA"}>{"GERENTE DE PLANEACIÓN ESTRATÉGICA"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN GMM"}>{"GERENTE DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN PATRIMONIALES"}>{"GERENTE DE PROMOCIÓN PATRIMONIALES"}</option>)
                    options.push(<option value={"GERENTE DE RECLUTAMIENTO"}>{"GERENTE DE RECLUTAMIENTO"}</option>)
                    options.push(<option value={"GERENTE DE SINIESTROS"}>{"GERENTE DE SINIESTROS"}</option>)
                    options.push(<option value={"GERENTE PROMOCIÓN GMM NOVELES"}>{"GERENTE PROMOCIÓN GMM NOVELES"}</option>)
                    options.push(<option value={"SUBDIRECTOR COMERCIAL DE DESARROLLO"}>{"SUBDIRECTOR COMERCIAL DE DESARROLLO"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE OPERACIONES"}>{"SUBDIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN GMM"}>{"SUBDIRECTOR DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN PYMES"}>{"SUBDIRECTOR DE PROMOCIÓN PYMES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN VIDA"}>{"SUBDIRECTOR DE PROMOCIÓN VIDA"}</option>)
                }  else if (columnasSalida.supervisor == "COORDINADOR DE SISTEMAS") {
                    options.push(<option value={"ASESOR SR"}>{"ASESOR SR"}</option>)
                    options.push(<option value={"CONSEJO DE ADMINISTRACIÓN"}>{"CONSEJO DE ADMINISTRACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE ADMINISTRACIÓN "}>{"COORDINADOR DE ADMINISTRACIÓN "}</option>)
                    options.push(<option value={"COORDINADOR DE CAPACITACIÓN"}>{"COORDINADOR DE CAPACITACIÓN"}</option>)
                    options.push(<option selected value={"COORDINADOR DE SISTEMAS"}>{"COORDINADOR DE SISTEMAS"}</option>)
                    options.push(<option value={"DIRECTOR DE OPERACIONES"}>{"DIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"DIRECTOR GENERAL"}>{"DIRECTOR GENERAL"}</option>)
                    options.push(<option value={"GERENTE DE ADMINISTRACIÓN Y RRHH"}>{"GERENTE DE ADMINISTRACIÓN Y RRHH"}</option>)
                    options.push(<option value={"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}>{"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}</option>)
                    options.push(<option value={"GERENTE DE COBRANZA"}>{"GERENTE DE COBRANZA"}</option>)
                    options.push(<option value={"GERENTE DE EMISIÓN"}>{"GERENTE DE EMISIÓN"}</option>)
                    options.push(<option value={"GERENTE DE PLANEACIÓN ESTRATÉGICA"}>{"GERENTE DE PLANEACIÓN ESTRATÉGICA"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN GMM"}>{"GERENTE DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN PATRIMONIALES"}>{"GERENTE DE PROMOCIÓN PATRIMONIALES"}</option>)
                    options.push(<option value={"GERENTE DE RECLUTAMIENTO"}>{"GERENTE DE RECLUTAMIENTO"}</option>)
                    options.push(<option value={"GERENTE DE SINIESTROS"}>{"GERENTE DE SINIESTROS"}</option>)
                    options.push(<option value={"GERENTE PROMOCIÓN GMM NOVELES"}>{"GERENTE PROMOCIÓN GMM NOVELES"}</option>)
                    options.push(<option value={"SUBDIRECTOR COMERCIAL DE DESARROLLO"}>{"SUBDIRECTOR COMERCIAL DE DESARROLLO"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE OPERACIONES"}>{"SUBDIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN GMM"}>{"SUBDIRECTOR DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN PYMES"}>{"SUBDIRECTOR DE PROMOCIÓN PYMES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN VIDA"}>{"SUBDIRECTOR DE PROMOCIÓN VIDA"}</option>)
                }  else if (columnasSalida.supervisor == "DIRECTOR DE OPERACIONES") {
                    options.push(<option value={"ASESOR SR"}>{"ASESOR SR"}</option>)
                    options.push(<option value={"CONSEJO DE ADMINISTRACIÓN"}>{"CONSEJO DE ADMINISTRACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE ADMINISTRACIÓN "}>{"COORDINADOR DE ADMINISTRACIÓN "}</option>)
                    options.push(<option value={"COORDINADOR DE CAPACITACIÓN"}>{"COORDINADOR DE CAPACITACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE SISTEMAS"}>{"COORDINADOR DE SISTEMAS"}</option>)
                    options.push(<option selected value={"DIRECTOR DE OPERACIONES"}>{"DIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"DIRECTOR GENERAL"}>{"DIRECTOR GENERAL"}</option>)
                    options.push(<option value={"GERENTE DE ADMINISTRACIÓN Y RRHH"}>{"GERENTE DE ADMINISTRACIÓN Y RRHH"}</option>)
                    options.push(<option value={"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}>{"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}</option>)
                    options.push(<option value={"GERENTE DE COBRANZA"}>{"GERENTE DE COBRANZA"}</option>)
                    options.push(<option value={"GERENTE DE EMISIÓN"}>{"GERENTE DE EMISIÓN"}</option>)
                    options.push(<option value={"GERENTE DE PLANEACIÓN ESTRATÉGICA"}>{"GERENTE DE PLANEACIÓN ESTRATÉGICA"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN GMM"}>{"GERENTE DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN PATRIMONIALES"}>{"GERENTE DE PROMOCIÓN PATRIMONIALES"}</option>)
                    options.push(<option value={"GERENTE DE RECLUTAMIENTO"}>{"GERENTE DE RECLUTAMIENTO"}</option>)
                    options.push(<option value={"GERENTE DE SINIESTROS"}>{"GERENTE DE SINIESTROS"}</option>)
                    options.push(<option value={"GERENTE PROMOCIÓN GMM NOVELES"}>{"GERENTE PROMOCIÓN GMM NOVELES"}</option>)
                    options.push(<option value={"SUBDIRECTOR COMERCIAL DE DESARROLLO"}>{"SUBDIRECTOR COMERCIAL DE DESARROLLO"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE OPERACIONES"}>{"SUBDIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN GMM"}>{"SUBDIRECTOR DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN PYMES"}>{"SUBDIRECTOR DE PROMOCIÓN PYMES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN VIDA"}>{"SUBDIRECTOR DE PROMOCIÓN VIDA"}</option>)
                }  else if (columnasSalida.supervisor == "DIRECTOR GENERAL") {
                    options.push(<option value={"ASESOR SR"}>{"ASESOR SR"}</option>)
                    options.push(<option value={"CONSEJO DE ADMINISTRACIÓN"}>{"CONSEJO DE ADMINISTRACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE ADMINISTRACIÓN "}>{"COORDINADOR DE ADMINISTRACIÓN "}</option>)
                    options.push(<option value={"COORDINADOR DE CAPACITACIÓN"}>{"COORDINADOR DE CAPACITACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE SISTEMAS"}>{"COORDINADOR DE SISTEMAS"}</option>)
                    options.push(<option value={"DIRECTOR DE OPERACIONES"}>{"DIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option selected value={"DIRECTOR GENERAL"}>{"DIRECTOR GENERAL"}</option>)
                    options.push(<option value={"GERENTE DE ADMINISTRACIÓN Y RRHH"}>{"GERENTE DE ADMINISTRACIÓN Y RRHH"}</option>)
                    options.push(<option value={"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}>{"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}</option>)
                    options.push(<option value={"GERENTE DE COBRANZA"}>{"GERENTE DE COBRANZA"}</option>)
                    options.push(<option value={"GERENTE DE EMISIÓN"}>{"GERENTE DE EMISIÓN"}</option>)
                    options.push(<option value={"GERENTE DE PLANEACIÓN ESTRATÉGICA"}>{"GERENTE DE PLANEACIÓN ESTRATÉGICA"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN GMM"}>{"GERENTE DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN PATRIMONIALES"}>{"GERENTE DE PROMOCIÓN PATRIMONIALES"}</option>)
                    options.push(<option value={"GERENTE DE RECLUTAMIENTO"}>{"GERENTE DE RECLUTAMIENTO"}</option>)
                    options.push(<option value={"GERENTE DE SINIESTROS"}>{"GERENTE DE SINIESTROS"}</option>)
                    options.push(<option value={"GERENTE PROMOCIÓN GMM NOVELES"}>{"GERENTE PROMOCIÓN GMM NOVELES"}</option>)
                    options.push(<option value={"SUBDIRECTOR COMERCIAL DE DESARROLLO"}>{"SUBDIRECTOR COMERCIAL DE DESARROLLO"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE OPERACIONES"}>{"SUBDIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN GMM"}>{"SUBDIRECTOR DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN PYMES"}>{"SUBDIRECTOR DE PROMOCIÓN PYMES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN VIDA"}>{"SUBDIRECTOR DE PROMOCIÓN VIDA"}</option>)
                }  else if (columnasSalida.supervisor == "GERENTE DE ADMINISTRACIÓN Y RRHH") {
                    options.push(<option value={"ASESOR SR"}>{"ASESOR SR"}</option>)
                    options.push(<option value={"CONSEJO DE ADMINISTRACIÓN"}>{"CONSEJO DE ADMINISTRACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE ADMINISTRACIÓN "}>{"COORDINADOR DE ADMINISTRACIÓN "}</option>)
                    options.push(<option value={"COORDINADOR DE CAPACITACIÓN"}>{"COORDINADOR DE CAPACITACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE SISTEMAS"}>{"COORDINADOR DE SISTEMAS"}</option>)
                    options.push(<option value={"DIRECTOR DE OPERACIONES"}>{"DIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"DIRECTOR GENERAL"}>{"DIRECTOR GENERAL"}</option>)
                    options.push(<option selected value={"GERENTE DE ADMINISTRACIÓN Y RRHH"}>{"GERENTE DE ADMINISTRACIÓN Y RRHH"}</option>)
                    options.push(<option value={"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}>{"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}</option>)
                    options.push(<option value={"GERENTE DE COBRANZA"}>{"GERENTE DE COBRANZA"}</option>)
                    options.push(<option value={"GERENTE DE EMISIÓN"}>{"GERENTE DE EMISIÓN"}</option>)
                    options.push(<option value={"GERENTE DE PLANEACIÓN ESTRATÉGICA"}>{"GERENTE DE PLANEACIÓN ESTRATÉGICA"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN GMM"}>{"GERENTE DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN PATRIMONIALES"}>{"GERENTE DE PROMOCIÓN PATRIMONIALES"}</option>)
                    options.push(<option value={"GERENTE DE RECLUTAMIENTO"}>{"GERENTE DE RECLUTAMIENTO"}</option>)
                    options.push(<option value={"GERENTE DE SINIESTROS"}>{"GERENTE DE SINIESTROS"}</option>)
                    options.push(<option value={"GERENTE PROMOCIÓN GMM NOVELES"}>{"GERENTE PROMOCIÓN GMM NOVELES"}</option>)
                    options.push(<option value={"SUBDIRECTOR COMERCIAL DE DESARROLLO"}>{"SUBDIRECTOR COMERCIAL DE DESARROLLO"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE OPERACIONES"}>{"SUBDIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN GMM"}>{"SUBDIRECTOR DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN PYMES"}>{"SUBDIRECTOR DE PROMOCIÓN PYMES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN VIDA"}>{"SUBDIRECTOR DE PROMOCIÓN VIDA"}</option>)
                }  else if (columnasSalida.supervisor == "GERENTE DE CAPACITACIÓN Y COMUNICACIÓN") {
                    options.push(<option value={"ASESOR SR"}>{"ASESOR SR"}</option>)
                    options.push(<option value={"CONSEJO DE ADMINISTRACIÓN"}>{"CONSEJO DE ADMINISTRACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE ADMINISTRACIÓN "}>{"COORDINADOR DE ADMINISTRACIÓN "}</option>)
                    options.push(<option value={"COORDINADOR DE CAPACITACIÓN"}>{"COORDINADOR DE CAPACITACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE SISTEMAS"}>{"COORDINADOR DE SISTEMAS"}</option>)
                    options.push(<option value={"DIRECTOR DE OPERACIONES"}>{"DIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"DIRECTOR GENERAL"}>{"DIRECTOR GENERAL"}</option>)
                    options.push(<option value={"GERENTE DE ADMINISTRACIÓN Y RRHH"}>{"GERENTE DE ADMINISTRACIÓN Y RRHH"}</option>)
                    options.push(<option selected value={"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}>{"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}</option>)
                    options.push(<option value={"GERENTE DE COBRANZA"}>{"GERENTE DE COBRANZA"}</option>)
                    options.push(<option value={"GERENTE DE EMISIÓN"}>{"GERENTE DE EMISIÓN"}</option>)
                    options.push(<option value={"GERENTE DE PLANEACIÓN ESTRATÉGICA"}>{"GERENTE DE PLANEACIÓN ESTRATÉGICA"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN GMM"}>{"GERENTE DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN PATRIMONIALES"}>{"GERENTE DE PROMOCIÓN PATRIMONIALES"}</option>)
                    options.push(<option value={"GERENTE DE RECLUTAMIENTO"}>{"GERENTE DE RECLUTAMIENTO"}</option>)
                    options.push(<option value={"GERENTE DE SINIESTROS"}>{"GERENTE DE SINIESTROS"}</option>)
                    options.push(<option value={"GERENTE PROMOCIÓN GMM NOVELES"}>{"GERENTE PROMOCIÓN GMM NOVELES"}</option>)
                    options.push(<option value={"SUBDIRECTOR COMERCIAL DE DESARROLLO"}>{"SUBDIRECTOR COMERCIAL DE DESARROLLO"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE OPERACIONES"}>{"SUBDIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN GMM"}>{"SUBDIRECTOR DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN PYMES"}>{"SUBDIRECTOR DE PROMOCIÓN PYMES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN VIDA"}>{"SUBDIRECTOR DE PROMOCIÓN VIDA"}</option>)
                }  else if (columnasSalida.supervisor == "GERENTE DE COBRANZA") {
                    options.push(<option value={"ASESOR SR"}>{"ASESOR SR"}</option>)
                    options.push(<option value={"CONSEJO DE ADMINISTRACIÓN"}>{"CONSEJO DE ADMINISTRACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE ADMINISTRACIÓN "}>{"COORDINADOR DE ADMINISTRACIÓN "}</option>)
                    options.push(<option value={"COORDINADOR DE CAPACITACIÓN"}>{"COORDINADOR DE CAPACITACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE SISTEMAS"}>{"COORDINADOR DE SISTEMAS"}</option>)
                    options.push(<option value={"DIRECTOR DE OPERACIONES"}>{"DIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"DIRECTOR GENERAL"}>{"DIRECTOR GENERAL"}</option>)
                    options.push(<option value={"GERENTE DE ADMINISTRACIÓN Y RRHH"}>{"GERENTE DE ADMINISTRACIÓN Y RRHH"}</option>)
                    options.push(<option value={" GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}>{" GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}</option>)
                    options.push(<option selected  value={"GERENTE DE COBRANZA"}>{"GERENTE DE COBRANZA"}</option>)
                    options.push(<option value={"GERENTE DE EMISIÓN"}>{"GERENTE DE EMISIÓN"}</option>)
                    options.push(<option value={"GERENTE DE PLANEACIÓN ESTRATÉGICA"}>{"GERENTE DE PLANEACIÓN ESTRATÉGICA"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN GMM"}>{"GERENTE DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN PATRIMONIALES"}>{"GERENTE DE PROMOCIÓN PATRIMONIALES"}</option>)
                    options.push(<option value={"GERENTE DE RECLUTAMIENTO"}>{"GERENTE DE RECLUTAMIENTO"}</option>)
                    options.push(<option value={"GERENTE DE SINIESTROS"}>{"GERENTE DE SINIESTROS"}</option>)
                    options.push(<option value={"GERENTE PROMOCIÓN GMM NOVELES"}>{"GERENTE PROMOCIÓN GMM NOVELES"}</option>)
                    options.push(<option value={"SUBDIRECTOR COMERCIAL DE DESARROLLO"}>{"SUBDIRECTOR COMERCIAL DE DESARROLLO"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE OPERACIONES"}>{"SUBDIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN GMM"}>{"SUBDIRECTOR DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN PYMES"}>{"SUBDIRECTOR DE PROMOCIÓN PYMES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN VIDA"}>{"SUBDIRECTOR DE PROMOCIÓN VIDA"}</option>)
                } else if (columnasSalida.supervisor == "GERENTE DE EMISIÓN") {
                    options.push(<option value={"ASESOR SR"}>{"ASESOR SR"}</option>)
                    options.push(<option value={"CONSEJO DE ADMINISTRACIÓN"}>{"CONSEJO DE ADMINISTRACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE ADMINISTRACIÓN "}>{"COORDINADOR DE ADMINISTRACIÓN "}</option>)
                    options.push(<option value={"COORDINADOR DE CAPACITACIÓN"}>{"COORDINADOR DE CAPACITACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE SISTEMAS"}>{"COORDINADOR DE SISTEMAS"}</option>)
                    options.push(<option value={"DIRECTOR DE OPERACIONES"}>{"DIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"DIRECTOR GENERAL"}>{"DIRECTOR GENERAL"}</option>)
                    options.push(<option value={"GERENTE DE ADMINISTRACIÓN Y RRHH"}>{"GERENTE DE ADMINISTRACIÓN Y RRHH"}</option>)
                    options.push(<option value={"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}>{"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}</option>)
                    options.push(<option value={"GERENTE DE COBRANZA"}>{"GERENTE DE COBRANZA"}</option>)
                    options.push(<option selected   value={"GERENTE DE EMISIÓN"}>{"GERENTE DE EMISIÓN"}</option>)
                    options.push(<option value={"GERENTE DE PLANEACIÓN ESTRATÉGICA"}>{"GERENTE DE PLANEACIÓN ESTRATÉGICA"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN GMM"}>{"GERENTE DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN PATRIMONIALES"}>{"GERENTE DE PROMOCIÓN PATRIMONIALES"}</option>)
                    options.push(<option value={"GERENTE DE RECLUTAMIENTO"}>{"GERENTE DE RECLUTAMIENTO"}</option>)
                    options.push(<option value={"GERENTE DE SINIESTROS"}>{"GERENTE DE SINIESTROS"}</option>)
                    options.push(<option value={"GERENTE PROMOCIÓN GMM NOVELES"}>{"GERENTE PROMOCIÓN GMM NOVELES"}</option>)
                    options.push(<option value={"SUBDIRECTOR COMERCIAL DE DESARROLLO"}>{"SUBDIRECTOR COMERCIAL DE DESARROLLO"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE OPERACIONES"}>{"SUBDIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN GMM"}>{"SUBDIRECTOR DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN PYMES"}>{"SUBDIRECTOR DE PROMOCIÓN PYMES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN VIDA"}>{"SUBDIRECTOR DE PROMOCIÓN VIDA"}</option>)
                }   else if (columnasSalida.supervisor == "GERENTE DE PLANEACIÓN ESTRATÉGICA") {
                    options.push(<option value={"ASESOR SR"}>{"ASESOR SR"}</option>)
                    options.push(<option value={"CONSEJO DE ADMINISTRACIÓN"}>{"CONSEJO DE ADMINISTRACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE ADMINISTRACIÓN "}>{"COORDINADOR DE ADMINISTRACIÓN "}</option>)
                    options.push(<option value={"COORDINADOR DE CAPACITACIÓN"}>{"COORDINADOR DE CAPACITACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE SISTEMAS"}>{"COORDINADOR DE SISTEMAS"}</option>)
                    options.push(<option value={"DIRECTOR DE OPERACIONES"}>{"DIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"DIRECTOR GENERAL"}>{"DIRECTOR GENERAL"}</option>)
                    options.push(<option value={"GERENTE DE ADMINISTRACIÓN Y RRHH"}>{"GERENTE DE ADMINISTRACIÓN Y RRHH"}</option>)
                    options.push(<option value={"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}>{"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}</option>)
                    options.push(<option value={"GERENTE DE COBRANZA"}>{"GERENTE DE COBRANZA"}</option>)
                    options.push(<option value={"GERENTE DE EMISIÓN"}>{"GERENTE DE EMISIÓN"}</option>)
                    options.push(<option selected value={"GERENTE DE PLANEACIÓN ESTRATÉGICA"}>{"GERENTE DE PLANEACIÓN ESTRATÉGICA"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN GMM"}>{"GERENTE DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN PATRIMONIALES"}>{"GERENTE DE PROMOCIÓN PATRIMONIALES"}</option>)
                    options.push(<option value={"GERENTE DE RECLUTAMIENTO"}>{"GERENTE DE RECLUTAMIENTO"}</option>)
                    options.push(<option value={"GERENTE DE SINIESTROS"}>{"GERENTE DE SINIESTROS"}</option>)
                    options.push(<option value={"GERENTE PROMOCIÓN GMM NOVELES"}>{"GERENTE PROMOCIÓN GMM NOVELES"}</option>)
                    options.push(<option value={"SUBDIRECTOR COMERCIAL DE DESARROLLO"}>{"SUBDIRECTOR COMERCIAL DE DESARROLLO"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE OPERACIONES"}>{"SUBDIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN GMM"}>{"SUBDIRECTOR DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN PYMES"}>{"SUBDIRECTOR DE PROMOCIÓN PYMES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN VIDA"}>{"SUBDIRECTOR DE PROMOCIÓN VIDA"}</option>)
                }   else if (columnasSalida.supervisor == "GERENTE DE PROMOCIÓN GMM") {
                    options.push(<option value={"ASESOR SR"}>{"ASESOR SR"}</option>)
                    options.push(<option value={"CONSEJO DE ADMINISTRACIÓN"}>{"CONSEJO DE ADMINISTRACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE ADMINISTRACIÓN "}>{"COORDINADOR DE ADMINISTRACIÓN "}</option>)
                    options.push(<option value={"COORDINADOR DE CAPACITACIÓN"}>{"COORDINADOR DE CAPACITACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE SISTEMAS"}>{"COORDINADOR DE SISTEMAS"}</option>)
                    options.push(<option value={"DIRECTOR DE OPERACIONES"}>{"DIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"DIRECTOR GENERAL"}>{"DIRECTOR GENERAL"}</option>)
                    options.push(<option value={"GERENTE DE ADMINISTRACIÓN Y RRHH"}>{"GERENTE DE ADMINISTRACIÓN Y RRHH"}</option>)
                    options.push(<option value={"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}>{"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}</option>)
                    options.push(<option value={"GERENTE DE COBRANZA"}>{"GERENTE DE COBRANZA"}</option>)
                    options.push(<option value={"GERENTE DE EMISIÓN"}>{"GERENTE DE EMISIÓN"}</option>)
                    options.push(<option value={"GERENTE DE PLANEACIÓN ESTRATÉGICA"}>{"GERENTE DE PLANEACIÓN ESTRATÉGICA"}</option>)
                    options.push(<option selected value={"GERENTE DE PROMOCIÓN GMM"}>{"GERENTE DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN PATRIMONIALES"}>{"GERENTE DE PROMOCIÓN PATRIMONIALES"}</option>)
                    options.push(<option value={"GERENTE DE RECLUTAMIENTO"}>{"GERENTE DE RECLUTAMIENTO"}</option>)
                    options.push(<option value={"GERENTE DE SINIESTROS"}>{"GERENTE DE SINIESTROS"}</option>)
                    options.push(<option value={"GERENTE PROMOCIÓN GMM NOVELES"}>{"GERENTE PROMOCIÓN GMM NOVELES"}</option>)
                    options.push(<option value={"SUBDIRECTOR COMERCIAL DE DESARROLLO"}>{"SUBDIRECTOR COMERCIAL DE DESARROLLO"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE OPERACIONES"}>{"SUBDIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN GMM"}>{"SUBDIRECTOR DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN PYMES"}>{"SUBDIRECTOR DE PROMOCIÓN PYMES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN VIDA"}>{"SUBDIRECTOR DE PROMOCIÓN VIDA"}</option>)
                }   else if (columnasSalida.supervisor == "GERENTE DE PROMOCIÓN PATRIMONIALES") {
                    options.push(<option value={"ASESOR SR"}>{"ASESOR SR"}</option>)
                    options.push(<option value={"CONSEJO DE ADMINISTRACIÓN"}>{"CONSEJO DE ADMINISTRACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE ADMINISTRACIÓN "}>{"COORDINADOR DE ADMINISTRACIÓN "}</option>)
                    options.push(<option value={"COORDINADOR DE CAPACITACIÓN"}>{"COORDINADOR DE CAPACITACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE SISTEMAS"}>{"COORDINADOR DE SISTEMAS"}</option>)
                    options.push(<option value={"DIRECTOR DE OPERACIONES"}>{"DIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"DIRECTOR GENERAL"}>{"DIRECTOR GENERAL"}</option>)
                    options.push(<option value={"GERENTE DE ADMINISTRACIÓN Y RRHH"}>{"GERENTE DE ADMINISTRACIÓN Y RRHH"}</option>)
                    options.push(<option value={"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}>{"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}</option>)
                    options.push(<option value={"GERENTE DE COBRANZA"}>{"GERENTE DE COBRANZA"}</option>)
                    options.push(<option value={"GERENTE DE EMISIÓN"}>{"GERENTE DE EMISIÓN"}</option>)
                    options.push(<option value={"GERENTE DE PLANEACIÓN ESTRATÉGICA"}>{"GERENTE DE PLANEACIÓN ESTRATÉGICA"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN GMM"}>{"GERENTE DE PROMOCIÓN GMM"}</option>)
                    options.push(<option selected value={"GERENTE DE PROMOCIÓN PATRIMONIALES"}>{"GERENTE DE PROMOCIÓN PATRIMONIALES"}</option>)
                    options.push(<option value={"GERENTE DE RECLUTAMIENTO"}>{"GERENTE DE RECLUTAMIENTO"}</option>)
                    options.push(<option value={"GERENTE DE SINIESTROS"}>{"GERENTE DE SINIESTROS"}</option>)
                    options.push(<option value={"GERENTE PROMOCIÓN GMM NOVELES"}>{"GERENTE PROMOCIÓN GMM NOVELES"}</option>)
                    options.push(<option value={"SUBDIRECTOR COMERCIAL DE DESARROLLO"}>{"SUBDIRECTOR COMERCIAL DE DESARROLLO"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE OPERACIONES"}>{"SUBDIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN GMM"}>{"SUBDIRECTOR DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN PYMES"}>{"SUBDIRECTOR DE PROMOCIÓN PYMES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN VIDA"}>{"SUBDIRECTOR DE PROMOCIÓN VIDA"}</option>)
                }   else if (columnasSalida.supervisor == "GERENTE DE RECLUTAMIENTO") {
                    options.push(<option value={"ASESOR SR"}>{"ASESOR SR"}</option>)
                    options.push(<option value={"CONSEJO DE ADMINISTRACIÓN"}>{"CONSEJO DE ADMINISTRACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE ADMINISTRACIÓN "}>{"COORDINADOR DE ADMINISTRACIÓN "}</option>)
                    options.push(<option value={"COORDINADOR DE CAPACITACIÓN"}>{"COORDINADOR DE CAPACITACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE SISTEMAS"}>{"COORDINADOR DE SISTEMAS"}</option>)
                    options.push(<option value={"DIRECTOR DE OPERACIONES"}>{"DIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"DIRECTOR GENERAL"}>{"DIRECTOR GENERAL"}</option>)
                    options.push(<option value={"GERENTE DE ADMINISTRACIÓN Y RRHH"}>{"GERENTE DE ADMINISTRACIÓN Y RRHH"}</option>)
                    options.push(<option value={"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}>{"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}</option>)
                    options.push(<option value={"GERENTE DE COBRANZA"}>{"GERENTE DE COBRANZA"}</option>)
                    options.push(<option value={"GERENTE DE EMISIÓN"}>{"GERENTE DE EMISIÓN"}</option>)
                    options.push(<option value={"GERENTE DE PLANEACIÓN ESTRATÉGICA"}>{"GERENTE DE PLANEACIÓN ESTRATÉGICA"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN GMM"}>{"GERENTE DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN PATRIMONIALES"}>{"GERENTE DE PROMOCIÓN PATRIMONIALES"}</option>)
                    options.push(<option selected value={"GERENTE DE RECLUTAMIENTO"}>{"GERENTE DE RECLUTAMIENTO"}</option>)
                    options.push(<option value={"GERENTE DE SINIESTROS"}>{"GERENTE DE SINIESTROS"}</option>)
                    options.push(<option value={"GERENTE PROMOCIÓN GMM NOVELES"}>{"GERENTE PROMOCIÓN GMM NOVELES"}</option>)
                    options.push(<option value={"SUBDIRECTOR COMERCIAL DE DESARROLLO"}>{"SUBDIRECTOR COMERCIAL DE DESARROLLO"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE OPERACIONES"}>{"SUBDIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN GMM"}>{"SUBDIRECTOR DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN PYMES"}>{"SUBDIRECTOR DE PROMOCIÓN PYMES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN VIDA"}>{"SUBDIRECTOR DE PROMOCIÓN VIDA"}</option>)
                }   else if (columnasSalida.supervisor == "GERENTE DE SINIESTROS") {
                    options.push(<option value={"ASESOR SR"}>{"ASESOR SR"}</option>)
                    options.push(<option value={"CONSEJO DE ADMINISTRACIÓN"}>{"CONSEJO DE ADMINISTRACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE ADMINISTRACIÓN "}>{"COORDINADOR DE ADMINISTRACIÓN "}</option>)
                    options.push(<option value={"COORDINADOR DE CAPACITACIÓN"}>{"COORDINADOR DE CAPACITACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE SISTEMAS"}>{"COORDINADOR DE SISTEMAS"}</option>)
                    options.push(<option value={"DIRECTOR DE OPERACIONES"}>{"DIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"DIRECTOR GENERAL"}>{"DIRECTOR GENERAL"}</option>)
                    options.push(<option value={"GERENTE DE ADMINISTRACIÓN Y RRHH"}>{"GERENTE DE ADMINISTRACIÓN Y RRHH"}</option>)
                    options.push(<option value={"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}>{"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}</option>)
                    options.push(<option value={"GERENTE DE COBRANZA"}>{"GERENTE DE COBRANZA"}</option>)
                    options.push(<option value={"GERENTE DE EMISIÓN"}>{"GERENTE DE EMISIÓN"}</option>)
                    options.push(<option value={"GERENTE DE PLANEACIÓN ESTRATÉGICA"}>{"GERENTE DE PLANEACIÓN ESTRATÉGICA"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN GMM"}>{"GERENTE DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN PATRIMONIALES"}>{"GERENTE DE PROMOCIÓN PATRIMONIALES"}</option>)
                    options.push(<option value={"GERENTE DE RECLUTAMIENTO"}>{"GERENTE DE RECLUTAMIENTO"}</option>)
                    options.push(<option selected value={"GERENTE DE SINIESTROS"}>{"GERENTE DE SINIESTROS"}</option>)
                    options.push(<option value={"GERENTE PROMOCIÓN GMM NOVELES"}>{"GERENTE PROMOCIÓN GMM NOVELES"}</option>)
                    options.push(<option value={"SUBDIRECTOR COMERCIAL DE DESARROLLO"}>{"SUBDIRECTOR COMERCIAL DE DESARROLLO"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE OPERACIONES"}>{"SUBDIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN GMM"}>{"SUBDIRECTOR DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN PYMES"}>{"SUBDIRECTOR DE PROMOCIÓN PYMES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN VIDA"}>{"SUBDIRECTOR DE PROMOCIÓN VIDA"}</option>)
                }   else if (columnasSalida.supervisor == "GERENTE PROMOCIÓN GMM NOVELES") {
                    options.push(<option value={"ASESOR SR"}>{"ASESOR SR"}</option>)
                    options.push(<option value={"CONSEJO DE ADMINISTRACIÓN"}>{"CONSEJO DE ADMINISTRACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE ADMINISTRACIÓN "}>{"COORDINADOR DE ADMINISTRACIÓN "}</option>)
                    options.push(<option value={"COORDINADOR DE CAPACITACIÓN"}>{"COORDINADOR DE CAPACITACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE SISTEMAS"}>{"COORDINADOR DE SISTEMAS"}</option>)
                    options.push(<option value={"DIRECTOR DE OPERACIONES"}>{"DIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"DIRECTOR GENERAL"}>{"DIRECTOR GENERAL"}</option>)
                    options.push(<option value={"GERENTE DE ADMINISTRACIÓN Y RRHH"}>{"GERENTE DE ADMINISTRACIÓN Y RRHH"}</option>)
                    options.push(<option value={"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}>{"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}</option>)
                    options.push(<option value={"GERENTE DE COBRANZA"}>{"GERENTE DE COBRANZA"}</option>)
                    options.push(<option value={"GERENTE DE EMISIÓN"}>{"GERENTE DE EMISIÓN"}</option>)
                    options.push(<option value={"GERENTE DE PLANEACIÓN ESTRATÉGICA"}>{"GERENTE DE PLANEACIÓN ESTRATÉGICA"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN GMM"}>{"GERENTE DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN PATRIMONIALES"}>{"GERENTE DE PROMOCIÓN PATRIMONIALES"}</option>)
                    options.push(<option value={"GERENTE DE RECLUTAMIENTO"}>{"GERENTE DE RECLUTAMIENTO"}</option>)
                    options.push(<option value={"GERENTE DE SINIESTROS"}>{"GERENTE DE SINIESTROS"}</option>)
                    options.push(<option selected value={"GERENTE PROMOCIÓN GMM NOVELES"}>{"GERENTE PROMOCIÓN GMM NOVELES"}</option>)
                    options.push(<option value={"SUBDIRECTOR COMERCIAL DE DESARROLLO"}>{"SUBDIRECTOR COMERCIAL DE DESARROLLO"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE OPERACIONES"}>{"SUBDIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN GMM"}>{"SUBDIRECTOR DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN PYMES"}>{"SUBDIRECTOR DE PROMOCIÓN PYMES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN VIDA"}>{"SUBDIRECTOR DE PROMOCIÓN VIDA"}</option>)
                }   else if (columnasSalida.supervisor == "SUBDIRECTOR COMERCIAL DE DESARROLLO") {
                    options.push(<option value={"ASESOR SR"}>{"ASESOR SR"}</option>)
                    options.push(<option value={"CONSEJO DE ADMINISTRACIÓN"}>{"CONSEJO DE ADMINISTRACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE ADMINISTRACIÓN "}>{"COORDINADOR DE ADMINISTRACIÓN "}</option>)
                    options.push(<option value={"COORDINADOR DE CAPACITACIÓN"}>{"COORDINADOR DE CAPACITACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE SISTEMAS"}>{"COORDINADOR DE SISTEMAS"}</option>)
                    options.push(<option value={"DIRECTOR DE OPERACIONES"}>{"DIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"DIRECTOR GENERAL"}>{"DIRECTOR GENERAL"}</option>)
                    options.push(<option value={"GERENTE DE ADMINISTRACIÓN Y RRHH"}>{"GERENTE DE ADMINISTRACIÓN Y RRHH"}</option>)
                    options.push(<option value={"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}>{"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}</option>)
                    options.push(<option value={"GERENTE DE COBRANZA"}>{"GERENTE DE COBRANZA"}</option>)
                    options.push(<option value={"GERENTE DE EMISIÓN"}>{"GERENTE DE EMISIÓN"}</option>)
                    options.push(<option value={"GERENTE DE PLANEACIÓN ESTRATÉGICA"}>{"GERENTE DE PLANEACIÓN ESTRATÉGICA"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN GMM"}>{"GERENTE DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN PATRIMONIALES"}>{"GERENTE DE PROMOCIÓN PATRIMONIALES"}</option>)
                    options.push(<option value={"GERENTE DE RECLUTAMIENTO"}>{"GERENTE DE RECLUTAMIENTO"}</option>)
                    options.push(<option value={"GERENTE DE SINIESTROS"}>{"GERENTE DE SINIESTROS"}</option>)
                    options.push(<option value={"GERENTE PROMOCIÓN GMM NOVELES"}>{"GERENTE PROMOCIÓN GMM NOVELES"}</option>)
                    options.push(<option selected value={"SUBDIRECTOR COMERCIAL DE DESARROLLO"}>{"SUBDIRECTOR COMERCIAL DE DESARROLLO"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE OPERACIONES"}>{"SUBDIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN GMM"}>{"SUBDIRECTOR DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN PYMES"}>{"SUBDIRECTOR DE PROMOCIÓN PYMES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN VIDA"}>{"SUBDIRECTOR DE PROMOCIÓN VIDA"}</option>)
                }    else if (columnasSalida.supervisor == "SUBDIRECTOR DE OPERACIONES") {
                    options.push(<option value={"ASESOR SR"}>{"ASESOR SR"}</option>)
                    options.push(<option value={"CONSEJO DE ADMINISTRACIÓN"}>{"CONSEJO DE ADMINISTRACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE ADMINISTRACIÓN "}>{"COORDINADOR DE ADMINISTRACIÓN "}</option>)
                    options.push(<option value={"COORDINADOR DE CAPACITACIÓN"}>{"COORDINADOR DE CAPACITACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE SISTEMAS"}>{"COORDINADOR DE SISTEMAS"}</option>)
                    options.push(<option value={"DIRECTOR DE OPERACIONES"}>{"DIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"DIRECTOR GENERAL"}>{"DIRECTOR GENERAL"}</option>)
                    options.push(<option value={"GERENTE DE ADMINISTRACIÓN Y RRHH"}>{"GERENTE DE ADMINISTRACIÓN Y RRHH"}</option>)
                    options.push(<option value={"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}>{"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}</option>)
                    options.push(<option value={"GERENTE DE COBRANZA"}>{"GERENTE DE COBRANZA"}</option>)
                    options.push(<option value={"GERENTE DE EMISIÓN"}>{"GERENTE DE EMISIÓN"}</option>)
                    options.push(<option value={"GERENTE DE PLANEACIÓN ESTRATÉGICA"}>{"GERENTE DE PLANEACIÓN ESTRATÉGICA"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN GMM"}>{"GERENTE DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN PATRIMONIALES"}>{"GERENTE DE PROMOCIÓN PATRIMONIALES"}</option>)
                    options.push(<option value={"GERENTE DE RECLUTAMIENTO"}>{"GERENTE DE RECLUTAMIENTO"}</option>)
                    options.push(<option value={"GERENTE DE SINIESTROS"}>{"GERENTE DE SINIESTROS"}</option>)
                    options.push(<option value={"GERENTE PROMOCIÓN GMM NOVELES"}>{"GERENTE PROMOCIÓN GMM NOVELES"}</option>)
                    options.push(<option value={"SUBDIRECTOR COMERCIAL DE DESARROLLO"}>{"SUBDIRECTOR COMERCIAL DE DESARROLLO"}</option>)
                    options.push(<option selected value={"SUBDIRECTOR DE OPERACIONES"}>{"SUBDIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN GMM"}>{"SUBDIRECTOR DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN PYMES"}>{"SUBDIRECTOR DE PROMOCIÓN PYMES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN VIDA"}>{"SUBDIRECTOR DE PROMOCIÓN VIDA"}</option>)
                }    else if (columnasSalida.supervisor == "SUBDIRECTOR DE PROMOCIÓN GMM") {
                    options.push(<option value={"ASESOR SR"}>{"ASESOR SR"}</option>)
                    options.push(<option value={"CONSEJO DE ADMINISTRACIÓN"}>{"CONSEJO DE ADMINISTRACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE ADMINISTRACIÓN "}>{"COORDINADOR DE ADMINISTRACIÓN "}</option>)
                    options.push(<option value={"COORDINADOR DE CAPACITACIÓN"}>{"COORDINADOR DE CAPACITACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE SISTEMAS"}>{"COORDINADOR DE SISTEMAS"}</option>)
                    options.push(<option value={"DIRECTOR DE OPERACIONES"}>{"DIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"DIRECTOR GENERAL"}>{"DIRECTOR GENERAL"}</option>)
                    options.push(<option value={"GERENTE DE ADMINISTRACIÓN Y RRHH"}>{"GERENTE DE ADMINISTRACIÓN Y RRHH"}</option>)
                    options.push(<option value={"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}>{"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}</option>)
                    options.push(<option value={"GERENTE DE COBRANZA"}>{"GERENTE DE COBRANZA"}</option>)
                    options.push(<option value={"GERENTE DE EMISIÓN"}>{"GERENTE DE EMISIÓN"}</option>)
                    options.push(<option value={"GERENTE DE PLANEACIÓN ESTRATÉGICA"}>{"GERENTE DE PLANEACIÓN ESTRATÉGICA"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN GMM"}>{"GERENTE DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN PATRIMONIALES"}>{"GERENTE DE PROMOCIÓN PATRIMONIALES"}</option>)
                    options.push(<option value={"GERENTE DE RECLUTAMIENTO"}>{"GERENTE DE RECLUTAMIENTO"}</option>)
                    options.push(<option value={"GERENTE DE SINIESTROS"}>{"GERENTE DE SINIESTROS"}</option>)
                    options.push(<option value={"GERENTE PROMOCIÓN GMM NOVELES"}>{"GERENTE PROMOCIÓN GMM NOVELES"}</option>)
                    options.push(<option value={"SUBDIRECTOR COMERCIAL DE DESARROLLO"}>{"SUBDIRECTOR COMERCIAL DE DESARROLLO"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE OPERACIONES"}>{"SUBDIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option selected value={"SUBDIRECTOR DE PROMOCIÓN GMM"}>{"SUBDIRECTOR DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN PYMES"}>{"SUBDIRECTOR DE PROMOCIÓN PYMES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN VIDA"}>{"SUBDIRECTOR DE PROMOCIÓN VIDA"}</option>)
                }    else if (columnasSalida.supervisor == "SUBDIRECTOR DE PROMOCIÓN PYMES") {
                    options.push(<option value={"ASESOR SR"}>{"ASESOR SR"}</option>)
                    options.push(<option value={"CONSEJO DE ADMINISTRACIÓN"}>{"CONSEJO DE ADMINISTRACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE ADMINISTRACIÓN "}>{"COORDINADOR DE ADMINISTRACIÓN "}</option>)
                    options.push(<option value={"COORDINADOR DE CAPACITACIÓN"}>{"COORDINADOR DE CAPACITACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE SISTEMAS"}>{"COORDINADOR DE SISTEMAS"}</option>)
                    options.push(<option value={"DIRECTOR DE OPERACIONES"}>{"DIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"DIRECTOR GENERAL"}>{"DIRECTOR GENERAL"}</option>)
                    options.push(<option value={"GERENTE DE ADMINISTRACIÓN Y RRHH"}>{"GERENTE DE ADMINISTRACIÓN Y RRHH"}</option>)
                    options.push(<option value={"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}>{"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}</option>)
                    options.push(<option value={"GERENTE DE COBRANZA"}>{"GERENTE DE COBRANZA"}</option>)
                    options.push(<option value={"GERENTE DE EMISIÓN"}>{"GERENTE DE EMISIÓN"}</option>)
                    options.push(<option value={"GERENTE DE PLANEACIÓN ESTRATÉGICA"}>{"GERENTE DE PLANEACIÓN ESTRATÉGICA"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN GMM"}>{"GERENTE DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN PATRIMONIALES"}>{"GERENTE DE PROMOCIÓN PATRIMONIALES"}</option>)
                    options.push(<option value={"GERENTE DE RECLUTAMIENTO"}>{"GERENTE DE RECLUTAMIENTO"}</option>)
                    options.push(<option value={"GERENTE DE SINIESTROS"}>{"GERENTE DE SINIESTROS"}</option>)
                    options.push(<option value={"GERENTE PROMOCIÓN GMM NOVELES"}>{"GERENTE PROMOCIÓN GMM NOVELES"}</option>)
                    options.push(<option value={"SUBDIRECTOR COMERCIAL DE DESARROLLO"}>{"SUBDIRECTOR COMERCIAL DE DESARROLLO"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE OPERACIONES"}>{"SUBDIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN GMM"}>{"SUBDIRECTOR DE PROMOCIÓN GMM"}</option>)
                    options.push(<option selected value={"SUBDIRECTOR DE PROMOCIÓN PYMES"}>{"SUBDIRECTOR DE PROMOCIÓN PYMES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN VIDA"}>{"SUBDIRECTOR DE PROMOCIÓN VIDA"}</option>)
                }    else if (columnasSalida.supervisor == "SUBDIRECTOR DE PROMOCIÓN VIDA") {
                    options.push(<option value={"ASESOR SR"}>{"ASESOR SR"}</option>)
                    options.push(<option value={"CONSEJO DE ADMINISTRACIÓN"}>{"CONSEJO DE ADMINISTRACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE ADMINISTRACIÓN "}>{"COORDINADOR DE ADMINISTRACIÓN "}</option>)
                    options.push(<option value={"COORDINADOR DE CAPACITACIÓN"}>{"COORDINADOR DE CAPACITACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE SISTEMAS"}>{"COORDINADOR DE SISTEMAS"}</option>)
                    options.push(<option value={"DIRECTOR DE OPERACIONES"}>{"DIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"DIRECTOR GENERAL"}>{"DIRECTOR GENERAL"}</option>)
                    options.push(<option value={"GERENTE DE ADMINISTRACIÓN Y RRHH"}>{"GERENTE DE ADMINISTRACIÓN Y RRHH"}</option>)
                    options.push(<option value={"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}>{"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}</option>)
                    options.push(<option value={"GERENTE DE COBRANZA"}>{"GERENTE DE COBRANZA"}</option>)
                    options.push(<option value={"GERENTE DE EMISIÓN"}>{"GERENTE DE EMISIÓN"}</option>)
                    options.push(<option value={"GERENTE DE PLANEACIÓN ESTRATÉGICA"}>{"GERENTE DE PLANEACIÓN ESTRATÉGICA"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN GMM"}>{"GERENTE DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN PATRIMONIALES"}>{"GERENTE DE PROMOCIÓN PATRIMONIALES"}</option>)
                    options.push(<option value={"GERENTE DE RECLUTAMIENTO"}>{"GERENTE DE RECLUTAMIENTO"}</option>)
                    options.push(<option value={"GERENTE DE SINIESTROS"}>{"GERENTE DE SINIESTROS"}</option>)
                    options.push(<option value={"GERENTE PROMOCIÓN GMM NOVELES"}>{"GERENTE PROMOCIÓN GMM NOVELES"}</option>)
                    options.push(<option value={"SUBDIRECTOR COMERCIAL DE DESARROLLO"}>{"SUBDIRECTOR COMERCIAL DE DESARROLLO"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE OPERACIONES"}>{"SUBDIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN GMM"}>{"SUBDIRECTOR DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN PYMES"}>{"SUBDIRECTOR DE PROMOCIÓN PYMES"}</option>)
                    options.push(<option selected value={"SUBDIRECTOR DE PROMOCIÓN VIDA"}>{"SUBDIRECTOR DE PROMOCIÓN VIDA"}</option>)
                } else {
                    options.push(<option selected value={0}>Seleccionar</option>)
                    options.push(<option value={"ASESOR SR"}>{"ASESOR SR"}</option>)
                    options.push(<option value={"CONSEJO DE ADMINISTRACIÓN"}>{"CONSEJO DE ADMINISTRACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE ADMINISTRACIÓN "}>{"COORDINADOR DE ADMINISTRACIÓN "}</option>)
                    options.push(<option value={"COORDINADOR DE CAPACITACIÓN"}>{"COORDINADOR DE CAPACITACIÓN"}</option>)
                    options.push(<option value={"COORDINADOR DE SISTEMAS"}>{"COORDINADOR DE SISTEMAS"}</option>)
                    options.push(<option value={"DIRECTOR DE OPERACIONES"}>{"DIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"DIRECTOR GENERAL"}>{"DIRECTOR GENERAL"}</option>)
                    options.push(<option value={"GERENTE DE ADMINISTRACIÓN Y RRHH"}>{"GERENTE DE ADMINISTRACIÓN Y RRHH"}</option>)
                    options.push(<option value={"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}>{"GERENTE DE CAPACITACIÓN Y COMUNICACIÓN"}</option>)
                    options.push(<option value={"GERENTE DE COBRANZA"}>{"GERENTE DE COBRANZA"}</option>)
                    options.push(<option value={"GERENTE DE EMISIÓN"}>{"GERENTE DE EMISIÓN"}</option>)
                    options.push(<option value={"GERENTE DE PLANEACIÓN ESTRATÉGICA"}>{"GERENTE DE PLANEACIÓN ESTRATÉGICA"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN GMM"}>{"GERENTE DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"GERENTE DE PROMOCIÓN PATRIMONIALES"}>{"GERENTE DE PROMOCIÓN PATRIMONIALES"}</option>)
                    options.push(<option value={"GERENTE DE RECLUTAMIENTO"}>{"GERENTE DE RECLUTAMIENTO"}</option>)
                    options.push(<option value={"GERENTE DE SINIESTROS"}>{"GERENTE DE SINIESTROS"}</option>)
                    options.push(<option value={"GERENTE PROMOCIÓN GMM NOVELES"}>{"GERENTE PROMOCIÓN GMM NOVELES"}</option>)
                    options.push(<option value={"SUBDIRECTOR COMERCIAL DE DESARROLLO"}>{"SUBDIRECTOR COMERCIAL DE DESARROLLO"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE OPERACIONES"}>{"SUBDIRECTOR DE OPERACIONES"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN GMM"}>{"SUBDIRECTOR DE PROMOCIÓN GMM"}</option>)
                    options.push(<option value={"SUBDIRECTOR DE PROMOCIÓN PYMES"}>{"SUBDIRECTOR DE PROMOCIÓN PYMES"}</option>)
                    options.push(<option selected value={"SUBDIRECTOR DE PROMOCIÓN VIDA"}>{"SUBDIRECTOR DE PROMOCIÓN VIDA"}</option>)
                }

                let salida = []
                salida.push(<select disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                    && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                        && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name={"supervisor"} style={{ borderColor: '#E1E5F0' }} class="form-control" aria-label="Default select example">{options}</select>)
                this.setState({ selectSupervisor: salida })
                /* armando select del supervisor */
                console.log("el json a entrar ", columnasSalida)
                let correo_electronico = columnasSalida.correo_electronico
                let otros_correos = columnasSalida.otros_correos
                this.setState({ contenidoEdicion: columnasSalida, validaEmail: correo_electronico != null ? correo_electronico.match(this.state.expresionEmail) : false, validaOtrosCorreos: otros_correos != null ? otros_correos.match(this.state.expresionEmail) : false })
                this.construyeSelect(datos.urlServicePy+"parametros/api_cat_escolaridad/0", "selectEscolaridad")
                this.construyeSelect(datos.urlServicePy+"parametros/api_vcat_puesto/0", "selectPuesto")
                this.construyeSelect(datos.urlServicePy+"parametros/api_vcat_area/0", "selectArea")
                this.construyeSelect(datos.urlServicePy+"parametros/api_cat_rol/0", "selectRol")
                this.construyeSelect(datos.urlServicePy+"parametros/api_cat_profeciones/0", "selectProfesion")
                this.construyeSelect(datos.urlServicePy+"parametros/api_cat_bancos/0", "selectBancos")
                this.construyeSelect(datos.urlServicePy+"parametros/api_cat_causas_baja/0", "selectCausasBaja")
                this.construyeSelect(datos.urlServicePy+"parametros/api_cat_estatusbaja/0", "selectEstatusBaja")//
                this.construyeSelect(datos.urlServicePy+"parametros/api_cat_sociedad/0", "selectSociedad")

                if (columnasSalida.doc_puesto != null && columnasSalida.doc_puesto.length > 0) {
                    // console.log("entrando al puesto ",columnasSalida.doc_puesto)
                    this.setState({ colorSubirArchivo: "#78CB5A", colorTextoSubirArchivo: "#FFFFFF", colorBotonSubirArchivo: "#E5F6E0", nombreArchivoPuesto: "Reemplazar Puesto...", textoPuesto: 'Subir archivo', muestraDocPuesto: true })
                }

                if (columnasSalida.doc_cv != null && columnasSalida.doc_cv.length > 0) {
                    this.setState({ colorSubirArchivoCV: "#78CB5A", colorTextoSubirArchivoCV: "#FFFFFF", colorBotonSubirArchivoCV: "#E5F6E0", nombreArchivoCV: "Reemplazar CV...", muestraDocCV: true })
                }
                if (columnasSalida.doc_psp != null && columnasSalida.doc_psp.length > 0) {
                    //  this.setState({  colorSubirArchivoCV: "#78CB5A", colorTextoSubirArchivoCV: "#FFFFFF", colorBotonSubirArchivoCV: "#E5F6E0", nombreArchivoCV:"Reemplazar..." })
                    this.setState({ colorSubirArchivoPSP: "#78CB5A", colorTextoSubirArchivoPSP: "#FFFFFF", colorBotonSubirArchivoPSP: "#E5F6E0", nombreArchivoPSP: "Reemplazar PSP...", muestraDocPSP: true })
                }

                if(this.state.contenidoEdicion.escolaridad_id == 2 || this.state.contenidoEdicion.escolaridad_id == 3){
                    this.setState({banProfesion:true})
                }else{
                    this.setState({banProfesion:false})
                }
            });

    }

    onChange = e => {
        console.log("entrando", e.target.name, e.target.value)
        let contenidoEdicion = this.state.contenidoEdicion
        if ("selectEscolaridad" == e.target.name) {
            console.log("entroa asleect")
            let contenido = this.state.contenidoEdicion
            contenido.escolaridad_id = parseInt(e.target.value)
            this.setState({ contenidoEdicion: contenido })
            if(e.target.value == 2 || e.target.value == 3){
                this.setState({banProfesion:true})
                contenido.profesion_id = null ;
                this.setState({ ["selectProfesion"]: null })
                this.construyeSelect(datos.urlServicePy+"parametros/api_cat_profeciones/0", "selectProfesion")
            }else{
                this.setState({banProfesion:false})
            }
        } else if ("selectPuesto" == e.target.name) {
            console.log("entroa asleect")
            let contenido = this.state.contenidoEdicion
            if (parseInt(e.target.value) == 10) {
                this.setState({ banpuesto: true })
            } else {
                contenido.figura_id = 0
                this.setState({ banpuesto: false })
            }
            contenido.puesto_id = parseInt(e.target.value)
            this.setState({ contenidoEdicion: contenido })
        } else if ("selectArea" == e.target.name) {
            console.log("entroa asleect")
            let contenido = this.state.contenidoEdicion
            contenido.area_id = parseInt(e.target.value)
            this.setState({ contenidoEdicion: contenido })
        } else if ("selectRol" == e.target.name) {
            console.log("entro a select selectRol")
            let contenido = this.state.contenidoEdicion
            let v=(e.target.value).split("-")
            let rol_id=parseInt(v[0])
            contenido.rol_id = rol_id    
            this.setState({ contenidoEdicion: contenido,homoClaveRol:v[1] })
        } else if ("selectProfesion" == e.target.name) {
            console.log("entroa asleect")
            let contenido = this.state.contenidoEdicion
            contenido.profesion_id = parseInt(e.target.value)
            this.setState({ contenidoEdicion: contenido })
        } else if ("selectBancos" == e.target.name) {
            console.log("entroa asleect")
            let contenido = this.state.contenidoEdicion
            contenido.banco_id = parseInt(e.target.value)
            this.setState({ contenidoEdicion: contenido })
        } else if ("selectCausasBaja" == e.target.name) {
            console.log("entroa asleect")
            let contenido = this.state.contenidoEdicion
            contenido.causabaja_id = parseInt(e.target.value)
            this.setState({ contenidoEdicion: contenido })

        } else if ("selectEstatusBaja" == e.target.name) {
            console.log("entroa asleect")
            let contenido = this.state.contenidoEdicion
            contenido.estatusbaja_id = parseInt(e.target.value)
            if (parseInt(e.target.value) == 0 || parseInt(e.target.value) == 1) {
                contenido.causabaja_id = parseInt(1)
            }
            this.setState({ contenidoEdicion: contenido })

        } else if ("selectSociedad" == e.target.name) {
            console.log("entroa asleect")
            let contenido = this.state.contenidoEdicion
            contenido.sociedad_id = parseInt(e.target.value)
            this.setState({ contenidoEdicion: contenido })
            console.log("sociedad es ::::" + this.state.selectSociedad);
        } else if (e.target.name == "acceso_impresion" || e.target.name == "acceso_copiadora" || e.target.name == "acceso_pai") {
            contenidoEdicion["" + e.target.name + ""] = e.target.checked == true ? 1 : 0
        } else if (e.target.name == "sueldo") {
            let arraSueldo = e.target.value.split(".")
            let sueldound = arraSueldo[1] == undefined ? 0 : arraSueldo[1].length
            if(arraSueldo[0].length <= 10 && sueldound <=2 ){
                contenidoEdicion["" + e.target.name + ""] = parseFloat(e.target.value)
            }
        } else if (e.target.name == "correo_electronico") {
            let correo_electronico = e.target.value;
            if (correo_electronico.match(this.state.expresionEmail)) {
                this.setState({ validaEmail: true })
            } else {
                this.setState({ validaEmail: false })
            }
            contenidoEdicion["" + e.target.name + ""] = e.target.value

        } else if (e.target.name == "otros_correos") {
            let otros_correos = e.target.value;
            if (otros_correos.match(this.state.expresionEmail)) {
                this.setState({ validaOtrosCorreos: true })
            } else {
                this.setState({ validaOtrosCorreos: false })
            }
            contenidoEdicion["" + e.target.name + ""] = e.target.value

        } else if (e.target.name == "cta_clabe") {
            let cta_clabe = e.target.value
            if (cta_clabe.length <= 18) {
                contenidoEdicion["" + e.target.name + ""] = e.target.value
            }
        } else if(e.target.name == "num_extension"){
            let num_extension = e.target.value
            if(num_extension.length == 0){
                contenidoEdicion["" + e.target.name + ""] = null
            }else{
                contenidoEdicion["" + e.target.name + ""] = e.target.value
            }

        } else {
            contenidoEdicion["" + e.target.name + ""] = e.target.value
        }



        this.setState({ contenidoEdicion: contenidoEdicion })
        //console.log("los props al persisitir ", this.props)
        //this.props.guardaFormulario2(contenidoEdicion)


        // this.setState({ [e.target.name]: e.target.value })
    }



    onChangeFilePuesto(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file de puesto", file);
        this.setState({ archivoPuesto: file, colorSubirArchivo: "#78CB5A", colorTextoSubirArchivo: "#FFFFFF", colorBotonSubirArchivo: "#E5F6E0", nombreArchivoPuesto: file.name })
        // this.props.guardaArchivo(file, "archivoPuesto")

    }
    onChangeFileCV(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chingado file de CV ", file);
        this.setState({ archivoCV: file, colorSubirArchivoCV: "#78CB5A", colorTextoSubirArchivoCV: "#FFFFFF", colorBotonSubirArchivoCV: "#E5F6E0", nombreArchivoCV: file.name })


    }

    onChangeFilePSP(event) {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
        console.log("el chinfago file de psp ", file);
        this.setState({ archivoPSP: file, colorSubirArchivoPSP: "#78CB5A", colorTextoSubirArchivoPSP: "#FFFFFF", colorBotonSubirArchivoPSP: "#E5F6E0", nombreArchivoPSP: file.name })


    }

    onClickBotonArchivoPuesto() {
        console.log("entrnaod a la imagen")
        this.upload.click()
    }


    onClickBotonArchivoCV() {
        console.log("entrnaod a la CV")
        this.cv.click()
    }

    onClickBotonArchivoPSP() {
        console.log("entrnaod a la PSP")
        this.psp.click()
    }


    validaExistenciaAlgunArchivo() {
        let actualizar = false;
        if (this.state.contenidoEdicion.doc_foto.length > 0 || this.state.contenidoEdicion.doc_puesto.length > 0 || this.state.contenidoEdicion.doc_cv.length || this.state.contenidoEdicion.doc_psp.length) {
            actualizar = true
        }
        return actualizar
    }

    persisteEnInicioDeSesion(){
        let jsonInicioSesion={
            "value":this.state.contenidoEdicion.usuario,
            "password":this.state.contenidoEdicion.password,
            "puesto":this.state.homoClaveRol,
            "nombre":this.state.contenidoEdicion.nombre,
            "apaterno":this.state.contenidoEdicion.ape_paterno
        }

        const requestOptions = {
            method: "POST",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(jsonInicioSesion)
        };
        fetch(datos.urlServiceJa+"admin/usuarios/persisteUsuarioEmpleado", requestOptions)
            .then(response => response.json())
            .then(data => {
                console.log("respuesta de la persistencia de inicio ",data)
            })
    }

    clickSiguiente() {
        console.log("json a editar", this.state.contenidoEdicion, this.props)
        this.state.contenidoEdicion.user_id = this.props.idUsuario
        this.state.contenidoEdicion.figura_id = this.state.contenidoEdicion.figura_id == 0 ? null : this.state.contenidoEdicion.figura_id;
        const requestOptions = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(this.state.contenidoEdicion)
        };
        fetch(datos.urlServicePy+"parametros_upd/api_cat_empleados/" + this.state.contenidoEdicion.id, requestOptions)
            .then(response => response.json())
            .then(data => {

                console.log(data)
                if (data.non_field_errors != undefined) {
                    this.modalInicio.click()
                    console.log("--------------- se lanza error ----------------------------")
                } else {
                    this.persisteEnInicioDeSesion()
                    if (this.validaExistenciaAlgunArchivo()) {
                        let formData = new FormData();
                        let banderaEnvioDocs = false
                        if (this.state.archivoPuesto != undefined) {
                            formData.append('puesto', this.state.archivoPuesto)
                            banderaEnvioDocs = true
                        }
                        if (this.state.archivoCV != undefined) {
                            formData.append('cv', this.state.archivoCV)
                            banderaEnvioDocs = true
                        }
                        if (this.state.archivoPSP != undefined) {
                            formData.append('psp', this.state.archivoPSP)
                            banderaEnvioDocs = true
                        }
                        const config = {
                            headers: { 'content-type': 'multipart/form-data' }
                        }
                        if (banderaEnvioDocs) {
                            axios.put(datos.urlServicePy+"api/doc_empleado/" + this.state.contenidoEdicion.id + "/", formData, config)
                                .then(res => {
                                    console.log(res.data);
                                    console.log(this.state.filename);
                                    console.log(formData);
                                })
                        }
                    } else {
                        let formData = new FormData();
                        let banderaEnvioDocs = false
                        if (this.state.archivoPuesto != undefined) {
                            formData.append('puesto', this.state.archivoPuesto)
                            banderaEnvioDocs = true
                        }
                        if (this.state.archivoCV != undefined) {
                            formData.append('cv', this.state.archivoCV)
                            banderaEnvioDocs = true
                        }
                        if (this.state.archivoPSP != undefined) {
                            formData.append('psp', this.state.archivoPSP)
                            banderaEnvioDocs = true
                        }
                        formData.append('id', this.state.contenidoEdicion.id)
                        formData.append('empleado_id', this.state.contenidoEdicion.id)
                        const config = {
                            headers: { 'content-type': 'multipart/form-data' }
                        }
                        if (banderaEnvioDocs) {
                            axios.post(datos.urlServicePy+"api/doc_empleado/", formData, config)
                                .then(res => {
                                    console.log(res.data);
                                    console.log(this.state.filename);
                                    console.log(formData);
                                })
                        }
                    }
                    if(this.state.contenidoEdicion.sociedad_id == 6 || this.state.contenidoEdicion.sociedad_id == 0 || this.state.contenidoEdicion.sociedad_id == null){
                        this.props.botonCancelar()
                    }else{
                        this.props.muestraFormulario3(this.state.contenidoEdicion.id)
                    }
                }
                // this.props.muestraFormulario3(this.state.contenidoEdicion.id)
            })
    }



    clickInfoGeneral(determinaclick) {
        console.log("json a editar", this.state.contenidoEdicion)
        this.state.contenidoEdicion.user_id = this.props.idUsuario
        this.state.contenidoEdicion.figura_id = this.state.contenidoEdicion.figura_id == 0 ? null : this.state.contenidoEdicion.figura_id;
        const requestOptions = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(this.state.contenidoEdicion)
        };
        fetch(datos.urlServicePy+"parametros_upd/api_cat_empleados/" + this.state.contenidoEdicion.id, requestOptions)
            .then(response => response.json())
            .then(data => {

                console.log(data)
                if (data.non_field_errors != undefined) {
                    this.modalInicio.click()
                    console.log("--------------- se lanza error ----------------------------")
                } else {
                    if (this.validaExistenciaAlgunArchivo()) {
                        let formData = new FormData();
                        let banderaEnvioDocs = false
                        if (this.state.archivoPuesto != undefined) {
                            formData.append('puesto', this.state.archivoPuesto)
                            banderaEnvioDocs = true
                        }
                        if (this.state.archivoCV != undefined) {
                            formData.append('cv', this.state.archivoCV)
                            banderaEnvioDocs = true
                        }
                        if (this.state.archivoPSP != undefined) {
                            formData.append('psp', this.state.archivoPSP)
                            banderaEnvioDocs = true
                        }
                        const config = {
                            headers: { 'content-type': 'multipart/form-data' }
                        }
                        if (banderaEnvioDocs) {
                            axios.put(datos.urlServicePy+"api/doc_empleado/" + this.state.contenidoEdicion.id + "/", formData, config)
                                .then(res => {
                                    console.log(res.data);
                                    console.log(this.state.filename);
                                    console.log(formData);
                                })
                        }
                    } else {
                        let formData = new FormData();
                        let banderaEnvioDocs = false
                        if (this.state.archivoPuesto != undefined) {
                            formData.append('puesto', this.state.archivoPuesto)
                            banderaEnvioDocs = true
                        }
                        if (this.state.archivoCV != undefined) {
                            formData.append('cv', this.state.archivoCV)
                            banderaEnvioDocs = true
                        }
                        if (this.state.archivoPSP != undefined) {
                            formData.append('psp', this.state.archivoPSP)
                            banderaEnvioDocs = true
                        }
                        formData.append('id', this.state.contenidoEdicion.id)
                        formData.append('empleado_id', this.state.contenidoEdicion.id)
                        const config = {
                            headers: { 'content-type': 'multipart/form-data' }
                        }
                        if (banderaEnvioDocs) {
                            axios.post(datos.urlServicePy+"api/doc_empleado/", formData, config)
                                .then(res => {
                                    console.log(res.data);
                                    console.log(this.state.filename);
                                    console.log(formData);
                                })
                        }
                    }
                    if (determinaclick == "1") {
                        this.props.muestraFormulario(this.props.idEmpleado)
                    } else if (determinaclick == "3") {
                        this.props.muestraFormulario3(this.props.idEmpleado)
                    } else if (determinaclick == "2") {
                        console.log("Entro en pre recarga y guardado");
                    }
                }
            })
    }





    render() {
        return (
            <div>
                {/* inicio modal */}
                <button type="button" style={{ display: 'none' }} ref={(ref) => this.modalInicio = ref} data-toggle="modal" data-target="#modal-error"   ></button>
                <div id="modal-error" className="modal fade" tabindex="-1">
                    <div className="modal-dialog ">
                        <div className="modal-content text-white" style={{ backgroundColor: "#FFFFF" }}>
                            <div className="modal-header text-white text-center" style={{ backgroundColor: "#FFFFF" }}>
                            </div>
                            <div className="modal-body">
                                <div className="card-body">

                                    <h6 className="col-12 text-center text-dark font-weight-semibold">El numero de extensión no puede ser repetido por otro empleado</h6>

                                </div>
                            </div>
                            <div class="modal-footer d-flex justify-content-center">
                                <div className="col-12">
                                    <div className="row">
                                        <button type="button" style={{ width: '100%', backgroundColor: '#8189D4' }} class="btn text-white" onClick={(e) => console.log("")} data-dismiss="modal"  >Aceptar</button>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>






                {/* fin modal */}
                <div class="card">
                    <div class="row mb-2 mt-2 ml-1">
                        <div class="col-12 col-lg-9">
                            <div class="row">

                                <div class="col-6 col-lg-3 d-flex align-items-center pb-sm-1">
                                    <div class="mr-2">
                                        {
                                            this.props.idEmpleado == null ?
                                                <a href="#" class="btn btn-light rounded-pill btn-icon btn-sm disabled ">
                                                    <span class="letter-icon">1</span>
                                                </a>
                                                :
                                                <a href="#"
                                                    class="btn btn-primary rounded-pill btn-icon btn-sm"> <span
                                                        class="letter-icon" onClick={() => this.clickInfoGeneral("1")}>1 </span>
                                                </a>
                                        }
                                    </div>
                                    <div>
                                        {
                                            this.props.idEmpleado == null ?
                                                <a href="#" class="text-body font-weight-semibold  letter-icon-title text-muted ">Información general</a>
                                                :
                                                <a href="#" class="text-body font-weight-semibold letter-icon-title" onClick={() => this.clickInfoGeneral("1")}>Información general</a>
                                        }
                                    </div>
                                </div>


                                <div class="col-6 col-lg-3 d-flex align-items-center pb-sm-1">
                                    <div class="mr-2">

                                        {
                                            this.props.idEmpleado == null ?
                                                <a href="#" class="btn btn-light rounded-pill btn-icon btn-sm disabled ">
                                                    <span class="letter-icon">2</span>
                                                </a>
                                                :
                                                <a href="#" onClick={() => this.clickInfoGeneral("2")}
                                                    class="btn btn-primary rounded-pill btn-icon btn-sm"> <span
                                                        class="letter-icon">2</span>
                                                </a>
                                        }
                                    </div>
                                    <div>

                                        {
                                            this.props.idEmpleado == null ?
                                                <a href="#" class="text-body font-weight-semibold  letter-icon-title text-muted ">Información laboral</a>
                                                :
                                                <a href="#" onClick={() => this.clickInfoGeneral("2")} class="text-body font-weight-semibold letter-icon-title">Información laboral</a>

                                        }

                                    </div>
                                </div>




                                <div class="col-6 col-lg-3 d-flex align-items-center pb-sm-1">
                                    <div class="mr-2">

                                        {
                                            this.props.idEmpleado == null || (this.state.contenidoEdicion.sociedad_id == 6 || this.state.contenidoEdicion.sociedad_id == 0 || this.state.contenidoEdicion.sociedad_id == null)?
                                                <a href="#" class="btn btn-light rounded-pill btn-icon btn-sm disabled">
                                                    <span class="letter-icon">3</span>
                                                </a>
                                                :
                                                <a href="#" onClick={() => this.clickInfoGeneral("3")}
                                                    class="btn btn-primary rounded-pill btn-icon btn-sm"> <span
                                                        class="letter-icon">3</span>
                                                </a>

                                        }


                                    </div>
                                    <div>
                                        {
                                            this.props.idEmpleado == null || (this.state.contenidoEdicion.sociedad_id == 6 || this.state.contenidoEdicion.sociedad_id == 0 || this.state.contenidoEdicion.sociedad_id == null)?
                                                <a href="#" class="text-body font-weight-semibold  letter-icon-title text-muted">Beneficiarios</a>
                                                :
                                                <a href="#" onClick={() => this.clickInfoGeneral("3")} class="text-body font-weight-semibold letter-icon-title">Beneficiarios</a>

                                        }

                                    </div>
                                </div>




                            </div>
                        </div>
                        <div class="col-12 col-lg-3">

                            <div class="float-right mr-3">

                                <button type="button" class="btn btn-light mr-1" onClick={(e) => this.props.botonCancelar()}>Cancelar</button>
                                {
                                    this.state.contenidoEdicion.estatusbaja_id == 2 ?
                                        <button type="button" class="btn btn-warning"
                                            style={{ backgroundColor: '#ED6C26' }} id="siguiente"

                                            disabled={
                                                this.state.contenidoEdicion.estatusbaja_id == null || this.state.contenidoEdicion.estatusbaja_id == 0 ||   /*this.state.contenidoEdicion.escolaridad_id ==0 || this.state.contenidoEdicion.escolaridad_id == null  ||*/
                                                (!this.state.banProfesion && (this.state.contenidoEdicion.profesion_id == null || this.state.contenidoEdicion.profesion_id == 0 )) || this.state.contenidoEdicion.fec_ingreso == "" || this.state.contenidoEdicion.fec_ingreso == null ||
                                                this.state.contenidoEdicion.puesto_id == null || this.state.contenidoEdicion.puesto_id == 0 || this.state.contenidoEdicion.area_id == null || this.state.contenidoEdicion.area_id == 0 || 
                                                this.state.contenidoEdicion.rol_id == null || this.state.contenidoEdicion.rol_id == 0 ||
                                                this.state.contenidoEdicion.supervisor + ''.length <= 3 || this.state.contenidoEdicion.supervisor == null || this.state.contenidoEdicion.sueldo == null || this.state.contenidoEdicion.sueldo == 0 ||
                                                this.state.contenidoEdicion.banco_id == null || this.state.contenidoEdicion.banco_id == 0 || this.state.contenidoEdicion.cta_clabe == null || (this.state.contenidoEdicion.cta_clabe + '').length < 18 ||
                                                this.state.validaEmail == false || /*this.state.validaOtrosCorreos == false ||*/ /*this.state.contenidoEdicion.num_extension == null || (this.state.contenidoEdicion.num_extension + '').length == 0  ||*/
                                                this.state.contenidoEdicion.dias_vacaciones == null || this.state.contenidoEdicion.dias_vacaciones <= 0 || (this.state.contenidoEdicion.dias_vacaciones + '').length == 0 ||
                                                this.state.nombreArchivoPuesto == "" || this.state.nombreArchivoPuesto == null || this.state.nombreArchivoPSP == null || this.state.nombreArchivoPSP == "" ||
                                                this.state.contenidoEdicion.figura_id == 0 || this.state.contenidoEdicion.figura_id == null || this.state.contenidoEdicion.sociedad_id == 0 || this.state.contenidoEdicion.sociedad_id == null ||
                                                this.state.contenidoEdicion.fecha_baja_correos == "" || this.state.contenidoEdicion.fecha_baja_correos == null || this.state.contenidoEdicion.causabaja_id == 0
                                            }

                                            onClick={this.clickSiguiente} > Siguiente</button> :
                                        <button type="button" class="btn btn-warning"
                                            style={{ backgroundColor: '#ED6C26' }} id="siguiente"

                                            disabled={
                                                this.state.contenidoEdicion.estatusbaja_id == null || this.state.contenidoEdicion.estatusbaja_id == 0 ||   /*this.state.contenidoEdicion.escolaridad_id ==0 || this.state.contenidoEdicion.escolaridad_id == null  ||*/
                                                (!this.state.banProfesion && (this.state.contenidoEdicion.profesion_id == null || this.state.contenidoEdicion.profesion_id == 0 ))|| this.state.contenidoEdicion.fec_ingreso == "" || this.state.contenidoEdicion.fec_ingreso == null ||
                                                this.state.contenidoEdicion.puesto_id == null || this.state.contenidoEdicion.puesto_id == 0 || this.state.contenidoEdicion.area_id == null || this.state.contenidoEdicion.area_id == 0 || this.state.contenidoEdicion.rol_id == null || this.state.contenidoEdicion.rol_id == 0 ||
                                                this.state.contenidoEdicion.supervisor + ''.length <= 3 || this.state.contenidoEdicion.supervisor == null || this.state.contenidoEdicion.sueldo == null || this.state.contenidoEdicion.sueldo == 0 ||
                                                this.state.contenidoEdicion.banco_id == null || this.state.contenidoEdicion.banco_id == 0 || this.state.contenidoEdicion.cta_clabe == null || (this.state.contenidoEdicion.cta_clabe + '').length < 18 ||
                                                this.state.validaEmail == false || /*this.state.validaOtrosCorreos == false ||*/ /*this.state.contenidoEdicion.num_extension == null || (this.state.contenidoEdicion.num_extension + '').length == 0 ||*/
                                                this.state.contenidoEdicion.dias_vacaciones == null || this.state.contenidoEdicion.dias_vacaciones <= 0 || (this.state.contenidoEdicion.dias_vacaciones + '').length == 0 ||
                                                this.state.nombreArchivoPuesto == "" || this.state.nombreArchivoPuesto == null || this.state.nombreArchivoPSP == null || this.state.nombreArchivoPSP == "" ||
                                                this.state.banpuesto && (this.state.contenidoEdicion.figura_id == 0 || this.state.contenidoEdicion.figura_id == null) || this.state.contenidoEdicion.sociedad_id == 0 || this.state.contenidoEdicion.sociedad_id == null
                                            }

                                            onClick={this.clickSiguiente} > Siguiente</button>
                                }

                            </div>

                        </div>
                    </div>
                </div>

                <div class="card-header bg-transparent header-elements-sm-inline">
                    <h2 class="mb-0 font-weight-semibold">2. Información laboral </h2>
                    <div class="header-elements">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item font-size-sm">Fecha de alta: <strong >{this.state.contenidoEdicion.ts_alta_audit}</strong></li>

                            <li class="list-inline-item font-size-sm">Usuario creador: <strong >{this.state.contenidoEdicion.user_id}</strong></li>
                        </ul>
                    </div>
                </div>
                <br></br>
                <br></br>

                <div>
                    <div className="row">
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Estatus</span>
                                    </span> {this.state.selectEstatusBaja}
                                    {/* <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />*/}
                                </div>
                                {
                                    this.state.contenidoEdicion.estatusbaja_id == 0 ?
                                        <span className="" style={{ color: "red" }}>El estatus es un campo requerido</span> : ''
                                }
                            </div>

                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }}>Fecha de ingreso</span>
                                    </span> <input type="date" style={{ borderColor: '#E1E5F0', opacity: 1 }} name="fec_ingreso" max="2100-01-01" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                        && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} value={"" + this.state.contenidoEdicion.fec_ingreso + ""} className="form-control " />
                                </div>
                                {
                                    this.state.contenidoEdicion.fec_ingreso == "" || this.state.contenidoEdicion.fec_ingreso == null ?
                                        <span className="" style={{ color: "red" }}>El campo fecha ingreso es un campo requerido</span> : ''
                                }
                            </div>
                        </div>

                        <div className="col-xs-4 col-lg-4">

                            {
                                this.state.contenidoEdicion.causabaja_id != 0 && this.state.contenidoEdicion.estatusbaja_id == 2 &&

                                <div className="form-group">

                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Fecha baja</span>
                                        </span> <input type="date" style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " name="fec_baja" max="2100-01-01" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                                && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} value={"" + this.state.contenidoEdicion.fec_baja + ""} />{/* probar value=undefined */}
                                    </div>
                                    {
                                        this.state.contenidoEdicion.fec_baja == "" ?
                                            <span className="" style={{ color: "red" }}>El campo fecha ingreso es un campo requerido</span> : ''
                                    }
                                </div>
                            }

                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }}>Escolaridad</span>
                                    </span>
                                    {this.state.selectEscolaridad}
                                    {/*<input type="text" style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " />*/}
                                </div>
                                {/*
                                    this.state.contenidoEdicion.escolaridad_id == 0 ?
                                        <span className="" style={{ color: "red" }}>El campo escolaridad es un campo requerido</span> : ''
                                */}
                            </div>
                        </div>
                        <div className="col-xs-4 col-lg-4">

                            {
                                this.state.contenidoEdicion.estatusbaja_id == 2 &&
                                <div className="form-group">

                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Causa de baja</span>
                                        </span>{this.state.selectCausasBaja}
                                        {/*<input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />*/}
                                    </div>
                                    {
                                        this.state.contenidoEdicion.causabaja_id == 0 ?
                                            <span className="" style={{ color: "red" }}>El campo causas de baja es un campo requerido</span> : ''
                                    }
                                </div>

                            }
                            {!this.state.banProfesion ?
                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }}>Profesión</span>
                                    </span>
                                    {this.state.selectProfesion}
                                    {/* <input type="text" style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " />*/}
                                </div>
                                {
                                    this.state.contenidoEdicion.profesion_id == 0 || this.state.contenidoEdicion.profesion_id == null ?
                                        <span className="" style={{ color: "red" }}>El campo profesión es un campo requerido</span> : ''
                                }
                            </div>
                            :''}
                            {
                                this.state.contenidoEdicion.causabaja_id != 0 && this.state.contenidoEdicion.estatusbaja_id == 2 &&
                                <div className="form-group">

                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }}>Observaciones de baja</span>
                                        </span>
                                        <textarea disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                                && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name="observaciones_baja" value={this.state.contenidoEdicion.observaciones_baja} style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " />
                                    </div>

                                </div>
                            }


                        </div>


                    </div>
                </div>

                <hr></hr>
                <div className="row">
                    <div className="col-xs-4 col-lg-4">
                        <div className="form-group">

                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >ID usuario</span>
                                </span> <input type="text" name="usuario" maxlength="20" value={this.state.contenidoEdicion.usuario} disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                    && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                        && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} style={{ borderColor: '#E1E5F0' }} className="form-control " tabIndex={4} />
                            </div>
                            {
                                this.state.contenidoEdicion.usuario == null || this.state.contenidoEdicion.usuario == "" ?
                                    <span className="" style={{ color: "red" }}>El Id Usuario es un campo requerido</span> : ''
                            }

                        </div>

                    </div>
                    <div className="col-xs-4 col-lg-4">
                        <div className="form-group">

                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Contraseña</span>
                                </span> <input type="password" name="password" maxlength="20" value={this.state.contenidoEdicion.password} disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                    && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                        && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} style={{ borderColor: '#E1E5F0' }} className="form-control " tabIndex={4} />
                            </div>
                            {
                                this.state.contenidoEdicion.password == null || this.state.contenidoEdicion.password == "" ?
                                    <span className="" style={{ color: "red" }}>La contraseña es un campo requerido</span> : ''
                            }

                        </div>
                    </div>
                    <div className="col-xs-4 col-lg-4">
                        <div className="form-group">

                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0' }} >Perfil</span>
                                </span> {this.state.selectRol}
                            </div>
                            {
                                this.state.contenidoEdicion.rol_id == null || this.state.contenidoEdicion.rol_id == 0 ?
                                    <span className="" style={{ color: "red" }}>El rol es un campo requerido</span> : ''
                            }

                        </div>
                    </div>

                </div>


                <div>
                    <div className="row">

                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Puesto</span>
                                    </span> {this.state.selectPuesto}
                                    {/*<input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />*/}
                                </div>
                                {
                                    this.state.contenidoEdicion.puesto_id == 0 || this.state.contenidoEdicion.puesto_id == null ?
                                        <span className="" style={{ color: "red" }}>El campo puesto es un campo requerido</span> : ''
                                }
                            </div>
                        </div>

                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">


                                <div className="input-group">
                                    <form encType="multipart/form" style={{ display: 'none' }} >
                                        <input type="file" style={{ display: 'none' }} ref={(ref) => this.upload = ref} onChange={this.onChangeFilePuesto.bind(this)}></input>
                                    </form>
                                    <input type="text" style={{ borderColor: '#E1E5F0' }} disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                        && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} className="form-control " placeholder="Descripción del puesto" value={this.state.nombreArchivoPuesto} />
                                    <span className="input-group-prepend">
                                        <button type="button" class="btn text-white" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                                && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onClick={this.onClickBotonArchivoPuesto} style={{ backgroundColor: this.state.colorBotonSubirArchivo }}>
                                            <h10 style={{ color: this.state.colorSubirArchivo }}>+</h10>
                                        </button>

                                        {
                                            this.state.muestraDocPuesto == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivo }}>
                                                <a href={this.state.contenidoEdicion.doc_puesto} target="_blank" rel="noopener noreferrer" >
                                                    <i className="fas fa-eye"
                                                        style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                </a>
                                            </button> : ''

                                        }



                                        <span className="input-group-text" style={{ background: this.state.colorSubirArchivo, borderColor: this.state.colorSubirArchivo, color: this.state.colorTextoSubirArchivo }} >{this.state.textoPuesto}</span>
                                    </span>
                                </div>
                                {
                                    this.state.nombreArchivoPuesto == "" || this.state.nombreArchivoPuesto == null ?
                                        <span className="" style={{ color: "red" }}>El campo Descripción del puesto es un campo requerido</span> : ''
                                }
                            </div>
                        </div>

                        <div className="col-xs-4 col-lg-4">
                            <div className="form-check">
                                <div className="input-group">
                                    <input style={{ borderColor: '#E1E5F0' }} class="form-check-input" type="checkbox" name="acceso_pai" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                        && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} checked={this.state.contenidoEdicion.acceso_pai} id="flexCheckDisabled" />
                                    <label class="form-check-label" for="flexCheckDisabled">
                                        Accesso a PAI/DA
                                    </label>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <br></br>


                <div className="row">
                    <div className="col-xs-4 col-lg-4">
                        <div className="row">
                            {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>} <label >Generación de la figura comercial</label>
                        </div>
                        <div className="row">
                            {<>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>}
                            <div class="form-check">
                                <input class="form-check-input" type="radio" value={1} disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                    && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                        && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} id="G1" name="figura_id" checked={this.state.contenidoEdicion.figura_id == 1} />
                                <label class="form-check-label" for="G1">
                                    G1
                                </label>
                            </div>
                            {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                            <div class="form-check">
                                <input class="form-check-input" type="radio" value={2} disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                    && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                        && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name="figura_id" id="G2" checked={this.state.contenidoEdicion.figura_id == 2} />
                                <label class="form-check-label" for="G2">
                                    G2
                                </label>
                            </div>

                            {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                            <div class="form-check">
                                <input class="form-check-input" type="radio" value={3} disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                    && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                        && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name="figura_id" id="G3" checked={this.state.contenidoEdicion.figura_id == 3} />
                                <label class="form-check-label" for="G3">
                                    G3
                                </label>
                            </div>
                            {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                            <div class="form-check">
                                <input class="form-check-input" type="radio" value={4} disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                    && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                        && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name="figura_id" id="G4" checked={this.state.contenidoEdicion.figura_id == 4} />
                                <label class="form-check-label" for="G4">
                                    G4
                                </label>
                            </div>
                            {<>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                            <div class="form-check">
                                <input class="form-check-input" type="radio" value={5} disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                    && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                        && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} name="figura_id" id="C" checked={this.state.contenidoEdicion.figura_id == 5} />
                                <label class="form-check-label" for="C">
                                    C
                                </label>
                            </div>
                        </div>
                        {
                            this.state.banpuesto && (this.state.contenidoEdicion.figura_id == 0 || this.state.contenidoEdicion.figura_id == null) ?
                                <span style={{ color: "red" }}>Generación de la figura comercial debe ser seleccionada</span> : ''
                        }
                    </div>

                    <div className="col-xs-4 col-lg-4">
                        <div className="form-group">

                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Área o departamento</span>
                                </span>{this.state.selectArea}
                                {/*<input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " />*/}
                            </div>
                            {
                                this.state.contenidoEdicion.area_id == 0 || this.state.contenidoEdicion.area_id == null ?
                                    <span className="" style={{ color: "red" }}>El campo Área o departamento es un campo requerido</span> : ''
                            }
                        </div>
                    </div>

                    <div className="col-xs-4 col-lg-4">
                        <div className="form-group">

                            <div className="input-group">
                                <span className="input-group-prepend"> <span
                                    className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Supervisor directo</span>
                                </span> {/*<input type="text" maxlength="150" style={{ borderColor: '#E1E5F0' }} className="form-control " name="supervisor" onChange={this.onChange} value={this.state.contenidoEdicion.supervisor} />*/}
                                {this.state.selectSupervisor}
                            </div>
                            {
                                this.state.contenidoEdicion.supervisor == null || this.state.contenidoEdicion.supervisor <= 0 || (this.state.contenidoEdicion.supervisor + '').length == 0 ?
                                    <span className="" style={{ color: "red" }}>El campo Supervisor directo es un campo requerido</span> : ''
                            }
                        </div>
                    </div>


                </div>

                <br></br>
                <div>
                    <div className="row">
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">


                                <div className="input-group">
                                    <form encType="multipart/form" style={{ display: 'none' }} >
                                        <input type="file" style={{ display: 'none' }} ref={(ref) => this.cv = ref} onChange={this.onChangeFileCV.bind(this)}></input>
                                    </form>
                                    <input type="text" style={{ borderColor: '#E1E5F0' }} disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                        && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} className="form-control " placeholder="Curriculum Vitae" value={this.state.nombreArchivoCV} />
                                    <span className="input-group-prepend">
                                        <button type="button" class="btn text-white" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                                && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onClick={this.onClickBotonArchivoCV} style={{ backgroundColor: this.state.colorBotonSubirArchivoCV }}>
                                            <h10 style={{ color: this.state.colorSubirArchivoCV }}>+</h10>
                                        </button>

                                        {
                                            this.state.muestraDocCV == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivo }}>
                                                <a href={this.state.contenidoEdicion.doc_cv} target="_blank" rel="noopener noreferrer" >
                                                    <i className="fas fa-eye"
                                                        style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                </a>
                                            </button>
                                                : ''
                                        }

                                        <span className="input-group-text" style={{ background: this.state.colorSubirArchivoCV, borderColor: this.state.colorSubirArchivoCV, color: this.state.colorTextoSubirArchivoCV }} >Subir archivo</span>
                                    </span>
                                </div>


                                {/*<div className="input-group">
                                    <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " placeholder="Curriculum Vitae" /> <span className="input-group-prepend">
                                        <button type="button" class="btn text-white" style={{ backgroundColor: "#0F69B8" }}>+</button>
                                        <span className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Subir archivo</span></span>
                                 </div>*/}






                            </div>

                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }}>Banco</span>
                                    </span>{this.state.selectBancos}
                                    {/* <input type="text" style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " />*/}
                                </div>
                                {
                                    this.state.contenidoEdicion.banco_id == 0 || this.state.contenidoEdicion.banco_id == null ?
                                        <span className="" style={{ color: "red" }}>El campo banco es un campo requerido</span> : ''
                                }
                            </div>
                        </div>

                        <div className="col-xs-4 col-lg-4">

                            <div className="form-group">

                                {/* <div className="input-group">
                                    <input type="text" style={{ borderColor: '#E1E5F0' }} className="form-control " placeholder="PSP" /> <span className="input-group-prepend">
                                        <button type="button" class="btn text-white" style={{ backgroundColor: "#0F69B8" }}>+</button>
                                        <span className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Subir archivo</span></span>
                                </div>*/}
                                <div className="input-group">
                                    <form encType="multipart/form" style={{ display: 'none' }} >
                                        <input type="file" style={{ display: 'none' }} ref={(ref) => this.psp = ref} onChange={this.onChangeFilePSP.bind(this)}></input>
                                    </form>
                                    <input type="text" style={{ borderColor: '#E1E5F0' }} disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                        && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} className="form-control " placeholder="PSP" value={this.state.nombreArchivoPSP} />
                                    <span className="input-group-prepend">
                                        <button type="button" class="btn text-white" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                                && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onClick={this.onClickBotonArchivoPSP} style={{ backgroundColor: this.state.colorBotonSubirArchivoPSP }}>
                                            <h10 style={{ color: this.state.colorSubirArchivoPSP }}>+</h10>
                                        </button>

                                        {
                                            this.state.muestraDocPSP == true ? <button type="button" class="btn text-white" style={{ backgroundColor: this.state.colorBotonSubirArchivo }}>
                                                <a href={this.state.contenidoEdicion.doc_psp} target="_blank" rel="noopener noreferrer" >
                                                    <i className="fas fa-eye"
                                                        style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i>
                                                </a>
                                            </button> : ''

                                        }


                                        <span className="input-group-text" style={{ background: this.state.colorSubirArchivoPSP, borderColor: this.state.colorSubirArchivoPSP, color: this.state.colorTextoSubirArchivoPSP }} >Subir archivo</span>
                                    </span>
                                </div>
                                {
                                    this.state.nombreArchivoPSP == null || this.state.nombreArchivoPSP == "" ?
                                        <span className="" style={{ color: "red" }}>El campo PSP es un campo requerido</span> : ''
                                }
                            </div>

                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }}>Clave interbancaria</span>
                                    </span> <input type="number" style={{ borderColor: '#E1E5F0', opacity: 1 }} placeholder="18 dígitos" name="cta_clabe" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                        && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} value={this.state.contenidoEdicion.cta_clabe} className="form-control " />
                                </div>
                                {
                                    this.state.contenidoEdicion.cta_clabe == 0 || (this.state.contenidoEdicion.cta_clabe + "").length <= 17 ?
                                        <span className="" style={{ color: "red" }}>El campo clave interbancaria es un campo requerido</span> : ''
                                }
                            </div>

                        </div>

                        <div className="col-xs-4 col-lg-4">

                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-prepend">
                                        <span className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Sueldo $</span></span>
                                    <input type="number" step="0.00" placeholder="Sueldo nominal mensual $0.00" style={{ borderColor: '#E1E5F0' }} name="sueldo" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                        && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} value={this.state.contenidoEdicion.sueldo} className="form-control " />
                                </div>
                                {
                                    this.state.contenidoEdicion.sueldo == 0 ?
                                        <span className="" style={{ color: "red" }}>El campo sueldo es un campo requerido</span> : ''
                                }
                            </div>

                            <div className="form-group">
                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }}>Sociedad a la que pertenece</span>
                                    </span>{this.state.selectSociedad}
                                </div>
                                {
                                    this.state.contenidoEdicion.sociedad_id == 0 || this.state.contenidoEdicion.sociedad_id == null ?
                                        <span className="" style={{ color: "red" }}>La Sociedad a la que pertenece ses un campo requerido</span> : ''
                                }
                            </div>

                        </div>


                    </div>
                </div>


                <hr></hr>
                <div>
                    <div className="row">
                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >@</span>
                                    </span> <input type="text" maxlength="100" style={{ borderColor: '#E1E5F0' }} placeholder="Correo electrónico asignado" name="correo_electronico" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                        && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} value={this.state.contenidoEdicion.correo_electronico} className="form-control " />
                                </div>
                                {
                                    this.state.validaEmail == false ?
                                        <span className="" style={{ color: "red" }}>El mail es un campo requerido</span> : ''
                                }
                            </div>

                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }}>No. extensión</span>
                                    </span> <input type="number" style={{ borderColor: '#E1E5F0', opacity: 1 }} name="num_extension" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                        && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} value={this.state.contenidoEdicion.num_extension} className="form-control " />
                                </div>
                                {
                                    /*this.state.contenidoEdicion.num_extension == null || this.state.contenidoEdicion.num_extension == false ?
                                        <span className="" style={{ color: "red" }}>El número de extensión es un campo requerido</span> : ''
                                */}
                            </div>
                        </div>

                        <div className="col-xs-4 col-lg-4">
                            <div className="form-group">

                                <div className="input-group">
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >@</span>
                                    </span> <input type="text" maxlength="100" style={{ borderColor: '#E1E5F0' }} placeholder="Otros correos" className="form-control " name="otros_correos" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                        && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} value={this.state.contenidoEdicion.otros_correos} />
                                </div>
                                {/*
                                    this.state.validaOtrosCorreos == false ?
                                        <span className="" style={{ color: "red" }}>Otros correos es un campo requerido</span> : ''
                            */}

                            </div>

                            <div className="form-group">

                                <div className="input-group">
                                    <button type="button" onClick={this.disminuyeVacaciones} disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                        && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} class="btn text-white" style={{ backgroundColor: "#0F69B8" }}>-</button>
                                    <span className="input-group-prepend"> <span
                                        className="input-group-text  " style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }}>Días de vacaciones</span>
                                    </span> <input type="number" name="dias_vacaciones" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                        && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} value={this.state.contenidoEdicion.dias_vacaciones} style={{ borderColor: '#E1E5F0', opacity: 1 }} className="form-control " />
                                    <button type="button" class="btn text-white" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                        && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                            && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onClick={this.aumentaVacaciones} style={{ backgroundColor: "#0F69B8" }}>+</button>
                                </div>
                                {
                                    this.state.contenidoEdicion.dias_vacaciones == null || this.state.contenidoEdicion.dias_vacaciones <= 0 || (this.state.contenidoEdicion.dias_vacaciones + '').length == 0 ?
                                        <span className="" style={{ color: "red" }}>Los días de vacacion es un campo requerido</span> : ''
                                }
                            </div>
                        </div>


                        <div className="col-xs-4 col-lg-4">
                            {this.state.contenidoEdicion.causabaja_id != 0 && this.state.contenidoEdicion.estatusbaja_id == 2 &&
                                <div className="form-group">
                                    <div className="input-group">
                                        <span className="input-group-prepend"> <span
                                            className="input-group-text" style={{ background: '#E1E5F0 0% 0% no-repeat padding-box', borderColor: '#E1E5F0', color: '#617187' }} >Fecha de baja de los correos</span>
                                        </span> <input type="date" style={{ borderColor: '#E1E5F0' }} className="form-control " name="fecha_baja_correos" max="2100-01-01" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                                && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} value={"" + this.state.contenidoEdicion.fecha_baja_correos + ""} />
                                    </div>
                                    {
                                        this.state.contenidoEdicion.fecha_baja_correos == "" || this.state.contenidoEdicion.fecha_baja_correos == null ?
                                            <span className="" style={{ color: "red" }}>El campo fecha de baja de los correos es un campo requerido</span> : ''
                                    }
                                </div>
                            }

                            <div className="row">
                                <div className="col-xs-4 col-lg-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" checked={this.state.contenidoEdicion.acceso_impresion} name="acceso_impresion" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                                && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} id="acceso_impresion" />
                                        <label class="form-check-label" for="acceso_impresion">
                                            Acceso a impresión
                                        </label>
                                    </div>
                                </div>

                                <div className="col-xs-6 col-lg-6">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" checked={this.state.contenidoEdicion.acceso_copiadora} name="acceso_copiadora" disabled={(this.props.idEmpleado != null || this.props.tipoUsuario != "ANAJR")
                                            && (this.props.idEmpleado != null || this.props.tipoUsuario != "ASESJRADM") && (this.props.tipoUsuario != "COORADM" && this.props.tipoUsuario != "GTEADM"
                                                && this.props.tipoUsuario != "SUPER" && this.props.tipoUsuario != "IT")} onChange={this.onChange} id="acceso_copiadora" />
                                        <label class="form-check-label" for="acceso_copiadora">
                                            Acceso a copiadora
                                        </label>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>


        )
    }
}