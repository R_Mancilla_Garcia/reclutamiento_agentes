import React, { Component } from "react";
import TarjetaProcesoReclutamiento from "../tarjetas/TarjetaProcesoReclutamiento";
import EntrevistaProfunda from "../formsReclutamiento/EntrevistaProfunda";
import RegistroEntrevista from "../formsReclutamiento/RegistroEntrevista";
import VentaDeCarrera from "../formsReclutamiento/VentaDeCarrera";
import CapacitacionConexion from "../formsReclutamiento/CapacitacionConexion";
import BajaReservaForm from "../formsReclutamiento/BajaReservaForm";
import Conectado from "../formsReclutamiento/Conectado";
import ConoceCandidatoForm from "../formsReclutamiento/ConoceCandidatoForm";
import datos from '../urls/datos.json'

const queryParams = new URLSearchParams(window.location.search)

export default class ProcesoReclutamiento extends Component {

    constructor() {
        super();
        this.mostrarEntrevistaProfunda = this.mostrarEntrevistaProfunda.bind(this)
        this.mostrarRegistroEntrevista = this.mostrarRegistroEntrevista.bind(this)
        this.mostrarVentaDeCarrera = this.mostrarVentaDeCarrera.bind(this)
        this.botonCancelar = this.botonCancelar.bind(this)
        this.construyeTarjetas = this.construyeTarjetas.bind(this)
        this.enviarReserva = this.enviarReserva.bind(this)
        this.reanudarReserva = this.reanudarReserva.bind(this)
        this.enviarBaja = this.enviarBaja.bind(this)
        this.mostrarConexionCapacitacion = this.mostrarConexionCapacitacion.bind(this)
        this.onChange = this.onChange.bind(this)
        this.peticionABaja = this.peticionABaja.bind(this)
        this.peticionAReserva = this.peticionAReserva.bind(this)
        this.mostrarBajaReserva = this.mostrarBajaReserva.bind(this)
        this.mostrarConectado = this.mostrarConectado.bind(this)
        this.mostrarConoceTuCandidato = this.mostrarConoceTuCandidato.bind(this)
        this.determinaEstatusReanudarReserva = this.determinaEstatusReanudarReserva.bind(this)
        this.enviaPeticionReAsignacion = this.enviaPeticionReAsignacion.bind(this)
        this.state = {
            idSeleccionado: null,
            muestraTarjetas: false,
            muestraProgramacionEntrevista: false,
            muestraEntrevistaProfunda: false,
            muestraConoceTuCandidato: false,
            muestraVentaDeCarrera: false,
            muestraConexionCapacitacion: false,
            muestraConectado: false,
            muestraBajaReservaForm: false,
            selectMotivosReserva: [],
            selectMotivosBaja: [],
            enRegistro: [],
            entrevistaProfunda: [],
            conoceTuCandidato: [],
            ventaDeCarrera: [],
            capacitacion: [],
            conectado: [],
            enReserva: [],
            baja: [],
            idMotivoBaja: 0,
            idMotivoReserva: 0,
            idCambiaEstatus: null,
            tipoUsuario: null,
            usuario:null,
            nombreUsuario: null,
            idUsuario: null
        };
    }



    UNSAFE_componentWillMount() {
        let tipoUsuario = queryParams.get("tipoUsuario")
        let usuario=queryParams.get("usuario")
        let nombreUsuario = queryParams.get("nombreUsuario")
        let idUsuario = queryParams.get("idUsuario")
        this.setState({ tipoUsuario: tipoUsuario, nombreUsuario: nombreUsuario, usuario:usuario ,idUsuario:idUsuario})
        this.construyeSelectMotivos(datos.urlServicePy+"parametros/api_cat_motivo_envio_reserva/0", "selectMotivosReserva")
        this.construyeSelectMotivos(datos.urlServicePy+"parametros/api_cat_causas_baja_prospecto/0", "selectMotivosBaja")

        this.construyeTarjetas()
    }



    construyeTarjetas() {
        fetch(datos.urlServicePy+"recluta/api_vprospectos/0")
            .then(response => response.json())
            .then(arrayContenido => {
                let enRegistro = []
                let entrevistaProfunda = []
                let ventaDeCarrera = []
                let conoceTuCandidato = []
                let capacitacion = []
                let conectado = []
                let enReserva = []
                let baja = []
                let bandera = false
                let ace_edat = 0
                this.setState({ enRegistro: [], entrevistaProfunda: [], capacitacion: [], ventaDeCarrera: [], conectado: [], enReserva: [], baja: [], muestraTarjetas: false })
                console.log("contenido del servicio ", arrayContenido)

                fetch(datos.urlServicePy+"parametros/api_cat_empleados/0")
                    .then(response => response.json())
                    .then(data => {
                        let fila = data.filas;
                        for (var i = 0; i < fila.length; i++) {
                            if (data.filas[i].fila[56].value == this.state.usuario) {
                                ace_edat = data.filas[i].fila[0].value
                                bandera = true
                            }
                        }
                        if(bandera){
                            for (var i = 0; i < arrayContenido.length; i++) {
                                if(ace_edat == arrayContenido[i].edat_id || ace_edat == arrayContenido[i].gdd_id || ace_edat == arrayContenido[i].sddc_id || ace_edat == arrayContenido[i].sas_id  ||  this.state.tipoUsuario== "GTEDR" || this.state.tipoUsuario== "DIR" 
                                || this.state.tipoUsuario== "DOP" || this.state.tipoUsuario == "SDDC" ||this.state.tipoUsuario== "COORCAP" || this.state.tipoUsuario== "COORADM" || this.state.tipoUsuario== "SUPER" || this.state.tipoUsuario== "ASRSIS" || this.state.usuario == "REAZ970917" || this.state.usuario == "PAMM921017"
                                    ){
                                        console.log("contenido de la vista prospctos ",arrayContenido[i])
                                    if (arrayContenido[i].estatus_id == 1 &&  this.state.usuario != "REAZ970917" &&  this.state.usuario != "PAMM921017" && this.state.tipoUsuario != "COORCAP" && this.state.tipoUsuario != "COORADM" && this.state.tipoUsuario != "ASRSIS"  ) {//estatus en registro
                                        enRegistro.push(arrayContenido[i])
                                    }
                                    if (arrayContenido[i].estatus_id == 4  &&  this.state.usuario != "REAZ970917" &&  this.state.usuario != "PAMM921017" && this.state.tipoUsuario != "COORCAP" && this.state.tipoUsuario != "COORADM" && this.state.tipoUsuario != "ASRSIS") {//esta  tus REgistrado
                                        entrevistaProfunda.push(arrayContenido[i])
                                    }
                                    if (arrayContenido[i].estatus_id == 16 &&(this.state.usuario != "REAZ970917" &&  this.state.usuario != "PAMM921017" && this.state.tipoUsuario != "COORADM" && this.state.tipoUsuario != "ASRSIS") && ( this.state.tipoUsuario != "COORCAP" ) && ( this.state.tipoUsuario== "GTEDR" || this.state.tipoUsuario == "SDDC"|| this.state.tipoUsuario== "DOP" || this.state.tipoUsuario== "DIR" || this.state.tipoUsuario =="GTEDES" || this.state.tipoUsuario == "ASESRSR" || this.state.tipoUsuario == "ASESRJR")) {//estatus Venta de carrera
                                        ventaDeCarrera.push(arrayContenido[i])
                                    }
                                    if (arrayContenido[i].estatus_id == 22 &&  ( this.state.tipoUsuario != "COORCAP" && this.state.tipoUsuario != "COORADM" && this.state.tipoUsuario != "ASRSIS") && (this.state.usuario == "REAZ970917" || this.state.tipoUsuario == "SDDC" || this.state.tipoUsuario== "DOP" || this.state.tipoUsuario== "DIR" || this.state.tipoUsuario== "SASSR" || this.state.tipoUsuario== "SASJR"  ||  this.state.tipoUsuario == "ASESRSR" || this.state.tipoUsuario == "ASESRJR" || this.state.tipoUsuario== "GTEDR" || this.state.tipoUsuario =="GTEDES" || this.state.tipoUsuario == "GTECAP")) {//estatus capacitacion
                                        capacitacion.push(arrayContenido[i]) 
                                    }
                                    if (arrayContenido[i].estatus_id == 24 &&  ( this.state.tipoUsuario != "COORCAP" && this.state.tipoUsuario != "COORADM" && this.state.tipoUsuario != "ASRSIS") && (this.state.usuario != "REAZ970917") && (this.state.usuario != "PAMM921017") && ( this.state.tipoUsuario== "GTEDR" || this.state.tipoUsuario == "SDDC" || this.state.tipoUsuario== "DOP" || this.state.tipoUsuario== "DIR" ||  this.state.tipoUsuario =="GTEDES" || this.state.tipoUsuario == "ASESRSR" || this.state.tipoUsuario == "ASESRJR"))  {//estatus conoce tu candidato
                                        console.log("contenido de la vista prospctos conociendo ",arrayContenido[i])
                                        conoceTuCandidato.push(arrayContenido[i])
                                    }
                                    if (arrayContenido[i].estatus_id == 17 && (this.state.usuario == "REAZ970917" ||  this.state.tipoUsuario == "SDDC" ||this.state.tipoUsuario== "DOP" || this.state.tipoUsuario== "ASESJRCAP" || this.state.tipoUsuario== "SDDC" ||  this.state.tipoUsuario== "GTEDR" ||  this.state.tipoUsuario== "DIR" ||
                                    this.state.tipoUsuario== "GTEDES" || this.state.tipoUsuario== "SASSR" || this.state.tipoUsuario== "SASJR"  || this.state.usuario == "CURA970216" || this.state.tipoUsuario== "COORCAP" || 
                                    this.state.usuario == "RASC730128" || this.state.tipoUsuario== "ASRSIS" || this.state.usuario == "CATA770718" || this.state.tipoUsuario== "COORADM" || this.state.tipoUsuario == "GTECAP"))  {//estatus Conectado
                                        conectado.push(arrayContenido[i])
                                    }
                                    if (arrayContenido[i].estatus_id == 18) {//estatus En reserva
                                        enReserva.push(arrayContenido[i])
                                    }
                                    if (arrayContenido[i].estatus_id == 19) {//estatus Baja
                                        baja.push(arrayContenido[i])
                                    }
                                }
                            }
                        }
                        this.setState({
                            enRegistro: enRegistro, entrevistaProfunda: entrevistaProfunda, capacitacion: capacitacion, ventaDeCarrera: ventaDeCarrera, conectado: conectado, conoceTuCandidato: conoceTuCandidato,
                            enReserva: enReserva, baja: baja, muestraTarjetas: true
                        })
                    })
            })
    }


    enviarReserva(id) {
        console.log("Entro en reserva ::");
        this.setState({ idCambiaEstatus: id })
        this.reserva.click()
    }


    reanudarReserva(id) {
        console.log("Entro en reanudar reserva ::", id);
        fetch(datos.urlServicePy+"parametros/api_recluta_prospectos/" + id)
            .then(response => response.json())
            .then(pros => {
                let banderaString = ''
                let fech_EntrevistaProfunda = pros.filas[0].fila[21].value
                console.log("fecha de la entrevista ", fech_EntrevistaProfunda)
                if (fech_EntrevistaProfunda == null) {
                    console.log("se queda hasta entrevista")
                    banderaString = "registroEntrevista"
                    this.determinaEstatusReanudarReserva(banderaString, id)
                } else {
                    fetch(datos.urlServicePy+"recluta/api_recluta_encuentro01/" + id)
                        .then(response => response.json())
                        .then(encuentro01 => {

                            if (encuentro01.length == 0) {
                                console.log("se queda hasta encuentro 01")
                                banderaString = "encuentro01"
                                this.determinaEstatusReanudarReserva(banderaString, id)
                            } else {
                                fetch(datos.urlServicePy+"recluta/api_recluta_encuentro02/" + id)
                                    .then(response => response.json())
                                    .then(encuentro02 => {
                                        if (encuentro02.length == 0) {
                                            console.log("se queda hasta encuentro 02")
                                            banderaString = "encuentro02"
                                            this.determinaEstatusReanudarReserva(banderaString, id)
                                        } else {
                                            fetch(datos.urlServicePy+"recluta/api_recluta_encuentro03/" + id)
                                                .then(response => response.json())
                                                .then(encuentro03 => {
                                                    if (encuentro03.length == 0) {
                                                        console.log("se queda hasta encuentro 03")
                                                        banderaString = "encuentro03"
                                                        this.determinaEstatusReanudarReserva(banderaString, id)
                                                    } else {
                                                        fetch(datos.urlServicePy+"recluta/api_recluta_encuentro04/" + id)
                                                            .then(response => response.json())
                                                            .then(encuentro04 => {
                                                                if (encuentro04.length == 0) {
                                                                    console.log("se queda hasta encuentro 04")
                                                                    banderaString = "encuentro04"
                                                                    this.determinaEstatusReanudarReserva(banderaString, id)
                                                                } else {
                                                                    fetch(datos.urlServicePy+"recluta/api_recluta_expagente05/" + id)
                                                                        .then(response => response.json())
                                                                        .then(encuentro05 => {
                                                                            fetch(datos.urlServicePy+"recluta/api_recluta_areasvida06/" + id)
                                                                                .then(response => response.json())
                                                                                .then(encuentro06 => {
                                                                                    if (encuentro05.length == 0 && encuentro06.length == 0) {
                                                                                        console.log("se queda hasta encuentro 05")
                                                                                        banderaString = "encuentro05"
                                                                                        this.determinaEstatusReanudarReserva(banderaString, id)
                                                                                    } else if ((encuentro05.length == 0 && encuentro06.length > 0) || (encuentro05.length > 0 && encuentro06.length > 0)) {
                                                                                        fetch(datos.urlServicePy+"recluta/api_recluta_evaluacion07/" + id)
                                                                                            .then(response => response.json())
                                                                                            .then(encuentro07 => {
                                                                                                if (encuentro07.length == 0) {
                                                                                                    console.log("se queda hasta encuentro 07")
                                                                                                    banderaString = "encuentro07"
                                                                                                    this.determinaEstatusReanudarReserva(banderaString, id)
                                                                                                } else {
                                                                                                    fetch(datos.urlServicePy+"recluta/api_recluta_vitales08/" + id)
                                                                                                        .then(response => response.json())
                                                                                                        .then(encuentro08 => {
                                                                                                            if (encuentro08.length == 0) {
                                                                                                                console.log("se queda hasta encuentro 08")
                                                                                                                banderaString = "encuentro08"
                                                                                                                this.determinaEstatusReanudarReserva(banderaString, id)
                                                                                                            } else {
                                                                                                                fetch(datos.urlServicePy+"recluta/api_recluta_factoresimp09/" + id)
                                                                                                                    .then(response => response.json())
                                                                                                                    .then(encuentro09 => {
                                                                                                                        if (encuentro09.length == 0) {
                                                                                                                            console.log("se queda hasta encuentro 09")
                                                                                                                            banderaString = "encuentro09"
                                                                                                                            this.determinaEstatusReanudarReserva(banderaString, id)
                                                                                                                        } else {
                                                                                                                            fetch(datos.urlServicePy+"recluta/api_recluta_knockout10/" + id)
                                                                                                                                .then(response => response.json())
                                                                                                                                .then(encuentro10 => {
                                                                                                                                    if (encuentro10.length == 0) {
                                                                                                                                        console.log("se queda hasta encuentro 10")
                                                                                                                                        banderaString = "encuentro10"
                                                                                                                                        this.determinaEstatusReanudarReserva(banderaString, id)
                                                                                                                                    } else {
                                                                                                                                        fetch(datos.urlServicePy+"recluta/api_conociendo_candidato/" + id)
                                                                                                                                            .then(response => response.json())
                                                                                                                                            .then(conociendo => {
                                                                                                                                                if (conociendo.length == 0) {
                                                                                                                                                    banderaString = "conociendo"
                                                                                                                                                    this.determinaEstatusReanudarReserva(banderaString, id)
                                                                                                                                                } else {
                                                                                                                                                    fetch(datos.urlServicePy+"recluta/api_recluta_ventacarrera16/" + id)
                                                                                                                                                        .then(response => response.json())
                                                                                                                                                        .then(ventaDeCarrera => {
                                                                                                                                                            if (ventaDeCarrera.length == 0) {
                                                                                                                                                                banderaString = "ventaDeCarrera"
                                                                                                                                                                this.determinaEstatusReanudarReserva(banderaString, id)
                                                                                                                                                            } else {
                                                                                                                                                                fetch(datos.urlServicePy+"recluta/api_recluta_conexion17/" + id)
                                                                                                                                                                    .then(response => response.json())
                                                                                                                                                                    .then(conexion => {
                                                                                                                                                                        if (conexion.length == 0) {
                                                                                                                                                                            banderaString = "conexion"
                                                                                                                                                                            this.determinaEstatusReanudarReserva(banderaString, id)
                                                                                                                                                                        } else if (conexion[0].correo_aps == null && conexion[0].fecha_fin_tarjetas == null && conexion[0].fecha_alta_boletin == null && conexion[0].fecha_plenarias_aps == null && conexion[0].fecha_alta_chats == null && conexion[0].alta_chats_noveles == null && conexion[0].alta_agenda_aps == null) {
                                                                                                                                                                            banderaString = "conexion"
                                                                                                                                                                            this.determinaEstatusReanudarReserva(banderaString, id)
                                                                                                                                                                        } else {
                                                                                                                                                                            fetch(datos.urlServicePy+"recluta/api_agente_expediente/" + id)
                                                                                                                                                                                .then(response => response.json())
                                                                                                                                                                                .then(jsonExpediente => {
                                                                                                                                                                                    banderaString = "conectado"
                                                                                                                                                                                    this.determinaEstatusReanudarReserva(banderaString, id)
                                                                                                                                                                                })
                                                                                                                                                                        }
                                                                                                                                                                    })
                                                                                                                                                            }
                                                                                                                                                        })
                                                                                                                                                }
                                                                                                                                            })
                                                                                                                                    }
                                                                                                                                })
                                                                                                                        }

                                                                                                                    })
                                                                                                            }
                                                                                                        })
                                                                                                }
                                                                                            })
                                                                                    }
                                                                                })


                                                                        })

                                                                }
                                                            })
                                                    }
                                                })
                                        }

                                    })
                            }
                        })
                }

            })// fin de api_recluta_prospectos 

    }


    determinaEstatusReanudarReserva(banderaString, id) {
        console.log("el resultado de la bandera ", banderaString)
        if (banderaString == "encuentro01" || banderaString == "encuentro02" || banderaString == "encuentro03" || banderaString == "encuentro04" || banderaString == "encuentro05" || banderaString == "encuentro06"
            || banderaString == "encuentro07" || banderaString == "encuentro08" || banderaString == "encuentro09" || banderaString == "encuentro10") {
            let json = { "estatus_id": 4 }
            this.enviaPeticionReAsignacion(json, id)
        } else if (banderaString == "conociendo") {
            let json = { "estatus_id": 24 }
            this.enviaPeticionReAsignacion(json, id)
        } else if (banderaString == "ventaDeCarrera") {
            let json = { "estatus_id": 16 }
            this.enviaPeticionReAsignacion(json, id)
        } else if (banderaString == "conexion") {
            let json = { "estatus_id": 22 }
            this.enviaPeticionReAsignacion(json, id)
        } else if (banderaString == "conectado") {
            let json = { "estatus_id": 17 }
            this.enviaPeticionReAsignacion(json, id)
        }
    }

    enviaPeticionReAsignacion(json, id) {
        const requestOptions = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };
        fetch(datos.urlServicePy+"parametros_upd/api_recluta_prospectos/" + id, requestOptions)
            .then(response => response.json())
            .then(data => {
                this.construyeTarjetas()
                this.botonCancelar()
            })
    }

    peticionAReserva() {
        let json = {
            "estatus_id": 18,
            "causas_baja_id": this.state.idMotivoReserva

        }
        const requestOptions = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };
        console.log("enviando a reserva")

        fetch(datos.urlServicePy+"parametros_upd/api_recluta_prospectos/" + this.state.idCambiaEstatus, requestOptions)
            .then(response => response.json())
            .then(data => {
                this.construyeTarjetas()
                this.botonCancelar()
            })

    }
    enviarBaja(id) {
        this.setState({ idCambiaEstatus: id })
        this.baja.click()
    }
    peticionABaja() {
        let json = {
            "estatus_id": 19,
            "causas_baja_id": this.state.idMotivoBaja
        }
        const requestOptions = {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };
        console.log("enviando a reserva")

        fetch(datos.urlServicePy+"parametros_upd/api_recluta_prospectos/" + this.state.idCambiaEstatus, requestOptions)
            .then(response => response.json())
            .then(data => {
                this.construyeTarjetas()
                this.botonCancelar()
            })
    }


    mostrarBajaReserva(idSeleccionado) {
        this.setState({
            muestraBajaReservaForm: true,
            muestraConexionCapacitacion: false, muestraVentaDeCarrera: false,
            muestraTarjetas: false, muestraProgramacionEntrevista: false, muestraConoceTuCandidato: false,
            muestraEntrevistaProfunda: false, idSeleccionado: idSeleccionado
        })

    }
    mostrarConectado(idSeleccionado) {

        this.setState({ muestraConectado: true, muestraBajaReservaForm: false, muestraVentaDeCarrera: false, muestraTarjetas: false, muestraProgramacionEntrevista: false, muestraEntrevistaProfunda: false, muestraConoceTuCandidato: false, idSeleccionado: idSeleccionado })

    }

    mostrarConexionCapacitacion(idSeleccionado) {

        this.setState({ muestraConexionCapacitacion: true, muestraBajaReservaForm: false, muestraVentaDeCarrera: false, muestraTarjetas: false, muestraProgramacionEntrevista: false, muestraEntrevistaProfunda: false, muestraConoceTuCandidato: false, idSeleccionado: idSeleccionado })

    }

    mostrarVentaDeCarrera(idSeleccionado) {

        this.setState({ muestraVentaDeCarrera: true, muestraBajaReservaForm: false, muestraTarjetas: false, muestraProgramacionEntrevista: false, muestraEntrevistaProfunda: false, muestraConoceTuCandidato: false, idSeleccionado: idSeleccionado })

    }

    mostrarRegistroEntrevista(idSeleccionado) {
        console.log("entrando a entrevista")
        this.setState({ muestraTarjetas: false, muestraBajaReservaForm: false, muestraProgramacionEntrevista: true, muestraEntrevistaProfunda: false, muestraConoceTuCandidato: false, idSeleccionado: idSeleccionado })
    }

    mostrarEntrevistaProfunda(idSeleccionado) {
        console.log("entrando a entrevista")
        this.setState({ muestraTarjetas: false, muestraBajaReservaForm: false, muestraProgramacionEntrevista: false, muestraEntrevistaProfunda: true, muestraConoceTuCandidato: false, idSeleccionado: idSeleccionado })
    }

    mostrarConoceTuCandidato(idSeleccionado) {
        console.log("entrando a entrevista")
        this.setState({ muestraTarjetas: false, muestraBajaReservaForm: false, muestraProgramacionEntrevista: false, muestraEntrevistaProfunda: false, muestraConoceTuCandidato: true, idSeleccionado: idSeleccionado })
    }

    botonCancelar() {
        this.construyeTarjetas()
        this.setState({ muestraTarjetas: true, muestraConoceTuCandidato: false, muestraBajaReservaForm: false, muestraProgramacionEntrevista: false, muestraEntrevistaProfunda: false, muestraVentaDeCarrera: false, muestraConexionCapacitacion: false, muestraConectado: false })
    }


    onChange = e => {
        if (e.target.name == 'selectMotivosReserva') {
            this.setState({ idMotivoReserva: parseInt(e.target.value) })
        } else if (e.target.name == 'selectMotivosBaja') {
            this.setState({ idMotivoBaja: parseInt(e.target.value) })
        } else {
            this.setState({ [e.target.name]: e.target.value })
        }
    }
    construyeSelectMotivos(url, selector) {
        fetch(url)
            .then(response => response.json())
            .then(json => {
                let filas = json.filas
                let options = []
                options.push(<option selected value={0}>Seleccionar</option>)

                console.log("respuesta  ", json)
                for (var i = 0; i < filas.length; i++) {
                    let fila = filas[i]

                    options.push(<option value={fila.fila[0].value}>{fila.fila[1].value}</option>)
                }
                let salida = []

                if (selector == "selectMotivosReserva") {
                    salida.push(<select onChange={this.onChange} name="selectMotivosReserva" style={{ borderColor: '#F1F3FA' }} class="form-control" aria-label="Default select example">{options}</select>)
                    this.setState({ selectMotivosReserva: salida })
                } else if (selector == "selectMotivosBaja") {
                    salida.push(<select onChange={this.onChange} name="selectMotivosBaja" style={{ borderColor: '#F1F3FA' }} class="form-control" aria-label="Default select example">{options}</select>)
                    this.setState({ selectMotivosBaja: salida })
                }
            })
    }

    render() {
        return (


            <div className="page-content" style={{ backgroundColor: '#f1f3fa', height: '100%', width: '100%' }}>
                <div className="content-wrapper">
                    <div className="content-inner">


                        <div className="content justify-content-center align-items-center"  >
                            {
                                this.state.muestraBajaReservaForm == true && this.state.muestraConexionCapacitacion == false && this.state.muestraVentaDeCarrera == false && this.state.muestraProgramacionEntrevista == false && this.state.muestraEntrevistaProfunda == false &&
                                <BajaReservaForm botonCancelar={this.botonCancelar} idProspecto={this.state.idSeleccionado} tipoUsuario={this.state.tipoUsuario} />
                            }


                            {
                                this.state.muestraConexionCapacitacion == true && this.state.muestraVentaDeCarrera == false && this.state.muestraProgramacionEntrevista == false && this.state.muestraEntrevistaProfunda == false &&
                                <CapacitacionConexion botonCancelar={this.botonCancelar} idProspecto={this.state.idSeleccionado} tipoUsuario={this.state.tipoUsuario} nombreUsuario={this.state.nombreUsuario} />
                            }

                            {
                                this.state.muestraConectado == true && this.state.muestraConexionCapacitacion == false && this.state.muestraVentaDeCarrera == false && this.state.muestraProgramacionEntrevista == false && this.state.muestraEntrevistaProfunda == false &&
                                <Conectado botonCancelar={this.botonCancelar} idProspecto={this.state.idSeleccionado} tipoUsuario={this.state.tipoUsuario} nombreUsuario={this.state.nombreUsuario} />
                            }


                            {
                                this.state.muestraVentaDeCarrera == true && this.state.muestraProgramacionEntrevista == false && this.state.muestraEntrevistaProfunda == false &&
                                <VentaDeCarrera botonCancelar={this.botonCancelar} idProspecto={this.state.idSeleccionado} tipoUsuario={this.state.tipoUsuario} idUsuario={this.state.idUsuario} nombreUsuario={this.state.nombreUsuario} />
                            }


                            {
                                this.state.muestraProgramacionEntrevista == false && this.state.muestraEntrevistaProfunda == true &&
                                <EntrevistaProfunda botonCancelar={this.botonCancelar} idProspecto={this.state.idSeleccionado}
                                    enviarReserva={this.enviarReserva}
                                    enviarBaja={this.enviarBaja} tipoUsuario={this.state.tipoUsuario} />
                            }

                            {
                                this.state.muestraProgramacionEntrevista == false && this.state.muestraEntrevistaProfunda == false && this.state.muestraConoceTuCandidato == true &&
                                <ConoceCandidatoForm botonCancelar={this.botonCancelar} idProspecto={this.state.idSeleccionado}
                                    enviarReserva={this.enviarReserva}
                                    enviarBaja={this.enviarBaja} tipoUsuario={this.state.tipoUsuario} />
                            }

                            {
                                this.state.muestraProgramacionEntrevista == true && this.state.muestraEntrevistaProfunda == false &&
                                <RegistroEntrevista botonCancelar={this.botonCancelar} idProspecto={this.state.idSeleccionado} tipoUsuario={this.state.tipoUsuario} />
                            }


                            {
                                this.state.muestraTarjetas == true && this.state.muestraProgramacionEntrevista == false && this.state.muestraEntrevistaProfunda == false &&
                                <div >
                                    <button type="button" style={{ display: 'none' }} ref={(ref) => this.reserva = ref} data-toggle="modal" data-target="#modal-reserva"   ></button>
                                    <button type="button" style={{ display: 'none' }} ref={(ref) => this.baja = ref} data-toggle="modal" data-target="#modal-baja"   ></button>
                                    <div id="modal-reserva" className="modal fade" tabindex="-1">
                                        <div className="modal-dialog ">
                                            <div className="modal-content text-white" style={{ backgroundColor: "#FFFFF" }}>
                                                <div className="modal-header text-white text-center" style={{ backgroundColor: "#617187" }}>
                                                    <h6 className="modal-title col-12 text-center">Enviar a reserva</h6>

                                                </div>
                                                <div className="modal-body">
                                                    <div className="card-body">
                                                        <h8 style={{ color: '#8F9EB3' }}>Si está seguro de enviar a reserva el registro, por favor seleccione el motivo</h8>
                                                        {this.state.selectMotivosReserva}
                                                        <br></br>
                                                        <h8 style={{ color: '#8F9EB3' }}>Fecha acordada para retomar el proceso</h8>
                                                        <input type="date" placeholder="Escribir" name="fecha_alta" className="form-control " style={{ borderColor: '#D5D9E8', backgroundColor: '#F1F3FA' }} />

                                                    </div>
                                                </div>
                                                <div class="modal-footer d-flex justify-content-center">
                                                    <div className="col-12">
                                                        <div className="row">
                                                            <button type="button" style={{ width: '100%', backgroundColor: "#617187" }} class="btn text-white" onClick={this.peticionAReserva} data-dismiss="modal"  >Enviar a reserva</button>
                                                        </div>
                                                        <br></br>
                                                        <div className="row">
                                                            <button type="button" style={{ width: '100%', backgroundColor: '#8F9EB3' }} class="btn text-white" id="btnAgregar" data-dismiss="modal"  >Cancelar</button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                    <div id="modal-baja" className="modal fade" tabindex="-1">
                                        <div className="modal-dialog ">
                                            <div className="modal-content text-white" style={{ backgroundColor: "#FFFFF" }}>
                                                <div className="modal-header text-white text-center" style={{ backgroundColor: "#617187" }}>
                                                    <h6 className="modal-title col-12 text-center">Dar baja el registro</h6>

                                                </div>
                                                <div className="modal-body">
                                                    <div className="card-body">
                                                        <h8 style={{ color: '#8F9EB3' }}>Si está seguro de descartar el registro, por favor seleccione el motivo</h8>
                                                        {this.state.selectMotivosBaja}
                                                    </div>
                                                </div>
                                                <div class="modal-footer d-flex justify-content-center">
                                                    <div className="col-12">
                                                        <div className="row">
                                                            <button type="button" style={{ width: '100%', backgroundColor: "#617187" }} class="btn text-white" onClick={this.peticionABaja} data-dismiss="modal"  >Dar de baja registro</button>
                                                        </div>
                                                        <br></br>
                                                        <div className="row">
                                                            <button type="button" style={{ width: '100%', backgroundColor: '#8F9EB3' }} class="btn text-white" id="btnAgregar" data-dismiss="modal"  >Cancelar</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                    <div className="row">
                                        <div className="col-xs-7 col-lg-7">
                                            <h2>Proceso de reclutamiento</h2>
                                        </div>
                                        {/* <div className="col-xs-5 col-lg-5">
                                            <div className="form-group">
                                                <div className="input-group">
                                                    <span className="input-group-prepend"> <span
                                                        className="input-group-text   " >Todos los estatus</span>
                                                    </span>
                                                    <select className="form-control">
                                                        <option value="" selected>selecciona</option>
                                                    </select>
                                                </div>
                                            </div>
                                           </div>*/}
                                    </div>


                                    <br></br>
                                    <br></br>
                                    <div style={{ overflowX: 'auto', overflowY: 'hidden', height: 'auto', width: '3950px', display: 'flex' }}>
                                        <div className="row">

                                            <div style={{ height: 'auto', width: '450px', marginRight: '20px' }} >
                                                <TarjetaProcesoReclutamiento mostrarForma={this.mostrarRegistroEntrevista}
                                                    titulo={"En registro"}
                                                    colorHeader={'#C28EE3'}
                                                    listaDatos={this.state.enRegistro}
                                                    controles={'descartar'}
                                                    enviarBaja={this.enviarBaja}
                                                />
                                            </div>

                                            <div style={{ height: 'auto', width: '450px', marginRight: '20px' }} >
                                                <TarjetaProcesoReclutamiento
                                                    titulo={"Entrevista profunda"} mostrarForma={this.mostrarEntrevistaProfunda}
                                                    colorHeader={'#8189D4'}
                                                    listaDatos={this.state.entrevistaProfunda}
                                                    enviarReserva={this.enviarReserva}
                                                    enviarBaja={this.enviarBaja}
                                                    controles={'bajaReserva'}

                                                />
                                            </div>


                                            <div style={{ height: 'auto', width: '450px', marginRight: '20px' }} >
                                                <TarjetaProcesoReclutamiento
                                                    titulo={"Conociendo a tu candidato"} mostrarForma={this.mostrarConoceTuCandidato}
                                                    colorHeader={'#e359a0'}
                                                    listaDatos={this.state.conoceTuCandidato}
                                                    enviarReserva={this.enviarReserva}
                                                    enviarBaja={this.enviarBaja}
                                                    controles={'bajaReserva'}

                                                />
                                            </div>

                                            <div style={{ height: 'auto', width: '450px', marginRight: '20px' }}>
                                                <TarjetaProcesoReclutamiento
                                                    titulo={"Venta de carrera"} mostrarForma={this.mostrarVentaDeCarrera}
                                                    colorHeader={'#5B96E2'}
                                                    listaDatos={this.state.ventaDeCarrera}
                                                    enviarReserva={this.enviarReserva}
                                                    enviarBaja={this.enviarBaja}
                                                    controles={'bajaReserva'}
                                                />
                                            </div>



                                            <div style={{ height: 'auto', width: '450px', marginRight: '20px' }} >
                                                <TarjetaProcesoReclutamiento
                                                    titulo={"En conexión / Capacitación"} mostrarForma={this.mostrarConexionCapacitacion}
                                                    colorHeader={'#41B3D1'}
                                                    listaDatos={this.state.capacitacion}
                                                    enviarReserva={this.enviarReserva}
                                                    enviarBaja={this.enviarBaja}
                                                    controles={'bajaReserva'}
                                                />
                                            </div>

                                            <div style={{ height: 'auto', width: '450px', marginRight: '20px' }} >
                                                <TarjetaProcesoReclutamiento
                                                    titulo={"Conectado"} mostrarForma={this.mostrarConectado}
                                                    colorHeader={'#78CB5A'}
                                                    listaDatos={this.state.conectado}
                                                    enviarReserva={this.enviarReserva}
                                                    enviarBaja={this.enviarBaja}
                                                    controles={'bajaReserva'}
                                                />

                                            </div>


                                            <div style={{ height: 'auto', width: '450px', marginRight: '20px' }}>
                                                <TarjetaProcesoReclutamiento
                                                    titulo={"En reserva"} mostrarForma={this.mostrarBajaReserva}
                                                    colorHeader={'#F97777'}
                                                    listaDatos={this.state.enReserva}
                                                    reanudarReserva={this.reanudarReserva}
                                                    enviarBaja={this.enviarBaja}
                                                    controles={'ReanudarBaja'}
                                                />
                                            </div>
                                            <div style={{ height: 'auto', width: '450px', marginRight: '20px' }}>
                                                <TarjetaProcesoReclutamiento
                                                    titulo={"Baja"} mostrarForma={this.mostrarBajaReserva}
                                                    colorHeader={'#8F9EB3'}
                                                    listaDatos={this.state.baja}
                                                    controles={'Baja'}
                                                />
                                            </div>


                                        </div>
                                    </div>



                                </div>
                            }





                        </div>
                    </div>
                </div>
            </div>

        )


    }
}