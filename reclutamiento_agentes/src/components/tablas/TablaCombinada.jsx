import { toHaveFocus } from "@testing-library/jest-dom/dist/matchers";
import React, { Component } from "react";
import servicio from '../servicios/servicio'
import Modal from '../modal/Modal'
import datos from '../urls/datos.json'

export default class TablaCombinada extends Component {
    constructor() {
        super();
        this.siguientePagina = this.siguientePagina.bind(this)
        this.anteriorPagina = this.anteriorPagina.bind(this)
        this.sortTable = this.sortTable.bind(this)
        this.cambiaPaginaPorBoton = this.cambiaPaginaPorBoton.bind(this)
        this.construyeModalesEdicion = this.construyeModalesEdicion.bind(this)
        this.seleccionaColumnas = this.seleccionaColumnas.bind(this)
        this.seleccionaFilas = this.seleccionaFilas.bind(this)
        // this.guardaElementoNuevo=this.guardaElementoNuevo.bind(this)
        this.state = {
            filasProps: [],
            columnas: [],
            filas: [],
            paginas: [],
            totalRegistros: 0,
            mostrandoRegistrosTope: 10,
            mostrandoRegistroDesde: 1,
            mostrandoRegistroHasta: 1,
            siguienteEsActivado: false,
            anteriorEsActivado: false,
            totalPaginas: 1,
            paginaActual: 1,
            jsonRespuesta: {},
            edicion: false,
            modales: []

        };
    }






    UNSAFE_componentWillMount() {
        /*if (this.props.pivote == "referido" || this.props.pivote == "prospecto") {
            fetch(this.props.url)
                .then(response => response.json())
                .then(json => {
                    console.log("entrando atabla", json)
                    fetch(datos.urlServicePy+"parametros/api_cat_empleados/0")
                        .then(response => response.json())
                        .then(data => {
                            let fila = data.filas;
                            for (var i = 0; i < fila.length; i++) {
                                if (data.filas[i].fila[56].value == this.props.usuario) {
                                    let ace_edat = data.filas[i].fila[0].value
                                    fetch(this.props.urltoUrl)
                                        .then(response => response.json())
                                        .then(dataReferido => {
                                            let referido = dataReferido.filas;
                                            for (var j = 0; j < referido.length; j++) {
                                                if (ace_edat != referido[j].fila[12].value) {
                                                    delete json.filas[j];
                                                }
                                                if (j == (referido.length) - 1) {
                                                    this.setState({ filasProps: json.filas, jsonRespuesta: json })
                                                    if (this.props.nonModal == undefined) {
                                                        this.construyePaginador(json.filas.length, json.filas, 1)
                                                        this.construyeColumnas(json.columnas)
                                                        //  this.construyeModalesEdicion("", json.filas)
                                                    } else {
                                                        this.construyePaginador(json.filas.length, json.filas, 1)
                                                        this.construyeColumnas(json.columnas)
                                                    }
                                                }
                                            }
                                        })
                                }
                            }
                        })
                });
        } else {*/
            fetch(this.props.url)
                .then(response => response.json())
                .then(json => {
                    console.log("entrando atabla", json)
                    this.setState({ filasProps: json.filas, jsonRespuesta: json })

                    if (this.props.nonModal == undefined) {
                        this.construyePaginador(json.filas.length, json.filas, 1)
                        this.construyeColumnas(json.columnas)
                        //  this.construyeModalesEdicion("", json.filas)
                    } else {
                        this.construyePaginador(json.filas.length, json.filas, 1)
                        this.construyeColumnas(json.columnas)
                    }

                });
        //}
        console.log(this.props)
        //this.setState({ filasProps: this.props.modelo.filas })
        //this.construyePaginador(this.props.modelo.filas.length, this.props.modelo.filas, 1)
        // this.construyeColumnas(this.props.modelo.columnas)
    }


    seleccionaColumnas(columnas) {
        let cols = []
        let row = []
        console.log("a seleccionar ", columnas)
        for (var i = 0; i < columnas.length; i++) {
            if (columnas[i].key == 'id') {
                cols.push(columnas[i])
                row.push(i)
            }
            if (columnas[i].key == 'nombre') {
                cols.push(columnas[i])
                row.push(i)
            }
            if (columnas[i].key == 'rfc') {
                cols.push(columnas[i])
                row.push(i)
            }

            if (columnas[i].key == 'area_id') {
                cols.push(columnas[i])
                row.push(i)
            }
        }
        return { columnas: cols, filas: row }
    }

    seleccionaFilas(filas, selecionadas) {

        console.log("las filas asi estan ", filas)
        let filasSalida = []
        for (var i = 0; i < filas.length; i++) {
            let filaArray = []
            for (var j = 0; j < selecionadas.length; j++) {
                console.log("segun el resultado", filas[i].fila[selecionadas[j]])
                filaArray.push(filas[i].fila[selecionadas[j]])
            }
            let filaJson = { fila: filaArray }
            filasSalida.push(filaJson)

        }
        return filasSalida
    }






    construyeColumnas(columnas) {
        console.log("entrando a cons")
        let stackcolumnas = []
        for (var i = 0; i < columnas.length; i++) {
            stackcolumnas.push(
                <th className="text-left font-weight-bold" style={{ backgroundColor: '#FFFFFF' }} >
                    <a onClick={this.sortTable}  >{columnas[i].value}</a>
                </th>
            )
            if (i == columnas.length - 1) {
                stackcolumnas.push(
                    <th className="text-left font-weight-bold"  >
                        <a >Acciones</a>
                    </th>
                )
            }
        }
        this.setState({ columnas: stackcolumnas })
    }

    construyeModalesEdicion(nombreModal, filas) {
        let modales = []

        for (var i = 0; i < filas.length; i++) {
            let contenidoFila = filas[i].fila
            modales.push(

                <Modal guardaElementoNuevo={this.props.guardaElementoNuevo} modalNombre={this.props.modalNombreEdicion + i} contenido={contenidoFila} />

            )
        }
        //  console.log("estas son las modales ",modales)
        this.setState({ modales: modales })

    }

    construyeFilas(filas, inicioConteo, finConteo) {
        inicioConteo = inicioConteo - 1
        finConteo = finConteo - 1
        let stackFilas = []
        let stackContenidoFila = []
        console.log(filas)
        console.log("inicioConteo ", inicioConteo)
        console.log("finConteo ", finConteo)
        for (var i = inicioConteo; i < finConteo; i++) {
            if (filas[i] != undefined) {
                let idEdicion = 0;
                let contenidoFila = filas[i].fila
                for (var j = 0; j < contenidoFila.length; j++) {

                    if (contenidoFila[j].type == 'AutoIncrement') {
                        idEdicion = contenidoFila[j].value
                        console.log("idEdicion ", idEdicion)
                        stackContenidoFila.push(
                            <td className="sorting_1" style={{ textAlign: "left", color: '#617187', opacity: '1' }}>{contenidoFila[j].value}</td>
                        )
                    }
                    if (contenidoFila[j].type == 'String' || contenidoFila[j].type == 'Date' || contenidoFila[j].type == 'int' || contenidoFila[j].type == 'Int') {
                        stackContenidoFila.push(
                            <td style={{ textAlign: "left", color: '#617187', opacity: '1' }}>{contenidoFila[j].value}</td>
                        )
                    }


                    if (contenidoFila[j].type == 'Boolean') {
                        if (contenidoFila[j].value == 1) {
                            stackContenidoFila.push(
                                <td style={{ textAlign: "left" }}>
                                    <div className="custom-control custom-switch custom-control-warning mb-2">
                                        <input type="checkbox" class="custom-control-input" id="swc_warning" checked={true} onChange={(e) => console.log("")} ></input>
                                        <label class="custom-control-label" for="swc_warning"></label>
                                    </div>
                                </td>
                            )
                        } else {
                            stackContenidoFila.push(
                                <td style={{ textAlign: "left" }}>
                                    <div className="custom-control custom-switch custom-control-warning mb-2">
                                        <input type="checkbox" class="custom-control-input" id="swc_warning" checked={false} onChange={(e) => console.log("")}  ></input>
                                        <label class="custom-control-label" for="swc_warning"></label>
                                    </div>
                                </td>
                            )
                        }

                    }

                    if (contenidoFila.length - 1 == j) {
                        //   if (this.props.nonModal == undefined) {
                        /*  stackContenidoFila.push(
                              <td style={{ textAlign: "left" }}>

                                  <a href="#" style={{ textDecoration: 'none', color: '#8F9EB3' }} ><i className="fas fa-pen" data-toggle="modal"
                                      data-target={"#" + this.props.modalNombreEdicion + i} style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} data-popup="tooltip" title="Editar" ></i></a>
                              </td>
                          )*/
                        //} else {
                        //idEdicion
                        console.log("idEdicion befor ", idEdicion)

                        stackContenidoFila.push(
                            <td style={{ textAlign: "left" }}>
                                <a href="#" style={{ textDecoration: 'none', color: '#8F9EB3' }} ><i onClick={(e) => { this.props.muestraFormulario(idEdicion) }} className="fas fa-pen"
                                    style={{ color: "rgb(137, 188, 67);", textDecoration: 'none' }} title="Editar" ></i></a>
                            </td>
                        )
                        // }

                    }
                }
                stackFilas.push(<tr className="odd">{stackContenidoFila}</tr>)
                stackContenidoFila = []

            }
        }
        this.setState({ filas: stackFilas })
    }

    construyePaginador(totalFilas, filas, paginaActual) {

        if (totalFilas > 0) {

            let divisionPaginador = totalFilas / this.state.mostrandoRegistrosTope;
            if (divisionPaginador < 1) {
                this.setState({ mostrandoRegistroHasta: totalFilas })
                this.construyeFilas(filas, 1, this.state.mostrandoRegistrosTope + 1)
            } else {

                this.setState({ mostrandoRegistroHasta: this.state.mostrandoRegistrosTope, siguienteEsActivado: true })
                if (divisionPaginador % 1 == 0) {
                    this.construyePaginasDePaginador(divisionPaginador, 1)
                    this.construyeFilas(filas, 1, this.state.mostrandoRegistrosTope + 1)

                } else {
                    let stringDivisor = divisionPaginador + ''
                    divisionPaginador = parseInt(stringDivisor.split('.')[0])
                    this.construyePaginasDePaginador(divisionPaginador + 1, 1)
                    this.construyeFilas(filas, 1, this.state.mostrandoRegistrosTope + 1)
                }

            }
        }
        this.setState({ totalRegistros: totalFilas })

    }

    cambiaPaginaPorBoton(pagina, e) {
        return event => {
            console.log(this, pagina)
            let conteo = (pagina) * this.state.mostrandoRegistrosTope
            let inicio = (conteo - this.state.mostrandoRegistrosTope) + 1
            let fin = conteo + 1

            this.construyeFilas(this.state.filasProps, inicio, fin)
            this.construyePaginasDePaginador(this.state.totalPaginas, pagina)
            this.setState({ mostrandoRegistroDesde: inicio, mostrandoRegistroHasta: fin })
        }

    }


    construyePaginasDePaginador(totalPaginas, paginaActual) {
        let paginas = []
        for (var i = 1; i <= totalPaginas; i++) {
            if (i == paginaActual) {
                paginas.push(
                    <a className="paginate_button current" onClick={this.cambiaPaginaPorBoton(i)} >{i}</a>
                )
            } else {
                paginas.push(
                    <a className="paginate_button " onClick={this.cambiaPaginaPorBoton(i)}  >{i}</a>
                )
            }
        }
        this.setState({ paginas: paginas, paginaActual: paginaActual, totalPaginas: totalPaginas })
    }


    siguientePagina() {

        let conteo = (this.state.paginaActual + 1) * this.state.mostrandoRegistrosTope
        let inicio = (conteo - this.state.mostrandoRegistrosTope) + 1
        let fin = conteo + 1

        this.construyeFilas(this.state.filasProps, inicio, fin)
        this.construyePaginasDePaginador(this.state.totalPaginas, this.state.paginaActual + 1)
        this.setState({ mostrandoRegistroDesde: inicio, mostrandoRegistroHasta: fin })

    }

    anteriorPagina() {
        let conteo = (this.state.paginaActual - 1) * this.state.mostrandoRegistrosTope
        let inicio = (conteo - this.state.mostrandoRegistrosTope) + 1
        let fin = conteo + 1
        this.construyeFilas(this.state.filasProps, inicio, fin)
        this.construyePaginasDePaginador(this.state.totalPaginas, this.state.paginaActual - 1)
        this.setState({ mostrandoRegistroDesde: inicio, mostrandoRegistroHasta: fin })
    }



    sortTable(n) {
        n = 0
        var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
        table = document.getElementById("tablaDatos");
        switching = true;

        dir = "asc";
        while (switching) {
            switching = false;
            rows = table.rows;
            for (i = 1; i < (rows.length - 1); i++) {
                shouldSwitch = false;
                x = rows[i].getElementsByTagName("td")[n];
                y = rows[i + 1].getElementsByTagName("td")[n];
                if (dir == "asc") {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
                switchcount++;
            } else {
                if (switchcount == 0 && dir == "asc") {
                    dir = "desc";
                    switching = true;
                }
            }
        }
    }







    render() {
        return (
            <div>
                {this.state.modales.length != 0 ?
                    this.state.modales : ''}

                <div className="card" style={{ backgroundColor: '#F1F3FA', border: 'none' }}>
                    <div className="card-header bg-transparent header-elements-inline " style={{ border: 'none' }}>
                        <h4 className="card-title py-3 font-weight-bold">

                            {this.props.tituloTabla}


                        </h4>
                        <div className="header-elements">
                            <form action={datos.urlXLS + this.props.tabla} method="get">
                                <button type="submit" className="btn text-white" style={{ backgroundColor: "#8F9EB3" }}>
                                    CSV <i className="icon-cloud-download ml-2"></i>
                                </button>
                            </form>
                            {<>&nbsp;&nbsp;&nbsp;</>}
                            <form action={datos.urlPDF + this.props.tabla} method="get">
                                <button type="submit" className="btn text-white" style={{ backgroundColor: "#8F9EB3" }}>
                                    PDF <i className="icon-cloud-download ml-2"></i>
                                </button>
                            </form>
                            {<>&nbsp;&nbsp;&nbsp;</>}
                            {
                                // this.props.nonModal == undefined ?
                                <button type="button" className="btn text-white" style={{ backgroundColor: this.props.colorAgregar }}
                                    data-toggle="modal" data-target={"#" + this.props.modalNombre}>
                                    Agregar Nuevo</button>
                                /*   :
                                   <button onClick={(e) => { this.props.muestraFormulario() }} type="button" className="btn text-white" style={{ backgroundColor: this.props.colorAgregar }} >
                                       Agregar Nuevo     </button>*/
                            }



                        </div>
                    </div>
                    <br />



                    <div className="card-body">
                        <div className="dataTables_wrapper no-footer">
                            <div className="datatable-scroll-wrap">
                                <table className="table datatable-sorting  table-striped table-hover" id="tablaDatos">
                                    <thead style={{ backgroundColor: '#FFFFFF' }}>
                                        <tr>
                                            {
                                                this.state.columnas
                                            }
                                        </tr>
                                    </thead>

                                    <tbody>

                                        {this.state.filas}

                                    </tbody>
                                </table>
                            </div>

                            <div className="datatable-footer">
                                <div className="dataTables_info" style={{ color: '#617187' }}>
                                    Mostrando registros del {this.state.mostrandoRegistroDesde} al {this.state.mostrandoRegistroHasta} de un total de {this.state.totalRegistros} registros
                                </div>
                                <div className="dataTables_paginate paging_simple_numbers">
                                    {
                                        this.state.paginaActual > 1 === true ?
                                            <a onClick={this.anteriorPagina} className="paginate_button  previous  " style={{ color: '#617187' }}>Anterior</a>
                                            :
                                            <a className="paginate_button previous disabled">Anterior</a>
                                    }
                                    <span >
                                        {this.state.paginas}
                                    </span>
                                    {
                                        this.state.paginaActual <= this.state.totalPaginas - 1 == true ? <a onClick={this.siguientePagina}
                                            className="paginate_button next" style={{ color: '#617187' }} >Siguiente</a> : <a className="paginate_button next disabled" style={{ color: '#617187' }}>Siguiente</a>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>










        )

    }
}