import { toHaveFocus } from "@testing-library/jest-dom/dist/matchers";
import React, { Component } from "react";
export default class BarraAdminEmpleadosAPS extends Component {

	constructor() {
		super();

		this.cambiaPaso = this.cambiaPaso.bind(this)
		this.botonCancelar = this.botonCancelar.bind(this)

		this.state = {
			desactiva2: "btn btn-light rounded-pill btn-icon btn-sm disabled ",
			activa2: 'btn btn-primary rounded-pill btn-icon btn-sm',
			desactivaText2: "text-body font-weight-semibold  letter-icon-title text-muted ",
			activaText2: 'text-body font-weight-semibold  letter-icon-title',

			desactiva3: "btn btn-light rounded-pill btn-icon btn-sm disabled ",
			activa3: 'btn btn-primary rounded-pill btn-icon btn-sm',
			desactivaText3: "text-body font-weight-semibold  letter-icon-title text-muted ",
			activaText3: 'text-body font-weight-semibold  letter-icon-title'



		};
	}

	UNSAFE_componentWillMount() {
		console.log("Entrando barra de empleados ", this.props)
		if (this.props.esEdicion) {
			this.setState({ desactiva2: this.state.activa2, desactivaText2: this.state.activaText2 })
			this.setState({ desactiva3: this.state.activa3, desactivaText3: this.state.activaText3 })
		}
	}

	cambiaPaso() {
		console.log("entro ",this.props)
		/*let nombre =this.props.jsonPrimerFormulario.nombre
		if(nombre == "" || nombre.length ==0){
			let validaNombre = document.getElementById("validaNombre").style.display="block"
		}*/
		
		if(!this.props.esEdicion){
			if(this.state.activa2 != this.state.desactiva2){
				console.log("cae en post"  )
				this.setState({desactiva2:this.state.activa2,desactivaText2:this.state.activaText2})
				this.props.enviaPrimerFormulario("POST")
				//this.props.muestraFormulario2(this.props.idEditando)
			}else{
				console.log("cae en put"  )
				this.setState({desactiva3:this.state.activa3,desactivaText3:this.state.activaText3})
				this.props.enviaSegundoFormulario("PUT")
				this.props.muestraFormulario3(this.props.idEditando)
			}
		}else{// es edicion del form
			if(this.props.muestraFormularioBandera == true){
				this.props.enviaPrimerFormulario("PUT") 
				this.props.muestraFormulario2(this.props.idEditando)
			}
			if(this.props.muestraFormularioBandera2 == true){
				this.props.enviaSegundoFormulario("PUT")
				this.props.muestraFormulario3()
			}
		}

	}

	botonCancelar() {
		this.props.muestraTabla()
	}





	render() {
		return (
			<div class="card">
				<div class="row mb-2 mt-2 ml-1">
					<div class="col-12 col-lg-9">
						<div class="row">

							<div class="col-6 col-lg-3 d-flex align-items-center pb-sm-1">
								<div class="mr-2">
									<a href="#"
										class="btn btn-primary rounded-pill btn-icon btn-sm"> <span
											class="letter-icon">1</span>
									</a>
								</div>
								<div>
									<a href="#" onClick={(e) => this.props.esEdicion ? this.props.muestraFormulario(this.props.idEditando) : ''}
										class="text-body font-weight-semibold letter-icon-title">Información general</a>
								</div>
							</div>

							<div class="col-6 col-lg-3 d-flex align-items-center pb-sm-1">
								<div class="mr-2">
									<a href="#" onClick={(e) => this.props.esEdicion ? this.props.muestraFormulario2(this.props.idEditando) : ''}
										class={this.state.desactiva2}>
										<span class="letter-icon">2</span>
									</a>
								</div>
								<div>
									<a href="#" onClick={(e) => this.props.esEdicion ? this.props.muestraFormulario2(this.props.idEditando) : ''}
										class={this.state.desactivaText2}>Información laboral</a>
								</div>
							</div>

							<div class="col-6 col-lg-3 d-flex align-items-center pb-sm-1">
								<div class="mr-2">
									<a href="#" onClick={(e) => this.props.esEdicion ? this.props.muestraFormulario3() : ''}
										class={this.state.desactiva3}>
										<span class="letter-icon">3</span>
									</a>
								</div>
								<div>
									<a href="#" onClick={(e) => this.props.esEdicion ? this.props.muestraFormulario3() : ''}
										class={this.state.desactivaText3}>Beneficiarios</a>
								</div>
							</div>

						</div>
					</div>
					<div class="col-12 col-lg-3">

						<div class="float-right mr-3">

							<button type="button" class="btn btn-light mr-1" onClick={this.botonCancelar}>Cancelar</button>
							{
								    this.props.esEdicion ?
									 <button type="button" class="btn btn-warning"
									 disabled={
										this.props.jsonPrimerFormulario.nombre == ""
										 ||
										 this.props.jsonPrimerFormulario.rfc == ""
										 ||
										 this.props.jsonPrimerFormulario.ape_paterno == ""
										 ||
										 this.props.jsonPrimerFormulario.curp == ""
										 ||
										 this.props.jsonPrimerFormulario.fec_nacimiento == ""
										 ||
										 this.props.jsonPrimerFormulario.codigo_postal == 0
										 ||
										 this.props.jsonPrimerFormulario.ciudad_id == 0
										 ||
										 this.props.jsonPrimerFormulario.calle == ""
										 ||
										 this.props.jsonPrimerFormulario.municipio_id == ""
										 ||
										 this.props.jsonPrimerFormulario.num_ext == 0
										 ||
										 this.props.jsonPrimerFormulario.num_int == 0
										 ||
										 this.props.jsonPrimerFormulario.tel_movil == 0
										 ||
										 this.props.jsonPrimerFormulario.estadocivil_id == 0
										 ||
										 this.props.jsonPrimerFormulario.nombre_contacto == ""
										 ||
										 this.props.jsonPrimerFormulario.parentesco_id == 0
										 ||
										 this.props.jsonPrimerFormulario.tel_contacto == 0
									
									}
									 style={{ backgroundColor: '#ED6C26' }} id="siguiente" onClick={this.cambiaPaso}> Guardar cambios</button>
									:

									<button type="button" class="btn btn-warning" disabled={
										this.props.jsonPrimerFormulario.nombre == ""
										 ||
										 this.props.jsonPrimerFormulario.rfc == ""
										 ||
										 this.props.jsonPrimerFormulario.ape_paterno == ""
										 ||
										 this.props.jsonPrimerFormulario.curp == ""
										 ||
										 this.props.jsonPrimerFormulario.fec_nacimiento == ""
										 ||
										 this.props.jsonPrimerFormulario.codigo_postal == 0
										 ||
										 this.props.jsonPrimerFormulario.ciudad_id == 0
										 ||
										 this.props.jsonPrimerFormulario.calle == ""
										 ||
										 this.props.jsonPrimerFormulario.municipio_id == ""
										 ||
										 this.props.jsonPrimerFormulario.num_ext == 0
										 ||
										 this.props.jsonPrimerFormulario.num_int == 0
										 ||
										 this.props.jsonPrimerFormulario.tel_movil == 0
										 ||
										 this.props.jsonPrimerFormulario.estadocivil_id == 0
										 ||
										 this.props.jsonPrimerFormulario.nombre_contacto == ""
										 ||
										 this.props.jsonPrimerFormulario.parentesco_id == 0
										 ||
										 this.props.jsonPrimerFormulario.tel_contacto == 0
									
									
									
									} style={{ backgroundColor: '#ED6C26' }} id="siguiente" onClick={this.cambiaPaso}> Siguiente</button>
							}


						</div>

					</div>
				</div>
			</div>
		)
	}
}