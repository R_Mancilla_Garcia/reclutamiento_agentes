import React, { Component } from "react";
export default class TarjetaProcesoReclutamiento extends Component {

    constructor() {
        super();
        this.construyePaginador = this.construyePaginador.bind(this)
        this.construyePaginasDePaginador = this.construyePaginasDePaginador.bind(this)
        this.siguientePagina = this.siguientePagina.bind(this)
        this.anteriorPagina = this.anteriorPagina.bind(this)
        this.reanudarReserva = this.reanudarReserva.bind(this)
        // this.mostrando=this.mostrando.bind(this)
        this.state = {
            filas: [],
            paginas: [],
            totalRegistros: 0,
            mostrandoRegistrosTope: 10,
            mostrandoRegistroDesde: 1,
            mostrandoRegistroHasta: 1,
            siguienteEsActivado: false,
            anteriorEsActivado: false,
            totalPaginas: 1,
            paginaActual: 1,
        };
    }

    construyePaginador(totalFilas, filas, paginaActual) {

        if (totalFilas > 0) {
            let divisionPaginador = totalFilas / this.state.mostrandoRegistrosTope;
            if (divisionPaginador < 1) {
                this.setState({ mostrandoRegistroHasta: totalFilas })
                this.construyePaginasDePaginador(1, 1)
                this.construyeFilas(filas, 1, this.state.mostrandoRegistrosTope + 1)
            } else {

                this.setState({ mostrandoRegistroHasta: this.state.mostrandoRegistrosTope, siguienteEsActivado: true })
                if (divisionPaginador % 1 == 0) {
                    this.construyePaginasDePaginador(divisionPaginador, 1)
                    this.construyeFilas(filas, 1, this.state.mostrandoRegistrosTope + 1)

                } else {
                    let stringDivisor = divisionPaginador + ''
                    divisionPaginador = parseInt(stringDivisor.split('.')[0])
                    this.construyePaginasDePaginador(divisionPaginador + 1, 1)
                    this.construyeFilas(filas, 1, this.state.mostrandoRegistrosTope + 1)
                }

            }
        }
        this.setState({ totalRegistros: totalFilas })

    }


    construyePaginasDePaginador(totalPaginas, paginaActual) {
        let paginas = []
        for (var i = 1; i <= totalPaginas; i++) {
            if (i == paginaActual) {
                paginas.push(
                    <option value={i} selected>{i}</option>
                )
            } else {
                paginas.push(
                    <option value={i} >{i}</option>
                )

            }
        }
        this.setState({ paginas: paginas, paginaActual: paginaActual, totalPaginas: totalPaginas })
    }


    mostrando(idProspecto) {
        console.log(idProspecto)
        this.props.mostrarForma(idProspecto)
    }


    enviarABaja(idProspecto){
        this.props.enviarBaja(idProspecto)
    }

    
    enviarAReserva(idProspecto){
        this.props.enviarReserva(idProspecto)
    }

    reanudarReserva(idProspecto){
        console.log("reanudar Reserva",idProspecto);
        this.props.reanudarReserva(idProspecto)
    }

    construyeFilas(listaDatos, inicioConteo, finConteo) {
        inicioConteo = inicioConteo - 1
        finConteo = finConteo - 1
        let stackFilas = []
        let stackContenidoFila = [] 
        console.log("inicioConteo ", inicioConteo)
        console.log("finConteo ", finConteo)
        for (var i = inicioConteo; i < finConteo; i++) {
            if (listaDatos[i] != undefined) {
                stackFilas.push(
                    <div className="card">

                        <div className="row">

                            <div className="col-xs-3 col-lg-3">
                                <span className=" text-center" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" ><i style={{ color: '#8F9EB3' }} className={listaDatos[0].estatus_id != 19 ? "icon-menu" : ""}></i></span>
                                {
                                    this.props.controles == 'bajaReserva' &&
                                    <div class="dropdown-menu">

                                        <div className="dropdown-item ">
                                            <div className="row">
                                                <div className="col-xs-3 col-lg-3">
                                                    <i style={{ color: 'red' }} onClick={this.enviarAReserva.bind(this,listaDatos[i].prospecto_id) } className="icon-forward3"></i>
                                                </div>
                                                <div className="col-xs-6 col-lg-6">
                                                    <a href="#" onClick={this.enviarAReserva.bind(this,listaDatos[i].prospecto_id) } style={{ color: 'red' }} >Enviar a reserva</a>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="dropdown-divider"></div>

                                        <div className="dropdown-item ">
                                            <div className="row">
                                                <div className="col-xs-3 col-lg-3">
                                                    <i style={{ color: '#8F9EB3' }} onClick={this.enviarABaja.bind(this,listaDatos[i].prospecto_id) } className="icon-user-minus"></i>
                                                </div>
                                                <div className="col-xs-6 col-lg-6">
                                                    <a href="#"  onClick={this.enviarABaja.bind(this,listaDatos[i].prospecto_id) } style={{ color: '#8F9EB3' }} >Dar de baja</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }
                                {
                                    this.props.controles == 'descartar' &&
                                    <div class="dropdown-menu">
                                        <div className="dropdown-item ">
                                            <div className="row">
                                                <div className="col-xs-3 col-lg-3">
                                                    <i style={{ color: '#8F9EB3' }} onClick={this.enviarABaja.bind(this,listaDatos[i].prospecto_id) } className="icon-blocked"></i>
                                                </div>
                                                <div className="col-xs-6 col-lg-6">
                                                    <a href="#" onClick={this.enviarABaja.bind(this,listaDatos[i].prospecto_id) } style={{ color: '#8F9EB3' }} >Descartar</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }
                                {
                                    this.props.controles == 'baja' &&
                                    <div class="dropdown-menu">
                                        <div className="dropdown-item ">
                                            <div className="row">
                                                <div className="col-xs-3 col-lg-3">
                                                    <i style={{ color: '#8F9EB3' }}  onClick={(e) => this.props.enviarBaja(listaDatos[i].prospecto_id)} className="icon-user-minus"></i>
                                                </div>
                                                <div className="col-xs-6 col-lg-6">
                                                    <a href="#" onClick={(e) => this.props.enviarBaja(listaDatos[i].prospecto_id)} style={{ color: '#8F9EB3' }} >Dar de baja</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }

                                {
                                    this.props.controles == 'ReanudarBaja' &&
                                    <div class="dropdown-menu">

                                        <div className="dropdown-item ">
                                            <div className="row">
                                                <div className="col-xs-3 col-lg-3">
                                                    <i style={{ color: 'black' }} onClick={ this.reanudarReserva.bind(this,listaDatos[i].prospecto_id)} className="icon-next2"></i>
                                                </div>
                                                <div className="col-xs-6 col-lg-6">
                                                    <a href="#" onClick={ this.reanudarReserva.bind(this,listaDatos[i].prospecto_id)} style={{ color: 'black' }} >Reanudar proceso</a>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="dropdown-divider"></div>

                                        <div className="dropdown-item ">
                                            <div className="row">
                                                <div className="col-xs-3 col-lg-3">
                                                    <i style={{ color: '#8F9EB3' }}  onClick={this.enviarABaja.bind(this,listaDatos[i].prospecto_id) } className="icon-user-minus"></i>
                                                </div>
                                                <div className="col-xs-6 col-lg-6">
                                                    <a href="#" onClick={this.enviarABaja.bind(this,listaDatos[i].prospecto_id) } style={{ color: '#8F9EB3' }} >Dar de baja</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }

                            </div>

                            <div className="col-xs-7 col-lg-7">
                                <h8 className=" text-center font-weight-semibold" style={{ letterSpacing: '0.2px', color: '#313A46' }} >
                                    {listaDatos[i].nombre}
                                </h8>
                            </div>

                            <div className="col-xs-2 col-lg-2 ">
                                <i style={{ color: this.props.colorHeader }} idProspecto={listaDatos[i].prospecto_id} onClick={this.mostrando.bind(this, listaDatos[i].prospecto_id)} className="icon-forward3"></i>
                            </div>

                        </div>

                    </div>

                )
            }

        }
        this.setState({ filas: stackFilas })
    }


    siguientePagina() {

        let conteo = (this.state.paginaActual + 1) * this.state.mostrandoRegistrosTope
        let inicio = (conteo - this.state.mostrandoRegistrosTope) + 1
        let fin = conteo + 1

        this.construyeFilas(this.props.listaDatos, inicio, fin)
        this.construyePaginasDePaginador(this.state.totalPaginas, this.state.paginaActual + 1)
        this.setState({ mostrandoRegistroDesde: inicio, mostrandoRegistroHasta: fin })

    }

    anteriorPagina() {
        let conteo = (this.state.paginaActual - 1) * this.state.mostrandoRegistrosTope
        let inicio = (conteo - this.state.mostrandoRegistrosTope) + 1
        let fin = conteo + 1
        this.construyeFilas(this.props.listaDatos, inicio, fin)
        this.construyePaginasDePaginador(this.state.totalPaginas, this.state.paginaActual - 1)
        this.setState({ mostrandoRegistroDesde: inicio, mostrandoRegistroHasta: fin })
    }


    UNSAFE_componentWillMount() {
        console.log("entrando a las tarejats", this.props)
        let listaDatos = this.props.listaDatos
        this.construyePaginador(listaDatos.length, this.props.listaDatos, 1)

    }

    render() {
        return (
            <div className="card ">

                <div className="card-header text-white" style={{ backgroundColor: this.props.colorHeader }}>
                    <div className="row">
                        <div className="col-xs-10 col-lg-10">
                            {this.props.titulo}
                        </div>
                        <div className="col-xs-1 col-lg-1">
                            {this.props.listaDatos.length}
                        </div>
                    </div>
                </div>
                <div className="card-body text-center" style={{ backgroundColor: '#E1E5F0' }} >
                    <div className="row">
                        <div className="card-body" style={{ backgroundColor: '#E1E5F0' }}>
                            {
                                this.state.filas
                            }
                            <div className="datatable-footer">
                                <div className="dataTables_paginate paging_simple_numbers">
                                    {
                                        this.state.paginaActual > 1 === true ?
                                            <a className="paginate_button  previous  " onClick={this.anteriorPagina} style={{ color: '#617187' }}><i className="icon-backward2"></i></a>
                                            :
                                            <a className="paginate_button  previous disabled "><i className="icon-backward2"></i></a>
                                    }

                                    <select style={{ borderRadius: '3px', border: '1px', borderColor: '#DDE0EB' }}  disabled>
                                        {this.state.paginas}
                                    </select>
                                    {
                                        this.state.paginaActual <= this.state.totalPaginas - 1 == true ?
                                            <a className="paginate_button next" onClick={this.siguientePagina} style={{ color: '#617187' }} ><i className="icon-forward3"></i></a>
                                            :
                                            <a className="paginate_button next disabled" style={{ color: '#617187' }} ><i className="icon-forward3"></i></a>
                                    }


                                </div>
                            </div>
                            {/*
                            <p className="card-text" >
                                <span><i className="icon-list2 "></i></span>
                                &nbsp;
                                Mancilla Garcia Edgar Rogelio
                                &nbsp;
                                <span><i style={{ color: this.props.colorHeader }} onClick={(e) => this.props.mostrarForma()} className="icon-next2"></i></span>
                            </p>

                            <p className="card-text">
                                <span><i className="icon-list2 "></i></span>
                                &nbsp;
                                Mancilla Garcia Edgar Rogelio
                                &nbsp;
                                <span><i className="icon-next2"></i></span>
                            </p>

                            <p className="card-text">
                                <span><i className="icon-list2 "></i></span>
                                &nbsp;
                                Mancilla Garcia Edgar Rogelio
                                &nbsp;
                                <span><i className="icon-next2"></i></span>
                            </p>

                            <p className="card-text">
                                <span><i className="icon-list2 "></i></span>
                                &nbsp;
                                Mancilla Garcia Edgar Rogelio
                                &nbsp;
                                <span><i className="icon-next2"></i></span>
                            </p>

                            <p className="card-text">
                                <span><i className="icon-list2 "></i></span>
                                &nbsp;
                                Mancilla Garcia Edgar Rogelio
                                &nbsp;
                                <span><i className="icon-next2"></i></span>
                            </p>

                            <p className="card-text">
                                <span><i className="icon-list2 "></i></span>
                                &nbsp;
                                Mancilla Garcia Edgar Rogelio
                                &nbsp;
                                <span><i className="icon-next2"></i></span>
                            </p>*/}

                        </div>
                    </div>
                </div>
            </div>
        )


    }
}