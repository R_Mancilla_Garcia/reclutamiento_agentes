import React, { Component } from "react";
import Tabla from "../tablas/Tabla";
import Modal from "../modal/Modal";
import datos from '../urls/datos.json'

const queryParams = new URLSearchParams(window.location.search)

export default class TabCatalogoReservas extends Component {

    constructor() {
        super();
        this.guardaElementoNuevo = this.guardaElementoNuevo.bind(this)
        this.state = {
            usuario: {},
            permisosSideBar: [],
            mostrarTablaParametrosMotivoEnvios: true,
            mostrarTablaParametrosFuenteReclutamiento: true,
            mostrarTablaParametrosFuenteReclutamientoBonos: true,
            mostrarTablaParametrosManejoAgentes: true,
            mostrarTablaParametrosEstatusAps: true,
            idUsuario:0,
            activaMotivosTab:'nav-link',
            activaMotivosTabla:'tab-pane fade',

            activaFuentesTab:'nav-link',
            activaFuentesTabla:'tab-pane fade',

            activaFuentesBonosTab:'nav-link',
            activaFuentesBonosTabla:'tab-pane fade',

            activaManejoTab:'nav-link',
            activaManejoTabla:'tab-pane fade',

            activaEstatusTab:'nav-link',
            activaEstatusTabla:'tab-pane fade',

            

        };
    }


    UNSAFE_componentWillMount() {
        let selector=queryParams.get("activo")
        let idUsuario=queryParams.get("idUsuario")
        
        if(selector == "motivos"){
            this.setState({activaMotivosTab:'nav-link active',activaMotivosTabla:'tab-pane fade show active'})
        }
        if(selector == "fuentes"){
            this.setState({activaFuentesTab:'nav-link active',activaFuentesTabla:'tab-pane fade show active'})
        }
        if(selector == "fuentesBonos"){
            this.setState({activaFuentesBonosTab:'nav-link active',activaFuentesBonosTabla:'tab-pane fade show active'})
        }
        if(selector == "manejo"){
            this.setState({activaManejoTab:'nav-link active',activaManejoTabla:'tab-pane fade show active'})
        }
        if(selector == "estatus"){
            this.setState({activaEstatusTab:'nav-link active',activaEstatusTabla:'tab-pane fade show active'})
        }
        this.setState({idUsuario:parseInt(idUsuario)})



        console.log("querys ",queryParams.get("activo"))
    }
    

    guardaElementoNuevo(url, json, verboHTTP, banderaParametro) {
        this.setState({ [banderaParametro]: false })
        console.log("la url al persistir ", url)
        console.log("el json al persistir ", json)


        const requestOptions = {
            method: verboHTTP,
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };
        fetch(url, requestOptions)
            .then(response => response.json())
            .then(data => {
                console.log("la respuesta ", data)
                this.setState({ [banderaParametro]: true })
            });
    }

    mostrarModalParametrosReclutamiento() {
        console.log("entrando a prender")
        this.setState({ mostrarModalParametrosReclutamiento: true })
    }

    render() {
        return (

            <div class="row">
                <div class="col-12">
                    <ul class="nav nav-tabs nav-tabs-bottom">
                        <li class="nav-item"><a href="#motivos" class={this.state.activaMotivosTab} data-toggle="tab"><strong className="font-weight-bold">Motivos de envío a reserva de prospectos</strong></a></li>
                        <li class="nav-item"><a href="#reclutamiento" class={this.state.activaFuentesTab} data-toggle="tab"><strong>Fuente de reclutamiento</strong></a></li>
                        <li class="nav-item"><a href="#reclutamientoBonos" class={this.state.activaFuentesBonosTab} data-toggle="tab"><strong>Fuente de reclutamiento para bonos</strong></a></li>
                        <li class="nav-item"><a href="#manejo" class={this.state.activaManejoTab} data-toggle="tab"><strong>Manejo y consideraciones de agentes consolidados</strong></a></li>
                        <li class="nav-item"><a href="#estatus" class={this.state.activaEstatusTab} data-toggle="tab"><strong>Estatus APS</strong></a></li>


                    </ul>
                    <div class="tab-content">

                        <div class={this.state.activaMotivosTabla} id="motivos">
                            {
                                this.state.mostrarTablaParametrosMotivoEnvios ?
                                    <div>
                                        <Tabla url={datos.urlServicePy+'parametros/api_cat_motivo_envio_reserva/0'} tituloTabla={'Motivos de envío a reserva'} guardaElementoNuevo={this.guardaElementoNuevo}
                                            mostrarModal={0} modalNombre="modal-motivoEnvio" colorAgregar="#ED6C26" modalNombreEdicion="modal-motivoEnvioEdicion" tabla="api_cat_motivo_envio_reserva" idUsuario={this.state.idUsuario} />
                                        <Modal modalNombre='modal-motivoEnvio' idUsuario={this.state.idUsuario} guardaElementoNuevo={this.guardaElementoNuevo} />
                                    </div> : ''
                            }


                        </div>


                        <div class={this.state.activaFuentesTabla} id="reclutamiento">
                            {
                                this.state.mostrarTablaParametrosFuenteReclutamiento ?
                                    <div>
                                        <Tabla url={datos.urlServicePy+'parametros/api_cat_fuente_reclutamiento/0'} tituloTabla={'Fuente de reclutamiento'} guardaElementoNuevo={this.guardaElementoNuevo}
                                            mostrarModal={0} modalNombre="modal-fuenteReclutamiento" colorAgregar="#ED6C26" modalNombreEdicion="modal-fuenteReclutamientoEdicion"  tabla="api_cat_fuente_reclutamiento" idUsuario={this.state.idUsuario}/>
                                        <Modal modalNombre='modal-fuenteReclutamiento' idUsuario={this.state.idUsuario} guardaElementoNuevo={this.guardaElementoNuevo} />
                                    </div> : ''
                            }
                        </div>

                        <div class={this.state.activaFuentesBonosTabla} id="reclutamientoBonos">
                            {
                                this.state.mostrarTablaParametrosFuenteReclutamientoBonos ?
                                    <div>
                                        <Tabla url={datos.urlServicePy+'parametros/api_cat_fuente_reclutamiento_bonos/0'} tituloTabla={'Fuente de reclutamiento para bonos'} guardaElementoNuevo={this.guardaElementoNuevo}
                                            mostrarModal={0} modalNombre="modal-recluBonos" colorAgregar="#ED6C26" modalNombreEdicion="modal-recluBonosEdicion" tabla="api_cat_fuente_reclutamiento_bonos" idUsuario={this.state.idUsuario}/>
                                        <Modal modalNombre='modal-recluBonos' idUsuario={this.state.idUsuario} guardaElementoNuevo={this.guardaElementoNuevo} />
                                    </div> : ''
                            }
                        </div>

                        <div class={this.state.activaManejoTabla} id="manejo">
                            {
                                this.state.mostrarTablaParametrosManejoAgentes ?
                                    <div>
                                        <Tabla url={datos.urlServicePy+'parametros/api_cat_manejo_agentes_consolidado/0'} tituloTabla={'Manejo agentes consolidados'} guardaElementoNuevo={this.guardaElementoNuevo}
                                            mostrarModal={0} modalNombre="modal-manejo" colorAgregar="#ED6C26" modalNombreEdicion="modal-manejoEdicion" tabla="api_cat_manejo_agentes_consolidado" idUsuario={this.state.idUsuario} />
                                        <Modal modalNombre='modal-manejo' idUsuario={this.state.idUsuario} guardaElementoNuevo={this.guardaElementoNuevo} />
                                    </div> : ''
                            }
                        </div>

                        <div class={this.state.activaEstatusTabla} id="estatus">
                            {
                                this.state.mostrarTablaParametrosEstatusAps ?
                                    <div>
                                        <Tabla url={datos.urlServicePy+'parametros/api_cat_estatus_aps/0'} tituloTabla={'Estatus APS'} guardaElementoNuevo={this.guardaElementoNuevo}
                                            mostrarModal={0} modalNombre="modal-estatus" colorAgregar="#ED6C26" modalNombreEdicion="modal-estatusEdicion" tabla="api_cat_estatus_aps" idUsuario={this.state.idUsuario}/>
                                        <Modal modalNombre='modal-estatus' idUsuario={this.state.idUsuario} guardaElementoNuevo={this.guardaElementoNuevo} />
                                    </div> : ''
                            }
                        </div>


                    </div>
                </div>
            </div>

        )

    }


}