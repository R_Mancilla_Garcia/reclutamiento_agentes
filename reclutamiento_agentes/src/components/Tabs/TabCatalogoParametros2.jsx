import React, { Component } from "react";
import Tabla from "../tablas/Tabla";
import Modal from "../modal/Modal";
import datos from '../urls/datos.json'

const queryParams = new URLSearchParams(window.location.search)

export default class TabCatalogoParametros2 extends Component {
 
    constructor() {
        super();
        this.guardaElementoNuevo = this.guardaElementoNuevo.bind(this)
        this.state = {
            usuario: {},
            permisosSideBar: [],
            mostrarTablaParametrosFiguras: true,
            mostrarTablaParametrosParentesco: true,
            mostrarTablaParametrosEstados: true,
            mostrarTablaParametrosEmailGrupoDestino:true,
            mostrarTablaParametrosTipoFlujo:true,
            mostrarTablaParametrosEmailFlujo:true,
          
            idUsuario:0,


           activaFigurasTab:'nav-link',
           activaFigurasTabla:'tab-pane fade',

            activaParentescoTab:'nav-link',
            activaParentescoTabla:'tab-pane fade',

            activaEstadosTab:'nav-link',
            activaEstadosTabla:'tab-pane fade',

            activaEmailGrupoDestinoTab:'nav-link',
            activaEmailGrupoDestinoTabla:'tab-pane fade',

            activaTipoFlujoTab:'nav-link',
            activaTipoFlujoTabla:'tab-pane fade',

            activaEmailFlujoTab:'nav-link',
            activaEmailFlujoTabla:'tab-pane fade',

          



        };
    }

    guardaElementoNuevo(url, json, verboHTTP, banderaParametro) {
        this.setState({ [banderaParametro]: false })
        console.log("la url al persistir ", url)
        console.log("el json al persistir ", json)


        const requestOptions = {
            method: verboHTTP,
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };
        fetch(url, requestOptions)
            .then(response => response.json())
            .then(data => {
                console.log("la respuesta ", data)
                this.setState({ [banderaParametro]: true })
            });
    }

    UNSAFE_componentWillMount() {
        let selector=queryParams.get("activo")
        let idUsuario=queryParams.get("idUsuario")
        
        if(selector == "figuras"){
            this.setState({activaFigurasTab:'nav-link active',activaFigurasTabla:'tab-pane fade show active'})
        }
        if(selector == "parentesco"){
            this.setState({activaParentescoTab:'nav-link active',activaParentescoTabla:'tab-pane fade show active'})
        }
        if(selector == "areas"){
            this.setState({activaEstadosTab:'nav-link active',activaEstadosTabla:'tab-pane fade show active'})
        }

        if(selector == "emailGrupoDestino"){
            this.setState({activaEmailGrupoDestinoTab:'nav-link active',activaEmailGrupoDestinoTabla:'tab-pane fade show active'})
        }     
        if(selector == "tipoFlujo"){
            this.setState({activaTipoFlujoTab:'nav-link active',activaTipoFlujoTabla:'tab-pane fade show active'})
        }

        if(selector == "emailFlujo"){
            this.setState({activaEmailFlujoTab:'nav-link active',activaEmailFlujoTabla:'tab-pane fade show active'})
        }
      
        this.setState({idUsuario:idUsuario})



        console.log("querys ",idUsuario)




    }

    render() {
        return (

            <div class="row">
                <div class="col-12">
                    <ul class="nav nav-tabs nav-tabs-bottom" >
                        <li  class="nav-item" ><a href="#bajaTemporal"  class={this.state.activaFigurasTab} data-toggle="tab"><strong>Email descripciones</strong></a></li>
                        <li class="nav-item"><a href="#puestos" class={this.state.activaParentescoTab} data-toggle="tab"><strong>Procesos de notificación</strong></a></li>
                        <li class="nav-item"><a href="#areasOdeptos" class={this.state.activaEstadosTab} data-toggle="tab"><strong>Grupos destino</strong></a></li>
                        <li class="nav-item"><a href="#emailGrupoDestino" class={this.state.activaEmailGrupoDestinoTab} data-toggle="tab"><strong>Email grupo destino</strong></a></li>
                        <li class="nav-item"><a href="#tipoFlujo" class={this.state.activaTipoFlujoTab} data-toggle="tab"><strong>Tipos de flujo</strong></a></li>
                        <li class="nav-item"><a href="#emailFlujo" class={this.state.activaEmailFlujoTab} data-toggle="tab"><strong>Email Flujos</strong></a></li>
                       
                       {/* <li class="nav-item"><a href="#bancos" class={this.state.activaBancosTab} data-toggle="tab"><strong>Motivos de envío a reserva</strong></a></li>*/}
                       

                    </ul>
                    <div class="tab-content">

                        <div class={this.state.activaFigurasTabla} id="bajaTemporal">
                            {
                                this.state.mostrarTablaParametrosFiguras ?
                                    <div>
                                        <Tabla url={datos.urlServicePy+'parametros/api_mail_descripcion_mail/0'} tituloTabla={'Email descripción'} guardaElementoNuevo={this.guardaElementoNuevo}
                                            mostrarModal={0} modalNombre="modal-mail_descripcion" colorAgregar="#ED6C26"  modalNombreEdicion="modal-mail_descripcionEdicion" tabla="api_mail_descripcion_mail"  idUsuario={this.state.idUsuario}/>
                                       { <Modal modalNombre='modal-mail_descripcion' idUsuario={this.state.idUsuario} guardaElementoNuevo={this.guardaElementoNuevo} />}
                                    </div> : ''
                            }


                        </div>


                        <div class={this.state.activaParentescoTabla} id="puestos">
                            {
                                this.state.mostrarTablaParametrosParentesco ?
                                    <div>
                                        <Tabla url={datos.urlServicePy+'parametros/api_mail_proceso/0'} tituloTabla={'Proceso notificación'} guardaElementoNuevo={this.guardaElementoNuevo}
                                            mostrarModal={0} modalNombre="modal-proceso" colorAgregar="#ED9D26" modalNombreEdicion="modal-procesoEdicion" tabla="api_mail_proceso" idUsuario={this.state.idUsuario} />
                                        <Modal modalNombre='modal-proceso' idUsuario={this.state.idUsuario} guardaElementoNuevo={this.guardaElementoNuevo} />
                                    </div> : ''
                            }
                        </div>

                        <div class={this.state.activaEstadosTabla} id="areasOdeptos">
                            {
                               this.state.mostrarTablaParametrosEstados ?
                                    <div>
                                        <Tabla url={datos.urlServicePy+'parametros/api_grupo_destino/0'} tituloTabla={'Grupo destino'} guardaElementoNuevo={this.guardaElementoNuevo}
                                            mostrarModal={0} modalNombre="modal-grupoDestino" colorAgregar="#41B3D1" modalNombreEdicion="modal-grupoDestinoEdicion"  tabla="api_grupo_destino" idUsuario={this.state.idUsuario}/>
                                        <Modal modalNombre='modal-grupoDestino' idUsuario={this.state.idUsuario} guardaElementoNuevo={this.guardaElementoNuevo} />
                                    </div> : ''
                            }
                        </div>


                        <div class={this.state.activaEmailGrupoDestinoTabla} id="emailGrupoDestino">
                            {
                               this.state.mostrarTablaParametrosEmailGrupoDestino ?
                                    <div>
                                        <Tabla url={datos.urlServicePy+'parametros/api_mail_grupo_destino/0'} tituloTabla={'Grupo de destino'} guardaElementoNuevo={this.guardaElementoNuevo}
                                            mostrarModal={0} modalNombre="modal-mailGrupoDestino" colorAgregar="#41B3D1" modalNombreEdicion="modal-mailGrupoDestinoEdicion"  tabla="api_mail_grupo_destino" idUsuario={this.state.idUsuario}/>
                                        <Modal modalNombre='modal-mailGrupoDestino' idUsuario={this.state.idUsuario} guardaElementoNuevo={this.guardaElementoNuevo} />
                                    </div> : ''
                            }
                        </div>


                        <div class={this.state.activaTipoFlujoTabla} id="tipoFlujo">
                            {
                               this.state.mostrarTablaParametrosTipoFlujo ?
                                    <div>
                                        <Tabla url={datos.urlServicePy+'parametros/api_tipo_flujo/0'} tituloTabla={'Tipo flujo'} guardaElementoNuevo={this.guardaElementoNuevo}
                                            mostrarModal={0} modalNombre="modal-tipoFlujo" colorAgregar="#41B3D1" modalNombreEdicion="modal-tipoFlujoEdicion"  tabla="api_tipo_flujo" idUsuario={this.state.idUsuario}/>
                                        <Modal modalNombre='modal-tipoFlujo' idUsuario={this.state.idUsuario} guardaElementoNuevo={this.guardaElementoNuevo} />
                                    </div> : ''
                            }
                        </div>


                        <div class={this.state.activaEmailFlujoTabla} id="emailFlujo">
                            {
                               this.state.mostrarTablaParametrosEmailFlujo ?
                                    <div>
                                        <Tabla url={datos.urlServicePy+'parametros/api_vmail_flujo/0'} tituloTabla={'Tipo flujo'} guardaElementoNuevo={this.guardaElementoNuevo}
                                            mostrarModal={0} modalNombre="modal-mailFlujo" colorAgregar="#41B3D1" modalNombreEdicion="modal-mailFlujoEdicion"  tabla="api_mail_flujo" idUsuario={this.state.idUsuario}/>
                                        <Modal modalNombre='modal-mailFlujo' idUsuario={this.state.idUsuario} guardaElementoNuevo={this.guardaElementoNuevo} />
                                    </div> : ''
                            }
                        </div>

                    

                       


                    </div>
                </div>
            </div>

        )

    }



}