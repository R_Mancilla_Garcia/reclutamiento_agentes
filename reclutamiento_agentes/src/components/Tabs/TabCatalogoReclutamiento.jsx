import React, { Component } from "react";
import Tabla from "../tablas/Tabla";
import Modal from "../modal/Modal";
import datos from '../urls/datos.json'

const queryParams = new URLSearchParams(window.location.search)

export default class TabCatalogoReclutamiento extends Component {

    constructor() {
        super();
        this.guardaElementoNuevo = this.guardaElementoNuevo.bind(this)
        this.state = {
            usuario: {},
            permisosSideBar: [],
            mostrarTablaParametrosBaja: true,
            mostrarTablaParametrosBajaProspecto:true,
            mostrarTablaParametrosPuestos: true,
            mostrarTablaParametrosAreas: true,
            mostrarTablaParametrosBancos: true,
            mostrarTablaParametrosprofesiones: true,
            idUsuario:0,

            //activaPuestoTab:'active',
           // activaPuestoTabla:'show active'

           activaCausasTab:'nav-link',
           activaCausasTabla:'tab-pane fade',

           activaCausasProspectoTab:'nav-link',
           activaCausasProspectoTabla:'tab-pane fade',

            activaPuestoTab:'nav-link',
            activaPuestoTabla:'tab-pane fade',

            activaAreasTab:'nav-link',
            activaAreasTabla:'tab-pane fade',

            activaBancosTab:'nav-link',
            activaBancosTabla:'tab-pane fade',

            activaProfesionesTab:'nav-link',
            activaProfesionesTabla:'tab-pane fade'



        };
    }


    UNSAFE_componentWillMount() {
        let selector=queryParams.get("activo")
        let idUsuario=queryParams.get("idUsuario")
        
        if(selector == "causas"){
            this.setState({activaCausasTab:'nav-link active',activaCausasTabla:'tab-pane fade show active'})
        }
        if(selector == "causasProspecto"){
            this.setState({activaCausasProspectoTab:'nav-link active',activaCausasProspectoTabla:'tab-pane fade show active'})
        }
        if(selector == "puestos"){
            this.setState({activaPuestoTab:'nav-link active',activaPuestoTabla:'tab-pane fade show active'})
        }
        if(selector == "areas"){
            this.setState({activaAreasTab:'nav-link active',activaAreasTabla:'tab-pane fade show active'})
        }
        if(selector == "bancos"){
            this.setState({activaBancosTab:'nav-link active',activaBancosTabla:'tab-pane fade show active'})
        }
        if(selector == "profesiones"){
            this.setState({activaProfesionesTab:'nav-link active',activaProfesionesTabla:'tab-pane fade show active'})
        }
        this.setState({idUsuario:idUsuario})



        console.log("querys ",idUsuario)




    }

    guardaElementoNuevo(url, json, verboHTTP, banderaParametro) {
        this.setState({ [banderaParametro]: false })
        console.log("la url al persistir ", url)
        console.log("el json al persistir ", json)


        const requestOptions = {
            method: verboHTTP,
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };
        fetch(url, requestOptions)
            .then(response => response.json())
            .then(data => {
                console.log("la respuesta ", data)
                this.setState({ [banderaParametro]: true })
            });
    }

    mostrarModalParametrosReclutamiento() {
       

        this.setState({ mostrarModalParametrosReclutamiento: true })
    }

    render() {
        return (

            <div class="row">
                <div class="col-12">
                    <ul class="nav nav-tabs nav-tabs-bottom" >
                        <li  class="nav-item" ><a href="#bajaTemporal"  class={this.state.activaCausasTab} data-toggle="tab"><strong>Causas de baja laboral</strong></a></li>
                        <li  class="nav-item" ><a href="#bajaProspecto"  class={this.state.activaCausasProspectoTab} data-toggle="tab"><strong>Causas de baja prospecto</strong></a></li>
                        <li class="nav-item"><a href="#puestos" class={this.state.activaPuestoTab} data-toggle="tab"><strong>Puestos</strong></a></li>
                        <li class="nav-item"><a href="#areasOdeptos" class={this.state.activaAreasTab} data-toggle="tab"><strong>Areas o departamentos</strong></a></li>
                        <li class="nav-item"><a href="#bancos" class={this.state.activaBancosTab} data-toggle="tab"><strong>Bancos</strong></a></li>
                        <li class="nav-item"><a href="#profesiones" class={this.state.activaProfesionesTab} data-toggle="tab"><strong>Profesiones</strong></a></li>

                    </ul>
                    <div class="tab-content">

                        <div class={this.state.activaCausasTabla} id="bajaTemporal">
                            {
                                this.state.mostrarTablaParametrosBaja ?
                                    <div>
                                        <Tabla url={datos.urlServicePy+'parametros/api_cat_causas_baja/0'} tituloTabla={'Causas de baja laboral'} guardaElementoNuevo={this.guardaElementoNuevo}
                                            mostrarModal={0} modalNombre="modal-bajaTemporal" colorAgregar="#ED6C26"  modalNombreEdicion="modal-bajaTemporalEdicion" tabla="api_cat_causas_baja"  idUsuario={this.state.idUsuario}/>
                                        <Modal modalNombre='modal-bajaTemporal' idUsuario={this.state.idUsuario} guardaElementoNuevo={this.guardaElementoNuevo} />
                                    </div> : ''
                            }


                        </div>

                        <div class={this.state.activaCausasProspectoTabla} id="bajaProspecto">
                            {
                                this.state.mostrarTablaParametrosBajaProspecto ?
                                    <div>
                                        <Tabla url={datos.urlServicePy+'parametros/api_cat_causas_baja_prospecto/0'} tituloTabla={'Causas de baja prospecto'} guardaElementoNuevo={this.guardaElementoNuevo}
                                            mostrarModal={0} modalNombre="modal-bajaProspecto" colorAgregar="#ED6C26"  modalNombreEdicion="modal-bajaProspectoTemporalEdicion" tabla="api_cat_causas_baja_prospecto"  idUsuario={this.state.idUsuario}/>
                                        <Modal modalNombre='modal-bajaProspecto' idUsuario={this.state.idUsuario} guardaElementoNuevo={this.guardaElementoNuevo} />
                                    </div> : ''
                            }


                        </div>


                        <div class={this.state.activaPuestoTabla} id="puestos">
                            {
                                this.state.mostrarTablaParametrosPuestos ?
                                    <div>
                                        <Tabla url={datos.urlServicePy+'parametros/api_cat_puestos/0'} tituloTabla={'Puestos'} guardaElementoNuevo={this.guardaElementoNuevo}
                                            mostrarModal={0} modalNombre="modal-puestos" colorAgregar="#ED9D26" modalNombreEdicion="modal-puestosEdicion" tabla="api_cat_puestos" idUsuario={this.state.idUsuario} />
                                        <Modal modalNombre='modal-puestos' idUsuario={this.state.idUsuario} guardaElementoNuevo={this.guardaElementoNuevo} />
                                    </div> : ''
                            }
                        </div>

                        <div class={this.state.activaAreasTabla} id="areasOdeptos">
                            {
                                this.state.mostrarTablaParametrosAreas ?
                                    <div>
                                        <Tabla url={datos.urlServicePy+'parametros/api_cat_areas/0'} tituloTabla={'Areas o departamentos'} guardaElementoNuevo={this.guardaElementoNuevo}
                                            mostrarModal={0} modalNombre="modal-areas" colorAgregar="#41B3D1" modalNombreEdicion="modal-areasEdicion"  tabla="api_cat_areas" idUsuario={this.state.idUsuario}/>
                                        <Modal modalNombre='modal-areas' idUsuario={this.state.idUsuario} guardaElementoNuevo={this.guardaElementoNuevo} />
                                    </div> : ''
                            }
                        </div>

                        <div class={this.state.activaBancosTabla} id="bancos">
                            {
                                this.state.mostrarTablaParametrosBancos ?
                                    <div>
                                        <Tabla url={datos.urlServicePy+'parametros/api_cat_bancos/0'} tituloTabla={'Bancos'} guardaElementoNuevo={this.guardaElementoNuevo}
                                            mostrarModal={0} modalNombre="modal-bancos" colorAgregar="#AD79B7" modalNombreEdicion="modal-bancosEdicion"  tabla="api_cat_bancos" idUsuario={this.state.idUsuario}/>
                                        <Modal modalNombre='modal-bancos' idUsuario={this.state.idUsuario} guardaElementoNuevo={this.guardaElementoNuevo} />
                                    </div> : ''
                            }
                        </div>

                        <div class={this.state.activaProfesionesTabla} id="profesiones">
                            {
                                this.state.mostrarTablaParametrosprofesiones ?
                                    <div>
                                        <Tabla url={datos.urlServicePy+'parametros/api_cat_profeciones/0'} tituloTabla={'Profesiones'} guardaElementoNuevo={this.guardaElementoNuevo}
                                            mostrarModal={0} modalNombre="modal-profesiones" colorAgregar="#69A3D6" modalNombreEdicion="modal-profesionesEdicion" tabla="api_cat_profeciones" idUsuario={this.state.idUsuario} />
                                        <Modal modalNombre='modal-profesiones' idUsuario={this.state.idUsuario} guardaElementoNuevo={this.guardaElementoNuevo} />
                                    </div> : ''
                            }
                        </div>


                    </div>
                </div>
            </div>

        )

    }


}