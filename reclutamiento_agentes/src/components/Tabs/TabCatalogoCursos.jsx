import React, { Component } from "react";
import Tabla from "../tablas/Tabla";
import Modal from "../modal/Modal";
import datos from '../urls/datos.json'

const queryParams = new URLSearchParams(window.location.search)

export default class TabCatalogoCursos extends Component {

    constructor() {
        super();
        this.guardaElementoNuevo = this.guardaElementoNuevo.bind(this)
        this.cancelarModal = this.cancelarModal.bind(this)
        this.mostrarMensajeModal = this.mostrarMensajeModal.bind(this)
        this.state = {
            usuario: {},
            idUsuario: 0,
            permisosSideBar: [],
            mostrarTablaCursosInmersion: true,
            mostrarTablaCursosIDEAS: true,
            mostrarTablaParametrosAreas: true,
            mostrarTablaParametrosBancos: true,
            mostrarTablaParametrosprofesiones: true,

            activaInmersionTab: 'nav-link',
            activaInmersionTabla: 'tab-pane fade',

            activaIdeasTab: 'nav-link',
            activaIdeasTabla: 'tab-pane fade',
            activaModal : 0

        };
    }



    UNSAFE_componentWillMount() {
        let selector = queryParams.get("activo")
        let idUsuario = queryParams.get("idUsuario")

        if (selector == "inmersion") {
            this.setState({ activaInmersionTab: 'nav-link active', activaInmersionTabla: 'tab-pane fade show active' })
        }
        if (selector == "ideas") {
            this.setState({ activaIdeasTab: 'nav-link active', activaIdeasTabla: 'tab-pane fade show active' })
        }
        this.setState({ idUsuario: idUsuario })
        console.log("querys ", queryParams.get("activo"))
    }

    guardaElementoNuevo(url, json, verboHTTP, banderaParametro) {
        this.setState({ [banderaParametro]: false })
        console.log("la url al persistir ", url)
        console.log("el json al persistir ", json)


        const requestOptions = {
            method: verboHTTP,
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };
        fetch(url, requestOptions)
            .then(response => response.json())
            .then(data => {
                console.log("la respuesta ", data)
                this.setState({ [banderaParametro]: true ,activaModal : false})
            });
    }

    mostrarMensajeModal(valorModal) {
        console.log("entro aquí en mostrarMensajeModal")
        this.setState({ activaModal :valorModal})
    }

    cancelarModal(banderaParametro) {
        console.log("Entro en parametro cancelar");
        this.setState({ [banderaParametro]: true })
    }

    mostrarModalParametrosReclutamiento() {
        console.log("entrando a prender")
        this.setState({ mostrarModalParametrosReclutamiento: true })
    }

    render() {
        return (

            <div class="row">
                <div class="col-12">
                    <ul class="nav nav-tabs nav-tabs-bottom">
                        <li class="nav-item"><a href="#inmersion" class={this.state.activaInmersionTab} data-toggle="tab"><strong>Cursos de inmersión</strong></a></li>
                        <li class="nav-item"><a href="#ideas" class={this.state.activaIdeasTab} data-toggle="tab"><strong>Temario cursos de Inmersión</strong></a></li>



                    </ul>
                    <div class="tab-content">

                        <div class={this.state.activaInmersionTabla} id="inmersion">
                            {
                                this.state.mostrarTablaCursosInmersion ?
                                    <div>
                                        <Tabla url={datos.urlServicePy+'parametros/api_cat_curso_inmersion/0'} tituloTabla={'Cursos de inmersión'} guardaElementoNuevo={this.guardaElementoNuevo} mostrarMensajeModal={this.mostrarMensajeModal}
                                            mostrarModal={0} modalNombre="modal-inmersion" colorAgregar="#ED6C26" modalNombreEdicion="modal-inmersionEdicion" tabla="api_cat_curso_inmersion" idUsuario={this.state.idUsuario} />
                                        <Modal modalNombre='modal-inmersion' idUsuario={this.state.idUsuario} guardaElementoNuevo={this.guardaElementoNuevo} mostrarMensajeModal={this.mostrarMensajeModal} />
                                        <div className="row">
                                            <div className="col-xs-12 col-lg-12">
                                                <div className="form-group">
                                                    <div className="input-group">
                                                        {
                                                            this.state.activaModal == 1 ?
                                                                <span id="valorDuplicado" style={{ color: "red" }}>El Curso de Inmersión ingresado ya se encuentra registrado, ingresar un codigo de Generación diferente</span> : ''
                                                        }
                                                        {
                                                            this.state.activaModal == 2 ?
                                                                <span id="valorDuplicado" style={{ color: "red" }}>No se puede modificar el Curso de Inmersión, ya se encuentra en otro registrado</span> : ''
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> : ''
                            }


                        </div>


                        <div class={this.state.activaIdeasTabla} id="ideas">
                            {
                                this.state.mostrarTablaCursosIDEAS ?
                                    <div>
                                        <Tabla url={datos.urlServicePy+'parametros/api_cat_curso_ideas/0'} tituloTabla={'Temario cursos de Inmersión'} guardaElementoNuevo={this.guardaElementoNuevo}
                                            mostrarModal={0} modalNombre="modal-cursoIdeas" colorAgregar="#ED6C26" modalNombreEdicion="modal-cursoIdeasEdicion" tabla="api_cat_curso_ideas" idUsuario={this.state.idUsuario} cancelarModal={this.cancelarModal} />
                                        <Modal modalNombre='modal-cursoIdeas' idUsuario={this.state.idUsuario} guardaElementoNuevo={this.guardaElementoNuevo} cancelarModal={this.cancelarModal} />
                                    </div> : ''
                            }
                        </div>



                    </div>
                </div>
            </div>

        )

    }


}