import React, { Component } from "react";
import Tabla from "../tablas/Tabla";
import Modal from "../modal/Modal";
import datos from '../urls/datos.json'

const queryParams = new URLSearchParams(window.location.search)

export default class TabCatalogoParametros extends Component {
 
    constructor() {
        super();
        this.guardaElementoNuevo = this.guardaElementoNuevo.bind(this)
        this.state = {
            usuario: {},
            permisosSideBar: [],
            mostrarTablaParametrosFiguras: true,
            mostrarTablaParametrosParentesco: true,
            mostrarTablaParametrosEstados: true,
            mostrarTablaParametrosSistema: true,
            idUsuario:0,
            tipoUsuario: null,


           activaFigurasTab:'nav-link',
           activaFigurasTabla:'tab-pane fade',

            activaParentescoTab:'nav-link',
            activaParentescoTabla:'tab-pane fade',

            activaEstadosTab:'nav-link',
            activaEstadosTabla:'tab-pane fade',

            activaSistemasTab: 'nav-link',
            activaSistemasTabla: 'tab-pane fade',


        };
    }

    guardaElementoNuevo(url, json, verboHTTP, banderaParametro) {
        this.setState({ [banderaParametro]: false })
        console.log("la url al persistir ", url)
        console.log("el json al persistir ", json)


        const requestOptions = {
            method: verboHTTP,
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(json)
        };
        fetch(url, requestOptions)
            .then(response => response.json())
            .then(data => {
                console.log("la respuesta ", data)
                this.setState({ [banderaParametro]: true })
            });
    }

    UNSAFE_componentWillMount() {
        let selector=queryParams.get("activo")
        let idUsuario=queryParams.get("idUsuario")
        let tipoUsuario = queryParams.get("tipoUsuario")
        
        if(selector == "figuras"){
            this.setState({activaFigurasTab:'nav-link active',activaFigurasTabla:'tab-pane fade show active'})
        }
        if(selector == "parentesco"){
            this.setState({activaParentescoTab:'nav-link active',activaParentescoTabla:'tab-pane fade show active'})
        }
        if(selector == "areas"){
            this.setState({activaEstadosTab:'nav-link active',activaEstadosTabla:'tab-pane fade show active'})
        }
        if(selector == "parametrossistema"){
            this.setState({activaEstadosTab:'nav-link active',activaEstadosTabla:'tab-pane fade show active'})
        }
      
        this.setState({idUsuario:idUsuario,tipoUsuario:tipoUsuario})



        console.log("querys ",idUsuario)




    }

    render() {
        return (

            <div class="row">
                <div class="col-12">
                    <ul class="nav nav-tabs nav-tabs-bottom" >
                       {/* <li  class="nav-item" ><a href="#bajaTemporal"  class={this.state.activaFigurasTab} data-toggle="tab"><strong>Figuras comerciales</strong></a></li> */}
                        <li class="nav-item"><a href="#puestos" class={this.state.activaParentescoTab} data-toggle="tab"><strong>Parentescos</strong></a></li>
                        <li class="nav-item"><a href="#areasOdeptos" class={this.state.activaEstadosTab} data-toggle="tab"><strong>Estados civíles</strong></a></li>
                        <li class="nav-item"><a href="#parametrossistema" class={this.state.activaSistemasTab} data-toggle="tab"><strong>Convenio de Arranque</strong></a></li>
                       
                       {/* <li class="nav-item"><a href="#bancos" class={this.state.activaBancosTab} data-toggle="tab"><strong>Motivos de envío a reserva</strong></a></li>*/}
                       

                    </ul>
                    <div class="tab-content">
{/*
                        <div class={this.state.activaFigurasTabla} id="bajaTemporal">
                            {
                                this.state.mostrarTablaParametrosFiguras ?
                                    <div>
                                        <Tabla url={datos.urlServicePy+'parametros/api_cat_figura/0'} tituloTabla={'Figura comercial'} guardaElementoNuevo={this.guardaElementoNuevo}
                                            mostrarModal={0} modalNombre="modal-figura" colorAgregar="#ED6C26"  modalNombreEdicion="modal-figuraEdicion" tabla="api_cat_figura"  idUsuario={this.state.idUsuario} tipoUsuario = {this.state.tipoUsuario}/>
                                       { <Modal modalNombre='modal-figura' idUsuario={this.state.idUsuario} guardaElementoNuevo={this.guardaElementoNuevo} tipoUsuario = {this.state.tipoUsuario}/>}
                                    </div> : ''
                            }


                        </div>*/}


                        <div class={this.state.activaParentescoTabla} id="puestos">
                            {
                                this.state.mostrarTablaParametrosParentesco ?
                                    <div>
                                        <Tabla url={datos.urlServicePy+'parametros/api_cat_parentesco/0'} tituloTabla={'Parentescos'} guardaElementoNuevo={this.guardaElementoNuevo}
                                            mostrarModal={0} modalNombre="modal-parentesco" colorAgregar="#ED9D26" modalNombreEdicion="modal-parentescoEdicion" tabla="api_cat_parentesco" idUsuario={this.state.idUsuario} tipoUsuario = {this.state.tipoUsuario}/>
                                        <Modal modalNombre='modal-parentesco' idUsuario={this.state.idUsuario} guardaElementoNuevo={this.guardaElementoNuevo} tipoUsuario = {this.state.tipoUsuario}/>
                                    </div> : ''
                            }
                        </div>

                        <div class={this.state.activaEstadosTabla} id="areasOdeptos">
                            {
                               this.state.mostrarTablaParametrosEstados ?
                                    <div>
                                        <Tabla url={datos.urlServicePy+'parametros/api_cat_estadocivil/0'} tituloTabla={'Estado civíl'} guardaElementoNuevo={this.guardaElementoNuevo}
                                            mostrarModal={0} modalNombre="modal-estadocivil" colorAgregar="#41B3D1" modalNombreEdicion="modal-estadocivilEdicion"  tabla="api_cat_estadocivil" idUsuario={this.state.idUsuario} tipoUsuario = {this.state.tipoUsuario}/>
                                        <Modal modalNombre='modal-estadocivil' idUsuario={this.state.idUsuario} guardaElementoNuevo={this.guardaElementoNuevo} tipoUsuario = {this.state.tipoUsuario}/>
                                    </div> : ''
                            }
                        </div>

                        <div class={this.state.activaSistemasTabla} id="parametrossistema">
                            {
                               this.state.mostrarTablaParametrosSistema ?
                                    <div>
                                        <Tabla url={datos.urlServicePy+'parametros/api_cat_parametros_sistema/0'} tituloTabla={'Convenio de Arranque'} guardaElementoNuevo={this.guardaElementoNuevo}
                                            mostrarModal={0} modalNombre="modal-parametrossistema" colorAgregar="#41B3D1" modalNombreEdicion="modal-parametrossistemaEdicion"  tabla="api_cat_parametros_sistema" idUsuario={this.state.idUsuario} tipoUsuario = {this.state.tipoUsuario}/>
                                        <Modal modalNombre='modal-parametrossistema' idUsuario={this.state.idUsuario} guardaElementoNuevo={this.guardaElementoNuevo} tipoUsuario = {this.state.tipoUsuario}/>
                                    </div> : ''
                            }
                        </div>

                       
                       {/*
                        <div class={this.state.activaBancosTabla} id="bancos">
                            {
                                this.state.mostrarTablaParametrosBancos ?
                                    <div>
                                        <Tabla url={datos.urlServicePy+'parametros/api_cat_motivo_envio_reserva/0'} tituloTabla={'Motivos de envío a reserva'} guardaElementoNuevo={this.guardaElementoNuevo}
                                            mostrarModal={0} modalNombre="modal-envio_reserva" colorAgregar="#AD79B7" modalNombreEdicion="modal-envio_reservaEdicion"  tabla="api_cat_motivo_envio_reserva" idUsuario={this.state.idUsuario}/>
                                        <Modal modalNombre='modal-envio_reserva' idUsuario={this.state.idUsuario} guardaElementoNuevo={this.guardaElementoNuevo} />
                                    </div> : ''
                            }
                        </div>*/}

                       


                    </div>
                </div>
            </div>

        )

    }



}