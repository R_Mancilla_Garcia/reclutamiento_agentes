import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { render } from "react-dom";
import { BrowserRouter, 
  Routes,
  Route, } from "react-router-dom";
import Login from './components/Login/Login';
import LoginComponent from './components/Login/LoginComponent';
import SideBar  from './components/InicioMenu/SideBar';
import DashBoard  from './components/InicioMenu/Dashboard';
import Tabla from './components/tablas/Tabla';
import CausasBaja from './components/InicioMenu/CausasBaja';
import Reservas from './components/InicioMenu/Reservas';
import Cursos from './components/InicioMenu/Cursos';
import AdminEmpleadosAPS from './components/InicioMenu/AdminEmpleadosAPS';
import AdminEmpleadosAPSNuevo from './components/InicioMenu/adminEmpleadosAPSNuevo';
import RegistroReferido from './components/InicioMenu/RegistroReferido';
import RegistroProspecto from './components/InicioMenu/RegistroProspectos';
import ProcesoReclutamiento from './components/procesoReclutamiento/ProcesoReclutamiento';
import RegistroEntrevista from './components/formsReclutamiento/RegistroEntrevista';
import EntrevistaProfunda from './components/formsReclutamiento/EntrevistaProfunda';
import CursoCedulaA from './components/InicioMenu/CursoCedulasA';
import CatalogoParametros from './components/InicioMenu/CatalogosParametros';
import CatalogoParametros2 from './components/InicioMenu/CatalogoParametros2';
import IndicadoresDesempeno from './components/graficas/indicadoresDesempeno';
import GraficasDesempeno from './components/graficas/GraficasDesempeno';
const rootElement = document.getElementById("root");
ReactDOM.render(
  <BrowserRouter>
  <Routes>
      <Route path="/" element={<LoginComponent />} />
      <Route path="/paramReclutamiento" element={<DashBoard />} />
      <Route path="/causasBaja" element={<CausasBaja />} />
      <Route path="/reservas" element={<Reservas />} />
      <Route path="/cursos" element={<Cursos />} />
      <Route path="/adminEmpleadosAPS" element={<AdminEmpleadosAPS />} />
      <Route path="/adminEmpleadosAPSNuevo" element={<AdminEmpleadosAPSNuevo />} />
      <Route path="/registroReferido" element={<RegistroReferido />} />
      <Route path="/registroProspecto" element={<RegistroProspecto />} />
      <Route path="/procesoReclutamiento" element={<ProcesoReclutamiento />} />
      <Route path="/registroEntrevista" element={<RegistroEntrevista />} />
      <Route path="/entrevistaProfunda" element={<EntrevistaProfunda />} />
      <Route path="/cursoCedulaA" element={<CursoCedulaA />} />
      <Route path="/catalogoParametros" element={<CatalogoParametros />} />
      <Route path="/catalogoParametros2" element={<CatalogoParametros2 />} />
      <Route path='/indicadoresDesempeno' element={<IndicadoresDesempeno/>}/>
      <Route path='/graficasDesempeno' element={<GraficasDesempeno/>}/>

     
    </Routes>
    
  </BrowserRouter>,
  rootElement
);



// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
//reportWebVitals();
